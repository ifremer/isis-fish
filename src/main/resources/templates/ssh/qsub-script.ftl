<#--
 #%L
 IsisFish
 
 $Id$
 $HeadURL$
 %%
 Copyright (C) 2009 - 2020 Ifremer, CodeLutin
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the 
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public 
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
#!/bin/csh

cd "${ISIS_INSTALL}"

setenv R_HOME /appli/R/3.2.4-intel-12.1.5/lib64/R
setenv LD_LIBRARY_PATH ${r"${R_HOME}"}/lib

<#assign SIMULATION_DIRECTORY="$SCRATCH/isis-fish-4/isis-database/simulations" />
<#assign commonCommand="${JAVA_BIN_PATH} -Duser.home=$SCRATCH -DR.type=jni -Xmx${JAVA_MEMORY} -XX:+UseSerialGC -jar isis-fish*.jar" />
<#assign commonOptions="--option launch.ui false --option perform.vcsupdate false --option perform.migration false --option perform.cron false" />
<#if QSUB_MULTIPLE_JOB>
	<#-- localsimulationid is short simulation id version -->
	<#assign localsimulationid='${SIMULATION_ID}_$PBS_ARRAY_INDEX' />
	<#if SIMULATION_STANDALONE_ZIP>
		<#assign localsimulationzip='${SIMULATION_ZIP?replace("\\\\Q${SIMULATION_ID}\\\\E_[0-9]+", "${SIMULATION_ID}_\\\\$PBS_ARRAY_INDEX","ri")}' />
	<#else>
		<#assign localsimulationzip='${SIMULATION_ZIP}' />
	</#if>
	<#assign localsimulationresultzip='${SIMULATION_RESULT_ZIP?replace("\\\\Q${SIMULATION_ID}\\\\E_[0-9]+", "${SIMULATION_ID}_\\\\$PBS_ARRAY_INDEX","ri")}' />
	<#assign localsimulationprescript='${SIMULATION_PRESCRIPT?replace("\\\\Q${SIMULATION_ID}\\\\E_[0-9]+", "${SIMULATION_ID}_\\\\$PBS_ARRAY_INDEX","ri")}' />
	<#assign localsimulationoutput='${ISIS_TEMP}simulation-${SIMULATION_ID}_$PBS_ARRAY_INDEX-output.txt' />
<#else>
	<#assign localsimulationid='${SIMULATION_ID}' />
	<#assign localsimulationzip='${SIMULATION_ZIP}' />
	<#assign localsimulationresultzip='${SIMULATION_RESULT_ZIP}' />
	<#assign localsimulationprescript='${SIMULATION_PRESCRIPT}' />
	<#assign localsimulationoutput='${ISIS_TEMP}simulation-${SIMULATION_ID}-output.txt' />
</#if>
<#if SIMULATION_PRESCRIPT?length &gt; 0>
${commonCommand} ${commonOptions} --simulateRemotellyWithPreScript "${localsimulationid}" "${localsimulationzip}" "${localsimulationresultzip}" "${localsimulationprescript}" >& "${localsimulationoutput}"
<#else>
${commonCommand} ${commonOptions} --simulateRemotelly "${localsimulationid}" "${localsimulationzip}" "${localsimulationresultzip}" >& "${localsimulationoutput}"
</#if>

cp "${SIMULATION_DIRECTORY}/${localsimulationid}/information" "${ISIS_TEMP}simulation-${localsimulationid}-information.txt"
rm -rf "${SIMULATION_DIRECTORY}/${localsimulationid}"
