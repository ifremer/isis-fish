<#--
 #%L
 IsisFish
 
 $Id$
 $HeadURL$
 %%
 Copyright (C) 2009 - 2016 Ifremer, CodeLutin
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the 
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public 
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
/*
 * Copyright (C) ${date?date?string("yyyy")} ${author}
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 */
package simulators;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.nuiton.math.matrix.*;

import fr.ifremer.isisfish.annotations.Doc;
import fr.ifremer.isisfish.entities.*;
import fr.ifremer.isisfish.simulator.Simulator;
import fr.ifremer.isisfish.simulator.SimulationContext;

/**
 * ${name}.java
 */
public class ${name} implements Simulator {

    /** to use log facility, just put in your code: log.info("..."); */
    private static Log log = LogFactory.getLog(${name}.class);

    public void simulate(SimulationContext context) throws Exception {
        // put your code here
	}
}
