<#--
 #%L
 IsisFish
 
 $Id: rule.ftl 4277 2015-06-21 17:24:03Z echatellier $
 $HeadURL: http://svn.codelutin.com/isis-fish/trunk/src/main/resources/templates/script/rule.ftl $
 %%
 Copyright (C) 2015 - 2016 Ifremer, CodeLutin
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the 
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public 
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
/*
 * Copyright (C) ${date?date?string("yyyy")} ${author}
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 */
package resultinfos;

import fr.ifremer.isisfish.result.AbstractResultInfo;

/**
 * ${name}.java
 */
public class ${name} extends AbstractResultInfo {

    public static final String NAME = ${name}.class.getSimpleName();

    protected String[] necessaryResult = {
        // put here all necessary result for this rule
	    // example: 
	    // MatrixBiomass.NAME,
        // MatrixNetValueOfLandingsPerStrategyMet.NAME,
    };

    @Override
    public String[] getNecessaryResult() {
        return necessaryResult;
    }

    @Override
    public String getDescription() {
        return "do the doc of Result ${name}";
    }
}
