<#--
 #%L
 IsisFish
 
 $Id$
 $HeadURL$
 %%
 Copyright (C) 2014 - 2016 Ifremer, CodeLutin
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the 
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public 
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
/*
 * Copyright (C) ${date?date?string("yyyy")} ${author}
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 */
package exports;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import java.io.Writer;

import org.nuiton.math.matrix.*;
import resultinfos.*;

import fr.ifremer.isisfish.annotations.Doc;
import fr.ifremer.isisfish.entities.*;
import fr.ifremer.isisfish.export.ExportStep;
import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.simulator.ResultManager;
import fr.ifremer.isisfish.types.TimeStep;

/**
 * ${name}.java
 */
public class ${name} implements ExportStep {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    private static Log log = LogFactory.getLog(${name}.class);

    protected String[] necessaryResult = {
        // put here all necessary result for this rule
        // example:
        // MatrixBiomass.NAME,
        // MatrixNetValueOfLandingsPerStrategyMet.NAME,
    };

    /**
     * Necessary results names for export execution.
     *
     * @return the necessaryResult name
     */
    @Override
    public String[] getNecessaryResult() {
        return this.necessaryResult;
    }

    /**
     * Return Export description.
     *
     * @return string displayable to the end user
     */
    @Override
    public String getExportFilename() {
        return "${name}";
    }

    /**
     * Return filename used to contains export data.
     *
     * @return filename by example "myexport"
     */
    @Override
    public String getExtensionFilename() {
        return ".csv";
    }

    /**
     * Return extension used as filename name extension.
     *
     * @return extension by example ".csv"
     */
    @Override
    public String getDescription() {
        return "TODO export description";
    }

    /**
     * Appeler au début de l'export
     *
     * @param simulation la simulation dont on souhaite exporter les resultats
     * @param out la sortie sur lequel il faut ecrire l'export
     * @throws Exception if export fail
     */
    public void exportBegin(SimulationStorage simulation, Writer out)
            throws Exception {
        // if you need to do something at the begin of export, put your code here
    }

    /**
     * Exporte les resultats, cette methode est appellee a la fin de chaque pas
     * de temps. Elle peut ecrire dans le fichier via le parametre out,
     * ou collecter les informations et les ecrires a la fin de la simulation
     * lorsque la methode {@link #export(fr.ifremer.isisfish.datastore.SimulationStorage, java.io.Writer)
     * est appelee.
     *
     * @param simulation la simulation dont on souhaite exporter les resultats
     * @param step le pas de temps courant de la simulation
     * @param out la sortie sur lequel il faut ecrire l'export
     * @throws Exception if export fail
     */
    @Override
    public void export(SimulationStorage simulation, TimeStep step, Writer out)
            throws Exception {
        // If your export can be done by step, write your code here.
        // You can collect data and write result in exportEnd method.
        // If you need to access database during simulation
        // you can use: TopiaContext tx = SimulationContext.get().getDB()

        // example:
        // MatrixND mat = simulation.getResultStorage().getMatrix(step,
        //                MatrixBiomass.class.getSimpleName());
        // if (mat != null) { // can be null if no result for this step
        //     for (MatrixIterator i = mat.iterator(); i.hasNext();) {
        //         i.next();
        //         Object[] sems = i.getSemanticsCoordinates();
        //         Strategy strategy = (Strategy) sems[0];
        //         Metier metier = (Metier) sems[1];
        //
        //         double val = i.getValue();
        //         out.write(step.getStep() + strategy.getName() + ";"
        //             + metier.getName() + val + "\n");
        //     }
        // }
    }

    /**
     * Appeler a la fin de l'export
     *
     * @param simulation la simulation dont on souhaite exporter les resultats
     * @param out la sortie sur lequel il faut ecrire l'export
     * @throws Exception if export fail
     */
    public void exportEnd(SimulationStorage simulation, Writer out)
            throws Exception {
        // if you need to do something at the end of export, put your code here
    }

}
