<#--
 #%L
 IsisFish
 
 $Id$
 $HeadURL$
 %%
 Copyright (C) 2009 - 2016 Ifremer, CodeLutin
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the 
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public 
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
/*
 * Copyright (C) ${date?date?string("yyyy")} ${author}
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 */
package sensitivityanalysis;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.nuiton.math.matrix.*;

import java.io.File;

import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.entities.*;
import fr.ifremer.isisfish.simulator.sensitivity.*;
import fr.ifremer.isisfish.annotations.Doc;

/**
 * ${name}.java
 */
public class ${name} extends AbstractSensitivityAnalysis {

    /** to use log facility, just put in your code: log.info("..."); */
    private static Log log = LogFactory.getLog(${name}.class);
    
    /**
     * Return calculator description.
     * 
     * @return calculator description
     * @throws Exception
     */
    public String getDescription() {
    	// TODO change descrition
        return "TODO ${name} description plan";
    }
    
    /**
     * Retourne vrai si le calculateur sait gerer la cardinalité
     * des facteurs continue.
     * 
     * @return {@code true} s'il sait la gerer
     */
    public boolean canManageCardinality() {
        return false;
    }
    
    /**
     * Envoi un plan a faire analyser par l'outils d'analyse de sensibilité.
     * 
     * Retourne un {@link SensitivityScenarios} qui représente l'ensemble des
     * scenarios à prendre en compte pour les simulations.
     * 
     * @param plan plan a analyser
     * @param outputDirectory master sensitivity export directory
     * 
     * @return un {@link SensitivityScenarios}
     * @throws SensitivityException if calculator impl fail to execute
     * 
     * @see DesignPlan
     * @see Scenario
     * @see SensitivityScenarios
     */
    public SensitivityScenarios compute(DesignPlan plan, File outputDirectory) 
    	throws SensitivityException {
    
    	SensitivityScenarios sensitivityScenarios = new SensitivityScenarios();
    	
    	// FIXME add implemantation
    	
    	return null;
    }
    
    /**
     * Permet de renvoyer les resultats de simulations à l'outils de d'analyse
     * de sensibilité.
     * 
     * @param simulationStorages
     *            ensemble des {@link SimulationStorage} qui ont résultés des
     *            simulations
     * @param outputDirectory master sensitivity export directory
     * @throws SensitivityException
     *             if calculator impl fail to execute
     * 
     * @see SensitivityScenarios
     */
    public void analyzeResult(List<SimulationStorage> simulationStorages,
    		File outputDirectory) throws SensitivityException {
    
    }
}
