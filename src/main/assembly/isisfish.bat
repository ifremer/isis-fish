@echo off
SetLocal EnableDelayedExpansion

:: Uncomment to easy configure memory for ISIS-Fish
:: SET MEMORY="-Xmx2048M"

if exist isisfish.jri (
    set /p JRI_PATH=<isisfish.jri
    echo Using JRI from path : !JRI_PATH!
    set JAVA_LIBRARY_PATH="-Djava.library.path=!JRI_PATH!"
)

echo [Script] Isis starting...
echo java !MEMORY! !JAVA_LIBRARY_PATH! -jar ${project.build.finalName}.${project.packaging} %*
java !MEMORY! !JAVA_LIBRARY_PATH! -jar ${project.build.finalName}.${project.packaging} %* > debug.txt 2>&1
