#!/bin/bash

# Uncomment this to configure java memory
#MEMORY="-Xmx2048M"

JAVA_LIBRARY_PATH="-Djava.library.path=$(cat ./isisfish.jri 2>/dev/null)"

echo "Running : java $MEMORY $JAVA_LIBRARY_PATH -jar ${project.build.finalName}.${project.packaging} $* &> debug.txt"
java $MEMORY $JAVA_LIBRARY_PATH -jar ${project.build.finalName}.${project.packaging} $* &> debug.txt