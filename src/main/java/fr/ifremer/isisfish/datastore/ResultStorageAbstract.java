/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer, Code Lutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.datastore;

import fr.ifremer.isisfish.export.ExportContext;
import fr.ifremer.isisfish.util.matrix.EntitySemanticsDecorator;

import static org.nuiton.i18n.I18n.t;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixIterator;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.math.matrix.MatrixSemanticsDecorator;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.util.ArrayUtil;
import org.nuiton.util.HashList;

import fr.ifremer.isisfish.IsisFishException;
import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.export.ExportInfo;
import fr.ifremer.isisfish.export.SensitivityExport;
import fr.ifremer.isisfish.result.ResultInfoHelper;
import fr.ifremer.isisfish.rule.Rule;
import fr.ifremer.isisfish.simulator.Objective;
import fr.ifremer.isisfish.simulator.Optimization;
import fr.ifremer.isisfish.simulator.SimulationContext;
import fr.ifremer.isisfish.simulator.SimulationException;
import fr.ifremer.isisfish.simulator.SimulationPlan;
import fr.ifremer.isisfish.simulator.SimulationResultGetter;
import fr.ifremer.isisfish.simulator.SimulationResultListener;
import fr.ifremer.isisfish.types.TimeStep;
import java.util.LinkedHashMap;
import org.nuiton.math.matrix.MatrixFactory;

/**
 * Classe abstraite servant a factoriser tous les traitements commun au differente
 * implantation. Lorsqu'on herite de cette classe il reste a definir la lecture
 * et l'ecriture reel des resultats (en memoire, en base, binary file, ...)
 * 
 * Created: 24 juin 2014
 *
 * @author Benjamin Poussin : poussin@codelutin.com
 * @version $Revision$
 *
 * Mise a jour: $Date$
 * par : $Author$
 */
public abstract class ResultStorageAbstract implements SimulationResultListener,
        SimulationResultGetter, ResultStorage { // ResultStorage

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(ResultStorageAbstract.class);

    protected SimulationStorage simulation = null;
    protected EntitySemanticsDecorator decorator;

    /** result enabled */
    transient protected Set<String> enabledResult = null;


    /**
     * Return one result for step and name. Matrix returned must be undecorated
     * (semantics must be string representation of object)
     *
     * @param step
     * @param name
     * @return
     */
    abstract protected MatrixND readResult(TimeStep step, String name);
//    /**
//     * Return all available result for name in argument
//     * @param name
//     * @return
//     */
//    abstract protected Map<TimeStep, MatrixND> readResult(String name);
    /**
     * write result
     * @param step
     * @param name
     * @param mat
     */
    abstract protected void writeResult(TimeStep step, String name, MatrixND mat);
    /**
     * Write active rule
     * @param step
     * @param name
     * @param params
     */
    abstract protected void writeActiveRule(TimeStep step, String name, String params);
    /**
     * return list of all result name available
     * @return
     */
    @Override
    abstract public List<String> getResultName();

    /**
     * sub class can overwrite this method to return specifique factory
     * @return
     */
    protected MatrixFactory getMatrixFactory() {
        return MatrixFactory.getInstance();
    }

    /**
     * Les ResultStorage ne doivent pas etre instancier directement, mais
     * recuperer a partir d'un
     * {@link fr.ifremer.isisfish.datastore.SimulationStorage#getResultStorage()}
     *
     * @param simulation storage to get result
     */
    public ResultStorageAbstract(SimulationStorage simulation) {
        this.simulation = simulation;
        this.decorator = new EntitySemanticsDecorator();
    }

    /**
     * Return some information on result storage. Information depend on
     * result storage type
     * @return
     */
    @Override
    public String getInfo() {
        return getClass().getSimpleName() + " No more information";
    }

    protected MatrixND decorate(MatrixND mat, TopiaContext tx) {
        MatrixND result = null;
        if (mat != null) {
            // on decore la matrice resultat au dernier moment, tous les calcules
            // ce font avec les strings
            EntitySemanticsDecorator.EntityProvider provider = getProvider(tx);
            result = new MatrixSemanticsDecorator(mat,
                    new EntitySemanticsDecorator(provider));
        }
        return result;
    }

    /**
     * Return undecorated version of matrix in argument
     * @param mat
     * @return new matrix undecorated
     */
    protected MatrixND undecorate(MatrixND mat) {
        MatrixND result = null;

        if (mat != null) {
            String name = mat.getName();
            String[] dimNames = mat.getDimensionNames();
            List[] sems = new List[mat.getDimCount()];
            for (int i=0,maxi=sems.length; i<maxi; i++) {
                sems[i] = undecorate(mat.getSemantic(i));
            }
            result = getMatrixFactory().create(name, sems, dimNames);
            result.paste(mat);
        }
        return result;
    }

    /**
     * return new list with undecorate item
     * @param l
     * @return new list
     */
    protected List<String> undecorate(List l) {
        List<String> result = new ArrayList<>(l.size());
        for (Object o : l) {
            result.add(undecorate(o));
        }
        return result;
    }

    protected String undecorate(Object o) {
        return decorator.undecorate(o);
    }

    @Override
    public void delete() {
        close();
    }

    @Override
    public void close() {
    }

    @Override
    public void addResult(TimeStep step, MatrixND mat) throws IsisFishException {
        addResult(false, step, mat.getName(), mat);
    }

    @Override
    public void addResult(TimeStep step, Population pop, MatrixND mat) throws IsisFishException {
        addResult(false, step, mat.getName(), pop, mat);
    }

    @Override
    public void addResult(boolean force, TimeStep step, MatrixND mat) throws IsisFishException {
        addResult(force, step, mat.getName(), mat);
    }

    @Override
    public void addResult(boolean force, TimeStep step, Population pop, MatrixND mat) throws IsisFishException {
        addResult(force, step, mat.getName(), pop, mat);
    }

    @Override
    public void addResult(TimeStep step, String name, Population pop, MatrixND mat) throws IsisFishException {
        addResult(false, step, name, pop, mat);
    }

    @Override
    public void addResult(TimeStep step, String name, MatrixND mat) throws IsisFishException {
        addResult(false, step, name, mat);
    }

    @Override
    public void addResult(boolean force, TimeStep step, String name, Population pop, MatrixND mat) throws IsisFishException {
        if (force || isEnabled(name)) {
            doAddResult(step, name + " " + pop, mat);
        }
    }

    @Override
    public void addResult(boolean force, TimeStep step, String name, MatrixND mat) throws IsisFishException {
        if (force || isEnabled(name)) {
            doAddResult(step, name, mat);
        }
    }

    protected void doAddResult(TimeStep step, String name, MatrixND mat) throws IsisFishException {
        // si la matrice n'a pas de semantique on refuse
        for (int i = 0; i < mat.getDimCount(); i++) {
            // la semantique n'est pas bonne des qu'il y a un null dedans
            if (mat.getSemantic(i).contains(null)) {
                throw new SimulationException(
                        "Erreur le résultat que vous souhaitez enregistrer n'a pas d'information convenable pour la dimension: "
                                + i + " " + mat.getDimensionName(i));
            }
        }

        try {
            writeResult(step, name, mat);
        } catch (Exception eee) {
            log.warn("Can't add result '" + name + "' at step " + step, eee);
        }
    }

    /**
     * Permet de savoir si lorsque l'on ajoutera ce resultat, il sera
     * sauvé ou non.
     * 
     * Check for result name returned by :
     * <ul>
     *  <li>{@link ExportInfo#getNecessaryResult()}</li>
     *  <li>{@link SensitivityExport#getNecessaryResult()}</li>
     *  <li>{@link Rule#getNecessaryResult()}</li>
     *  <li>{@link SimulationPlan#getNecessaryResult()}</li>
     * </ul>
     * 
     * @param name result name
     * @return {@code true} if result is enabled
     * @deprecated since 4.4.0.0 : this method is duplicated with ResultManager#isEnabled(String)
     */
    @Override
    public boolean isEnabled(String name) {
        name = name.trim();
        if (enabledResult == null && simulation.getSimulationParametersFile().exists()) {

            Set<String> requestedResult = ResultInfoHelper.cleanResultNames(simulation.getParameter().getResultEnabled());

            // test on export
            List<String> exportNames = simulation.getParameter().getExportNames();
            if (exportNames != null) {
                for (String exportName : exportNames) {
                    ExportStorage storage = ExportStorage.getExport(exportName);
                    try {
                        ExportInfo export = storage.getNewInstance();
                        Collections.addAll(requestedResult, export.getNecessaryResult());
                    } catch (IsisFishException eee) {
                        if (log.isWarnEnabled()) {
                            log.warn(t("isisfish.error.instanciate.export",
                                            exportName), eee);
                        }
                    }
                }
            }

            // test on sensitivity export
            List<SensitivityExport> sensitivityExports = simulation
                    .getParameter().getSensitivityExport();
            if (sensitivityExports != null) {
                for (SensitivityExport sensitivityExport : sensitivityExports) {
                    Collections.addAll(requestedResult, sensitivityExport.getNecessaryResult());
                }
            }

            // test on rules
            List<Rule> rules = simulation.getParameter().getRules();
            if (rules != null) {
                for (Rule rule : rules) {
                    Collections.addAll(requestedResult, rule.getNecessaryResult());
                }
            }

            // test on plans
            List<SimulationPlan> plans = simulation.getParameter().getSimulationPlans();
            if (plans != null) {
                for (SimulationPlan plan : plans) {
                    Collections.addAll(requestedResult, plan.getNecessaryResult());
                }
            }

            // on objective and optimization
            Objective objective = simulation.getParameter().getObjective();
            if (objective != null) {
                Collections.addAll(requestedResult, objective.getNecessaryResult());
            }

            Optimization optimization = simulation.getParameter().getOptimization();
            if (optimization != null) {
                Collections.addAll(requestedResult, optimization.getNecessaryResult());
            }

            // recursive result extraction
            enabledResult = ResultInfoHelper.extractAllNecessaryResults(requestedResult);

            log.info("Enabled result: " + enabledResult);
        }
        // par defaut on dit qu'on conserve le resultat
        boolean result = true;
        if (enabledResult != null) {
            result = enabledResult.contains(name);
        }
        return result;
    }


    @Override
    public void addActiveRule(TimeStep step, Rule rule) throws IsisFishException {
        String name = RuleStorage.getName(rule);
        String params = RuleStorage.getParamAsString(rule);
        writeActiveRule(step, name, params);
    }
    
    /**
     * Retourne la matrice stocke pour un pas de temps
     * @param step le pas de temps que l'on souhaite
     * @param pop la population pour lequelle on souhaite le resultat
     * @param name le nom des resultats dont on veut la matrice
     * @return La matrice demandée ou null si aucune matrice ne correspond a
     * la demande.
     */
    @Override
    public MatrixND getMatrix(TimeStep step, Population pop, String name) {
        String newName = name + " " + pop;
        return getMatrix(step, newName, null);
    }

    /**
     * Retourne la matrice stocke pour un pas de temps
     * @param step le pas de temps que l'on souhaite
     * @param pop la population pour lequelle on souhaite le resultat
     * @param name le nom des resultats dont on veut la matrice
     * @return La matrice demandée ou null si aucune matrice ne correspond a
     * la demande.
     */
    @Override
    public MatrixND getMatrix(TimeStep step, Population pop, String name, TopiaContext tx) {
        String newName = name + " " + pop;
        return getMatrix(step, newName, tx);
    }

    @Override
    public MatrixND getMatrix(TimeStep step, String name) {
        return getMatrix(step, name, null);
    }

    /**
     * Retourne une matrice contenant tous les pas de temps.
     *
     * @param name le nom des resultats dont on veut une matrice globale.
     * @return copy of result matrix
     */
    @Override
    public MatrixND getMatrix(TimeStep step, String name, TopiaContext tx) {
        MatrixND mat = readResult(step, name);
        if (mat != null) {
            mat = mat.copy();
        }

        mat = decorate(mat, tx);
        return mat;
    }

    /**
     * Retourne une matrice contenant tous les pas de temps.
     * @param pop la population pour lequel on souhaite la matrice
     * @param name le nom des resultats dont on veut une matrice globale.
     */
    @Override
    public MatrixND getMatrix(Population pop, String name) {
        String newName = name + " " + pop;
        return getMatrix(newName, null);
    }

    @Override
    public MatrixND getMatrix(Population pop, String name, TopiaContext tx) {
        String newName = name + " " + pop;
        return getMatrix(newName, tx);
    }

    /**
     * Retourne une matrice contenant tous les pas de temps.
     * 
     * @param name le nom des resultats dont on veut une matrice globale.
     */
    @Override
    public MatrixND getMatrix(String name) {
        return getMatrix(name, null);
    }

    /**
     * Retourne une matrice contenant tous les pas de temps.
     * @param name le nom des resultats dont on veut une matrice globale.
     * @return new Matrix
     */
    @Override
    public MatrixND getMatrix(String name, TopiaContext tx) {
        log.debug("Get result: " + name);

        // collect de tous les pas de temps possible
        TimeStep lastStep = getLastStep();
        List<TimeStep> steps = new ArrayList<>();
        TimeStep step = new TimeStep(0);
        steps.add(step);
        while (step.before(lastStep)) {
            step = step.next();
            steps.add(step);
        }

        MatrixND result = getMatrix(steps, name, tx);
        return result;
    }

    @Override
    public MatrixND getMatrix(List<TimeStep> steps, Population pop, String name) {
        String newName = name + " " + pop;
        return getMatrix(steps, newName, null);
    }

    @Override
    public MatrixND getMatrix(List<TimeStep> steps, Population pop, String name, TopiaContext tx) {
        String newName = name + " " + pop;
        return getMatrix(steps, newName, tx);
    }

    @Override
    public MatrixND getMatrix(List<TimeStep> steps, String name) {
        return getMatrix(steps, name, null);
    }

    @Override
    public MatrixND getMatrix(List<TimeStep> steps, String name, TopiaContext tx) {
        log.debug("Get result: " + name);
        
        // collect de toutes les matrices existantes
        Map<TimeStep, MatrixND> results = new LinkedHashMap<>();
        MatrixND sample = null;
        for (TimeStep s : steps) {
            MatrixND m = readResult(s, name);
            if (m != null) {
                results.put(s, m);
                sample = m;
            }
        }

        // s'il n'y pas de resultat, on retourne null
        if (sample == null) {
            return null;
        }

        // creation des dimensions
        String[] dimNames = new String[1 + sample.getDimCount()];
        dimNames[0] = t("isisfish.common.date");
        for (int i = 1; i < dimNames.length; i++) {
            dimNames[i] = sample.getDimensionName(i - 1);
        }

        // collect des semantics. +1 pour les dates
        List[] sem = new List[1 + sample.getDimCount()];
        sem[0] = undecorate(steps);

        for (int i = 1; i < sem.length; i++) {
            sem[i] = new HashList();
        }

        for (MatrixND m : results.values()) {
            if (m != null) {
                for (int s = 0; s < m.getDimCount(); s++) {
                    sem[s + 1].addAll(m.getSemantic(s));
                }
            }
        }

        // creation de la matrice resultat
        MatrixND resultMat = getMatrixFactory().create(name, sem, dimNames);


        // recuperation du resultat pour chaque date de la simulation, de Date(0) à lastDate
        for (Map.Entry<TimeStep, MatrixND> result : results.entrySet()) {
            TimeStep s = result.getKey();
            String stepString = undecorate(s);
            MatrixND m = result.getValue();

            // on recupere dans la matrice resultat l'endroit on il faut
            // mettre la matrice
            MatrixND submat = resultMat.getSubMatrix(0, stepString, 1);
            // on met les valeur de mat dans la sous matrice extraite
            for (MatrixIterator mi = m.iteratorNotZero(); mi.next();) {
                submat.setValue(
                        ArrayUtil.concat(new Object[] { stepString },
                                mi.getSemanticsCoordinates()), mi.getValue());
            }
        }

        resultMat = decorate(resultMat, tx);
        return resultMat;
    }

    /**
     * Get last simulation date.
     * 
     * @return last simulation date
     */
    @Override
    public TimeStep getLastStep() {
        int monthNumber = simulation.getParameter().getNumberOfMonths();
        TimeStep result = new TimeStep(monthNumber - 1); // -1 because date begin at 0
        return result;
    }

    @Override
    public void addResult(SimulationContext context, TimeStep step, String name,
            MatrixND mat) throws IsisFishException {
        doAddResult(step, name, mat);
    }

    @Override
    public MatrixND getMatrix(SimulationContext context, TimeStep step, String name) {
        MatrixND result = null;
        try {
            result = getMatrix(step, name, context.getDB());
        } catch (TopiaException eee) {
            if (log.isWarnEnabled()) {
                log.warn(String.format("Can't get result: %1$s", name), eee);
            }
        }
        return result;

    }

    public MatrixND getMatrix(SimulationContext context, List<TimeStep> steps, String name) {
        MatrixND result = null;
        try {
            result = getMatrix(steps, name, context.getDB());
        } catch (TopiaException eee) {
            if (log.isWarnEnabled()) {
                log.warn(String.format("Can't get result: %1$s", name), eee);
            }
        }
        return result;
    }

    @Override
    public MatrixND getMatrix(SimulationContext context, String name) {
        MatrixND result = null;
        try {
            result = getMatrix(name, context.getDB());
        } catch (TopiaException eee) {
            if (log.isWarnEnabled()) {
                log.warn(String.format("Can't get result: %1$s", name), eee);
            }
        }
        return result;
    }

    /**
     * Try to find better tx. If argument is not null, return it. otherwize
     * try to get tx in SimulationContext.
     *
     * @param tx
     * @return
     */
    protected EntitySemanticsDecorator.EntityProvider getProvider(TopiaContext tx) {
        // FIXME echatellier 20140811 : when reruninig again export outside of simulation
        // no simulation is registred into SimulationContext thread local, so this
        // method return no transaction

        if (tx == null) {
            // si on a pas de tx, on recherche si on est dans une simulation
            // pour recuperer la tx de la simulation
            if (simulation == SimulationContext.get().getSimulationStorage()) {
                try {
                    tx = SimulationContext.get().getDB();
                } catch (TopiaException eee) {
                    throw new IsisFishRuntimeException("Can't get database from SimulationContext", eee);
                }
            } else if (simulation == ExportContext.get().getSimulationStorage()) {
                try {
                    tx = ExportContext.get().getDB();
                } catch (TopiaException eee) {
                    throw new IsisFishRuntimeException("Can't get database from ExportContext", eee);
                }
            }
        }

        EntitySemanticsDecorator.EntityProvider result = null;
        if (tx != null) {
            result = new EntitySemanticsDecorator.EntityTxProvider(tx);
        }
        return result;
    }

    @Override
    public void afterSimulation(SimulationContext context) {
    }

    @Override
    public void beforeSimulation(SimulationContext context) {
    }

    @Override
    public void stepChange(SimulationContext context, TimeStep step) {
    }

    // public void addActivatedRule(ResultStorage self, Date date, RegleParam rule){
    //     List rules = (List)activatedRules.get(date);
    //     if(rules == null){
    //         activatedRules.put(date, rules = new LinkedList());
    //     }
    //     rules.add(rule);
    // }

    // /**
    // * Retourne pour une date données tous les RegleParam qui ont été activé
    // * a la date demandé.
    // * @return une list de {@link fr.ifremer.nodb.RegleParam}
    // */
    // public List getActivatedRule(ResultStorage self, Date date){
    //     List rules = (List)activatedRules.get(date);
    //     if(rules == null){
    //         activatedRules.put(date, rules = new LinkedList());
    //     }
    //     return rules;
    // }

} 
