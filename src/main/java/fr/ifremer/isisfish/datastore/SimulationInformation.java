/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2007 - 2018 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.datastore;

import static org.nuiton.i18n.I18n.t;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.StringUtil;

/**
 * Cette classe permet de conserver des informations sur le deroulement d'une
 * simulation. La plupart des informations sont automatiquement renseignees,
 * mais l'utilisateur peut lui aussi ajouter des informations avec la methode
 * {@link #addInformation(String)}.
 * 
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class SimulationInformation {

    /** Class logger. */
    private static Log log = LogFactory.getLog(SimulationInformation.class);

    private static final String START_SIMULATION = "simulationStart";
    private static final String END_SIMULATION = "simulationEnd";
    private static final String EXPORT_TIME = "exportTime";
    private static final String EXPORT_SIZE = "exportSize";
    private static final String EXPORT_EXCEPTION = "exportException";
    private static final String RULE_TIME = "ruleTime";
    private static final String RULE_TIME_INIT = RULE_TIME + ".init";
    private static final String RULE_TIME_PRE = RULE_TIME + ".pre";
    private static final String RULE_TIME_POST = RULE_TIME + ".post";
    private static final String OTHER_INFO = "otherInfo";
    private static final String STATISTIC = "statistic";
    private static final String COMPUTE_RESULT = "computeResult";
    private static final String OPTIMIZATION_USAGE = "optimizationUsage";
    private static final String SIMULATION_EXCEPTION = "exception";

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat(
            "yyyy.MM.dd HH:mm:ss");

    protected Properties info = new Properties();

    protected File file = null;

    /**
     * Constructor.
     * 
     * If file already exists, load his content into current instance.
     * 
     * @param file simulation information output file
     */
    public SimulationInformation(File file) {
        this.file = file;
        reloadLocal();
    }

    /**
     * Si les simulations ont été effectuée à distance, l'instance de SimulationInformation doit
     * se recharger avec les nouvelles informations à jour en fin de simulation.
     */
    public void reloadLocal() {
        if (file.exists()) {
            try (FileReader reader = new FileReader(file)){
                info.load(reader);
            } catch (IOException eee) {
                if (log.isWarnEnabled()) {
                    log.warn(t("isisfish.error.read.simulation", file.getPath()), eee);
                }
            }
        }
    }

    @Override
    public String toString() {
        String result = "Simulation Information:\n";

        // date start/end
        Date start = getSimulationStart();
        Date end = getSimulationEnd();

        if (start != null && end != null) {
            result += "Duration: " + StringUtil.convertTime(start.getTime() * 1000000, end.getTime() * 1000000)
                    + " Start: " + dateFormat.format(start)
                    + " End: " + dateFormat.format(end) + "\n";
        } else if (start != null) {
            result += " Start: " + dateFormat.format(start);
            result += " End: N/A\n";
        } else {
            result += " Start: N/A";
            result += " End: N/A\n";
        }

        result += "\n";

        // exports
        Map<String, Long> exportTime = getExportTimes();
        if (exportTime.size() > 0) {
            result += "Export time: " + StringUtil.convertTime(getAllExportTime()) + "\n";
            for (Map.Entry<String, Long> entry : exportTime.entrySet()) {
                result += "\t"
                        + entry.getKey()
                        + " : "
                        + StringUtil.convertTime(entry.getValue()) + "\n";
            }
        }

        // exports
        Map<String, Long> exportSize = getExportSizes();
        if (exportSize.size() > 0) {
            result += "Export size: " + StringUtil.convertMemory(getAllExportSize()) + "\n";
            for (Map.Entry<String, Long> entry : exportSize.entrySet()) {
                result += "\t"
                        + entry.getKey()
                        + " : "
                        + StringUtil.convertMemory(entry.getValue()) + "\n";
            }
        }

        // rules
        Set<String> ruleNames = getRuleNames();
        if (ruleNames.size() > 0) {
            result += "Rule time:\n";
            for (String ruleName : ruleNames) {
                
                String details = "";
                long total = 0;

                long time = getRuleInitTime(ruleName);
                if (time > 0) {
                    total += time;
                    details += "init:" + DurationFormatUtils.formatDuration(time, "s'.'S");
                }
                
                // can be 0 if condition always return false, never entrer pre/post
                time = getRulePreTime(ruleName);
                if (time > 0) {
                    if (total > 0) {
                        details += ", ";
                    }
                    total += time;
                    details += "pre:" + DurationFormatUtils.formatDuration(time, "s'.'S");
                }
                time = getRulePostTime(ruleName);
                if (time > 0) {
                    if (total > 0) {
                        details += ", ";
                    }
                    total += time;
                    details += "post:" + DurationFormatUtils.formatDuration(time, "s'.'S");
                }

                if (total > 0) {
                    result += "\t" + ruleName + " : " + DurationFormatUtils.formatDuration(total, "s'.'S");
                    result += "s (" + details + ")";
                    result += "\n";
                }
            }
        }

        // general information
        String info = getInformation();
        if (StringUtils.isNotBlank(info)) {
            result += "\nInformation:\n\t" + info + "\n";
        }

        // Statistic
        String v = getStatistic();
        if (v != null) {
            result += "\nStatistic:\n" + v + "\n";
        }

        // Optimisation usage
        v = getOptimizationUsage();
        if (v != null) {
            result += "Optimisation usage:\n" + v + "\n";
        }

        // Statistic
        v = getComputeResult();
        if (v != null) {
            result += "\nResult compute time:\n" + v + "\n";
        }

        // Exception
        v = getException();
        if (v != null) {
            result += "Simulation exception:\n" + v + "\n";
        }

        // exports Exception
        Map<String, String> exportException = getExportExceptions();
        if (exportException.size() > 0) {
            result += "Export exception:\n ";
            for (Map.Entry<String, String> entry : exportException.entrySet()) {
                result += entry.getKey()
                        + " : "
                        + entry.getValue() + "\n\n";
            }
        }


        return result;
    }

    protected void store() {
        try (FileWriter writer = new FileWriter(file)) {
            info.store(writer, "Simulation Information");
        } catch (IOException eee) {
            if (log.isWarnEnabled()) {
                log.warn(t("isisfish.error.write.simulation", file.getPath()), eee);
            }
        }
    }

    protected void setInfo(String key, String value) {
        info.setProperty(key, value);
        store();
    }

    /**
     * Get the date of simulation start.
     * 
     * @return simulation start date
     */
    public Date getSimulationStart() {
        String d = info.getProperty(START_SIMULATION);
        Date result = null;
        if (d != null) {
            try {
                result = dateFormat.parse(d);
            } catch (ParseException eee) {
                if (log.isWarnEnabled()) {
                    log.warn(t("isisfish.error.parse.date", d), eee);
                }
            }
        }
        return result;
    }

    public void setSimulationStart(Date date) {
        setInfo(START_SIMULATION, dateFormat.format(date));
    }

    /**
     * Get the date of simulation start.
     * 
     * @return simulation end date
     */
    public Date getSimulationEnd() {
        String d = info.getProperty(END_SIMULATION);
        Date result = null;
        if (d != null) {
            try {
                result = dateFormat.parse(d);
            } catch (ParseException eee) {
                if (log.isWarnEnabled()) {
                    log.warn(t("isisfish.error.parse.date", d), eee);
                }
            }
        }
        return result;
    }

    public void setSimulationEnd(Date date) {
        setInfo(END_SIMULATION, dateFormat.format(date));
    }

    public void addExportTime(String exportName, long time) {
        setInfo(EXPORT_TIME + "." + exportName, String.valueOf(time));
    }

    public void addExportSize(String exportName, long size) {
        setInfo(EXPORT_SIZE + "." + exportName, String.valueOf(size));
    }

    public void addExportException(String exportName, Throwable eee) {
        setInfo(EXPORT_EXCEPTION + "." + exportName, exceptionToString(eee));
    }

    public void addAllExportTime(long time) {
        setInfo(EXPORT_TIME, String.valueOf(time));
    }

    public void addAllExportSize(long size) {
        setInfo(EXPORT_SIZE, String.valueOf(size));
    }

    /**
     * Get all export time in map.
     * 
     * @return a map with all export time
     */
    protected Map<String, Long> getExportTimes() {
        Map<String, Long> result = new TreeMap<>();
        for (String key : info.stringPropertyNames()) {
            if (key.startsWith(EXPORT_TIME + ".")) {
                String exportName = key.substring(EXPORT_TIME.length() + 1);
                result.put(exportName, getExportTime(exportName));
            }
        }
        return result;
    }

    /**
     * Get all export size in map.
     *
     * @return a map with all export size
     */
    protected Map<String, Long> getExportSizes() {
        Map<String, Long> result = new TreeMap<>();
        for (String key : info.stringPropertyNames()) {
            if (key.startsWith(EXPORT_SIZE + ".")) {
                String exportName = key.substring(EXPORT_SIZE.length() + 1);
                result.put(exportName, getExportSize(exportName));
            }
        }
        return result;
    }

    /**
     * Get all export exception in map.
     *
     * @return a map with all export exception
     */
    protected Map<String, String> getExportExceptions() {
        Map<String, String> result = new TreeMap<>();
        for (String key : info.stringPropertyNames()) {
            if (key.startsWith(EXPORT_EXCEPTION + ".")) {
                String exportName = key.substring(EXPORT_EXCEPTION.length() + 1);
                result.put(exportName, info.getProperty(key));
            }
        }
        return result;
    }

    /**
     * Get all export time in map.
     * 
     * @return a map with all export time
     * @deprecated since 3.2.0.5, use {@link #getExportTimes()} instead
     */
    public Map<String, Long> getExportTime() {
        return getExportTimes();
    }

    protected long getLong(String prop) {
        String t = info.getProperty(prop);
        long result = 0;
        if (t != null) {
            try {
                result = Long.parseLong(t);
            } catch (NumberFormatException eee) {
                if (log.isWarnEnabled()) {
                    log.warn(t("isisfish.error.parse.long", t), eee);
                }
            }
        }
        return result;

    }

    public long getExportTime(String exportName) {
        String prop = EXPORT_TIME;
        if (StringUtils.isNotEmpty(exportName)) {
            prop += "." + exportName;
        }
        return getLong(prop);
    }

    public long getExportSize(String exportName) {
        String prop = EXPORT_SIZE;
        if (StringUtils.isNotEmpty(exportName)) {
            prop += "." + exportName;
        }
        return getLong(prop);
    }
    
    public long getAllExportTime() {
        return getExportTime(null);
    }

    public long getAllExportSize() {
        return getExportSize(null);
    }

    /**
     * Add rule time.
     * 
     * If a time already exists for ruleName, add time to previous time.
     * (usefull because pre/post action are called multiples time)
     * 
     * @param keyName (ie {@code #RULE_TIME_INIT}, {@code #RULE_TIME_PRE}, {@code #RULE_TIME_POST})
     * @param ruleName rule name
     * @param time time to add
     * 
     * @since 3.2.0.5
     */
    protected void addRuleTime(String keyName, String ruleName, long time) {

        // get previous time
        String previousTimeAsString = info
                .getProperty(keyName + "." + ruleName);
        long previousTime = 0;
        if (previousTimeAsString != null) {
            try {
                previousTime = Long.parseLong(previousTimeAsString);
            } catch (NumberFormatException eee) {
                if (log.isWarnEnabled()) {
                    log.warn(t("isisfish.error.parse.long",
                            previousTimeAsString), eee);
                }
            }
        }

        // add
        long newTime = previousTime + time;

        // set new time in
        setInfo(keyName + "." + ruleName, String.valueOf(newTime));
    }

    /**
     * Add rule init time.
     * 
     * If a time already exists for ruleName, add time to previous time.
     * 
     * @param ruleName rule name
     * @param time time to add
     * @since 3.2.0.5
     */
    public void addRuleInitTime(String ruleName, long time) {
        addRuleTime(RULE_TIME_INIT, ruleName, time);
    }

    /**
     * Add rule pre operation time time.
     * 
     * If a time already exists for ruleName, add time to previous time.
     * 
     * @param ruleName rule name
     * @param time time to add
     * @since 3.2.0.5
     */
    public void addRulePreTime(String ruleName, long time) {
        addRuleTime(RULE_TIME_PRE, ruleName, time);
    }

    /**
     * Add rule post operation time.
     * 
     * If a time already exists for ruleName, add time to previous time.
     * 
     * @param ruleName rule name
     * @param time time to add
     * @since 3.2.0.5
     */
    public void addRulePostTime(String ruleName, long time) {
        addRuleTime(RULE_TIME_POST, ruleName, time);
    }

    /**
     * Get rule init operation time.
     * 
     * @param ruleName rule name
     * @return time
     * @since 3.2.0.5
     */
    public long getRuleInitTime(String ruleName) {
        return getRuleTime(RULE_TIME_INIT, ruleName);
    }

    /**
     * Get rule pre operation time.
     * 
     * @param ruleName rule name
     * @return time
     * @since 3.2.0.5
     */
    public long getRulePreTime(String ruleName) {
        return getRuleTime(RULE_TIME_PRE, ruleName);
    }

    /**
     * Get rule post operation time.
     * 
     * @param ruleName rule name
     * @return time
     * @since 3.2.0.5
     */
    public long getRulePostTime(String ruleName) {
        return getRuleTime(RULE_TIME_POST, ruleName);
    }

    /**
     * Get rule operation time.
     * 
     * @param ruleName rule name
     * @since 3.2.0.5
     */
    protected long getRuleTime(String prefixName, String ruleName) {
        String t = info.getProperty(prefixName + "." + ruleName);
        long result = 0;
        if (t != null) {
            try {
                result = Long.parseLong(t);
            } catch (NumberFormatException eee) {
                if (log.isWarnEnabled()) {
                    log.warn(t("isisfish.error.parse.long", t), eee);
                }
            }
        }
        return result;
    }

    /**
     * Get rules names.
     * 
     * @return a map with all export time
     */
    protected Set<String> getRuleNames() {
        Set<String> result = new HashSet<>();
        for (String key : info.stringPropertyNames()) {
            if (key.startsWith(RULE_TIME + ".")) {
                // recupere le nom apres le deuxieme "."
                String ruleName = key.substring(key.indexOf('.', RULE_TIME.length() + 1) + 1);
                result.add(ruleName);
            }
        }
        return result;
    }

    public String getStatistic() {
        String result = info.getProperty(STATISTIC);
        return result;
    }

    public void setStatistic(String v) {
        setInfo(STATISTIC, v);
    }

    public String getComputeResult() {
        String result = info.getProperty(COMPUTE_RESULT);
        return result;
    }

    public void setComputeResult(String v) {
        setInfo(COMPUTE_RESULT, v);
    }

    public String getOptimizationUsage() {
        String result = info.getProperty(OPTIMIZATION_USAGE);
        return result;
    }

    public void setOptimizationUsage(String v) {
        setInfo(OPTIMIZATION_USAGE, v);
    }

    public String getException() {
        String result = info.getProperty(SIMULATION_EXCEPTION);
        return result;
    }

    public void setException(Throwable eee) {
        String v = exceptionToString(eee);
        setInfo(SIMULATION_EXCEPTION, v);
    }

    protected String exceptionToString(Throwable eee) {
        StringWriter w = new StringWriter();
        PrintWriter pw = new PrintWriter(w);
        eee.printStackTrace(pw);
        String v = w.getBuffer().toString();
        return v;
    }

    /**
     * Return {@code true} if an exception has been set.
     * 
     * @return {@code true} if there is an exception
     */
    public boolean hasError() {
        boolean result = getException() != null && getException().length() > 0;
        return result;
    }

    /**
     * @deprecated since 3.2.0.5, use {@link #getInformation()} instead
     * @return other information
     */
    public String getInfomation() {
        return getInformation();
    }

    /**
     * Get other information.
     * 
     * @return other information
     */
    protected String getInformation() {
        String result = info.getProperty(OTHER_INFO);
        if (result == null) {
            result = "";
        }
        return result;
    }

    /**
     * Add additional simulation information.
     * 
     * @param info new info
     */
    public void addInformation(String info) {
        setInfo(OTHER_INFO, getInformation() + info + "\n\n");
    }
}
