/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2005 - 2012 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.datastore;

import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.annotations.Doc;
import fr.ifremer.isisfish.simulator.SimulationPlan;
import fr.ifremer.isisfish.vcs.VCSException;
import org.apache.commons.collections4.map.ReferenceMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

import static org.nuiton.i18n.I18n.t;

/**
 * Class permettant de representer un plan de simulation.
 * Un plan de simulation est un fichier Java que l'on compile si besoin.
 * <p>
 * Gere les fichiers VCS de type {@link SimulationPlan} (package simulationplans)
 *
 * Created: 17 août 2005 11:11:51 CEST
 *
 * @author Benjamin POUSSIN : poussin@codelutin.com
 *
 * @version $Revision$
 *
 * Last update: $Date$ by : $Author$
 */
public class SimulationPlanStorage extends JavaSourceStorage { // SimulationPlanStorage

    /** to use log facility, just put in your code: log.info(\"...\"); */
    private static Log log = LogFactory.getLog(SimulationPlanStorage.class);
    
    public final static String SIMULATION_PLAN_PATH = "simulationplans";

    /** Template freemarker pour les plans de simulation . */
    public static final String SIMULATION_PLAN_TEMPLATE = "templates/script/simulationplan.ftl";

    /** Instance cache. */
    static protected Map<String, SimulationPlanStorage> plansCache = new ReferenceMap<>();

    protected static Set<StorageChangeListener> storageListeners = Collections.newSetFromMap(new WeakHashMap<>());

    public static void addStorageListener(StorageChangeListener storageListener) {
        storageListeners.add(storageListener);
    }

    /**
     * Construit un nouveau SimulationPlan storage.
     *
     * @param rootSrc   repertoire root de stockage des plans
     * @param directory le repertoire ou devrait se trouver le plan
     * @param name      le nom du plan
     */
    protected SimulationPlanStorage(File rootSrc, File directory, String name) {
        super(rootSrc, directory, name);
    }

    public static void registerWatcher() {
        registerWatcher(storageListeners, getSimulationPlanDirectory(), getCommunitySimulationPlanDirectory());
    }

    @Override
    public String getPackage() {
        return SIMULATION_PLAN_PATH;
    }

    /**
     * Get context (official VCS) simulation plan directory.
     * 
     * @return context simulation plan directory
     */
    static public File getSimulationPlanDirectory() {
        File result = new File(getContextDatabaseDirectory(), SIMULATION_PLAN_PATH);
        result.mkdirs();
        return result;
    }

    /**
     * Get community VCS simulation plan directory.
     * 
     * @return community simulation plan directory
     */
    public static File getCommunitySimulationPlanDirectory() {
        File result = new File(getCommunityDatabaseDirectory(), SIMULATION_PLAN_PATH);
        result.mkdirs();
        return result;
    }

    /**
     * Retourne le nom de toutes les plans existantes
     *
     * @return les noms de tous les plans existant en local
     */
    static public List<String> getSimulationPlanNames() {
        List<String> result = getStorageNames(getSimulationPlanDirectory());
        result.addAll(getStorageNames(getCommunitySimulationPlanDirectory()));
        return result;
    }

    /**
     * Retourne le storage pour le plan demandé.
     *
     * @param name le nom du plan souhaité
     * @param location location to open storage file
     * @return Le storage pour le plan
     */
    static public SimulationPlanStorage getSimulationPlan(String name, Location... location) {
        SimulationPlanStorage result = plansCache.get(name);
        if (result == null) {
            Location loc = nonEmptyLocation(location);
            for (File dir : loc.getDirectories()) {
                SimulationPlanStorage storage = new SimulationPlanStorage(dir, new File(dir, SIMULATION_PLAN_PATH), name);
                if (storage.getFile().isFile()) {
                    result = storage;
                    plansCache.put(name, result);
                }
            }
        }
        return result;
    }

    /**
     * Create new simulation plan.
     * 
     * @param name new simulation plan to create
     * @param location location to simulation plan storage file
     * @return new simulation plan storage
     */
    public static SimulationPlanStorage createSimulationPlan(String name, Location location) {
        File dir = location.getDirectories()[0];
        SimulationPlanStorage storage = new SimulationPlanStorage(dir, new File(dir, SIMULATION_PLAN_PATH), name);
        return storage;
    }

    static public void checkout() throws VCSException {
        checkout(IsisFish.config.getDatabaseDirectory(), SIMULATION_PLAN_PATH);
    }

    /**
     * Retourne la liste des noms de tous les plans disponibles en local qui
     * ne sont pas encore sur le serveur VCS.
     *
     * @return liste de noms de plans
     */
    public static List<String> getNewSimulationPlanNames() {
        List<String> result = getSimulationPlanNames();
        result.removeAll(getRemoteSimulationPlanNames());
        return result;
    }

    /**
     * Retourne la liste des noms de tous les plans disponibles sur le
     * serveur VCS.
     *
     * @return la liste des noms de tous les plans disponibles sur le serveur
     *         VCS. Si le serveur n'est pas disponible la liste retournée est
     *         vide.
     */
    public static List<String> getRemoteSimulationPlanNames() {
        File dir = getSimulationPlanDirectory();
        return getRemoteStorageNames(dir);
    }

    /**
     * Retourne la liste des noms de tous les plans disponibles sur le
     * serveur VCS qui ne sont pas encore en local
     *
     * @return liste de noms de regions
     * @throws VCSException
     */
    public static List<String> getNewRemoteSimulationPlanNames() throws VCSException {
        List<String> result = getRemoteSimulationPlanNames();
        result.removeAll(getSimulationPlanNames());
        return result;
    }

    /**
     * <b>Be ware this method require to instanciate a SimulationPlan, so
     * it would be better to call as often as possible.</b>
     *
     * @return the descript of the instanciate SimulationPlan
     * @see Doc
     */
    public String getDescription() {
        String result = null;
        try {
            SimulationPlan simulationPlan = getNewInstance();
            result = simulationPlan == null ? null : simulationPlan.getDescription();
        } catch (Exception e) {
            log.warn(t("isisfish.error.not.found.description", this));
        }
        return result;
    }

} // SimulationPlanStorage
