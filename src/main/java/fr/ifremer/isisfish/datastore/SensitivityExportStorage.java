/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2011 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.datastore;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

import org.apache.commons.collections4.map.ReferenceMap;

import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.export.SensitivityExport;
import fr.ifremer.isisfish.vcs.VCSException;

/**
 * Gestion des fichers VCS de type {@link SensitivityExport}
 * (appartenant au module exports).
 *
 * Created: 18 août 2005 15:07:36 CEST
 *
 * @author chatellier eric &lt;chatellier@codelutin.com&gt;
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class SensitivityExportStorage extends ExportStorage {

    public static final String SENSITIVITY_EXPORT_PATH = "sensitivityexports";

    /** Template freemarker pour les scripts d'export . */
    public static final String SENSITIVITY_EXPORT_TEMPLATE = "templates/script/sensitivityexport.ftl";

    /** Instance cache. */
    protected static Map<String, SensitivityExportStorage> sensitivityExportsCache = new ReferenceMap<>();

    protected static Set<StorageChangeListener> storageListeners = Collections.newSetFromMap(new WeakHashMap<>());

    public static void addStorageListener(StorageChangeListener storageListener) {
        storageListeners.add(storageListener);
    }

    public static void registerWatcher() {
        registerWatcher(storageListeners, getSensitivityExportDirectory(), getCommunitySensitivityExportDirectory());
    }

    /**
     * Constructeur.
     *
     * @param rootSrc   le repertoire root de stockage des exports
     * @param directory le repertoire de l'export
     * @param name      le nom de l'export
     */
    protected SensitivityExportStorage(File rootSrc, File directory, String name) {
        super(rootSrc, directory, name);
    }

    @Override
    public String getPackage() {
        return SENSITIVITY_EXPORT_PATH;
    }

    /**
     * Get context (official VCS) sensitivity export directory.
     * 
     * @return context sensitivity export directory
     */
    public static File getSensitivityExportDirectory() {
        File result = new File(getContextDatabaseDirectory(), SENSITIVITY_EXPORT_PATH);
        result.mkdirs();
        return result;
    }

    /**
     * Get community VCS sensitivity export directory.
     * 
     * @return community sensitivity export directory
     */
    public static File getCommunitySensitivityExportDirectory() {
        File result = new File(getCommunityDatabaseDirectory(), SENSITIVITY_EXPORT_PATH);
        result.mkdirs();
        return result;
    }

    /**
     * Retourne le storage pour l'export demandé.
     *
     * @param name le nom de la export souhaitée
     * @param location location to open storage file
     * @return Le storage pour l'export
     */
    public static SensitivityExportStorage getSensitivityExport(String name, Location... location) {
        SensitivityExportStorage result = sensitivityExportsCache.get(name);
        if (result == null) {
            Location loc = nonEmptyLocation(location);
            for (File dir : loc.getDirectories()) {
                SensitivityExportStorage storage = new SensitivityExportStorage(dir, new File(dir, SENSITIVITY_EXPORT_PATH), name);
                if (storage.getFile().isFile()) {
                    result = storage;
                    sensitivityExportsCache.put(name, result);
                }
            }
        }
        return result;
    }

    /**
     * Create new sensitivity export.
     * 
     * @param name new sensitivity export to create
     * @param location location to sensitivity export storage file
     * @return new sensitivity export storage
     */
    public static SensitivityExportStorage createSensitivityExport(String name, Location location) {
        File dir = location.getDirectories()[0];
        SensitivityExportStorage storage = new SensitivityExportStorage(dir, new File(dir, SENSITIVITY_EXPORT_PATH), name);
        return storage;
    }

    /**
     * Retourne la liste des noms de toutes les régions disponible en local.
     *
     * @return la liste des noms de toutes les régions disponible en local
     */
    public static List<String> getSensitivityExportNames() {
        List<String> result = getStorageNames(getSensitivityExportDirectory());
        result.addAll(getStorageNames(getCommunitySensitivityExportDirectory()));
        return result;
    }

    public static void checkout() throws VCSException {
        checkout(IsisFish.config.getDatabaseDirectory(), SENSITIVITY_EXPORT_PATH);
    }
}
