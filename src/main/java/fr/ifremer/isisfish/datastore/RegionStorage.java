/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2005 - 2012 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.datastore;

import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.IsisFishDAOHelper;
import fr.ifremer.isisfish.entities.FisheryRegion;
import fr.ifremer.isisfish.entities.FisheryRegionDAO;
import fr.ifremer.isisfish.entities.Result;
import fr.ifremer.isisfish.simulator.SimulationContext;
import fr.ifremer.isisfish.vcs.VCSException;
import org.apache.commons.collections4.map.ReferenceMap;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import static org.nuiton.i18n.I18n.t;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.util.ZipUtil;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

/**
 * Classe permettant de recuperer les {@link TopiaContext} des regions.
 *
 * Created: 17 août 2005 03:44:57 CEST
 *
 * @author Benjamin POUSSIN &lt;poussin@codelutin.com&gt;
 *
 * @version $Revision$
 *
 * Last update: $Date$ by : $Author$
 */
public class RegionStorage extends DataStorage { // RegionStorage

    public static final String REGION_PATH = "regions";
    public static final String MAP_DIRECTORY = "maps";
    
    /** to use log facility, just put in your code: log.info(\"...\"); */
    private static Log log = LogFactory.getLog(RegionStorage.class);

    /** Instance cache. */
    protected static Map<String, RegionStorage> regions = new ReferenceMap<>();

    protected static Set<StorageChangeListener> storageListeners = Collections.newSetFromMap(new WeakHashMap<>());

    static {
        registerWatcher(storageListeners, getRegionDirectory());
    }

    public static void addStorageListener(StorageChangeListener storageListener) {
        storageListeners.add(storageListener);
    }

    /** File to use to store next commit region comment. */
    protected File commentForNextCommitFile;

    /**
     * Constructor.
     * 
     * @param directory region directory
     * @param name region name
     */
    protected RegionStorage(File directory, String name) {
        super(directory, name);
        commentForNextCommitFile = new File(getDirectory(), "commentForNextCommit.txt");
    }

    public static File getRegionDirectory() {
        File result = getContextDatabaseDirectory();
        result = new File(result, REGION_PATH);
        result.mkdirs();
        return result;
    }

    public static File getRegionDirectory(String name) {
        File result = getRegionDirectory();
        result = new File(result, name);
        return result;
    }

    /**
     * @return Returns the commentForNextCommit.
     * @throws IOException if IO problem while reading file
     */
    public String getCommentForNextCommit() throws IOException {
        commentForNextCommitFile.createNewFile();
        String result;
        result = FileUtils.readFileToString(commentForNextCommitFile, StandardCharsets.UTF_8);
        return result;
    }

    /**
     * @param commentForNextCommit The commentForNextCommit to set.
     * @throws IOException if io problem while writing file
     */
    public void setCommentForNextCommit(String commentForNextCommit) throws IOException {
        commentForNextCommitFile.createNewFile();
        FileUtils.writeStringToFile(commentForNextCommitFile, commentForNextCommit, StandardCharsets.UTF_8);
    }

    /**
     * rename commentForNextCommitFile with current date at end
     * must be used in user application after commit
     */
    public void clearCommentForNextCommit() {
        String date = DateFormatUtils.ISO_8601_EXTENDED_DATETIME_FORMAT.format(new Date());
        File dest = new File(commentForNextCommitFile, date);
        commentForNextCommitFile.renameTo(dest);
    }

    @Override
    protected boolean isVersionnableFile(File file) {
        boolean result = super.isVersionnableFile(file);

        // do not commit file commentForNextCommitFile
        if (result) {
            // file can be named commentForNextCommit.txt/date
            result = !file.getAbsolutePath().startsWith(commentForNextCommitFile.getAbsolutePath());
        }
        return result;
    }

    /**
     * Add new comment in comment used for the next cvs commit. This comment is
     * prefixed by the current date.
     *
     * @param commentToAdd the comment to add
     * @throws IOException if IO problem while writing file
     */
    public void addCommentForNextCommit(String commentToAdd) throws IOException {
        String comment = getCommentForNextCommit();
        comment += MessageFormat.format("{0,date,long}: " + commentToAdd + "\n", new Date());
        setCommentForNextCommit(comment);
    }

    @Override
    public void rename(String toName) throws StorageException {
        try {
            regions.remove(getName());
            super.rename(toName);
            TopiaContext tx = getStorage().beginTransaction();
            FisheryRegion region = getFisheryRegion(tx);
            region.setName(toName);
            tx.commitTransaction();
            tx.closeContext();

            regions.put(toName, this);
        } catch (TopiaException eee) {
            throw new StorageException(t("isisfish.error.rename.region", toName), eee);
        }
    }

    public File getMapRepository() {
        File result = new File(getDirectory(), MAP_DIRECTORY);
        return result;
    }

    /**
     * Checkout not existing Region locally from server
     *
     * @param name name of region to retrieve
     * @throws VCSException if problem with vcs while checkout
     * @throws TopiaException if problem while restoring db
     * @return le storage après checkout
     */
    static public RegionStorage checkout(String name) throws VCSException, TopiaException {
        checkout(IsisFish.config.getDatabaseDirectory(), REGION_PATH + File.separator + name);
        RegionStorage region = getRegion(name);
        if (region != null) {
            File file = region.getDataBackupFile();
            if (file.exists()) {
                TopiaContext tx = region.getStorage().beginTransaction();
                tx.restore(file);
                tx.commitTransaction();
                tx.closeContext();
            }
        }
        return region;
    }
    
    /**
     * Recupere le TopiaContext d'une region.
     *
     * @param name le nom de la region a recuperer
     * @return Le TopiaContext contenant la region, si la region n'existait null
     *         est retourné
     */
    static public RegionStorage getRegion(String name) {
        RegionStorage result = regions.get(name);
        if (result == null) {
            // recherche du repertoire de la region en fonction de la config
            File directory;

            // in simulation context, region is not located in "regions" directory
            // FIXME remove this, hack, don't known what to used here :(
            if (SimulationContext.get().getScriptDirectory() != null) {
                directory = getContextDatabaseDirectory();
            }
            else {
                directory = getRegionDirectory(name);
            }
            
            if (log.isInfoEnabled()) {
                log.info("Try to open region " + name + "(" + directory + ")");
            }

            if (directory.exists()) {
                result = new RegionStorage(directory, name);
                regions.put(name, result);
            }
        }
        return result;
    }

    /**
     * Verifie si la region exists en local ou sur le serveur CVS.
     *
     * @param name le nom de la region
     * @return vrai si la region existe deja
     */
    static public boolean exists(String name) {
        boolean result = false;
        result = result || getRegionNames().contains(name);
        result = result || getRemoteRegionNames().contains(name);
        return result;
    }

    /**
     * Permet de creer une nouvelle region. Si le nom est deja utilisé en local
     * ou sur le serveur alors une exception est leve. Sinon le repertoire pour
     * accueillir la region est cree.
     *
     * @param name Le nom de la nouvelle region
     * @return La nouvelle region
     * @throws StorageException Si le nom est deja uitlisé
     */
    static public RegionStorage create(String name) throws StorageException {
        if (exists(name)) {
            throw new StorageException(
                    "Can't create region this region name exists: " + name);
        }
        File directory = getRegionDirectory(name);
        RegionStorage result = new RegionStorage(directory, name);

        try {
            TopiaContext context = result.getStorage().beginTransaction();
            context.createSchema();
            FisheryRegionDAO regionDAO = IsisFishDAOHelper
                    .getFisheryRegionDAO(context);
            regionDAO.create("name", name);
            context.commitTransaction();
            context.closeContext();
        } catch (TopiaException eee) {
            throw new StorageException("Can't create new Region", eee);
        }

        regions.put(name, result);
        
        return result;
    }

    @Override
    public void delete(boolean cvsDelete) throws StorageException {
        super.delete(cvsDelete);

        regions.remove(getName());
    }

    /**
     * Retourne la liste des noms de toutes les régions disponible en local.
     *
     * @return la liste des noms de toutes les régions disponible en local
     */
    static public List<String> getRegionNames() {
        File dir = getRegionDirectory();
        List<String> result;
        result = getStorageNames(dir);
        return result;
    }

    /**
     * Retourne la liste de toutes les régions disponible en local.
     *
     * @return la liste de toutes les régions disponible en local
     */
    static public List<RegionStorage> getRegions() {
        List<RegionStorage> result = new ArrayList<>();
        for (String name : getRegionNames()) {
            RegionStorage region = getRegion(name);
            result.add(region);
        }
        return result;
    }

    /**
     * Retourne la liste des noms de toutes les régions disponible en local qui
     * ne sont pas encore sur le serveur VCS.
     *
     * @return liste de noms de regions
     */
    static public List<String> getNewRegionNames() {
        List<String> result = getRegionNames();
        result.removeAll(getRemoteRegionNames());
        return result;
    }

    /**
     * Retourne la liste des noms de toutes les régions disponible sur le
     * serveur VCS.
     *
     * @return la liste des noms de toutes les régions disponible sur le serveur
     *         VCS. Si le serveur n'est pas disponible la liste retournée est
     *         vide.
     */
    static public List<String> getRemoteRegionNames() {
        File dir = getRegionDirectory();
        List<String> result;
        result = getRemoteStorageNames(dir);
        return result;
    }

    /**
     * Retourne la liste des noms de toutes les régions disponible sur le
     * serveur VCS qui ne sont pas encore en local.
     *
     * @return liste de noms de regions
     */
    static public List<String> getNewRemoteRegionNames() {
        List<String> result = getRemoteRegionNames();
        result.removeAll(getRegionNames());
        return result;
    }

    static public FisheryRegion getFisheryRegion(TopiaContext context)
            throws StorageException {
        FisheryRegion result;
        try {
            FisheryRegionDAO regionDAO = IsisFishDAOHelper
                    .getFisheryRegionDAO(context);
            List<FisheryRegion> regions = regionDAO.findAll();
            if (regions.size() != 1) {
                throw new StorageException("Invalide region database"
                        + " number of region must be 1 not " + regions.size());
            }
            result = regions.get(0);
        } catch (TopiaException eee) {
            throw new StorageException(
                    "Can't find FisheryRegion in this Region", eee);
        }
        return result;
    }

    /**
     * Import zipped region.
     *
     * @param file zipped region file
     * @return region storage or null
     * @throws IOException if IO problem while import zip
     * @throws RegionAlreadyExistException if problem while restoring db
     */
    public static RegionStorage importZip(File file) throws IOException, StorageException, RegionAlreadyExistException {
        RegionStorage result;
        result = importAndRenameZip(file, null);
        return result;
    }

    /**
     * Import zipped region.
     *
     * @param file zipped region file
     * @param newName new name for the imported simulation
     * @return region storage or null
     * @throws IOException if IO problem while import zip
     * @throws RegionAlreadyExistException if problem while restoring db
     */
    public static RegionStorage importAndRenameZip(File file, String newName) throws IOException, RegionAlreadyExistException, StorageException {

        String name = "";

        try {
            String renameFrom = null;
            String renameTo = null;
            if (newName != null) {
                renameFrom = "^.*?/(.*)$";
                renameTo = newName + "/$1";
            }
            String lastEntry = ZipUtil.uncompressAndRename(file, getRegionDirectory(), renameFrom, renameTo);
            name = lastEntry.substring(0, lastEntry.indexOf("/"));
            RegionStorage result = RegionStorage.getRegion(name);
            if (result != null) {
                File data = result.getDataBackupFile();
                if (data.exists()) {
                    TopiaContext tx = result.getStorage().beginTransaction();
                    tx.restore(data);
                    if (newName != null) {
                        FisheryRegion region = getFisheryRegion(tx);
                        region.setName(newName);
                    }

                    // delete results in new region database
                    // this can occure when create region from simulation
                    tx.execute("DELETE " + Result.class.getName());

                    tx.commitTransaction();
                    tx.closeContext();
                }
                
                // close storage to force migration in next opening
                result.closeStorage();
            }

            return result;
        } catch (TopiaException eee) {
            throw new RegionAlreadyExistException(t("isisfish.error.import.file", file), eee, name);
        }
    }

} // RegionStorage
