/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2012 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.datastore;

import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.config.IsisConfig;
import fr.ifremer.isisfish.vcs.VCS;
import fr.ifremer.isisfish.vcs.VCSException;
import fr.ifremer.isisfish.vcs.VCSNone;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.version.Version;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_DELETE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;
import static java.nio.file.StandardWatchEventKinds.OVERFLOW;
import static org.nuiton.i18n.I18n.t;

/**
 * Classe permettant de géré l'interaction avec le VCS.
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public abstract class VersionStorage {

    /** Logger for this class */
    private static final Log log = LogFactory.getLog(VersionStorage.class);

    /** VCS root directory. */
    protected File root = null;

    /** Versionned file to manage in VCS. */
    protected File file = null;

    protected static void registerWatcher(Collection<StorageChangeListener> storageListeners, File... directories) {
        try {
            WatchService watchService = FileSystems.getDefault().newWatchService();
            for (File dir : directories) {
                Path path = dir.toPath();
                path.register(watchService, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);
            }

            new Thread(() -> {
                for (;;) {

                    // wait for key to be signaled
                    WatchKey key;
                    try {
                        key = watchService.take();

                        // le thread permet d'attendre un peut que plusieurs element
                        // s'acumule un peu. Cela permet de ne faire qu'un event "dataChanged" au final
                        Thread.sleep( 50 );
                    } catch (InterruptedException x) {
                        return;
                    }

                    StorageChangeEvent.Type typeEvent = null;
                    for (WatchEvent<?> event: key.pollEvents()) {
                        WatchEvent.Kind<?> kind = event.kind();

                        // This key is registered only
                        // for ENTRY_CREATE events,
                        // but an OVERFLOW event can
                        // occur regardless if events
                        // are lost or discarded.
                        if (kind == OVERFLOW) {
                            continue;
                        }

                        //System.out.println(" Event kind:" + event.kind() + ". File affected: " + event.context() + ".");

                        if (kind == ENTRY_CREATE) {
                            typeEvent = StorageChangeEvent.Type.ADDED;
                        } else if (kind == ENTRY_MODIFY) {
                            typeEvent = StorageChangeEvent.Type.MODIFIED;
                        } else if (kind == ENTRY_DELETE) {
                            typeEvent = StorageChangeEvent.Type.DELETED;
                        }
                    }

                    // FIXME le typeEvent ne peut avoir qu'une valeur au final (peut etre problématique)
                    if (typeEvent != null) {
                        StorageChangeEvent storageEvent = new StorageChangeEvent(typeEvent);
                        for (StorageChangeListener storageListener : storageListeners) {
                            storageListener.dataChanged(storageEvent);
                        }
                    }

                    // Reset the key -- this step is critical if you want to
                    // receive further watch events.  If the key is no longer valid,
                    // the directory is inaccessible so exit the loop.
                    boolean valid = key.reset();
                    if (!valid) {
                        break;
                    }
                }
            }).start();
        } catch (IOException e) {
            throw new IsisFishRuntimeException("Can't register watcher", e);
        }
    }

    /**
     * Par exemple si on a /home/poussin/isis-database comme root
     * il faut que file soit un fichier ou  un sous fichiers dans root
     *
     * @param root le repertoire racine de mise en VCS (HOME VCS)
     * @param file le fichier a gérer.
     */
    protected VersionStorage(File root, File file) {
        this.root = root;
        this.file = file;
    }

    /**
     * Return context root directory to use for all data files depending on
     * context (simulation/no simulation).
     * 
     * In simulation context, must look for files in simulation directory
     * instead of isis database.
     */
    protected static File getContextDatabaseDirectory() {
        return IsisFish.config.getContextDatabaseDirectory();
    }

    /**
     * Return community database directory.
     * 
     * @return community database directory
     */
    protected static File getCommunityDatabaseDirectory() {
        return IsisFish.config.getCommunityDatabaseDirectory();
    }

    /**
     * Get VCS instance for storage file.
     * 
     * @return vcs instance
     */
    protected VCS getCurrentVCS() {
        return getCurrentVCS(getFile());
    }

    /**
     * Get VCS instance for given file.
     * 
     * @param file file to get VCS
     * @return 
     */
    protected VCS getCurrentVCS(File file) {
        VCS result;
        String absolutePath = file.getAbsolutePath();
        if (absolutePath.startsWith(IsisFish.config.getDatabaseDirectory() + File.separator)) {
            result = IsisFish.vcs;
        } else if (absolutePath.startsWith(IsisFish.config.getCommunityDatabaseDirectory() + File.separator)) {
            result = IsisFish.communityVcs;
        } else {
            result = new VCSNone(file.getParentFile(), "", "", "", null, "", "");
        }

        if (log.isDebugEnabled()) {
            log.debug("VCS for file " + getFile() + " is " + result);
        }
        return result;
    }

    /**
     * Get {@link VCS}.
     * 
     * @return VCS
     */
    protected static VCS getVCS() {
        return IsisFish.vcs;
    }
    
    /**
     * Get community {@link VCS}.
     * 
     * @return community VCS
     */
    protected static VCS getCommunityVCS() {
        return IsisFish.communityVcs;
    }

    /**
     * Permet de demander la preparation des fichiers pour etre envoyé vers le VCS.
     */
    protected abstract void prepare();

    /**
     * Get VCS root directory.
     * 
     * @return the root.
     */
    public File getRoot() {
        return this.root;
    }

    /**
     * Get file.
     * 
     * @return the file.
     */
    public File getFile() {
        return this.file;
    }

    /**
     * Indique s'il a deja ete ajouté au VCS. Si file est un repertoire
     * (ex pour Region) alors vrai meme si tous les fichiers du repertoire
     * ne sont pas sur le VCSNone
     *
     * @return {@code true} si deja dans le VCS
     * @throws VCSException
     */
    public boolean isOnRemote() throws VCSException {
        return getVCS().isOnRemote(getFile());
    }

    /**
     * Permet de savoir si un fichier doit etre géré par le vcs ou non.
     * L'implantation par defaut exclus les répertoires VCS, il faut
     * toujours appeler le super si on surcharge la methode.
     *
     * @param file le fichier a tester
     * @return {@code true} si le fichier est versionné
     */
    protected boolean isVersionnableFile(File file) {

        // Dans le cas ou on essaye de savoir si file
        // est versionnable dans le storage courant
        // mais le VCS est toujours celui d'ISIs en static,
        // si le storage est alleur que ce VCS
        // cela ne fonctionne pas.

        // FIXME il ne faurdrait pas que le VCS soit
        // statique , mais contextuel au this.file
        // du storage

        boolean result = true;
        
        // Ca fait une erreur de fichier non versionne
        // si on essaye de l'appeler sur le working directory
        // dans ce cas, on retourne la valeur par defaut "true"
        if (!getVCS().getLocalRepository().equals(this.file)) {
            result = getVCS().isVersionnableFile(file);
        }
        return result;
    }

    /**
     * Donne la liste de tous les fichiers que le VCS doit gérer.
     * Par defaut parcours tous les repertoires et ajouter tous les fichiers
     * et repertoire trouvé. Il est possible d'exclure des fichiers et/ou
     * repertoire en surchargeant {@link #isVersionnableFile(File)}
     *
     * @param current le fichier
     * @param result la liste des fichiers à traiter
     * @return La liste des fichiers a géré par le CVS pour un add, remove
     *         update
     */
    protected List<File> getFiles(File current, List<File> result) {
        if (isVersionnableFile(current)) {
            result.add(current);
            if (current.exists() && current.isDirectory()) {
                for (File child : current.listFiles()) {
                    getFiles(child, result);
                }
            }
        }
        return result;
    }

    /**
     * Donne la liste de tous les fichiers à gérer par le VCS.
     * Ceci inclu la liste des répertoires pour aller de ce storage
     * jusqu'a la racine VCSNone si withParent est vrai
     *
     * @param withParent si vrai inclu les parents
     * @return la liste des fichiers trouvés
     */
    protected List<File> getFiles(boolean withParent) {
        List<File> result = new ArrayList<>();

        // on ajoute tous les directories jusqu'a root
        File current = getFile();
        while (withParent && current != null && !current.equals(getRoot())) {
            current = current.getParentFile();
            if (current != null) {
                // insere les parents avant tous les autres
                result.add(0, current);
            }
        }
        getFiles(getFile(), result);
        return result;
    }

    /**
     * Permet d'ajouter ce storage dans le VCSNone. Cela prend effet immediatement
     * (un commit est fait).
     *
     * @param msg le message indiquant le but du fichier
     * @throws VCSException si pb pendant l'op
     *
     */
    public void add(String msg) throws VCSException {
        prepare();

        // parent folder must be added too
        List<File> files = getFiles(true);

        // we can sure remove module directory and root directory
        files.remove(IsisFish.config.getDatabaseDirectory());

        if (log.isDebugEnabled()) {
            log.debug("files to add: " + files);
        }
        getCurrentVCS().add(files, msg);
    }

    /**
     * Permet de supprimer un fichier ou répertoire versionné ou non.
     *
     * @param vcsDelete si vrai alors le fichier sera aussi supprimé sur le
     *                  vcs si elle existait. Cela prend effet immediatement (un commit est fait)
     * @throws StorageException if delete operation fail
     */
    public void delete(boolean vcsDelete) throws StorageException {
        if (vcsDelete) {
            
            // parent folder must not be deleted
            List<File> files = getFiles(false);

            if (log.isDebugEnabled()) {
                log.debug("About to delete : " + files);
            }

            try {
                getCurrentVCS().delete(files, t("isisfish.versionStorage.removed"));
            } catch (VCSException eee) {
                throw new StorageException(
                        t("isisfish.error.delete.vcs.files"), eee);
            }
        }
        //TODO There is a bug to fix ? some files are not deleted!
        // due to h2, storage need to be closed before deletion
        if (getFile().isDirectory()) {
            FileUtils.deleteQuietly(getFile());
        } else {
            getFile().delete();
        }
    }

    /**
     * Permet d'envoyer des modifications faite en local sur le VCS.
     *
     * @param msg le message indiquant le type des modifications
     * @throws VCSException si pb pendant l'op
     */
    public void commit(String msg) throws VCSException {
        // on appelle en fait add, car il y a peut-etre des nouveaux fichiers
        // a ajouter au VCS et ca doit etre fait automatiquement
        add(msg);
    }

    /**
     * Permet de mettre a jour le fichier local en fonction de ce qu'il y
     * a sur le VCS
     *
     * @throws VCSException si pb pendant l'op
     */
    public void update() throws VCSException {
        prepare();
        getCurrentVCS().update(getFile(), true);
    }

    public boolean isUpToDate() throws VCSException {
        prepare();
        return getCurrentVCS().isUpToDate(getFile());
    }

    /**
     * Return diff between current file content and VCS file content.
     * 
     * @return diff as string
     * @throws VCSException
     * @throws IOException
     */
    public String diff() throws VCSException, IOException {
        return getCurrentVCS().getDiff(getFile());
    }

    /**
     * Permet de ramener tout un répertoire du VCS. Utile seulement pour le
     * premier lancement pour scipts et exports.
     *
     * @param destDir le repertoire parent
     * @param module le repertoire qui peut etre scripts ou exports
     * @throws VCSException si pb pendant l'opération
     */
    public static void checkout(File destDir, String module)
            throws VCSException {
        // Si on utilise pas le bon tag on change de tag
        Version tag = IsisConfig.getApiVersion();
        if (!getVCS().isTag(tag)) {
            // pas de tag pour cette version, on checkout le trunk
            tag = null;
        }

        File file = new File(destDir, module);
        getVCS().update(file, true);
    }

    /**
     * Retourne la liste des noms de toutes les storages disponible en local
     *
     * @param directory le répertoire dans lequel vie l'ensemble des storage
     * @return la liste des noms de toutes les storages disponible en local
     */
    public static List<String> getStorageNames(File directory) {
        List<String> result = new ArrayList<>();

        if (directory.exists()) {
            for (File f : directory.listFiles()) {
                if (getVCS().isVersionnableFile(f)) {
                    result.add(f.getName());
                }
                else if (getCommunityVCS().isVersionnableFile(f)) {
                    result.add(f.getName());
                }
            }
        }
        Collections.sort(result);

        return result;
    }

    /**
     * Retourne la liste des noms de tous les storages disponibles sur le
     * serveur VCSNone
     *
     * @param directory le répertoire sur le VCSNone ou doivent se trouver
     *                  les storages (regions, simulations)
     * @return la liste des noms de tous les storages disponibles sur le
     *         serveur VCSNone. Si le serveur n'est pas disponible la liste retournée
     *         est vide.
     */
    public static List<String> getRemoteStorageNames(File directory) {
        List<String> result;
        try {
            result = getVCS().getFileList(directory);
        } catch (VCSException e) {
            if (log.isWarnEnabled()) {
                log.warn("Error during connection to VCS server", e);
            }
            result = new ArrayList<>();
        }
        Collections.sort(result);
        return result;
    }
}
