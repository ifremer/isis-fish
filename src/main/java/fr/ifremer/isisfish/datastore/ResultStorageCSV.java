package fr.ifremer.isisfish.datastore;

/*
 * #%L
 * IsisFish
 * %%
 * Copyright (C) 1999 - 2018 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.ifremer.isisfish.config.IsisConfig;
import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.simulator.SimulationContext;
import fr.ifremer.isisfish.simulator.SimulationControl;
import fr.ifremer.isisfish.types.TimeStep;
import fr.ifremer.isisfish.util.matrix.MatrixCSVHelper;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.output.CountingOutputStream;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.util.StringUtil;

/**
 * Ecrit les resultats dans des fichiers au format pseudo csv compresse (gz)
 * Seules les valeurs differentes de 0 sont ecrites dans le fichier.
 *
 * Il est possible de modifier via la configuration:
 * - le nombre de TimeStep en cache
 * - de definir le nombre de TimeStep a conserver sur disque
 * 
 * 
 * Format d'une matrice:
 * <pre>
 * # commentaire
 * [nom]
 * [nom dimension1]:[semantique1];[semantique2];...
 * [nom dimension2]:[semantique1];[semantique2];...
 * ...
 * [ligne blanche]
 * [coordonnee1];[coordonnee2];...;[valeur]
 * [coordonnee1];[coordonnee2];...;[valeur]
 * [coordonnee1];[coordonnee2];...;[valeur]
 * ...
 * </pre>
 * 
 * Exemple
 * <pre>
 * MaMatrice
 * Mois:Janvier;Fevrier;Mars
 * Ville:Nantes;Paris;Nice
 * 
 * 1;1;2;13.5
 * 0;2;1;4.2
 * </pre>
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class ResultStorageCSV extends ResultStorageAbstract {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(ResultStorageCSV.class);

    /** number of step result to store on disk. 0 for none, negative for all result */
    protected int storeOnDiskStep = -1;
    /** number of step in cache */
    protected int cacheStep = 13; // default to 13 months

    /** Le pas de temps a partir duquel il faut sauver les resultats sur disque*/
    protected TimeStep startDiskStep = null;

    /** use to prevent to much call */
    private File directoryCache;

    /** nombre total de resultat */
    protected int numberOfResult = 0;
    /** nombre de resultat reellement stocke sur disque */
    protected int numberOfResultOnDisk = 0;
    /** le nombre de resultat demande */
    protected int numberOfResultAsked = 0;
    /** le nombre de resultat lu dans le cache */
    protected int numberOfResultReadInCache = 0;
    /** le nombre de resultat lu sur le disque */
    protected int numberOfResultReadOnDisk = 0;
    /** le nombre d'octet ecris sur le disque */
    protected long bytesWritten = 0;
    /**
     * le plus vieux pas de temps demande par rapport au pas de temps courant
     * lu sur le disque
     */
    protected int maxGapWithCurrentStepReadOnDisk = 0;

    protected MatrixCSVHelper matrixCSVHelper;

    LinkedHashMap<TimeStep, Map<String, MatrixND>> cache =
            new LinkedHashMap<TimeStep, Map<String, MatrixND>>() {
        private static final long serialVersionUID = 1L;
        @Override
        protected boolean removeEldestEntry(Map.Entry<TimeStep, Map<String, MatrixND>> eldest) {
            return size() > cacheStep;
        }
    };

    public ResultStorageCSV(SimulationStorage simulation) {
        super(simulation);

        storeOnDiskStep = IsisFish.config.getSimulationStoreResultOnDisk();
        cacheStep = IsisFish.config.getSimulationStoreResultCacheStep();

    }

    protected MatrixCSVHelper getMatrixCSVHelper() {
        if (matrixCSVHelper == null) {
            matrixCSVHelper = new MatrixCSVHelper(decorator);
        }
        return matrixCSVHelper;
    }


    @Override
    public String getInfo() {
        String oldest = "";
        if (maxGapWithCurrentStepReadOnDisk > 0) {
            oldest = String.format("\tthe oldest step read on disk was %s step before current step\n",
                    maxGapWithCurrentStepReadOnDisk);
        }

        return String.format(
                "ResultStorageCSV:\n"
                        + "\t%s results written to disk on a total of %s (for %s)\n"
                        + "\t%s results read (%s in cache and %s on disk)\n"
                        + "%s",
                numberOfResultOnDisk, numberOfResult, StringUtil.convertMemory(bytesWritten),
                numberOfResultAsked, numberOfResultReadInCache, numberOfResultReadOnDisk,
                oldest);
    }

    /**
     * donne le repertoire de stockage des resultats
     * @return
     */
    protected File getDirectory() {
        if (directoryCache == null) {
            directoryCache = SimulationStorage.getResultDirectory(simulation.getDirectory());
        }
        return directoryCache;
    }

    /**
     * Donne le repertoire de stockage pour un certain type de resultat matriciel
     * @param name
     * @return
     */
    protected File getMatrixDirectory(String name) {
        File file = new File(getDirectory(), "matrix" + File.separator + name);
        return file;
    }

    protected File getMatrixFile(TimeStep step, String name) {
        File file = new File(getMatrixDirectory(name), step.getStep() + "-" + name + ".csv" + IsisConfig.COMPRESSION_EXTENSION);
        return file;
    }

    protected HashSet<String> fileExistCache;
    protected HashSet<String> getFileExistCache() {
        if (fileExistCache == null) {
            fileExistCache = new HashSet<>();
            File matrixDir = getMatrixDirectory("");
            File[] resultDirs = matrixDir.listFiles();
            if (resultDirs != null) {
                for (File resultDir : resultDirs) {
                    
                    Collections.addAll(fileExistCache, resultDir.list());
                }
            }
        }
        return fileExistCache;
    }
    /**
     * Method used to know if file result exist. Usage of File.exists take to long time
     * during simulation
     * @param f
     * @return
     */
    protected boolean fileExist(File f) {
        return getFileExistCache().contains(f.getName());
    }
    protected void addFileExist(File f) {
        getFileExistCache().add(f.getName());
    }

    protected MatrixND readMatrix(String file) throws IOException {
        MatrixND result;
        try (Reader in = new InputStreamReader(new GZIPInputStream(new FileInputStream(file)), StandardCharsets.UTF_8)) {
            result = getMatrixCSVHelper().readMatrix(in);

        }
        return result;
    }

    /**
     * Extract and create TimeStep from file name. If file name doesn't contains
     * TimeStep null is returned
     * @param file
     * @return 
     */
    protected TimeStep getTimeStep(String file) {
        TimeStep result = null;
        try {
            String num = StringUtils.substringBefore(file, "-");
            int step = Integer.parseInt(num);
            result = new TimeStep(step);
        } catch(NumberFormatException eee) {
            log.error("File doesn't contains step information: " + file, eee);
        }
        return result;
    }

    @Override
    public void delete() {
        super.delete();
        try {
            File file = getDirectory();
            FileUtils.deleteDirectory(file);
            fileExistCache = null;
        } catch (IOException eee) {
            throw new IsisFishRuntimeException("Can't delete results", eee);
        }
    }

    @Override
    protected MatrixND readResult(TimeStep step, String name) {
        // un peu de statistique (seulement en simulation
        numberOfResultAsked++;

        MatrixND result = null;

        // try to get in cache first
        if(cache.containsKey(step)) {
            result = getCacheForStep(step).get(name);
        }

        // if not found looking for result on disk
        if (result != null) {
            numberOfResultReadInCache++;
        } else {
            File file = getMatrixFile(step, name);
            if (fileExist(file)) {
                try {
                    result = readMatrix(file.getPath());

                    // un peu de statistique (seulement en simulation
                    numberOfResultReadOnDisk++;
                    SimulationControl sc = SimulationContext.get().getSimulationControl();
                    if (sc != null) {
                        TimeStep currentStep = sc.getStep();
                        if (!step.equals(currentStep)) {
                            maxGapWithCurrentStepReadOnDisk = Math.max(maxGapWithCurrentStepReadOnDisk, currentStep.gap(step));
                        }
                    }
                } catch (IOException eee) {
                    log.error("Can't read result file: " + file, eee);
                }
            }
        }
        return result;
    }

    /**
     * Indique s'il faut sauver sur disque les resultats
     * @param step
     * @return
     */
    protected boolean isDiskResult(TimeStep step) {
        if (startDiskStep == null) {
            startDiskStep = getLastStep().minus(storeOnDiskStep);
        }
        return storeOnDiskStep < 0 || startDiskStep.before(step);
    }

    protected Map<String, MatrixND> getCacheForStep(TimeStep step) {
        Map<String, MatrixND> result = cache.computeIfAbsent(step, k -> new HashMap<>());
        return result;
    }


    @Override
    protected void writeResult(TimeStep step, String name, MatrixND mat) {
        // copy and undecorate matrix
        mat = undecorate(mat);
        
        // add mat in cache
        getCacheForStep(step).put(name, mat);

        // for statistics
        numberOfResult++;

        // write mat on disk
        if (isDiskResult(step)) {
            // for statistics
            numberOfResultOnDisk++;
            
            File file = getMatrixFile(step, name);
            file.getParentFile().mkdirs();
            CountingOutputStream counter = null;
            try (Writer out = new OutputStreamWriter(
                    new GZIPOutputStream(counter = new CountingOutputStream(new FileOutputStream(file))), StandardCharsets.UTF_8)) {
                getMatrixCSVHelper().writeMatrix(out, name, mat);

                // result file is writen without error, add it to fileExistCache
                addFileExist(file);
            } catch (Exception eee) {
                throw new IsisFishRuntimeException("Can't write result: " + file, eee);
            } finally {
                if (counter != null) {
                    this.bytesWritten += counter.getByteCount();
                }
            }
        }
    }

    @Override
    protected void writeActiveRule(TimeStep step, String name, String params) {
        // TODO
    }

    @Override
    public List<String> getResultName() {
        File dir = getMatrixDirectory("");
        String[] files = dir.list();
        List<String> result;
        if (files != null) {
            Arrays.sort(files);
            result = Arrays.asList(files);
        } else {
            result = Collections.EMPTY_LIST;
        }
        return result;
    }

    public long getBytesWritten() {
        return bytesWritten;
    }

}
