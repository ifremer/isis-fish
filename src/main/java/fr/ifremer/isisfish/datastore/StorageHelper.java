/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.datastore;

import fr.ifremer.isisfish.IsisFishException;
import fr.ifremer.isisfish.util.converter.ConverterUtil;
import org.apache.commons.beanutils.ConvertUtilsBean;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.persistence.TopiaEntity;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.Properties;

import static fr.ifremer.isisfish.simulator.SimulationParameterPropertiesHelper.DOT;
import static fr.ifremer.isisfish.simulator.SimulationParameterPropertiesHelper.PARAMETER_KEY;

/**
 * Helper used to populate and extract instance parameters
 * from simulation.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class StorageHelper {

    /** Class logger. */
    private static final Log log = LogFactory.getLog(StorageHelper.class);

    /**
     * Recupere dans prop les valeurs des champs specifique au export et met a
     * jour les champs de l'export.
     * 
     * @param instanceIndex l'index de l'instance
     * @param instance l'instance a mettre à jour
     * @param context le topia context dont on a besoin
     * @param props les proprietes contenant les parametre de l'export
     * @param propPrefix prefix des clés a prendre en compte dans {@code props}
     */
    public static void populateStorageParams(int instanceIndex, TopiaContext context,
            Object instance, Properties props, String propPrefix) {
        //ConvertUtilsBean beanUtils = ConverterUtil.getConverter(region
        //        .getStorage());
        String exportName = JavaSourceStorage.getName(instance);
        String paramTag = propPrefix + "." + instanceIndex + DOT + PARAMETER_KEY + DOT;

        for (Map.Entry<String, Field> entry : JavaSourceStorage.getParameterNamesAndField(instance).entrySet()) {
            String propName = entry.getKey();
            Field field = entry.getValue();
            Class<?> type = field.getType();
            if (TopiaEntity.class.isAssignableFrom(type)) {
                type = TopiaEntity.class;
            }
            String valueString = "not initialized";
            try {
                valueString = props.getProperty(paramTag + propName);

                // ATTENTION il semblerait que notre intance de beanUtils
                // soit ecrasé par celle du MatrixType ...
                // a ne pas deplacer avant la boucle
                ConvertUtilsBean beanUtils = ConverterUtil.getConverter(context);

                Object value = beanUtils.convert(valueString, type);
                if (log.isDebugEnabled()) {
                    log.debug("Set instance param: " + paramTag +
                            propName + " = " + value + "(" + valueString + ")");
                }

                JavaSourceStorage.setParameterValue(instance, propName, value);
            } catch (Exception eee) {
                if (log.isWarnEnabled()) {
                    log.info("Properties: " + props);
                    log.warn("Can't reload field " + propName + " for export "
                            + exportName + " with value " + valueString, eee);
                }
            }
        }
    }
    
    /**
     * Permet de mettre les parametres d'un object Isis sous une forme String pour
     * pouvoir les relire ensuite.
     *
     * @param instanceIndex l'index de la rule
     * @param context le context
     * @param instance La regle dont on souhaite mettre les parametres dans l'objet
     *        Properties retourne
     * @param propPrefix prefix des clés a prendre en compte dans {@code props}
     * @return L'objet Properties contenant les valeurs des parametres de l'instance
     */
    public static Properties getParamsAsProperties(int instanceIndex, TopiaContext context,
            Object instance, String propPrefix) {
        Properties result = new Properties();
        ConvertUtilsBean beanUtils = ConverterUtil.getConverter(context);
        for (String paramName : RuleStorage.getParameterNamesAndField(instance).keySet()) {
            String paramValueString;
            try {
                Object value = RuleStorage.getParameterValue(instance, paramName);
                paramValueString = beanUtils.convert(value);
                if (paramValueString != null) {
                    result.setProperty(propPrefix + DOT + instanceIndex
                             + DOT + PARAMETER_KEY + DOT + paramName, paramValueString);
                }
            } catch (IsisFishException eee) {
                if (log.isWarnEnabled()) {
                    log.warn("Can't convert parameter value to String: " + paramName, eee);
                }
            }
        }

        return result;
    }
}
