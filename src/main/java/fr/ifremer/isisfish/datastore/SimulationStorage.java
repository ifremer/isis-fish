/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2005 - 2022 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.datastore;

import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.IsisFishDAOHelper;
import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.entities.FisheryRegion;
import fr.ifremer.isisfish.entities.FisheryRegionDAO;
import fr.ifremer.isisfish.simulator.SimulationControl;
import fr.ifremer.isisfish.simulator.SimulationParameter;
import fr.ifremer.isisfish.simulator.SimulationParameterImpl;
import fr.ifremer.isisfish.util.IsisFileUtil;
import fr.ifremer.isisfish.vcs.VCSException;
import org.apache.commons.collections4.map.ReferenceMap;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RegExUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.util.ZipUtil;
import org.nuiton.version.Versions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import static org.nuiton.i18n.I18n.t;

/**
 * Class permettant la gestion de la persistance d'une simulation.
 *
 * Created: 17 août 2005 03:48:50 CEST
 *
 * @author Benjamin POUSSIN : poussin@codelutin.com
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class SimulationStorage extends DataStorage { // SimulationStorage

    public static final String SIMULATION_PATH = "simulations";
    public static final String INFORMATION_FILENAME = "information";
    public static final String OBJECTIVE_FILENAME = "objective";
    public static final String CONTROL_FILENAME = "control";
    public static final String PARAMETERS_FILENAME = "parameters.properties";
    public static final String RESULT_XML_FILENAME = "isis-mexico-output.xml";
    public static final String RESULT_FILENAME = "results.mapped";
    public static final String RESULT_DIRECTORY = "results";
    public static final String RESULT_EXPORT_DIRECTORY = "resultExports";
    public static final String DESIGN_PLAN_FILENAME = "isis-mexico-designplan.xml";
    public static final String SENSITIVITY_RESULTS = "sensitivityresults";

    /** to use log facility, just put in your code: log.info(\"...\"); */
    private static Log log = LogFactory.getLog(SimulationStorage.class);

    /** Cache des simulation storage. */
    protected static Map<String, SimulationStorage> simulations = new ReferenceMap<>();

    /** some free information, that user can add during simulation */
    protected SimulationInformation information = null;
    protected SimulationParameter parameter = null;
    protected transient ResultStorage resultStorage = null;
    /** represent the category used for logging in simulation */
    public static String LOG_CATEGORY = "fr.ifremer.isisfish.simulator";

    /** flag to says if simulation is actually using a simulation logger */
    protected boolean useLog;

    protected transient File simulationControlFile = null;
    /** objective value used as cache to prevent disk access */
    protected transient Double objective = null;

    /**
     * Pour la lecture d'une simulation existante.
     * 
     * @param directory repertoire de base du storage (inclu le nom de la sim)
     * @param name le nom de la simulation
     * @param parameter les paramètres de la simulation
     */
    protected SimulationStorage(File directory, String name,
            SimulationParameter parameter) {
        super(directory, name);
        setParameter(parameter);
    }

    @Override
    public void closeStorage() throws TopiaException {
        // force result storage close
        if (resultStorage != null) {
            resultStorage.close();
            resultStorage = null;
        }
        super.closeStorage();
    }

    /**
     * Retourne le repertoire de base de stockage des simulations.
     * 
     * @return simulation directory
     */
    public static File getSimulationDirectory() {
        // warning, here, do not use getContextDatabaseDirectory()
        File result = IsisFish.config.getDatabaseDirectory();
        result = new File(result, SIMULATION_PATH);
        if (!result.exists()) {
            result.mkdirs();
        }
        return result;
    }

    /**
     * Retourne le repertoire de stockage d'une simulation portant le nom name.
     * 
     * @param name le nom de la simulation dont on souhaite le repertoire
     * @return simulation name directory
     */
    public static File getSimulationDirectory(String name) {
        File result = getSimulationDirectory();
        result = new File(result, name);
        if (!result.exists()) {
            result.mkdirs();
        }
        return result;
    }

    /**
     * Retourne le repertoire de base de stockage des resutats des analyses
     * de sensibilité.
     * 
     * @return sensitivity result directory
     */
    public static File getSensitivityResultsDirectory() {
        // warning, here, do not use getContextDatabaseDirectory()
        File result = IsisFish.config.getDatabaseDirectory();
        result = new File(result, SENSITIVITY_RESULTS);
        if (!result.exists()) {
            result.mkdirs();
        }
        return result;
    }

    /**
     * Retourne le repertoire de stockage des exports de resultat d'une
     * simulation.
     * 
     * @param root le repertoire de stockage de la simulation
     * @return result export directory
     */
    public static File getResultExportDirectory(File root) {
        File result = new File(root, RESULT_EXPORT_DIRECTORY);
        if (!result.exists()) {
            result.mkdirs();
        }
        return result;
    }

    /**
     * Retourne le fichier de stockage des parametres de la simulation.
     * 
     * @param root le repertoire de stockage de la simulation
     * @return parameter file
     */
    public static File getSimulationParametersFile(File root) {
        File result = new File(root, PARAMETERS_FILENAME);
        return result;
    }

    /**
     * Retourne le fichier de stockage des informations de la simulation.
     *
     * @param root le repertoire de stockage de la simulation
     * @return information file
     */
    public static File getSimulationInformationFile(File root) {
        File result = new File(root, INFORMATION_FILENAME);
        return result;
    }

    public Double getObjective() throws IOException {
        if (objective == null) {
            File objFile = getObjectiveFile();
            if (objFile.exists()) {
                String s = FileUtils.readFileToString(objFile, StandardCharsets.UTF_8);
                objective = Double.parseDouble(s);
            }
        }
        return objective;
    }

    public void setObjective(double d) throws IOException {
        objective = d;
        FileUtils.writeStringToFile(getObjectiveFile(), String.valueOf(d), StandardCharsets.UTF_8);
    }

    /**
     * Retourne le fichier de stockage des resultats sous format XML mexico
     * de la simulation.
     *
     * @param root le repertoire de stockage de la simulation
     * @return mexico xml file
     */
    public static File getSimulationResultXmlFile(File root) {
        File result = new File(root, RESULT_XML_FILENAME);
        return result;
    }

    /**
     * Retourne le fichier de stockage des resultats
     * de la simulation.
     *
     * @param root le repertoire de stockage de la simulation
     * @return
     */
    public static File getResultFile(File root) {
        File result = new File(root, RESULT_FILENAME);
        return result;
    }

    /**
     * Retourne le fichier de stockage des resultats
     * de la simulation.
     *
     * @param root le repertoire de stockage de la simulation
     * @return
     */
    public static File getResultDirectory(File root) {
        File result = new File(root, RESULT_DIRECTORY);
        if (!result.exists()) {
            result.mkdirs();
        }
        return result;
    }

    /**
     * Retourne le fichier de stockage des parametres de la simulation courante.
     * 
     * @return parameter file
     */
    protected File getSimulationParametersFile() {
        File result = getSimulationParametersFile(getDirectory());
        return result;
    }

    /**
     * Get Mexico XML design plan file.
     * 
     * @param root le repertoire de stockage de la simulation
     * @return a {@link File} even if file doesn't exist
     */
    public static File getMexicoDesignPlan(File root) {
        File result = new File(root, DESIGN_PLAN_FILENAME);
        return result;
    }

    /**
     * Retourne le fichier de stockage du fichier de control de la simulation.
     * Ce fichier conserve l'etat de la simulation, cela permet a un processus
     * externe de connaitre l'etat d'une simulation distante.
     * 
     * @return simulation control file
     */
    protected File getSimulationControlFile() {
        if (simulationControlFile == null) {
            simulationControlFile = new File(getDirectory(), CONTROL_FILENAME);
        }
        return simulationControlFile;
    }

    /**
     * Retourne le fichier de stockage du fichier de control de la simulation.
     * Ce fichier conserve l'etat de la simulation, cela permet a un processus
     * externe de connaitre l'etat d'une simulation distante.
     * 
     * @return simulation control file
     */
    public static File getSimulationControlFile(String id) {
        File result = new File(getSimulationDirectory(id), CONTROL_FILENAME);
        return result;
    }

    /**
     * Force la sauvegarde du fichier de control d'une simulation, ce fichier
     * est sous la forme d'un fichier de proprietes
     * @param control l'objet control de la simulation courante
     */
    public void saveControl(SimulationControl control) {
        try (FileOutputStream out = new FileOutputStream(getSimulationControlFile())) {
            Properties prop = control.getProperties();
            prop.store(out, "Control");
        } catch (Exception eee) {
            // juste un log, car la sauvegarde d'un control ne doit jamais echouer
            // car simplement utilise pour indique l'etat de simulation
            log.warn("Can't save control", eee);
        }
    }

    /**
     * Relit le fichier contenant les infos de SimulationControl, en excluant
     * certain champs.
     * 
     * @param id l'identifiant de la simulation a lire
     * @param control le control a mettre a jour en fonction de ce qui est lu
     * @param exclude les champs a exclure
     */
    public static void readControl(String id, SimulationControl control,
            String... exclude) {
        File file = getSimulationControlFile(id);
        readControl(file, control, exclude);
    }

    /**
     * Reli le fichier contenant les infos de SimulationControl, en excluant
     * certain champs.
     * 
     * @param controlFile le fichier de control
     * @param control le control a mettre a jour en fonction de ce qui est lu
     * @param exclude les champs a exclure
     */
    public static void readControl(File controlFile, SimulationControl control, String... exclude) {
        try {
            Properties prop = new Properties();
            if (controlFile.exists()) {
                try (FileInputStream in = new FileInputStream(controlFile)) {
                    prop.load(in);
                }
                for (String e : exclude) {
                    prop.remove(e);
                }
                control.updateFromProperties(prop);
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("Control file '" + controlFile.getAbsolutePath()
                            + "' doesn't exists");
                }
            }
        } catch (Exception e) {
            log.warn("Can't read control", e);
        }
    }

    /**
     * Modifie les parametres de la simulation.
     * 
     * @param parameter les nouveaux parametres
     */
    public void setParameter(SimulationParameter parameter) {
        this.parameter = parameter;
        if (parameter != null) {
            Properties prop = parameter.toProperties();
            if (log.isDebugEnabled()) {
                log.debug("Store params: " + prop);
            }
            File file = getSimulationParametersFile();
            try (FileOutputStream out = new FileOutputStream(file)) {
                prop.store(out, "Parameters");
            } catch (IOException eee) {
                throw new IsisFishRuntimeException(t("isisfish.error.save.simulation.parameters", file), eee);
            }
        }
    }

    /**
     * Get parameter property.
     * 
     * @return Parameter property.
     */
    public SimulationParameter getParameter() {
        if (parameter == null) {
            Properties prop = new Properties();
            File file = getSimulationParametersFile();

            // log
            if (log.isDebugEnabled()) {
                log.debug("Loading properties from : "
                                + file.getAbsolutePath());
            }

            try (FileInputStream in = new FileInputStream(file)) {
                prop.load(in);
                parameter = new SimulationParameterImpl();
                parameter.fromProperties(prop);
            } catch (IOException eee) {
                throw new IsisFishRuntimeException(t("isisfish.error.read.simulation.parameters", file), eee);
            }
        }
        return this.parameter;
    }

    /**
     * Permet de force la relecture des parametres et donc que les scripts
     * soit charge dans le bon classloader
     *
     * @return les paramètres de la simulation après opération
     */
    public SimulationParameter getForceReloadParameter() {
        // if parameter allready loaded, write it before reload. To take
        // all modification that user can be done
        // rewrite parameter
        if (parameter != null) {
            setParameter(parameter);
        }

        Properties prop = new Properties();
        File file = getSimulationParametersFile();
        try (FileInputStream in = new FileInputStream(file)) {
            prop.load(in);
            parameter = new SimulationParameterImpl();
            parameter.fromProperties(prop);
        } catch (IOException eee) {
            throw new IsisFishRuntimeException(t("isisfish.error.read.simulation.parameters", file), eee);
        }
        return this.parameter;
    }

    /**
     * Retourne le nom du fichier contenant les informations de simulation.
     * 
     * @return information file
     */
    protected File getInformationFile() {
        File result = new File(getDirectory(), INFORMATION_FILENAME);
        return result;
    }

    /**
     * Retourne le nom du fichier contenant les informations de simulation.
     *
     * @return information file
     */
    protected File getObjectiveFile() {
        File result = new File(getDirectory(), OBJECTIVE_FILENAME);
        return result;
    }

    /**
     * Get simulation informations.
     * 
     * @return simulation information
     */
    public SimulationInformation getInformation() {
        if (information == null) {
            information = new SimulationInformation(getInformationFile());
        }
        return this.information;
    }

    /**
     * Get simulation {@link ResultDatabaseStorage}.
     * 
     * @return le {@link ResultDatabaseStorage} a utiliser pour cette simulation
     * @throws StorageException
     */
    public ResultStorage getResultStorage() throws StorageException {
        if (resultStorage == null) {

            // depuis la version 4.2.0.0 le stockage a changé de base
            // de données à fichier mappe pour les resultat
            // mais il faut pouvoir relire les anciennes simulations
            SimulationParameter param = getParameter();
            String isisFishVersion = param.getIsisFishVersion();

            // instancier un ResultStorage ou un ResultMappedStorage
            if (Versions.smallerThan(isisFishVersion, "4.2.0.0")) {
                resultStorage = new ResultDatabaseStorage(this);
            } else {
                // if user ask for ResultStorageInMemory, used it
                String maxTimeStep = param.getTagValue().get(ResultStorageInMemory.MAX_TIME_STEP);
                if (maxTimeStep != null) {
                    // use in memory with max time step
                    resultStorage = new ResultStorageInMemory(this);
                } else {
                    try {
                        if (getResultFile(getDirectory()).exists()) {
                            // if result.mapped exist, use ResultMappedStorage
                            // need to read old simulation
                            resultStorage = new ResultMappedStorage(this);
                        } else {
                            // else use ResultStorageCSV, the new default result storage
                            resultStorage = new ResultStorageCSV(this);
                        }
                    } catch (IOException ex) {
                        throw new StorageException("Can't get storage", ex);
                    }
                }
            }
        }
        return resultStorage;
    }

    /**
     * Indique si la simulation demandée existe ou non.
     * 
     * @param name le nom de la simulation à tester
     * @return true si la simulation existe déjà
     */
    static public boolean exists(String name) {
        boolean result;
        result = getSimulationNames().contains(name);
        result = result || getRemoteSimulationNames().contains(name);
        return result;
    }

    /**
     * Indique si la simulation demandée existe ou non.
     * 
     * @param name le nom de la simulation à tester
     * @return true si la simulation existe déjà
     */
    public static boolean localyExists(String name) {
        
        // FIXME echatellier 20120229 this method can tak huge time
        // to replace with Directory existence
        return getSimulationNames().contains(name);
    }

    /**
     * Retourne la simulation demandée.
     * 
     * @param name name of the required simulation
     * @return la simulation souhaitée ou null si la simulation n'existe pas
     */
    public static SimulationStorage getSimulation(String name) {
        SimulationStorage result = simulations.get(name);
        if (result == null) {
            // recherche du repertoire de la simulation en fonction de la config
            File directory = getSimulationDirectory(name);
            log.info("Try to open simulation " + name + "(" + directory + ")");
            if (directory.exists()) {
                result = new SimulationStorage(directory, name, null);
                // we don't want to register this simulation since we don't
                // get all the data for the simulation but we just want the
                simulations.put(name, result);
            }
        }
        return result;
    }

    /**
     * Permet de créer une nouvelle simulation si la simulation existe déjà
     * une exception est levée.
     * @param name le nom de la simulation
     * @param parameter Les parametres de la simulation
     * @return la nouvelle simulation
     * @throws StorageException si problème lors de la création du storage
     * @throws IllegalArgumentException si le nom existe déjà en tant que
     * simulation
     */
    public static SimulationStorage create(String name,
            SimulationParameter parameter) throws StorageException {
        if (localyExists(name)) {
            throw new IllegalArgumentException(
                    "Can't create simulation this simulation name exists: "
                            + name);
        }
        File directory = getSimulationDirectory(name);
        SimulationStorage result = new SimulationStorage(directory, name,
                parameter);

        // 20060819: poussin
        // don't create schema, schema is created during data import from region
        //      try {            
        //      TopiaContext context = result.getStorage().beginTransaction();
        //      context.createSchema();
        //      context.commitTransaction();
        //      } catch (TopiaException eee) {
        //      throw new StorageException("Can't create new Region", eee);
        //      }

        simulations.put(name, result);
        return result;
    }

    /**
     * Retourne la liste des noms de toutes les régions disponible en local.
     * 
     * @return la liste des noms de toutes les régions disponible en local
     */
    public static List<String> getSimulationNames() {
        File dir = getSimulationDirectory();
        return getStorageNames(dir);
    }

    /**
     * Retourne la liste des noms de toutes les régions disponible en local qui
     * ne sont pas encore sur le serveur VCS.
     * 
     * @return liste de noms de simulations
     */
    public static List<String> getNewSimulationNames() {
        List<String> result = getSimulationNames();
        result.removeAll(getRemoteSimulationNames());
        return result;
    }

    /**
     * Retourne la liste des noms de toutes les régions disponible sur le
     * serveur VCS.
     * 
     * @return la liste des noms de toutes les régions disponible sur le
     * serveur VCS. Si le serveur n'est pas disponible la liste retournée
     * est vide.
     */
    public static List<String> getRemoteSimulationNames() {
        File dir = getSimulationDirectory();
        return getRemoteStorageNames(dir);
    }

    /**
     * Retourne la liste des noms de toutes les régions disponible sur le
     * serveur VCS qui ne sont pas encore en local.
     * 
     * @return liste de noms de simulations
     */
    public static List<String> getNewRemoteSimulationNames() {
        List<String> result = getRemoteSimulationNames();
        result.removeAll(getSimulationNames());
        return result;
    }

    /**
     * Retourne la {@link FisheryRegion} associee a cette simulation.
     * 
     * @param context context
     * @return simulation's {@link FisheryRegion}
     * @throws StorageException
     */
    public static FisheryRegion getFisheryRegion(TopiaContext context)
            throws StorageException {
        FisheryRegion result;
        try {
            FisheryRegionDAO regionDAO = IsisFishDAOHelper
                    .getFisheryRegionDAO(context);
            List<FisheryRegion> regions = regionDAO.findAll();
            if (regions.size() != 1) {
                throw new StorageException("Invalide region database"
                        + " number of region must be 1 not " + regions.size());
            }
            result = regions.get(0);
        } catch (TopiaException eee) {
            throw new StorageException(
                    "Can't find FisheryRegion in this Region", eee);
        }
        return result;
    }

    /**
     * Checkout not existing Simulation localy from server
     *
     * @param name name of simulation to retrieve
     * @throws VCSException si problème avec le VCS
     * @throws TopiaException si problème lors de l'opération sur la base embarquée
     */
    public static void checkout(String name) throws VCSException,
            TopiaException {
        checkout(IsisFish.config.getDatabaseDirectory(), SIMULATION_PATH + "/" + name);
        SimulationStorage sim = getSimulation(name);
        File file = sim.getDataBackupFile();
        TopiaContext tx = sim.getStorage().beginTransaction();
        tx.restore(file);
        tx.commitTransaction();
        tx.closeContext();
    }

    @Override
    public void rename(String toName) throws StorageException {
        simulations.remove(getName());
        super.rename(toName);
        simulations.put(toName, this);
    }

    /**
     * Import zipped simulation.
     *
     * @param file zipped region file
     * @return region storage or null
     * @throws IOException si problème IO lors de la lecture du zip
     * @throws TopiaException si problème lors de la création du contexte
     */
    public static SimulationStorage importZip(File file) throws IOException,
            TopiaException {
        return importAndRenameZip(file, null);
    }

    /**
     * Import zipped simulation.
     *
     * @param file zipped region file
     * @param newName new name for the imported simulation
     * @return region storage or null
     * @throws IOException si problème IO lors de la lecture du zip
     * @throws TopiaException si problème lors de la création du contexte
     */
    public static SimulationStorage importAndRenameZip(File file, String newName)
            throws IOException, TopiaException {
        SimulationStorage result = importAndRenameZip(getSimulationDirectory(),
                file, newName);
        // put result in cache
        simulations.put(result.getName(), result);
        return result;
    }

    /**
     * Import zipped simulation in specific directory, result is not put in cache.
     *
     * @param directory where we want new simulation storage
     * @param file zipped region file
     * @param newName new name for the imported simulation
     * @return region storage or null
     * @throws IOException si problème IO lors de la lecture du zip
     * @throws TopiaException si problème lors de la création du contexte
     */
    public static SimulationStorage importAndRenameZip(File directory,
            File file, String newName) throws IOException, TopiaException {
        if (!directory.exists()) {
            directory.mkdirs();
        }
        if (!directory.isDirectory()) {
            throw new IllegalArgumentException(t(
                    "directory %s must be a directory", directory
                            .getAbsolutePath()));
        }
        String renameFrom = null;
        String renameTo = null;
        if (newName != null) {
            renameFrom = "^.*?/(.*)$";
            renameTo = newName + "/$1";
        }
        if (log.isInfoEnabled()) {
            log.info(t("Import simulation file %s in directory %s and rename from %s to %s",
                        file, directory, renameFrom, renameTo));
        }
        String lastEntry = ZipUtil.uncompressAndRename(file, directory,
                renameFrom, renameTo);
        String name = lastEntry.substring(0, lastEntry.indexOf("/"));
        if (log.isInfoEnabled()) {
            log.info(t("Last entry was %s extract name %s", lastEntry, name));
        }
        File simDir = new File(directory, name);
        SimulationStorage result = new SimulationStorage(simDir, name, null);

        File data = result.getDataBackupFile();
        if (data.exists()) {
            TopiaContext tx = result.getStorage().beginTransaction();
            // force to have clean database before import
//            tx.executeSQL("DROP ALL OBJECTS;");
            tx.restore(data);
            tx.commitTransaction();
            result.closeStorage();
        }

        return result;
    }

    /**
     * Extract the region of a given simulation in a
     * @param regionName the name of the region to export
     * @throws StorageException if any problem while operation
     */
    public void extractRegion(String regionName) throws StorageException {

        File tmpDir = null;

        try {

            TopiaContext tx = getStorage().beginTransaction();
            String oldRegionName = getFisheryRegion(tx).getName();

            // create a pseudo oldRegionName region
            tmpDir = IsisFileUtil.createTempDirectory("extractRegionFromSimulation", "");
            List<File> forZip = new ArrayList<>(2);
            File file1;
            forZip.add(file1 = new File(tmpDir, oldRegionName));
            //forZip.add(file1 = getDataBackupFile(file1));
            forZip.add(file1 = new File(file1, DATA_BACKUP_FILENAME));
            if (!file1.exists()) {
                // we must prepare storage (no file found)
                prepare();
            }
            FileUtils.copyFile(getDataBackupFile(), file1);

            // zip it
            File zipRegion = new File(tmpDir, "extractedRegion.zip");
            ZipUtil.compressFiles(zipRegion, tmpDir, forZip);

            // import region with regionName as new name 
            RegionStorage.importAndRenameZip(zipRegion, regionName);

            tx.commitTransaction();
            tx.closeContext();
        } catch (IOException | TopiaException | RegionAlreadyExistException ex) {
            throw new StorageException("Can't extract region", ex);
        } finally {
            if (tmpDir != null && !FileUtils.deleteQuietly(tmpDir)) {
                log.warn(t("isisfish.error.delete.file", tmpDir));
            }
        }
    }

    /**
     * @return the simulation log file
     */
    public String getSimulationLogFile() {
        File root = getSimulationDirectory(name);
        return root + File.separator + "simulation.log";
    }

    @Override
    public File createZip(File file) throws IOException {
        
        // force parameter save
        // because they can have unsaved modification
        if (log.isDebugEnabled()) {
            log.debug("Save simulation parameters");
        }
        setParameter(parameter);
        
        return super.createZip(file);
    }

    @Override
    protected boolean isVersionnableFile(File file) {

        // Warning, don't call super() here

        // Cette methode a été surchargé, car les fichiers ne sont pas dans
        // le repos, mais seulement pour le cas du SimulationStorage
        String filename = file.getName();
        boolean result = !".svn".equals(filename) && !"CVS".equals(filename) && !filename.endsWith("~");
        
        // don't set data
        // FIXME don't set it here
        result &= !new File(getFile(), "data").equals(file);

        return result;
    }

    /**
     * Export un zip contenant les données de réference avec:
     * <ul>
     *     <li>toutes les simulations en cas de plan</li>
     *     <li>les données R, Rdata</li>
     *     <li>les export de sensibilité</li>
     * </ul>
     * @param file out zip file
     */
    public void createReferenceZip(File file) throws IOException {
        String simulationName = getName();

        // in case of multi-simulation, get short simulation name
        if (getParameter().getUseSimulationPlan() || getParameter().getUseOptimization() || getParameter().getSensitivityAnalysis() != null) {
            simulationName = RegExUtils.removeFirst(simulationName, "_\\d+$");
        }

        // add extension if not present
        file = IsisFileUtil.addExtensionIfNeeded(file, "zip");

        // compute common paths
        String simulationPrefix = new File(getSimulationDirectory(), simulationName).getAbsolutePath();
        String sensitivityResultPrefix = new File(getSensitivityResultsDirectory(), simulationName).getAbsolutePath();
        ZipUtil.compress(file, getRoot(), f -> {
            boolean result = StringUtils.startsWithAny(f.getAbsolutePath(), simulationPrefix, sensitivityResultPrefix);
            return result;
        });

    }
} // SimulationStorage
