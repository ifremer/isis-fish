/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2005 - 2012 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.datastore;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.map.ReferenceMap;

import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.vcs.VCSException;

/**
 * Gestion des fichers CVS de type scripts (appartenant au module scripts).
 * 
 * Created: 18 août 2005 15:07:36 CEST
 *
 * @author Grégoire DESSARD &lt;dessard@codelutin.com&gt;
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class ScriptStorage extends JavaSourceStorage {

    public static final String SCRIPT_PATH = "scripts";

    /** Template freemarker pour les scripts. */
    public static final String SCRIPT_TEMPLATE = "templates/script/script.ftl";

    /** Instance cache. */
    static protected Map<String, ScriptStorage> scriptsCache = new ReferenceMap<>();

    /**
     * 
     * Constructeur
     * 
     * @param rootSrc le repertoire root de stockage
     * @param directory le repertoire des scripts
     * @param name le nom du script
     */
    protected ScriptStorage(File rootSrc, File directory, String name) {
        super(rootSrc, directory, name);
    }

    @Override
    public String getPackage() {
        return SCRIPT_PATH;
    }

    /**
     * Get context (official VCS) script directory.
     * 
     * @return context script directory
     */
    static public File getScriptDirectory() {
        File result = new File(getContextDatabaseDirectory(), SCRIPT_PATH);
        result.mkdirs();
        return result;
    }

    /**
     * Get community VCS script directory.
     * 
     * @return community script directory
     */
    public static File getCommunityScriptDirectory() {
        File result = new File(getCommunityDatabaseDirectory(), SCRIPT_PATH);
        result.mkdirs();
        return result;
    }

    /**
     * Retourne le storage pour la regle demandée
     * 
     * @param name le nom de la regle souhaitée
     * @param location location to open storage file
     * @return Le storage pour la regle
     */
    static public ScriptStorage getScript(String name, Location... location) {
        ScriptStorage result = scriptsCache.get(name);
        if (result == null) {
            Location loc = nonEmptyLocation(location);
            for (File dir : loc.getDirectories()) {
                ScriptStorage storage = new ScriptStorage(dir, new File(dir, SCRIPT_PATH), name);
                if (storage.getFile().isFile()) {
                    result = storage;
                    scriptsCache.put(name, result);
                }
            }
        }
        return result;
    }

    /**
     * Create new script.
     * 
     * @param name new script to create
     * @param location location to script storage file
     * @return new rule script
     */
    public static ScriptStorage createScript(String name, Location location) {
        File dir = location.getDirectories()[0];
        ScriptStorage storage = new ScriptStorage(dir, new File(dir, SCRIPT_PATH), name);
        return storage;
    }

    static public void checkout() throws VCSException {
        checkout(IsisFish.config.getDatabaseDirectory(), SCRIPT_PATH);
    } 
    
    /**
     * Retourne la liste des noms de toutes les régions disponible en local
     * 
     * @return la liste des noms de toutes les régions disponible en local
     */
    static public List<String> getScriptNames() {
        File dir = getScriptDirectory();
        return getStorageNames(dir);
    }

    /**
     * Retourne la liste des noms de toutes les régions disponible en local qui
     * ne sont pas encore sur le serveur CVS
     * 
     * @return liste de noms de regions
     */
    static public List<String> getNewScriptNames() {
        List<String> result = getScriptNames();
        result.removeAll(getRemoteScriptNames());
        return result;
    }

    /**
     * Retourne la liste des noms de toutes les régions disponible sur le
     * serveur CVS
     * 
     * @return la liste des noms de toutes les régions disponible sur le serveur
     *         CVS. Si le serveur n'est pas disponible la liste retournée est
     *         vide.
     */
    static public List<String> getRemoteScriptNames() {
        File dir = getScriptDirectory();
        return getRemoteStorageNames(dir);

    }

    /**
     * Retourne la liste des noms de toutes les régions disponible sur le
     * serveur CVS qui ne sont pas encore en local
     * 
     * @return liste de noms de regions
     */
    static public List<String> getNewRemoteScriptNames() {
        List<String> result = getRemoteScriptNames();
        result.removeAll(getScriptNames());
        return result;
    }
}
