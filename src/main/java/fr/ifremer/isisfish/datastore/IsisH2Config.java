/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2019 Ifremer, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.datastore;

import java.io.File;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.cfg.Environment;
import org.hibernate.dialect.H2Dialect;
import org.nuiton.topia.migration.TopiaMigrationEngine;
import org.nuiton.topia.migration.TopiaMigrationService;

import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.IsisFishDAOHelper;
import fr.ifremer.isisfish.datastore.migration.DatabaseMigrationClass;

/**
 * Isis H2 concrete Config implementation.
 *
 * @author chemit
 *
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class IsisH2Config {

    /** Class logger. */
    private static Log log = LogFactory.getLog(IsisH2Config.class);

    /** Set in static field to be overridden by tests. Default to swing callback. */
    protected static Class<?> databaseMigrationClass = DatabaseMigrationClass.class;

    protected static final String COMMON_URL =
            // Sets the default lock timeout (in milliseconds) in this database
            // that is used for the new sessions.
            "DEFAULT_LOCK_TIMEOUT=1000;" +
            // -1: the database is never closed until the close delay is set to
            // some other rev or SHUTDOWN is called., 0: no delay (default; the
            // database is closed if the last connection to it is closed)., n:
            // the database is left open for n second after the last connection
            // is closed.
            "DB_CLOSE_DELAY=0;" +
            // 0: no locking (should only be used for testing),
            // 1: table level locking (default),
            // 2: table level locking with garbage collection (if the
            // application does not close all connections). 
            // LOCK_MODE 3 (READ_COMMITTED). Table level locking, but only when
            // writing (no read locks).
            "LOCK_MODE=3;" +
            // Levels: 0=off, 1=error, 2=info, 3=debug.
            "TRACE_LEVEL_FILE=0;" +
            // on system.out: 0=off, 1=error, 2=info, 3=debug.
            "TRACE_LEVEL_SYSTEM_OUT=0";

    protected static final String H2_URL = 
            "jdbc:h2:file:%s;" +
            // on peut aussi utiliser file, socket
            "FILE_LOCK=%s;" +
            //1 or 2 is needed to restore avec crash 
            // 0: logging is disabled (faster),
            // 1: logging of the data is enabled, but logging of the index
            // changes is disabled (default), 2: logging of both data and index
            // changes are enabled
            "LOG=1;" +
                // permet de prevenir le bug lorsqu'on utilise un script d'optimisation
            // la simulation est deja creer (pour etre configurable par le script)
            // et on dezippe dessus la simulation a faire si la simulation se fait
            // sur la meme machine. Normalement la simulation devrait etre ferme
            // fr.ifremer.isisfish.simulator.launcher.OptimizationPrepareJob.run#118
            // mais certaine fois non (pourquoi ???)
            "AUTO_SERVER=TRUE;" +
            COMMON_URL;

    protected static final String H2_MEM_URL =
            "jdbc:h2:mem:%s;" +
            "LOG=0;" +
            COMMON_URL;

    /**
     * @param rootDir le repertoire de base du DataStorage
     * @return le repertoire de donnees d'un Data storage a partir de son repertoire de base
     */
    public static File getStorageDataDirectory(File rootDir) {
        return new File(rootDir, "data");
    }

    //////////////////////////////////////////////////
    // Les operations sur base embarquee
    //////////////////////////////////////////////////

    /**
     * Retourne une base en memoire
     *
     * @param config la configuration a remplir
     * @param id     l'identifiant de la base
     * @return l'objet config passe en parametre
     */
    public static Properties addMemDatabaseConfig(Properties config, String id) {

        config.setProperty(Environment.USER, "sa");
        config.setProperty(Environment.PASS, "");
        config.setProperty(Environment.DIALECT, H2Dialect.class.getName());
        config.setProperty(Environment.DRIVER, org.h2.Driver.class.getName());
        // correct error : org.hibernate.HibernateException: No CurrentSessionContext configured!
        config.setProperty(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");
        //config.setProperty(Environment.SHOW_SQL, "true");

        // Maximum waiting time for a connection from the pool
        config.setProperty("hibernate.hikari.connectionTimeout", "20000");
        // Minimum number of ideal connections in the pool
        config.setProperty("hibernate.hikari.minimumIdle", "1");
        // Maximum number of actual connection in the pool
        config.setProperty("hibernate.hikari.maximumPoolSize", "20");
        // Maximum time that a connection is allowed to sit ideal in the pool
        config.setProperty("hibernate.hikari.idleTimeout", "300000");

        String url = String.format(H2_MEM_URL, id);
        config.setProperty(Environment.URL, url);

        return config;
    }

    /**
     * Ajoute differentes informations au parametre config qui indique comment
     * se connecter a la base de donnees.
     *
     * @param config    l'objet properties dans lequel il faut ajouter les informations
     * @param directory le repertoire dans lequel la base de données doit se trouver
     * @return l'objet config passe en parametre
     */
    public static Properties addDatabaseConfig(Properties config, File directory) {
        File databasePath = getStorageDataDirectory(directory);
        databasePath.mkdirs();

        // on reutilise les memes valeurs, seul l'url change, mais on l'ecrase ensuite
        addMemDatabaseConfig(config, "");

        // pour h2 on remet STORAGE_DATA car il ne cree pas de repertoire
        String url = String.format(H2_URL, getStorageDataDirectory(databasePath).getPath(), IsisFish.config.getDatabaseLockMode());
        config.setProperty(Environment.URL, url);
        if (log.isDebugEnabled()) {
            log.debug(Environment.URL + " = " + url);
        }

        // migration configuration, seulement pour les fichiers, pas pour les bases memoires
        config.put(TopiaMigrationService.TOPIA_SERVICE_NAME, TopiaMigrationEngine.class.getName());
        config.put(TopiaMigrationService.MIGRATION_CALLBACK, databaseMigrationClass.getName());
        /*if (!performMigration) {
            config.put(TopiaMigrationService.MIGRATION_MIGRATE_ON_INIT, databaseMigrationClass.getName());
        }*/

        return config;
    }

    /**
     * Permet d'ajouter les differents mapping hibernate spécifique à l'application.
     *
     * @param config l'objet properties dans lequel il faut ajouter les informations
     * @return l'objet config passe en parametre
     */
    public static Properties addHibernateMapping(Properties config) {
        config.setProperty("topia.persistence.classes",
                IsisFishDAOHelper.getImplementationClassesAsString()
        );
        return config;
    }

}
