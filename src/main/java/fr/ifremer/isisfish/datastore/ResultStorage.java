/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer, Code Lutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.datastore;

import java.util.List;

import org.nuiton.math.matrix.MatrixND;
import org.nuiton.topia.TopiaContext;

import fr.ifremer.isisfish.IsisFishException;
import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.export.ExportInfo;
import fr.ifremer.isisfish.export.SensitivityExport;
import fr.ifremer.isisfish.rule.Rule;
import fr.ifremer.isisfish.simulator.SimulationPlan;
import fr.ifremer.isisfish.simulator.SimulationResultListener;
import fr.ifremer.isisfish.types.TimeStep;

public interface ResultStorage extends SimulationResultListener {

    /**
     * Return some information on result storage. Information depend on
     * result storage type
     * @return
     */
    String getInfo();

    /**
     * Permet de savoir si lorsque l'on ajoutera ce resultat, il sera
     * sauvé ou non.
     * 
     * Check for result name returned by :
     * <ul>
     *  <li>{@link ExportInfo#getNecessaryResult()}</li>
     *  <li>{@link SensitivityExport#getNecessaryResult()}</li>
     *  <li>{@link Rule#getNecessaryResult()}</li>
     *  <li>{@link SimulationPlan#getNecessaryResult()}</li>
     * </ul>
     * 
     * @param name result name
     * @return {@code true} if result is enabled
     * @deprecated since 4.4.0.0 : this method is duplicated with ResultManager#isEnabled(String)
     */
    boolean isEnabled(String name);

    void addResult(TimeStep step, MatrixND mat) throws IsisFishException;

    void addResult(TimeStep step, Population pop, MatrixND mat)
            throws IsisFishException;

    void addResult(boolean force, TimeStep step, MatrixND mat)
            throws IsisFishException;

    void addResult(boolean force, TimeStep step, Population pop,
                   MatrixND mat) throws IsisFishException;

    void addResult(TimeStep step, String name, Population pop,
                   MatrixND mat) throws IsisFishException;

    void addResult(TimeStep step, String name, MatrixND mat)
            throws IsisFishException;

    void addResult(boolean force, TimeStep step, String name,
                   Population pop, MatrixND mat) throws IsisFishException;

    void addResult(boolean force, TimeStep step, String name,
                   MatrixND mat) throws IsisFishException;

    void addActiveRule(TimeStep step, Rule rule)
            throws IsisFishException;

    /**
     * Retourne la liste de tous les résultats. Si le résultat est categorisé
     * par une population alors le nom de la population est automatiquement
     * ajouté au nom du résultat.
     */
    List<String> getResultName();

    /**
     * Retourne la matrice stocke pour un pas de temps
     * @param step le pas de temps que l'on souhaite
     * @param pop la population pour lequelle on souhaite le resultat
     * @param name le nom des resultats dont on veut la matrice
     * @return La matrice demandée ou null si aucune matrice ne correspond a
     * la demande.
     */
    MatrixND getMatrix(TimeStep step, Population pop, String name);

    MatrixND getMatrix(TimeStep step, Population pop, String name, TopiaContext tx);

    MatrixND getMatrix(TimeStep step, String name);

    /**
     * Retourne la matrice stocke pour un pas de temps.
     * 
     * @param step le pas de temps que l'on souhaite
     * @param name le nom des resultats dont on veut la matrice
     * @param tx TopiaContext a utiliser pour recuperer les resultats et donc les semantiques
     * @return La matrice demandée ou {@code null} si aucune matrice ne correspond a
     * la demande.
     */
    MatrixND getMatrix(TimeStep step, String name, TopiaContext tx);

    /**
     * Retourne la matrice stocke pour des pas de temps
     * @param steps les pas de temps que l'on souhaite
     * @param pop la population pour lequelle on souhaite le resultat
     * @param name le nom des resultats dont on veut la matrice
     * @return La matrice demandée ou null si aucune matrice ne correspond a
     * la demande.
     */
    MatrixND getMatrix(List<TimeStep> steps, Population pop, String name);

    MatrixND getMatrix(List<TimeStep> steps, Population pop, String name, TopiaContext tx);

    MatrixND getMatrix(List<TimeStep> steps, String name);

    /**
     * Retourne la matrice stocke pour des pas de temps.
     *
     * @param steps les pas de temps que l'on souhaite
     * @param name le nom des resultats dont on veut la matrice
     * @param tx TopiaContext a utiliser pour recuperer les resultats et donc les semantiques
     * @return La matrice demandée ou {@code null} si aucune matrice ne correspond a
     * la demande.
     */
    MatrixND getMatrix(List<TimeStep> steps, String name, TopiaContext tx);


    /**
     * Retourne une matrice contenant tous les pas de temps.
     * @param pop la population pour lequel on souhaite la matrice
     * @param name le nom des resultats dont on veut une matrice globale.
     */
    MatrixND getMatrix(Population pop, String name);

    /**
     * Retourne une matrice contenant tous les pas de temps.
     * @param pop la population pour lequel on souhaite la matrice
     * @param name le nom des resultats dont on veut une matrice globale.
     * @param tx la transaction a utiliser
     * @since 4.1.0.0
     */
    MatrixND getMatrix(Population pop, String name, TopiaContext tx);

    /**
     * Retourne une matrice contenant tous les pas de temps.
     * @param name le nom des resultats dont on veut une matrice globale.
     */
    MatrixND getMatrix(String name);

    /**
     * Retourne une matrice contenant tous les pas de temps.
     * @param name le nom des resultats dont on veut une matrice globale.
     * @param tx TopiaContext a utiliser pour recuperer les resultats et donc les semantiques
     */
    MatrixND getMatrix(String name, TopiaContext tx);


    /**
     * Get last simulation date.
     * 
     * @return last simulation date
     */
    TimeStep getLastStep();

    /**
     * Delete all result for this result storage.
     * 
     * In case of sensitivity analysis, after export, result are no longer
     * needed and can be deleted to save disk space.
     * 
     * @since 4.1.1.2
     */
    void delete();

    /**
     * Close result storage.
     * 
     * @since 4.1.1.2
     */
    void close();
}
