/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2005 - 2019 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.datastore;

import static org.nuiton.i18n.I18n.t;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

import org.apache.commons.collections4.map.ReferenceMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.export.ExportInfo;
import fr.ifremer.isisfish.annotations.Doc;
import fr.ifremer.isisfish.vcs.VCSException;

/**
 * Gestion des fichers VCS de type {@link ExportInfo}
 * (appartenant au module exports).
 *
 * Created: 18 août 2005 15:07:36 CEST
 *
 * @author Grégoire DESSARD &lt;dessard@codelutin.com&gt;
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class ExportStorage extends JavaSourceStorage {

    /** Class logger. */
    private static Log log = LogFactory.getLog(ExportStorage.class);

    public static final String EXPORT_PATH = "exports";

    /** Template freemarker pour les scripts d'export . */
    public static final String EXPORT_TEMPLATE = "templates/script/export.ftl";

    /** Instance cache. */
    private static Map<String, ExportStorage> exportCache = new ReferenceMap<>();

    protected static Set<StorageChangeListener> storageListeners = Collections.newSetFromMap(new WeakHashMap<>());

    public static void addStorageListener(StorageChangeListener storageListener) {
        storageListeners.add(storageListener);
    }

    /**
     * Constructeur.
     *
     * @param rootSrc   le repertoire root de stockage des exports
     * @param directory le repertoire de l'export
     * @param name      le nom de l'export
     */
    protected ExportStorage(File rootSrc, File directory, String name) {
        super(rootSrc, directory, name);
    }

    public static void registerWatcher() {
        registerWatcher(storageListeners, getExportDirectory(), getCommunityExportDirectory());
    }

    @Override
    public String getPackage() {
        return EXPORT_PATH;
    }

    /**
     * Get context (official VCS) export directory.
     * 
     * @return context export directory
     */
    public static File getExportDirectory() {
        File result = new File(getContextDatabaseDirectory(), EXPORT_PATH);
        result.mkdirs();
        return result;
    }
    
    /**
     * Get community VCS export directory.
     * 
     * @return community export directory
     */
    public static File getCommunityExportDirectory() {
        File result = new File(getCommunityDatabaseDirectory(), EXPORT_PATH);
        result.mkdirs();
        return result;
    }

    /**
     * Retourne le storage pour l'export demandée
     *
     * @param name le nom de l'export souhaitée
     * @param location location to open storage file
     * @return Le storage pour l'export
     */
    public static ExportStorage getExport(String name, Location... location) {
        ExportStorage result = exportCache.get(name);
        if (result == null) {
            Location loc = nonEmptyLocation(location);
            for (File dir : loc.getDirectories()) {
                ExportStorage storage = new ExportStorage(dir, new File(dir, EXPORT_PATH), name);
                if (storage.getFile().isFile()) {
                    result = storage;
                    exportCache.put(name, result);
                }
            }
        }
        return result;
    }

    /**
     * Create new export.
     * 
     * @param name new export to create
     * @param location location to create storage file
     * @return new export storage
     */
    public static ExportStorage createExport(String name, Location location) {
        File dir = location.getDirectories()[0];
        ExportStorage storage = new ExportStorage(dir, new File(dir, EXPORT_PATH), name);
        return storage;
    }

    /**
     * Retourne la liste des noms de toutes les régions disponible en local
     *
     * @return la liste des noms de toutes les régions disponible en local
     */
    public static List<String> getExportNames() {
        List<String> result = getStorageNames(getExportDirectory());
        result.addAll(getStorageNames(getCommunityExportDirectory()));
        return result;
    }

    public static void checkout() throws VCSException {
        checkout(IsisFish.config.getDatabaseDirectory(), EXPORT_PATH);
    }

    /**
     * <b>Be ware this method require to instantiate a ExportInfo, so
 it would be better to call as often as possible.</b>
     *
     * @return the description of the instantiate ExportInfo
     * @see Doc
     */
    public String getDescription() {
        String result = null;
        try {
            ExportInfo export = getNewInstance();
            if (export != null) {
                result = export.getDescription();
            }
        } catch (Exception e) {
            log.warn(t("isisfish.error.not.found.description",this));
        }
        return result;
    }
}
