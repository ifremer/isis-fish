/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2011 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.datastore;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.collections4.map.ReferenceMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.util.FileUtil;

import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.IsisFishDAOHelper;
import fr.ifremer.isisfish.entities.Equation;
import fr.ifremer.isisfish.entities.EquationDAO;
import fr.ifremer.isisfish.entities.Formule;
import fr.ifremer.isisfish.vcs.VCSException;

/**
 * Formule storage.
 * 
 * Created: 4 févr. 2006 16:30:59
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class FormuleStorage extends CodeSourceStorage implements Formule {

    /**
     * Logger for this class
     */
    private static final Log log = LogFactory.getLog(FormuleStorage.class);

    public static final String FORMULE_PATH = "formules";

    /** Template freemarker pour les formules. */
    public static final String FORMULE_TEMPLATE = "templates/script/equation.ftl";

    /** Instance cache. */
    static private Map<String, FormuleStorage> formulesCache = new ReferenceMap<>();

    /**
     * la categorie de la formule
     */
    protected String category = null;

    /**
     * Create new formule storage.
     * 
     * @param rootSrc   le repertoire root de stockage des formules
     * @param directory le repertoire des formules
     * @param category  la category de la formule
     * @param name      le nom de la formule
     * @param extension l'extension a utiliser ".java"
     */
    public FormuleStorage(File rootSrc, File directory, String category, String name, String extension) {
        super(rootSrc, new File(directory, category), name, "." + extension);
        this.category = category;
    }

    /**
     * Get official VCS formule directory.
     * 
     * @return context formule directory
     */
    public static File getFormuleDirectory() {
        File result = new File(IsisFish.config.getDatabaseDirectory(), FORMULE_PATH);
        result.mkdirs();
        return result;
    }

    /**
     * Get community VCS formule directory.
     * 
     * @return community formule directory
     */
    public static File getCommunityFormuleDirectory() {
        File result = new File(getCommunityDatabaseDirectory(), FORMULE_PATH);
        result.mkdirs();
        return result;
    }

    /**
     * @return Returns the category.
     */
    public String getCategory() {
        return this.category;
    }

    /**
     * Retourne toutes les formules sauvegardees
     *
     * @param category la categorie des formules recherchees
     * @return la liste de formule de la category
     */
    static public List<FormuleStorage> getFormules(String category) {
        List<FormuleStorage> result = new ArrayList<>();
        try {
            File dir = new File(getFormuleDirectory(), category);
            dir.mkdirs();            
            for (File f : dir.listFiles()) {
                if (getVCS().isVersionnableFile(f)) {
                    String name = FileUtil.basename(f);
                    result.add(getFormule(category, name));
                }
            }
        } catch (Exception eee) {
            if (log.isErrorEnabled()) {
                log.error("Can't load formule for category: " + category, eee);
            }
        }
        return result;
    }

    /**
     * Retourne toutes les formules sauvegardees
     *
     * @param category la categorie des formules recherchees
     * @return la liste de formule de la category
     */
    static public List<FormuleStorage> getCommunityFormules(String category) {
        List<FormuleStorage> result = new ArrayList<>();
        try {
            File dir = new File(getCommunityFormuleDirectory(), category);
            dir.mkdirs();
            for (File f : dir.listFiles()) {
                if (getCommunityVCS().isVersionnableFile(f)) {
                    String name = FileUtil.basename(f);
                    result.add(getCommunityFormule(category, name));
                }
            }
        } catch (Exception eee) {
            if (log.isErrorEnabled()) {
                log.error("Can't load formule for category: " + category, eee);
            }
        }
        return result;
    }

    /**
     * Retourne toutes les formules sauvegardees
     *
     * @param category la categorie des formules dont on souhiate le nom
     * @return la liste des noms formules existantes dans une categorie
     */
    static public List<String> getFormuleNames(String category) {
        List<String> result = new ArrayList<>();
        try {
            File dir = new File(getFormuleDirectory(), category);
            dir.mkdirs();
            for (File f : dir.listFiles()) {
                if (getVCS().isVersionnableFile(f)) {
                    String name = FileUtil.basename(f);
                    result.add(name);
                }
            }
        } catch (Exception eee) {
            if (log.isErrorEnabled()) {
                log.error("Can't load formule for category: " + category, eee);
            }
        }
        return result;
    }

    /**
     * Retourne toutes les categories existantes dans les formules sauvegardées
     * ainsi que dans le TopiaContext passé en parametre
     *
     * @param context le context dans lequel il faut faire la recherche
     * @return la liste des noms des categories existantes
     */
    static public List<String> getCategories(TopiaContext context) {
        Set<String> all = new TreeSet<>(getCategories());

        try {
            List<String> contextCategory = (List<String>) context.findAll(
                    "SELECT DISTINCT category FROM Equation");
            all.addAll(contextCategory);
        } catch (TopiaException eee) {
            if (log.isWarnEnabled()) {
                log.warn("Can't get category from TopiaContext", eee);
            }
        }

        List<String> result = new ArrayList<>(all);

        return result;
    }

    /**
     * Retourne toutes les formules sauvegardees ainsi que dans le TopiaContext
     * passé en parametre
     *
     * @param category la categorie dans lequel il faut faire la recherche
     * @param context  le context dans lequel il faut aussi recherche en plus
     * des formules sauvegardees en tant que model
     * @return la liste des formules de la categorie
     */
    static public List<Formule> getFormules(TopiaContext context, String category) {

        List<Formule> result = new ArrayList<>(getFormules(category));
        result.addAll(getCommunityFormules(category));

        if (context != null) {
            try {
                EquationDAO dao = IsisFishDAOHelper.getEquationDAO(context);
                List<Equation> contextFormule = dao.findAllByCategory(category);
                result.addAll(contextFormule);
            } catch (TopiaException eee) {
                if (log.isWarnEnabled()) {
                    log.warn("Can't get Equation from TopiaContext", eee);
                }
            }
        }

        return result;
    }

    /**
     * Retourne toutes les categories existantes dans les formules sauvegardées.
     *
     * @return la liste des categories existant en tant que modele
     */
    static public List<String> getCategories() {
        List<String> result = new ArrayList<>();
        File dir = getFormuleDirectory();
        if (dir.exists()) {
            for (File f : dir.listFiles()) {
                if (f.isDirectory() && getVCS().isVersionnableFile(f)) {
                    result.add(f.getName());
                }
            }
        }
        return result;
    }

    /**
     * Retourne le storage pour la regle demandée
     *
     * @param name     le nom de la regle souhaitée
     * @param category la categorie de la regle
     * @return Le storage pour la regle
     */
    static public FormuleStorage getFormule(String category, String name) {
        String key = category + File.separator + name;
        FormuleStorage result = formulesCache.get(key);
        if (result == null) {
            String extension = FileUtil.extension(new File(name), ".");
            if (!"".equals(extension)) {
                name = name.substring(0, name.length() - (extension.length() + 1));
            }
            result = new FormuleStorage(IsisFish.config.getDatabaseDirectory(),
                    getFormuleDirectory(), category, name, extension);
            formulesCache.put(key, result);
        }
        return result;
    }
    
    /**
     * Retourne le storage pour la regle demandée
     *
     * @param name     le nom de la regle souhaitée
     * @param category la categorie de la regle
     * @return Le storage pour la regle
     */
    static public FormuleStorage getCommunityFormule(String category, String name) {
        String key = "community" + category + File.separator + name;
        FormuleStorage result = formulesCache.get(key);
        if (result == null) {
            String extension = FileUtil.extension(new File(name), ".");
            if (!"".equals(extension)) {
                name = name.substring(0, name.length() - (extension.length() + 1));
            }
            result = new FormuleStorage(IsisFish.config.getCommunityDatabaseDirectory(),
                    getCommunityFormuleDirectory(), category, name, extension);
            formulesCache.put(key, result);
        }
        return result;
    }

    /**
     * creer une nouvelle formule, si la formule existait deja, elle est ecrasé
     *
     * @param category  la category de la formule
     * @param name      le nom de la formule
     * @param extension le script de la formule
     * @return Le storage pour la regle
     */
    static public FormuleStorage createFormule(String category, String name, String extension) {
        String key = category + File.separator + name + extension;
        FormuleStorage result = formulesCache.get(key);
        if (result == null) {
            result = new FormuleStorage(IsisFish.config.getDatabaseDirectory(),
                    getCommunityFormuleDirectory(), category, name, extension);
            formulesCache.put(key, result);
        }
        return result;
    }

    static public void checkout() throws VCSException {
        checkout(IsisFish.config.getDatabaseDirectory(), FORMULE_PATH);
    }
}
