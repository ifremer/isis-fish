/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2019 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.datastore;

import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.IsisFishException;
import fr.ifremer.isisfish.config.IsisConfig;
import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.annotations.Doc;
import fr.ifremer.isisfish.util.CompileHelper;
import fr.ifremer.isisfish.util.JavadocHelper;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Classes abstraite permettant la gestion des fichiers de code source.
 * 
 * Exemple :
 * {@link fr.ifremer.isisfish.datastore.SimulationPlanStorage}
 * {@link fr.ifremer.isisfish.datastore.ExportStorage},
 * {@link fr.ifremer.isisfish.datastore.RuleStorage},
 * {@link fr.ifremer.isisfish.datastore.ScriptStorage},
 * {@link fr.ifremer.isisfish.datastore.SensitivityAnalysisStorage},
 * {@link fr.ifremer.isisfish.datastore.SimulatorStorage}
 *
 * @author poussin
 * 
 * @version $Revision$
 * 
 * Last update: $Date$ by : $Author$
 */
public abstract class JavaSourceStorage extends CodeSourceStorage {

    /** Class logger. */
    private static final Log log = LogFactory.getLog(ExportStorage.class);

    /** Le prefix de tous les attributs java manipulable par Isis. */
    protected static final String PARAM_PREFIX = "param_";

    /**
     * Build a new java source storage.
     * 
     * @param rootSrc sources root directory
     * @param directory class location directory (rootSrc subdirectory)
     * @param name class name
     */
    protected JavaSourceStorage(File rootSrc, File directory, String name) {
        super(rootSrc, directory, name, ".java");
    }

    /**
     * Get package name where script should be declared.
     * 
     * @return package declaration
     */
    public abstract String getPackage();

    /**
     * Return class fully qualified name.
     * 
     * @return class name including package name
     */
    public String getFQN() {
        String root = getRoot().getAbsolutePath();
        String path = getFile().getParentFile().getAbsolutePath();

        String packageName = path.substring(1 + root.length()).replace(
                File.separatorChar, '.');

        String result = packageName + "." + getName();
        if (result.endsWith(".java")) {
            result = result.substring(0, result.length() - ".java".length());
        }

        return result;
    }

    /**
     * Retourne le nom de la classe (sans le package) a partir d'un objet.
     * 
     * @param instance l'instance dont on veut le nom
     * @return le nom de la classe ou null si instance est null
     */
    public static String getName(Object instance) {
        return ClassUtils.getShortClassName(instance, null);
    }

    /**
     * Compile cette classe dans le répertoire par defaut de compilation
     * ({@link IsisConfig#getCompileDirectory()})
     * 
     * @param force si vrai alors meme si le fichier destination est plus recent
     *        la compilation aura lieu
     * @param out le flux sur lequel le resultat de la compilation doit
     *        apparaitre. Peut-etre null, dans ce cas les sorties standards sont
     *        utilisées.
     * @return 0 si la compilation a reussi, une autre valeur sinon
     */
    public int compile(boolean force, PrintWriter out) {
        int result = CompileHelper.compile(this, IsisFish.config
                .getCompileDirectory(), force, out);

        // add a specific isi test for "package xxx;" declaration that is NOT an error
        // in pur Java, but is for isis fish
        if (result == 0) {
            String content = getContent();
            String packageDeclaration = "package " + getPackage() + ";";
            if (!content.contains(packageDeclaration)) {
                if (out != null) {
                    out.println("Script doesn't contains mandatory package declaration:");
                    out.println("   " + packageDeclaration);
                }

                result = 2;
            }
        }

        return result;
    }

    /**
     * Build class javadoc.
     * 
     * Ouput javadoc will be stored in default javadoc directory :
     * {@link IsisConfig#getJavadocDirectory()}
     * 
     * @param force force javadoc build even if destination file is never
     * @param out output print stream. if {@code null} standart output will be used
     * @return 0 si la generation a reussi, une autre valeur sinon
     */
    public int doJavadoc(boolean force, PrintWriter out) {
        int result = JavadocHelper.generateJavadoc(this, IsisFish.config.getJavadocDirectory(), force, out);
        return result;
    }
    
    /**
     * Retourne la classe compilée. Compile le fichier si besoin.
     * 
     * @return la class
     * @throws IsisFishException
     */
    public <E> Class<E> getCodeClass() throws IsisFishException {
        String fqn = getFQN();
        compile(false, null);
        Class<E> result = (Class<E>)CompileHelper.loadClass(fqn);
        if (result == null) {
            throw new IsisFishException(String.format("Can't load class: %s", fqn));
        }
        return result;
    }

    /**
     * Retourne une nouvelle instance de la class. Compile le fichier si besoin.
     * 
     * @return une nouvelle instance de la class
     * @throws IsisFishException if can't make new instance
     */
    public <E> E getNewInstance() throws IsisFishException {
        Class<E> clazz = getCodeClass();
        E result;
        try {
            result = clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException eee) {
            throw new IsisFishException("Can't instanciate class: "
                    + clazz.getName(), eee);
        }
        return result;
    }

    protected static String getFieldDoc(Class klass, String paramName) throws SecurityException, NoSuchFieldException {
        String result = null;
        if (klass != null) {
            Field f = klass.getField(paramName);
            if (f != null) {
                Doc ann = f.getAnnotation(Doc.class);
                if (ann != null) {
                    result = ann.value();
                }
                fr.ifremer.isisfish.util.Doc annOld = f.getAnnotation(fr.ifremer.isisfish.util.Doc.class);
                if (annOld != null) {
                    result = annOld.value();
                }
            }
        }
        return result;
    }

    /**
     * Get doc of parameter.
     * 
     * @param object object containing parameter
     * @param paramName parameter name
     * @return {@link Doc} value
     */
    public static String getParameterDoc(Object object, String paramName) {
        String result = null;
        try {
            result = getFieldDoc(object.getClass(), PARAM_PREFIX + paramName);
        } catch (Exception e) {
            log.warn(String.format("could not found field %s doc for %s", paramName, object));
        }
        return result;
    }
    
    /**
     * Get docable element description.
     */
    public String getDescription() {
        return null;
    }
    
    /**
     * Recherche par introspection tous les parametres de la classe
     * commencant par {@link #PARAM_PREFIX}.
     *
     * @param instance the instance to inspect
     * @return the list of parameters for a given export
     */
    public static Map<String, Field> getParameterNamesAndField(Object instance) {
        Map<String, Field> result = new LinkedHashMap<>();
        for (Field field : instance.getClass().getFields()) {
            if (field.getName().startsWith(PARAM_PREFIX)) {
                result.put(field.getName().substring(PARAM_PREFIX.length()), field);
            }
        }
        return result;
    }

    /**
     * Donne la valeur d'un parametre par introspection.
     *
     * @param name le nom du parametre
     * @param instance the instance to inspect
     * @return la valeur courante du parametre
     * @throws IsisFishException if any exception
     */
    public static Object getParameterValue(Object instance, String name) throws IsisFishException {
        if (instance == null || name == null || "".equals(name)) {
            return null;
        }
        try {
            String fieldName = PARAM_PREFIX + name;
            Field field = instance.getClass().getDeclaredField(fieldName);
            return field.get(instance);
        } catch (IllegalAccessException | NoSuchFieldException eee) {
            throw new IsisFishException("Can't get rule parameter: " + name, eee);
        }
    }

    /**
     * Donne le type d'un paramametre par introspection.
     * 
     * Utilisé dans le cas de la définition de facteur d'analyse de sensibilité
     * sur les parametres de regles.
     * 
     * @param instance intance to inspect
     * @param name parameter name
     * @return parameter type
     * @throws IsisFishException if parameter doesn't exists
     */
    public static Class getParameterType(Object instance, String name) throws IsisFishException {
        if (instance == null || name == null || "".equals(name)) {
            return null;
        }
        try {
            String fieldName = PARAM_PREFIX + name;
            Field field = instance.getClass().getDeclaredField(fieldName);
            return field.getType();
        } catch (NoSuchFieldException eee) {
            throw new IsisFishException("Can't get rule parameter: " + name, eee);
        }
    }

    /**
     * Modifie la valeur d'un attribut par introspection.
     *
     * @param name  le nom de l'attribut
     * @param value la valeur de l'attribut
     * @param instance the instance to inspect
     * @throws IsisFishException if any exception
     */
    public static void setParameterValue(Object instance, String name,
            Object value) throws IsisFishException {
        try {
            String fieldName = PARAM_PREFIX + name;
            Field field = instance.getClass().getDeclaredField(fieldName);
            field.set(instance, value);
        } catch (IllegalAccessException | NoSuchFieldException | IllegalArgumentException eee) {
            throw new IsisFishException("Can't modify script parameter: "
                    + name + " with '" + value + "'("
                    + ObjectUtils.identityToString(value) + ")", eee);
        }
    }
    
    /**
     * Recupere les parametres et leur valeur pour les retourner sous forme
     * de chaine. Pour pouvoir par exemple les afficher a l'utilisateur.
     *
     * @param instance the instance to inspect
     * @return a string representation of parameters and their values for
     *         a given rule
     * @throws IsisFishException if any exception
     */
    public static String getParamAsString(Object instance) throws IsisFishException {
        StringBuilder result = new StringBuilder();

        for (String name : getParameterNamesAndField(instance).keySet()) {
            Object value = getParameterValue(instance, name);

            result.append(name).append(" : ").append(value);
            result.append("\n");
        }
        return result.toString();
    }

    /**
     * Clone une instance en recopiant manuellement les champs "param_".
     */
    public static Object clone(Object instance) {
        Object clone;
        try {
            clone = BeanUtils.cloneBean(instance);
            for (Field field : instance.getClass().getFields()) {
                if (field.getName().startsWith(PARAM_PREFIX)) {
                    field.set(clone, field.get(instance));
                }
            }
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException | NoSuchMethodException e) {
            throw new IsisFishRuntimeException("Can't clone object", e);
        }
        return clone;
    }
}
