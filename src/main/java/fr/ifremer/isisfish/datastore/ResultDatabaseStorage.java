/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2015 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.datastore;

import static org.nuiton.i18n.I18n.t;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import fr.ifremer.isisfish.entities.Equation;
import org.apache.commons.collections4.map.AbstractReferenceMap.ReferenceStrength;
import org.apache.commons.collections4.map.ReferenceMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixFactory;
import org.nuiton.math.matrix.MatrixIterator;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.util.ArrayUtil;
import org.nuiton.util.HashList;

import fr.ifremer.isisfish.IsisFishDAOHelper;
import fr.ifremer.isisfish.IsisFishException;
import fr.ifremer.isisfish.entities.ActiveRule;
import fr.ifremer.isisfish.entities.ActiveRuleDAO;
import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.entities.Result;
import fr.ifremer.isisfish.entities.ResultDAO;
import fr.ifremer.isisfish.export.ExportInfo;
import fr.ifremer.isisfish.export.SensitivityExport;
import fr.ifremer.isisfish.rule.Rule;
import fr.ifremer.isisfish.simulator.Objective;
import fr.ifremer.isisfish.simulator.Optimization;
import fr.ifremer.isisfish.simulator.SimulationContext;
import fr.ifremer.isisfish.simulator.SimulationException;
import fr.ifremer.isisfish.simulator.SimulationPlan;
import fr.ifremer.isisfish.simulator.SimulationResultGetter;
import fr.ifremer.isisfish.types.TimeStep;

/**
 * Cette classe permet de conserver des résultats de simulation. Elle permet
 * ensuite de les récupérer.
 * 
 * Created: 29 sept. 2004
 *
 * @author Benjamin Poussin &lt;poussin@codelutin.com&gt;
 * @version $Revision$
 *
 * Mise a jour: $Date$
 * par : $Author$
 *
 * @deprecated {@link ResultStorageCSV} replace this implementation. This class
 * will be remove in 5.0.0.0
 */
@Deprecated
public class ResultDatabaseStorage implements SimulationResultGetter, ResultStorage { // ResultDatabaseStorage

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(ResultDatabaseStorage.class);

    protected SimulationStorage simulation = null;
    // transient protected HashMap<String, MatrixND> globalMatrix = new HashMap<String, MatrixND>();
    transient protected ReferenceMap<String, TopiaContext> cacheContext = new ReferenceMap<>(ReferenceStrength.HARD, ReferenceStrength.WEAK);

    /** cache to maintains some result. key: String(date + ':' + name), value: matrix
     * TODO: cache will be more efficient if it keep at min the number of result by year */
    transient protected ReferenceMap<String, MatrixND> cache = new ReferenceMap<>(ReferenceStrength.HARD, ReferenceStrength.SOFT);
    /** contains all available result as string: String(date + ':' + name) */
    transient protected Set<String> availableResult = null;
    /** result enabled */
    transient protected Set<String> enabledResult = null;

    /**
     * Les ResultStorage ne doivent pas etre instancier directement, mais
     * recuperer a partir d'un
     * {@link fr.ifremer.isisfish.datastore.SimulationStorage#getResultStorage()}
     * 
     * @param simulation storage to get result
     */
    public ResultDatabaseStorage(SimulationStorage simulation) {
        this.simulation = simulation;
    }

    @Override
    public String getInfo() {
        return "ResultDatabaseStorage no more info.";
    }

    @Override
    public void delete() {
        TopiaContext tx = null;
        boolean mustClose = false;

        try {
            if (simulation == SimulationContext.get().getSimulationStorage()) {
                tx = SimulationContext.get().getDbResult();
            }
    
            if (tx == null) {
                // not in simulation, create transaction
                tx = simulation.getStorage().beginTransaction();
                mustClose = true;
            }
    
            tx.execute("DELETE " + Result.class.getName());
            if (mustClose) {
                tx.closeContext();
            }
        } catch (Exception ex) {
            if (log.isErrorEnabled()) {
                log.error("Can't delete results", ex);
            }
        }
    }

    @Override
    public void close() {
        // do nothing, closed by simulation storage closing
    }

    protected void putInCache(TimeStep step, String name, MatrixND mat, TopiaContext context) {
        String key = step.getStep() + ":" + name;
        putInCache(key, mat, context);
    }

    protected void putInCache(String name, MatrixND mat, TopiaContext context) {
        if (mat != null) {
            cache.put(name, mat);
            cacheContext.put(name, context);
        }
    }

    /**
     * Result can be removed from cache when computed result (from multiple step)
     * become invalidated by new step result.
     * 
     * @param name result name to remove
     */
    protected void removeInCache(String name) {
        cache.remove(name);
        cacheContext.remove(name);
    }

    protected MatrixND getInCache(TimeStep step, String name) {
        // do not use toString for step (depend on i18n)
        String key = step.getStep() + ":" + name;
        MatrixND result = getInCache(key);
        return result;
    }

    protected MatrixND getInCache(String name) {
        MatrixND result = null;
        TopiaContext context = cacheContext.get(name);
        if (context != null && !context.isClosed()) {
            // on verifie que le context existe encore car on peut vouloir
            // naviguer dans les semantics
            result = cache.get(name);
        }
        return result;
    }

    /**
     * Retourne le nom de tous les resultats disponibles le nom est constitué
     * de la date et du nom du resultat.
     * 
     * @return available results
     */
    protected Set<String> getAvailableResult() {
        if (availableResult == null) {
            availableResult = new HashSet<>();
            try {
                TopiaContext tx = null;
                boolean mustClose = false;

                if (simulation == SimulationContext.get().getSimulationStorage()) {
                    tx = SimulationContext.get().getDbResult();
                }

                if (tx == null) {
                    // not in simulation, create transaction
                    tx = simulation.getStorage().beginTransaction();
                    mustClose = true;
                }

                List<String> result = (List<String>) tx.findAll("Select " + Result.PROPERTY_RESULT_STEP + "||':'||name from fr.ifremer.isisfish.entities.Result");
                if (mustClose) {
                    tx.closeContext();
                }
                availableResult.addAll(result);
            } catch (Exception eee) {
                if (log.isWarnEnabled()) {
                    log.warn("Can't get result available", eee);
                }
            }
        }
        return availableResult;
    }

    /**
     * Verifie si un resultat est disponible pour une date donnée.
     * 
     * @param step
     * @param name
     * @return {@code true} if result is available
     */
    protected boolean isAvailableResult(TimeStep step, String name) {
        String key = step.getStep() + ":" + name;
        boolean result = getAvailableResult().contains(key);
        return result;
    }

    /**
     * Ajoute un resultat comme etant disponible pour une date donnée.
     * 
     * @param step
     * @param name
     */
    protected void addAvailableResult(TimeStep step, String name) {
        String key = step.getStep() + ":" + name;
        getAvailableResult().add(key);
    }

    /* @deprecated since 4.4.0.0 : this method is duplicated with ResultManager#isEnabled(String)
     */
    @Override
    public boolean isEnabled(String name) {
        name = name.trim();
        if (enabledResult == null) {
            enabledResult = new HashSet<>();

            Collection<String> resultEnabled = simulation.getParameter()
                    .getResultEnabled();
            enabledResult.addAll(resultEnabled);

            // test on export
            List<String> exportNames = simulation.getParameter().getExportNames();
            if (exportNames != null) {
                for (String exportName : exportNames) {
                    ExportStorage storage = ExportStorage.getExport(exportName);
                    try {
                        ExportInfo export = storage.getNewInstance();
                        enabledResult.addAll(Arrays.asList(export.getNecessaryResult()));
                    } catch (IsisFishException eee) {
                        if (log.isWarnEnabled()) {
                            log.warn(t("isisfish.error.instanciate.export",
                                            exportName), eee);
                        }
                    }
                }
            }

            // test on sensitivity export
            List<SensitivityExport> sensitivityExports = simulation
                    .getParameter().getSensitivityExport();
            if (sensitivityExports != null) {
                for (SensitivityExport sensitivityExport : sensitivityExports) {
                    enabledResult.addAll(Arrays.asList(sensitivityExport.getNecessaryResult()));
                }
            }

            // test on rules
            List<Rule> rules = simulation.getParameter().getRules();
            if (rules != null) {
                for (Rule rule : rules) {
                    enabledResult.addAll(Arrays.asList(rule.getNecessaryResult()));
                }
            }

            // test on plans
            List<SimulationPlan> plans = simulation.getParameter().getSimulationPlans();
            if (plans != null) {
                for (SimulationPlan plan : plans) {
                    enabledResult.addAll(Arrays.asList(plan.getNecessaryResult()));
                }
            }

            // on objective and optimization
            Objective objective = simulation.getParameter().getObjective();
            if (objective != null) {
                enabledResult.addAll(Arrays.asList(objective.getNecessaryResult()));
            }

            Optimization optimization = simulation.getParameter().getOptimization();
            if (optimization != null) {
                enabledResult.addAll(Arrays.asList(optimization.getNecessaryResult()));
            }

            // equation results
            TopiaContext tx = null;
            boolean mustClose = false;
            if (simulation == SimulationContext.get().getSimulationStorage()) {
                tx = SimulationContext.get().getDbResult();
            }
            if (tx == null) {
                // not in simulation, create transaction
                tx = simulation.getStorage().beginTransaction();
                mustClose = true;
            }
            List<Equation> equations = IsisFishDAOHelper.getEquationDAO(tx).findAll();
            for (Equation e : equations) {
                enabledResult.addAll(Arrays.asList(e.evaluateNecessaryResult()));
            }
            if (mustClose) {
                tx.closeContext();
            }

            log.info("Enabled result: " + enabledResult);
        }
        boolean result = enabledResult.contains(name);
        return result;
    }

    @Override
    public void addResult(TimeStep step, MatrixND mat) throws IsisFishException {
        addResult(false, step, mat.getName(), mat);
    }

    @Override
    public void addResult(TimeStep step, Population pop, MatrixND mat) throws IsisFishException {
        addResult(false, step, mat.getName(), pop, mat);
    }

    @Override
    public void addResult(boolean force, TimeStep step, MatrixND mat) throws IsisFishException {
        addResult(force, step, mat.getName(), mat);
    }

    @Override
    public void addResult(boolean force, TimeStep step, Population pop, MatrixND mat) throws IsisFishException {
        addResult(force, step, mat.getName(), pop, mat);
    }

    @Override
    public void addResult(TimeStep step, String name, Population pop, MatrixND mat) throws IsisFishException {
        addResult(false, step, name, pop, mat);
    }

    @Override
    public void addResult(TimeStep step, String name, MatrixND mat) throws IsisFishException {
        addResult(false, step, name, mat);
    }

    @Override
    public void addResult(boolean force, TimeStep step, String name, Population pop, MatrixND mat) throws IsisFishException {
        if (force || isEnabled(name)) {
            doAddResult(step, name + " " + pop, mat);
        }
    }

    @Override
    public void addResult(boolean force, TimeStep step, String name, MatrixND mat) throws IsisFishException {
        if (force || isEnabled(name)) {
            doAddResult(step, name, mat);
        }
    }

    protected void doAddResult(TimeStep step, String name, MatrixND mat) throws IsisFishException {
        try {
            TopiaContext tx = null;
            boolean mustClose = false;

            if (simulation == SimulationContext.get().getSimulationStorage()) {
                tx = SimulationContext.get().getDbResult();
            }
            if (tx == null) {
                // not in simulation, create transaction
                tx = simulation.getStorage().beginTransaction();
                mustClose = true;
            }
            doAddResult(step, name, mat, tx);
            if (mustClose) {
                tx.commitTransaction();
                tx.closeContext();
            }
        } catch (TopiaException eee) {
            log.warn("Can't add result '" + name + "' at step " + step, eee);
        }
    }

    protected void doAddResult(TimeStep step, String name, MatrixND mat,
            TopiaContext tx) throws IsisFishException {
        // si la matrice n'a pas de semantique on refuse
        for (int i = 0; i < mat.getDimCount(); i++) {
            // la semantique n'est pas bonne des qu'il y a un null dedans
            if (mat.getSemantic(i).contains(null)) {
                throw new SimulationException(
                        "Erreur le résultat que vous souhaitez enregistrer n'a pas d'information convenable pour la dimension: "
                                + i + " " + mat.getDimensionName(i));
            }
        }

        // on fait une copie pour avoir reellement des resultats independant
        // FIXME echatellier 20120829 : faire une copie optimiser
        // suivant l'implementation du vector plutot qu'un parcourt
        // via un iterateur de semantiques (plus couteux)
        MatrixND newMat = mat.copy();
        try {
            ResultDAO resultPS = IsisFishDAOHelper.getResultDAO(tx);
            Result result = resultPS.create();
            result.setResultStep(step);
            result.setName(name);
            result.setMatrix(newMat);
            resultPS.update(result);

            addAvailableResult(step, name);
            putInCache(step, name, newMat, tx);

            // depuis isis fish 4.1.1, le commit est automatique à chaque
            // résultat (gain de performance de 20% avec le clearCache)
            tx.commitTransaction();
            // vide le cache hibernate. Sans cela, les resultats ne sont
            // jamais supprimé du cache car la session n'est close
            // qu'à la fin de la simulation
            tx.clearCache();

            // remove from cache result computed by #getMatrix(name)
            // beacause, if a new step is added, result become false
            // but DO NOT REMOVE result named step + name
            removeInCache(name);

        } catch (TopiaException eee) {
            log.warn("Can't add result '" + name + "' at step " + step, eee);
        }
    }

    @Override
    public void addActiveRule(TimeStep step, Rule rule) throws IsisFishException {
        try {
            TopiaContext tx = null;
            boolean mustClose = false;

            if (simulation == SimulationContext.get().getSimulationStorage()) {
                tx = SimulationContext.get().getDbResult();
            }
            if (tx == null) {
                // not in simulation, create transaction
                tx = simulation.getStorage().beginTransaction();
                mustClose = true;
            }
            ActiveRuleDAO ps = IsisFishDAOHelper.getActiveRuleDAO(tx);
            ActiveRule result = ps.create();
            result.setActiveRuleStep(step);
            result.setName(RuleStorage.getName(rule));
            result.setParam(RuleStorage.getParamAsString(rule));
            ps.update(result);
            if (mustClose) {
                tx.commitTransaction();
                tx.closeContext();
            }
        } catch (TopiaException eee) {
            throw new IsisFishException("Can't add result", eee);
        }
    }

    @Override
    public List<String> getResultName() {

        List<String> result = null;
        try {
            TopiaContext tx = null;
            boolean mustClose = false;

            if (simulation == SimulationContext.get().getSimulationStorage()) {
                tx = SimulationContext.get().getDbResult();
            }
            if (tx == null) {
                // not in simulation, create transaction
                tx = simulation.getStorage().beginTransaction();
                mustClose = true;
            }
            ResultDAO resultPS = IsisFishDAOHelper.getResultDAO(tx);

            result = (List<String>) resultPS.getContext()
                    .findAll("Select distinct name from fr.ifremer.isisfish.entities.Result order by name");
            if (mustClose) {
                tx.closeContext();
            }
        } catch (TopiaException eee) {
            if (log.isWarnEnabled()) {
                log.warn("Can't get result name", eee);
            }
        }
        if (result == null) {
            result = new ArrayList<>();
        }
        return result;
    }

    @Override
    public MatrixND getMatrix(TimeStep step, Population pop, String name) {
        String newName = name + " " + pop;
        return getMatrix(step, newName);
    }

    @Override
    public MatrixND getMatrix(TimeStep step, String name) {
        MatrixND mat = getInCache(step, name);
        if (mat == null && isAvailableResult(step, name)) {
            try {
                TopiaContext tx = null;
                boolean mustClose = false;

                if (simulation == SimulationContext.get().getSimulationStorage()) {
                    tx = SimulationContext.get().getDbResult();
                }
                if (tx == null) {
                    // not in simulation, create transaction
                    tx = simulation.getStorage().beginTransaction();
                    mustClose = true;
                }
                mat = getMatrix(step, name, tx);
                if (mustClose) {
                    // FIXME transaction never closed
                    // quand peut on fermer la transaction ?
                    // lorsque plus aucune matrice ne l'utilise.
                    // donc mettre la matrice et la connexion dans une map
                    // la matrice dans une weak reference. Des que la matrice
                    // est liberer faire un close sur la transaction
                } else {
                    // echatellier 20120829 : vidage du cache hibernate
                    // sinon on fait trop de lecture que la meme transaction
                    // et le cache hibernate se remplit sans se vider
                    //tx.clearCache();
                }
            } catch (Exception eee) {
                if (log.isWarnEnabled()) {
                    log.warn("Can't return matrix '" + name + "' for step "
                            + step, eee);
                }
            }
        }
        return mat;
    }

    @Override
    public MatrixND getMatrix(TimeStep step, String name, TopiaContext tx) {
        MatrixND mat = getInCache(step, name);
        if (mat == null && isAvailableResult(step, name)) {
            try {
                ResultDAO resultPS = IsisFishDAOHelper.getResultDAO(tx);
                Result result = resultPS.findByProperties(
                        Result.PROPERTY_RESULT_STEP, step,
                        Result.PROPERTY_NAME, name);
                if (result != null) {
                    mat = result.getMatrix();
                    putInCache(step, name, mat, tx);
                }
            } catch (Exception eee) {
                if (log.isWarnEnabled()) {
                    log.warn("Can't return matrix '" + name + "' for step "
                            + step, eee);
                }
            }
        }
        return mat;
    }

    @Override
    public MatrixND getMatrix(Population pop, String name) {
        String newName = name + " " + pop;
        return getMatrix(newName);
    }

    @Override
    public MatrixND getMatrix(Population pop, String name, TopiaContext tx) {
        String newName = name + " " + pop;
        return getMatrix(newName, tx);
    }

    @Override
    public MatrixND getMatrix(String name) {
        MatrixND resultMat = null;
        try {
            TopiaContext tx = null;
            boolean mustClose = false;

            if (simulation == SimulationContext.get().getSimulationStorage()) {
                tx = SimulationContext.get().getDbResult();
            }
            if (tx == null) {
                // not in simulation, create transaction
                tx = simulation.getStorage().beginTransaction();
                mustClose = true;
            }
            resultMat = getMatrix(name, tx);
            if (mustClose) {
                // FIXME transaction never closed
                // quand peut on fermer la transaction ?
                // lorsque plus aucune matrice ne l'utilise.
                // donc mettre la matrice et la connexion dans une map
                // la matrice dans une weak reference. Des que la matrice
                // est liberer faire un close sur la transaction
            } else {
                // echatellier 20120829 : vidage du cache hibernate
                // sinon on fait trop de lecture que la meme transaction
                // et le cache hibernate se remplit sans se vider
                //tx.clearCache();
            }
        } catch (TopiaException eee) {
            if (log.isWarnEnabled()) {
                log.warn("Can't get result: " + name, eee);
            }
        }
        return resultMat;
    }

    @Override
    public MatrixND getMatrix(String name, TopiaContext tx) {
        log.debug("Get result: " + name);

        MatrixND resultMat = getInCache(name);
        if (resultMat != null) {
            return resultMat;
        }

        // recuperation des resultats qui nous interesse
        List<Result> results = null;
        try {
            ResultDAO resultPS = IsisFishDAOHelper.getResultDAO(tx);
            results = resultPS.findAllByName(name);
        } catch (TopiaException eee) {
            if (log.isWarnEnabled()) {
                log.warn("Can't get result: " + name, eee);
            }
        }

        // s'il n'y pas de resultat, on retourne null
        if (results == null || results.size() == 0) {
            return null;
        }

        // Creation des listes pour chaque dimension

        // creation de la liste de date
        TimeStep lastStep = getLastStep();
        List<TimeStep> steps = new ArrayList<>();
        TimeStep step = new TimeStep(0);
        steps.add(step);
        while (step.before(lastStep)) {
            step = step.next();
            steps.add(step);
        }

        if (log.isTraceEnabled()) {
            log.trace("Steps list : " + steps);
        }

        // recuperation des dimensions des matrices
        MatrixND mat = results.get(0).getMatrix();

        // recuperation des noms des dimensions
        String[] dimNames = new String[1 + mat.getDimCount()];
        dimNames[0] = t("isisfish.common.date");
        for (int i = 1; i < dimNames.length; i++) {
            dimNames[i] = mat.getDimensionName(i - 1);
        }

        // creation de la semantique pour la matrice resultat. +1 pour les dates
        List[] sem = new List[1 + mat.getDimCount()];
        sem[0] = steps;

        for (int i = 1; i < sem.length; i++) {
            sem[i] = new HashList();
        }

        for (Result result : results) {
            MatrixND mattmp = result.getMatrix();
            if (log.isTraceEnabled()) {
                log.trace("Ajout de la semantics: "
                        + Arrays.asList(mattmp.getSemantics()));
            }

            for (int s = 0; s < mattmp.getDimCount(); s++) {
                sem[s + 1].addAll(mattmp.getSemantic(s));
            }
        }

        if (log.isTraceEnabled()) {
            log.trace("La semantique final est: " + Arrays.asList(sem));
        }

        // creation de la matrice resultat
        resultMat = MatrixFactory.getInstance().create(name, sem, dimNames);

        // recuperation du resultat pour chaque date de la simulation, de Date(0) à lastDate
        for (Result result : results) {
            TimeStep d = result.getResultStep();
            mat = result.getMatrix();
            // on met ce resultat dans la matrice result si besoin
            if (mat != null) {
                // on recupere dans la matrice resultat l'endroit on il faut
                // mettre la matrice
                MatrixND submat = resultMat.getSubMatrix(0, d, 1);
                // on met les valeur de mat dans la sous matrice extraite
                for (MatrixIterator mi = mat.iteratorNotZero(); mi.next();) {
                    submat.setValue(
                            ArrayUtil.concat(new Object[] { d },
                                    mi.getSemanticsCoordinates()), mi.getValue());
                }
            }
        }

        putInCache(name, resultMat, tx);
        return resultMat;
    }

    @Override
    public TimeStep getLastStep() {
        int monthNumber = simulation.getParameter().getNumberOfMonths();
        TimeStep result = new TimeStep(monthNumber - 1); // -1 because date begin at 0
        return result;
    }

    @Override
    public void addResult(SimulationContext context, TimeStep step, String name,
            MatrixND mat) throws IsisFishException {
        try {
            doAddResult(step, name, mat, context.getDbResult());
        } catch (TopiaException eee) {
            log.warn(t("Can't add result '%1$s' at date %2$s", name, step), eee);
        }
    }

    @Override
    public MatrixND getMatrix(SimulationContext context, TimeStep step, String name) {
        MatrixND result = null;
        try {
            result = getMatrix(step, name, context.getDbResult());
        } catch (TopiaException eee) {
            if (log.isWarnEnabled()) {
                log.warn(t("Can't get result: %1$s", name), eee);
            }
        }
        return result;
    }

    @Override
    public MatrixND getMatrix(SimulationContext context, String name) {
        MatrixND result = null;
        try {
            result = getMatrix(name, context.getDbResult());
        } catch (TopiaException eee) {
            if (log.isWarnEnabled()) {
                log.warn(t("Can't get result: %1$s", name), eee);
            }
        }
        return result;
    }

    @Override
    public void afterSimulation(SimulationContext context) {
    }

    @Override
    public void beforeSimulation(SimulationContext context) {
    }

    @Override
    public void stepChange(SimulationContext context, TimeStep step) {
    }

    // public void addActivatedRule(ResultStorage self, Date date, RegleParam rule){
    //     List rules = (List)activatedRules.get(date);
    //     if(rules == null){
    //         activatedRules.put(date, rules = new LinkedList());
    //     }
    //     rules.add(rule);
    // }

    // /**
    // * Retourne pour une date données tous les RegleParam qui ont été activé
    // * a la date demandé.
    // * @return une list de {@link fr.ifremer.nodb.RegleParam}
    // */
    // public List getActivatedRule(ResultStorage self, Date date){
    //     List rules = (List)activatedRules.get(date);
    //     if(rules == null){
    //         activatedRules.put(date, rules = new LinkedList());
    //     }
    //     return rules;
    // }

    @Override
    public MatrixND getMatrix(SimulationContext context, List<TimeStep> steps, String name) {
        MatrixND result = null;
        try {
            result = getMatrix(steps, name, context.getDbResult());
        } catch (TopiaException eee) {
            if (log.isWarnEnabled()) {
                log.warn(t("Can't get result: %1$s", name), eee);
            }
        }
        return result;
    }

    @Override
    public MatrixND getMatrix(TimeStep step, Population pop, String name, TopiaContext tx) {
        String newName = name + " " + pop;
        return getMatrix(step, newName, tx);
    }

    @Override
    public MatrixND getMatrix(List<TimeStep> steps, Population pop, String name) {
        String newName = name + " " + pop;
        return getMatrix(steps, newName, null);
    }

    @Override
    public MatrixND getMatrix(List<TimeStep> steps, Population pop, String name, TopiaContext tx) {
        String newName = name + " " + pop;
        return getMatrix(steps, newName, tx);
    }

    @Override
    public MatrixND getMatrix(List<TimeStep> steps, String name) {
        return getMatrix(steps, name, null);
    }

    @Override
    public MatrixND getMatrix(List<TimeStep> steps, String name, TopiaContext tx) {
        MatrixND result = getMatrix(name, tx);
        result = result.getSubMatrix(0, steps.toArray());
        return result;
    }

} // ResultDatabaseStorage
