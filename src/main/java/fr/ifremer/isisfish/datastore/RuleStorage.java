/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2005 - 2019 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.datastore;

import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.annotations.Doc;
import fr.ifremer.isisfish.rule.Rule;
import fr.ifremer.isisfish.vcs.VCSException;
import org.apache.commons.collections4.map.ReferenceMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

import static org.nuiton.i18n.I18n.t;

/**
 * Class permettant de representer une regle de gestion.
 * Une regle de gestion est un fichier Java que l'on compile si besoin.
 * Il se trouve dans le package portant le nom de la region de cette maniere
 * plusieurs region peuvent avoir des regles avec le meme nom mais pas le
 * meme code.
 * <p>
 * Gere les fichiers VCS de type {@link Rule} (package rules)
 *
 * Created: 17 août 2005 11:11:51 CEST
 *
 * @author Benjamin POUSSIN &lt;poussin@codelutin.com&gt;
 *
 * @version $Revision$
 *
 * Last update: $Date$ by : $Author$
 */
public class RuleStorage extends JavaSourceStorage { // RulesStorage

    /** to use log facility, just put in your code: log.info(\"...\"); */
    private static Log log = LogFactory.getLog(RuleStorage.class);
    
    public static final String RULE_PATH = "rules";

    /** Template freemarker pour les regles. */
    public static final String RULE_TEMPLATE = "templates/script/rule.ftl";

    /** Instance cache. */
    protected static Map<String, RuleStorage> rulesCache = new ReferenceMap<>();

    protected static Set<StorageChangeListener> storageListeners = Collections.newSetFromMap(new WeakHashMap<>());

    public static void addStorageListener(StorageChangeListener storageListener) {
        storageListeners.add(storageListener);
    }

    /**
     * Contruit un nouveau rule storage
     *
     * @param rootSrc   La region auquelle est attaché la rule
     * @param directory le repertoire ou devrait se trouver la rule
     * @param name      le nom de la rule
     */
    protected RuleStorage(File rootSrc, File directory, String name) {
        super(rootSrc, directory, name);
    }

    public static void registerWatcher() {
        registerWatcher(storageListeners, getRuleDirectory(), getCommunityRuleDirectory());
    }

    @Override
    public String getPackage() {
        return RULE_PATH;
    }

    /**
     * Get context (official VCS) rule directory.
     * 
     * @return context rule directory
     */
    static public File getRuleDirectory() {
        File result = new File(getContextDatabaseDirectory(), RULE_PATH);
        result.mkdirs();
        return result;
    }

    /**
     * Get community VCS rule directory.
     * 
     * @return community rule directory
     */
    public static File getCommunityRuleDirectory() {
        File result = new File(getCommunityDatabaseDirectory(), RULE_PATH);
        result.mkdirs();
        return result;
    }

    /**
     * Retourne le nom de toutes les regles existantes pour cette region
     *
     * @return all rule names found in local user database
     */
    static public List<String> getRuleNames() {
        List<String> rules = getStorageNames(getRuleDirectory());
        rules.addAll(getStorageNames(getCommunityRuleDirectory()));
        return rules;
    }

    /**
     * Retourne le storage pour la regle demandée
     *
     * @param name le nom de la regle souhaitée
     * @param location location to open storage file
     * @return Le storage pour la regle
     */
    static public RuleStorage getRule(String name, Location... location) {
        RuleStorage result = rulesCache.get(name);
        if (result == null) {
            Location loc = nonEmptyLocation(location);
            for (File dir : loc.getDirectories()) {
                RuleStorage storage = new RuleStorage(dir, new File(dir, RULE_PATH), name);
                File sFile = storage.getFile();
                if (sFile.isFile()) {
                    result = storage;
                    rulesCache.put(name, result);
                }
            }
        }
        return result;
    }

    /**
     * Create new rule.
     * 
     * @param name new rule to create
     * @param location location to rule storage file
     * @return new rule storage
     */
    public static RuleStorage createRule(String name, Location location) {
        File dir = location.getDirectories()[0];
        RuleStorage storage = new RuleStorage(dir, new File(dir, RULE_PATH), name);
        return storage;
    }

    static public void checkout() throws VCSException {
        checkout(IsisFish.config.getDatabaseDirectory(), RULE_PATH);
    }

    /**
     * Retourne la liste des noms de toutes les régions disponible en local qui
     * ne sont pas encore sur le serveur VCS
     *
     * @return liste de noms de regions
     */
    static public List<String> getNewRuleNames() {
        List<String> result = getRuleNames();
        result.removeAll(getRemoteRuleNames());
        return result;
    }

    /**
     * Retourne la liste des noms de toutes les régions disponible sur le
     * serveur VCS
     *
     * @return la liste des noms de toutes les régions disponible sur le serveur
     *         VCS. Si le serveur n'est pas disponible la liste retournée est
     *         vide.
     */
    static public List<String> getRemoteRuleNames() {
        File dir = getRuleDirectory();
        return getRemoteStorageNames(dir);
    }

    /**
     * Retourne la liste des noms de toutes les régions disponible sur le
     * serveur VCS qui ne sont pas encore en local
     *
     * @return liste de noms de regions
     */
    static public List<String> getNewRemoteRuleNames() {
        List<String> result = getRemoteRuleNames();
        result.removeAll(getRuleNames());
        return result;
    }

    /**
     * <b>Be ware this method require to instanciate a Rule, so
     * it would be better to call as often as possible.</b>
     *
     * @return the description of the instanciate Rule
     * @see Doc
     */
    public String getDescription() {
        String result = null;
        try {
            Rule rule = getNewInstance();
            result = rule == null ? null : rule.getDescription();
        } catch (Exception e) {
            log.warn(t("isisfish.error.not.found.description",this));
        }
        return result;
    }

} // RuleStorage

