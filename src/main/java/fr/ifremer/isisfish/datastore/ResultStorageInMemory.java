/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2011 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.datastore;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.topia.TopiaContext;
import fr.ifremer.isisfish.types.TimeStep;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * Cette classe permet de conserver des résultats de simulation. Elle permet
 * ensuite de les récupérer. Les résultats sont stockés en mémoire.
 * 
 * Created: 29 sept. 2004
 *
 * @author Benjamin Poussin : poussin@codelutin.com
 * @version $Revision$
 *
 * Mise a jour: $Date$
 * par : $Author$
 *
 * @deprecated ResultStorageCSV now support no storage and in memory usage, will be removed in version 5.0.0.0
 */
@Deprecated
public class ResultStorageInMemory extends ResultStorageAbstract {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(ResultStorageInMemory.class);

    final static public String MAX_TIME_STEP = "ResultStorageInMemory.maxTimeStep";
    protected int maxTimeStep = 12; // default to 1 year

    LinkedHashMap<TimeStep, Map<String, MatrixND>> data = new LinkedHashMap<TimeStep, Map<String, MatrixND>>() {
        private static final long serialVersionUID = 1L;
        @Override
        protected boolean removeEldestEntry(Map.Entry<TimeStep, Map<String, MatrixND>> eldest) {
            return size() > maxTimeStep;
        }
    };

    LinkedHashMap<TimeStep, List<String>> rules = new LinkedHashMap<TimeStep, List<String>>() {
        private static final long serialVersionUID = 1L;
        @Override
        protected boolean removeEldestEntry(Map.Entry<TimeStep, List<String>> eldest) {
            return size() > maxTimeStep;
        }
    };

    /**
     * Les ResultStorage ne doivent pas etre instancier directement, mais
     * recuperer a partir d'un
     * {@link fr.ifremer.isisfish.datastore.SimulationStorage#getResultStorage()}
     * 
     * @param simulation storage to get result
     */
    public ResultStorageInMemory(SimulationStorage simulation) {
        super(simulation);
        maxTimeStep = Integer.parseInt(simulation.getParameter().getTagValue().get(MAX_TIME_STEP));
    }

    @Override
    public void delete() {
        super.delete();
        data.clear();
    }

    @Override
    protected MatrixND decorate(MatrixND mat, TopiaContext tx) {
        // for in memory do nothing, matrice has already semantics (not string)
        return mat;
    }

    @Override
    protected void writeResult(TimeStep step, String name, MatrixND mat) {
        MatrixND newMat = undecorate(mat);
        Map<String, MatrixND> mats = data.computeIfAbsent(step, k -> new HashMap<>());

        mats.put(name, newMat);
   }

    @Override
    protected void writeActiveRule(TimeStep step, String name, String params) {
        List<String> list = rules.computeIfAbsent(step, k -> new LinkedList<>());
        list.add(name);
    }

    @Override
    public List<String> getResultName() {

        Set<String> result = new HashSet<>();

        for (Map<String, MatrixND> mats : data.values()) {
            result.addAll(mats.keySet());
        }

        return new ArrayList<>(result);
    }

    @Override
    public MatrixND readResult(TimeStep step, String name) {
        MatrixND result = null;
        Map<String, MatrixND> mats = data.get(step);
        if (mats != null) {
            result = mats.get(name);
        }
        return result;
    }

}
