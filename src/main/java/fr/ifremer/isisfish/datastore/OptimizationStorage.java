/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.datastore;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

import org.apache.commons.collections4.map.ReferenceMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.vcs.VCSException;

/**
 * Class permettant de gérer les fonctions d'objectif des optimisations.
 */
public class OptimizationStorage extends JavaSourceStorage { // RulesStorage

    /** to use log facility, just put in your code: log.info(\"...\"); */
    private static Log log = LogFactory.getLog(OptimizationStorage.class);
    
    public static final String OPTIMIZATION_PATH = "optimizations";

    /** Template freemarker pour les regles. */
    public static final String OPTIMIZATION_TEMPLATE = "templates/script/optimization.ftl";

    /** Instance cache. */
    protected static Map<String, OptimizationStorage> objectiveCache = new ReferenceMap<>();

    protected static Set<StorageChangeListener> storageListeners = Collections.newSetFromMap(new WeakHashMap<>());

    public static void addStorageListener(StorageChangeListener storageListener) {
        storageListeners.add(storageListener);
    }

    /**
     * Contruit un nouveau storage
     *
     * @param rootSrc   La region auquelle est attaché la rule
     * @param directory le repertoire ou devrait se trouver la rule
     * @param name      le nom de la rule
     */
    protected OptimizationStorage(File rootSrc, File directory, String name) {
        super(rootSrc, directory, name);
    }

    public static void registerWatcher() {
        registerWatcher(storageListeners, getOptimizationDirectory(), getCommunityOptimizationDirectory());
    }

    @Override
    public String getPackage() {
        return OPTIMIZATION_PATH;
    }

    /**
     * Get context (official VCS) rule directory.
     * 
     * @return context rule directory
     */
    static public File getOptimizationDirectory() {
        File result = new File(getContextDatabaseDirectory(), OPTIMIZATION_PATH);
        result.mkdirs();
        return result;
    }

    /**
     * Get community VCS rule directory.
     * 
     * @return community rule directory
     */
    public static File getCommunityOptimizationDirectory() {
        File result = new File(getCommunityDatabaseDirectory(), OPTIMIZATION_PATH);
        result.mkdirs();
        return result;
    }

    /**
     * Retourne le nom de toutes les regles existantes pour cette region
     *
     * @return all rule names found in local user database
     */
    static public List<String> getOptimizationNames() {
        List<String> rules = getStorageNames(getOptimizationDirectory());
        rules.addAll(getStorageNames(getCommunityOptimizationDirectory()));
        return rules;
    }

    /**
     * Retourne le storage pour la regle demandée
     *
     * @param name le nom de la regle souhaitée
     * @param location location to open storage file
     * @return Le storage pour la regle
     */
    static public OptimizationStorage getOptimization(String name, Location... location) {
        OptimizationStorage result = objectiveCache.get(name);
        if (result == null) {
            Location loc = nonEmptyLocation(location);
            for (File dir : loc.getDirectories()) {
                OptimizationStorage storage = new OptimizationStorage(dir, new File(dir, OPTIMIZATION_PATH), name);
                File sFile = storage.getFile();
                if (sFile.isFile()) {
                    result = storage;
                    objectiveCache.put(name, result);
                }
            }
        }
        return result;
    }

    /**
     * Create new rule.
     * 
     * @param name new rule to create
     * @param location location to rule storage file
     * @return new rule storage
     */
    public static OptimizationStorage createOptimization(String name, Location location) {
        File dir = location.getDirectories()[0];
        OptimizationStorage storage = new OptimizationStorage(dir, new File(dir, OPTIMIZATION_PATH), name);
        return storage;
    }


    static public void checkout() throws VCSException {
        checkout(IsisFish.config.getDatabaseDirectory(), OPTIMIZATION_PATH);
    }

    /**
     * Retourne la liste des noms de toutes les régions disponible en local qui
     * ne sont pas encore sur le serveur VCS
     *
     * @return liste de noms de regions
     */
    static public List<String> getNewOptimisationNames() {
        List<String> result = getOptimizationNames();
        result.removeAll(getRemoteOptimizationNames());
        return result;
    }

    /**
     * Retourne la liste des noms de toutes les régions disponible sur le
     * serveur VCS
     *
     * @return la liste des noms de toutes les régions disponible sur le serveur
     *         VCS. Si le serveur n'est pas disponible la liste retournée est
     *         vide.
     */
    static public List<String> getRemoteOptimizationNames() {
        File dir = getOptimizationDirectory();
        return getRemoteStorageNames(dir);
    }

    /**
     * Retourne la liste des noms de toutes les régions disponible sur le
     * serveur VCS qui ne sont pas encore en local
     *
     * @return liste de noms de regions
     */
    static public List<String> getNewRemoteOptimizationNames() {
        List<String> result = getRemoteOptimizationNames();
        result.removeAll(getOptimizationNames());
        return result;
    }

} // OptimisationStorage

