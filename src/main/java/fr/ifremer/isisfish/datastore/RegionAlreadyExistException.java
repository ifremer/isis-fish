package fr.ifremer.isisfish.datastore;

/*-
 * #%L
 * ISIS-Fish
 * %%
 * Copyright (C) 2020 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

public class RegionAlreadyExistException extends Exception {

    protected String regionName;

    public RegionAlreadyExistException(String message, String regionName) {
        super(message);
        this.regionName = regionName;
    }

    public RegionAlreadyExistException(String message, Throwable cause, String regionName) {
        super(message, cause);
        this.regionName = regionName;
    }

    public String getRegionName() {
        return regionName;
    }
}
