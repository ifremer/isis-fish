/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2014 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.datastore;

import static org.nuiton.i18n.I18n.t;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import fr.ifremer.isisfish.util.IsisFileUtil;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaContextFactory;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.TopiaNotFoundException;
import org.nuiton.util.ZipUtil;

import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.IsisFishRuntimeException;

/**
 * Class abstraite dont herite tous les storages qui on besoin d'un TopiaContext
 * {@link fr.ifremer.isisfish.datastore.RegionStorage} et
 * {@link fr.ifremer.isisfish.datastore.SimulationStorage}
 * 
 * Created: 21 janv. 2006 14:20:51
 * 
 * @author poussin
 * 
 * @version $Revision$
 * 
 * Last update: $Date$ by : $Author$
 */
public abstract class DataStorage extends VersionStorage {

    /** Backup archive filename. */
    public static final String DATA_BACKUP_FILENAME = "data-backup.sql.gz";

    /** Logger for this class */
    private static final Log log = LogFactory.getLog(DataStorage.class);

    /** Le nom du storage (nom du dernier répertoire) */
    protected String name = null;

    /** Le TopiaContext contenant les données */
    protected TopiaContext storage = null;

    /** Le TopiaContext contenant les données en memoire */
    protected TopiaContext memstorage = null;

    /** le répertoire contenant les données sur le disque */
    private File storageDirectory = null;

    /**
     * Permet de créer un nouveau DataStorage
     * 
     * @param directory
     *            repertoire de base du storage
     * @param name
     *            nom du storage (nom du dernier répertoire)
     */
    protected DataStorage(File directory, String name) {
        super(IsisFish.config.getDatabaseDirectory(), directory);
        this.name = name;
        if (!directory.exists()) {
            directory.mkdirs();
        }
    }

    protected File getStorageDirectory() {
        if (storageDirectory == null) {
            storageDirectory = IsisH2Config.getStorageDataDirectory(getDirectory());
            //storageDirectory = new File(getDirectory(), IsisConfig.STORAGE_DATA);
        }
        return storageDirectory;
    }

    /**
     * @return le fichier contenant la representation text des données
     * de la base
     */
    protected File getDataBackupFile() {
        File result = new File(getDirectory(), DATA_BACKUP_FILENAME);
        return result;
    }

    /**
     * Sauve toute la base avec le schéma dans ls fichier
     * &lt;region&gt;/data-backup.sql
     * 
     * @see fr.ifremer.isisfish.datastore.VersionStorage#prepare()
     */
    @Override
    protected void prepare() {
        try {
            File file = getDataBackupFile();

            TopiaContext tx = getStorage().beginTransaction();
            tx.backup(file, true);
            tx.closeContext();

        } catch (TopiaException eee) {
            throw new IsisFishRuntimeException(t("isisfish.error.prepare.data"), eee);
        }
    }

    @Override
    protected boolean isVersionnableFile(File file) {
        boolean result = super.isVersionnableFile(file);
        if (result) {
            result = !file.equals(getStorageDirectory());
        }
        return result;
    }

    /**
     * @return Returns the directory.
     */
    public File getDirectory() {
        return getFile();
    }

    /**
     * @return Returns the name.
     */
    public String getName() {
        return this.name;
    }

    /**
     * @return Retourne une copie de la base de donnée en memoire seulement
     * Util pour les simulations pour l'acces au données rapide
     */
    public TopiaContext getMemStorage() {        
        if (memstorage == null || memstorage.isClosed()) {
            log.info("Create new memory storage for " + getName());
            try {
                // creation des proprietes pour creer le TopiaContext
                Properties config = new Properties();
                IsisH2Config.addMemDatabaseConfig(config, getName());
                IsisH2Config.addHibernateMapping(config);
                
                // instanciation du TopiaContext
                memstorage = TopiaContextFactory.getContext(config);
            } catch (Exception eee) {
                throw new IsisFishRuntimeException("Can't open in memory storage", eee);
            }
            try {
                // il faut absolument faire le prepare, car un script de
                // pre-simulation a pu modifier les données et les données
                // de la region dans le fichier existant ne sont plus forcement
                // valide, il faut donc le mettre a jour
                // FIXME 20230703 dans ce rare cas, de prepare cause la creation d'un backup h2 d'une base
                // vide, donc la simulation mémoire restaure une base vide
                prepare();
                TopiaContext tx = memstorage.beginTransaction();
                tx.restore(getDataBackupFile());
                tx.commitTransaction();
                tx.closeContext();
            } catch (TopiaException eee) {
                throw new IsisFishRuntimeException("Can't populate in memory storage", eee);
            }
        }
        return this.memstorage;
    }

    public void closeMemStorage() throws TopiaException {
        if (memstorage != null) {
            memstorage.closeContext();
            memstorage = null;
        }
    }

    /**
     * Returns the storage.
     * 
     * @return Returns the storage.
     */
    public TopiaContext getStorage() {
        if (storage == null || storage.isClosed()) {
            log.info("Create new storage for " + getName());
            try {
                // creation des proprietes pour creer le TopiaContext
                Properties config = new Properties();
                IsisH2Config.addDatabaseConfig(config, getDirectory());
                IsisH2Config.addHibernateMapping(config);

                // instanciation du TopiaContext
                storage = TopiaContextFactory.getContext(config);

            } catch (TopiaNotFoundException eee) {
                throw new IsisFishRuntimeException("Can't open storage", eee);
            }
        }
        return this.storage;
    }

    public void closeStorage() throws TopiaException {
        if (storage != null) {
            storage.closeContext();
            storage = null;
        }
    }

    @Override
    public void delete(boolean cvsDelete) throws StorageException {
        try {
            if (storage != null) {
                TopiaContext root = getStorage();
                root.clear(true);
            }
        } catch (TopiaException eee) {
            throw new StorageException(t("isisfish.error.delete.database"), eee);
        }
        super.delete(cvsDelete);
    }

    /**
     * Copy le DataStorage courant.
     * 
     * Attention, all script with package name will not be modified you must
     * change package name in each script to reflect new name
     * 
     * @param toName le nouveau nom de la region
     * @throws IOException si problème lors de la copie
     * @throws TopiaException ???
     */
    public void copy(String toName) throws IOException, TopiaException {
        // preparation des datas
        prepare();

        // copy all file in new directory
        File rootDirSrc = getDirectory();
        File rootDirDest = new File(rootDirSrc.getParentFile(), toName);

        // list de tous les fichiers a copier, ce sont les memes que le VCS
        List<File> files = getFiles(false);

        File backupFile = null;
        for (File file : files) {
            // just copy real file, not directory
            if (file.isFile()) {
                String tmp = file.getPath();
                int prefixLength = rootDirSrc.getPath().length();
                tmp = tmp.substring(prefixLength);

                File target = new File(rootDirDest, tmp);
                // keep backup file for next stop copy (data restoration)
                if (file.equals(getDataBackupFile())) {
                    backupFile = target;
                }
                FileUtils.copyFile(file, target);
            }
        }

        // load data
        // creation des proprietes pour creer le TopiaContext
        Properties config = new Properties();
        // false = don't perform, schema is empty 
        IsisH2Config.addDatabaseConfig(config, rootDirDest);
        IsisH2Config.addHibernateMapping(config);

        // instanciation du TopiaContext
        TopiaContext tx = TopiaContextFactory.getContext(config)
                .beginTransaction();
        tx.restore(backupFile);
        tx.commitTransaction();
        tx.closeContext();
    }

    /**
     * Renome juste le repertoire, s'il y a besoin de modifier des données dans
     * le TopiaContext il faut surcharger cette methode. Il est surement
     * necessaire de surcharger cette methode pour aussi modifier l'entre qu'il
     * y a dans le cache.
     * 
     * @param toName le nouveau nom
     * @throws StorageException si problème lors du renommage
     */
    public void rename(String toName) throws StorageException {
        File dir = getDirectory();
        File newdir = new File(dir.getParentFile(), toName);
        dir.renameTo(newdir);
    }

    /**
     * Cree un zip. Le zip contient les memes fichiers lorsqu'on utilise le CVS.
     * 
     * @return le fichier contenant les données zippées, ce fichier sera
     *         automatiquement supprimé à la sortie de l'application s'il existe
     *         encore.
     * @throws IOException si problème lors de la création du zip
     */
    public File createZip() throws IOException {
        File result = IsisFileUtil.createTempFile("isis-" + getName(), ".zip");
        result.deleteOnExit();
        createZip(result);
        return result;
    }

    /**
     * Cree un zip. Le zip contient les memes fichiers lorsqu'on utilise le CVS.
     * 
     * @param file le fichier dans lequel l'export doit se faire
     * @return l'argument file
     * @throws IOException si problème lors de la création du zip
     */
    public File createZip(File file) throws IOException {
        createZip(file, false);
        return file;
    }
    
    /**
     * Cree un zip. Le zip contient les memes fichiers lorsqu'on utilise le CVS.
     * 
     * @param file le fichier dans lequel l'export doit se faire
     * @param createMD5 if {@code true} create zip md5
     * @return l'argument file
     * @throws IOException si problème lors de la création du zip
     */
    public File createZip(File file, boolean createMD5) throws IOException {
        prepare();

        file = IsisFileUtil.addExtensionIfNeeded(file, "zip");

        List<File> files = getFiles(false);
        ZipUtil.compressFiles(file, getDirectory().getParentFile(), files, createMD5);

        log.info("Zip DataStorage in " + file);

        return file;
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            closeStorage();
        } finally {
            super.finalize();
        }
    }

}
