/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2019 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.datastore;

/**
 * Event change on storage.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class StorageChangeEvent {

    public enum Type {
        ADDED,
        DELETED,
        MODIFIED
    }

    /** serialVersionUID. */
    private static final long serialVersionUID = 1526286543413553975L;

    protected Type type;

    /**
     * Constructs a StorageChangeEvent.
     */
    public StorageChangeEvent(Type type) {
        this.type = type;
    }

    public Type getType() {
        return type;
    }
}
