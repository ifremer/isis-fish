/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.datastore.migration;

import org.nuiton.topia.TopiaException;
import org.nuiton.topia.framework.TopiaContextImplementor;
import org.nuiton.topia.migration.TopiaMigrationCallbackByClass;
import org.nuiton.topia.migration.TopiaMigrationCallbackByClass.MigrationCallBackForVersion;
import org.nuiton.version.Version;

import java.util.List;

/**
 * Migration between version 0 and 3.2.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class MigrationV0V32 extends MigrationCallBackForVersion {

    /**
     * Constructor.
     * 
     * @param version version
     * @param callBack callback
     */
    public MigrationV0V32(Version version, TopiaMigrationCallbackByClass callBack) {
        super(version, callBack);
    }

    @Override
    protected void prepareMigrationScript(TopiaContextImplementor tx,
            List<String> queries, boolean showSql, boolean showProgression)
            throws TopiaException {
        queries.add("alter table SETOFVESSELS add column TECHNICALEFFICIENCYEQUATION VARCHAR(255);");
        queries.add("alter table STRATEGY add column INACTIVITYEQUATIONUSED BIT default false;");
        queries.add("alter table STRATEGY add column INACTIVITYEQUATION VARCHAR(255);");
        queries.add("alter table STRATEGYMONTHINFO alter NUMBEROFTRIPS double;");
        queries.add("alter table STRATEGYMONTHINFO alter MININACTIVITYDAYS double;");
    }
}
