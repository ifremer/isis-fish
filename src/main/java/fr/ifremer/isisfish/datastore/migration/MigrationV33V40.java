/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.datastore.migration;

import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.entities.Equation;
import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.entities.PopulationImpl;
import fr.ifremer.isisfish.ui.input.equation.EquationEditorPaneUI;
import fr.ifremer.isisfish.util.EvaluatorHelper;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;
import org.hibernate.query.NativeQuery;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.framework.TopiaContextImplementor;
import org.nuiton.topia.migration.TopiaMigrationCallbackByClass;
import org.nuiton.topia.migration.TopiaMigrationCallbackByClass.MigrationCallBackForVersion;
import org.nuiton.version.Version;

import java.math.BigInteger;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Migration between version 3.3 and 4.0.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class MigrationV33V40 extends MigrationCallBackForVersion {

    static private Log log = LogFactory.getLog(MigrationV33V40.class);

    /**
     * Constructor.
     * 
     * @param version version
     * @param callback callback
     */
    public MigrationV33V40(Version version, TopiaMigrationCallbackByClass callback) {
        super(version, callback);
    }

    @Override
    protected void prepareMigrationScript(TopiaContextImplementor tx,
            List<String> queries, boolean showSql, boolean showProgression)
            throws TopiaException {

        Session session = tx.getHibernate();

        // get capturability value
        NativeQuery query = session.createNativeQuery("SELECT p.topiaid as pop, g.topiaid as popgroup, g.id, (select count(*) FROM POPULATIONGROUP WHERE population = p.topiaid) as c FROM POPULATION p, POPULATIONGROUP g WHERE p.MATURITYGROUP = g.TOPIAID");
        List<Object[]> maturityGroupResult = query.list();
       
        // get reproduction rate value
        query = session.createNativeQuery("SELECT population, id, REPRODUCTIONRATE FROM POPULATIONGROUP");
        List<Object[]> reproductionResults = query.list();

        // remove maturity group
        tx.executeSQL("alter table POPULATIONGROUP drop column REPRODUCTIONRATE;");
        tx.executeSQL("alter table POPULATION drop column MATURITYGROUP;");

        // new equation
        tx.executeSQL("alter table POPULATION add column MATURITYOGIVEEQUATION VARCHAR(255);");
        tx.executeSQL("alter table POPULATION add column REPRODUCTIONRATEEQUATION VARCHAR(255);");

        // date > step
        tx.executeSQL("alter table Result ALTER COLUMN resultdate RENAME TO resultstep;");
        tx.executeSQL("alter table ActiveRule ALTER COLUMN activeruledate RENAME TO activerulestep;");

        // capturability equation
        tx.executeSQL("alter table POPULATION add column CAPTURABILITYEQUATION VARCHAR(255);");
        tx.executeSQL("alter table POPULATION add column CAPTURABILITYEQUATIONUSED BIT default false;");

        // migration v 4.1.2 (mais necessaire de la faire avant sinon le findByTopiaId va planter)
        tx.executeSQL("alter table POPULATION add column fbargroupmin integer default 0");
        tx.executeSQL("alter table POPULATION add column fbargroupmax integer default 0");

        // migration v 4.4 (mais necessaire de la faire avant sinon le from va planter)
        tx.executeSQL("alter table POPULATION add column IF NOT EXISTS RecruitmentEQUATION VARCHAR(255);");
        tx.executeSQL("alter table POPULATION add column IF NOT EXISTS abundanceReferenceMonth integer default null;");
        tx.executeSQL("alter table POPULATION add column IF NOT EXISTS computeFOnLandings BIT default false;");
        tx.executeSQL("alter table POPULATION add column IF NOT EXISTS FishingMortalityOtherFleets VARCHAR(255);");

        // build new equation for maturity group
        for (Object[] maturityRow : maturityGroupResult) {
            String population = (String)maturityRow[0];
            String populationgroup = (String)maturityRow[1];
            int groupId = (Integer)maturityRow[2];
            int groupCount = ((BigInteger)maturityRow[3]).intValue();
            if (populationgroup != null) {
                Population pop = (Population)tx.findByTopiaId(population);
                String content = getMaturityEquationContent(groupId, groupCount);
                ((PopulationImpl)pop).setMaturityOgiveEquationContent(content);
                pop.update();
            }
        }
        
        // build new equation for maturity group and reproduction rate
        List<Population> pops = tx.findAll("from " + Population.class.getName());
        for (Population pop : pops) {
            String content = getReproductionRateContent(pop, reproductionResults);
            ((PopulationImpl)pop).setReproductionRateEquationContent(content);
            pop.update();
        }

        // start equation migration
        if (IsisFish.config.isLaunchUI()) {
            List<Equation> equations = tx.findAll("from " + Equation.class.getName());
            for (Equation equation : equations) {

                // empty is empty and stay empty
                if (StringUtils.isBlank(equation.getContent())) {
                    continue;
                }

                // first compilation
                String content = equation.getContent();
                int ok = EvaluatorHelper.check(equation.getJavaInterface(), content, null);
                if (ok != 0) {
                    content = StringUtils.replace(content, "Date ", "TimeStep ");
                    content = StringUtils.replace(content, ".getDate()", ".getStep()");
                    content = StringUtils.replace(content, "new Date(", "new TimeStep(");
                    // second compilation
                    ok = EvaluatorHelper.check(equation.getJavaInterface(), content, null);
                    if (ok == 0) {
                        equation.setContent(content);
                        equation.update();
                    } else {
                        EquationEditorPaneUI frame = new EquationEditorPaneUI();
                        frame.setTitle(t("isisfish.message.import.equation.convert"));
        
                        try {
                            frame.getHandler().setEquation(equation.getCategory(), equation.getName(),
                                    equation.getJavaInterface(), content);
                        } catch (Exception ex) {
                            if (log.isErrorEnabled()) {
                                log.error("Can't migrate equation", ex);
                            }
                        }
        
                        frame.setVisible(true);
                        if (frame.isResultOk()) {
                            equation.setContent(frame.getEditor().getText());
                            equation.update();
                        }
                    }
                }
            }
        }
    }

    /**
     * Return reproduction rate equation content generated from previous group
     * reproduction rate fields (v3).
     * 
     * @param pop pop
     * @param reproductionResults v3 reproduction rate (gtopiaid, rate);
     * @return equation content
     */
    protected String getReproductionRateContent(Population pop, List<Object[]> reproductionResults) {
        
        StringBuilder b = new StringBuilder();
        b.append("if (group == null) return 0;\n");
        b.append("switch (group.getId()) {\n");
        for (Object[] reproContent : reproductionResults) {
            String popid = (String)reproContent[0];
            int gid = (Integer)reproContent[1];
            double rate = (Double)reproContent[2];
            if (pop.getTopiaId().equals(popid)) { // take care of current pop only
                b.append("   case " + gid + ": return " + rate + ";\n");
            }
        }
        b.append("  default: return 0;\n");
        b.append("}\n");

        return b.toString();
    }

    /**
     * Return maturity equation content generated from previous
     * maturity group (v3) value.
     * 
     * @param groupId previous group id
     * @param groupCount population group count
     * @return equation content
     */
    protected String getMaturityEquationContent(int groupId, int groupCount) {

        StringBuilder b = new StringBuilder();
        b.append("if (group == null) return 0;\n");
        b.append("switch (group.getId()) {\n");
        for (int i = 0 ; i < groupCount; ++i) {
            if (i > groupId) {
                b.append("   case " + i + ": return 0;\n");
            } else {
                b.append("   case " + i + ": return 1;\n");
            }
        }
        b.append("  default: return 0;\n");
        b.append("}\n");

        return b.toString();
    }
}
