/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.datastore.migration;

import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.entities.PopulationImpl;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.framework.TopiaContextImplementor;
import org.nuiton.topia.migration.TopiaMigrationCallbackByClass;
import org.nuiton.topia.migration.TopiaMigrationCallbackByClass.MigrationCallBackForVersion;
import org.nuiton.version.Version;

import java.util.List;

/**
 * Migration between version 4.3 and 4.4.
 * 
 * @author poussin
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class MigrationV43V44 extends MigrationCallBackForVersion {

    /**
     * Constructor.
     * 
     * @param version version
     * @param callback callback
     */
    public MigrationV43V44(Version version, TopiaMigrationCallbackByClass callback) {
        super(version, callback);
    }

    @Override
    protected void prepareMigrationScript(TopiaContextImplementor tx,
            List<String> queries, boolean showSql, boolean showProgression)
            throws TopiaException {

        // relicat de la migration depuis la version MigrationV33V40
        tx.executeSQL("alter table POPULATION drop column IF EXISTS fbargroupmin;");
        tx.executeSQL("alter table POPULATION drop column IF EXISTS fbargroupmax;");

        // add recruitment equation
        tx.executeSQL("alter table POPULATION add column IF NOT EXISTS RecruitmentEQUATION VARCHAR(255);");
        // add abundanceReferenceMonth field
        tx.executeSQL("alter table POPULATION add column IF NOT EXISTS abundanceReferenceMonth integer default null;");
        tx.executeSQL("update POPULATION set abundanceReferenceMonth = null;"); // strange default
        // rename groupMin/groupMax
        tx.executeSQL("alter table POPULATION alter column groupMin RENAME TO fbarGroupMin;");
        tx.executeSQL("alter table POPULATION alter column groupMax RENAME TO fbarGroupMax;");
        // add representativeAbundanceMonth field
        tx.executeSQL("alter table POPULATION add column IF NOT EXISTS computeFOnLandings BIT default false;");
        // add recruitment equation
        tx.executeSQL("alter table POPULATION add column IF NOT EXISTS FishingMortalityOtherFleets VARCHAR(255);");
        

        // build new equation for maturity group and reproduction rate
        List<Population> pops = tx.findAll("from " + Population.class.getName());
        for (Population pop : pops) {
            ((PopulationImpl)pop).setRecruitmentEquationContent("return 0;");
            pop.update();
        }

    }
}
