/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.datastore.migration;

import org.nuiton.topia.TopiaException;
import org.nuiton.topia.framework.TopiaContextImplementor;
import org.nuiton.topia.migration.TopiaMigrationCallbackByClass;
import org.nuiton.topia.migration.TopiaMigrationCallbackByClass.MigrationCallBackForVersion;
import org.nuiton.version.Version;

import java.util.List;

/**
 * Migration between version 3.2 and 3.3.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class MigrationV32V33 extends MigrationCallBackForVersion {

    /**
     * Constructor.
     * 
     * @param version version
     * @param callback callback
     */
    public MigrationV32V33(Version version, TopiaMigrationCallbackByClass callback) {
        super(version, callback);
    }

    @Override
    protected void prepareMigrationScript(TopiaContextImplementor tx,
            List<String> queries, boolean showSql, boolean showProgression)
            throws TopiaException {
        // replace in equation
        queries.add("update EQUATION set content = replace(content, 'org.codelutin.', 'org.nuiton.');");
        // replace in matrix semantics
        queries.add("update POPULATION set recruitmentDistribution_semantics = replace(recruitmentDistribution_semantics, 'org.codelutin.', 'org.nuiton.') where recruitmentDistribution_semantics IS NOT null;");
        queries.add("update POPULATION set mappingZoneReproZoneRecru_semantics = replace(mappingZoneReproZoneRecru_semantics, 'org.codelutin.', 'org.nuiton.') where mappingZoneReproZoneRecru_semantics IS NOT null;");
        queries.add("update POPULATION set capturability_semantics = replace(capturability_semantics, 'org.codelutin.', 'org.nuiton.') where capturability_semantics IS NOT null;");
        queries.add("update POPULATIONSEASONINFO set reproductionDistribution_semantics = replace(reproductionDistribution_semantics, 'org.codelutin.', 'org.nuiton.') where reproductionDistribution_semantics IS NOT null;");
        queries.add("update POPULATIONSEASONINFO set lengthChangeMatrix_semantics = replace(lengthChangeMatrix_semantics, 'org.codelutin.', 'org.nuiton.') where lengthChangeMatrix_semantics IS NOT null;");
        queries.add("update POPULATIONSEASONINFO set migrationMatrix_semantics = replace(migrationMatrix_semantics, 'org.codelutin.', 'org.nuiton.') where migrationMatrix_semantics IS NOT null;");
        queries.add("update POPULATIONSEASONINFO set emigrationMatrix_semantics = replace(emigrationMatrix_semantics, 'org.codelutin.', 'org.nuiton.') where emigrationMatrix_semantics IS NOT null;");
        queries.add("update POPULATIONSEASONINFO set immigrationMatrix_semantics = replace(immigrationMatrix_semantics, 'org.codelutin.', 'org.nuiton.') where immigrationMatrix_semantics IS NOT null;");
        queries.add("update RESULT set matrix_semantics = replace(matrix_semantics, 'org.codelutin.', 'org.nuiton.') where matrix_semantics IS NOT null;");
        queries.add("update STRATEGYMONTHINFO set proportionMetier_semantics = replace(proportionMetier_semantics, 'org.codelutin.', 'org.nuiton.') where proportionMetier_semantics IS NOT null;");
        // add .shp default extension
        queries.add("update FISHERYREGION set MAPFILES = regexp_replace(MAPFILES, '(,|$)', '.shp$1') where MAPFILES IS NOT null AND LENGTH(TRIM(MAPFILES)) > 0;");
    }
}
