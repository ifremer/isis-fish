/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.datastore.migration;

import org.nuiton.topia.TopiaException;
import org.nuiton.topia.framework.TopiaContextImplementor;
import org.nuiton.topia.migration.TopiaMigrationCallbackByClass;
import org.nuiton.topia.migration.TopiaMigrationCallbackByClass.MigrationCallBackForVersion;
import org.nuiton.version.Version;

import java.util.List;

/**
 * Migration between version 4.1 and 4.2.1.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class MigrationV421V43 extends MigrationCallBackForVersion {

    /**
     * Constructor.
     * 
     * @param version version
     * @param callback callback
     */
    public MigrationV421V43(Version version, TopiaMigrationCallbackByClass callback) {
        super(version, callback);
    }

    @Override
    protected void prepareMigrationScript(TopiaContextImplementor tx,
            List<String> queries, boolean showSql, boolean showProgression)
            throws TopiaException {

        // ajout de la table 'observation'
        queries.add("CREATE TABLE observation(  " +
                "TOPIAID VARCHAR(255) NOT NULL, " +
                "TOPIAVERSION BIGINT NOT NULL,  " +
                "TOPIACREATEDATE DATE,          " +
                "NAME LONGVARCHAR,              " +
                "COMMENT LONGVARCHAR,           " +
                "VALUE_NAME VARCHAR(255),       " +
                "VALUE_DIM VARCHAR(255),        " +
                "VALUE_DIMNAMES LONGVARCHAR,    " +
                "VALUE_SEMANTICS LONGVARCHAR,   " +
                "VALUE_DATA LONGVARCHAR)        ");
    }
}
