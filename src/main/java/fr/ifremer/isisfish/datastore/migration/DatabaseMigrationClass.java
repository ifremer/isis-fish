/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.datastore.migration;

import static org.nuiton.i18n.I18n.t;

import java.util.List;

import javax.swing.JOptionPane;

import org.nuiton.topia.migration.TopiaMigrationCallbackByClass;

import fr.ifremer.isisfish.IsisFishDAOHelper;
import org.nuiton.version.Version;
import org.nuiton.version.Versions;

/**
 * Migration callback by class for all isis database migration.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class DatabaseMigrationClass extends TopiaMigrationCallbackByClass {

    protected static final Version VERSION_32 = Versions.valueOf("3.2");
    protected static final Version VERSION_33 = Versions.valueOf("3.3");
    protected static final Version VERSION_40 = Versions.valueOf("4.0");
    protected static final Version VERSION_41 = Versions.valueOf("4.1");
    protected static final Version VERSION_421 = Versions.valueOf("4.2.1");
    protected static final Version VERSION_43 = Versions.valueOf("4.3");
    protected static final Version VERSION_44 = Versions.valueOf("4.4");

    public DatabaseMigrationClass() {
        super(new MigrationResolver());
    }

    protected static class MigrationResolver implements MigrationCallBackForVersionResolver {

        @Override
        public Class<? extends MigrationCallBackForVersion> getCallBack(Version version) {
            Class<? extends MigrationCallBackForVersion> result = null;
            
            if (version.equals(VERSION_32)) {
                result = MigrationV0V32.class;
            } else if (version.equals(VERSION_33)) {
                result = MigrationV32V33.class;
            } else if (version.equals(VERSION_40)) {
                result = MigrationV33V40.class;
            } else if (version.equals(VERSION_41)) {
                result = MigrationV40V41.class;
            } else if (version.equals(VERSION_421)) {
                result = MigrationV41V421.class;
            } else if (version.equals(VERSION_43)) {
                result = MigrationV421V43.class;
            } else if (version.equals(VERSION_44)) {
                result = MigrationV43V44.class;
            }
            return result;
        }
        
    }

    @Override
    public Version[] getAvailableVersions() {
        Version[] result = new Version[] { VERSION_32, VERSION_33, VERSION_40, VERSION_41, VERSION_421, VERSION_43, VERSION_44};
        return result;
    }

    @Override
    public Version getApplicationVersion() {
        Version appVersion = Versions.valueOf(IsisFishDAOHelper.getModelVersion());
        return appVersion;
    }

    @Override
    public boolean askUser(Version dbVersion, List<Version> versions) {
        boolean result = false;
        
        int answer = JOptionPane.showConfirmDialog(
                null, t("isisfish.misc.databasemigration.question", dbVersion, versions.get(versions.size() - 1)),
                t("isisfish.misc.databasemigration.title"),
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        
        if (answer == JOptionPane.YES_OPTION) {
            result = true;
        }
        
        return result;
    }

}
