/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2012 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.datastore;

import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.annotations.Doc;
import fr.ifremer.isisfish.simulator.sensitivity.SensitivityAnalysis;
import fr.ifremer.isisfish.vcs.VCSException;
import org.apache.commons.collections4.map.ReferenceMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

import static org.nuiton.i18n.I18n.t;

/**
 * Cette class permet de stocker les fichiers de calculateur de sensibilité.
 * <p>
 * Gere les fichiers VCS de type {@link SensitivityAnalysis} (package sensitivityanalysis).
 * 
 * Created: 17 août 2005 11:11:51 CEST
 *
 * @author chatellier &lt;chatellier@codelutin.com&gt;
 * @version $Revision$
 * Last update: $Date$ by : $Author$
 */
public class SensitivityAnalysisStorage extends JavaSourceStorage { // SensitivityStorage

    /** to use log facility, just put in your code: log.info(\"...\"); */
    private static Log log = LogFactory.getLog(SensitivityAnalysisStorage.class);

    /** Emplacement de stockage des fichiers de sensibilité */
    public final static String SENSITIVITY_ANALYSIS_PATH = "sensitivityanalysis";

    /** Template freemarker pour les scripts de sensibilité. */
    public static final String SENSITIVITY_ANALYSIS_TEMPLATE = "templates/script/sensitivityanalysis.ftl";

    /** Instance cache. */
    protected static Map<String, SensitivityAnalysisStorage> sensitivityCache = new ReferenceMap<>();

    protected static Set<StorageChangeListener> storageListeners = Collections.newSetFromMap(new WeakHashMap<>());

    public static void addStorageListener(StorageChangeListener storageListener) {
        storageListeners.add(storageListener);
    }

    /**
     * Build new {@link SensitivityAnalysisStorage}.
     *
     * @param rootSrc   repertoire root de stockage des calculateurs de sensibilité.
     * @param directory le repertoire ou devrait se trouver le calculateur de sensibilité
     * @param name      le nom du calculateur de sensibilité
     */
    protected SensitivityAnalysisStorage(File rootSrc, File directory, String name) {
        super(rootSrc, directory, name);
    }

    public static void registerWatcher() {
        registerWatcher(storageListeners, getSensitivityAnalysisDirectory(), getCommunitySensitivityAnalysisDirectory());
    }

    @Override
    public String getPackage() {
        return SENSITIVITY_ANALYSIS_PATH;
    }

    /**
     * Get sensitivity storage directory.
     * 
     * Create directory if not exists.
     * 
     * @return sensitivity storage directory
     */
    public static File getSensitivityAnalysisDirectory() {
        File result = new File(getContextDatabaseDirectory(), SENSITIVITY_ANALYSIS_PATH);
        result.mkdirs();
        return result;
    }

    /**
     * Get community VCS sensitivity directory.
     * 
     * @return community sensitivity directory
     */
    public static File getCommunitySensitivityAnalysisDirectory() {
        File result = new File(getCommunityDatabaseDirectory(), SENSITIVITY_ANALYSIS_PATH);
        result.mkdirs();
        return result;
    }

    /**
     * Retourne le nom de tous les calculateurs de sensibilité existant.
     *
     * @return le nom de tous les calculateurs de sensibilité existans en local
     */
    public static List<String> getSensitivityAnalysisNames() {
        List<String> result = getStorageNames(getSensitivityAnalysisDirectory());
        result.addAll(getStorageNames(getCommunitySensitivityAnalysisDirectory()));
        return result;
    }

    /**
     * Retourne le storage pour le calculateur demandé.
     *
     * @param name le nom du calculateur souhaité
     * @param location location to open storage file
     * @return Le {@link SensitivityAnalysisStorage} pour le calculateur
     */
    static public SensitivityAnalysisStorage getSensitivityAnalysis(String name, Location... location) {
        SensitivityAnalysisStorage result = sensitivityCache.get(name);
        if (result == null) {
            Location loc = nonEmptyLocation(location);
            for (File dir : loc.getDirectories()) {
                SensitivityAnalysisStorage storage = new SensitivityAnalysisStorage(dir, new File(dir, SENSITIVITY_ANALYSIS_PATH), name);
                if (storage.getFile().isFile()) {
                    result = storage;
                    sensitivityCache.put(name, result);
                }
            }
        }
        return result;
    }

    /**
     * Create new sensitivity analysis.
     * 
     * @param name new sensitivity analysis to create
     * @param location location to sensitivity analysis storage file
     * @return new sensitivity analysis storage
     */
    public static SensitivityAnalysisStorage createSensitivityAnalysis(String name, Location location) {
        File dir = location.getDirectories()[0];
        SensitivityAnalysisStorage storage = new SensitivityAnalysisStorage(dir, new File(dir, SENSITIVITY_ANALYSIS_PATH), name);
        return storage;
    }

    /**
     * Effectue un chekout VCS sur le répertoire des calculateurs.
     * 
     * @see VersionStorage#checkout(File, String)
     * @see #SENSITIVITY_ANALYSIS_PATH
     * 
     * @throws VCSException if an error occurs during checkout
     */
    public static void checkout() throws VCSException {
        checkout(IsisFish.config.getDatabaseDirectory(), SENSITIVITY_ANALYSIS_PATH);
    }

    /**
     * Retourne la liste des noms de tous les calculateurs disponibles en local qui
     * ne sont pas encore sur le serveur VCS.
     *
     * @return liste de noms de calculateurs
     */
    static public List<String> getNewSensitivityAnalysisNames() {
        List<String> result = getSensitivityAnalysisNames();
        result.removeAll(getRemoteSensitivityAnalysisNames());
        return result;
    }

    /**
     * Retourne la liste des noms de tous les calculateurs disponibles sur le
     * serveur VCS
     *
     * @return la liste des noms de tous les calculateurs disponibles sur le serveur
     *         VCS. Si le serveur n'est pas disponible la liste retournée est
     *         vide.
     */
    static public List<String> getRemoteSensitivityAnalysisNames() {
        File dir = getSensitivityAnalysisDirectory();
        return getRemoteStorageNames(dir);

    }

    /**
     * Retourne la liste des noms de tous les calculateurs disponibles sur le
     * serveur VCS qui ne sont pas encore en local.
     *
     * @return liste de noms de regions
     * @throws VCSException
     */
    static public List<String> getNewRemoteSensitivityAnalysisNames()
            throws VCSException {
        List<String> result = getRemoteSensitivityAnalysisNames();
        result.removeAll(getSensitivityAnalysisNames());
        return result;
    }

    /**
     * <b>Be ware this method require to instanciate a AnalysePlan, so
     * it would be better to call as often as possible.</b>
     *
     * @return the descript of the instanciate AnalysePlan
     * @see Doc
     */
    @Override
    public String getDescription() {
        String result = null;
        try {
            SensitivityAnalysis sensitivityAnalysis = getNewInstance();
            result = sensitivityAnalysis == null ? null : sensitivityAnalysis.getDescription();
        } catch (Exception e) {
            log.warn(t("isisfish.error.not.found.description", this));
        }
        return result;
    }

} // SensitivityStorage
