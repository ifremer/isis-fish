/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2012 Ifremer, Code Lutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.datastore;


import fr.ifremer.isisfish.util.matrix.EntitySemanticsDecorator;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.DoubleBigMappedVector;
import org.nuiton.math.matrix.MatrixFactory;
import org.nuiton.math.matrix.MatrixHelper;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.math.matrix.MatrixSemanticsDecorator;
import org.nuiton.math.matrix.SemanticsDecorator;
import org.nuiton.topia.TopiaContext;

import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.types.TimeStep;
import fr.ifremer.isisfish.util.BitUtil;

/**
 * Cette classe permet de conserver des résultats de simulation. Elle permet
 * ensuite de les récupérer.
 * 
 * Created: 31 aout 2012
 *
 * @author Benjamin Poussin : poussin@codelutin.com
 * @version $Revision$
 *
 * Mise a jour: $Date$
 * par : $Author$
 *
 * @deprecated {@link ResultStorageCSV} replace this implementation. This class
 * will be remove in 5.0.0.0
 */
@Deprecated
public class ResultMappedStorage extends ResultStorageAbstract { // ResultStorage

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(ResultMappedStorage.class);

    protected MatrixFactory matrixFactory;
    
    protected RandomAccessFile raf;

    protected long offset;

    protected Map<String, Map<TimeStep, ResultMapped>> nameStepResults = new TreeMap<>();

    /**
     * Represente un resultat
     * Lors de la construction, si on ne passe que le RandomAccessFile et l'offset, on relie un resultat
     * si on passe toutes les infos, on ecrit le resultat.
     * Les deux operations doivent etre symetrique on lit et ecrit les meme informations
     */
    static protected class ResultMapped {
        protected RandomAccessFile raf;
        protected long offset;
        protected long size;
        protected TimeStep step;
        protected String name;
        protected MatrixND matrix;

        /**
         * Classe permettant de lire et ecrire le header d'un resultat matrice
         * Le header comprend tout sauf les donnees de la matrice.
         * Si on ne retrouve pas la mark, alors on ne lit pas le resultat.
         * Cette classe sert a simplifier la lecture et l'ecriture pour garantir
         * la symetrie entre les deux
         */
        static protected class ResultHeaderMatrix {
            final static long RESULT_MATRIX = BitUtil.toMark("resmat01");

            int stepValue; // le pas de temps
            String name;   // le nom du resultat
            int dimSize;   // le nombre de dimension
            String[] dimNames; // le nom des dimensions
            int[] dims;    // la taille de chaque dimension
            List[] sems;   // les semantiques de chaque dimension
            long dataSize;  // la taille du vecteur. En mettant un int on est limite a 2Go x 8 (double) = 16Go par matrice

            /**
             * Methode qui prend tous les champs en parametre, ce qui force a ne pas en oublier
             * @param stepValue
             * @param name
             * @param dimSize
             * @param dimNames
             * @param dims
             * @param sems
             * @param dataSize
             */
            public void set(int stepValue, String name, int dimSize,
                    String[] dimNames, int[] dims, List[] sems, long dataSize) {
                this.stepValue = stepValue;
                this.name = name;
                this.dimSize = dimSize;
                this.dimNames = dimNames;
                this.dims = dims;
                this.sems = sems;
                this.dataSize = dataSize;
            }

            /**
             * Lit le header si possible. Si ce n'est pas possible replace le
             * raf a l'emplacement auquel il etait avant la tentative de lecture.
             *
             * Utilise pour toutes les chaines lu, la representation interne
             * pour minimiser le nombre de chaine reellement en memoire
             * 
             * @param raf
             * @return
             * @throws IOException
             */
            public static ResultHeaderMatrix read(RandomAccessFile raf, long offset) throws IOException {
                raf.seek(offset);

                long mark = raf.readLong();
                ResultHeaderMatrix result = null;
                if (mark != RESULT_MATRIX) {
                    // on a pas le bon marqueur, on se replace avant sa lecture
                    raf.seek(offset);
                } else {
                    result = new ResultHeaderMatrix();
                    result.stepValue = raf.readInt();
                    result.name = raf.readUTF().intern();
                    result.dimSize = raf.readInt();
                    result.dimNames = new String[result.dimSize];
                    for (int i=0; i< result.dimSize; i++) {
                        result.dimNames[i] = raf.readUTF().intern();
                    }
                    result.dims = new int[result.dimSize];
                    for (int i=0; i< result.dimSize; i++) {
                        result.dims[i] = raf.readInt();
                    }

                    result.sems = new List[result.dimSize];
                    for (int i=0; i< result.dimSize; i++) {
                        result.sems[i] = new ArrayList();
                        for (int j=0; j< result.dims[i]; j++) {
                            String s = raf.readUTF().intern();
                            result.sems[i].add(s);
                        }
                    }
                    result.dataSize = raf.readInt();
                }
                return result;
            }
            public void write (RandomAccessFile raf, long offset) throws IOException {
                raf.seek(offset);
                long mark = RESULT_MATRIX;

                raf.writeLong(mark);
                raf.writeInt(this.stepValue);
                raf.writeUTF(this.name);
                raf.writeInt(this.dimSize);
                for (int i=0; i< this.dimSize; i++) {
                    raf.writeUTF(this.dimNames[i]);
                }
                for (int i=0; i< this.dimSize; i++) {
                    raf.writeInt(this.dims[i]);
                }


                for (List sem : this.sems) {
                    for (Object s : sem) {
                        raf.writeUTF(String.valueOf(s));
                    }
                }
                raf.writeLong(this.dataSize);
            }
        }



        /** read data from file */
        public ResultMapped(MatrixFactory matrixFactory, RandomAccessFile raf, long offset) throws IOException {
            this.raf = raf;
            this.offset = offset;
            ResultHeaderMatrix header = ResultHeaderMatrix.read(raf, offset);
            if (header != null) {
                step = new TimeStep(header.stepValue);
                name = header.name;
                String[] dimNames = header.dimNames;
                List[] sems = header.sems;

                long dataOffset = raf.getFilePointer();
                long size = raf.getFilePointer() - offset;

                long dataSize = header.dataSize;

                DoubleBigMappedVector data = new DoubleBigMappedVector(raf, dataOffset, dataSize);
                this.matrix = matrixFactory.create(name, sems, dimNames, data);

                size += dataSize * 8; /* un double est sur 8 bytes*/
                this.size = size;
            }
        }

        /** write date to file */
        public ResultMapped(RandomAccessFile raf, long offset,
                TimeStep step, String name, MatrixND matrix) throws IOException {
            this.raf = raf;
            this.offset = offset;
            this.step = step;
            this.name = name;

            String[] dimNames = matrix.getDimensionNames();
            int[] dims = matrix.getDim();
            List[] sems = matrix.getSemantics();
            long dataSize = MatrixHelper.getVectorSize(dims);  // en mettant un int on est limite a 2Go x 8 (double) = 16Go par matrice

            // conversion des semantiques
            SemanticsDecorator deco = new EntitySemanticsDecorator();
            for (int i=0; i<sems.length; i ++) {
                List l = sems[i];
                List undecorate = new ArrayList(l.size());
                sems[i] = undecorate;
                for (Object o : l) {
                    o = deco.undecorate(o);
                    undecorate.add(o);
                }
            }

            ResultHeaderMatrix header = new ResultHeaderMatrix();
            header.set(step.getStep(), name, dims.length, dimNames, dims, sems, dataSize);
            header.write(raf, offset);

            // on prend la position apres l'ecriture de l'entete
            long dataOffset = raf.getFilePointer();
            // on cree la nouvelle matrice comme il faut (semantique non decore)
            DoubleBigMappedVector data = new DoubleBigMappedVector(raf, dataOffset, dataSize);
            this.matrix = MatrixFactory.getInstance().create(name, sems, dimNames, data);
            // et on met les valeurs de l'ancienne dans la nouvelle
            this.matrix.paste(matrix);

            // la taille du tout est la longueur du header (position des data - offset) + taille des data
            size = dataOffset - offset + dataSize * 8; /* un double est sur 8 bytes*/
        }

        /**
         * retourne la taille en nombre de byte
         * @return
         */
        public long size() {
            return size;
        }

        public TimeStep getStep() {
            return step;
        }

        public String getName() {
            return name;
        }

        public MatrixND getMatrix() {
            return matrix;
        }
        
        public MatrixND getMatrix(TopiaContext tx) {
            // on met la matrice dans un decorateur pour convertir automatiquement les semantiques
            MatrixND result = new MatrixSemanticsDecorator(matrix,
                    new EntitySemanticsDecorator(
                            new EntitySemanticsDecorator.EntityTxProvider(tx)));
            return result;
        }
    }

    /**
     * Les ResultStorage ne doivent pas etre instancier directement, mais
     * recuperer a partir d'un
     * {@link fr.ifremer.isisfish.datastore.SimulationStorage#getResultStorage()}
     *
     * @param simulation storage to get result
     */
    public ResultMappedStorage(SimulationStorage simulation) throws IOException {
        super(simulation);

        File file = SimulationStorage.getResultFile(simulation.getDirectory());

        // il faut toujours ouvrir en 'rw' car sinon les matrices n'arrive pas
        // a ce charger car elle sont toujours en RW.
        raf = new RandomAccessFile(file, "rw");

        // on lit les donnees deja presente
        offset = 0;
        while (offset < raf.length()) {
            ResultMapped r = new ResultMapped(getMatrixFactory(), raf, offset);
            storeResult(r);
            offset += r.size();
        }
    }

    @Override
    protected MatrixFactory getMatrixFactory() {
        if (matrixFactory == null) {
            // instanciation de la factory avec l'implementation
            // choisie dans la config
            Class vectorClass = IsisFish.config.getMappedResultMatrixVectorClass();
            matrixFactory = MatrixFactory.getInstance(vectorClass);
        }
        return matrixFactory;
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            raf.close();
        } finally {
            super.finalize();
        }
    }
    
    @Override
    public void delete() {
        super.delete();
        File file = SimulationStorage.getResultFile(simulation.getDirectory());
        file.delete();
    }

    @Override
    public void close() {
        if (raf != null) {
            IOUtils.closeQuietly(raf);
            raf = null;
        }
    }

    /**
     * Methode interne pour que les deux Map soit mise a jour en meme temps
     * C'est la seul methode qui permet d'ajouter des noms de result dans les Map
     * @param r 
     */
    protected void storeResult(ResultMapped r) {
        TimeStep step = r.getStep();
        String name = r.getName();

//        getResult(step).put(name, r);
        getResult(name).put(step, r);
    }

//    protected Map<String, ResultMapped> getResult(TimeStep step) {
//        Map<String, ResultMapped> result = stepNameResults.get(step);
//        if (result == null) {
//            result = new TreeMap<String, ResultMapped>();
//            stepNameResults.put(step, result);
//        }
//        return result;
//    }

    protected Map<TimeStep, ResultMapped> getResult(String name) {
        Map<TimeStep, ResultMapped> result = nameStepResults.computeIfAbsent(name, k -> new TreeMap<>());
        return result;
    }

    @Override
    protected MatrixND readResult(TimeStep step, String name) {
        MatrixND result = null;

        ResultMapped rm = getResult(name).get(step);
        if (rm != null) {
            result = rm.getMatrix();
        }

        return result;
    }

//    @Override
//    protected Map<TimeStep, MatrixND> readResult(String name) {
//        Map<TimeStep, MatrixND> result = Collections.EMPTY_MAP;
//        Map<TimeStep, ResultMapped> rms = nameStepResults.get(name);
//        if (rms != null) {
//            result = new HashMap<TimeStep, MatrixND>();
//            for (Map.Entry<TimeStep, ResultMapped> e : rms.entrySet()) {
//                result.put(e.getKey(), e.getValue().getMatrix());
//            }
//        }
//        return result;
//    }

    @Override
    protected void writeResult(TimeStep step, String name, MatrixND mat) {
        try {
            ResultMapped r = new ResultMapped(raf, offset, step, name, mat);
            storeResult(r);
            offset += r.size();
            // si on force un sync (ecriture disque) au cas
            // ou la simulation plante, on a tout de meme des resultat. Mais
            // cela dimini grandement les performances :(  On divise par 3 le
            // temps de simulation, si on ne force pas le sync.
            // raf.getFD().sync();
        } catch (IOException eee) {
            throw new IsisFishRuntimeException("Can't write result", eee);
        }
    }

    @Override
    protected void writeActiveRule(TimeStep step, String name, String params) {
        // TODO
    }

    /**
     * Retourne la liste de tous les résultats. Si le résultat est categorisé
     * par une population alors le nom de la population est automatiquement
     * ajouté au nom du résultat.
     */
    public List<String> getResultName() {
        List<String> result = new ArrayList<>(nameStepResults.keySet());
        return result;
    }
    
} 
