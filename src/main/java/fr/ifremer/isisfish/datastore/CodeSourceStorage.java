/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2012 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.datastore;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;

import org.apache.commons.io.FileUtils;

import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.IsisFishRuntimeException;

/**
 * Classes abstraite permettant la gestion des fichiers de code source.
 * 
 * {@link fr.ifremer.isisfish.datastore.ScriptStorage},
 * {@link fr.ifremer.isisfish.datastore.RuleStorage},
 * {@link fr.ifremer.isisfish.datastore.ExportStorage},
 * {@link fr.ifremer.isisfish.datastore.SimulationPlanStorage},
 * {@link fr.ifremer.isisfish.datastore.SensitivityAnalysisStorage}
 * {@link fr.ifremer.isisfish.datastore.SensitivityExportStorage}
 * 
 * Created: 21 janv. 2006 15:20:24
 *
 * @author poussin
 *
 * @version $Revision$
 *
 * Last update: $Date$ by : $Author$
 */
public abstract class CodeSourceStorage extends VersionStorage {

    /**
     * Location enum to look for script in official repository or
     * community directory.
     * 
     * Il y a un gros méchant hack qui tache car on ne peut pas initialiser
     * un repertoire en particulier pour OFFICIAL. Certaines fois, cela
     * depend du context de simulation, donc on ne la définie que lors de
     * l'appel a getDirectories().
     * 
     * Implements iterable to be used in JavaFileManager.
     */
    public enum Location {
        OFFICIAL(IsisFish.config.getDatabaseDirectory()),
        COMMUNITY(IsisFish.config.getCommunityDatabaseDirectory()),
        ALL(null, IsisFish.config.getCommunityDatabaseDirectory());

        protected File[] directory;
        Location(File... directory) {
            this.directory = directory;
        }

        public File[] getDirectories() {
            File[] result = new File[this.directory.length];
            for (int i = 0 ; i < result.length ; i++) {
                if (directory[i] == null) {
                    result[i] = IsisFish.config.getContextDatabaseDirectory();
                } else {
                    result[i] = directory[i];
                }
            }
            return result;
        }

        public void setDirectory(File... directory) {
            this.directory = directory;
        }
    }

    /**
     * Get non empty location.
     * 
     * @param location current location (if empty, return {@link Location#ALL}.
     */
    protected static Location nonEmptyLocation(Location... location) {
        Location result;
        if (location == null || location.length == 0) {
            result = Location.ALL;
        } else {
            result = location[0];
        }
        return result;
    }

    /**
     * Le nom de la classe sans le package.
     */
    protected String name;

    /**
     * Le contenu du fichier.
     */
    protected String content = null;

    /**
     * Date de derniere mise a jour de content, sert a detecter si content doit etre relu.
     */
    protected long lastContentUpdate = 0;

    /**
     * La derniere taille connu du content, sert a detecter si content doit etre relu.
     */
    protected long lastContentLength = 0;

    /**
     * Contruit un nouveau rule storage.
     *
     * @param rootSrc   repertoire racine des sources
     * @param directory le repertoire ou devrait se trouver la classe. Ce
     *                  répertoire doit etre un sous répertoire de rootSrc
     * @param name      le nom de la classe
     * @param suffix    l'extension des fichiers
     */
    protected CodeSourceStorage(File rootSrc, File directory, String name, String suffix) {
        // if name end with suffix we don't add suffix to filename
        super(rootSrc, name.endsWith(suffix) ? new File(directory, name) : new File(directory, name + suffix));
        this.name = name.endsWith(suffix) ? name : name + suffix;
    }

    @Override
    protected void prepare() {
        // Il n'y a rien a faire pour les code sources
    }

    @Override
    protected List<File> getFiles(boolean withParent) {
        
        // don't work in svn, commiting each parent
        // just return current script
        List<File> result = Collections.singletonList(getFile());

        return result;
    }

    /**
     * Check if script exists.
     * 
     * @return {@code true} if storage file exists
     */
    public boolean exists() {
        return getFile().exists();
    }

    /**
     * Get storage name.
     * 
     * @return the name.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Retourne le contenu du fichier. Si le fichier reel est plus recent
     * que la derniere lecture, alors il est relu automatiquement. Ce
     * mecanisme de relecture automatique peut ne pas fonctionner si entre
     * la derniere lecture de content et la modification directement du fichier
     * a ete fait dans la meme seconde et que le fichier modifier a la meme
     * taille que le precedent. Pour reellement forcer la relecture on peut
     * utiliser la methode {@link #reload()}}
     *
     * @return le contenu du fichier
     */
    public String getContent() {
        if (content == null || getFile().lastModified() > lastContentUpdate
                || getFile().length() != lastContentLength) {
            if (!exists()) {
                content = "";
            } else {
                try {
                    content = FileUtils.readFileToString(getFile(), StandardCharsets.UTF_8);
                    lastContentUpdate = System.nanoTime();
                    lastContentLength = getFile().length();
                } catch (IOException eee) {
                    throw new IsisFishRuntimeException("Can't get content", eee);
                }
            }
        }
        return content;
    }

    /**
     * Set file content.
     * 
     * @param content new content
     * @throws IOException
     */
    public void setContent(String content) throws IOException {
        setContent(content, true);
    }
    
    /**
     * Set file content.
     * 
     * @param content new content
     * @param saveToFile if {@code true} also save content to file
     * @throws IOException
     */
    public void setContent(String content, boolean saveToFile) throws IOException {
        this.content = content;
        if (saveToFile) {
            FileUtils.writeStringToFile(getFile(), content, "utf-8");
            lastContentUpdate = System.nanoTime();
            lastContentLength = getFile().length();
        }
    }

    /**
     * Permet de forcer la relecture du fichier sur le disque.
     */
    public void reload() {
        content = null;
    }
}
