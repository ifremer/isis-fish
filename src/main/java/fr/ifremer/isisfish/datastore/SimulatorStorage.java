/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2005 - 2019 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.datastore;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

import org.apache.commons.collections4.map.ReferenceMap;

import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.simulator.Simulator;
import fr.ifremer.isisfish.vcs.VCSException;

/**
 * Gestion des fichers CVS de type {@link Simulator} (appartenant au module Simulator).
 * 
 * Created: 18 août 2005 15:07:36 CEST
 *
 * @author Grégoire DESSARD &lt;dessard@codelutin.com&gt;
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class SimulatorStorage extends JavaSourceStorage {

    public static final String SIMULATOR_PATH = "simulators";

    /** Template freemarker pour les scripts. */
    public static final String SIMULATOR_TEMPLATE = "templates/script/simulator.ftl";

    /** Simulators cache. */
    static protected Map<String, SimulatorStorage> simulatorsCache = new ReferenceMap<>();

    protected static Set<StorageChangeListener> storageListeners = Collections.newSetFromMap(new WeakHashMap<>());

    public static void addStorageListener(StorageChangeListener storageListener) {
        storageListeners.add(storageListener);
    }

    /**
     * Constructeur.
     *
     * @param rootSrc
     */
    protected SimulatorStorage(File rootSrc, File directory, String name) {
        super(rootSrc, directory, name);
    }

    public static void registerWatcher() {
        registerWatcher(storageListeners, getSimulatorDirectory(), getCommunitySimulatorDirectory());
    }

    @Override
    public String getPackage() {
        return SIMULATOR_PATH;
    }

    /**
     * Get context (official VCS) simulator directory.
     * 
     * @return context simulator directory
     */
    public static File getSimulatorDirectory() {
        File result = new File(getContextDatabaseDirectory(), SIMULATOR_PATH);
        result.mkdirs();
        return result;
    }

    /**
     * Get community VCS simulator directory.
     * 
     * @return community simulator directory
     */
    public static File getCommunitySimulatorDirectory() {
        File result = new File(getCommunityDatabaseDirectory(), SIMULATOR_PATH);
        result.mkdirs();
        return result;
    }

    /**
     * Retourne le storage pour le simulateur demandée.
     * 
     * {@link SimulatorStorage} is cached by name.
     * 
     * @param name le nom de la regle souhaitée
     * @param location location to open storage file
     * @return Le storage pour la regle
     */
    static public SimulatorStorage getSimulator(String name, Location... location) {
        SimulatorStorage result = simulatorsCache.get(name);
        if (result == null) {
            Location loc = nonEmptyLocation(location);
            for (File dir : loc.getDirectories()) {
                SimulatorStorage storage = new SimulatorStorage(dir, new File(dir, SIMULATOR_PATH), name);
                if (storage.getFile().isFile()) {
                    result = storage;
                    simulatorsCache.put(name, result);
                }
            }
        }
        return result;
    }

    /**
     * Create new simulation plan.
     * 
     * @param name new simulation plan to create
     * @param location location to simulation plan storage file
     * @return new simulation plan storage
     */
    public static SimulatorStorage createSimulator(String name, Location location) {
        File dir = location.getDirectories()[0];
        SimulatorStorage storage = new SimulatorStorage(dir, new File(dir, SIMULATOR_PATH), name);
        return storage;
    }

    static public void checkout() throws VCSException {
        checkout(IsisFish.config.getDatabaseDirectory(), SIMULATOR_PATH);
    } 
    
    /**
     * Retourne la liste des noms de toutes les régions disponible en local
     * 
     * @return la liste des noms de toutes les régions disponible en local
     */
    static public List<String> getSimulatorNames() {
        List<String> result = getStorageNames(getSimulatorDirectory());
        result.addAll(getStorageNames(getCommunitySimulatorDirectory()));
        return result;
    }

    /**
     * Retourne la liste des noms de toutes les régions disponible en local qui
     * ne sont pas encore sur le serveur CVS
     * 
     * @return liste de noms de regions
     */
    static public List<String> getNewSimulatorNames() {
        List<String> result = getSimulatorNames();
        result.removeAll(getRemoteSimulatorNames());
        return result;
    }

    /**
     * Retourne la liste des noms de toutes les régions disponible sur le
     * serveur CVS
     * 
     * @return la liste des noms de toutes les régions disponible sur le serveur
     *         CVS. Si le serveur n'est pas disponible la liste retournée est
     *         vide.
     */
    static public List<String> getRemoteSimulatorNames() {
        File dir = getSimulatorDirectory();
        List<String> result = getRemoteStorageNames(dir);
        return result;

    }

    /**
     * Retourne la liste des noms de toutes les régions disponible sur le
     * serveur CVS qui ne sont pas encore en local
     * 
     * @return liste de noms de regions
     */
    static public List<String> getNewRemoteSimulatorNames() {
        List<String> result = getRemoteSimulatorNames();
        result.removeAll(getSimulatorNames());
        return result;
    }
}
