/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 - 2019 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.datastore;

import static org.nuiton.i18n.I18n.t;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

import org.apache.commons.collections4.map.ReferenceMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.result.ResultInfo;
import fr.ifremer.isisfish.annotations.Doc;
import fr.ifremer.isisfish.vcs.VCSException;

/**
 * This class define a result name.
 *
 * @since 4.4
 */
public class ResultInfoStorage extends JavaSourceStorage { // ResultInfoStorage

    /** to use log facility, just put in your code: log.info(\"...\"); */
    private static Log log = LogFactory.getLog(ResultInfoStorage.class);
    
    public static final String RESULT_INFO_PATH = "resultinfos";

    /** Template freemarker pour les noms de resultats. */
    public static final String RESULT_INFO_TEMPLATE = "templates/script/resultinfo.ftl";

    /** Instance cache. */
    protected static Map<String, ResultInfoStorage> resultInfosCache = new ReferenceMap<>();

    protected static Set<StorageChangeListener> storageListeners = Collections.newSetFromMap(new WeakHashMap<>());

    public static void addStorageListener(StorageChangeListener storageListener) {
        storageListeners.add(storageListener);
    }

    /**
     * Contruit un nouveau resultInfo storage
     *
     * @param rootSrc   La region auquelle est attaché le resultats
     * @param directory le repertoire ou devrait se trouver le resultat
     * @param name      le nom du resultat
     */
    protected ResultInfoStorage(File rootSrc, File directory, String name) {
        super(rootSrc, directory, name);
    }

    public static void registerWatcher() {
        registerWatcher(storageListeners, getResultInfoDirectory(), getCommunityResultInfoDirectory());
    }

    @Override
    public String getPackage() {
        return RESULT_INFO_PATH;
    }

    /**
     * Get context (official VCS) resultInfo directory.
     * 
     * @return context resultInfo directory
     */
    static public File getResultInfoDirectory() {
        File result = new File(getContextDatabaseDirectory(), RESULT_INFO_PATH);
        result.mkdirs();
        return result;
    }

    /**
     * Get community VCS resultInfo directory.
     * 
     * @return community resultInfo directory
     */
    public static File getCommunityResultInfoDirectory() {
        File result = new File(getCommunityDatabaseDirectory(), RESULT_INFO_PATH);
        result.mkdirs();
        return result;
    }

    /**
     * Retourne le nom de toutes les resultats existants pour cette region
     *
     * @return all result names found in local user database
     */
    static public List<String> getResultInfoNames() {
        List<String> rules = getStorageNames(getResultInfoDirectory());
        rules.addAll(getStorageNames(getCommunityResultInfoDirectory()));
        return rules;
    }

    /**
     * Retourne le storage pour le resultat demandé
     *
     * @param name le nom du resultat souhaité
     * @param location location to open storage file
     * @return Le storage pour le resultat
     */
    static public ResultInfoStorage getResultInfo(String name, Location... location) {
        ResultInfoStorage result = resultInfosCache.get(name);
        if (result == null) {
            Location loc = nonEmptyLocation(location);
            for (File dir : loc.getDirectories()) {
                File resultDirectory = new File(dir, RESULT_INFO_PATH);
                ResultInfoStorage storage = new ResultInfoStorage(dir, resultDirectory, name);
                File sFile = storage.getFile();
                if (sFile.isFile()) {
                    result = storage;
                    resultInfosCache.put(name, result);
                    break;
                } else {
                    System.out.println("Storage name " + name + ", " + sFile);
                    // dans les simulations, il se trouve qu'on se retrouve avec le fichier .class, mais
                    // pas le fichier .java
                    // cela semble être du à un problème de la méthode SimulationService#compileAllFile
                    // qui ne semble pas récupérer les dépendances de classe compilé sur plusieurs niveau
                    // exemple : DefaultSimulation > SiMatrix > ResultInfos.MatrixDiscardsWeightPerStrMetPerZonePop
                    /*String className = StringUtils.removeEnd(name, ".java") + ".class";
                    File classFile = new File(resultDirectory, className);
                    if (classFile.isFile()) {
                        result = storage;
                        resultInfosCache.put(name, result);
                    }*/
                }
            }
        }
        return result;
    }

    /**
     * Create new result name.
     * 
     * @param name new result name to create
     * @param location location to result name storage file
     * @return new rule storage
     */
    public static ResultInfoStorage createResultInfo(String name, Location location) {
        File dir = location.getDirectories()[0];
        ResultInfoStorage storage = new ResultInfoStorage(dir, new File(dir, RESULT_INFO_PATH), name);
        return storage;
    }

    static public void checkout() throws VCSException {
        checkout(IsisFish.config.getDatabaseDirectory(), RESULT_INFO_PATH);
    }

    /**
     * Retourne la liste des noms de toutes les resultats disponibles en local qui
     * ne sont pas encore sur le serveur VCS
     *
     * @return liste de noms de resultats
     */
    static public List<String> getNewResultInfoNames() {
        List<String> result = getResultInfoNames();
        result.removeAll(getRemoteResultInfoNames());
        return result;
    }

    /**
     * Retourne la liste des noms de toutes les régions disponible sur le
     * serveur VCS
     *
     * @return la liste des noms de toutes les régions disponible sur le serveur
     *         VCS. Si le serveur n'est pas disponible la liste retournée est
     *         vide.
     */
    static public List<String> getRemoteResultInfoNames() {
        File dir = getResultInfoDirectory();
        return getRemoteStorageNames(dir);
    }

    /**
     * Retourne la liste des noms de toutes les régions disponible sur le
     * serveur VCS qui ne sont pas encore en local
     *
     * @return liste de noms de regions
     */
    static public List<String> getNewRemoteResultInfoNames() {
        List<String> result = getRemoteResultInfoNames();
        result.removeAll(getResultInfoNames());
        return result;
    }

    /**
     * <b>Be ware this method require to instanciate a resultInfo, so
     * it would be better to call as often as possible.</b>
     *
     * @return the description of the instanciate Rule
     * @see Doc
     */
    public String getDescription() {
        String result = null;
        try {
            ResultInfo resultInfo = getNewInstance();
            result = resultInfo == null ? null : resultInfo.getDescription();
        } catch (Exception e) {
            log.warn(t("isisfish.error.not.found.description",this));
        }
        return result;
    }

} // ResultInfoStorage

