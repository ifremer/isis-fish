/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2018 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.mexico;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.ConvertUtilsBean;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.text.StringEscapeUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.nuiton.math.matrix.MatrixFactory;
import org.nuiton.math.matrix.MatrixIterator;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.persistence.TopiaDAO;
import org.nuiton.topia.persistence.TopiaEntity;
import org.xml.sax.InputSource;

import fr.ifremer.isisfish.IsisFishDAOHelper;
import fr.ifremer.isisfish.IsisFishException;
import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.datastore.RuleStorage;
import fr.ifremer.isisfish.mexico.xml.DesignPlanXMLVisitor;
import fr.ifremer.isisfish.mexico.xml.DomXMLParser;
import fr.ifremer.isisfish.rule.Rule;
import fr.ifremer.isisfish.rule.RuleHelper;
import fr.ifremer.isisfish.simulator.sensitivity.DesignPlan;
import fr.ifremer.isisfish.types.Month;
import fr.ifremer.isisfish.types.TimeStep;
import fr.ifremer.isisfish.util.converter.ConverterUtil;

/**
 * Mexico helper class.
 *
 * @author chatellier
 * @version $Revision$
 * 
 * @since 3.2.0.4
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class MexicoHelper {

    /** Class logger. */
    private static Log log = LogFactory.getLog(MexicoHelper.class);

    /**
     * Get xml representation of a design plan.
     * 
     * @param designPlan design plan
     * @param args some meta information to put into generated xml (author, date, id, license)
     * @return xml design plan representation
     */
    public static String getDesignPlanAsXML(DesignPlan designPlan, String... args) {

        DesignPlanXMLVisitor visitor = new DesignPlanXMLVisitor(args);
        designPlan.accept(visitor);
        String designPlanXml = visitor.getXML();

        // apply beautiful xml indented format
        designPlanXml = MexicoHelper.formatXML(designPlanXml);

        return designPlanXml;
    }

    /**
     * Convert design plan to xml and write it to given file.
     * 
     * @param designPlan
     */
    public static void writeDesignPlanToFile(File file, DesignPlan designPlan) {
        String content = getDesignPlanAsXML(designPlan);
        try {
            FileUtils.writeStringToFile(file, content, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            throw new IsisFishRuntimeException("Can't export factors to xml file", ex);
        }
    }

    /**
     * Parse xmlFile with sax, and return a {@link DesignPlan}.
     * 
     * @param xmlFile file path to parse
     * @param topiaContext database context
     * @return DesignPlan
     * @throws IOException 
     */
    public static DesignPlan getDesignPlanFromXML(File xmlFile, TopiaContext topiaContext)
            throws IOException {
        DesignPlan designPlan;
        try {
            SAXReader reader = new SAXReader();
            // don't use reader.read(String);
            // don't work on windows because of : in path
            // Document doc = reader.read(xmlFile);
            reader.setEncoding("utf-8");
            Document doc = reader.read(xmlFile);
            Element root = doc.getRootElement();
            designPlan = DomXMLParser.parseDesignPlan(root, topiaContext);
        } catch (DocumentException e) {
            throw new IOException(e);
        }
        return designPlan;
    }
    
    /**
     * Format xml string.
     * 
     * @param unformattedXml non formatted xml string (must be valid xml)
     * @return xml, formatted and indented
     * 
     * Code from http://stackoverflow.com/questions/139076/how-to-pretty-print-xml-from-java
     * 
     * @throws IsisFishRuntimeException
     * @throws IllegalArgumentException if input xml is not valid
     */
    public static String formatXML(String unformattedXml) {
        try {
            Transformer serializer= SAXTransformerFactory.newInstance().newTransformer();
            serializer.setOutputProperty(OutputKeys.INDENT, "yes");
            serializer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            Source xmlSource = new SAXSource(new InputSource(new ByteArrayInputStream(unformattedXml.getBytes(StandardCharsets.UTF_8))));
            StreamResult res = new StreamResult(new ByteArrayOutputStream());
            serializer.transform(xmlSource, res);
            return new String(((ByteArrayOutputStream)res.getOutputStream()).toByteArray(), StandardCharsets.UTF_8);
        } catch (Exception e) {
            throw new IsisFishRuntimeException("Can't format xml", e);
        }
    }
    
    /**
     * Transform matrix into XML mexico format.
     * 
     * Format is :
     * <pre>
     *   &lt;mx name="une matrice"&gt;
     *    &lt;dimension name="classe" size="2"&gt;
     *      &lt;label&gt;jeune&lt;/label&gt;
     *      &lt;label&gt;vieux&lt;/label&gt;
     *    &lt;/dimension&gt;
     *    &lt;dimension name="x" size="3"&gt;
     *      &lt;label&gt;0&lt;/label&gt;
     *      &lt;label&gt;1&lt;/label&gt;
     *      &lt;label&gt;2&lt;/label&gt;
     *    &lt;/dimension&gt;
     *    &lt;d&gt;2.0&lt;/d&gt;
     *    &lt;d&gt;3.1415&lt;/d&gt;
     *    &lt;d&gt;2.0&lt;/d&gt;
     *    &lt;d&gt;3.1415&lt;/d&gt;
     *    &lt;d&gt;2.0&lt;/d&gt;
     *    &lt;d&gt;3.1415&lt;/d&gt;
     *  &lt;/mx&gt;
     * </pre>
     * 
     * @param matrix matrix
     * @return matrix as xml or {@code null} if matrix is null
     * @since 3.3.0.0
     */
    public static String getMatrixAsXML(MatrixND matrix) {
        
        if (matrix == null) {
            return null;
        }

        StringBuffer buffer = new StringBuffer();

        // matrix name
        buffer.append("<mx name=\"" + matrix.getName() + "\">");
        
        // matrix dimensions
        for (int dimIndex = 0 ; dimIndex < matrix.getDim().length ; dimIndex++) {
            List<?> semantics = matrix.getSemantic(dimIndex);
            buffer.append("<dimension name=\"" + matrix.getDimensionName(dimIndex) + "\" size=\"" + semantics.size() + "\">");
            for (Object semantic : semantics) {
                buffer.append("<label>");
                appendString(buffer, semantic);
                buffer.append("</label>");
            }
            buffer.append("</dimension>");
        }

        // matrix data
        for (MatrixIterator mi = matrix.iterator(); mi.next();) {
            // d for double
            buffer.append("<d>" + mi.getValue() + "</d>");
        }

        buffer.append("</mx>");

        return buffer.toString();
    }

    /**
     * Append object type and value in stringbuffer.
     * 
     * Append it as :
     * <pre>fqn(value)</pre>
     * 
     * @param buffer buffer to append to
     * @param o value to append
     * @return stringbuffer
     */
    protected static StringBuffer appendString(StringBuffer buffer, Object o) {
        if (o == null) {
            buffer.append("null()");
        } else {
            String qualifiedName = getQualifiedName(o);
            buffer.append(qualifiedName).append("(");
            ConvertUtilsBean beanUtils = ConverterUtil.getConverter(null);
            buffer.append(beanUtils.convert(o));
            buffer.append(")");
        }
        return buffer;
    }

    /**
     * Parse a dom element (mx) as a {@link MatrixND}.
     * 
     * Format is :
     * <pre>
     *   &lt;mx name="une matrice"&gt;
     *    &lt;dimension name="classe" size="2"&gt;
     *      &lt;label&gt;jeune&lt;/label&gt;
     *      &lt;label&gt;vieux&lt;/label&gt;
     *    &lt;/dimension&gt;
     *    &lt;dimension name="x" size="3"&gt;
     *      &lt;label&gt;0&lt;/label&gt;
     *      &lt;label&gt;1&lt;/label&gt;
     *      &lt;label&gt;2&lt;/label&gt;
     *    &lt;/dimension&gt;
     *    &lt;d&gt;2.0&lt;/d&gt;
     *    &lt;d&gt;3.1415&lt;/d&gt;
     *    &lt;d&gt;2.0&lt;/d&gt;
     *    &lt;d&gt;3.1415&lt;/d&gt;
     *    &lt;d&gt;2.0&lt;/d&gt;
     *    &lt;d&gt;3.1415&lt;/d&gt;
     *  &lt;/mx&gt;
     * </pre>
     * 
     * @param mxElement dom element
     * @param context topia context
     * @return matrix nd
     */
    public static MatrixND getMatrixFromXml(Element mxElement, TopiaContext context) {

        String name = mxElement.attributeValue("name");

        // get dimension names and semantics
        List<String> dimNames = new ArrayList<>();
        List<List<?>> semantics = new ArrayList<>();

        List<Element> dimensionElements = mxElement.elements("dimension");
        for (Element dimensionElement : dimensionElements) {
            String dimName = dimensionElement.attributeValue("name");
            dimNames.add(dimName);

            // parse sub semantics
            List<Object> semantic = new ArrayList<>();
            List<Element> labelElements = dimensionElement.elements("label");
            for (Element labelElement : labelElements) {
                String content = labelElement.getText();
                Object value = null;

                if (content != null) {
                    content = content.trim();

                    Pattern matrixPattern = Pattern.compile("^(.*)\\((.*)\\)$");
                    Matcher matcher = matrixPattern.matcher(content);

                    if (matcher.find()) {
                        String objectType = matcher.group(1);
                        String objectString = matcher.group(2);

                        if (log.isDebugEnabled()) {
                            log.debug("Looking for object : " + objectType + ":" + objectString);
                        }

                        if (!"null".equals(objectType)) {
                            ConvertUtilsBean beanUtils = ConverterUtil.getConverter(context);
                            try {
                                value = beanUtils.convert(objectString, Class.forName(objectType));
                            } catch (Exception e) {
                                // if can't create object, put String representation as semantics
                                value = objectType + "(" + objectString + ")";
                                if (log.isWarnEnabled()) {
                                    log.warn("Can't parse '" + content + "' as valid semantic");
                                }
                            }
                        }
                    }
                    else {
                        if (log.isWarnEnabled()) {
                            log.warn("Can't parse '" + content + "' as valid semantic");
                        }
                    }
                }
                // always add value even if value is null
                semantic.add(value);
            }
            semantics.add(semantic);
        }

        MatrixND result = MatrixFactory.getInstance().create(name,
                semantics.toArray(new List<?>[0]),
                dimNames.toArray(new String[0]));

        MatrixIterator iterator = result.iterator();
        // TODO it's d for double here, can be int...
        List<Element> values = mxElement.elements("d");
        for (Element value : values) {
            iterator.next();
            String text = value.getText().trim();
            double doubleValue = Double.parseDouble(text);
            iterator.setValue(doubleValue);
        }

        return result;
    }

    /**
     * Return object fully qualified name excepted for {@link TopiaEntity}.
     * 
     * @param o object to get fqn
     * @return fqn for mexico file format
     */
    protected static String getQualifiedName(Object o) {
        String qualifiedName;
        if (o instanceof TopiaEntity) {
            qualifiedName = TopiaEntity.class.getName();
        }
        else {
            qualifiedName = o.getClass().getName();
        }
        return qualifiedName;
    }
    
    /**
     * Transform rules list as xml.
     * 
     * Format is :
     * <pre>
     *  &lt;rules&gt;
     *    &lt;rule name="RuleName1"&gt;
     *      &lt;param key="rule.0.parameter.param1"&gt;param1&lt;/param&gt;
     *      &lt;param key="rule.0.parameter.pop"&gt;topiaId1&lt;/param&gt;
     *    &lt;/rule&gt;
     *    &lt;rule name="RuleName2"&gt;
     *      &lt;param key="rule.0.parameter.param1"&gt;param2&lt;/param&gt;
     *      &lt;param key="rule.0.parameter.pop"&gt;topiaId2&lt;/param&gt;
     *    &lt;/rule&gt;
     *  &lt;/rules&gt;
     * </pre>
     * 
     * @param rules rules list
     * @return rules as xml
     */
    public static String getRulesAsXml(List<Rule> rules) {
        StringBuilder ruleAsString = new StringBuilder();
        ruleAsString.append("<rules>");
        for (Rule rule : rules) {
            Properties props = RuleHelper.getRuleAsProperties(0, null, rule);
            
            // rule name and rule parameters
            ruleAsString.append("<rule name=\"");
            ruleAsString.append(rule.getClass().getSimpleName());
            ruleAsString.append("\">");
            for (String propName : props.stringPropertyNames()) {
                ruleAsString.append("<param key=\"");
                ruleAsString.append(propName);
                ruleAsString.append("\">");
                ruleAsString.append(StringEscapeUtils.escapeXml11(props.getProperty(propName)));
                ruleAsString.append("</param>");
            }
            ruleAsString.append("</rule>");
        }
        ruleAsString.append("</rules>");
        return ruleAsString.toString();
    }

    /**
     * Parse xml as rules list.
     * 
     * Format is :
     * <pre>
     *  &lt;rules&gt;
     *    &lt;rule name="RuleName1"&gt;
     *      &lt;param key="rule.0.parameter.param1"&gt;param1&lt;/param&gt;
     *      &lt;param key="rule.0.parameter.pop"&gt;topiaId1&lt;/param&gt;
     *    &lt;/rule&gt;
     *    &lt;rule name="RuleName2"&gt;
     *      &lt;param key="rule.0.parameter.param1"&gt;param2&lt;/param&gt;
     *      &lt;param key="rule.0.parameter.pop"&gt;topiaId2&lt;/param&gt;
     *    &lt;/rule&gt;
     *  &lt;/rules&gt;
     * </pre>
     * 
     * @param rulesElement rules dom element
     * @param topiaContext topia context (for rules parameters)
     * @return rules as xml
     */
    public static List<Rule> getRulesFromXml(Element rulesElement, TopiaContext topiaContext) {
        List<Rule> rules = new ArrayList<>();
        List<Element> ruleElements = rulesElement.elements("rule");
        for (Element ruleElement : ruleElements) {
            String name = ruleElement.attributeValue("name");
            try {
                Rule rule = RuleStorage.getRule(name).getNewInstance();

                // get properties
                Properties props = new Properties();
                List<Element> paramElements = ruleElement.elements("param");
                for (Element paramElement : paramElements) {
                    String key = paramElement.attributeValue("key");
                    String value = paramElement.getTextTrim();
                    props.setProperty(key, value);
                }
                RuleHelper.populateRule(0, topiaContext, rule, props);

                rules.add(rule);
            }
            catch (IsisFishException ex) {
                if (log.isWarnEnabled()) {
                    log.warn("Can't make instance of rule " + name, ex);
                }
            }
        }
        
        return rules;
    }

    /**
     * Parse string content (human readable) to get isis fish instance for
     * this object.
     * 
     * For example:
     * <ul>
     *  <li>Species:Nephrops</li>
     *  <li>Population:Anchois</li>
     *  <li>TimeStep:21</li>
     *  <li>...</li>
     * </ul>
     * 
     * @param content content to parse
     * @param context context to database
     * @return object instance
     */
    public static Object getObjectFromString(String content, TopiaContext context) {

        Object result = null;

        int semiIndex = content.indexOf(':');
        if (semiIndex != -1) {
            String clazzName = content.substring(0, semiIndex);
            String objectId = content.substring(semiIndex + 1);

            // first test entities
            try {
                Method m = IsisFishDAOHelper.class.getMethod("get" + clazzName + "DAO", TopiaContext.class);
                TopiaDAO dao = (TopiaDAO)m.invoke(null, context);
                result = dao.findByProperty("name", objectId);
            } catch (Exception ex) {
                if (log.isDebugEnabled()) {
                    log.debug("Can't find object for topia entity", ex);
                }
            }

            // try some other types
            if (result == null) {
                ConvertUtilsBean beanUtils = ConverterUtil.getConverter(null);
                if ("TimeStep".equals(clazzName)) {
                    result = beanUtils.convert(objectId, TimeStep.class);
                } else if ("Month".equals(clazzName)) {
                    result = beanUtils.convert(objectId, Month.class);
                }
            }
        } else {
            // ca peut arriver pour des parametres de type "String" sur les param_ de regles.
            result = content;
        }

        if (result == null && log.isWarnEnabled()) {
            log.warn("Can't convert '" + content + "' to isis object");
        }

        return result;
    }

    /**
     * Get string representation (human readable) for an object.
     * 
     * @param object
     * @return string representation
     */
    public static String getStringFromObject(Object object) {
        String representation = null;
        
        if (object instanceof TopiaEntity) {
            String name;
            try {
                String clazzName = object.getClass().getSimpleName();
                // Impl is not needed
                clazzName = StringUtils.removeEnd(clazzName, "Impl");
                name = BeanUtils.getProperty(object, "name");
                representation = clazzName + ":" + name;
            } catch (Exception ex) {
                if (log.isWarnEnabled()) {
                    log.error("No getName() method found on " + object, ex);
                }
            }
            
        } else if (object instanceof TimeStep || object instanceof Month) {
            ConvertUtilsBean beanUtils = ConverterUtil.getConverter(null);
            representation = object.getClass().getSimpleName() + ":" + beanUtils.convert(object); 
        } else {
            // regular primitive : double, int ...
            representation = object.toString();
        }

        return representation;
    }
}
