/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2010 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.mexico.export;

import org.nuiton.topia.persistence.EntityVisitor;
import org.nuiton.topia.persistence.HorizontalEntityVisitor;
import org.nuiton.topia.persistence.TopiaEntity;

import fr.ifremer.isisfish.entities.Equation;

/**
 * Parcourt en largeur du model et délegation à un autre visiteur.
 * 
 * Extend {@link HorizontalEntityVisitor} from topia to add
 * a hack about internal entity equation.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class RegionEntityVisitor extends HorizontalEntityVisitor {

    /**
     * Constructor.
     * 
     * @param delegateVisitor delegate visitor
     */
    public RegionEntityVisitor(EntityVisitor delegateVisitor) {
        super(delegateVisitor);
    }

    @Override
    public void visit(TopiaEntity e, String name, Class<?> type, Object value) {
        // si c'est une entité
        
        // FIXME only hack about Equation is used instead of Topia visitor
        if (value instanceof TopiaEntity && !(value instanceof Equation)) {
            TopiaEntity entity = (TopiaEntity) value;
            toVisitEntities.add(entity);
        } else {
            delegateVisitor.visit(e, name, type, value);
        }
    }

} // HorizontallyEntityVisitor
