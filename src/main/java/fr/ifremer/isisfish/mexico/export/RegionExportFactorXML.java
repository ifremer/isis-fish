/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2018 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.mexico.export;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.text.StringEscapeUtils;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.topia.persistence.TopiaEntity;

import fr.ifremer.isisfish.entities.EffortDescription;
import fr.ifremer.isisfish.entities.Equation;
import fr.ifremer.isisfish.entities.PopulationGroup;
import fr.ifremer.isisfish.entities.PopulationSeasonInfo;
import fr.ifremer.isisfish.entities.Selectivity;
import fr.ifremer.isisfish.entities.StrategyMonthInfo;
import fr.ifremer.isisfish.entities.TargetSpecies;
import fr.ifremer.isisfish.mexico.MexicoHelper;
import fr.ifremer.isisfish.simulator.sensitivity.SensitivityUtils;
import fr.ifremer.isisfish.types.RangeOfValues;
import fr.ifremer.isisfish.types.TimeUnit;

/**
 * Export implementation for factor into xml.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class RegionExportFactorXML implements RegionExport {

    /** Class logger. */
    private static Log log = LogFactory.getLog(RegionExportFactorXML.class);

    protected File outputFile;

    protected StringBuffer stringBuffer;

    protected String lastReadName;

    public RegionExportFactorXML(File outputFile) {
        this.outputFile = outputFile;
    }

    @Override
    public void beginExport() {
        stringBuffer = new StringBuffer();
        stringBuffer.append("<regionFactors>");
        stringBuffer.append("<factors>");
    }

    @Override
    public void start(TopiaEntity entity) {
        lastReadName = null;
    }

    @Override
    public void visit(TopiaEntity entity, String propertyName, Class<?> type, Object value) {

        // FIXME this mechanism won't work if name is not the first attribute
        // in visitor. (need to be fixed)
        if ("name".equals(propertyName)) {
            lastReadName = (String)value;
        }

        // build factor name (entityName.propertyName)
        String factorClass = entity.getClass().getSimpleName();
        // if there is a "Impl" in entityName, remove it
        factorClass = factorClass.replaceFirst("Impl", "");

        String factorName = factorClass + "." + propertyName;

        // test if factor name is enabled
        boolean enabled = SensitivityUtils.isSensitivityFactorEnabled(factorName);

        if (log.isDebugEnabled()) {
            log.debug("Testing if attribute (" + factorName + ") is factor enabled : " + enabled);
        }

        if (enabled) {
            String exportFactorName = getUniqueFactorName(entity, factorClass, propertyName);
            String stringValue = getStringValue(entity, type, value);
            
            stringBuffer.append("<!-- " + factorName + " -->");
            stringBuffer.append("<factor name=\"" + exportFactorName + "\"");
            stringBuffer.append(" type=\"" + type.getSimpleName() + "\"");
            stringBuffer.append(">");
            stringBuffer.append("<target>" + entity.getTopiaId() + "#" + propertyName + "</target>");
            stringBuffer.append("<value>" + stringValue + "</value>");
            stringBuffer.append("</factor>");
        }
    }

    @Override
    public void visit(TopiaEntity entity, String propertyName, Class<?> collectionType,
            Class<?> type, Object value) {
        visit(entity, propertyName, type, value);
    }

    @Override
    public void visit(TopiaEntity entity, String propertyName, Class<?> collectionType,
            Class<?> type, int index, Object value) {

    }

    @Override
    public void end(TopiaEntity entity) {

    }

    @Override
    public void clear() {

    }

    @Override
    public void endExport() {
        stringBuffer.append("</factors>");
        stringBuffer.append("</regionFactors>");
        String xml = MexicoHelper.formatXML(stringBuffer.toString());
        
        try {
            FileUtils.writeStringToFile(outputFile, xml, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            if (log.isErrorEnabled()) {
                log.error("Can't wrtie xml content to output file", ex);
            }
        }
    }

    /**
     * Get factor name.
     * 
     * Dans la majorité des cas, on a un {@link #lastReadName} défini. Dans les
     * autre cas, il faut chercher un meilleur nom.
     * 
     * @param entity entity to get factor name
     * @param factorClass entity short class name
     * @param propertyName property name
     * @return unique name for factor
     */
    protected String getUniqueFactorName(TopiaEntity entity, String factorClass,
            String propertyName) {
        
        String exportFactorName = null;
        
        if (lastReadName != null) {
            exportFactorName = factorClass + "." + lastReadName + "." + propertyName;
        }
        else {
            // example : EffortDescription.TfjMJyI2.BolincheProfil1gir.fishingOperation
            if (entity instanceof EffortDescription) {
                EffortDescription newEntity = (EffortDescription)entity;
                // FIXME newEntity.getSetOfVessels() is null ???
                exportFactorName = factorClass + ".";
                exportFactorName += RandomStringUtils.randomAlphanumeric(8); // getSetOfVessels().getName()
                exportFactorName += "." + newEntity.getPossibleMetiers() + "." + propertyName;
            }
            // example : PopulationGroup.Population_new.0.reproductionRate
            else if (entity instanceof PopulationGroup) {
                PopulationGroup newEntity = (PopulationGroup)entity;
                exportFactorName = factorClass + "." + newEntity.getPopulation().getName();
                exportFactorName += "." + newEntity.getId() + "." + propertyName;
            }
            // example : PopulationSeasonInfo.Population_new.janvier.avril.lengthChangeMatrix
            else if (entity instanceof PopulationSeasonInfo) {
                PopulationSeasonInfo newEntity = (PopulationSeasonInfo)entity;
                exportFactorName = factorClass + "." + newEntity.getPopulation().getName() + ".";
                exportFactorName += newEntity.getFirstMonth() + "." + newEntity.getLastMonth() + "." + propertyName;
            }
            // example : StrategyMonthInfo.Espagnols.janvier.minInactivityDays
            else if (entity instanceof StrategyMonthInfo) {
                StrategyMonthInfo newEntity = (StrategyMonthInfo)entity;
                exportFactorName = factorClass + "." + newEntity.getStrategy().getName() + ".";
                exportFactorName += newEntity.getMonth() + "." + propertyName;
            }
            // example : Selectivity.Senne.Anchois_long.equation
            else if (entity instanceof Selectivity) {
                // association class with no name attribute
                Selectivity newEntity = (Selectivity)entity;
                exportFactorName = factorClass + "." + newEntity.getGear().getName();
                exportFactorName += "." + newEntity.getPopulation().getName() + "." + propertyName;
            }
            // example : TargetSpecies.Anchois_long.janvier-decembre.
            else if (entity instanceof TargetSpecies) {
                // association class with no name attribute
                TargetSpecies newEntity = (TargetSpecies)entity;
                exportFactorName = factorClass + "." + newEntity.getSpecies().getName() + ".";
                exportFactorName += newEntity.getMetierSeasonInfo().getFirstMonth() + "." + newEntity.getMetierSeasonInfo().getLastMonth();
                exportFactorName += "." + propertyName + "." + RandomStringUtils.randomAlphanumeric(8);
            }
        }
        
        return exportFactorName;
    }
    
    /**
     * Get string value.
     * 
     * @param entity entity
     * @param type value type
     * @param value value
     */
    protected String getStringValue(TopiaEntity entity, Class<?> type, Object value) {

        String result = null;

        if (type.isAssignableFrom(TimeUnit.class)) {
            result = String.valueOf(((TimeUnit)value).getTime());
        }
        else if (type.isAssignableFrom(MatrixND.class)) {
            result = MexicoHelper.getMatrixAsXML((MatrixND)value);
        }
        else if (type.isAssignableFrom(RangeOfValues.class)) {
            result = String.valueOf(((RangeOfValues)value).getValues());
        }
        else if (type.isAssignableFrom(Equation.class)) {
            // not initialized equation can be null :(
            if (value != null) {
                result = String.valueOf(((Equation)value).getContent());
                result = StringEscapeUtils.escapeXml11(result);
            }
        }
        // default case, just toString() on value
        else if (value != null) {
            result = value.toString();
        }

        return result;
    }
}
