/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2011 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.mexico.export;

import org.nuiton.topia.TopiaException;

import fr.ifremer.isisfish.entities.Cell;
import fr.ifremer.isisfish.entities.FisheryRegion;
import fr.ifremer.isisfish.entities.Gear;
import fr.ifremer.isisfish.entities.Metier;
import fr.ifremer.isisfish.entities.Port;
import fr.ifremer.isisfish.entities.SetOfVessels;
import fr.ifremer.isisfish.entities.Species;
import fr.ifremer.isisfish.entities.Strategy;
import fr.ifremer.isisfish.entities.TripType;
import fr.ifremer.isisfish.entities.VesselType;
import fr.ifremer.isisfish.entities.Zone;

/**
 * Region explorer.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class RegionExplorer {

    /**
     * Explore region and call export on each entity found.
     * 
     * @param region region to explore
     * @param regionExport region export implementation
     * @throws TopiaException 
     */
    public void explore(FisheryRegion region, RegionExport regionExport) throws TopiaException {

        regionExport.beginExport();

        RegionEntityVisitor visitor = new RegionEntityVisitor(regionExport);

        // explore cells
        for (Cell cell : region.getCell()) {
            cell.accept(visitor);
        }

        // explore zone
        for (Zone zone : region.getZone()) {
            zone.accept(visitor);
        }

        // explore ports
        for (Port port : region.getPort()) {
            port.accept(visitor);
        }

        // explore species and population
        for (Species species : region.getSpecies()) {
            species.accept(visitor);
        }

        // explore gears
        for (Gear gear : region.getGear()) {
            gear.accept(visitor);
        }

        // explore metier
        for (Metier metier : region.getMetier()) {
            metier.accept(visitor);
        }

        // explore tripType
        for (TripType tripType : region.getTripType()) {
            tripType.accept(visitor);
        }

        // explore vesselType
        for (VesselType vesselType : region.getVesselType()) {
            vesselType.accept(visitor);
        }

        // explore setOfVessels
        for (SetOfVessels setOfVessels : region.getSetOfVessels()) {
            setOfVessels.accept(visitor);
        }

        // explore strategy
        for (Strategy strategy : region.getStrategy()) {
            strategy.accept(visitor);
        }

        regionExport.endExport();
    }
} // RegionExplorer
