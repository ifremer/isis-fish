/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2012 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.mexico.xml;

import java.util.Map.Entry;

import org.nuiton.math.matrix.MatrixND;

import fr.ifremer.isisfish.mexico.MexicoHelper;
import fr.ifremer.isisfish.simulator.sensitivity.Domain;
import fr.ifremer.isisfish.simulator.sensitivity.domain.ContinuousDomain;

/**
 * XML continuous domain visitor.
 *
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class ContinuousDomainXMLVisitor extends DomainXMLVisitor {

    /**
     * {@inheritDoc}
     */
    @Override
    public void start(Domain domain) {
        ContinuousDomain cDomain = (ContinuousDomain)domain;
        xmlBuffer.append("<domain distributionName=\"" + cDomain.getDistribution().toString().toLowerCase() + "\">");
        for (Entry<String, Object> distributionParameterEntry : cDomain.getDistributionParameters().entrySet()) {
            String name = distributionParameterEntry.getKey();
            Object value = distributionParameterEntry.getValue();
            xmlBuffer.append("<distributionParameter name=\"" + name + "\"");
            appendParamValue(xmlBuffer, value);
            xmlBuffer.append("</distributionParameter>");
        }
    }

    /**
     * Append value type and value content.
     * 
     * @param xmlBuffer buffer
     * @param value value to append
     */
    protected void appendParamValue(StringBuffer xmlBuffer, Object value) {
        if (value instanceof MatrixND) {
            xmlBuffer.append(" type=\"matrix\">");
            xmlBuffer.append(MexicoHelper.getMatrixAsXML((MatrixND)value));
        } else {
            xmlBuffer.append(" type=\"decimal\">");
            xmlBuffer.append(value.toString());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void visit(Domain domain, Object label, Object value) {

    }

    @Override
    public void end(Domain domain) {
        xmlBuffer.append("</domain>");
    }
}
