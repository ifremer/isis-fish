/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.mexico.xml;

import fr.ifremer.isisfish.simulator.sensitivity.Factor;
import fr.ifremer.isisfish.simulator.sensitivity.FactorGroup;
import fr.ifremer.isisfish.simulator.sensitivity.visitor.FactorGroupVisitor;

/**
 * XML factor group visitor.
 *
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class FactorGroupXMLVisitor extends AbstractXMLVisitor implements FactorGroupVisitor {

    @Override
    public void start(FactorGroup factorGroup) {

    }

    @Override
    public void visit(FactorGroup factorGroup, Factor factor) {
        if (factor instanceof FactorGroup) {
            FactorGroup factorGroup2 = (FactorGroup)factor;
            factorGroup2.accept(this);
        }
        else {
            FactorXMLVisitor visitor = new FactorXMLVisitor(factorGroup);
            factor.accept(visitor);
            xmlBuffer.append(visitor.getXML());
        }
    }

    @Override
    public void end(FactorGroup factorGroup) {

    }
}
