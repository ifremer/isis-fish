/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2012 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.mexico.xml;

import org.apache.commons.lang3.StringUtils;

import fr.ifremer.isisfish.simulator.sensitivity.Domain;
import fr.ifremer.isisfish.simulator.sensitivity.Factor;
import fr.ifremer.isisfish.simulator.sensitivity.FactorGroup;
import fr.ifremer.isisfish.simulator.sensitivity.domain.ContinuousDomain;
import fr.ifremer.isisfish.simulator.sensitivity.domain.DiscreteDomain;
import fr.ifremer.isisfish.simulator.sensitivity.domain.EquationDiscreteDomain;
import fr.ifremer.isisfish.simulator.sensitivity.domain.RuleDiscreteDomain;
import fr.ifremer.isisfish.simulator.sensitivity.visitor.FactorVisitor;

/**
 * XML factor visitor.
 *
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class FactorXMLVisitor extends AbstractXMLVisitor implements FactorVisitor {

    protected FactorGroup group;

    /**
     * Factor constructor with group (for mexico features).
     * 
     * @param group group
     */
    public FactorXMLVisitor(FactorGroup group) {
        this.group = group;
    }

    @Override
    public void start(Factor factor) {

        // required by mexico, must be unique, but not used by isis
        String factorId = factor.getPath();
        if (StringUtils.isNotBlank(factor.getEquationVariableName())) {
            factorId += "." + factor.getEquationVariableName();
        }

        xmlBuffer.append("<factor");
        xmlBuffer.append(" id=\"").append(factorId).append("\"");
        xmlBuffer.append(" name=\"").append(factor.getName()).append("\"");
        if (StringUtils.isNotBlank(factor.getComment())) {
            xmlBuffer.append(" description=\"").append(factor.getComment()).append("\"");
        }
        xmlBuffer.append(">");

        // other things are features
        //cardinality
        xmlBuffer.append("<feature name=\"cardinality\">" + factor.getCardinality() + "</feature>");
        // group
        if (StringUtils.isNotBlank(group.getName())) {
            xmlBuffer.append("<feature name=\"group\">" + group.getName() + "</feature>");
            xmlBuffer.append("<feature name=\"grouptype\">");
            if (group.isContinuous()) {
                xmlBuffer.append("continuous");
            } else {
                xmlBuffer.append("discrete");
            }
            xmlBuffer.append("</feature>");
        }
        //path
        xmlBuffer.append("<feature name=\"target\">" + factor.getPath() + "</feature>");
        // equation variable name
        if (StringUtils.isNotBlank(factor.getEquationVariableName())) {
            xmlBuffer.append("<feature name=\"equationVariableName\">" + factor.getEquationVariableName() + "</feature>");
        }
    }

    @Override
    public void visit(Factor factor, Domain domain) {
        DomainXMLVisitor visitor = null;
        if (factor.getDomain() instanceof ContinuousDomain) {
            visitor = new ContinuousDomainXMLVisitor();
        } else if (factor.getDomain() instanceof RuleDiscreteDomain) {
            visitor = new RuleDiscreteDomainXMLVisitor();
        } else if (factor.getDomain() instanceof EquationDiscreteDomain) {
            visitor = new EquationDiscreteDomainXMLVisitor();
        } else if (factor.getDomain() instanceof DiscreteDomain) {
            visitor = new DiscreteDomainXMLVisitor();
        }
        domain.accept(visitor);
        xmlBuffer.append(visitor.getXML());
    }

    @Override
    public void end(Factor factor) {
        xmlBuffer.append("</factor>");
    }
}
