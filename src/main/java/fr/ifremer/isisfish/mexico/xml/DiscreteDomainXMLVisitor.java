/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2012 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.mexico.xml;

import fr.ifremer.isisfish.mexico.MexicoHelper;
import fr.ifremer.isisfish.simulator.sensitivity.Domain;
import fr.ifremer.isisfish.simulator.sensitivity.domain.DiscreteDomain;
import org.nuiton.math.matrix.MatrixND;

/**
 * XML discrete domain visitor.
 *
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class DiscreteDomainXMLVisitor extends DomainXMLVisitor {

    @Override
    public void start(Domain domain) {
        DiscreteDomain discreteDomain = (DiscreteDomain)domain;

        // lorsque le facteur est catégoriel, la liste de ses niveaux, par une
        // liste ouverte d'éléments 'level', d'attributs 'value' et 'weight')
        xmlBuffer.append("<domain distributionName=\"categorical\" type=\"");

        if (!discreteDomain.getValues().isEmpty()) {
            Object first = discreteDomain.getValues().firstKey();
            Object firstValue = discreteDomain.getValues().get(first);

            if (firstValue instanceof Integer) {
                xmlBuffer.append("integer");
            } else if (firstValue instanceof Double) {
                xmlBuffer.append("double");
            } else if (firstValue instanceof MatrixND) {
                xmlBuffer.append("matrix");
            } else {
                xmlBuffer.append("string");
            }
        }

        xmlBuffer.append("\">");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void visit(Domain domain, Object label, Object value) {
        xmlBuffer.append("<level>");
        if (value instanceof MatrixND) {
            xmlBuffer.append(MexicoHelper.getMatrixAsXML((MatrixND)value));
        } else {
            xmlBuffer.append(MexicoHelper.getStringFromObject(value));
        }
        xmlBuffer.append("</level>");
    }

    @Override
    public void end(Domain domain) {
        xmlBuffer.append("</domain>");
    }
}
