/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2012 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.mexico.xml;

import java.util.Date;

import org.apache.commons.lang3.ArrayUtils;

import fr.ifremer.isisfish.simulator.sensitivity.DesignPlan;
import fr.ifremer.isisfish.simulator.sensitivity.FactorGroup;
import fr.ifremer.isisfish.simulator.sensitivity.visitor.DesignPlanVisitor;

/**
 * XML design plan visitor.
 *
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class DesignPlanXMLVisitor extends AbstractXMLVisitor implements DesignPlanVisitor {

    protected String[] args;

    public DesignPlanXMLVisitor(String... args) {
        this.args = args;
    }

    /**
     * {@inheritDoc}
     * 
     * Changes :
     *  v3 : depuis la v3, les min/max, pourcentage, coefficient... ont été
     *       remplacé par les distributions et leur parametres
     *  v2 : depuis la v2, meilleur gestion des min/max, pourcentage pour quasiment
     *      tous les facteurs continue
     *  v0 : version original
     */
    @Override
    public void start(DesignPlan designPlan) {
        
        int authorIndex = ArrayUtils.indexOf(args, "author");
        int licenseIndex = ArrayUtils.indexOf(args, "license");
        int dateIndex = ArrayUtils.indexOf(args, "date");
        int idIndex = ArrayUtils.indexOf(args, "id");

        xmlBuffer.append("<experimentalDesign");
        // for validation (tests)
        //xmlBuffer.append(" xmlns='http://www.reseau-mexico.fr/sites/reseau-mexico.fr/files/expDesign.xsd'");
        xmlBuffer.append(" version=\"3\"");
        xmlBuffer.append(" author=\"" + (authorIndex != -1 ? args[authorIndex + 1] : "") + "\"");
        xmlBuffer.append(" license=\"" + (licenseIndex != -1 ? args[licenseIndex + 1] : "") + "\"");
        xmlBuffer.append(" date=\"" + (dateIndex != -1 ? args[dateIndex + 1] : new Date()) + "\"");
        xmlBuffer.append(" id=\"" + (idIndex != -1 ? args[idIndex + 1] : "") + "\">");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void visit(DesignPlan designPlan, FactorGroup factorGroup) {
        xmlBuffer.append("<factors>");
        FactorGroupXMLVisitor visitor = new FactorGroupXMLVisitor();
        factorGroup.accept(visitor);
        xmlBuffer.append(visitor.getXML());
        xmlBuffer.append("</factors>");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void end(DesignPlan designPlan) {
        xmlBuffer.append("</experimentalDesign>");
    }

}
