/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.mexico.xml;

import java.util.List;

import fr.ifremer.isisfish.mexico.MexicoHelper;
import fr.ifremer.isisfish.rule.Rule;
import fr.ifremer.isisfish.simulator.sensitivity.Domain;

/**
 * XML rule domain visitor.
 *
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class RuleDiscreteDomainXMLVisitor extends DiscreteDomainXMLVisitor {

    @Override
    public void start(Domain domain) {
        // lorsque le facteur est catégoriel, la liste de ses niveaux, par une
        // liste ouverte d'éléments 'level', d'attributs 'value' et 'weight')
        xmlBuffer.append("<domain distributionName=\"categorical\" type=\"rule\">");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void visit(Domain domain, Object label, Object value) {
        List<Rule> rulesList = (List<Rule>)value;
        String rulesAsXml = MexicoHelper.getRulesAsXml(rulesList);

        xmlBuffer.append("<level>");
        xmlBuffer.append(rulesAsXml);
        xmlBuffer.append("</level>");
    }
}
