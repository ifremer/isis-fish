/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2018 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.mexico.xml;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.text.StringEscapeUtils;
import org.dom4j.Element;
import org.dom4j.Node;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.topia.TopiaContext;

import fr.ifremer.isisfish.mexico.MexicoHelper;
import fr.ifremer.isisfish.rule.Rule;
import fr.ifremer.isisfish.simulator.sensitivity.DesignPlan;
import fr.ifremer.isisfish.simulator.sensitivity.Distribution;
import fr.ifremer.isisfish.simulator.sensitivity.Distribution.DistributionParam;
import fr.ifremer.isisfish.simulator.sensitivity.Factor;
import fr.ifremer.isisfish.simulator.sensitivity.FactorGroup;
import fr.ifremer.isisfish.simulator.sensitivity.domain.ContinuousDomain;
import fr.ifremer.isisfish.simulator.sensitivity.domain.DiscreteDomain;
import fr.ifremer.isisfish.simulator.sensitivity.domain.EquationDiscreteDomain;
import fr.ifremer.isisfish.simulator.sensitivity.domain.RuleDiscreteDomain;

/**
 * Parse xml using dom.
 * 
 * @see Element
 *
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class DomXMLParser {

    private static Log log = LogFactory.getLog(DomXMLParser.class);

    /**
     * Parse element root node as Design plan.
     * 
     * @param rootElement root dom element
     * @param topiaContext database context
     * @return a {@link DesignPlan}
     */
    public static DesignPlan parseDesignPlan(Element rootElement, TopiaContext topiaContext) {

        DesignPlan plan = new DesignPlan();

        // could return "2" or null
        String version = rootElement.attributeValue("version");

        List<Element> factorGroupElements = rootElement.elements("factors");
        FactorGroup factorGroup = null;
        if (!factorGroupElements.isEmpty()) {
            if ("3".equals(version)) {
                factorGroup = parseFactorGroupV3(factorGroupElements.get(0), topiaContext);
            } else if ("2".equals(version)) {
                factorGroup = parseFactorGroupV2(factorGroupElements.get(0), topiaContext);
            }
            else {
                factorGroup = parseFactorGroup(factorGroupElements.get(0), topiaContext);
            }
        }
        plan.setFactorGroup(factorGroup);
        
        return plan;
    }

    /**
     * Recursive parse of factor group elements (&gt;factors&lt;).
     * 
     * @param fgElement factor group element (&gt;factors&lt;)
     * @param topiaContext context
     * @return factor group
     *
     * @deprecated since 4.0.0.0, this parsing method parse experimentalDesign
     *      in version "0" or "null" version, don't remove for
     *      data reading purpose, but could be removed in a future version
     */
    @Deprecated
    protected static FactorGroup parseFactorGroup(Element fgElement, TopiaContext topiaContext) {

        String factorGroupName = fgElement.attributeValue("name");
        String factorGroupType = fgElement.attributeValue("type");
        boolean continuous = "continuous".equalsIgnoreCase(factorGroupType);
        FactorGroup factorGroup = new FactorGroup(factorGroupName, continuous);

        // sub factor group
        List<Node> factorGroupElements = fgElement.selectNodes("child::factors");
        for (Node factorGroupElement : factorGroupElements) {
            FactorGroup subFactorGroup = parseFactorGroup((Element)factorGroupElement, topiaContext);
            factorGroup.addFactor(subFactorGroup);
        }
        
        // normal factors
        List<Node> factorElements = fgElement.selectNodes("child::factor");

        for (Node factorNode : factorElements) {
            Element factorElement = (Element)factorNode;
            String type = factorElement.attributeValue("type");
            String name = factorElement.attributeValue("name");
            String property = factorElement.attributeValue("property");
            String path = factorElement.element("target").getTextTrim();

            // double
            if ("real".equals(type)) {
                Factor factor = new Factor(name);
                factor.setPath(path);
                Element fixedElement = factorElement.element("domain").element("fixed");
                // tous les domaines continues
                if (property.endsWith("continuous")) {

                    ContinuousDomain domain;
                    if (property.equals("matrixcontinuous")) {
                        // matrix specific
                        // <coefficient operator="-" value="0.799"/>
                        domain = new ContinuousDomain(Distribution.QUNIFPC);

                        // <mx name="test1" step="0">...
                        Element matrixElement = fixedElement.element("mx");
                        MatrixND matrix = MexicoHelper.getMatrixFromXml(matrixElement, topiaContext);
                        domain.addDistributionParam("reference", matrix);
                        
                        Element coefficientElement = fixedElement.element("coefficient");
                        domain.addDistributionParam("coefficient", Double.valueOf(coefficientElement.attributeValue("value")));
                    }
                    else if (property.equals("equationcontinuous")) {
                        // equation specific
                        domain = new ContinuousDomain(Distribution.QUNIFPC);
                        
                        Element equationElement = fixedElement.element("equation");
                        domain.addDistributionParam("reference", Double.valueOf(equationElement.attributeValue("reference")));
                        String variable = equationElement.attributeValue("variable");
                        factor.setEquationVariableName(variable);
                        // hack since 4.0.1, factor name doesn't contains variable
                        name = StringUtils.removeEnd(name, "." + variable);
                        factor.setName(name);
                        
                        Element coefficientElement = fixedElement.element("coefficient");
                        domain.addDistributionParam("coefficient", Double.valueOf(coefficientElement.attributeValue("value")));


                    }
                    else {
                        // continous domain

                        Element percentageElement = fixedElement.element("percentage");
                        if (percentageElement != null) {
                            domain = new ContinuousDomain(Distribution.QUNIFPC);
                            domain.addDistributionParam("reference", Double.valueOf(percentageElement.attributeValue("reference")));
                            domain.addDistributionParam("coefficient", Double.valueOf(percentageElement.attributeValue("coefficient")));
                        }
                        else {
                            // <range max="1.0" min="0.0"/>
                            domain = new ContinuousDomain(Distribution.QUNIFMM);
                            Element rangeElement = fixedElement.element("range");
                            domain.addDistributionParam("min", Double.valueOf(rangeElement.attributeValue("min")));
                            domain.addDistributionParam("max", Double.valueOf(rangeElement.attributeValue("max")));
                        }
                    }

                    factor.setCardinality(Integer.parseInt(fixedElement.attributeValue("cardinality")));

                    factor.setDomain(domain);
                } else if ("discrete".equals(property)) {
                    DiscreteDomain domain = new DiscreteDomain();
                    List<Element> valueElements = fixedElement.element("enumeration").elements("value");
                    int label = 0;
                    for (Element valueElement : valueElements) {
                        domain.getValues().put(label, Double.valueOf(valueElement.getTextTrim()));
                        ++label;
                    }
                    factor.setDomain(domain);
                }
                factorGroup.addFactor(factor);
            } else if ("integer".equals(type)) {
                Factor factor = new Factor(name);
                factor.setPath(path);
                Element fixedElement = factorElement.element("domain").element("fixed");
                if ("continuous".equals(property)) {
                    ContinuousDomain domain;

                    if(property.equals("matrixcontinuous")) {
                        // matrix specific
                        domain = new ContinuousDomain(Distribution.QUNIFPC);

                        // matrix specific
                        Element matrixElement = fixedElement.element("mx");
                        MatrixND matrix = MexicoHelper.getMatrixFromXml(matrixElement, topiaContext);
                        domain.addDistributionParam("reference", matrix);
                        
                        Element coefficientElement = fixedElement.element("coefficient");
                        domain.addDistributionParam("coefficient", Double.valueOf(coefficientElement.attributeValue("value")));
                    }
                    else if (property.equals("equationcontinuous")) {
                        // equation specific
                        domain = new ContinuousDomain(Distribution.QUNIFPC);

                        Element equationElement = fixedElement.element("equation");
                        domain.addDistributionParam("reference", Double.valueOf(equationElement.attributeValue("reference")));
                        String variable = equationElement.attributeValue("variable");
                        factor.setEquationVariableName(variable);
                        // hack since 4.0.1, factor name doesn't contains variable
                        name = StringUtils.removeEnd(name, "." + variable);
                        factor.setName(name);

                        Element coefficientElement = fixedElement.element("coefficient");
                        domain.addDistributionParam("coefficient", Double.valueOf(coefficientElement.attributeValue("value")));
                    }
                    else {
                        // continous domain
                        Element percentageElement = fixedElement.element("percentage");
                        if (percentageElement != null) {
                            domain = new ContinuousDomain(Distribution.QUNIFPC);
                            domain.addDistributionParam("reference", Double.valueOf(percentageElement.attributeValue("reference")));
                            domain.addDistributionParam("coefficient", Double.valueOf(percentageElement.attributeValue("coefficient")));
                        }
                        else {
                            // <range max="1" min="3"/>
                            domain = new ContinuousDomain(Distribution.QUNIFMM);
                            Element rangeElement = fixedElement.element("range");
                            domain.addDistributionParam("min", Integer.valueOf(rangeElement.attributeValue("min")));
                            domain.addDistributionParam("max", Integer.valueOf(rangeElement.attributeValue("max")));
                        }
                    }

                    factor.setCardinality(Integer.parseInt(fixedElement.attributeValue("cardinality")));

                    factor.setDomain(domain);
                } else if ("discrete".equals(property)) {
                    DiscreteDomain domain = new DiscreteDomain();
                    List<Element> valueElements = fixedElement.element(
                            "enumeration").elements("value");
                    int label = 0;
                    for (Element valueElement : valueElements) {
                        domain.getValues().put(label,
                                Integer.valueOf(valueElement.getTextTrim()));
                        ++label;
                    }
                    factor.setDomain(domain);
                }
                factorGroup.addFactor(factor);
            } else if ("rule".equals(type)) {
                Factor factor = new Factor(name);
                factor.setPath(path);
                Element fixedElement = factorElement.element("domain").element("fixed");
                if ("discrete".equals(property)) {
                    RuleDiscreteDomain domain = new RuleDiscreteDomain();
                    List<Element> valueElements = fixedElement.element("enumeration").elements("value");
                    int label = 0;
                    for (Element valueElement : valueElements) {
                        Element rulesElement = valueElement.element("rules");
                        List<Rule> rulesValue = MexicoHelper.getRulesFromXml(rulesElement, topiaContext);
                        domain.getValues().put(label, rulesValue);
                        ++label;
                    }
                    factor.setDomain(domain);
                }
                factorGroup.addFactor(factor);
            }
        }

        return factorGroup;
    }
    
    /**
     * Recursive parse of factor group elements (&gt;factors&lt;).
     * 
     * This version handle xml file with min/max and percentage factor
     * in each continuous factors.
     * 
     * @param fgElement factor group element (&gt;factors&lt;)
     * @param topiaContext context
     * @return factor group
     * 
     * @deprecated since 4.0.1.0, this parsing method parse experimentalDesign
     *      in version "2", don't remove for
     *      data reading purpose, but could be removed in a future version
     */
    protected static FactorGroup parseFactorGroupV2(Element fgElement, TopiaContext topiaContext) {

        String factorGroupName = fgElement.attributeValue("name");
        String factorGroupType = fgElement.attributeValue("type");
        boolean continuous = "continuous".equalsIgnoreCase(factorGroupType);
        FactorGroup factorGroup = new FactorGroup(factorGroupName, continuous);

        // sub factor group
        List<Node> factorGroupElements = fgElement.selectNodes("child::factors");
        for (Node factorGroupElement : factorGroupElements) {
            FactorGroup subFactorGroup = parseFactorGroupV2((Element)factorGroupElement, topiaContext);
            factorGroup.addFactor(subFactorGroup);
        }
        
        // normal factors
        List<Node> factorElements = fgElement.selectNodes("child::factor");

        for (Node factorNode : factorElements) {
            Element factorElement = (Element)factorNode;
            String type = factorElement.attributeValue("type");
            String name = factorElement.attributeValue("name");
            String property = factorElement.attributeValue("property");
            String path = factorElement.element("target").getTextTrim();
            String cardinalityString = factorElement.attributeValue("cardinality");
            Integer cardinality = 0;
            if (StringUtils.isNotEmpty(cardinalityString)) {
                cardinality = Integer.valueOf(cardinalityString);
            }

            // double
            if ("real".equals(type)) {
                Factor factor = new Factor(name);
                factor.setPath(path);
                Element fixedElement = factorElement.element("domain").element("fixed");
                // tous les domaines continues
                if (property.endsWith("continuous")) {

                    ContinuousDomain domain;
                    if(property.equals("matrixcontinuous")) {

                        Element referenceElement = fixedElement.element("reference");
                        if (referenceElement != null) {
                            domain = new ContinuousDomain(Distribution.QUNIFPC);
                            // matrix specific
                            // <coefficient operator="-" value="0.799"/>
                            // <mx name="test1" step="0">...
                            Element matrixElement = referenceElement.element("mx");
                            MatrixND matrix = MexicoHelper.getMatrixFromXml(matrixElement, topiaContext);
                            domain.addDistributionParam("reference", matrix);
                            
                            domain.addDistributionParam("coefficient", Double.valueOf(referenceElement.attributeValue("coefficient")));
                        }
                        else {
                            domain = new ContinuousDomain(Distribution.QUNIFMM);
                            Element rangeElement = fixedElement.element("range");
                            Element minElement = rangeElement.element("min");
                            Element maxElement = rangeElement.element("max");
                            MatrixND minMatrix = MexicoHelper.getMatrixFromXml(minElement.element("mx"), topiaContext);
                            MatrixND maxMatrix = MexicoHelper.getMatrixFromXml(maxElement.element("mx"), topiaContext);
                            domain.addDistributionParam("min", minMatrix);
                            domain.addDistributionParam("max", maxMatrix);
                        }
                    }
                    else if (property.equals("equationcontinuous")) {
                        // equation specific
                        
                        String variable = fixedElement.attributeValue("variable");
                        factor.setEquationVariableName(variable);
                        // hack since 4.0.1, factor name doesn't contains variable
                        name = StringUtils.removeEnd(name, "." + variable);
                        factor.setName(name);

                        Element referenceElement = fixedElement.element("reference");
                        if (referenceElement != null) {
                            domain = new ContinuousDomain(Distribution.QUNIFPC);
                            domain.addDistributionParam("reference", Double.valueOf(referenceElement.getTextTrim()));
                            domain.addDistributionParam("coefficient", Double.valueOf(referenceElement.attributeValue("coefficient")));
                        }
                        else {
                            domain = new ContinuousDomain(Distribution.QUNIFMM);
                            Element rangeElement = fixedElement.element("range");
                            Element minElement = rangeElement.element("min");
                            Element maxElement = rangeElement.element("max");
                            domain.addDistributionParam("min", minElement.getTextTrim());
                            domain.addDistributionParam("max", maxElement.getTextTrim());
                        }
                    }
                    else {
                        // continous domain
                        Element referenceElement = fixedElement.element("reference");
                        if (referenceElement != null) {
                            domain = new ContinuousDomain(Distribution.QUNIFPC);
                            domain.addDistributionParam("reference", Double.valueOf(referenceElement.getTextTrim()));
                            domain.addDistributionParam("coefficient", Double.valueOf(referenceElement.attributeValue("coefficient")));
                        }
                        else {
                            // <range max="1.0" min="0.0"/>
                            domain = new ContinuousDomain(Distribution.QUNIFMM);
                            Element rangeElement = fixedElement.element("range");
                            Element minElement = rangeElement.element("min");
                            Element maxElement = rangeElement.element("max");
                            domain.addDistributionParam("min", Double.valueOf(minElement.getTextTrim()));
                            domain.addDistributionParam("max", Double.valueOf(maxElement.getTextTrim()));
                        }
                    }

                    factor.setDomain(domain);
                } else if ("discrete".equals(property)) {
                    DiscreteDomain domain = new DiscreteDomain();
                    List<Element> valueElements = fixedElement.element(
                            "enumeration").elements("value");
                    int label = 0;
                    for (Element valueElement : valueElements) {
                        domain.getValues().put(label,
                                Double.valueOf(valueElement.getTextTrim()));
                        ++label;
                    }
                    factor.setDomain(domain);
                }
                factor.setCardinality(cardinality);
                factorGroup.addFactor(factor);
            } else if ("integer".equals(type)) {
                Factor factor = new Factor(name);
                factor.setPath(path);
                Element fixedElement = factorElement.element("domain").element("fixed");
                if ("continuous".equals(property)) {
                    ContinuousDomain domain;

                    if(property.equals("matrixcontinuous")) {
                        
                        
                        Element referenceElement = fixedElement.element("reference");
                        if (referenceElement != null) {
                            domain = new ContinuousDomain(Distribution.QUNIFPC);
                            // matrix specific
                            // <coefficient operator="-" value="0.799"/>
                            // <mx name="test1" step="0">...
                            Element matrixElement = referenceElement.element("mx");
                            MatrixND matrix = MexicoHelper.getMatrixFromXml(matrixElement, topiaContext);
                            domain.addDistributionParam("reference", matrix);
                            domain.addDistributionParam("coefficient", Double.valueOf(referenceElement.attributeValue("coefficient")));
                        }
                        else {
                            domain = new ContinuousDomain(Distribution.QUNIFMM);
                            Element rangeElement = fixedElement.element("range");
                            Element minElement = rangeElement.element("min");
                            Element maxElement = rangeElement.element("max");
                            MatrixND minMatrix = MexicoHelper.getMatrixFromXml(minElement.element("mx"), topiaContext);
                            MatrixND maxMatrix = MexicoHelper.getMatrixFromXml(maxElement.element("mx"), topiaContext);
                            domain.addDistributionParam("min", minMatrix);
                            domain.addDistributionParam("max", maxMatrix);
                        }
                    }
                    else if (property.equals("equationcontinuous")) {
                        // equation specific
                        String variable = fixedElement.attributeValue("variable");
                        factor.setEquationVariableName(variable);
                        // hack since 4.0.1, factor name doesn't contains variable
                        name = StringUtils.removeEnd(name, "." + variable);
                        factor.setName(name);

                        Element referenceElement = fixedElement.element("reference");
                        if (referenceElement != null) {
                            domain = new ContinuousDomain(Distribution.QUNIFPC);
                            domain.addDistributionParam("reference", Double.valueOf(referenceElement.getTextTrim()));
                            domain.addDistributionParam("coefficient", Double.valueOf(referenceElement.attributeValue("coefficient")));
                        }
                        else {
                            domain = new ContinuousDomain(Distribution.QUNIFMM);
                            Element rangeElement = fixedElement.element("range");
                            Element minElement = rangeElement.element("min");
                            Element maxElement = rangeElement.element("max");
                            domain.addDistributionParam("min", Double.valueOf(minElement.getTextTrim()));
                            domain.addDistributionParam("max", Double.valueOf(maxElement.getTextTrim()));
                        }
                    }
                    else {
                        // continous domain
                        Element referenceElement = fixedElement.element("reference");
                        if (referenceElement != null) {
                            domain = new ContinuousDomain(Distribution.QUNIFPC);
                            domain.addDistributionParam("reference", Double.valueOf(referenceElement.getTextTrim()));
                            domain.addDistributionParam("coefficient", Double.valueOf(referenceElement.attributeValue("coefficient")));
                        }
                        else {
                            // <range max="1.0" min="0.0"/>
                            domain = new ContinuousDomain(Distribution.QUNIFMM);
                            Element rangeElement = fixedElement.element("range");
                            Element minElement = rangeElement.element("min");
                            Element maxElement = rangeElement.element("max");
                            domain.addDistributionParam("min", Integer.valueOf(minElement.getTextTrim()));
                            domain.addDistributionParam("max", Integer.valueOf(maxElement.getTextTrim()));
                        }
                    }

                    factor.setDomain(domain);
                } else if ("discrete".equals(property)) {
                    DiscreteDomain domain = new DiscreteDomain();
                    List<Element> valueElements = fixedElement.element(
                            "enumeration").elements("value");
                    int label = 0;
                    for (Element valueElement : valueElements) {
                        domain.getValues().put(label,
                                Integer.valueOf(valueElement.getTextTrim()));
                        ++label;
                    }
                    factor.setDomain(domain);
                }
                factor.setCardinality(cardinality);
                factorGroup.addFactor(factor);
            } else if ("rule".equals(type)) {
                Factor factor = new Factor(name);
                factor.setPath(path);
                Element fixedElement = factorElement.element("domain").element("fixed");
                if ("discrete".equals(property)) {
                    RuleDiscreteDomain domain = new RuleDiscreteDomain();
                    List<Element> valueElements = fixedElement.element("enumeration").elements("value");
                    int label = 0;
                    for (Element valueElement : valueElements) {
                        Element rulesElement = valueElement.element("rules");
                        List<Rule> rulesValue = MexicoHelper.getRulesFromXml(rulesElement, topiaContext);
                        domain.getValues().put(label, rulesValue);
                        ++label;
                    }
                    factor.setDomain(domain);
                }
                factor.setCardinality(cardinality);
                factorGroup.addFactor(factor);
            } else if ("equation".equals(type)) {
                Factor factor = new Factor(name);
                factor.setPath(path);
                Element fixedElement = factorElement.element("domain").element("fixed");
                if ("discrete".equals(property)) {
                    EquationDiscreteDomain domain = new EquationDiscreteDomain();
                    List<Element> valueElements = fixedElement.element("enumeration").elements("value");
                    int label = 0;
                    for (Element valueElement : valueElements) {
                        String content = StringEscapeUtils.unescapeXml(valueElement.getText());
                        domain.getValues().put(label, content);
                        ++label;
                    }
                    factor.setDomain(domain);
                }
                factor.setCardinality(cardinality);
                factorGroup.addFactor(factor);
            } else if ("string".equals(type)) {
                Factor factor = new Factor(name);
                factor.setPath(path);
                Element fixedElement = factorElement.element("domain").element("fixed");
                if ("discrete".equals(property)) {
                    DiscreteDomain domain = new DiscreteDomain();
                    List<Element> valueElements = fixedElement.element("enumeration").elements("value");
                    int label = 0;
                    for (Element valueElement : valueElements) {
                        String content = valueElement.getText();
                        Object object = MexicoHelper.getObjectFromString(content, topiaContext);
                        domain.getValues().put(label, object);
                        ++label;
                    }
                    factor.setDomain(domain);
                }
                factor.setCardinality(cardinality);
                factorGroup.addFactor(factor);
            }
        }

        return factorGroup;
    }
    
    /**
     * Non recursive parse of factor group elements (&gt;factors&lt;).
     * 
     * This version handle xml file distribution definition in each continuous
     * factors.
     * 
     * The parsing is more complicated here because mexico file format is a bit
     * hard too read (factor groups, features...)
     * 
     * @param fgElement factor group element (&gt;factors&lt;)
     * @param topiaContext context
     * @return factor group
     * @since 4.0.1.0
     */
    protected static FactorGroup parseFactorGroupV3(Element fgElement, TopiaContext topiaContext) {

        FactorGroup rootGroup = new FactorGroup(null); // root group
        Map<String, FactorGroup> groups = new HashMap<>();

        // normal factors
        List<Element> factorElements = fgElement.elements("factor");
        for (Element factorElement : factorElements) {
            String name = factorElement.attributeValue("name");
            String groupName = null;
            String groupType = null;

            Factor factor = new Factor(name);

            Element cardinalityElement = (Element)factorElement.selectSingleNode("child::feature[@name='cardinality']");
            if (cardinalityElement != null) {
                factor.setCardinality(Integer.parseInt(cardinalityElement.getTextTrim()));
            }
            Element targetElement = (Element)factorElement.selectSingleNode("child::feature[@name='target']");
            if (targetElement != null) {
                factor.setPath(targetElement.getTextTrim());
            }
            Element groupElement = (Element)factorElement.selectSingleNode("child::feature[@name='group']");
            if (groupElement != null) {
                groupName = groupElement.getTextTrim();
            }
            Element groupTypeElement = (Element)factorElement.selectSingleNode("child::feature[@name='grouptype']");
            if (groupTypeElement != null) {
                groupType = groupTypeElement.getTextTrim();
            }
            Element varNameElement = (Element)factorElement.selectSingleNode("child::feature[@name='equationVariableName']");
            if (varNameElement != null) {
                factor.setEquationVariableName(varNameElement.getTextTrim());
            }
            

            // parse domain
            Element domainElement = factorElement.element("domain");
            String distributionName = domainElement.attributeValue("distributionName");
            String type = domainElement.attributeValue("type");

            // facteur discret
            if ("categorical".equals(distributionName)) {
                DiscreteDomain domain;

                List<Element> levelElements = domainElement.elements("level");
                if ("rule".equals(type)) {
                    domain = new RuleDiscreteDomain();
                    int valueName = 0;
                    for (Element levelElement : levelElements) {
                        Element rulesElement = levelElement.element("rules");
                        List<Rule> rulesValue = MexicoHelper.getRulesFromXml(rulesElement, topiaContext);
                        domain.getValues().put(valueName++, rulesValue);
                    }
                } else if ("equation".equals(type)) { // topia entities ...
                    domain = new EquationDiscreteDomain();
                    int valueName = 0;
                    for (Element levelElement : levelElements) { // no trim here
                        Object object = StringEscapeUtils.unescapeXml(levelElement.getText());
                        domain.getValues().put(valueName++, object);
                    }
                } else if ("string".equals(type)) { // topia entities ...
                    domain = new DiscreteDomain();
                    int valueName = 0;
                    for (Element levelElement : levelElements) {
                        Object object = MexicoHelper.getObjectFromString(levelElement.getTextTrim(), topiaContext);
                        domain.getValues().put(valueName++, object);
                    }
                } else if ("integer".equals(type)) {
                    domain = new DiscreteDomain();
                    int valueName = 0;
                    for (Element levelElement : levelElements) {
                        Integer levelValue = Integer.valueOf(levelElement.getTextTrim());
                        domain.getValues().put(valueName++, levelValue);
                    }
                } else if ("matrix".equals(type)) {
                    domain = new DiscreteDomain();
                    int valueName = 0;
                    for (Element levelElement : levelElements) {
                        Element matrixElement = levelElement.element("mx");
                        MatrixND matrix = MexicoHelper.getMatrixFromXml(matrixElement, topiaContext);
                        domain.getValues().put(valueName++, matrix);
                    }
                } else {
                    domain = new DiscreteDomain();
                    int valueName = 0;
                    for (Element levelElement : levelElements) {
                        Double levelValue = Double.valueOf(levelElement.getTextTrim());
                        domain.getValues().put(valueName++, levelValue);
                    }
                }

                factor.setDomain(domain);

            } else if ("sequence".equals(distributionName)) {
                // can happen in mexico, but not in isis
                // skip this one
                if (log.isInfoEnabled()) {
                    log.info(String.format("Skip factor %s : unknown distribution name : %s !", name, distributionName));
                }
            } else {

                // facteur continuous with custom distribution
                Distribution distribution = Distribution.valueOf(distributionName.toUpperCase());
                if (distribution == null) {
                    if (log.isWarnEnabled()) {
                        log.warn(String.format("Skip factor %s : can't found distribution name : %s !", name, distributionName));
                    }
                    continue;
                }
                
                ContinuousDomain domain = new ContinuousDomain(distribution);

                // ne tient pas compte de l'ordre des parametres
                // dans le fichier xml
                DistributionParam[] paramsDef = distribution.getDistibutionParams();
                for (DistributionParam paramDef : paramsDef) {
                    Element paramElement = (Element)domainElement.selectSingleNode("distributionParameter[@name='" + paramDef.getName() + "']");
                    String paramName = paramElement.attributeValue("name");
                    String paramType = paramElement.attributeValue("type");

                    if ("matrix".equals(paramType)) {
                        Element mxElement = paramElement.element("mx");
                        MatrixND mxMatrix = MexicoHelper.getMatrixFromXml(mxElement, topiaContext);
                        domain.addDistributionParam(paramName, mxMatrix);
                    } else if ("decimal".equals(paramType)) {
                        Double doubleValue = Double.parseDouble(paramElement.getTextTrim());
                        domain.addDistributionParam(paramName, doubleValue);
                    }
                }

                factor.setDomain(domain);
            }

            // add factor to existing or new group
            FactorGroup group = rootGroup;
            if (StringUtils.isNotBlank(groupName)) {
                group = groups.get(groupName);
                if (group == null) {
                    group = new FactorGroup(groupName, "continuous".equals(groupType));
                    groups.put(groupName, group);
                    rootGroup.addFactor(group);
                }
            }
            group.addFactor(factor);
        }

        return rootGroup;
    }
}
