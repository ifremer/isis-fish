/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 1999 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.aspect;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

import fr.ifremer.isisfish.datastore.SimulationInformation;
import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.entities.Metier;
import fr.ifremer.isisfish.rule.Rule;
import fr.ifremer.isisfish.simulator.SimulationContext;
import fr.ifremer.isisfish.types.TimeStep;

/**
 * Aspect utilisé pour intersepecter les appels a
 * {@link Rule#init(SimulationContext)}, {@link Rule#preAction(SimulationContext, TimeStep, Metier)}, 
 * {@link Rule#postAction(SimulationContext, TimeStep, Metier)} et mémoriser
 * le temps mit.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
@Aspect
public class RuleAspect {

    /** Log. */
    private static Log log = LogFactory.getLog(RuleAspect.class);

    /**
     * Aspect around {@link Rule#init(SimulationContext)} in packages "rules".
     * 
     * @param jp join point
     * @return init result
     * @throws Throwable
     */
    @Around("execution(* rules.*.init(..))")
    public Object initCall(final ProceedingJoinPoint jp) throws Throwable {
        
        Object result = makeTimedCall(jp, 0);
        return result;
        
    }

    /**
     * Aspect around {@link Rule#preAction(SimulationContext, TimeStep, Metier)} in packages "rules".
     * 
     * @param jp join point
     * @return preAction result
     * @throws Throwable
     */
    @Around("execution(* rules.*.preAction(..))")
    public Object initPreCall(final ProceedingJoinPoint jp) throws Throwable {
        
        Object result = makeTimedCall(jp, 1);
        return result;
    }
    
    /**
     * Aspect around {@link Rule#postAction(SimulationContext, TimeStep, Metier)} in packages "rules".
     * 
     * @param jp join point
     * @return preAction result
     * @throws Throwable
     */
    @Around("execution(* rules.*.postAction(..))")
    public Object initPostCall(final ProceedingJoinPoint jp) throws Throwable {
        
        Object result = makeTimedCall(jp, 2);
        return result;
    }

    /**
     * Effectue l'appel reel en calculant le temps pris.
     * 
     * @param jp join point
     * @param state state
     * @throws Throwable
     * @return target joint point method result
     */
    protected Object makeTimedCall(ProceedingJoinPoint jp, int state) throws Throwable {
        
        if (log.isTraceEnabled()) {
            log.trace("Rule aspect called : " + jp.getTarget().getClass().getSimpleName());
        }
        
        // get time before
        long timeBeforeCall = System.currentTimeMillis();
        
        // make real call
        Object result = jp.proceed();
        
        // get time after
        long timeAfterCall = System.currentTimeMillis();
        
        // get real time
        long timeTaken = timeAfterCall - timeBeforeCall;

        registerTime(jp, state, timeTaken);

        return result;
        
    }

    /**
     * Add time (init/pre/post) to simulation information in
     * current {@link SimulationContext}.
     * 
     * @param jp join point
     * @param state state 
     * @param timeTaken time taken by {@code state}
     */
    protected void registerTime(JoinPoint jp, int state, long timeTaken) {
        SimulationContext context = SimulationContext.get();
        
        SimulationStorage simulation = context.getSimulationStorage();
        
        // happen if called outside a simulation
        // so ... can't happen :)
        if (simulation != null) {
            
            // get rule information
            Class<?> calleeClass = jp.getTarget().getClass();
            String ruleName = calleeClass.getSimpleName();
            
            SimulationInformation info = simulation.getInformation();

            // since 20140521: remove use of enum because aspectj don't like that
            switch (state) {
            case 0:
                info.addRuleInitTime(ruleName, timeTaken);
                break;
            case 1:
                info.addRulePreTime(ruleName, timeTaken);
                break;
            case 2:
                info.addRulePostTime(ruleName, timeTaken);
                break;
            default:
                break;
            }
        } else {
            if (log.isWarnEnabled()) {
                log.warn("Time register called outside a simulation context");
            }
        }
    }
}
