/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.aspect;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.aspectj.apache.bcel.classfile.ClassParser;
import org.aspectj.apache.bcel.classfile.JavaClass;
import org.aspectj.bridge.IMessageHandler;
import org.aspectj.util.FileUtil;
import org.aspectj.util.LangUtil;
import org.aspectj.weaver.CrosscuttingMembersSet;
import org.aspectj.weaver.IClassFileProvider;
import org.aspectj.weaver.IUnwovenClassFile;
import org.aspectj.weaver.IWeaveRequestor;
import org.aspectj.weaver.ResolvedType;
import org.aspectj.weaver.bcel.BcelObjectType;
import org.aspectj.weaver.bcel.BcelWeaver;
import org.aspectj.weaver.bcel.BcelWorld;
import org.aspectj.weaver.bcel.UnwovenClassFile;
import org.aspectj.weaver.tools.WeavingAdaptor;
import org.aspectj.weaver.tools.WeavingClassLoader;
import org.aspectj.weaver.tools.cache.SimpleCache;
import org.aspectj.weaver.tools.cache.SimpleCacheFactory;

/**
 * Cette classe étend {@link WeavingAdaptor} pour avoir une instance personnalisée du champ {@link #weaver}.
 * 
 * Toutes la logique consiste ensuite à pouvoir appeler le code {@code weaver.weave(wcp);} pour pouvoir déployer un
 * aspect à l'execution dans le {@code weaver} qui servira ensuite à "aspectiser une classe" via la méthode
 * {@link #weaveClass(String, byte[])}.
 * 
 * Elle utilise pour l'instant de l'introspection agressive pour acceder aux attributs privés de la classe parentes et
 * les rendre visible.
 * 
 * TODO : au lieu d'ajouter à la demande des aspects, on pourrait les ajouters en parmanence, mais en les activant de
 * facon conditionnelle (if() cutpoint).
 * A tester. Il est possible que sans le cas d'isis, une classe non aspectisée soit plus performante qu'une classe
 * dont les aspects ne sont pas activés.
 * Voir la discussion : http://aspectj.2085585.n4.nabble.com/Runtime-weaving-without-agent-tp4650837p4650849.html
 * 
 * @since 4.2.0.0
 */
public class AspectJWeavingAdaptor extends WeavingAdaptor {

    protected ClassLoader parentLoader;

    protected CrosscuttingMembersSet xcutSet;

    public AspectJWeavingAdaptor(ClassLoader parentLoader, WeavingClassLoader loader) {
        this.parentLoader = parentLoader;

        generatedClassHandler = loader;
        init((ClassLoader)loader, getFullClassPath((ClassLoader) loader));
    }

    /**
     * Initialize the WeavingAdapter
     * @param loader ClassLoader used by this adapter; which can be null
     * @param classPath classpath of this adapter
     */
    private void init(ClassLoader loader, List<String> classPath) {
        try {
            // introspection of : abortOnError = true;
            Field abortOnErrorField = WeavingAdaptor.class.getDeclaredField("abortOnError");
            abortOnErrorField.setAccessible(true);
            abortOnErrorField.set(this, true);

            createMessageHandler();

            //info("using classpath: " + classPath);
            //info("using aspectpath: " + aspectPath);

            // introspection of : messageHandler
            Field messageHandlerField = WeavingAdaptor.class.getDeclaredField("messageHandler");
            messageHandlerField.setAccessible(true);
            IMessageHandler messageHandler = (IMessageHandler)messageHandlerField.get(this);
            bcelWorld = new BcelWorld(classPath, messageHandler, null);
            bcelWorld.setXnoInline(false);
            bcelWorld.getLint().loadDefaultProperties();

            if (LangUtil.is15VMOrGreater()) {
                bcelWorld.setBehaveInJava5Way(true);
            }

            // CL custom logger
            bcelWorld.setMessageHandler(new CommonsLoggingMessageHandler());

            weaver = new BcelWeaver(bcelWorld);
            //registerAspectLibraries(aspectPath);
            //initializeCache(loader, aspectPath, null, getMessageHandler());

            // introspection of : enabled = true;
            Field enabledField = WeavingAdaptor.class.getDeclaredField("enabled");
            enabledField.setAccessible(true);
            enabledField.set(this, true);

            // introspection of : xcutSet = weaver.xcutSet
            Field xcutSetField = BcelWeaver.class.getDeclaredField("xcutSet");
            xcutSetField.setAccessible(true);
            xcutSet = (CrosscuttingMembersSet) xcutSetField.get(weaver);

        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public byte[] deploy(Class<?> aspectClass) {
        byte[] result;
        String name = aspectClass.getName();
        try (InputStream is = AspectJWeavingAdaptor.class.getResourceAsStream("/" + name.replace('.', '/') + ".class")) {

            byte[] bytes = FileUtil.readAsByteArray(is);
            is.close();
            ClassParser parser = new ClassParser(new ByteArrayInputStream(bytes), name);

            JavaClass jc = parser.parse();
            BcelObjectType bcType = bcelWorld.addSourceObjectType(jc, false);
            ResolvedType type = bcType.getResolvedTypeX();
            xcutSet.addOrReplaceAspect(type);

            WeavingClassFileProvider2 wcp = new WeavingClassFileProvider2(name, bytes);
            wcp.setApplyAtAspectJMungersOnly();
            weaver.weave(wcp);
            result = wcp.getBytes();

        } catch (IOException e) {
            throw new RuntimeException("Can't create aspect", e);
        }

        return result;
    }

    /**
     * Mark bcel weaver ready for weaving classes.
     */
    public void prepare() {
        weaver.prepareForWeave();
    }

    /**
     * Cliper/coller de la classe originale {@code WeavingClassFileProvider} car elle n'est pas visible (private).
     * 
     * L'appel à {@code delegateForCurrentClass.getResolvedTypeX().getName()} a été désactivé car c'est quelque chose
     * qui est valorisé par l'instrospecteur d'aspect qui n'est pas défini pour IsisFish (aspect runtime).
     */
    private class WeavingClassFileProvider2 implements IClassFileProvider {

        private final UnwovenClassFile unwovenClass;
        private final List<UnwovenClassFile> unwovenClasses = new ArrayList<>();
        private IUnwovenClassFile wovenClass;
        private boolean isApplyAtAspectJMungersOnly = false;

        public WeavingClassFileProvider2(String name, byte[] bytes) {
            ensureDelegateInitialized(name, bytes);
            this.unwovenClass = new UnwovenClassFile(name, name /*delegateForCurrentClass.getResolvedTypeX().getName()*/, bytes);
            this.unwovenClasses.add(unwovenClass);

            if (shouldDump(name.replace('/', '.'), true)) {
                dump(name, bytes, true);
            }

        }

        public void setApplyAtAspectJMungersOnly() {
            isApplyAtAspectJMungersOnly = true;
        }

        public boolean isApplyAtAspectJMungersOnly() {
            return isApplyAtAspectJMungersOnly;
        }

        public byte[] getBytes() {
            if (wovenClass != null) {
                return wovenClass.getBytes();
            } else {
                return unwovenClass.getBytes();
            }
        }

        public Iterator<UnwovenClassFile> getClassFileIterator() {
            return unwovenClasses.iterator();
        }

        public IWeaveRequestor getRequestor() {
            return new IWeaveRequestor() {

                public void acceptResult(IUnwovenClassFile result) {
                    if (wovenClass == null) {
                        wovenClass = result;
                        String name = result.getClassName();
                        if (shouldDump(name.replace('/', '.'), false)) {
                            dump(name, result.getBytes(), false);
                        }
                    } else {
                        // Classes generated by weaver e.g. around closure advice
                        String className = result.getClassName();
                        byte[]  resultBytes = result.getBytes();

                        if (SimpleCacheFactory.isEnabled()) {
                            SimpleCache lacache=SimpleCacheFactory.createSimpleCache();
                            lacache.put(result.getClassName(), wovenClass.getBytes(), result.getBytes());
                            lacache.addGeneratedClassesNames(wovenClass.getClassName(), wovenClass.getBytes(), result.getClassName());
                        }

                        generatedClasses.put(className, result);
                        generatedClasses.put(wovenClass.getClassName(), result);
                        generatedClassHandler.acceptClass(className, null, resultBytes);
                    }
                }

                public void processingReweavableState() {
                }

                public void addingTypeMungers() {
                }

                public void weavingAspects() {
                }

                public void weavingClasses() {
                }

                public void weaveCompleted() {
                    // ResolvedType.resetPrimitives();
                    if (delegateForCurrentClass != null) {
                        delegateForCurrentClass.weavingCompleted();
                    }
                    // ResolvedType.resetPrimitives();
                    // bcelWorld.discardType(typeBeingProcessed.getResolvedTypeX()); // work in progress
                }
            };
        }
    }
}
