/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2014 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.aspect;

import java.lang.reflect.Method;
import java.util.Arrays;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;

import fr.ifremer.isisfish.simulator.SimulationContext;
import fr.ifremer.isisfish.util.cache.IsisCache;

/**
 * CacheAspect aspect.
 * 
 * Created: 25 août 06 22:42:47
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
@Aspect
public class CacheAspect {
    
    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(CacheAspect.class);

    /**
     * Return trace object from context.
     *
     * @return trace object from context
     */
    protected IsisCache getCache() {
        SimulationContext context = SimulationContext.get();
        IsisCache result = context.getCache();
        return result;
    }

    @Around("execution(* scripts..*(..))")
    public Object call(final ProceedingJoinPoint jp) throws Throwable {
        Method method = ((MethodSignature)jp.getSignature()).getMethod();
        Object[] args = jp.getArgs();
        Object result = getCache().get(method, args, jp);
        
        if (log.isTraceEnabled()) {
            log.trace(((MethodSignature)jp.getSignature()).getMethod()
                    + " args " + Arrays.toString(jp.getArgs())
                    + " result = " + result);
        }
        return result;
    }
}
