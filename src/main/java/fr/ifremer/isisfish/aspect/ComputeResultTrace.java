/*
 * #%L
 * IsisFish
 *
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2020 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.aspect;

import fr.ifremer.isisfish.annotations.ComputeResult;
import org.nuiton.profiling.StatisticMethod;
import org.nuiton.profiling.Trace;

import java.lang.reflect.Method;
import java.util.Map;

/**
 * Extends default trace to render output using result name instead of method name.
 */
public class ComputeResultTrace extends Trace {

    public ComputeResultTrace(boolean multithread, boolean distinctThreadCall) {
        super(multithread, distinctThreadCall);
    }

    @Override
    public String getStatisticsText() {
        StringBuilder result = new StringBuilder();

        for (StatAndStack ss : allStatAndStack) {
            for (Map.Entry<Method, StatisticMethod> methodAndStat : ss.getStatistics().entrySet()) {
                Method method = methodAndStat.getKey();
                String resultName = method.getAnnotation(ComputeResult.class).value();
                StatisticMethod stats = methodAndStat.getValue();
                result.append("- ").append(resultName)
                        .append(", max:").append(stats.getMax() / 1000).append("ms")
                        .append(", mean:").append((long)stats.getMean() / 1000).append("ms")
                        .append(", total:").append((long)stats.getTotal() / 1000).append("ms")
                        .append("\n");
            }
        }

        return result.toString();
    }
}
