/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2014 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.aspect;

import java.lang.reflect.Method;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;

import fr.ifremer.isisfish.simulator.SimulationContext;
import org.nuiton.profiling.Trace;

/**
 * Permet de tracer les appels aux methodes utilisateur ainsi que l'execution
 * a ces methodes. La difference entre les deux est lors de l'utilisation du
 * cache les appels seront superieur a l'execution car certaine valeur seront
 * reutilisé dans le cache
 * 
 * Created: 25 août 06 22:19:21
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
@Aspect
public class TraceAspect {

    /**
     * Return trace object from context.
     *
     * @return trace object from context
     */
    protected Trace getTrace() {
        SimulationContext context = SimulationContext.get();
        Trace result = context.getTrace();
        return result;
    }

    @Before("execution(* scripts..*(..))"
            + " || execution(* simulators..*(..))"
            + " || execution(* rules..*(..)) "
            + " || execution(* simulationplans..*(..)) "
    )
    public void traceBeforeExecute(JoinPoint jp) {
        Method method = ((MethodSignature) jp.getSignature()).getMethod();
        getTrace().enter(method);
    }

    @AfterThrowing(throwing = "ex",
            pointcut = "execution(* scripts..*(..))"
            + " || execution(* simulators..*(..))"
            + " || execution(* rules..*(..)) "
            + " || execution(* simulationplans..*(..)) "
    )
    public void traceAfterThrowingExecute(JoinPoint jp, Exception ex) {
        // si une exeption est leve, il faut faire la meme chose
        traceAfterExecute(jp);
    }

    @After("execution(* scripts..*(..))"
            + " || execution(* simulators..*(..))"
            + " || execution(* rules..*(..)) "
            + " || execution(* simulationplans..*(..)) "

    )
    public void traceAfterExecute(JoinPoint jp) {
        Method method = ((MethodSignature) jp.getSignature()).getMethod();
        getTrace().exit(method);
    }
}
