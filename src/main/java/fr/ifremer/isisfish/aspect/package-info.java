/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 1999 - 2014 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
/**
 * Aspect definition classes.
 * 
 * Contains two aspects :
 * <ul>
 *  <li>{@link fr.ifremer.isisfish.aspect.CacheAspect} : cache aspect</li>
 *  <li>{@link fr.ifremer.isisfish.aspect.TraceAspect} : trace expect</li>
 *  <li>{@link fr.ifremer.isisfish.aspect.RuleAspect} : rule timing aspect</li>
 * </ul>
 */
package fr.ifremer.isisfish.aspect;
