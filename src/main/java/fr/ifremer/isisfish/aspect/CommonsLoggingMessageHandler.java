/*
 * #%L
 * IsisFish
 *
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2018 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.aspect;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.bridge.AbortException;
import org.aspectj.bridge.IMessage;
import org.aspectj.bridge.IMessageHandler;

/**
 * Handler utilisé pour surcharger le logging par defaut dans STD_ERR.
 */
public class CommonsLoggingMessageHandler implements IMessageHandler {

    protected Log log = LogFactory.getLog(CommonsLoggingMessageHandler.class);

    @Override
    public boolean handleMessage(IMessage message) throws AbortException {
        if (message.isError()) {
            log.error(message);
        } else if (message.isWarning()) {
            log.warn(message);
        } else if (message.isInfo()) {
            log.info(message);
        } else {
            log.debug(message);
        }
        return true;
    }

    @Override
    public boolean isIgnoring(IMessage.Kind kind) {
        return false;
    }

    @Override
    public void dontIgnore(IMessage.Kind kind) {

    }

    @Override
    public void ignore(IMessage.Kind kind) {

    }
}
