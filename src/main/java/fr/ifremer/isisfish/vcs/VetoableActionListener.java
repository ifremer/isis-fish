/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2010 Ifremer, Code Lutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.vcs;

import java.io.File;

/**
 * Permet a une action d'etre interdite avant qu'elle ne soit faite. Pour cela
 * Il faut implanter cette classe pour surveiller les actions, et l'enregistrer
 * en tant que listener d'action sur le vcs.
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public interface VetoableActionListener {
    
    /**
     * Permet d'arreter l'action qui souhaite etre faite, si on retourne false
     * @param vcs le vcs qui va faire l'action
     * @param action l'action que l'on souhaite faire
     * @param files les fichiers impactes par l'action
     * @return vrai si l'action peut-etre faite, false pour interdire l'action
     */
    boolean canDoAction(VCS vcs, VCSActionEvent action, File... files);

}
