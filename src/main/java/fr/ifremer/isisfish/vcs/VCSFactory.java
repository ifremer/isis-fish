/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2015 Ifremer, Code Lutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.vcs;

import fr.ifremer.isisfish.config.IsisConfig;
import java.io.File;
import static org.nuiton.i18n.I18n.t;

import java.util.Properties;

import fr.ifremer.isisfish.config.Option;
import org.apache.commons.beanutils.ConstructorUtils;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Permet de recupere le bon VCS en fonction de la configuration.
 *
 * @author poussin
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class VCSFactory {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    private static Log log = LogFactory.getLog(VCSFactory.class);

    protected static final Properties TYPE_ALIASES = new Properties();

    static {
        TYPE_ALIASES.setProperty("", VCSNone.class.getName());
        TYPE_ALIASES.setProperty(VCS.TYPE_NONE, VCSNone.class.getName());
        TYPE_ALIASES.setProperty(VCS.TYPE_SVN, VCSSVN.class.getName());
    }

    private VCSFactory() {
        
    }

    /**
     * VCS factory, looking for vcs.type key in config to find vcs class to used.
     * If no vcs.type found or is not valid class, use {@link VCSNone}.
     * 
     * @param config config
     * @return usable VCS
     */
    static public VCS createVCS(IsisConfig config) {
        String type = config.getOption(VCS.VCS_TYPE);
        // try to convert if type is not class name but an alias
        String classname = TYPE_ALIASES.getProperty(type, type);

        File dataDir = config.getDatabaseDirectory();
        String protocol = config.getOption(VCS.VCS_PROTOCOL);
        String host = config.getOption(VCS.VCS_HOST_NAME);
        String path = config.getOption(VCS.VCS_PATH);
        File sshKeyFile = config.getSSHPrivateKeyFilePath();
        String login = config.getOption(VCS.VCS_USER_NAME);
        String password = config.getOption(VCS.VCS_USER_PASSWORD);

        VCS result = makeVCSInstance(classname, dataDir, protocol, host, path, sshKeyFile, login, password);
        return result;
    }

    /**
     * Create VCS to manage community VCS repository.
     * 
     * @param config config
     * @return usable VCS
     */
    static public VCS createPublicVCS(IsisConfig config) {
        String type = config.getOption(Option.VCS_COMMUNITY_TYPE.key);
        // try to convert if type is not class name but an alias
        String classname = TYPE_ALIASES.getProperty(type, type);

        File dataDir = config.getCommunityDatabaseDirectory();
        String protocol = config.getOption(Option.VCS_COMMUNITY_PROTOCOL.key);
        String host = config.getOption(Option.VCS_COMMUNITY_HOST_NAME.key);
        String path = config.getOption(Option.VCS_COMMUNITY_PATH.key);
        File sshKeyFile = config.getSSHPrivateKeyFilePath();
        String login = config.getOption(Option.VCS_COMMUNITY_USER_NAME.key);
        String password = config.getOption(Option.VCS_COMMUNITY_PASSWORD.key);

        VCS result = makeVCSInstance(classname, dataDir, protocol, host, path, sshKeyFile, login, password);
        return result;
    }

    /**
     * Make CSV instance.
     * 
     * @param classname class name
     * @param dataDir root data dir
     * @param protocol protocol
     * @param host host
     * @param path path
     * @param sshKeyFile ssh key file
     * @param login login
     * @param password password
     * @return CSV instance
     */
    static protected VCS makeVCSInstance(String classname, File dataDir, String protocol,
            String host, String path, File sshKeyFile, String login, String password) {
        VCS result;
        try {
            Class<VCS> clazz = (Class<VCS>) ConvertUtils.convert(classname, Class.class);
            result = ConstructorUtils.invokeConstructor(clazz,
                    new Object[]{dataDir, protocol, host, path, sshKeyFile, login, password});
        } catch (Exception eee) {
            if (log.isErrorEnabled()) {
                log.error(t("isisfish.vcs.factory.cantinstanciate", classname), eee);
            }
            result = new VCSNone(dataDir, protocol, host, path, sshKeyFile, login, password);
        }
        return result;
    }

}
