/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2007 - 2010 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.vcs;

import org.nuiton.version.Version;

import java.io.File;
import java.io.FileFilter;
import java.util.List;
import java.util.Map;

/**
 * Version control system.
 * 
 * @author poussin
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public interface VCS extends FileFilter {
    
    /** CVS VCS Type */
    String TYPE_CVS = "cvs";
    
    /** None CVS Type */
    String TYPE_NONE = "none";
    
    /** SVN VCS Type */
    String TYPE_SVN = "svn";
    
    /**
     * VCS Status type.
     */
    enum Status {
        STATUS_NORMAL,
        STATUS_ADDED,
        STATUS_MODIFIED,
        STATUS_DELETED
    }

    /**
     * can be None, CVS or SVN. only None or SVN work
     */
    String VCS_TYPE = "vcs.type";
    
    /**
     * can be http, ssh, pserver, ...
     */
    String VCS_PROTOCOL = "vcs.protocol";

    /*
     * ssh key file
     *
    public static final String VCS_SSH_KEY_FILE = "vcs.ssh.keyFile";*/

    /**
     * remote server adresse (ip or serveur name) ex: svn.forge.codelutin.com
     */
    String VCS_HOST_NAME = "vcs.hostName";
    
    /**
     * remote repository path ex: /svnroot/isis-fish-data
     */
    String VCS_PATH = "vcs.path";
    
    /**
     * user login to access vcs ex: bpoussin
     */
    String VCS_USER_NAME = "vcs.username";
    
    /**
     * user passwd or passphrase (protocol dependent) to access vcs ex: xxxxxxxx
     */
    String VCS_USER_PASSWORD = "vcs.password";

    /**
     * Ajout un listener pouvant interdire les actions du vcs
     * 
     * @param l listener to add
     */
    void addVetoableActionListener(VetoableActionListener l);

    /**
     * Supprime un listener pouvant interdire les actions du vcs
     * 
     * @param l listener to remove
     */
    void remoteVetoableActionListener(VetoableActionListener l);

    /**
     * Get local repository directory
     * 
     * @return local repository
     */
    File getLocalRepository();

    /**
     * Return true, if local repository is valid repository for actual vcs.
     * 
     * @return validity
     */
    boolean isValidLocalRepository();

    /**
     * Return true, if local repository is writable.
     * (use trunk and is not anonymous)
     * 
     * @return writable
     * @throws VCSException
     */
    boolean isWriteable() throws VCSException;

    /**
     * Permit to force repository to read-only if value is false, otherwise
     * use normal rules.
     * 
     * @param value writable
     */
    void setWriteable(boolean value);

    /**
     * Cleanup a directory, removing all lock.
     * 
     * @param path path to cleanup
     * @throws VCSException
     */
    void cleanup(File path) throws VCSException;
    
    /**
     * Commit specified files, if files is null, all files are committed.
     * 
     * @param files files to commit
     * @param msg message used to commit
     * @throws VCSException
     */
    void commit(List<File> files, String msg) throws VCSException;

    /**
     * Add and commit file in server repository.
     * 
     * @param files list of file to add
     * @param msg message for commit
     * @throws VCSException
     */
    void add(List<File> files, String msg) throws VCSException;

    /**
     * get repository on server and put it in localdir.
     * 
     * @param tag tag to used, null = /trunk other is tags/[tag]
     * @param recurse if file is directory checkout sub file
     * @throws VCSException
     */
    void checkout(Version tag, boolean recurse) throws VCSException;

    /**
     * Delete and commit files in server repository.
     * 
     * @param files file to delete
     * @param msg message for commit
     * @throws VCSException
     */
    void delete(List<File> files, String msg) throws VCSException;

    /**
     * Return all changelog between local file version and remote repository
     * file version.
     * 
     * @param files
     * @return changelog for each file
     * @throws VCSException
     */
    Map<File, String> getChanglog(List<File> files) throws VCSException;

    /**
     * show diff between local file and repository file.
     * 
     * @param file file to get diff
     * @return string diff
     * @throws VCSException
     */
    String getDiff(File file) throws VCSException;

    /**
     * Get file local status.
     * 
     * @param file file to get status
     * @return file status
     * @throws VCSException
     */
    Status getLocalStatus(File file) throws VCSException;

    /**
     * Return list of all file on directory on remote server.
     * 
     * @param directory
     * @return files list
     * @throws VCSException
     */
    List<String> getFileList(File directory) throws VCSException;

    /**
     * get list of new or modified files on server.
     * 
     * @return list of modified or new files
     * @throws VCSException
     */
    List<File> getUpdatedFile() throws VCSException;

    /**
     * Ask if there are some new or modified files on server.
     * 
     * @return true if new file available
     * @throws VCSException
     */
    boolean haveUpdate() throws VCSException;

    /**
     * Get connection state.
     * 
     * @return true if server is connected
     */
    boolean isConnected();

    /**
     * Check if file is available on server.
     * 
     * @param file file to check
     * @return true if file available
     * @throws VCSException
     */
    boolean isOnRemote(File file) throws VCSException;

    /**
     * Look on server if version is tag repository.
     * 
     * @param version version number like 3.2
     * @return true if tag found with this name
     * @throws VCSException
     */
    boolean isTag(Version version) throws VCSException;

    /**
     * Check if file is up-to-date.
     * 
     * @param file file to check
     * @return true if file is in last version
     * @throws VCSException
     */
    boolean isUpToDate(File file) throws VCSException;

    /**
     * Check if file can be put in vcs repository. For example when you used
     * CVS, you must not put CVS file.
     * <p>
     * default refused .svn, CVS and ~ filename
     * 
     * Must be override, this default implementation return true.
     *
     * @param file
     * @return {@code true} if file is versionnable
     */
    boolean isVersionnableFile(File file);

    /**
     * Update file.
     * 
     * @param file files to update
     * @param recurse 
     * @return true if there are some merging conflict, false otherwise
     * @throws VCSException
     */
    List<File> update(File file, boolean recurse) throws VCSException;

    /**
     * Verifie la connexion et si le protocole a change, switch le repository
     * pour utiliser le nouveau protocole. Si on est en mode interface (mode
     * graphique) et que le switch se passe mal, demande a l'utilisateur
     * de nouvelle valeur pour le protocole (+ identifiant, ...)
     * @throws VCSException
     */
    void checkProtocol() throws VCSException;

    /**
     * Verifie si tous les fichiers du repository local sont les dernieres
     * version par rapport au serveur. Si ce n'est pas le cas et que l'on est
     * en mode interactif (mode graphique), on lui propose de mettre a jour
     * les fichiers, avec la possibilite de voir les changements sur les
     * fichiers
     * 
     * @return list of file with unresolved conflict
     * 
     * @throws VCSException
     */
    List<File> checkFileStatus() throws VCSException;

    /**
     * Get host.
     * 
     * @return host
     */
    String getHost();

    /**
     * Get login.
     * 
     * @return login
     */
    String getLogin();

    /**
     * Get password.
     * 
     * @return password
     */
    String getPassword();

    /**
     * Get repository path.
     * 
     * @return path
     */
    String getPath();

    /**
     * Get protocol.
     * 
     * @return protocol
     */
    String getProtocol();

    /**
     * Get ssh key file.
     * 
     * @return ssh key file
     */
    File getSshKeyFile();

    /**
     * Retourne le tag reellement utilise, par exemple si on a fait un
     * setTag(3.2.0) cette methode retourne "/tags/3.2.0", pour setTag(null)
     * on retourne "/trunk"
     * @return tag
     * @throws VCSException
     */
    String getTag() throws VCSException;

    /**
     * Change repository tag, used when we use some tag and we want to go to
     * trunk.
     * @param version version to go, if null trunk is used, otherwize
     * tags/version is used
     * @return a list of all file in conflict
     * @throws VCSException
     */
    List<File> setTag(Version version) throws VCSException;

    /**
     * Change host.
     * 
     * checkProtocol is automaticaly done
     * 
     * @param host new host.
     * 
     * @throws VCSException
     */
    void setHost(String host) throws VCSException;

    /**
     * Change login.
     * 
     * checkProtocol is automaticaly done
     * 
     * @param login new login
     * @throws VCSException
     */
    void setLogin(String login) throws VCSException;

    /**
     * Change password.
     * 
     * @param password password
     */
    void setPassword(String password);

    /**
     * Change path.
     * 
     * checkPath is automaticaly done
     * 
     * @param path new path
     * @throws VCSException
     */
    void setPath(String path) throws VCSException;

    /**
     * Change protocol.
     * 
     * checkProtocol is automaticaly done
     * 
     * @param protocol
     * @throws VCSException
     */
    void setProtocol(String protocol) throws VCSException;

    /**
     * Change ssk key file to use.
     * 
     * @param sshKeyFile new ssh kay file
     */
    void setSshKeyFile(File sshKeyFile);

}
