/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2008 - 2010 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.vcs;

import org.nuiton.version.Version;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * VCSNone (svn or vcs) must extends this class. This class can be used as dummy
 * VCSNone if not valid VCSNone found.
 * 
 * @author poussin
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class VCSNone extends AbstractVCS {

    public VCSNone(File localRepository, String protocol, String host,
            String path, File sshKeyFile, String login, String password) {
        super(localRepository, protocol, host, path, sshKeyFile, login,
                password);
    }

    @Override
    public boolean isValidLocalRepository() {
        return true;
    }

    /**
     * look on server if version is tag repository
     * @param version version number like 3.2
     * @return true if tag found with this name
     */
    @Override
    public boolean isTag(Version version) throws VCSException {
        return false;
    }

    /**
     * Ask if there are some new or modified files on server
     * @return true if new file available
     */
    @Override
    public boolean haveUpdate() throws VCSException {
        return false;
    }

    /**
     * get list of new or modified files on server
     * @return list of modified or new files
     */
    @Override
    public List<File> getUpdatedFile() throws VCSException {
        return new ArrayList<>();
    }

    /**
     * Return all changelog between local file version and remote repository
     * file version
     * @param files
     * @return changelog for each file
     */
    @Override
    public Map<File, String> getChanglog(List<File> files) {
        return new HashMap<>();
    }

    @Override
    public void cleanup(File path) throws VCSException {
        
    }

    @Override
    public void commit(List<File> files, String msg) throws VCSException {
        throw new VCSException("Can't commit file with dummy VCS");
    }

    /**
     * Add and commit file in server repository
     * @param files list of file to add
     * @param msg message for commit
     * @throws VCSException
     */
    @Override
    public void add(List<File> files, String msg) throws VCSException {
        throw new VCSException("Can't add file with dummy VCS");
    }

    /**
     * Get repository module on server and put it in destDir.
     * 
     * @param tag destination directory
     * @param b 
     * @throws VCSException
     */
    @Override
    public void checkout(Version tag, boolean b) throws VCSException {
        throw new VCSException("Can't checkout with dummy VCS");
    }

    /**
     * Delete and commit files in server repository
     * @param files file to delete
     * @param msg message for commit
     * @throws VCSException
     */
    @Override
    public void delete(List<File> files, String msg) throws VCSException {
        // do nothing
    }

    /**
     * Show diff between local file and repository file.
     * 
     * @param file
     * @return diff
     * @throws VCSException
     */
    @Override
    public String getDiff(File file) throws VCSException {
        throw new VCSException("Can't diff with dummy VCS");
    }

    @Override
    public Status getLocalStatus(File file) throws VCSException {
        return Status.STATUS_NORMAL;
    }
    
    /**
     * Return list of all file on directory on remote server.
     * 
     * @param directory
     * @return files list
     */
    @Override
    public List<String> getFileList(File directory) throws VCSException {
        List<String> result = new ArrayList<>();
        return result;
    }

    /**
     * Get connection state.
     * @return true if server is connected
     */
    @Override
    public boolean isConnected() {
        return false;
    }

    /**
     * Check if file is available on server
     * @param file file to check
     * @return true if file available
     * @throws VCSException
     */
    @Override
    public boolean isOnRemote(File file) throws VCSException {
        return false;
    }

    /**
     * Check if file is uptodate
     * @param file file to check
     * @return true if file is in last version
     * @throws VCSException
     */
    @Override
    public boolean isUpToDate(File file) throws VCSException {
        return true;
    }

    /**
     * Update file
     * @param file file to update
     * @return true if there are some merging conflict, false otherwise
     * @throws VCSException
     */
    @Override
    public List<File> update(File file, boolean recurse) throws VCSException {
        throw new VCSException("Can't update file with dummy VCS");
    }

    @Override
    public void checkProtocol() throws VCSException {
        // nothing to do
    }

    @Override
    public List<File> checkFileStatus() throws VCSException {
        // nothing to do
        return null;
    }

    @Override
    public boolean isWriteable() throws VCSException {
        return false;
    }

    @Override
    public List<File> setTag(Version version) throws VCSException {
        // do nothing
        return null;
    }

    @Override
    public String getTag() throws VCSException {
        return null;
    }
}
