/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2007 - 2010 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.vcs;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Abstract VCS implementation.
 * 
 * @author poussin
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public abstract class AbstractVCS implements VCS {

    /** Class logger */
    private static Log log = LogFactory.getLog(AbstractVCS.class);

    protected Set<VetoableActionListener> listeners = new HashSet<>();
    protected File localRepository;
    protected String protocol;
    protected String host;
    protected String path;
    protected File sshKeyFile;
    protected String login;
    protected String password;
    protected boolean writeable = true;

    public AbstractVCS(File localRepository, String protocol, String host,
            String path, File sshKeyFile, String login, String password) {
        this.localRepository = localRepository;
        this.protocol = protocol;
        this.host = host;
        this.path = path;
        this.sshKeyFile = sshKeyFile;
        this.login = login;
        this.password = password;
    }

    public void addVetoableActionListener(VetoableActionListener l) {
        listeners.add(l);
    }

    public void remoteVetoableActionListener(VetoableActionListener l) {
        listeners.remove(l);
    }

    public void setWriteable(boolean value) {
        this.writeable = value;
    }

    protected boolean fireAction(VCSActionEvent e, File... files) {
        boolean result = true;
        if (listeners.size() > 0) {
            // on evite la concurrence
            VetoableActionListener[] ls = listeners.toArray(new VetoableActionListener[0]);
            for (VetoableActionListener l : ls) {
                result = result && l.canDoAction(this, e, files);
                if (!result) {
                    // on s'arrete des qu'il y en a un qui ne souhaite pas 
                    // qu'on fasse l'action
                    break;
                }
            }
        }
        if (!result) {
            log.info("Canceled action: " + e);
        }
        return result;
    }

    public File getLocalRepository() {
        return localRepository;
    }

    public void setLocalRepository(File localRepository) {
        this.localRepository = localRepository;
    }

    public String getProtocol() {
        return protocol;
    }

    /**
     * checkProtocol is automatically done
     * @param protocol
     */
    public void setProtocol(String protocol) throws VCSException {
        this.protocol = protocol;
        checkProtocol();
    }

    public String getHost() {
        return host;
    }

    /**
     * checkProtocol is automatically done
     * @param host
     */
    public void setHost(String host) throws VCSException {
        this.host = host;
        checkProtocol();
    }

    public String getPath() {
        return path;
    }

    /**
     * checkProtocol is automatically done
     * @param path
     */
    public void setPath(String path) throws VCSException {
        this.path = path;
        
        // in case of set path, don't do checkProtocol
        // don't work with svn
        // FIXME won't work on SVN
        checkProtocol();
    }

    public File getSshKeyFile() {
        return sshKeyFile;
    }

    public void setSshKeyFile(File sshKeyFile) {
        this.sshKeyFile = sshKeyFile;
    }

    public String getLogin() {
        return login;
    }

    /**
     * checkProtocol is automatically done
     * @param login
     */
    public void setLogin(String login) throws VCSException {
        this.login = login;
        checkProtocol();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean accept(File pathname) {
        return isVersionnableFile(pathname);
    }

    public boolean isVersionnableFile(File file) {
        String filename = file.getName();
        return !".svn".equals(filename) && !"CVS".equals(filename)
                && !filename.endsWith("~") &&
                // si le fichier n'appartient pas a local repository,
                // il ne pourra pas etre versionne
                file.getAbsolutePath().startsWith(
                        getLocalRepository().getAbsolutePath());
    }

}
