/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2008 - 2015 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.vcs;

import fr.ifremer.isisfish.util.ssh.InvalidPassphraseException;
import fr.ifremer.isisfish.util.ssh.SSHAgent;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.version.Version;
import org.tmatesoft.svn.core.ISVNDirEntryHandler;
import org.tmatesoft.svn.core.ISVNLogEntryHandler;
import org.tmatesoft.svn.core.SVNCommitInfo;
import org.tmatesoft.svn.core.SVNDepth;
import org.tmatesoft.svn.core.SVNDirEntry;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNLogEntryPath;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.auth.ISVNAuthenticationManager;
import org.tmatesoft.svn.core.internal.io.dav.DAVRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.fs.FSRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.svn.SVNRepositoryFactoryImpl;
import org.tmatesoft.svn.core.internal.util.SVNPathUtil;
import org.tmatesoft.svn.core.internal.wc.DefaultSVNOptions;
import org.tmatesoft.svn.core.wc.ISVNOptions;
import org.tmatesoft.svn.core.wc.ISVNStatusHandler;
import org.tmatesoft.svn.core.wc.SVNClientManager;
import org.tmatesoft.svn.core.wc.SVNCommitClient;
import org.tmatesoft.svn.core.wc.SVNConflictChoice;
import org.tmatesoft.svn.core.wc.SVNDiffClient;
import org.tmatesoft.svn.core.wc.SVNInfo;
import org.tmatesoft.svn.core.wc.SVNLogClient;
import org.tmatesoft.svn.core.wc.SVNRevision;
import org.tmatesoft.svn.core.wc.SVNStatus;
import org.tmatesoft.svn.core.wc.SVNStatusClient;
import org.tmatesoft.svn.core.wc.SVNStatusType;
import org.tmatesoft.svn.core.wc.SVNUpdateClient;
import org.tmatesoft.svn.core.wc.SVNWCClient;
import org.tmatesoft.svn.core.wc.SVNWCUtil;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * SVN VCS.
 * 
 * Based on SVNKit.
 * 
 * @author poussin
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class VCSSVN extends AbstractVCS {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    protected static Log log = LogFactory.getLog(VCSSVN.class);

    /** Connection state. */
    protected enum ConnectionState {
        NOT_TESTED, OFF_LINE, ON_LINE
    }

    /** Svn client manager instance. */
    protected SVNClientManager svnManager = null;
    
    /** Current connection state. */
    protected ConnectionState connectionState = ConnectionState.NOT_TESTED;

    /**
     * Constructor.
     * 
     * @param localRepository
     * @param protocol
     * @param host
     * @param path
     * @param sshKeyFile
     * @param login
     * @param password
     */
    public VCSSVN(File localRepository, String protocol, String host,
            String path, File sshKeyFile, String login, String password) {
        super(localRepository, protocol, host, path, sshKeyFile, login,
                password);
        if (protocol.startsWith("file")) {
            FSRepositoryFactory.setup();
        } else if (protocol.startsWith("http")) {
            DAVRepositoryFactory.setup();
        } else {
            //svn://, svn+xxx:// (svn+ssh:// in particular)
            SVNRepositoryFactoryImpl.setup();
        }
    }

    /**
     * Return svnkit SVNManager.
     * 
     * @see SVNClientManager
     * @return SVNManager instance
     */
    protected SVNClientManager getSVNManager() {

        if (svnManager == null) {

            // log
            if (log.isInfoEnabled()) {
                try {
                    log.info("Try to connect to " + getRemoteURL());
                } catch (SVNException e) {
                    if (log.isErrorEnabled()) {
                        log.error("Can't get remote repo url");
                    }
                }
            }

            String login = getLogin();
            String password = getPassword();
            char[] passwd = password != null ? password.toCharArray() : null;

            DefaultSVNOptions options = SVNWCUtil.createDefaultOptions(true);
            ISVNAuthenticationManager auth;

            if (getProtocol().contains("ssh")) {

                if (sshKeyFile != null && sshKeyFile.canRead()) {

                    // log for private key
                    if (log.isInfoEnabled()) {
                        log.info("Using ssh private key : "
                                + sshKeyFile.getAbsolutePath());
                    }

                    char[] passphrase = null;
                    try {
                        passphrase = SSHAgent.getAgent().getPassphrase(sshKeyFile);
                    }
                    catch(InvalidPassphraseException e) {
                        if (log.isWarnEnabled()) {
                            log.warn("Can't get passphrase for key", e);
                        }
                    }
                    
                    auth = SVNWCUtil.createDefaultAuthenticationManager(
                            SVNWCUtil.getDefaultConfigurationDirectory(), //File configDir
                            login, // String userName
                            null, //String password
                            sshKeyFile, //File privateKey
                            passphrase, //String passphrase,
                            true); //boolean storeAuth)
                } else {

                    // log for private key
                    if (log.isWarnEnabled()) {
                        log.warn("Cannot read ssh private key : " + sshKeyFile);
                    }

                    auth = SVNWCUtil.createDefaultAuthenticationManager(
                            SVNWCUtil.getDefaultConfigurationDirectory(),
                            login, passwd);
                }
                
            } else {
                auth = SVNWCUtil.createDefaultAuthenticationManager(login, passwd);
            }

            svnManager = SVNClientManager.newInstance(options, auth);

        }
        return svnManager;
    }

    /**
     * Return true, if local repository is valid repository for actual vcs.
     * 
     * @return validity
     */
    @Override
    public boolean isValidLocalRepository() {
        File local = getLocalRepository();
        File svn = new File(local, ".svn");
        boolean result = svn.exists();
        return result;
    }

    /**
     * Get connection state.
     * 
     * @return true if server is connected
     */
    @Override
    public boolean isConnected() {
        if (connectionState == ConnectionState.NOT_TESTED) {
            try {
                SVNURL url = getRemoteURL();
                getSVNManager().getWCClient().doGetProperty(url, "",
                        SVNRevision.HEAD, SVNRevision.HEAD);
                connectionState = ConnectionState.ON_LINE;

                if (log.isInfoEnabled()) {
                    log.info(t("isisfish.vcs.vcssvn.isconnected.switchto",
                            getRemoteRepository()));
                }
            } catch (SVNException eee) {
                if (log.isWarnEnabled()) {
                    log.warn(t("isisfish.vcs.vcssvn.isconnected.switchoff",
                            getRemoteRepository()), eee);
                }
                connectionState = ConnectionState.OFF_LINE;
            }
        }
        boolean result = connectionState == ConnectionState.ON_LINE;
        return result;
    }

    /**
     * Verifie la connexion et si le protocole a change, switch le repository
     * pour utiliser le nouveau protocole. Si on est en mode interface (mode
     * graphique) et que le switch se passe mal, demande a l'utilisateur
     * de nouvelle valeur pour le protocole (+ identifiant, ...)
     * @throws VCSException
     */
    @Override
    public void checkProtocol() throws VCSException {

        // on doit verifier ici que seul le protocole a change
        // le doRelocate de svn, ne permet de ne change que
        // le protocol ou host par exemple
        // dans le cas d'un changement de path, le do relocate
        // echoue (operation non permise)

        try {
            // test que les protocoles, userInfo, host, port sont egaux.

            // copies locales
            File localRoot = getLocalRepository();
            SVNInfo info = getSVNManager().getWCClient().doInfo(localRoot,
                    SVNRevision.WORKING);
            SVNURL url = info.getURL();

            // url distante (suposée)
            SVNURL newUrl = getRemoteURL();

            // hack just for doRelocate to work
            newUrl = newUrl.setPath(url.getPath(), false);

            if (!url.getProtocol().equals(newUrl.getProtocol()) // http, svn ...
                    || (url.getUserInfo() == null && newUrl.getUserInfo() != url
                            .getUserInfo()) // username
                    || (url.getUserInfo() != null && !url.getUserInfo().equals(
                            newUrl.getUserInfo())) // username
                    || url.getPort() != newUrl.getPort() // 80
                    || !url.getHost().equals(newUrl.getHost())) {
                if (fireAction(VCSActionEvent.SWITCH_PROTOCOL)) {
                    if (log.isInfoEnabled()) {
                        log.info(t("isisfish.vcs.vcssvn.checkProtocol.relocate",
                                localRoot, url, newUrl));
                    }

                    // le relocate de SVNKit
                    // ne supporte que le changement de protocole/host/port
                    // pas le path
                    getSVNManager().getUpdateClient().doRelocate(localRoot,
                            url, newUrl, true);
                }
            }
        } catch (SVNException e) {
            throw new VCSException(
                    t("isisfish.vcs.vcssvn.checkProtocol.error"), e);
        }
    }

    /**
     * Vérifie si tous les fichiers du repository local sont les dernieres
     * version par rapport au serveur. Si ce n'est pas le cas et que l'on est
     * en mode interactif (mode graphique), on lui propose de mettre a jour
     * les fichiers, avec la possibilite de voir les changements sur les
     * fichiers
     * 
     * @return list of file with unresolved conflict
     * @throws VCSException
     */
    @Override
    public List<File> checkFileStatus() throws VCSException {
        List<File> fileInConflict = null;

        Map<File, SVNStatus> status = getRemoteStatus(null, true);
        // add local missing files
        status.putAll(getLocalStatus(null, true, SVNStatusType.STATUS_MISSING));

        // si des fichiers ont ete mis a jour sur le serveur on se synchronise
        if (status.size() > 0) {
            if (fireAction(VCSActionEvent.UPDATE_REPOSITORY, status.keySet().toArray(new File[0]))) {
                fileInConflict = update(null, true);
            }
        }
        
        return fileInConflict;
    }

    /**
     * Retourne l'url dans un objet SVNURL (svnkit).
     * 
     * @see SVNURL
     * @return l'url distante
     * @throws SVNException
     */
    protected SVNURL getRemoteURL() throws SVNException {
        SVNURL remoteURL = SVNURL.parseURIEncoded(getRemoteRepository());
        return remoteURL;
    }

    /**
     * Retourne l'url du repository distant.
     * 
     * ex: ssh+svn://labs.le.org/svnroot/isis-fish/data/branches/3.2
     * 
     * @return remote repository url
     */
    public String getRemoteRepository() {
        String proto = getProtocol();
        String user = getLogin();
        String host = getHost();
        String path = getPath();

        String result;

        if (proto.startsWith("file")) {
            result = proto + "://" + path;
        } else {
            if (user == null) {
                user = "";
            } else if (!"".equals(user)) {
                user = user + "@";
            }

            result = proto + "://" + user + host + path;
        }
        return result;
    }

    @Override
    public boolean isVersionnableFile(File file) {

        boolean result = super.isVersionnableFile(file);

        if (result) {
            ISVNOptions svnOptions = getSVNManager().getOptions();
            result &= !DefaultSVNOptions.isIgnored(svnOptions, file.getAbsolutePath());
        }

        return result;
    }

    /**
     * {@inheritDoc}
     * 
     * Perform a "svn cleanup" command.
     */
    @Override
    public void cleanup(File path) throws VCSException {

        File localFile = path;

        if (localFile == null) {
            localFile = getLocalRepository();
        }

        SVNWCClient wcClient = getSVNManager().getWCClient();
        try {
            wcClient.doCleanup(localFile);
        } catch (SVNException e) {
            throw new VCSException(t("isisfish.vcs.vcssvn.cleanup.error"), e);
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void commit(List<File> files, String msg) throws VCSException {

        // if can't commit
        if (!isWriteable()) {
            throw new VCSException(
                    t("isisfish.vcs.vcssvn.commit.errorreadonly"));
        }

        // list to array
        List<File> localFiles = files;
        if (localFiles == null) {
            localFiles = Arrays.asList(getLocalRepository());
        }

        if (fireAction(VCSActionEvent.COMMIT, localFiles.toArray(new File[0]))) {
            commitWithoutCheck(localFiles, msg);
        }
    }

    /**
     * Commit without ask for registred listeners.
     * 
     * @param files files to commit
     * @param msg message for commit
     * @throws VCSException
     */
    protected void commitWithoutCheck(List<File> files, String msg)
            throws VCSException {
        try {
            SVNCommitClient commitClient = getSVNManager().getCommitClient();

            File[] filesToCommit = files.toArray(new File[0]);
            
            // warning, SVNDepth.INFINITY is important, mandatory for deletion
            
            SVNCommitInfo commitInfo = commitClient.doCommit(filesToCommit, // paths
                    false, // keepLocks
                    msg, // commitMessage
                    null, // revisionProperties
                    null, // changelists
                    false, // keepChangelist
                    false, // force
                    SVNDepth.INFINITY); // depth

            if (log.isInfoEnabled()) {
                log.info(t("isisfish.vcs.vcssvn.global.torevision", commitInfo
                        .getNewRevision()));
            }
            
            // commitInfo.getNewRevision() == -1 if file with no modification
            // is commited
        } catch (SVNException e) {
            throw new VCSException(t("isisfish.vcs.vcssvn.commit.error"), e);
        }
    }

    /**
     * Perform :
     *  - svn add
     *  - commit
     */
    @Override
    public void add(List<File> files, String msg) throws VCSException {
        if (!isWriteable()) {
            throw new VCSException(t("isisfish.vcs.vcssvn.add.errorreadonly"));
        }
        try {
            if (fireAction(VCSActionEvent.ADD, files.toArray(new File[0]))) {
                for (File file : files) {
                    // FIXME ajoute dans le ignore les fichiers regions/<region>/data/*

                    // warning, SVNDepth.EMPTY is important
                    // commit only files specified in {@files}, to not commit
                    // entiere directory !!!
                    
                    SVNWCClient wcClient = getSVNManager().getWCClient();
                    wcClient.doAdd(file, // File path
                            true, // boolean force
                            false, // boolean mkdir
                            true, // boolean climbUnversionedParents
                            SVNDepth.EMPTY, // SVNDepth depth
                            false, // boolean includeIgnored
                            true); // boolean makeParents
                }
                commitWithoutCheck(files, msg);
            }
        } catch (SVNException eee) {
            throw new VCSException(t("isisfish.vcs.vcssvn.add.error"), eee);
        }
    }

    @Override
    public void checkout(Version tag, boolean recurse)
            throws VCSException {
        try {
            if (fireAction(VCSActionEvent.CHECKOUT, getLocalRepository())) {
                String tagPath = "/trunk/";
                if (tag != null) {
                    tagPath = "/tags/" + tag + "/";
                }

                SVNURL source = getRemoteURL().appendPath(tagPath, false);

                File destDir = getLocalRepository();
                destDir.mkdirs();

                SVNUpdateClient updateClient = getSVNManager()
                        .getUpdateClient();
                long newRevision = updateClient.doCheckout(source, // SVNURL url
                        destDir, // File dstPath
                        null, // SVNRevision pegRevision
                        SVNRevision.HEAD, // SVNRevision revision
                        SVNDepth.INFINITY, // SVNDepth depth
                        true); // boolean allowUnversionedObstructions

                if (log.isInfoEnabled()) {
                    log.info(t("isisfish.vcs.vcssvn.global.torevision",
                            newRevision));
                }
            }
        } catch (SVNException eee) {
            throw new VCSException(t("isisfish.vcs.vcssvn.checkout.error"), eee);
        }
    }

    public void delete(List<File> files, String msg) throws VCSException {
        if (!isWriteable()) {
            throw new VCSException(
                    t("isisfish.vcs.vcssvn.delete.errorreadonly"));
        }
        try {
            if (fireAction(VCSActionEvent.DELETE, files.toArray(new File[0]))) {
                for (File file : files) {
                    getSVNManager().getWCClient().doDelete(file, // path
                            true, // force
                            true, // deleteFiles
                            false); // dryRun
                }
                commitWithoutCheck(files, msg);
            }
        } catch (SVNException eee) {
            throw new VCSException(t("isisfish.vcs.vcssvn.delete.error"), eee);
        }
    }

    @Override
    public Status getLocalStatus(File file) throws VCSException {
        
        Status resultStatus = Status.STATUS_NORMAL;
        
        try {
            SVNStatusClient statusClient = getSVNManager().getStatusClient();
            SVNStatusType svnStatus = statusClient.doStatus(file, false).getNodeStatus();
            
            if (svnStatus == SVNStatusType.STATUS_MODIFIED) {
                resultStatus = Status.STATUS_MODIFIED;
            }
            else if (svnStatus == SVNStatusType.STATUS_DELETED) {
                resultStatus = Status.STATUS_DELETED;
            }
            else if (svnStatus == SVNStatusType.STATUS_ADDED || svnStatus == SVNStatusType.STATUS_UNVERSIONED) {
                resultStatus = Status.STATUS_ADDED;
            }

        } catch (SVNException e) {
            throw new VCSException("Can't get local status", e);
        }
        
        return resultStatus;
    }
    
    /**
     * Recherche le statut des fichiers locaux, ne retourne jamais les fichiers
     * NORMAL ou NONE sauf si demandé explicitement via wanted
     * 
     * @param file le repertoire a partir duquel on souhaite le status
     * @param recurse si l'on souhaite le faire recursivement
     * @param wanted l'ensemble des status type que l'on recheche, si vide
     * recherche tous les statuts
     * @return une map avec comme cle le File local et en valeur le status
     * @throws VCSException
     */
    protected Map<File, SVNStatus> getLocalStatus(File file, boolean recurse,
            SVNStatusType... wanted) throws VCSException {
        try {

            File localFile = file;

            if (localFile == null) {
                localFile = getLocalRepository();
            }

            final Map<File, SVNStatus> result = new HashMap<>();
            final Set<SVNStatusType> acceptedStatusType = new HashSet<>(Arrays.asList(wanted));

            ISVNStatusHandler handler = status -> {
                if ((acceptedStatusType.isEmpty()
                        && status.getNodeStatus() != SVNStatusType.STATUS_NONE && status
                        .getNodeStatus() != SVNStatusType.STATUS_NORMAL)
                        || acceptedStatusType.contains(status.getNodeStatus())) {
                    File statusFile = status.getFile();
                    if (!statusFile.isDirectory() ||
                            status.getRemoteNodeStatus() == SVNStatusType.STATUS_ADDED ||
                            status.getRemoteNodeStatus() == SVNStatusType.STATUS_DELETED) {
                        // on ne met pas les repertoires pere dans le status
                        // car en fait ca veut dire qu'un fichier/rep dans ce
                        // repertoire a ete ajout/modifier/delete, et on l'aura
                        // aussi dans les resultats et ca suffit
                        result.put(statusFile, status);
                    }
                }
            };

            SVNStatusClient statusClient = getSVNManager().getStatusClient();
            statusClient.doStatus(localFile, // File
                    SVNRevision.WORKING, // Revision
                    SVNDepth.INFINITY, // Depth
                    false, // remote
                    !acceptedStatusType.contains(SVNStatusType.STATUS_NORMAL), // reportAll
                    acceptedStatusType.contains(SVNStatusType.STATUS_IGNORED), // includeIgnored
                    false, // obsolete (not used)
                    handler, // handler
                    null); // changeLists

            return result;
        } catch (SVNException eee) {
            throw new VCSException(t("isisfish.vcs.vcssvn.localstatus.error"),
                    eee);
        }
    }

    /**
     * Retourne la liste des fichiers qui ont été mis à jour sur le serveur.
     * 
     * Le status des fichiers retourné est :
     * <ul>
     * <li>SVNStatusType.STATUS_ADDED (remote) pour ajout local
     * <li>SVNStatusType.STATUS_DELETED (remote) pour suppression locale
     * </ul>
     * 
     * @param file
     * @param recurse
     * @return remote status
     * @throws VCSException
     */
    protected Map<File, SVNStatus> getRemoteStatus(File file, boolean recurse) throws VCSException {
        try {

            File localFile = file;
            if (localFile == null) {
                localFile = getLocalRepository();
            }

            final Map<File, SVNStatus> result = new HashMap<>();

            ISVNStatusHandler handler = status -> {
                if (log.isDebugEnabled()) {
                    log.debug("Status on file " + status.getFile() + " rstatus:"
                            + status.getRemoteNodeStatus() + " lstatus:" + status.getNodeStatus());
                }

                if (!status.getRemoteNodeStatus().equals(SVNStatusType.STATUS_NONE) &&
                        !status.getRemoteNodeStatus().equals(SVNStatusType.STATUS_NORMAL)) {
                    File statusFile = status.getFile();
                    if (!statusFile.isDirectory() ||
                            status.getRemoteNodeStatus().equals(SVNStatusType.STATUS_ADDED) ||
                            status.getRemoteNodeStatus().equals(SVNStatusType.STATUS_DELETED)) {
                        // on ne met pas les repertoires pere dans le status
                        // car en fait ca veut dire qu'un fichier/rep dans ce
                        // repertoire a ete ajout/modifier/delete, et on l'aura
                        // aussi dans les resultats et ca suffit
                        result.put(statusFile, status);
                    }
                }
            };

            SVNStatusClient statusClient = getSVNManager().getStatusClient();
            statusClient.doStatus(localFile, // File path,
                    SVNRevision.HEAD, // SVNRevision revision,
                    SVNDepth.INFINITY, // SVNDepth depth,
                    true, // boolean remote,
                    true, // boolean reportAll,
                    false, // boolean includeIgnored,
                    true, // boolean collectParentExternals,
                    handler, // ISVNStatusHandler handler,
                    null); // Collection changeLists

            return result;
        } catch (SVNException eee) {
            throw new VCSException(t("isisfish.vcs.vcssvn.remotestatus.error"),
                    eee);
        }
    }

    /**
     * Return all changelog between local file version and remote repository
     * file version.
     * 
     * @param files
     * @return changelog for each file
     * @throws VCSException
     */
    public Map<File, String> getChanglog(List<File> files) throws VCSException {

        final Map<File, String> changLog = new HashMap<>();
        
        try {
            SVNInfo info = getSVNManager().getWCClient().doInfo(getLocalRepository(),
                    SVNRevision.WORKING);
            final String repoPath = info.getURL().getPath();
            final String repoRootPath = info.getRepositoryRootURL().getPath();
            
            // Handler
            ISVNLogEntryHandler handler = logEntry -> {

                for (Map.Entry<String, SVNLogEntryPath> entry : logEntry.getChangedPaths().entrySet()) {

                    String path = entry.getKey();
                    SVNLogEntryPath value = entry.getValue();

                    //log
                    if(log.isDebugEnabled()) {
                        log.debug("path changed : " + path + " =  [" + value.getType() + "] " + logEntry.getMessage());
                    }

                    // handler return url as :
                    //  /isis-fish-data/trunk/scripts/version.txt
                    // just transform in into
                    //  scripts/version.txt
                    String relativePath = SVNPathUtil.getRelativePath(repoPath, repoRootPath + path);

                    // tranform SVNLogEntryPath into file
                    File file = new File(getLocalRepository().getAbsoluteFile() + File.separator + relativePath);

                    changLog.put(file, logEntry.getMessage());

                }
            };

            File[] filesToGetLog = files.toArray(new File[0]);

            // do svn ls on remote
            SVNLogClient logClient = getSVNManager().getLogClient();
            logClient.doLog(filesToGetLog, // File[] paths,
                    SVNRevision.WORKING, // SVNRevision startRevision,
                    SVNRevision.HEAD, // SVNRevision endRevision,
                    false, // boolean stopOnCopy,
                    true, // boolean discoverChangedPaths,
                    20, // long limit,
                    handler); // ISVNLogEntryHandler handler

        } catch (SVNException e) {
            throw new VCSException(t("Can't get changlog"), e);
        }

        return changLog;

    }

    /**
     * show diff between local file and repository file.
     * 
     * @param file file to get diff
     * @return string diff
     * @throws VCSException
     */
    @Override
    public String getDiff(File file) throws VCSException {

        String diff;

        try (ByteArrayOutputStream byte1 = new ByteArrayOutputStream()) {

            SVNDiffClient diffClient = getSVNManager().getDiffClient();
            diffClient.doDiff(file, // File path1,
                    SVNRevision.HEAD, // SVNRevision rN,
                    file, // File path2,
                    SVNRevision.WORKING, // SVNRevision rM,
                    SVNDepth.IMMEDIATES, // SVNDepth depth,
                    false, // boolean useAncestry,
                    byte1, // OutputStream result,
                    null); // Collection changeLists

            diff = byte1.toString(StandardCharsets.UTF_8.name());

        } catch (SVNException | IOException e) {
            throw new VCSException(t("isisfish.vcs.vcssvn.diff.error"), e);
        }

        return diff;
    }

    @Override
    public List<String> getFileList(File directory) throws VCSException {

        File localFile = directory;
        if (localFile == null) {
            localFile = getLocalRepository();
        }

        // Handler
        final List<String> files = new ArrayList<>();
        ISVNDirEntryHandler handler = dirEntry -> {

            String path = dirEntry.getRelativePath();
            // first path is "", exclude it
            if (!path.isEmpty()) {
                files.add(path);
            }
        };

        try {
            // do svn ls on remote
            SVNLogClient logClient = getSVNManager().getLogClient();
            logClient.doList(localFile, // SVNURL url,
                    SVNRevision.HEAD, // SVNRevision pegRevision,
                    SVNRevision.HEAD, // SVNRevision revision,
                    false, // boolean fetchLocks,
                    SVNDepth.IMMEDIATES, // SVNDepth depth,
                    SVNDirEntry.DIRENT_ALL, // int entryFields,
                    handler);
        } catch (SVNException e) {
            throw new VCSException(t("isisfish.vcs.vcssvn.list.error"), e);
        }

        return files;
    }

    /**
     * get list of new or modified files on server.
     * 
     * @return list of modified or new files
     * @throws VCSException
     */
    public List<File> getUpdatedFile() throws VCSException {

        try {

            final List<File> result = new ArrayList<>();

            ISVNStatusHandler handler = status -> {

                // log
                if (log.isDebugEnabled()) {
                    log.debug(t("isisfish.vcs.vcssvn.global.filestatus",
                            status.getFile().getAbsolutePath(), status
                                    .getRemoteNodeStatus().toString()));
                }

                if (status.getRemoteNodeStatus() == SVNStatusType.STATUS_ADDED
                        || status.getRemoteNodeStatus() == SVNStatusType.STATUS_MODIFIED) {

                    // log
                    if (log.isDebugEnabled()) {
                        log.debug(t("isisfish.vcs.vcssvn.global.foundUpdatedFile",
                                        status.getFile().getAbsolutePath()));
                    }
                    result.add(status.getFile());

                }
            };

            SVNStatusClient statusClient = getSVNManager().getStatusClient();
            statusClient.doStatus(getLocalRepository(), // File path,
                    SVNRevision.HEAD, // SVNRevision revision,
                    SVNDepth.INFINITY, // SVNDepth depth,
                    true, // boolean remote,
                    true, // boolean reportAll,
                    false, // boolean includeIgnored,
                    true, // boolean collectParentExternals,
                    handler, // ISVNStatusHandler handler,
                    null); // Collection changeLists

            return result;
        } catch (SVNException eee) {
            throw new VCSException(t("isisfish.vcs.vcssvn.getupdate.error"),
                    eee);
        }
    }

    /**
     * Ask if there are some new or modified files on server.
     * 
     * @return true if new file available
     * @throws VCSException
     */
    public boolean haveUpdate() throws VCSException {

        // c'est juste si la liste renvoyé par getUpdatedFile() n'est pas vide ?
        List<File> updatedFiles = getUpdatedFile();

        boolean result = false;

        if (updatedFiles != null && !updatedFiles.isEmpty()) {
            result = true;
        }
        return result;
    }

    /**
     * Check if file is available on server.
     * 
     * @param file file to check
     * @return true if file available
     * @throws VCSException
     */
    public boolean isOnRemote(File file) throws VCSException {

        File localFile = file;
        if (localFile == null) {
            localFile = getLocalRepository();
        }

        boolean isOnRemote = false;

        try {
            SVNStatusClient statusClient = getSVNManager().getStatusClient();

            SVNStatus status = statusClient.doStatus(localFile, true /*remote*/);
            SVNStatusType localStatus = status.getNodeStatus();
            SVNStatusType remoteStatus = status.getRemoteNodeStatus();

            if (log.isDebugEnabled()) {
                log.debug(t("isisfish.vcs.vcssvn.global.filelocalandremotestatus",
                                localFile.getAbsolutePath(), localStatus,
                                remoteStatus));
            }
            
            // don't return true if:
            // - file is locally added
            // - file is remotely deleted
            if (!localStatus.equals(SVNStatusType.STATUS_ADDED) && 
                    !remoteStatus.equals(SVNStatusType.STATUS_UNVERSIONED) &&
                    !remoteStatus.equals(SVNStatusType.STATUS_DELETED)) {
                isOnRemote = true;
            }

        } catch (SVNException e) {
            // catch exception
            // if exception, file doesn't exists on server
            // result is still 'false'
            if (log.isDebugEnabled()) {
                log.debug(t("isisfish.vcs.vcssvn.isonremote.error", localFile
                        .getAbsolutePath()), e);
            }
        }

        return isOnRemote;
    }

    /**
     * Check if file is uptodate.
     * 
     * @param file file to check
     * @return true if file is in last version
     * @throws VCSException
     */
    @Override
    public boolean isUpToDate(File file) throws VCSException {

        File localFile = file;
        if (localFile == null) {
            localFile = getLocalRepository();
        }

        boolean isUpToDate = false;

        try {
            SVNStatusClient statusClient = getSVNManager().getStatusClient();
            SVNStatus status = statusClient.doStatus(localFile, true /*remote*/);
            SVNStatusType localStatus = status.getNodeStatus();
            SVNStatusType remoteStatus = status.getRemoteNodeStatus();

            // TODO peut on dire que le fichier est à jour
            // si le status local est normal et le distant est none
            if (localStatus == SVNStatusType.STATUS_NORMAL
                    && remoteStatus == SVNStatusType.STATUS_NONE) {
                isUpToDate = true;
            }

        } catch (SVNException eee) {
            throw new VCSException(t("isisfish.vcs.vcssvn.isuptodate.error"),
                    eee);
        }

        return isUpToDate;
    }

    @Override
    public List<File> update(File file, boolean recurse) throws VCSException {
        List<File> result = new ArrayList<>();
        try {

            File localFile = file;
            if (localFile == null) {
                localFile = getLocalRepository();
            }
            if (!accept(localFile)) {
                throw new VCSException(
                        t("isisfish.vcs.vcssvn.update.notinlocal"));
            }
            if (fireAction(VCSActionEvent.UPDATE, localFile)) {
                // si le repertoire pere, n'est pas encore dans le repo local
                // il faut aussi l'ajouter
                if (!localFile.getParentFile().exists()) {
                    update(localFile.getParentFile(), false);
                }

                SVNUpdateClient updateClient = getSVNManager()
                        .getUpdateClient();
                long newRevision = updateClient.doUpdate(localFile, // File file
                        SVNRevision.HEAD, // SVNRevision revision
                        recurse ? SVNDepth.INFINITY : SVNDepth.FILES, // SVNDepth depth
                        true, // boolean allowUnversionedObstructions
                        false); // boolean depthIsSticky

                if (log.isInfoEnabled()) {
                    log.info(t("isisfish.vcs.vcssvn.global.torevision",
                            newRevision));
                }

                // recherche de tous les fichiers locaux en conflit
                Map<File, SVNStatus> status = getLocalStatus(localFile,
                        recurse, SVNStatusType.STATUS_CONFLICTED);
                if (status.size() > 0) {
                    result.addAll(status.keySet());
                    // on supprime les conflits pour pouvoir commiter convenablement
                    // les fichiers
                    // FIXME use conflit resolution choice ?
                    SVNWCClient wcClient = getSVNManager().getWCClient();
                    wcClient.doResolve(localFile, // File file
                            recurse ? SVNDepth.INFINITY : SVNDepth.FILES, // depth
                            SVNConflictChoice.MERGED); // ConflictChoice
                }

            }
        } catch (SVNException e) {
            throw new VCSException(t("isisfish.vcs.vcssvn.update.error"), e);
        }
        return result;
    }

    @Override
    public boolean isWriteable() throws VCSException {

        // si on est clairement en anonyme
        boolean result = writeable;
        if (writeable) {
            // check normal rules
            String login = getLogin();
            result = result && login != null && !"".equals(login)
                    && !"anonymous".equals(login);
            // meme s'il n'y a pas d'utilisateur, mais qu'on utilise le protocole file:// on est writeable
            result = result || getProtocol().startsWith("file");
            // ou que le repertoire n'est pas utilisable pour ce type de vcs
            result = result && isValidLocalRepository();
            
            // ou que l'on est dans un tag, donc par convention not writeable
            // since 3.3.0.0 users can commit on tag
            //result = result && !getTag().startsWith("/tags");
        }

        // on indique que l'utilisateur n'a pas le droit d'ecrire
        return result;
    }

    @Override
    public boolean isTag(Version version) throws VCSException {
        boolean result = version == null; // le trunk exist toujours
        if (!result) {
            try {
                SVNURL url = getRemoteURL();
                url = url.appendPath("tags/" + version, true);
                SVNInfo info = getSVNManager().getWCClient().doInfo(url, SVNRevision.HEAD,
                        SVNRevision.HEAD);
                // si le tag n'existe pas, on n'arrive pas ici car une exception
                // est leve, et donc result reste a faux
                result = info != null;
            } catch (SVNException eee) {
                log.debug(t("isisfish.vcs.vcssvn.istag.notexist", version));
            }
        }
        return result;
    }

    @Override
    public String getTag() throws VCSException {
        try {
            File localRoot = getLocalRepository();
            SVNInfo info = getSVNManager().getWCClient().doInfo(localRoot,
                    SVNRevision.WORKING);
            String url = info.getURL().toDecodedString();
            String result = "/trunk";
            if (!url.endsWith("/trunk")) {
                // on est pas sur le trunk, on est soit sur un tag ou une
                // branche, dans les deux cas, il faut descendre de 2 /
                int i = url.lastIndexOf("/");
                i = url.lastIndexOf("/", i - 1);
                result = url.substring(i);
            }
            return result;
        } catch (SVNException eee) {
            throw new VCSException(t("isisfish.vcs.vcssvn.gettag.error"), eee);
        }
    }

    @Override
    public List<File> setTag(Version version) throws VCSException {

        List<File> filesInConflict = null;

        try {

            String tag = "/trunk";
            if (version != null) {
                tag = "/tags/" + version;
            }

            String currentTag = getTag();
            if (!tag.equals(currentTag)) {
                // on ne fait le switch que si le tag change rellement, sinon
                // c equivalent a faire un update, ce que l'on ne souhaite pas
                // forcement
                if (fireAction(VCSActionEvent.SWITCH)) {
                    File localRoot = getLocalRepository();

                    SVNURL newUrl = getRemoteURL();
                    newUrl = newUrl.appendPath(tag, true);

                    if (log.isInfoEnabled()) {
                        log.info(t("isisfish.vcs.vcssvn.settag.switchfromto",
                                currentTag, tag));
                    }

                    SVNUpdateClient updateClient = getSVNManager()
                            .getUpdateClient();
                    long newRevision = updateClient.doSwitch(localRoot, // File path
                            newUrl, // SVNURL url
                            SVNRevision.HEAD, // SVNRevision pegRevision
                            SVNRevision.HEAD, // SVNRevision revision
                            SVNDepth.INFINITY, // SVNDepth depth
                            true, // boolean allowUnversionedObstructions 
                            false);// boolean depthIsSticky

                    // chatellier: allowUnversionedObstructions must be true
                    // if there is unversionned file or folder in repo, update will fail
                    // with org.tmatesoft.svn.core.SVNException: svn: Unable to lock 'xxx'

                    if (log.isInfoEnabled()) {
                        log.info(t("isisfish.vcs.vcssvn.global.torevision",
                                newRevision));
                    }

                    // recherche de tous les fichiers locaux en conflit apres le switch
                    Map<File, SVNStatus> status = getLocalStatus(localRoot,
                            true, SVNStatusType.STATUS_CONFLICTED);
                    if (!status.isEmpty()) {
                        filesInConflict = new ArrayList<>(status.keySet());
                        // on supprime les conflits pour pouvoir commiter convenablement
                        // les fichiers

                        // FIXME use conflit resolution choice ?
                        SVNWCClient wcClient = getSVNManager().getWCClient();
                        wcClient.doResolve(localRoot, // File file
                                SVNDepth.INFINITY, // depth
                                SVNConflictChoice.MERGED); // ConflictChoice
                    }
                }
            }

        } catch (SVNException eee) {
            throw new VCSException(t("isisfish.vcs.vcssvn.setTag.error"), eee);
        }

        return filesInConflict;
    }
}
