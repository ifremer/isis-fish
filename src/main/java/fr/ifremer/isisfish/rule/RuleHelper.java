/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2007 - 2010 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.rule;

import java.util.Properties;

import org.nuiton.topia.TopiaContext;

import fr.ifremer.isisfish.datastore.RegionStorage;
import fr.ifremer.isisfish.datastore.StorageHelper;

/**
 * Helper pour effectuer des manipulations sur les règles.
 * 
 * Permet de :
 * <ul>
 * <li>affecter les valeurs aux parametres des règles
 * </ul>
 * 
 * Created: 6 juin 07 12:03:42
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class RuleHelper extends StorageHelper {

    /**
     * Recupere dans propriétés les valeurs des champs spécifiques à la règle et
     * met à jour les champs de la règle.
     * 
     * @param ruleIndex l'index de la règle
     * @param rule la règle dont les paramètres doivent être lu depuis les
     *        propriétés
     * @param region le topia context dont on a besoin
     * @param props les propriétés contenant les paramètres de la règle
     * 
     * @deprecated since 3.3.0.0, use {@link #populateRule(int, TopiaContext , Rule, Properties)} instead.
     */
    @Deprecated
    public static void populateRule(int ruleIndex, RegionStorage region, Rule rule, Properties props) {
        populateRule(ruleIndex, region.getStorage(), rule, props);
    }

    /**
     * Recupere dans propriétés les valeurs des champs spécifiques à la règle et
     * met à jour les champs de la règle.
     * 
     * @param ruleIndex l'index de la règle
     * @param rule la règle dont les paramètres doivent être lu depuis les
     *        propriétés
     * @param context le topia context dont on a besoin
     * @param props les propriétés contenant les paramètres de la règle
     */
    public static void populateRule(int ruleIndex, TopiaContext context, Rule rule, Properties props) {
        populateStorageParams(ruleIndex, context, rule, props, "rule");
    }
    
    /**
     * Permet de mettre les parametres de la regle sous une forme String pour
     * pouvoir les relire ensuite.
     *
     * @param ruleIndex l'index de la rule
     * @param context le context
     * @param rule La regle dont on souhaite mettre les parametres dans l'objet
     *        Properties retourne
     * @return L'objet Properties contenant les valeurs des parametres de la
     *         regle
     */
    public static Properties getRuleAsProperties(int ruleIndex, TopiaContext context, Rule rule) {
        return getParamsAsProperties(ruleIndex, context, rule, "rule");
    }
}
