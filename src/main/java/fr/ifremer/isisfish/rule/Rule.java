/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2011 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.rule;

import fr.ifremer.isisfish.entities.Metier;
import fr.ifremer.isisfish.result.NecessaryResult;
import fr.ifremer.isisfish.simulator.SimulationContext;
import fr.ifremer.isisfish.types.TimeStep;

/**
 * Interface d'une regle. setValue et getValue permet de stocker des
 * informations liées a l'instance de la règle.
 * 
 * <p> Lorsqu'on implante une rule, les paramètres que l'on souhaite qu'elle 
 * ait, doivent etre des attributs public commencant par param.
 * 
 * <p> Les types acceptable pour un parametre sont:
 * <ul>
 * <li> un nombre (int, long, float, double, ...)</li>
 * <li> un boolean</li>
 * <li> une chaine String</li>
 * <li> un objet Date ({@link fr.ifremer.isisfish.types.TimeStep})</li>
 * <li> un objet Month ({@link fr.ifremer.isisfish.types.Month})</li>
 * <li> Le type d'une entité (@see fr.ifremer.isisfish.entities)</li>
 * </ul>
 * 
 * <p> Pour supporter d'autre type il faut créer des converters commons-beanutils
 * //{@link fr.ifremer.isisfish.rule.RuleHelper#getRuleAsProperties(int, org.nuiton.topia.TopiaContext, Rule)}
 * 
 * Created: 12 janv. 2006 17:02:43
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public interface Rule extends NecessaryResult {

    /**
     * Permet d'afficher a l'utilisateur une aide sur la regle.
     * 
     * @return L'aide ou la description de la regle
     * @throws Exception
     */
    String getDescription() throws Exception;

    /**
     * Appelé au démarrage de la simulation, cette méthode permet d'initialiser
     * la règle.
     * 
     * @param context La simulation pour lequel on utilise cette regle
     * @throws Exception
     */
    void init(SimulationContext context) throws Exception;

    /**
     * La condition qui doit etre vrai pour faire les actions.
     * 
     * Si la condition return vrai alors les methodes
     * {@link #preAction(SimulationContext, TimeStep, Metier)} et
     * {@link #postAction(SimulationContext, TimeStep, Metier)} seront appelée.
     * 
     * @param context la simulation pour lequel on utilise cette regle
     * @param step le pas de temps courant
     * @param metier le metier
     * @return vrai si on souhaite que les actions soit faites
     * @throws Exception s'il y a une erreur, met fin a la simulation
     */
    boolean condition(SimulationContext context, TimeStep step, Metier metier)
            throws Exception;

    /**
     * Si la condition est vrai alors cette action est executée avant le pas
     * de temps de la simulation.
     * 
     * @param context la simulation pour lequel on utilise cette regle
     * @param step le pas de temps courant
     * @param metier le metier
     * @throws Exception s'il y a une erreur, met fin a la simulation
     */
    void preAction(SimulationContext context, TimeStep step, Metier metier)
            throws Exception;

    /**
     * Si la condition est vrai alors cette action est executée apres le pas
     * de temps de la simulation.
     * 
     * @param context la simulation pour lequel on utilise cette regle
     * @param step le pas de temps courant
     * @param metier le metier
     * @throws Exception s'il y a une erreur, met fin a la simulation
     */
    void postAction(SimulationContext context, TimeStep step, Metier metier)
            throws Exception;

    /**
     * Permet de recuperer une valeur prealablement stockée avec un
     * {@link #setValue(String, Object)}.
     * 
     * @param name le nom de la valeur souhaitée
     * @return la valeur ou null si aucune valeur ne porte se nom
     */
    Object getValue(String name);

    /**
     * Permet de stocker une valeur en fonction d'une cle.
     * 
     * @param name le nom de la valeur
     * @param value la valeur
     */
    void setValue(String name, Object value);
}
