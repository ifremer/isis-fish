/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2011 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.rule;

import java.util.HashMap;
import java.util.Map;

/**
 * Class abstraite d'une regles comprenant le code commun à toutes les regles.
 * 
 * Permet de :
 * <ul>
 * <li>Gerer les valeurs entre les regles
 * </ul>
 * 
 * Created: 12 janv. 2006 17:12:51
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public abstract class AbstractRule implements Rule {

    protected transient Map<String, Object> values = new HashMap<>();

    @Override
    public Object getValue(String name) {
        return values.get(name);
    }

    @Override
    public void setValue(String name, Object value) {
        values.put(name, value);
    }
}
