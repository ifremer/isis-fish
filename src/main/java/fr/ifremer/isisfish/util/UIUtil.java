package fr.ifremer.isisfish.util;

/*-
 * #%L
 * ISIS-Fish
 * %%
 * Copyright (C) 2020 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import java.awt.Image;
import java.awt.Window;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class UIUtil {

    private static final Log log = LogFactory.getLog(UIUtil.class);

    public static ImageIcon getIconImage(String path) {
        URL url = UIUtil.class.getResource(path);
        ImageIcon image = new ImageIcon(url);
        return image;
    }

    public static void setIconImage(Window w) {
        try (InputStream imageStream = UIUtil.class.getResourceAsStream("/images/sensitivities.gif")) {
            Image image = ImageIO.read(imageStream);
            w.setIconImage(image);
        } catch (IOException ex) {
            if (log.isErrorEnabled()) {
                log.error("Can't set frame icon", ex);
            }
        }
    }
}
