package fr.ifremer.isisfish.util;

/*
 * #%L
 * IsisFish
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 1999 - 2018 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.FileUtils;

/**
 * Cette class réintroduit les méthodes qui ont été supprimées dans nuiton-utils
 * (car des equivalents commons existent).
 * 
 * Pour facilité la vie des utilisateurs, on les regroupent ici.
 * 
 * @author echatellier
 */
public class ScriptUtil {

    /**
     * Reads the contents of a file into a String. The file is always closed.
     * 
     * @param file the file to read, must not be {@code null}
     * @return the file contents, never {@code null}
     * @since 4.3.0
     */
    public static String readAsString(File file) throws IOException {
        return FileUtils.readFileToString(file, StandardCharsets.UTF_8);
    }

    /**
     * Writes a String to a file creating the file if it does not exist.
     *
     * @param file the file to write
     * @param content the content to write to the file
     * @since 4.3.0
     */
    public static void writeString(File file, String content) throws IOException {
        FileUtils.writeStringToFile(file, content, StandardCharsets.UTF_8);
    }

    /**
     * Retourne le nom du fichier entre dans la boite de dialogue.
     * Si le bouton annuler est utilisé, ou qu'il y a une erreur retourne null.
     *
     * @param patternOrDescriptionFilters les filtres a utiliser, les chaines doivent etre données
     *                                    par deux, le pattern du filtre + la description du filtre
     * @return le fichier accepté, ou null si rien n'est chois ou l'utilisateur a annulé
     * @since 4.4.0
     */
    public static File getFile(String... patternOrDescriptionFilters) {
        return IsisFileUtil.getFile(patternOrDescriptionFilters);
    }
}
