/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2024 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.util;

import static org.nuiton.i18n.I18n.t;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ifremer.isisfish.annotations.Args;
import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.result.NecessaryResult;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.simulator.SimulationContext;
import java.util.HashMap;
import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Permet d'evaluer les equations ecritent en Java
 * 
 * Created: 3 juil. 2006 23:44:48
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class EvaluatorHelper {

    /** Logger for this class. */
    private static Log log = LogFactory.getLog(EvaluatorHelper.class);

    public static final String FORMULE_CACHE_FILE = "formules.cache";

    /**
     * Regex to match import:
     * - "^(import ...;)" for import at line beginning
     * - ";(import ...;)" for multiple import on same line
     */
    static protected Pattern grepImportPattern = Pattern.compile("(?:^\\s*|(?<=;)\\s*)(import[^;]+;)", Pattern.DOTALL + Pattern.MULTILINE);

    private static final String HASH_CACHE_KEY = "__hashCache__";

    protected static String getHashCache(File fileCheckSum) {
        String result = "";

        SimulationContext context = SimulationContext.get();
        SimulationStorage simulationStorage = context.getSimulationStorage();
        if (simulationStorage == null) {
            // on est pas dans une simulation on verifie dans le fichier
            if (fileCheckSum.exists()) {
                try {
                    result = FileUtils.readFileToString(fileCheckSum, StandardCharsets.UTF_8);
                } catch (IOException eee) {
                    log.info("Can't read old checkSum:  " + fileCheckSum, eee);
                }
            }
        } else {
            // on est dans une simulation, on verifie dans le cache de la simulation
            Map<String, String> cache = (Map<String, String>)context.getValue(HASH_CACHE_KEY);
            if (cache != null) {
                result = StringUtils.defaultString(cache.get(fileCheckSum.getName()));
            }
        }

        return result;
    }

    protected static void setHashCache(File fileCheckSum, String hashcode) {
        SimulationContext context = SimulationContext.get();
        SimulationStorage simulationStorage = context.getSimulationStorage();
        if (simulationStorage == null) {
            // on est pas dans une simulation
            // on ecrit dans le fichier
            try {
                FileUtils.writeStringToFile(fileCheckSum, hashcode, StandardCharsets.UTF_8);
            } catch (IOException eee) {
                log.info("Can't write checkSum:  " + fileCheckSum, eee);
            }
        } else {
            // on est dans une simulation, on verifie dans le cache de la simulation
            Map<String, String> cache = (Map<String, String>)context.getValue(HASH_CACHE_KEY);
            if (cache == null) {
                context.setValue(HASH_CACHE_KEY, cache = new HashMap<>());
            }

            cache.put(fileCheckSum.getName(), hashcode);
        }
    }

    protected static File getFormuleCacheFile(File directory) {
        return new File(directory, FORMULE_CACHE_FILE);
    }

    public static String exportCache(File directory) {
        SimulationContext context = SimulationContext.get();
        Map<String, String> cache = (Map<String, String>)context.getValue(HASH_CACHE_KEY);
        String jacksonData = null;
        if (cache != null) {
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                objectMapper.writerWithDefaultPrettyPrinter().writeValue(getFormuleCacheFile(directory), cache);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return jacksonData;
    }

    public static void importCache(File directory) {
        SimulationContext context = SimulationContext.get();
        ObjectMapper objectMapper = new ObjectMapper();
        File file = getFormuleCacheFile(directory);
        if (file.isFile()) {
            try {
                Map<String, String> cache = objectMapper.readValue(file, new TypeReference<Map<String, String>>() {});
                context.setValue(HASH_CACHE_KEY, cache);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    protected static String normalizeClassName(String name) {
        StringBuilder result = new StringBuilder(name);
        for (int i=0; i<result.length(); i++) {
            char c = result.charAt(i);
            if (!Character.isJavaIdentifierPart(c)) {
                result.setCharAt(i, '_');
            }
        }
        return result.toString();
    }

    /**
     * Verifie si un script (prescript/equation) est syntaxiquement correct.
     * 
     * @param javaInterface
     * @param script
     * @param out output writer (can be null for non output)
     * @return 0 si ok
     */
    public static int check(Class javaInterface, String script, PrintWriter out) {
        try {
            File src = IsisFileUtil.createTempFile("check" + javaInterface.getSimpleName(), "Equation");
            src.delete(); // delete now, we don't need file, just name to create java file
            String packageName = null;
            String className = normalizeClassName(src.getName());

            src = new File(src.getParentFile(), className + ".java");
            
            // recherche la methode de l'interface
            Method [] methods = javaInterface.getDeclaredMethods();
            Method interfaceMethod = methods[0];

            String content = generateContent(packageName, className, javaInterface, interfaceMethod, script);

            FileUtils.writeStringToFile(src, content, "utf-8");

            int compileResult = CompileHelper.compile(src.getParentFile(), src, src.getParentFile(), out);
            File dest = new File(src.getParentFile(), className + ".class");

            // no more need file .java and .class remove them now
            src.delete();
            dest.delete();

            return compileResult;

        } catch (Exception eee) {
            log.warn("Can't check equation", eee);
            return -10000;
        }
    }

    /**
     * Prend un script d'equation, le convertit en Java, le compile et retour la
     * classe. Si ce script a deja ete compile, retourne la classe existante
     *
     * @param packageName le nom de package de la classe
     * @param className le nom de la classe
     * @param javaInterface l'interface que la classe doit etendre,
     *        cette interface n'a qu'un methode
     * @param script le code de la methode
     * @return la valeur retourné par la methode
     */
    protected static Class compileAndGetClass(String packageName, String className, Class javaInterface, String script) {
        // ajout du nom de l'interface dans le nom de la classe pour retrouver
        // plus facilement a quelle equation correspond les fichiers sur disque.
        className = javaInterface.getSimpleName() + normalizeClassName(className);

        Class clazz;

        // recherche la methode de l'interface
        Method[] methods = javaInterface.getDeclaredMethods();
        Method interfaceMethod = methods[0];
        String classFQN = packageName + "." + className;
        File fileRootSrc = IsisFish.config.getCompileDirectory();
        File fileCheckSum = new File(fileRootSrc, packageName + File.separator + className + ".hashCode");
        File fileSrc = new File(fileRootSrc, packageName + File.separator + className + ".java");

        // get default content
        if (StringUtils.isBlank(script)) {
            try {
                Field defaultContentField = javaInterface.getDeclaredField("DEFAULT_CONTENT");
                script = (String)defaultContentField.get(null);
            } catch (NoSuchFieldException|IllegalArgumentException|IllegalAccessException ex) {
                throw new IsisFishRuntimeException(t("Can't get default content", fileSrc), ex);
            }
        }

        // if equation's Java file exists, check the checksum 
        String oldCheckSum = getHashCache(fileCheckSum);
        String newCheckSum = Integer.toString(script.hashCode());
        boolean checkSumEquals = newCheckSum.equals(oldCheckSum);

        SimulationContext context = SimulationContext.get();
        Map<String, ClassLoader> classLoaderMap = context.getEquationClassLoaders();

        // if Java file's checkSum is not equals to script's checkSum
        // generate new Java file
        if (checkSumEquals) {
            if (classLoaderMap.get(classFQN) == null) {
                ClassLoader cl = IsisFish.config.getScriptClassLoader();
                // first load ? set current class loader
                classLoaderMap.put(classFQN, cl);
            }
        } else {
            String content = generateContent(packageName, className, javaInterface, interfaceMethod, script);
            try {
                // force writing to UTF-8
                // fix compilation issue : unmappable characters
                FileUtils.writeStringToFile(fileSrc, content, StandardCharsets.UTF_8);
                setHashCache(fileCheckSum, Integer.toString(script.hashCode()));
            } catch (IOException zzz) {
                throw new IsisFishRuntimeException(t("isisfish.error.save.script.compilation", fileSrc), zzz);
            }

            compile(fileRootSrc, fileSrc);

            // change class loader if necessary
            ClassLoader cl = IsisFish.config.getScriptClassLoader();
            if (classLoaderMap.get(classFQN) == null) {
                // first load ? set current class loader
                classLoaderMap.put(classFQN, cl);
            } else {
                URL[] cp = IsisFish.config.getScriptDirectoryURLs();
                ForceLoadCurrentClassLoader forceLoadCurrentClassLoader = new ForceLoadCurrentClassLoader(cp, cl);
                forceLoadCurrentClassLoader.forceReloadClass(classFQN);
                classLoaderMap.put(classFQN, forceLoadCurrentClassLoader);
            }
        }

        // try to load class
        ClassLoader cl = classLoaderMap.get(classFQN);
        try {
            clazz = cl.loadClass(classFQN);
        } catch (Exception ex) {

            // force to compile it again; may happen if hashcode
            // is correct, but .class file can't be loaded
            compile(fileRootSrc, fileSrc);

            try {
                // second load
                clazz = cl.loadClass(classFQN);
            } catch (Exception ex2) {
                throw new IsisFishRuntimeException(t("isisfish.error.compile.script", fileSrc), ex2);
            }
        }

        return clazz;
    }

    /**
     * Throw Runtime or Non runtime exception depending on simulation state.
     *
     * @param ex exception to handle
     */
    public static void catchEvaluateException(Exception ex, Log log) {
        if (SimulationContext.get().getSimulationStorage() == null) {
            if (log.isWarnEnabled()) {
                log.warn("Error in equation");
            }
            if (log.isDebugEnabled()) {
                log.debug("StackTrace", ex);
            }
        } else {
            throw new IsisFishRuntimeException("Can't evaluate equation", ex);
        }
    }

    /**
     * Evalue une equation.
     *
     * @param packageName le nom de package de la classe
     * @param className le nom de la classe
     * @param javaInterface l'interface que la classe doit etendre,
     *        cette interface n'a qu'un methode
     * @param script le code de la methode
     * @param args les arguments a utiliser pour l'appel de la methode
     * @return la valeur retourné par la methode
     */
    public static Object evaluate(String packageName, String className,
                                  Class javaInterface, String script, Object... args) {
        Class clazz = compileAndGetClass(packageName, className, javaInterface, script);

        // recherche la methode de l'interface
        Method [] methods = javaInterface.getDeclaredMethods();
        Method interfaceMethod = methods[0];

        Object result = invoke(clazz, interfaceMethod, args);

        return result;
    }

    /**
     * Evalue les necessaryResult d'une equation.
     *
     * @param packageName le nom de package de la classe
     * @param className le nom de la classe
     * @param javaInterface l'interface que la classe doit etendre,
     *        cette interface n'a qu'un methode
     * @param script le code de la methode
     * @return les necessaryResult
     */
    public static String[] evaluateNecessaryResult(String packageName, String className,
                                  Class javaInterface, String script) {

        try {
            Class clazz = compileAndGetClass(packageName, className, javaInterface, script);
            // recherche la methode de l'interface
            Method interfaceMethod = javaInterface.getMethod("getNecessaryResult");
            String[] result = (String[]) invoke(clazz, interfaceMethod);

            return result;
        } catch (NoSuchMethodException eee) {
            log.debug("Can't get necessary result for equation: " + script);
            return NecessaryResult.EMPTY_STRING_ARRAY;
        }
    }

    protected static void compile(File fileRootSrc, File fileSrc) {
        try {
            List<File> classpath = new ArrayList<>();
            classpath.add(fileRootSrc.getAbsoluteFile());
            classpath.add(IsisFish.config.getContextDatabaseDirectory().getAbsoluteFile());
            int compileResult = CompileHelper.compile(classpath, Collections.singletonList(fileSrc), fileRootSrc, null);

            if (compileResult != 0) {
                throw new IsisFishRuntimeException(t("isisfish.error.compile.script", compileResult, fileSrc));
            }
        } catch (Exception zzz) {
            throw new IsisFishRuntimeException(t("isisfish.error.compile.script", fileSrc), zzz);
        }
    }

    /**
     * Generate script content.
     * 
     * Warning, content are always on a unique single line (without \n) for debugging purpose.
     * 
     * @param packageName
     * @param className
     * @param interfaceMethod
     * @param script
     * @return script return (or null)
     */
    protected static String generateContent(String packageName, String className, Class javaInterface, Method interfaceMethod, String script) {

        // extraction du necessary result
        StringBuilder necessaryResultContent = new StringBuilder();
        StringBuilder contentWithoutResult = new StringBuilder();
        boolean writeNecessaryResultContent = NecessaryResult.class.isAssignableFrom(javaInterface);
        handleNecessaryResult(script, necessaryResultContent, contentWithoutResult, writeNecessaryResultContent);

        // extraction des imports
        StringBuilder imports = new StringBuilder();
        StringBuilder contentWithoutImport = new StringBuilder();
        grepImport(contentWithoutResult.toString(), imports, contentWithoutImport);

        String content = "";
        if (packageName != null && !"".equals(packageName)) {
            content += "package " + packageName + ";";
        }

        // add common used imports
        content += "import java.util.*;";
        content += "import java.io.*;";
        content += "import fr.ifremer.isisfish.entities.*;";
        content += "import fr.ifremer.isisfish.types.*;";
        content += "import org.nuiton.math.matrix.*;";
        content += "import org.apache.commons.logging.*;";
        content += "import fr.ifremer.isisfish.simulator.*;";
        content += "import resultinfos.*;";
        content += imports.toString();
        
        // generate content (do not add \n here, this help debug)
        content += "public class " + className + " implements " + interfaceMethod.getDeclaringClass().getName() + " {";
        content += "private static Log log = LogFactory.getLog(" + className + ".class);";
        content += necessaryResultContent.toString();
        content += "public " + interfaceMethod.getReturnType().getName() +  " " + interfaceMethod.getName() + "(";

        Args args = interfaceMethod.getAnnotation(Args.class);
        String[] names = args.value();

        String[] stringTypes = new String[names.length];
        Type[] types = interfaceMethod.getGenericParameterTypes();
        for (int i = 0; i < types.length; i++) {
            stringTypes[i] = types[i].getTypeName();
        }

        for (int i = 0; i < names.length; i++) {
            content += stringTypes[i] + " " + names[i];
            if ( i + 1 < names.length) {
                content += ", ";
            }
        }
        content += ") throws Exception {";
        content += contentWithoutImport.toString();
        content += "\n}}\n";

        return content;
    }

    /**
     * looking for import in code. return all import as found in code in imports args
     * all other code are put in others
     *
     * @param code
     * @param imports
     * @param others
     */
    protected static void grepImport(String code, StringBuilder imports, StringBuilder others) {
        Matcher matches = grepImportPattern.matcher(code);
        int pos = 0;
        while (matches.find()) {
            int begin = matches.start(1);
            int end = matches.end(1);

            others.append(code, pos, begin);
            imports.append(code, begin, end);
            pos = end;

        }

        others.append(code.substring(pos));
    }

    protected static void handleNecessaryResult(String code, StringBuilder necessaryResultContent, StringBuilder equationContent, boolean writeNecessaryResultContent) {
        Pattern p = Pattern.compile("(String\\[\\]\\s+necessaryResult\\s*=.*?;)", Pattern.DOTALL);
        Matcher m = p.matcher(code);
        if (m.find()) {
            if (writeNecessaryResultContent) {
                necessaryResultContent.append("private ").append(m.group(1));
                necessaryResultContent.append("@Override public String[] getNecessaryResult () {return necessaryResult;}");
            }
            equationContent.append(m.replaceFirst(""));
        } else {
            equationContent.append(code);
        }
    }

    protected static Object invoke(Class clazz, Method interfaceMethod, Object... args) {

        Object result;
        try {
            //Method method = clazz.getDeclaredMethod(interfaceMethod.getName(),
            //        interfaceMethod.getParameterTypes());

            Object eq = clazz.newInstance();
            result = interfaceMethod.invoke(eq, args);

        } catch (Exception eee) {
            throw new IsisFishRuntimeException(t("isisfish.error.invoke.method", interfaceMethod, clazz.getName()), eee);
        }
        return result;
    }
}
