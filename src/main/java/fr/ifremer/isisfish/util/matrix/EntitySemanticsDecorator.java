package fr.ifremer.isisfish.util.matrix;

/*
 * #%L
 * ISIS-Fish
 * %%
 * Copyright (C) 2016 - 2017 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.types.Month;
import fr.ifremer.isisfish.types.TimeStep;
import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.SemanticsDecorator;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.Collection;
import java.util.Map;

/**
 * Convertie une entite, month, timestep en string et inversement.
 * Entity = "TopiaId:Entity.toString"
 */
public class EntitySemanticsDecorator implements SemanticsDecorator<Object, String> {

    static private Log log = LogFactory.getLog(EntitySemanticsDecorator.class);

    public interface EntityProvider {

        Object findById(String id);
    }

    public static class EntityTxProvider implements EntityProvider {

        protected TopiaContext tx;

        public EntityTxProvider(TopiaContext tx) {
            this.tx = tx;
        }

        @Override
        public Object findById(String id) {
            Object result = tx.findByTopiaId(id);
            return result;
        }
    }

    public static class EntityMapProvider implements EntityProvider {

        protected Map<String, TopiaEntity> map;

        public EntityMapProvider(Map<String, TopiaEntity> map) {
            this.map = map;
        }

        @Override
        public Object findById(String id) {
            Object result = map.get(id);
            if (result == null) {
                throw new IsisFishRuntimeException("Can't find entity in map: " + id);
            }
            return result;
        }
    }


    private static final String SEP = ":";
    protected EntityProvider provider;
    /** en cle les representation interne (get) en valeur les valeurs decoree (getKey) */
    protected BidiMap<String, Object> cache = new DualHashBidiMap<>();

    public EntitySemanticsDecorator() {
    }

    public EntitySemanticsDecorator(EntityProvider provider) {
        this.provider = provider;
    }

    /**
     * return all currently cached object decorated. Can be used to transforme
     * matrix and work on object found in semantics.
     * If returned collection is cleared, all object in cache are removed too
     * @return
     */
    public Collection<Object> getDecoratedObject() {
        Collection<Object> result = cache.values();
        return result;
    }

    @Override
    public Object decorate(String internalValue) {
        Object result = cache.get(internalValue);
        if (result == null && internalValue != null) {
            // on retrouve souvent les memes semantiques dans les matrices
            // pour minimiser les chaines en memoire, on prend la representation
            // interne avant de l'utiliser comme cle.
            internalValue = internalValue.intern();
            result = internalValue;
            if (StringUtils.startsWith(internalValue, "Month")) {
                String val = StringUtils.substringAfter(internalValue, SEP);
                int monthNumber = Integer.parseInt(val);
                result = Month.MONTH[monthNumber];
            } else if (StringUtils.startsWith(internalValue, "TimeStep")) {
                String val = StringUtils.substringAfter(internalValue, SEP);
                int stepNumber = Integer.parseInt(val);
                result = new TimeStep(stepNumber);
            } else if (StringUtils.startsWith(internalValue, "fr.ifremer.isisfish.entities.")) {
                if (provider == null) {
                    result = StringUtils.substringAfter(internalValue, SEP);
                } else {
                    try {
                        String id = StringUtils.substringBefore(internalValue, SEP);
                        result = provider.findById(id);
                    } catch (Exception eee) {
                        log.info("Fallback use string representation because" + " i can't decorate (String->Entity): " + internalValue, eee);
                        // si on arrive pas a convertir cette fois-ci, on
                        // renvoi internalValue pour que le undecorate est toutes les infos
                        // et donc que la prochaine fois on y arrive peut etre
                    }
                }
            }
            cache.put(internalValue, result);
        }
        return result;
    }

    @Override
    public String undecorate(Object decoratedValue) {
        if (decoratedValue instanceof String) {
            return (String)decoratedValue;
        }
        
        String result = cache.getKey(decoratedValue);
        if (result == null && decoratedValue != null) {
            if (decoratedValue instanceof Month) {
                result = "Month" + SEP + ((Month) decoratedValue).getMonthNumber();
            } else if (decoratedValue instanceof TimeStep) {
                result = "TimeStep" + SEP + ((TimeStep) decoratedValue).getStep();
            } else if (decoratedValue instanceof TopiaEntity) {
                result = ((TopiaEntity) decoratedValue).getTopiaId() + SEP + decoratedValue;
            } else {
                result = String.valueOf(decoratedValue);
            }
            // on retrouve souvent les memes semantiques dans les matrices
            // pour minimiser les chaines en memoire, on prend la representation
            // interne avant de l'utiliser comme cle.
            result = result.intern();
            cache.put(result, decoratedValue);
        }
        return result;
    }

}
