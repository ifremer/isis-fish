/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2022 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.util.matrix;

import fr.ifremer.isisfish.entities.PopulationSeasonInfo;
import fr.ifremer.isisfish.entities.PopulationSeasonInfoDAO;
import fr.ifremer.isisfish.types.Month;
import fr.ifremer.isisfish.types.TimeStep;
import fr.ifremer.isisfish.ui.simulator.SimulatorContext;
import fr.ifremer.isisfish.util.converter.ConverterUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.proxy.HibernateProxy;
import org.hibernate.proxy.LazyInitializer;
import org.nuiton.math.matrix.SemanticMapper;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.persistence.TopiaDAO;
import org.nuiton.topia.persistence.TopiaEntity;

import fr.ifremer.isisfish.IsisFishDAOHelper;
import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.entities.PopulationDAO;
import fr.ifremer.isisfish.entities.PopulationGroup;
import fr.ifremer.isisfish.entities.PopulationGroupDAO;
import fr.ifremer.isisfish.simulator.SimulationContext;
import fr.ifremer.isisfish.ui.input.InputContext;

/**
 * Implementation du mapper de semantique pour Isis pour que l'import/export
 * des matrices en N dimensions de nuiton matrix fonctionne.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class IsisMatrixSemanticMapper extends SemanticMapper {

    /** Logger for this class. */
    private static Log log = LogFactory.getLog(IsisMatrixSemanticMapper.class);

    private static final String SEP = ":";

    /**
     * Return class for type identified by typeName.
     * 
     * For example : "Population" can return "fr.ifremer.entities.Population.class"
     * 
     * Return {@code String} by default.
     * 
     * @param typeName type to get class.
     * @return type for typeId
     */
    public Class getType(String typeName) {

        // In simulation context :
        Class clazz;
        try {
            // try first exact class (need for java.lang.String, java.lang.Integer, ...)
            clazz = Class.forName(typeName);
        } catch (Exception eee) {
            try {
                clazz = Class.forName("fr.ifremer.isisfish.entities." + typeName);
            } catch (Exception eee1) {
                try {
                    clazz = Class.forName("fr.ifremer.isisfish.types." + typeName);
                } catch (Exception eee2) {
                    log.warn("Can't find class for name " + typeName, eee2);
                    clazz = String.class;
                }
            }
        }
        return clazz;
    }

    /**
     * Return value identified by valueId and type {@code type}.
     *
     * Return {@code valueId} by default;
     * 
     * @param type
     * @param valueId
     * @return value identified by {valueId}
     */
    public Object getValue(Class type, String valueId) {

        // In simulation context :
        Object value;
        try {

            TopiaContext context = SimulationContext.get().getDB();
            // si on est pas en simulation, on est peut-etre en saisie,
            // on utilise la base de la saisie (Evolution #8971)
            if (context == null) {
                context = InputContext.getDb();

                if (context == null) {
                    context = SimulatorContext.getDb();
                }
            }

            if (Month.class.isAssignableFrom(type)) {
                int monthNumber = Integer.parseInt(valueId);
                value = Month.MONTH[monthNumber];
            } else if (TimeStep.class.isAssignableFrom(type)) {
                int stepNumber = Integer.parseInt(valueId);
                value = new TimeStep(stepNumber);
            }

            // les populations groupes sont speciaux car ils n'ont pas de
            // noms. Dans l'import, ils sont composés du nom de la population
            // et du numero de groupe séparé par +.
            else if (PopulationGroup.class.isAssignableFrom(type)) {
                int separatorIndex = valueId.indexOf('+');
                if (separatorIndex >= 0) {
                    String popName = valueId.substring(0, separatorIndex);
                    int groupId = Integer.parseInt(valueId.substring(separatorIndex + 1));

                    PopulationDAO populationDao = IsisFishDAOHelper.getPopulationDAO(context);
                    PopulationGroupDAO populationGroupDAO = IsisFishDAOHelper.getPopulationGroupDAO(context);
                    Population population = populationDao.findByName(popName);
                    value = populationGroupDAO.findByProperties(
                            PopulationGroup.PROPERTY_POPULATION, population,
                            PopulationGroup.PROPERTY_ID, groupId);
                } else {
                    if (log.isWarnEnabled()) {
                        log.warn("PopulationGroup identifiant doesn't contains '+' separator");
                    }
                    value = valueId;
                }
            }
            // les saisons aussi sont spéciales (pas de noms)
            // on adopte une syntaxe nom dde population+mois debut+mois fin
            else if (PopulationSeasonInfo.class.isAssignableFrom(type)) {
                String[] componentParts = StringUtils.split(valueId, "+");
                if (componentParts.length == 3) {
                    String popName = componentParts[0].trim();
                    Month firstMonth = new Month(Integer.parseInt(componentParts[1]));
                    Month lastMonth = new Month(Integer.parseInt(componentParts[2]));

                    PopulationDAO populationDao = IsisFishDAOHelper.getPopulationDAO(context);
                    PopulationSeasonInfoDAO populationSeasonInfoDAO = IsisFishDAOHelper.getPopulationSeasonInfoDAO(context);
                    Population population = populationDao.findByName(popName);
                    value = populationSeasonInfoDAO.findByProperties(
                            PopulationSeasonInfo.PROPERTY_POPULATION, population,
                            PopulationSeasonInfo.PROPERTY_FIRST_MONTH, firstMonth,
                            PopulationSeasonInfo.PROPERTY_LAST_MONTH, lastMonth
                    );
                } else {
                    if (log.isWarnEnabled()) {
                        log.warn("PopulationSeasonInfo identifiant doesn't contains two '+' separator");
                    }
                    value = valueId;
                }
            }
            else if (TopiaEntity.class.isAssignableFrom(type)) {
                TopiaDAO dao = IsisFishDAOHelper.getDAO(context, type);
                value = dao.findByProperty("name", valueId);
            } else {
                value = ConverterUtil.getConverter(null).convert(valueId, type);
            }
        } catch (Exception ex) {
            log.warn("Can't get value for " + valueId, ex);
            value = valueId;
        }
        return value;
    }

    @Override
    public String getTypeName(Object type) {
        String result;

        if (type instanceof TopiaEntity) {
            Class typeClass = type.getClass();
            if (type instanceof HibernateProxy) {
                LazyInitializer lazyInitializer = ((HibernateProxy) type).getHibernateLazyInitializer();
                typeClass = lazyInitializer.getPersistentClass();
            }
            result = typeClass.getSimpleName().replace("Impl", "");
        } else if (type.getClass().getPackage().getName().startsWith("fr.ifremer.isisfish.types.")) {
            result = type.getClass().getSimpleName();
        } else {
            result = super.getTypeName(type);
        }

        return result;
    }

    @Override
    public String getValueId(Object value) {
        String result;

        if (value instanceof Month) {
            result = String.valueOf(((Month) value).getMonthNumber());
        } else if (value instanceof TimeStep) {
            result = String.valueOf(((TimeStep) value).getStep());
        } else if (value instanceof PopulationGroup) {
            PopulationGroup populationGroup = (PopulationGroup) value;
            result = populationGroup.getPopulation().getName() + "+" + populationGroup.getId();
        } else if (value instanceof TopiaEntity) {
            result = value.toString(); // name
        } else {
            result = super.getValueId(value);
        }

        return result;
    }
}
