package fr.ifremer.isisfish.util.matrix;

/*
 * #%L
 * IsisFish
 * %%
 * Copyright (C) 1999 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import java.io.BufferedReader;
import java.io.IOException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Very simple parse write for efficiency. Input stream must be correct no check
 * is done on data.
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class SimpleParser {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(SimpleParser.class);

    // MARK_SIZE = 1 doesn't work when EOF is reached
    static final private int MARK_SIZE = 2;

    protected boolean intern;
    protected BufferedReader in;
    protected StringBuilder sb = new StringBuilder();

    protected boolean eof = false;
    protected boolean eol = false;
    /**
     *
     * @param in BufferedReader because Reader must support mark and reset
     */
    public SimpleParser(BufferedReader in, boolean intern) {
        this.in = in;
        this.intern = intern;
    }

    /**
     *
     * @return true if end of file
     */
    public boolean isEOF() {
        return eof;
    }

    /**
     *
     * @return true if end of line
     */
    public boolean isEOL() {
        return eol;
    }

    protected void read(char sep) throws IOException {
        eol = false;
        sb.setLength(0);

        int i;
        while ((i = in.read()) != -1) {
            char c = (char)i;
            if (c == '\r') {
                // try to eat \n after \r
                in.mark(MARK_SIZE);
                c = (char)in.read();
                if (c != '\n') {
                    in.reset();
                }
                eol = true;
                break;
            } else if (c == '\n') {
                eol = true;
                break;
            } else if (c == sep) {
                break;
            } else {
                sb.append(c);
            }
        }

        if (i != -1) {
            // test if not eof
            in.mark(MARK_SIZE);
            i = in.read();
            in.reset();
        }
        eof = i == -1;
    }
    
    public String readString(char sep) throws IOException {
        read(sep);
        String result = sb.toString();
        if (intern) {
            result = result.intern();
        }
        return result;
    }

    public int readInt(char sep) throws IOException {
        read(sep);
        int result = parseInt(sb);
        return result;
    }

    public double readDouble(char sep) throws IOException {
        read(sep);
        double result = parseDouble(sb);
        return result;
    }

    protected double parseDouble(StringBuilder s) throws IOException {
        return Double.parseDouble(s.toString());
    }

    /**
     * custom parseInt: 4 fois plus performant que Integer.parseInt
     * ne supporte que les chiffres positif, et la chaine doit forcement etre un nombre valide
     * @param sb la chaine representant l'entier
     * @return
     */
    protected int parseInt(StringBuilder sb) {
        int result = 0;
        for (int i=0, maxi=sb.length(); i<maxi; i++) {
            char c = sb.charAt(i);
            result = result * 10 + c - '0';
        }
        return result;
    }


}
