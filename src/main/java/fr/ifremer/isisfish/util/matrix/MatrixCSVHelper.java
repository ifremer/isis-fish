package fr.ifremer.isisfish.util.matrix;

/*
 * #%L
 * ISIS-Fish
 * %%
 * Copyright (C) 2016 - 2017 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixFactory;
import org.nuiton.math.matrix.MatrixIterator;
import org.nuiton.math.matrix.MatrixND;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe permettant de convertir une matrice en String et inversement en 
 * prenant en compte la conversion des semantiques via l'utilisation 
 * d'un decorator ({@link EntitySemanticsDecorator}).
 *
 * Ce format est utilise comme format de sauvegarde des resultats dans les
 * simulation. Il remplace le stockage en base et les fichiers mapper.
 *
 * Format d'une matrice:
 * <pre>
 * # commentaire
 * [nom]
 * [nom dimension1]:[semantique1];[semantique2];...
 * [nom dimension2]:[semantique1];[semantique2];...
 * ...
 * [ligne blanche]
 * [coordonnee1];[coordonnee2];...;[valeur]
 * [coordonnee1];[coordonnee2];...;[valeur]
 * [coordonnee1];[coordonnee2];...;[valeur]
 * ...
 * </pre>
 *
 * Exemple
 * <pre>
 * MaMatrice
 * Mois:Janvier;Fevrier;Mars
 * Ville:Nantes;Paris;Nice
 *
 * 1;1;2;13.5
 * 0;2;1;4.2
 * </pre>
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 *
 * @since 4.4.1.0
 */
public class MatrixCSVHelper {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(MatrixCSVHelper.class);

    protected EntitySemanticsDecorator decorator;

    public MatrixCSVHelper() {
        this(new EntitySemanticsDecorator());
    }

    public MatrixCSVHelper(EntitySemanticsDecorator decorator) {
        this.decorator = decorator;
    }

    protected MatrixFactory getMatrixFactory() {
        return MatrixFactory.getInstance();
    }

    /**
     * return new list with undecorate item
     * @param l
     * @return new list
     */
    protected List<String> undecorate(List l) {
        List<String> result = new ArrayList<>(l.size());
        for (Object o : l) {
            result.add(undecorate(o));
        }
        return result;
    }

    protected String undecorate(Object o) {
        return decorator.undecorate(o);
    }

    public MatrixND readMatrix(String mat) throws IOException {
        try (Reader in = new StringReader(mat)) {
            MatrixND result = this.readMatrix(in);
            return result;
        }
    }

    public String writeMatrix(String name, MatrixND mat) throws IOException {
        try (StringWriter out = new StringWriter()) {
            writeMatrix(out, name, mat);
            String result = out.toString();
            return result;
        }
    }

    public MatrixND readMatrix(Reader reader) throws IOException {
        LineNumberReader in = new LineNumberReader(reader);

        SimpleParser sp = new SimpleParser(in, true);

        // lecture du nom de la matrice
        String name = sp.readString('\n');


        // lecture du nom des dimensions et des semantics
        List<String> dimNames = new ArrayList<>();
        List<List<String>> semantics = new ArrayList<>();

        String dimName = sp.readString(':');
        while (!sp.isEOL() || StringUtils.isNotBlank(dimName)) {
            dimNames.add(dimName);
            List<String> sems = new ArrayList<>();
            semantics.add(sems);
            while (!sp.isEOL()) {
                String sem = sp.readString(';');
                sems.add(sem);
            }
            dimName = sp.readString(':');
        }

        // creation de la matrice resultat avec les infos collectes
        MatrixND result = getMatrixFactory().create(name,
                semantics.toArray(new List[0]),
                dimNames.toArray(new String[0]));


        // lecture des data
        int nbDim = dimNames.size();
        int[] coord = new int[nbDim];

        while (!sp.isEOF()) {
            for (int i=0; i<nbDim; i++) {
                coord[i] = sp.readInt(';');
            }
            double v = sp.readDouble(';');
            result.setValue(coord, v);
        }

        return result;
    }

    public void writeMatrix(Writer writer, String name, MatrixND mat) {
        PrintWriter out = new PrintWriter(new BufferedWriter(writer));

        out.print(name);
        out.println();

        List[] sems = mat.getSemantics();
        for (int i=0, maxi=sems.length; i<maxi; i++) {
            String dimName = mat.getDimensionName(i);
            out.print(dimName);

            List l = undecorate(sems[i]);
            char sep = ':';
            for (Object o : l) {
                out.print(sep);
                out.print(o);
                sep = ';';
            }
            out.println();
        }

        // blank line to separate header and data
        out.println();

        for (MatrixIterator i = mat.iteratorNotZero(); i.hasNext();) {
            i.next();
            int[] pos = i.getCoordinates();
            double value = i.getValue();
            for (int p : pos) {
                out.print(p);
                out.print(';');
            }
            out.print(value);
            out.println();
        }

        out.flush();
    }

}
