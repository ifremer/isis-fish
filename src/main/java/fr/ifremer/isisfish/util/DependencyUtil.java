/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 - 2020 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.objectweb.asm.AnnotationVisitor;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.TypePath;

import fr.ifremer.isisfish.datastore.ExportStorage;
import fr.ifremer.isisfish.datastore.FormuleStorage;
import fr.ifremer.isisfish.datastore.ObjectiveStorage;
import fr.ifremer.isisfish.datastore.OptimizationStorage;
import fr.ifremer.isisfish.datastore.ResultInfoStorage;
import fr.ifremer.isisfish.datastore.RuleStorage;
import fr.ifremer.isisfish.datastore.ScriptStorage;
import fr.ifremer.isisfish.datastore.SensitivityAnalysisStorage;
import fr.ifremer.isisfish.datastore.SensitivityExportStorage;
import fr.ifremer.isisfish.datastore.SimulationPlanStorage;
import fr.ifremer.isisfish.datastore.SimulatorStorage;

/**
 * Class utilitaire chargées d'inspecter le bytecode de certaines classe des scripts utilisateur
 * d'IsisFish pour en extraire des informations.
 */
public class DependencyUtil {

    private static final Log log = LogFactory.getLog(DependencyUtil.class);

    /**
     * Extrait les dépendences d'une class compilé à partir de son bytecode en ne conservant que
     * les dependances des scripts utilisateurs.
     * 
     * @param rootDirectory 
     * @param files ensemble de fichier à analyzer (fichiers .class)
     * @return extract class set
     */
    public static Set<String> extractDependencies(File rootDirectory, Collection<File> files) {
        Set<String> deps = new HashSet<>();
        Set<File> alreadyDone = new HashSet<>();
        recursiveExtractDependencies(rootDirectory, files, alreadyDone, deps);
        return deps;
    }

    protected static void recursiveExtractDependencies(File rootDirectory, Collection<File> todoClassFiles, Set<File> alreadyDone, Set<String> deps) {

        // extract deps
        for (File file : todoClassFiles) {
            alreadyDone.add(file);
            try (InputStream is = new BufferedInputStream(new FileInputStream(file))) {
                Set<String> fileDeps = extractDependencies(is);
                deps.addAll(fileDeps);
            } catch (IOException ex) {
                if (log.isErrorEnabled()) {
                    log.error("Can't extract dependencies", ex);
                }
            }
            
        }
        
        // convert deps to class filTopiaFlywayServicees
        Collection<File> newClassFiles = new ArrayList<>();
        for (String dep : deps) {
            String classRelative = dep.replace('.', File.separatorChar) + ".class";
            File classFile = new File(rootDirectory, classRelative);
            if (!alreadyDone.contains(classFile)) {
                newClassFiles.add(classFile);
            }
        }

        // make new recursion if necessary
        if (!newClassFiles.isEmpty()) {
            recursiveExtractDependencies(rootDirectory, newClassFiles, alreadyDone, deps);
        }
    }

    /**
     * Extrait les dépendences d'une class compilé à partir de son bytecode.
     * 
     * @param clazzStream la classe à analyser
     * @return extract class set
     */
    protected static Set<String> extractDependencies(InputStream clazzStream) {
        final Set<String> result = new HashSet<>();
        
        // method body visitor
        final MethodVisitor mv = new MethodVisitor(Opcodes.ASM9) {

            @Override
            public AnnotationVisitor visitAnnotation(String desc, boolean visible) {
                registerDescriptor(result, desc);
                return super.visitAnnotation(desc, visible);
            }

            @Override
            public AnnotationVisitor visitTypeAnnotation(int typeRef, TypePath typePath, String desc, boolean visible) {
                registerDescriptor(result, desc);
                return super.visitTypeAnnotation(typeRef, typePath, desc, visible);
            }

            @Override
            public AnnotationVisitor visitParameterAnnotation(int parameter, String desc, boolean visible) {
                registerDescriptor(result, desc);
                return super.visitParameterAnnotation(parameter, desc, visible);
            }

            @Override
            public void visitTypeInsn(int opcode, String type) {
                registerType(result, type);
                super.visitTypeInsn(opcode, type);
            }

            @Override
            public void visitFieldInsn(int opcode, String owner, String name, String desc) {
                registerType(result, owner);
                registerDescriptor(result, desc);
                super.visitFieldInsn(opcode, owner, name, desc);
            }

            @Override
            public void visitMethodInsn(int opcode, String owner, String name, String desc, boolean itf) {
                registerType(result, owner);
                registerDescriptor(result, desc);
                super.visitMethodInsn(opcode, owner, name, desc, itf);
            }

            @Override
            public void visitLdcInsn(Object cst) {
                if (cst instanceof Type) {
                    registerType(result, ((Type)cst).getClassName());
                }
                super.visitLdcInsn(cst);
            }

            @Override
            public void visitTryCatchBlock(Label start, Label end, Label handler, String type) {
                registerType(result, type);
                super.visitTryCatchBlock(start, end, handler, type);
            }

            @Override
            public void visitLocalVariable(String name, String desc, String signature, Label start, Label end, int index) {
                registerDescriptor(result, desc);
                super.visitLocalVariable(name, desc, signature, start, end, index);
            }
        };

        // class visitor
        ClassVisitor visitor = new ClassVisitor(Opcodes.ASM9) {

            @Override
            public void visit(int version, int access, String name,
                    String signature, String superName, String[] interfaces) {
                registerType(result, superName);
                super.visit(version, access, name, signature, superName, interfaces);
            }

            @Override
            public AnnotationVisitor visitAnnotation(String desc,
                    boolean visible) {
                registerDescriptor(result, desc);
                return super.visitAnnotation(desc, visible);
            }

            @Override
            public FieldVisitor visitField(int access, String name,
                    String desc, String signature, Object value) {
                registerDescriptor(result, desc);
                return super.visitField(access, name, desc, signature, value);
            }

            @Override
            public MethodVisitor visitMethod(int access, String name,
                    String desc, String signature, String[] exceptions) {
                registerDescriptor(result, desc);
                return mv;
            }
        };

        // parse input class
        try {
            ClassReader classReader = new ClassReader(clazzStream);
            classReader.accept(visitor, 0);
        } catch (IOException ex) {
            if (log.isErrorEnabled()) {
                log.error("Can't extract dependencies", ex);
            }
        }

        return result;
    }

    /**
     * Register type if necessary (depends on another user script).
     * 
     * @param types types collection
     * @param type type to add
     */
    protected static void registerType(Set<String> types, String type) {

        //System.out.println("==> " + type);
        if (StringUtils.startsWithAny(type,
                ExportStorage.EXPORT_PATH,
                FormuleStorage.FORMULE_PATH,
                ObjectiveStorage.OBJECTIVE_PATH,
                OptimizationStorage.OPTIMIZATION_PATH,
                RuleStorage.RULE_PATH,
                ResultInfoStorage.RESULT_INFO_PATH,
                ScriptStorage.SCRIPT_PATH,
                SensitivityAnalysisStorage.SENSITIVITY_ANALYSIS_PATH,
                SensitivityExportStorage.SENSITIVITY_EXPORT_PATH,
                SimulationPlanStorage.SIMULATION_PLAN_PATH,
                SimulatorStorage.SIMULATOR_PATH)) {

            // exclude inner types
            if (!type.contains("$")) {
                String dotType = type.replace('/', '.');
                types.add(dotType);
            }
        }
    }

    /**
     * Clean type to extract valid class name.
     * 
     * Ex:
     * <ul>
     *   <li>Lfr/ifremer/isisfish/types/Month;</li>
     *   <li>[Ljava/lang/String;</li>
     *   <li>(Ljava/lang/Class;)Lorg/apache/commons/logging/Log;</li>
     * </ul>
     * 
     * @param types types collection
     * @param descriptor descriptor
     */
    protected static void registerDescriptor(Set<String> types, String descriptor) {
        if (descriptor.startsWith("(")) {
            int endIndex = descriptor.indexOf(')');
            registerDescriptor(types, descriptor.substring(1, endIndex));
            registerDescriptor(types, descriptor.substring(endIndex + 1));
        } else {
            // full pattern : \\[*L([^;]+);|\\[[ZBCSIFDJ]|[ZBCSIFDJ]
            // on exclut la fin (les types primitifs) pour ne conserver que la partie interessante
            // du pattern qui nous concerne
            Pattern pattern = Pattern.compile("\\[*L([^;]+);");
            Matcher matcher = pattern.matcher(descriptor);
            while (matcher.find()) {
                String match = matcher.group(1);
                registerType(types, match);
            }
        }
    }
}
