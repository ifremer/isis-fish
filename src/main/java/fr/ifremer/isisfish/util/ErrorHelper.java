/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2020 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.util;

import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.IsisFishRuntimeException;
import org.apache.commons.lang3.StringUtils;
import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.error.ErrorInfo;
import org.jdesktop.swingx.error.ErrorReporter;

import java.awt.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import static org.nuiton.i18n.I18n.t;

/**
 * Error helper.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class ErrorHelper implements ErrorReporter {

    /**
     * Display a user friendly error frame.
     * 
     * @param parent parent component
     * @param message message for user
     * @param cause exception cause
     */
    public static void showErrorDialog(Component parent, String message, Throwable cause) {
        JXErrorPane pane = new JXErrorPane();

        String details = "<pre>\n" + getDetails(cause) + "\n</pre>";

        ErrorInfo info = new ErrorInfo(t("isisfish.error.errorpane.title"),
                t("isisfish.error.errorpane.htmlmessage", message), details, null,
                cause, null, null);
        pane.setErrorInfo(info);
        if (cause != null) {
            pane.setErrorReporter(new ErrorHelper());
        }
        JXErrorPane.showDialog(parent, pane);
    }

    /**
     * Display a user friendly error frame.
     * 
     * @param message message for user
     */
    public static void showErrorDialog(String message) {
        showErrorDialog(message, null);
    }

    /**
     * Display a user friendly error frame.
     * 
     * @param message message for user
     * @param cause exception cause
     */
    public static void showErrorDialog(String message, Throwable cause) {
        showErrorDialog(null, message, cause);
    }

    private static String getDetails(Throwable cause) {
        String result = "";
        if (cause != null) {
            StringWriter out = new StringWriter();
            PrintWriter writer = new PrintWriter(out);
            cause.printStackTrace(writer);
            out.flush();
            result = out.toString();
        }
        return result;
    }

    @Override
    public void reportError(ErrorInfo errorInfo) throws NullPointerException {

        try {
            // ruby max url length = 2083
            // https://forge.codelutin.com/projects/isis-fish/issues/new?issue[description]= is 78 chars
            // let's truncate to max 1900 char for stack trace
            Throwable rootCause = errorInfo.getErrorException();
            String details = null;
            do {
                if (rootCause == null && details == null) {
                    details = "[Please, copy/paste JAVA stacktrace here]";
                    details = URLEncoder.encode(details, StandardCharsets.UTF_8.name());
                } else if (rootCause == null) {
                    details = StringUtils.truncate(details, 1900); // max size
                } else {
                    details = "[...]\n" + getDetails(rootCause);
                    details = URLEncoder.encode(details, StandardCharsets.UTF_8.name());
                    rootCause = rootCause.getCause();
                }
            } while (details.length() > 1900);

            // open browser
            String urlString = IsisFish.config.getBugReportUrl() + "?issue[description]=" + details;
            URI uri = new URI(urlString);
            Desktop.getDesktop().browse(uri);
        } catch (URISyntaxException | IOException e) {
            throw new IsisFishRuntimeException("Can't open desktop", e);
        }

    }
}
