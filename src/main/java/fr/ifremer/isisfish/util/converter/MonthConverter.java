/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2010 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.util.converter;

import org.apache.commons.beanutils.ConversionException;
import org.apache.commons.beanutils.Converter;

import fr.ifremer.isisfish.types.Month;

/**
 * Classe utilise pour convertir une chaine en un objet Month.
 * 
 * Utilisé pour la conversion et le stockage en propriete des parametres
 * 
 * @author poussin
 */
public class MonthConverter implements Converter {

    @Override
    public Object convert(Class type, Object value) {
        Object result;
        if (value instanceof Month) {
            result = value;
        } else if (value instanceof String) {
            int m = Integer.parseInt(value.toString());
            result = Month.MONTH[m];
        } else if (value instanceof Integer) {
            int m = (Integer) value;
            result = Month.MONTH[m];
        } else {
            throw new ConversionException("Can't convert '" + value + "' to " + type.getName());
        }
        return result;
    }

}


