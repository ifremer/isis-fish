/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2013 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.util.converter;

import org.apache.commons.beanutils.ConversionException;
import org.apache.commons.beanutils.ConvertUtilsBean;
import org.apache.commons.beanutils.Converter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Classe utilise pour convertir une collection.
 *
 * Chaque element est séparé par ",".
 * 
 * Pour la convertion String > collection, il reste un problème à régler, on ne peut pas connaitre au runtime
 * le typage de la collection (Collection&lt;Zone&gt;) par exemple.
 * Autre problème, le converter prend en paramêtre seulement une Class et non un type qui pourrait avoir une annotation
 * pouvant préciser son type par exemple.
 * 
 * @author poussin
 */
public class CollectionConverter implements Converter {

    /** Logger for this class. */
    private static final Log log = LogFactory.getLog(CollectionConverter.class);

    protected ConvertUtilsBean defaultConverter;

    public CollectionConverter(ConvertUtilsBean defaultConverter) {
        this.defaultConverter = defaultConverter;
    }

    @Override
    public Object convert(Class type, Object value) {
        Object result;
        if (value == null) {
            result = null;
        } else if (value instanceof Collection) {
            result = value;
        } else if (value instanceof String) {
            Stream<TopiaEntity> defaultStream = Stream.of(((String) value).split("\\s*,\\s*"))
                    .map(s -> (TopiaEntity)defaultConverter.convert(s, TopiaEntity.class));
            if (type.isAssignableFrom(Set.class)) {
                result = defaultStream.collect(Collectors.toSet());
            } else {
                result = defaultStream.collect(Collectors.toList());
            }
        } else {
            throw new ConversionException("Can't convert '" + value + "' to " + type.getName());
        }
        return result;
    }

    public Object getCollectionAsString(Collection<?> coll) {
        String result = null;
        if (coll != null) {
            result = coll.stream()
                    .map(defaultConverter::convert)
                    .collect(Collectors.joining(","));
        }
        return result;
    }
}
