/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2018 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.util.converter;

import fr.ifremer.isisfish.util.converter.MatrixConverter;
import org.apache.commons.beanutils.ConvertUtilsBean;
import org.apache.commons.beanutils.Converter;
import org.apache.commons.text.StringEscapeUtils;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.topia.persistence.TopiaEntity;

import fr.ifremer.isisfish.types.TimeStep;
import fr.ifremer.isisfish.types.Month;
import fr.ifremer.isisfish.types.TimeUnit;

import java.util.Collection;

/**
 * Classe utilisée pour convertir en chaine un objet Date, Month ou TopiaEntity.
 * Utilisé pour la conversion et le stockage en propriete des parametres.
 * 
 * Created: 12 janv. 2006 20:38:26
 * 
 * @author poussin
 * 
 * @version $Revision$
 * 
 * Last update: $Date$
 * by : $Author$
 */
public class StringConverter implements Converter {

    protected Converter defaultStringConverter = new org.apache.commons.beanutils.converters.StringConverter();

    protected ConvertUtilsBean simpleElementConverter;

    public StringConverter(ConvertUtilsBean simpleElementConverter) {
        this.simpleElementConverter = simpleElementConverter;
    }

    @Override
    public Object convert(Class type, Object o) {
        Object result = null;
        if (o != null) {
            if (o instanceof TopiaEntity) {
                // put after ':' string entity representation
                result = ((TopiaEntity) o).getTopiaId() + ":" + o.toString();
            } else if (o instanceof TimeStep) {
                result = String.valueOf(((TimeStep) o).getStep());
            } else if (o instanceof Month) {
                result = String.valueOf(((Month) o).getMonthNumber());
            } else if (o instanceof TimeUnit) {
                result = String.valueOf(((TimeUnit) o).getTime());
            // Not used since mexico file format use
            // Still used in sensivity analysis prescript
            } else if (o instanceof MatrixND) {
                MatrixConverter converter = new MatrixConverter();
                result = converter.getMatrixAsString((MatrixND) o);
                result = StringEscapeUtils.escapeJava((String) result);
            } else if (o instanceof Collection) {
                Collection<?> coll = (Collection<?>)o;
                CollectionConverter converter = new CollectionConverter(simpleElementConverter);
                result = converter.getCollectionAsString(coll);
            } else {
                // dans tous les autres cas, on appelle le converter par defaut
                result = defaultStringConverter.convert(type, o);
            }
        }
        return result;
    }

}
