/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2013 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.util.converter;

import java.util.List;

import org.apache.commons.beanutils.ConversionException;
import org.apache.commons.beanutils.Converter;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaId;

/**
 * Classe utilise pour convertir une chaine en un objet TopiaEntity.
 * 
 * Utilisé pour la conversion et le stockage en propriete des parametres
 * 
 * @author poussin
 */
public class TopiaEntityConverter implements Converter {

    /** Logger for this class. */
    private static final Log log = LogFactory.getLog(TopiaEntityConverter.class);

    protected TopiaContext context = null;
    protected SharedSessionContractImplementor session = null;

    public TopiaEntityConverter(TopiaContext context) {
        this.context = context;
    }

    public TopiaEntityConverter(SharedSessionContractImplementor session) {
        this.session = session;
    }

    @Override
    public Object convert(Class type, Object value) {
        Object result;
        if (value == null) {
            result = null;
        } else if (value instanceof TopiaEntity) {
            result = value;
        } else if (value instanceof String) {
            // after ':' is entity string representation
            value = StringUtils.substringBefore((String)value, ":");
            result = getTopiaEntity((String) value);
        } else {
            throw new ConversionException("Can't convert '" + value + "' to " + type.getName());
        }
        return result;
    }

    private TopiaEntity getTopiaEntity(String topiaId) {
        TopiaEntity result = null;
        if (context != null) {
            try {
                TopiaContext tx = context.beginTransaction();
                result = tx.findByTopiaId(topiaId);
                // FIXME when after tx.closeContext we can continu to load
                // object, call it here
                tx.commitTransaction();

                // XXX : ne pas clore ce context sinon les semantiques ne se rechargent pas toutes
                // par exemple, pour un chargement de liste, il change la premiere avec le context
                // et il tente les appels suivants avec session, mais je ne comprends pas pourquoi
                // TODO : ajouter une doc sur le pourquoi de la nécéssité de clore le context
                tx.closeContext();
            } catch (TopiaException eee) {
                if (log.isWarnEnabled()) {
                    log.warn("Can't find Entity from TopiaId " + topiaId, eee);
                }
            }
        } else if (session != null) {
            String entityClass = TopiaId.getClassNameAsString(topiaId);

            try {
                /* Non compilable code since hibernate 4.2, but not a real, not usefull
                boolean mustCommit = false;
                if (!session.getJDBCContext().getTransaction().isActive()) {
                    mustCommit = true;
                    session.getJDBCContext().getTransaction().begin();
                }*/
                String hql = "from " + entityClass + " where topiaId=:id";
                List results = session.createQuery(hql)
                        .setParameter("id", topiaId)
                        .list();

                /*if (mustCommit) {
                    session.getJDBCContext().getTransaction().commit();
                }*/
                result = (TopiaEntity) results.get(0);
            } catch (HibernateException eee) {
                if (log.isDebugEnabled()) {
                    log.debug("Can't find TopiaEntity: " + topiaId, eee);
                }
                throw eee;
            }
        }
        return result;
    }

}
