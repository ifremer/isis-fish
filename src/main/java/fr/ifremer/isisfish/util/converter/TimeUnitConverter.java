/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2010 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.util.converter;

import org.apache.commons.beanutils.ConversionException;
import org.apache.commons.beanutils.Converter;

import fr.ifremer.isisfish.types.TimeUnit;


/**
 * Classe utilise pour convertir une chaine en un objet TimeUnit.
 * 
 * Utilisé pour la conversion et le stockage en propriete des parametres
 * 
 * @author poussin
 */
public class TimeUnitConverter implements Converter {

    @Override
    public Object convert(Class type, Object value) {
        Object result;
        if (value instanceof TimeUnit) {
            result = value;
        } else if (value instanceof Number) {
            result = new TimeUnit(((Number)value).doubleValue());
        } else if (value instanceof String) {
            double d = Double.parseDouble(value.toString());
            result = new TimeUnit(d);
        } else {
            throw new ConversionException("Can't convert '" + value + "' to " + type.getName());
        }
        return result;
    }

}


