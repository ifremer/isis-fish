/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2012 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.util.converter;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.ConvertUtilsBean;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.math.matrix.MatrixNDImpl;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.persistence.TopiaEntity;

import fr.ifremer.isisfish.types.Month;
import fr.ifremer.isisfish.types.RangeOfValues;
import fr.ifremer.isisfish.types.TimeStep;
import fr.ifremer.isisfish.types.TimeUnit;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * ConverterUtil.
 * 
 * Created: 25 sept. 06 19:37:16
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class ConverterUtil {

    //static protected Map<TopiaContext, ConvertUtilsBean> cache = new WeakHashMap<TopiaContext, ConvertUtilsBean>();
    static protected ConvertUtilsBean nullConverter = getConverterNoCache(null);
    
    public static ConvertUtilsBean getConverter(TopiaContext context) {
        ConvertUtilsBean result;
        if (context == null) {
            result = nullConverter;
        } else {
            /*result = cache.get(context);
            if (result == null) {*/
                result = getConverterNoCache(context);
            //}   
        }
        return result;
    }

    protected static ConvertUtilsBean getConverterNoCache(TopiaContext context) {
        BeanUtilsBean bub = BeanUtilsBean.getInstance();
        ConvertUtilsBean result = bub.getConvertUtils();
        //cache.put(context, result);

        // mise en place de converter de string vers des objet ...
        result.register(new TopiaEntityConverter(context), TopiaEntity.class);
        result.register(new TimeStepConverter(), TimeStep.class);
        result.register(new MonthConverter(), Month.class);
        result.register(new TimeUnitConverter(), TimeUnit.class);
        result.register(new RangeOfValuesConverter(), RangeOfValues.class);

        // collections
        CollectionConverter collectionConverter = new CollectionConverter(result);
        result.register(collectionConverter, Collection.class);
        result.register(collectionConverter, List.class);
        result.register(collectionConverter, Set.class);

        // Not used since mexico file format use
        // Still used in sensivity analysis prescript
        MatrixConverter matrixConverter = new MatrixConverter();
        result.register(matrixConverter, MatrixND.class);
        // dans les prescripts generé on a MatrixNDImpl.class
        // c'est tres étrange, mais ca ne fonctionne pas par MatrixND.class
        result.register(matrixConverter, MatrixNDImpl.class);

        // ... et inversement
        result.register(new StringConverter(result), String.class);
        return result;
    }
}
