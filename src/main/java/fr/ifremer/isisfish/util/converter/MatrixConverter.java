/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2018 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.util.converter;

import org.apache.commons.beanutils.ConversionException;
import org.apache.commons.beanutils.Converter;
import org.apache.commons.text.StringEscapeUtils;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.math.matrix.MatrixStringEncoder;
import org.nuiton.topia.persistence.TopiaEntity;

/**
 * Classe utilisées pour convertir des matrices en string et vice-versa.
 * 
 * Since 3.3.0.0, this converter is not used in mexico export, but still
 * in prescript (sensivity analysis).
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update: $Date$
 * by : $Author$
 */
public class MatrixConverter extends MatrixStringEncoder implements Converter {

    @Override
    public Object convert(Class type, Object value) {
        Object result;
        if (value instanceof MatrixND) {
            result = value;
        } else if (value instanceof String) {
            String sValue = (String)value;
            sValue = StringEscapeUtils.unescapeJava(sValue);
            result = getMatrixFromString(sValue);
        } else if (value == null) {
            result = null;
        } else {
            throw new ConversionException("Can't convert '" + value + "' to " + type.getName());
        }
        return result;
    }

    @Override
    public String getQualifiedName(Object o) {
        
        String qualifiedName;
        
        if (o instanceof TopiaEntity) {
            qualifiedName = TopiaEntity.class.getName();
        }
        else {
            qualifiedName = o.getClass().getName();
        }
        return qualifiedName;
    }

}
