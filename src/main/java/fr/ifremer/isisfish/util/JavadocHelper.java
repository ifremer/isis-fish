/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2020 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.util;

import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.datastore.FormuleStorage;
import fr.ifremer.isisfish.datastore.JavaSourceStorage;
import fr.ifremer.isisfish.datastore.SimulationStorage;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.JavaVersion;
import org.apache.commons.lang3.SystemUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.FileUtil;

import javax.tools.DocumentationTool;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;
import java.io.File;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Helper for javadoc.
 * 
 * Use "javadoc" executable tools located in JAVA_HOME.
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 * 
 * @see CompileHelper for classpath purpose
 * @since 3.2.0.4
 */
public class JavadocHelper {

    /** Logger for this class. */
    private static final Log log = LogFactory.getLog(JavadocHelper.class);

    /**
     * Generate javadoc of current.
     * 
     * @param source source storage
     * @param destDir javadoc destination directory
     * @param force force (if html file is newer than java file)
     * @param out out writer (javadoc output, can be {@code null})
     * @return javadoc tool result
     */
    public static int generateJavadoc(JavaSourceStorage source, File destDir, boolean force, PrintWriter out) {
        File src = source.getFile();
        File dst = new File(destDir, source.getFQN().replace('.', File.separatorChar) + ".html");
        
        int result = 0;
        if (force || !dst.exists() || FileUtils.isFileNewer(src, dst)) {
            result = generateJavadoc(source.getRoot(), src, destDir, out);
        }
        return result;
    }

    /**
     * Generate javadoc on all java file in a directory.
     * 
     * @param rootSrc le repertoire de toutes les sources
     * @param dest le répertoire de destination de la javadoc
     * @param out le flux de sortie (peut etre null)
     * @return le resulat de sortie de l'executable javadoc (0 si ok)
     */
    public static int generateJavadoc(File rootSrc, File dest, PrintWriter out) {
        // use specific filter to add more condition than FileUtil#find
        List<File> javaFiles = FileUtil.getFilteredElements(rootSrc, f -> {
            boolean result = true;
            // seulement sur les fichiers java
            result &= f.getAbsolutePath().endsWith(".java");
            // don't do javadoc in "formules" directory (fail)
            result &= !f.getAbsolutePath().startsWith(FormuleStorage.getFormuleDirectory().getAbsolutePath());
            // don't do javadoc for simulations too (useless)
            result &= !f.getAbsolutePath().startsWith(SimulationStorage.getSimulationDirectory().getAbsolutePath());
            return result;
        }, true);

        int result = generateJavadoc(rootSrc, javaFiles, dest, out);
        return result;
    }
    
    /**
     * Generate single file javadoc.
     * 
     * @param rootSrc le repertoire de toutes les sources
     * @param fileSrc le fichier dont on veut la javadoc
     * @param dest le répertoire de destination de la javadoc
     * @param out le flux de sortie (peut etre null)
     * @return le resulat de sortie de l'executable javadoc (0 si ok)
     */
    public static int generateJavadoc(File rootSrc, File fileSrc, File dest, PrintWriter out) {
        int result = generateJavadoc(rootSrc, Collections.singletonList(fileSrc), dest, out);
        return result;
    }

    /**
     * Generate file collection javadoc.
     * 
     * @param rootSrc le repertoire de toutes les sources
     * @param filesSrc les fichiers dont on veut la javadoc
     * @param dest le répertoire de destination de la javadoc
     * @param out le flux de sortie (peut etre null)
     * @return le resulat de sortie de l'executable javadoc (0 si ok)
     */
    public static int generateJavadoc(File rootSrc, Collection<File> filesSrc, File dest, PrintWriter out) {
        int result = -1;
        try {
            List<File> classpath = new ArrayList<>();
            classpath.add(rootSrc.getAbsoluteFile());
            result = generateJavadoc(classpath, filesSrc, dest, out);
        } catch (Exception eee) {
            if (log.isWarnEnabled()) {
                log.warn("Javadoc failed", eee);
            }
        }
        return result;
    }
    
    /**
     * Generate file collection javadoc.
     * 
     * @param classpath common classpath
     * @param filesSrc les fichiers dont on veut la javadoc
     * @param dest le répertoire de destination de la javadoc
     * @param out le flux de sortie (peut etre null)
     * @return le resulat de sortie de l'executable javadoc (0 si ok)
     */
    protected static int generateJavadoc(List<File> classpath, Collection<File> filesSrc, File dest, PrintWriter out) {

        int result = 0;
        
        // make destination directory
        dest.mkdirs();

        try {
            List<String> args = new ArrayList<>();
            args.add("-link");
            args.add(IsisFish.config.getJavadocJavaURL());
            args.add("-link");
            args.add(IsisFish.config.getJavadocIsisURL());
            args.add("-link");
            args.add(IsisFish.config.getJavadocTopiaURL());
            args.add("-link");
            args.add(IsisFish.config.getJavadocMatrixURL());

            String classpathAsString = CompileHelper.getClassPathAsString(classpath);
            args.add("-classpath");
            args.add(classpathAsString);

            if (SystemUtils.isJavaVersionAtLeast(JavaVersion.JAVA_9)) {
                // Options de compilations
                args.add("--release=8");

                DocumentationTool tool = ToolProvider.getSystemDocumentationTool();
                StandardJavaFileManager fm = tool.getStandardFileManager(null, null, StandardCharsets.UTF_8);
                Iterable<? extends JavaFileObject> javaFileObjectsFromFiles = fm.getJavaFileObjectsFromFiles(filesSrc);
                fm.setLocation(DocumentationTool.Location.DOCUMENTATION_OUTPUT, Collections.singletonList(dest));
                //fm.setLocation(StandardLocation.CLASS_PATH, CompileHelper.getClassPathAsFiles(classpath));
                DocumentationTool.DocumentationTask task = tool.getTask(out, fm, null, null, args, javaFileObjectsFromFiles);

                if (!task.call()) {
                    result = 1;
                }
            } else {

                // FIXME JDK-8 : remove it later when jdk 8 is not supported anymore
                ClassLoader cl = Thread.currentThread().getContextClassLoader();
                try {
                    args.add("-d");
                    args.add(dest.getAbsolutePath());

                    for (File srcFile : filesSrc) {
                        args.add(srcFile.getAbsolutePath());
                    }

                    // on est obligé de changer le classloader, sinon il ne trouve pas la class
                    // com.sun.tools.doclets.standard.Standard
                    File jreHome = new File(System.getProperty("java.home"));
                    File jreLib = new File(jreHome.getParent(), "lib");
                    File toolsJar = new File(jreLib, "tools.jar");
                    URLClassLoader toolsJarLoader = new URLClassLoader(new URL[]{toolsJar.toURI().toURL()});
                    Thread.currentThread().setContextClassLoader(toolsJarLoader);

                    // invocation dynamique pour ne pas embarqué le jar
                    Class<?> mainTool = toolsJarLoader.loadClass("com.sun.tools.javadoc.Main");
                    Method method = mainTool.getMethod("execute", String.class, PrintWriter.class, PrintWriter.class,
                            PrintWriter.class, String.class, String[].class);
                    method.invoke(null, "javadoc", out, out, out,
                            "com.sun.tools.doclets.standard.Standard", args.toArray(new String[0]));
                    toolsJarLoader.close();
                } finally {
                    Thread.currentThread().setContextClassLoader(cl);
                }
            }
        } catch (Exception ex) {
            out.println("This function is only available with JDK (not JRE)");
            ex.printStackTrace(out);
            result = -1;
        }
        return result;
    }
}
