/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2008 - 2011 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.util.ssh;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Writer;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpATTRS;
import com.jcraft.jsch.SftpException;
import com.jcraft.jsch.SftpProgressMonitor;

/**
 * SSH utils class.
 * 
 * All this code has be taken from ant optionnal ssh task.
 * 
 * Use full for:
 *  - exec command
 *  - scpTo command
 *  - scpFrom command
 *  
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class SSHUtils {

    /** log. */
    private static Log log = LogFactory.getLog(SSHUtils.class);

    protected static final byte LINE_FEED = 0x0a;
    protected static final int BUFFER_SIZE = 1024;
    protected static final int HUNDRED_KILOBYTES = 102400;

    /** Utility class */
    protected SSHUtils() {

    }

    /**
     * Send an ack.
     * @param out the output stream to use
     * @throws IOException on error
     */
    protected static void sendAck(OutputStream out) throws IOException {
        byte[] buf = new byte[1];
        buf[0] = 0;
        out.write(buf);
        out.flush();
    }

    /**
     * Reads the response, throws a BuildException if the response
     * indicates an error.
     * @param in the input stream to use
     * @throws IOException on I/O error
     */
    protected static void waitForAck(InputStream in) throws IOException,
            SSHException {
        int b = in.read();

        // b may be 0 for success,
        //          1 for error,
        //          2 for fatal error,

        if (b == -1) {
            // didn't receive any response
            throw new SSHException("No response from server");
        } else if (b != 0) {
            StringBuilder sb = new StringBuilder();

            int c = in.read();
            while (c > 0 && c != '\n') {
                sb.append((char) c);
                c = in.read();
            }

            if (b == 1) {
                throw new SSHException("server indicated an error: "
                        + sb.toString());
            } else if (b == 2) {
                throw new SSHException("server indicated a fatal error: "
                        + sb.toString());
            } else {
                throw new SSHException("unknown response, code " + b
                        + " message: " + sb.toString());
            }
        }
    }

    /**
     * Track progress every 10% if 100kb &lt; filesize &lt; 1mb. For larger
     * files track progress for every percent transmitted.
     * @param filesize the size of the file been transmitted
     * @param totalLength the total transmission size
     * @param percentTransmitted the current percent transmitted
     * @return the percent that the file is of the total
     */
    protected static int trackProgress(long filesize, long totalLength,
            int percentTransmitted) {

        int percent = (int) Math.round(Math
                .floor((totalLength / (double) filesize) * 100));

        if (percent > percentTransmitted) {
            if (filesize < 1048576) {
                if (percent % 5 == 0) {
                    if (percent == 100) {
                        System.out.println(" 100%");
                    } else {
                        System.out.print("*");
                    }
                }
            } else {
                if (percent == 50) {
                    System.out.println(" 50%");
                } else if (percent == 100) {
                    System.out.println(" 100%");
                } else {
                    System.out.print(".");
                }
            }
        }

        return percent;
    }

    /**
     * Exec command on remote server.
     * 
     * @param session opened valid session
     * @param command command to exec
     * 
     * @return channel exit status (0 = no problem)
     * @throws SSHException 
     */
    public static int exec(Session session, String command) throws SSHException {
        return exec(session, command, null);
    }

    /**
     * Exec command on remote server.
     * 
     * @param session opened valid session
     * @param command command to exec
     * @param out exec output (can be null)
     * 
     * @return channel exit status (0 = no problem)
     * @throws SSHException 
     */
    public static int exec(Session session, String command, Writer out)
            throws SSHException {

        int exitStatus;
        
        BufferedReader br = null;
        try {
            // exec previous command
            Channel channel = session.openChannel("exec");
            ((ChannelExec) channel).setCommand(command);

            br = new BufferedReader(new InputStreamReader(channel.getInputStream()));
            channel.connect();
            String line;
            while (true) {
                while ((line = br.readLine()) != null) {
                    if (out != null) {
                        out.write(line);
                    }
                    else if (log.isInfoEnabled()) {
                        log.info("Remote output : " + line);
                    }
                }
                if (channel.isClosed()) {
                    exitStatus = channel.getExitStatus();
                    if (log.isInfoEnabled()) {
                        log.info("JSch channel exit-status: "
                                + exitStatus);
                    }
                    break;
                }
                try {
                    Thread.sleep(500);
                } catch (Exception ee) {
                }
            }
            channel.disconnect();
            // end read buffer
        } catch (JSchException | IOException e) {
            throw new SSHException("I/O error while executing command", e);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    // ignored
                }
            }
        }

        return exitStatus;
    }

    /**
     * Download a local file from remote server and check md5sum.
     * 
     * @param session opened valid jsch session
     * @param remoteFileName remote file name to download
     * @param localFile local file name to download into
     *
     * @throws SSHException if transfer fail
     */
    public static void scpFrom(Session session, String remoteFileName,
            File localFile) throws SSHException {
        scpFrom(session, remoteFileName, localFile, null);
    }

    /**
     * Download a local file from remote server and check md5sum.
     * 
     * @param session opened valid jsch session
     * @param remoteFileName remote file name to download
     * @param localFile local file name to download into
     * @param monitor progress monitor (can be null)
     *
     * @throws SSHException if transfer fail
     */
    public static void scpFrom(Session session, String remoteFileName,
            File localFile, ProgressMonitor monitor) throws SSHException {

        String command = "scp -f -r \"" + remoteFileName + "\"";

        ChannelExec channel = null;
        try {
            channel = (ChannelExec) session.openChannel("exec");
            channel.setCommand(command);

            // get I/O streams for remote scp
            OutputStream out = channel.getOutputStream();
            InputStream in = channel.getInputStream();

            channel.connect();

            sendAck(out);
            startRemoteCpProtocol(in, out, localFile, monitor);
        } catch (IOException | JSchException e) {
            throw new SSHException(e);
        } finally {
            if (channel != null) {
                channel.disconnect();
            }
        }
    }

    /**
     * Upload file on remote server.
     * 
     * @param session opened valid session
     * @param localFile file to upload
     * @param remoteFilePath remote file path
     *
     * @throws SSHException 
     */
    public static void scpTo(Session session, File localFile,
            String remoteFilePath) throws SSHException {

        try {
            doSingleTransfer(session, localFile, remoteFilePath);
        } catch (IOException | JSchException e) {
            throw new SSHException(e);
        }
    }

    // scp
    protected static void startRemoteCpProtocol(InputStream in,
            OutputStream out, File localFile, ProgressMonitor monitor) throws IOException, SSHException {
        File startFile = localFile;
        while (true) {
            // C0644 filesize filename - header for a regular file
            // T time 0 time 0\n - present if perserve time.
            // D directory - this is the header for a directory.
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            while (true) {
                int read = in.read();
                if (read < 0) {
                    return;
                }
                if ((byte) read == LINE_FEED) {
                    break;
                }
                stream.write(read);
            }
            String serverResponse = stream.toString("UTF-8");
            if (serverResponse.charAt(0) == 'C') {
                parseAndFetchFile(serverResponse, startFile, out, in, monitor);
            } else if (serverResponse.charAt(0) == 'D') {
                startFile = parseAndCreateDirectory(serverResponse, startFile);
                sendAck(out);
            } else if (serverResponse.charAt(0) == 'E') {
                startFile = startFile.getParentFile();
                sendAck(out);
            } else if (serverResponse.charAt(0) == '\01'
                    || serverResponse.charAt(0) == '\02') {
                // this indicates an error.
                throw new IOException(serverResponse.substring(1));
            }
        }
    }

    // scp
    protected static File parseAndCreateDirectory(String serverResponse,
            File localFile) {
        int start = serverResponse.indexOf(" ");
        // appears that the next token is not used and it's zero.
        start = serverResponse.indexOf(" ", start + 1);
        String directoryName = serverResponse.substring(start + 1);
        if (localFile.isDirectory()) {
            File dir = new File(localFile, directoryName);
            dir.mkdir();
            log.debug("Creating: " + dir);
            return dir;
        }
        return null;
    }

    // scp
    protected static void parseAndFetchFile(String serverResponse,
            File localFile, OutputStream out, InputStream in, ProgressMonitor monitor)
            throws IOException, SSHException {
        int start = 0;
        int end = serverResponse.indexOf(" ", start + 1);
        start = end + 1;
        end = serverResponse.indexOf(" ", start + 1);
        long filesize = Long.parseLong(serverResponse.substring(start, end));
        String filename = serverResponse.substring(end + 1);
        log.debug("Receiving: " + filename + " : " + filesize);
        File transferFile = (localFile.isDirectory()) ? new File(localFile,
                filename) : localFile;
        fetchFile(transferFile, filesize, out, in, monitor);
        waitForAck(in);
        sendAck(out);
    }

    // scp
    protected static void fetchFile(File localFile, long filesize,
            OutputStream out, InputStream in, ProgressMonitor monitor) throws IOException {
        byte[] buf = new byte[BUFFER_SIZE];
        sendAck(out);

        // case were progression is requested
        if (monitor != null) {
            monitor.init(filesize);
        }

        // read a content of lfile
        FileOutputStream fos = null;
        int length;
        //long totalLength = 0;

        try {
            fos = new FileOutputStream(localFile);
            while (true) {
                length = in.read(buf, 0, (BUFFER_SIZE < filesize) ? BUFFER_SIZE
                        : (int) filesize);
                if (length < 0) {
                    throw new EOFException("Unexpected end of stream.");
                }
                fos.write(buf, 0, length);
                filesize -= length;
                //totalLength += length;
                if (filesize == 0) {
                    break;
                }

                // case were progression is requested
                if (monitor != null) {
                    monitor.count(length);
                }
            }
        } finally {
            fos.flush();
            fos.close();
        }
        
        // case were progression is requested
        if (monitor != null) {
            monitor.end();
        }
    }

    // scp
    protected static void doSingleTransfer(Session session, File localFile,
            String remoteFilePath) throws IOException, JSchException,
            SSHException {

        String command = "scp -t \"" + remoteFilePath + "\"";
        ChannelExec channel = (ChannelExec) session.openChannel("exec");
        channel.setCommand(command);
        try {

            OutputStream out = channel.getOutputStream();
            InputStream in = channel.getInputStream();

            channel.connect();

            waitForAck(in);
            sendFileToRemote(localFile, in, out);
        } finally {
            channel.disconnect();
        }
    }

    // scp
    protected static void sendFileToRemote(File localFile, InputStream in,
            OutputStream out) throws IOException, SSHException {
        // send "C0644 filesize filename", where filename should not include '/'
        long filesize = localFile.length();
        String command = "C0644 " + filesize + " ";
        command += localFile.getName();
        command += "\n";

        out.write(command.getBytes());
        out.flush();

        waitForAck(in);

        // send a content of lfile
        byte[] buf = new byte[BUFFER_SIZE];
        //long totalLength = 0;

        try (FileInputStream fis = new FileInputStream(localFile)) {
            while (true) {
                int len = fis.read(buf, 0, buf.length);
                if (len <= 0) {
                    break;
                }
                out.write(buf, 0, len);
                //totalLength += len;
            }
            out.flush();
            sendAck(out);
            waitForAck(in);
        }
    }
    
    /**
     * Progress monitor (sftp).
     */
    protected static class UtilsProgressMonitor implements SftpProgressMonitor {
        private long initFileSize = 0;
        private long totalLength = 0;
        private int percentTransmitted = 0;

        public void init(int op, String src, String dest, long max) {
            initFileSize = max;
            totalLength = 0;
            percentTransmitted = 0;
        }

        public boolean count(long len) {
            totalLength += len;
            percentTransmitted = trackProgress(initFileSize,
                                               totalLength,
                                               percentTransmitted);
            return true;
        }

        public void end() {

        }

        public long getTotalLength() {
            return totalLength;
        }
    }
    
    /**
     * Open sftp channel.
     * 
     * @param session
     * @return sftp channel
     * @throws JSchException
     * @see ChannelSftp
     */
    protected static ChannelSftp openSftpChannel(Session session) throws JSchException {
        ChannelSftp channel = (ChannelSftp) session.openChannel("sftp");
        return channel;
    }
    
    /**
     * Perform sftp file transfert.
     * 
     * @param session 
     * @param localFile 
     * @param remotePath 
     * 
     * @throws SSHException on i/o errors
     */
    public static void sftpFrom(Session session, File localFile, String remotePath) throws SSHException {
        ChannelSftp channel = null;
        try {
            channel = openSftpChannel(session);
            channel.connect();
            try {
                SftpATTRS attrs = channel.stat(remotePath);
                if (attrs.isDir() && !remotePath.endsWith("/")) {
                    remotePath = remotePath + "/";
                }
            } catch (SftpException ee) {
                // Ignored
            }
            getDir(channel, remotePath, localFile);
        } catch (SftpException | JSchException | IOException e) {
            throw new SSHException(e);
        } finally {
            if (channel != null) {
                channel.disconnect();
            }
        }
    }

    protected static void getDir(ChannelSftp channel,
                        String remoteFile,
                        File localFile) throws IOException, SftpException {
        String pwd = remoteFile;
        if (remoteFile.lastIndexOf('/') != -1) {
            if (remoteFile.length() > 1) {
                pwd = remoteFile.substring(0, remoteFile.lastIndexOf('/'));
            }
        }
        channel.cd(pwd);
        if (!localFile.exists()) {
            localFile.mkdirs();
        }
        Vector files = channel.ls(remoteFile);
        for (int i = 0; i < files.size(); i++) {
            ChannelSftp.LsEntry le = (ChannelSftp.LsEntry) files.elementAt(i);
            String name = le.getFilename();
            if (le.getAttrs().isDir()) {
                if (name.equals(".") || name.equals("..")) {
                    continue;
                }
                getDir(channel,
                       channel.pwd() + "/" + name + "/",
                       new File(localFile, le.getFilename()));
            } else {
                getFile(channel, le, localFile);
            }
        }
        channel.cd("..");
    }

    protected static void getFile(ChannelSftp channel,
                         ChannelSftp.LsEntry le,
                         File localFile) throws IOException, SftpException {
        String remoteFile = le.getFilename();
        if (!localFile.exists()) {
            String path = localFile.getAbsolutePath();
            int i;
            if ((i = path.lastIndexOf(File.pathSeparator)) != -1) {
                if (path.length() > File.pathSeparator.length()) {
                    new File(path.substring(0, i)).mkdirs();
                }
            }
        }

        if (localFile.isDirectory()) {
            localFile = new File(localFile, remoteFile);
        }

        long totalLength = le.getAttrs().getSize();

        SftpProgressMonitor monitor = null;
        boolean trackProgress = totalLength > HUNDRED_KILOBYTES;
        if (trackProgress) {
            monitor = new UtilsProgressMonitor();
        }
        channel.get(remoteFile, localFile.getAbsolutePath(), monitor);
    }
    
    /**
     * Perform sftp file transfert.
     * 
     * @param session 
     * @param localFile 
     * @param remoteFilePath
     * 
     * @throws SSHException on i/o errors
     */
    public static void sftpTo(Session session, File localFile,
            String remoteFilePath) throws SSHException {
        try {
            doSingleSftpransfer(session, localFile, remoteFilePath);
        } catch (IOException | JSchException e) {
            throw new SSHException(e);
        }
    }

    // sftp
    protected static void doSingleSftpransfer(Session session, File localFile,
            String remoteFilePath) throws IOException, JSchException {
        ChannelSftp channel = openSftpChannel(session);
        try {
            channel.connect();
            try {
                sendFileToRemote(channel, localFile, remoteFilePath);
            } catch (SftpException e) {
                throw new JSchException(e.toString());
            }
        } finally {
            if (channel != null) {
                channel.disconnect();
            }
        }
    }

    // sftp
    protected static void sendFileToRemote(ChannelSftp channel,
                                  File localFile,
                                  String remotePath)
        throws IOException, SftpException {
        long filesize = localFile.length();

        if (remotePath == null) {
            remotePath = localFile.getName();
        }

        // only track progress for files larger than 100kb in verbose mode
        boolean trackProgress = filesize > HUNDRED_KILOBYTES;

        SftpProgressMonitor monitor = null;
        if (trackProgress) {
            monitor = new UtilsProgressMonitor();
        }

        channel.put(localFile.getAbsolutePath(), remotePath, monitor);
    }
}
