/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2010 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.util.ssh;

/**
 * Exception throws for password input cancellation.
 *
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class InvalidPassphraseException extends Exception {
    
    /** serialVersionUID. */
    private static final long serialVersionUID = -8878596298083203705L;

    /**
     * Constructor with message and cause.
     * 
     * @param message message
     * @param cause cause
     */
    public InvalidPassphraseException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructor with message.
     * 
     * @param message message
     */
    public InvalidPassphraseException(String message) {
        super(message);
    }
}
