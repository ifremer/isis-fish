/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2011 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.util.ssh;

/**
 * Progress monitor (sftp).
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * @since 3.3.0.0
 * Last update : $Date$
 * By : $Author$
 */
public class ProgressMonitor {

    /** Size of file to download. */
    protected long initFileSize = 0;

    /** Current dowloaded length. */
    protected long totalLength = 0;

    public void init(long max) {
        initFileSize = max;
        totalLength = 0;
    }

    public void count(long len) {
        totalLength += len;
    }

    public void end() {

    }
}
