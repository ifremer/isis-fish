/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2008 - 2010 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.util.ssh;

import com.jcraft.jsch.UIKeyboardInteractive;
import com.jcraft.jsch.UserInfo;
import fr.ifremer.isisfish.ui.keystore.PasswordManager;

import javax.swing.JOptionPane;
import javax.swing.JPasswordField;

/**
 * Class used to ask used for connection info.
 * 
 * Password, passphrase...
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class SSHUserInfo implements UserInfo, UIKeyboardInteractive {

    /**
     * Passphrase.
     * 
     * Static to be stored on multiple connexion.
     */
    protected String passphrase;

    /**
     * Password text field.
     */
    protected String passwd;

    /**
     * Password text field.
     */
    protected JPasswordField passwordField = new JPasswordField(20);

    public boolean forceNewPassword = false;

    /**
     * Call to ask user in remote server key
     * can be trusted. Here, auto accept.
     */
    @Override
    public boolean promptYesNo(String str) {
        return true;
    }

    @Override
    public String getPassphrase() {
        return passphrase;
    }

    @Override
    public String getPassword() {
        return passwd;
    }

    /**
     * @return the passwd
     */
    public String getPasswd() {
        return passwd;
    }

    /**
     * @param passwd the passwd to set
     */
    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    /**
     * @param passphrase the passphrase to set
     */
    public void setPassphrase(String passphrase) {
        this.passphrase = passphrase;
    }

    @Override
    public boolean promptPassphrase(String message) {
        return true;
    }

    @Override
    public boolean promptPassword(String message) {
        Object[] ob = { passwordField };
        int result = JOptionPane.showConfirmDialog(null, ob, message ,
                JOptionPane.OK_CANCEL_OPTION);
        boolean bResult = false;
        if (result == JOptionPane.OK_OPTION) {
            passwd = String.valueOf(passwordField.getPassword());
            bResult = true;
        }
        return bResult;
    }

    @Override
    public void showMessage(String message) {
        JOptionPane.showMessageDialog(null, message);
    }

    @Override
    public String[] promptKeyboardInteractive(String destination, String name, String instruction, String[] prompt, boolean[] echo) {
        String[] response = null;
        String password = PasswordManager.getInstance().getPassword(destination, forceNewPassword);
        if (password != null) {
            response = new String[]{password};
            forceNewPassword = true;
        }
        return response;
    }
}
