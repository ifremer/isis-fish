/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2015 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.util.ssh;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.KeyPair;
import fr.ifremer.isisfish.ui.keystore.PasswordManager;

import java.io.File;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Isis SSH agent.
 * 
 * This class centralize, ssh key and passphrase
 * management.
 *
 * This is a singleton class.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class SSHAgent {

    /** JSSch instance. */
    protected JSch jsch;

    /**
     * Passphrase associated to key file path.
     * 
     * private key file path =&lt; passphrase.
     * 
     * Ensure that passphrase are valid before add into.
     */
    protected Map<String, char[]> passphraseForKeys;

    /** Unique agent. */
    private static SSHAgent agent = new SSHAgent();

    /**
     * Constructor.
     */
    private SSHAgent() {
        jsch = new JSch();
        passphraseForKeys = new HashMap<>();
    }

    /**
     * Get agent instance.
     * 
     * @return agent
     */
    public static SSHAgent getAgent() {
        return agent;
    }

    /**
     * Get passphrase for key.
     * 
     * @param privatekeyFile private key file
     * @return {@code null} if there is no passphrase
     * @throws InvalidPassphraseException if user cancel authentication
     */
    public char[] getPassphrase(File privatekeyFile) throws InvalidPassphraseException {
        char[] passphrase = getPassphrase(privatekeyFile.getAbsolutePath());
        return passphrase;
    }

    /**
     * Get passphrase for key.
     * 
     * @param privatekeyFile private key file path
     * @return {@code null} if there is no passphrase
     * @throws InvalidPassphraseException if user cancel authentication
     */
    public char[] getPassphrase(String privatekeyFile) throws InvalidPassphraseException {

        char[] passphrase = passphraseForKeys.get(privatekeyFile);

        if (passphrase == null) {
            try {
                KeyPair kpair = KeyPair.load(jsch, privatekeyFile);

                if (kpair.isEncrypted()) {

                    boolean isValid = false;
                    boolean forceNew = false;
                    do {
                        String passphraseStr = PasswordManager.getInstance().getPassword(privatekeyFile, forceNew);
                        forceNew = true;
                        if (passphraseStr == null) {
                            throw new InvalidPassphraseException("User cancel passphrase ask");
                        }

                        if (kpair.decrypt(toBytes(passphraseStr.toCharArray()))) {
                            isValid = true;
                            passphraseForKeys.put(privatekeyFile, passphrase);
                        }
                    } while (!isValid);
                }
            } catch (JSchException e) {
                throw new RuntimeException("Can't find key : " + privatekeyFile);
            }
        }

        return passphrase;
    }

    /**
     * Transform char array to byte array without creating String instance.
     * 
     * see http://stackoverflow.com/a/9670279/2038100 for details
     * 
     * @param chars char array
     * @return byte array
     */
    protected static byte[] toBytes(char[] chars) {
        CharBuffer charBuffer = CharBuffer.wrap(chars);
        ByteBuffer byteBuffer = StandardCharsets.UTF_8.encode(charBuffer);
        byte[] bytes = Arrays.copyOfRange(byteBuffer.array(), byteBuffer.position(), byteBuffer.limit());
        Arrays.fill(byteBuffer.array(), (byte) 0); // clear sensitive data
        return bytes;
    }
}
