/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2010 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.util.cache;

import fr.ifremer.isisfish.annotations.Nocache;
import fr.ifremer.isisfish.types.TimeStep;
import org.aspectj.lang.ProceedingJoinPoint;
import org.nuiton.topia.persistence.TopiaEntity;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * IsisCacheOnReferenceMap utilise pour mettre les resultats de methode durant
 * la simulation pour minimiser les appels
 * <p>
 * Created: 25 août 06 22:42:47
 * <p>
 * @author poussin
 * @version $Revision$
 <p>
 Last update: $Date$ by :
 $Author$
 */
public class IsisCache {

    static public IsisCacheBackend.Factory defaultFactory
            = IsisCacheBackendOnGuava.factory;

    // la longueur du package pour minimiser la longueur des topiaId
    static final private int entityPackageLenght = "fr.ifremer.isisfish.entities.".length();
    // la valeur NULL a utilise a la place de null pour les timeStep, et les retours de method
    static final private Object NULL = new Object();

    protected long totalCall = 0;
    protected long cacheUsed = 0;

    protected IsisCacheBackend cacheBackend;

    public IsisCache() {
        this(defaultFactory.createNew());
    }

    public IsisCache(IsisCacheBackend cacheBackend) {
        this.cacheBackend = cacheBackend;
    }

//    /**
//     * Return trace object from context.
//     *
//     * @return trace object from context
//     */
//    protected org.nuiton.profiling.Trace getTrace() {
//        SimulationContext context = SimulationContext.get();
//        org.nuiton.profiling.Trace result = context.getTrace();
//        return result;
//    }

    /**
     * La representation de la methode en string, la representation commence
     * par un @ si il y a l'annotation noCache trouver pour cette methode.
     */
    protected Map<Method, String> methodStringCache = new HashMap<>();
    // use to compute key, isis simulation is monothread, we can instanciate it here
    // to minimise object instanciation
    protected StringBuilder sbKey = new StringBuilder(300);

    /**
     * Recupere pour un pas de temps donnes une valeur calcule pour une cle.
     * 
     * @param defaultValue la valeur par defaut a retourner si elle n'est pas en
     *                     cache si defaultValue est une JoinPoint alors un
     *                     proceed est appele dessus et le resultat de l'appel
     *                     est utiliser comme valeur par defaut
     * @return la valeur dans le cache ou defaultValue
     */
    public Object get(Method method, Object[] args, Object defaultValue) throws Throwable {
        totalCall++;

        Object result;

        if (Void.TYPE.equals(method.getReturnType())) {
            // for void return type, never put it in cache, we can call directly real method
            result = realCall(defaultValue);
        } else {

            String methodString = methodStringCache.get(method);
            if (methodString == null) {
                Class declaringClass = method.getDeclaringClass();
                methodString = (method.getAnnotation(Nocache.class) != null
                        || declaringClass.getAnnotation(Nocache.class) != null
                        || method.getAnnotation(fr.ifremer.isisfish.util.Nocache.class) != null
                        || declaringClass.getAnnotation(fr.ifremer.isisfish.util.Nocache.class) != null) ? "@" : "";
                methodString += declaringClass.getSimpleName() + "."+ method.getName();
                methodString = methodString.intern();
                methodStringCache.put(method, methodString);
            }

            // if method have annotation 'noCache' name start with @
            if (methodString.charAt(0) == '@') {
                result = realCall(defaultValue);
            } else {
                // compute key and keep TimeStep objet
                // le pas de temps trouve dans les arguments
                sbKey.setLength(0);
                TimeStep step = computeKey(sbKey, methodString, args);

                // on recupere le intern de la String car normalement en cache on
                // retrouve souvent les memes chaines comme cle (sinon le cache
                // servirait a rien :D)
                String key = sbKey.toString().intern();
                result = get(step, key);
                if (result == null) {
                    result = realCall(defaultValue);
                    // si return null on stock NULL pour faire la distinction
                    // entre present dans le cache ou absent (get(...) return null)
                    put(step, key, result==null?NULL:result);
                } else {
                    if (result == NULL) {
                        result = null;
                    }
                    cacheUsed++;
                }
            }
        }
        return result;
    }

    protected TimeStep computeKey(StringBuilder sbKey, String methodString, Object[] args) {
        TimeStep result = null;
        sbKey.append(methodString);
        for (Object o : args) {
            sbKey.append(";");
            if (o == null) {
                sbKey.append("null");
            } else if (o instanceof Number || o instanceof String) {
                sbKey.append(o.toString());
            } else if (o instanceof TimeStep) {
                result = (TimeStep) o;
                sbKey.append(result.getStep());
            } else if (o instanceof TopiaEntity) {
                sbKey.append(((TopiaEntity) o).getTopiaId().substring(entityPackageLenght));
            } else {
                // on minimise en prenant que le nom de la classe (pas le package)
                sbKey.append(o.getClass().getSimpleName());
                sbKey.append('@');
                // et en lui ajoutant son adresse memoire en Hexa
                sbKey.append(System.identityHashCode(o));
            }
        }
        return result;
    }

    protected Object get(TimeStep step, String key) {
        Object result = cacheBackend.get(step, key);
        return result;
    }

    protected void put(TimeStep step, String key, Object value) {
        cacheBackend.put(step, key, value);
    }

    /**
     * On fait l'appel reel dans une autre methode pour pouvoir le savoir dans
     * les traces.
     * 
     * @return ?
     * @throws Throwable
     */
    protected Object realCall(Object defaultValue) throws Throwable {
        Object result = defaultValue;
        if (defaultValue instanceof ProceedingJoinPoint) {
            result = ((ProceedingJoinPoint)defaultValue).proceed();
        }
        return result;
    }

    /**
     * remove all values in cache
     */
    public void clear() {
        cacheBackend.clear();
    }

    /**
     * remove all values in cache for the specied TimeStep
     */
    public void clear(TimeStep step) {
        Object key = step;
        if (key == null) {
            key = NULL;
        }
        cacheBackend.removeStep(key);
    }

    /**
     * @return Returns the cacheUsed.
     */
    public long getCacheUsed() {
        long result = cacheUsed;
        return result;
    }

    /**
     * @return Returns the totalCall.
     */
    public long getTotalCall() {
        long result = totalCall;
        return result;
    }

    /**
     * Affiche les statistiques
     * <p>
     */
    public String printStatistiqueAndClear() {
        cacheBackend.clear();

        StringBuilder result = new StringBuilder();
        result.append("--- Cache Statistiques ---\n");
        result.append("Total call: ").append(totalCall).append("\n");
        result.append("Cache used: ").append(cacheUsed).append("\n");
        long percent = 0;
        if (totalCall != 0) {
            percent = 100 * cacheUsed / totalCall;
        }
        result.append("Cache usage: ").append(percent).append("%\n");
        result.append(cacheBackend.getStat());
        result.append("--------------------\n");

        System.out.println(result.toString());
        return result.toString();
    }

    // EN COURS D'ECRITURE MAIS JAMAIS FINI :(
    // l'idee est de prendre la place memoire qu'il y a, mais s'il n'y en
    // a plus on commence par enlever les objets les plus vieux
//    class AdaptaptativeCache extends LinkedHashMap<String, Object> {
//        protected int maxMemory = 95;
//        /**
//         *
//         * @param capacity initial capacity
//         * @param maxMemory maximum memory used (0-100)
//         */
//        public AdaptaptativeCache(int capacity, int maxMemory) {
//            super(capacity, 0.75f, true);
//            this.maxMemory = maxMemory;
//        }
//
//        @Override
//        protected boolean removeEldestEntry(Entry<String, Object> eldest) {
//            double free = 100.0 * Runtime.getRuntime().freeMemory() / Runtime.getRuntime().maxMemory() ;
//            boolean result = 100 - free > maxMemory;
//            return result;
//        }
//    }
}
