/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2014 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.util.cache;

import java.util.Map;

import org.apache.commons.collections4.map.AbstractReferenceMap.ReferenceStrength;
import org.apache.commons.collections4.map.ReferenceMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.ifremer.isisfish.types.TimeStep;

/**
 * IsisCacheBackendOnReferenceMap utilise pour mettre les resultats de methode durant la simulation
 * pour minimiser les appels
 *
 * Created: 25 août 06 22:42:47
 *
 * @author poussin
 * @version $Revision$
 * 
 * Last update: $Date$
 * by : $Author$
 */
public class IsisCacheBackendOnReferenceMap implements IsisCacheBackend {
    
    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(IsisCacheBackendOnReferenceMap.class);

    static public class IsisCacheBackendOnReferenceMapFactory implements IsisCacheBackend.Factory {
        @Override
        public IsisCacheBackend createNew() {
            return new IsisCacheBackendOnReferenceMap();
        }
    }

    static public Factory factory = new IsisCacheBackendOnReferenceMapFactory();

    // la longueur du package pour minimiser la longueur des topiaId
    static final private int entityPackageLenght = "fr.ifremer.isisfish.entities.".length();
    // la valeur NULL a utilise a la place de null pour les timeStep
    static final private Object NULL = new Object();

    /**
     * map&lt;TimeStep, Map&lt;Key, Value&gt;&gt;
     * TimeStep peut etre null via l'objet NULL
     * Key est la cle calcule par computeKey
     * Value est la valeur du cache
     *
     * TimeStep est en WEAK pour que des que l'on passe au pas de temps suivant
     * ils soit efface de la memoire si plus personne n'a de reference sur
     * ce pas de temps. L'autre moyen est de force l'effacement via clear(TimeStep)
     *
     * Value est en SOFT reference pour que les valeurs soit effacee du cache
     * lorsqu'il n'y a plus de place memoire
     */
    protected Map cache = new ReferenceMap(ReferenceStrength.WEAK, ReferenceStrength.HARD);
    
    public IsisCacheBackendOnReferenceMap() {
    }

//    /**
//     * Return trace object from context.
//     *
//     * @return trace object from context
//     */
//    protected Trace getTrace() {
//        SimulationContext context = SimulationContext.get();
//        Trace result = context.getTrace();
//        return result;
//    }

    protected Map getCacheTimeStep(TimeStep step) {
        Object key = step;
        if (step == null)  {
            key = NULL;
        }
        Map result = (Map)cache.get(key);
        if (result == null) {
            result = new ReferenceMap(ReferenceStrength.HARD, ReferenceStrength.SOFT);
            cache.put(key, result);
        }
        return result;
    }

    @Override
    public Object get(TimeStep step, String key) {
        Object result = null;
        Map cacheTimeStep = getCacheTimeStep(step);
        if (cacheTimeStep != null) {
            result = cacheTimeStep.get(key);
        }
        return result;
    }

    @Override
    public void put(TimeStep step, String key, Object value) {
        getCacheTimeStep(step).put(key, value);
    }

    /**
     * remove all values in cache
     */
    @Override
    public void clear() {
        cache.clear();
    }

    /**
     * remove all values in cache for the specified TimeStep
     */
    @Override
    public void removeStep(Object o) {
        cache.remove(o);
    }

    @Override
    public String getStat() {
        return String.format("%s - no specific stat\n", getClass().getSimpleName());
    }
}


