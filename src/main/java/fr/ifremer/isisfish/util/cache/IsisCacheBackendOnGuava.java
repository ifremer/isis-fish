/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 Ifremer, Code Lutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.util.cache;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.Weigher;
import fr.ifremer.isisfish.types.TimeStep;
import java.util.Collection;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.util.StringUtil;

/**
 * Cache qui reserve de la place pour l'equivalent de N pas de temps dans le cache.
 * 
 * Une premiere instanciation du cache est faite. Puis apres le premier pas
 * de temps on regarde la taille des objets ayant ete ajoutes au cache pour
 * recalculer la nouvelle taille pour permettre de stocker N pas de temps.
 *
 * @author poussin
 * @version $Revision$

 Last update: $Date$
 by : $Author$
 */
public class IsisCacheBackendOnGuava implements IsisCacheBackend {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(IsisCacheBackendOnGuava.class);

    final static public int POINTER_SIZE = 8;
    final static public int DOUBLE_SIZE = 8;
    final static public int CHAR_SIZE = 2;

    /** number of time steps to cache by interpolating from the first step*/
    // perhaps allow the configuration
    static public double CACHE_STEP = 2; // between 2 step, cache size is very close: 2 seem correct

    static public class IsisCacheBackendOnGuavaFactory implements IsisCacheBackend.Factory {
        @Override
        public IsisCacheBackend createNew() {
            return new IsisCacheBackendOnGuava();
        }
    }

    static public Factory factory = new IsisCacheBackendOnGuavaFactory();

    static public class IsisWeigher implements Weigher<String, Object> {

        protected IsisCacheBackendOnGuava backend;
        public IsisWeigher(IsisCacheBackendOnGuava backend) {
            this.backend = backend;
        }

        @Override
        public int weigh(String key, Object value) {
            int result = key.length() * CHAR_SIZE;
            if (value instanceof MatrixND) {
                // MatrixND
                // on prend le nombre de case de la matrice pour sa taille
                // on ne tient pas compte des dimensions qui sont des objets
                // utilises ailleurs
                long size = ((MatrixND)value).getNumberOfAssignedValue();
                // size is wrong, because depend on matrix type (float or double), but approximation is acceptable
                result += size * DOUBLE_SIZE;
            } else if (value instanceof Collection) {
                // List<Zone|Pop|Str|Metier|Cell>
                // on prend la taille de la collection, car la donnees
                // elle meme qu'elle soit dans le backend ou non est
                // certainement utilisee ailleur, donc seul la taille
                // des pointeurs dans la collection est prise ici
                result += ((Collection<?>)value).size() * POINTER_SIZE;
            } else if (value instanceof Number) {
                // int| double
                // seulement une valeur, on ajoute 1
                result += DOUBLE_SIZE;
            } else {
                // a priori, il n'y a pas d'autre type de donnee
                if (value != null) {
                    log.info("Cache can't compute value size of " + ((Object)value).getClass());
                }
            }

            backend.addStat(result);
            
            result = Math.max(1024, result); // min to 1ko
            return result;
        }
    }

    protected Cache<String, Object> cache;
    protected IsisWeigher isisWeigher;
    protected long maxMemory;
    protected long maxCache;
    protected long sizeCache;

    /** the last step use to put value in cache */
    protected TimeStep lastStep = null;
    /** if true size of cache has been recomputed */
    protected boolean adjusted = false;

    /** le total en octet mis en cache au precedent pas de temps*/
    protected long totalLastStepCached = 0;

    // STATISTIQUE PAR ENTREE DANS LE CACHE
    /** le total en d'element mis en cache */
    protected long numberCached = 0;
    /** le total en octet mis en cache */
    protected long totalCached = 0;
    /** la plus petite donnee mis en cache */
    protected long smallestCached = Long.MAX_VALUE;
    /** la plus grosse donnee mis en cache */
    protected long biggestCached = 0;

    // for variance
    double delta = 0;
    double mean = 0;
    double M2 = 0;

    // STATISTIQUE PAR PAS DE TEMPS
    /** le total en d'element mis en cache */
    protected long numberStepCached = 0;
    /** le total en octet mis en cache */
    protected long totalStepCached = 0;
    /** la plus petite donnee mis en cache */
    protected long smallestStepCached = Long.MAX_VALUE;
    /** la plus grosse donnee mis en cache */
    protected long biggestStepCached = 0;

    // for variance
    double stepDelta = 0;
    double stepMean = 0;
    double stepM2 = 0;



    public IsisCacheBackendOnGuava() {
        isisWeigher = new IsisWeigher(this);

        maxMemory = Runtime.getRuntime().maxMemory();
        maxCache = maxMemory - 500*1024*1024; // isis need about 500M
        maxCache = Math.max(maxCache, 200*1024*1024); // to do simulation we need 200M cache

        // create first cache, this cache is juste for one step
        adjustedCache(maxCache);
    }

    public void addStat(long value) {
        numberCached++;
        totalCached = totalCached + value;

        // compute variance
        delta = value - mean;
        mean = mean + delta/numberCached;
        M2 = M2 + delta * (value - mean);

        biggestCached = Math.max(biggestCached, value);
        smallestCached = Math.min(smallestCached, value);
    }

    public void addStepStat(long value) {
        numberStepCached++;
        totalStepCached = totalStepCached + value;

        // compute variance
        stepDelta = value - stepMean;
        stepMean = stepMean + stepDelta/numberStepCached;
        stepM2 = stepM2 + stepDelta * (value - stepMean);

        biggestStepCached = Math.max(biggestStepCached, value);
        smallestStepCached = Math.min(smallestStepCached, value);
    }

    @Override
    public void clear() {
        cache.invalidateAll();
        // clear is assimilate to step change (last step end)
        // useful to finalize variance computation for last step
        changeStep();
    }

    @Override
    public void removeStep(Object o) {
    }

    /**
     * resizes the cache if amount is not greater than autorized maxCache
     * @param amount new size of cache in byte
     */
    public void adjustedCache(long amount){
        if (amount == 0) {
            log.info("Cache size not ajusted because 0 is not acceptable amount");
        } else {
            // on estime le nombre d'entree necessaire pour ce volume de donnees
            long bytePerStep = totalCached / Math.max(1, numberStepCached);
            long entriesPerStep = numberCached / Math.max(1, numberStepCached);

            long initialCapacity = amount * entriesPerStep / Math.max(1, bytePerStep);
            long stepEquivalent = initialCapacity / Math.max(1, entriesPerStep);

            // use amount as new size for cache
            // if this new size is less than maxCache, use it
            // to keep more memory for JVM
            sizeCache = Math.min(this.maxCache, amount);
        
            CacheBuilder<Object, Object> builder = CacheBuilder.newBuilder();
            builder.maximumWeight(sizeCache);
            builder.weigher(isisWeigher);
            if (initialCapacity > 0) {
                builder.initialCapacity((int)initialCapacity);
            }
            cache = builder.build();

            log.info(String.format("Cache size ajusted to %s (equivalent to %s step need)",
                    StringUtil.convertMemory(sizeCache), stepEquivalent));
        }
    }

    /**
     * Previent le cache, qu'on change de TimeStep et qu'il peut faire
     * du travail s'il en a besoin.
     *
     * - calcul de statistique
     * - ajustement de la taille du cache
     */
    public void changeStep() {
        // on est passe au pas de temps suivant
        long stepAmount = totalCached - totalLastStepCached;
        totalLastStepCached = totalCached;
        addStepStat(stepAmount);

        // after first step, use information to resize cache
        if (!adjusted) {
            adjustedCache((long)(stepAmount * CACHE_STEP)); // keep place for 1.3 step
            adjusted = true;
        }
    }

    @Override
    public Object get(TimeStep step, String key) {
        Object result = cache.getIfPresent(key);
        return result;
    }

    @Override
    public void put(TimeStep step, String key, Object value) {
        cache.put(key, value);
        if (step != null) {
            if (lastStep == null) {
                lastStep = step;
            } else if (lastStep.getStep() != step.getStep()) {
                // if step differ, this indicate new Step
                changeStep();
                lastStep = step;
            }
        }
    }
    
    @Override
    public String getStat() {
        long mean = totalCached / Math.max(1, numberCached);
        long stepMean = totalStepCached / Math.max(1, numberStepCached);

        double variance = M2/Math.max(1, numberCached - 1);
        double stepVariance = stepM2/Math.max(1, numberStepCached - 1);

        double standardDeviation = Math.sqrt(variance);
        double stepStandardDeviation = Math.sqrt(stepVariance);

        String trick = "";
        if (sizeCache < biggestStepCached) {
            trick = String.format(
                    "TRICK: for better performance you must set memory at least to %s\n",
                    StringUtil.convertMemory(biggestStepCached + 500 * 1024 *1024));
        }
        String result = String.format("%s - \n%s"
                + "\tinitial cache size: %s, cache size reevaluated: %s, totalCached: %s, numberStep: %s, numberEntries: %s\n"
                + "\tStep min: %s, max: %s, mean: %s, standard deviation: %s\n"
                + "\tEntry min: %s, max: %s, mean: %s, standard deviation: %s\n",
                trick,
                getClass().getSimpleName(), 
                StringUtil.convertMemory(maxMemory), StringUtil.convertMemory(sizeCache), StringUtil.convertMemory(totalCached), numberStepCached, numberCached,
                StringUtil.convertMemory(smallestStepCached), StringUtil.convertMemory(biggestStepCached), StringUtil.convertMemory(stepMean), StringUtil.convertMemory((long)stepStandardDeviation),
                StringUtil.convertMemory(smallestCached), StringUtil.convertMemory(biggestCached), StringUtil.convertMemory(mean), StringUtil.convertMemory((long)standardDeviation)
        );

        return result;
    }


}
