/*
 * #%L
 * IsisFish
 *
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2024 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.util;

import com.github.therapi.runtimejavadoc.Comment;
import com.github.therapi.runtimejavadoc.CommentFormatter;
import com.github.therapi.runtimejavadoc.Link;
import com.github.therapi.runtimejavadoc.MethodJavadoc;
import com.github.therapi.runtimejavadoc.ParamJavadoc;
import com.github.therapi.runtimejavadoc.RuntimeJavadoc;
import com.github.therapi.runtimejavadoc.ToHtmlStringCommentVisitor;
import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.annotations.Args;
import fr.ifremer.isisfish.datastore.CodeSourceStorage.Location;
import fr.ifremer.isisfish.datastore.JavaSourceStorage;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.JavaVersion;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.text.StringEscapeUtils;
import org.eclipse.jdt.internal.compiler.tool.EclipseCompiler;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.topia.TopiaContext;
import org.nuiton.util.FileUtil;
import org.nuiton.util.StringUtil;

import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.StandardLocation;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringJoiner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static org.nuiton.i18n.I18n.t;

/**
 * Compile helper used to compile Java code.
 * <p>
 * JDK must be installed to use compilation.
 * (JRE won't work).
 * <p>
 * Created: 12 janv. 2006 15:29:53
 *
 * @author poussin
 * @version $Revision$
 * <p>
 * Last update: $Date$
 * by : $Author$
 */
public class CompileHelper {

    /**
     * Logger for this class.
     */
    private static final Log log = LogFactory.getLog(CompileHelper.class);

    /**
     * Recherche tous les fichiers qui un source plus recent que la version compilé.
     *
     * @param srcDir
     * @param destDir
     * @return File list
     */
    public static List<File> searchSrcToCompile(File srcDir, File destDir) {
        List<File> result = new ArrayList<>();
        for (File src : srcDir.listFiles()) {
            File dest = new File(FileUtil.basename(src, ".java"), ".class");
            if (src.getName().endsWith(".java") && FileUtils.isFileNewer(src, dest)) {
                result.add(src);
            }
        }
        return result;
    }

    /**
     * Load la class demandé.
     *
     * @param fqn le nom complet de la classe a charger
     * @return la classe souhaité ou null si la class n'est pas trouvée
     */
    public static Class<?> loadClass(String fqn) {
        Class<?> result = null;
        try {
            ClassLoader cl = IsisFish.config.getScriptClassLoader();
            result = cl.loadClass(fqn);
        } catch (ClassNotFoundException eee) {
            log.info(t("isisfish.error.load.class", fqn), eee);
        }
        return result;
    }

    public static Object newInstance(String fqn) {
        Object result = null;
        try {
            Class<?> clazz = loadClass(fqn);

            if (clazz != null) {
                result = clazz.newInstance();
            }
        } catch (Exception eee) {
            log.warn(t("isisfish.error.instanciate", fqn), eee);
        }
        return result;
    }

    /**
     * Compile le fichier source en .class si le source est plus recent que
     * le .class
     *
     * @param source  le JavaSourceStorage a compiler
     * @param destDir le repertoire destination de la compilation
     * @param force   si vrai alors meme si le fichier destination est plus
     *                recent la compilation aura lieu
     * @param out     le flux sur lequel le resultat de la compilation doit
     *                apparaitre. Peut-etre null, dans ce cas les sorties standards sont
     *                utilisées.
     * @return 0 si la compilation a reussi une autre valeur sinon
     */
    public static int compile(JavaSourceStorage source, File destDir,
                              boolean force, PrintWriter out) {
        File src = source.getFile();
        File dst = new File(destDir, source.getFQN().replace('.',
                File.separatorChar) + ".class");
        if (force || !dst.exists() || FileUtils.isFileNewer(src, dst)) {
            return compile(source.getRoot(), src, destDir, out);
        }
        return 0;
    }

    /**
     * Methode permettant de compiler un fichier Java.
     *
     * @param rootSrc le répertoire ou se trouve les sources
     * @param src     Le fichier source a compiler, il doit etre dans un sous
     *                répertoire de rootSrc en fonction du package
     * @param dest    le repertoire destination de la compilation
     * @param out     l'objet sur lequel on ecrit la sortie (erreur) de la
     *                compilation
     * @return un nombre different de 0 s'il y a une erreur
     * <ul>
     * <li> -1000 si l'exception vient de la recherche du compilateur par
     * introspection</li>
     * <li> -10000 si une autre exception</li>
     * <li> sinon les valeurs retourné par le compilateur java</li>
     * </ul>
     */
    public static int compile(File rootSrc, File src, File dest, PrintWriter out) {
        int result = compile(rootSrc, Collections.singletonList(src), dest, out);
        return result;
    }

    /**
     * Methode permettant de compiler un ensemble de fichiers Java.
     *
     * @param rootSrc le répertoire ou se trouve les sources
     * @param src     Le fichier source a compiler, il doit etre dans un sous
     *                répertoire de rootSrc en fonction du package
     * @param dest    le repertoire destination de la compilation
     * @param out     l'objet sur lequel on ecrit la sortie (erreur) de la
     *                compilation
     * @return un nombre different de 0 s'il y a une erreur
     * <ul>
     * <li> -1000 si l'exception vient de la recherche du compilateur par
     * introspection</li>
     * <li> -10000 si une autre exception</li>
     * <li> sinon les valeurs retourné par le compilateur java</li>
     * </ul>
     */
    public static int compile(File rootSrc, Collection<File> src, File dest,
                              PrintWriter out) {
        int result = -10000;
        try {
            List<File> classpath = new ArrayList<>();

            classpath.add(rootSrc.getAbsoluteFile());

            // works better than
            // fileManager.setLocation(StandardLocation.SOURCE_PATH, Location.ALL);
            // for some test
            classpath.addAll(Arrays.asList(Location.ALL.getDirectories()));

            result = compile(classpath, src, dest, out);
        } catch (Exception eee) {
            if (log.isWarnEnabled()) {
                log.warn("Compilation failed", eee);
            }
        }
        return result;
    }

    /**
     * Compile un fichier java.
     *
     * @param src  les fichiers java source
     * @param dest le repertoire destination
     */
    protected static int compile(List<File> classpath, Collection<File> src, File dest, PrintWriter out) {
        dest.mkdirs();

        int result = -1000;
        try {
            // use eclipse ecj compiler
            JavaCompiler compiler = new EclipseCompiler();

            // Get compilation units
            StandardJavaFileManager fileManager = compiler.getStandardFileManager(null, null, StandardCharsets.UTF_8);
            //fileManager.setLocation(StandardLocation.CLASS_PATH, CompileHelper.getClassPathAsFiles(classpath));
            fileManager.setLocation(StandardLocation.CLASS_OUTPUT, Collections.singleton(dest));
            Iterable<? extends JavaFileObject> compilationUnits = fileManager.getJavaFileObjectsFromFiles(src);

            List<String> args = new ArrayList<>();
            // Options de compilations
            args.add("-" + JavaVersion.JAVA_17);
            String classpathAsString = CompileHelper.getClassPathAsString(classpath);
            args.add("-classpath");
            args.add(classpathAsString);
            // debug infos
            args.add("-g");
            // Affichage seulement des deprecated (sinon, trop de unused...)
            args.add("-warn:deprecation");
            //args.add("-deprecation");

            // Compilation
            boolean b = compiler.getTask(out, fileManager, null, args, null, compilationUnits).call();
            // on retourne 0 si tout s'est bien déroulé et -1 sinon
            result = b ? 0 : -1;

            fileManager.close();
        } catch (IOException eee) {
            if (log.isWarnEnabled()) {
                log.warn("Can't get system compiler or classpath info", eee);
            }
        }
        return result;
    }

    /**
     * Return full classpath (for compilation or javadoc) as string.
     * Separated by {@link File#pathSeparator}.
     * <p>
     * Add :
     * <ul>
     *  <li>System.getProperty("java.class.path")</li>
     *  <li>All first jar dependency (META-INF/MANIFEST.MF)</li>
     * </ul>
     *
     * @param classpath initial classpath
     * @return classpath as string
     * @throws IOException
     */
    public static String getClassPathAsString(List<File> classpath) throws IOException {
        String result = StringUtils.join(classpath.iterator(), File.pathSeparator);

        // chatellier : since 20090512 java.class.path in not added to
        // classpath.
        // result in duplicated entry or non normalised entry
        // en compilation fail some times
        //+ File.pathSeparator + System.getProperty("java.class.path");

        // Ajout des jars
        for (Enumeration<?> e = CompileHelper.class.getClassLoader().getResources("META-INF/MANIFEST.MF"); e.hasMoreElements(); ) {
            URL url = (URL) e.nextElement();
            if (log.isDebugEnabled()) {
                log.debug("Found manifest : " + url);
            }
            if ((url != null) && url.getFile().startsWith("file:/")) {
                String jarName = url.getPath().substring(5,
                        url.getPath().indexOf("!"));
                if (!result.contains(jarName)) {
                    result += File.pathSeparator + jarName;
                }
            }
        }

        // chatellier : mais sous eclipse par exemple, on a besoin du classpath
        // on test que le class path n'est pas un jar et qu'il n'apparait
        // pas deja :
        String systemClassPath = System.getProperty("java.class.path");
        String[] systemClassPathes = systemClassPath.split(File.pathSeparator);
        for (String path : systemClassPathes) {
            String absolutePath = new File(path).getCanonicalPath();
            if (!result.contains(absolutePath)) {
                result += File.pathSeparator + absolutePath;
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("CLASSPATH : " + result);
        }

        return result;
    }

    /**
     * Return full classpath (for compilation or javadoc) as string.
     * Separated by {@link File#pathSeparator}.
     * <p>
     * Add :
     * <ul>
     *  <li>System.getProperty("java.class.path")</li>
     *  <li>All first jar dependency (META-INF/MANIFEST.MF)</li>
     * </ul>
     *
     * @param classpath initial classpath
     * @return classpath as string
     * @throws IOException
     */
    public static Iterable<File> getClassPathAsFiles(List<File> classpath) throws IOException {
        Set<File> result = new HashSet<>(classpath);

        // Ajout des jars
        for (Enumeration<?> e = CompileHelper.class.getClassLoader().getResources("META-INF/MANIFEST.MF"); e.hasMoreElements(); ) {
            URL url = (URL) e.nextElement();
            if (log.isDebugEnabled()) {
                log.debug("Found manifest : " + url);
            }
            if (url != null && url.getFile().startsWith("file:/")) {
                String jarName = url.getPath().substring(5, url.getPath().indexOf("!"));
                // FIXME not working on windows
                // fix it on jdk9+
                String path = Paths.get(jarName).toString();
                result.add(new File(path));
            }
        }

        // chatellier : mais sous eclipse par exemple, on a besoin du classpath
        // on test que le class path n'est pas un jar et qu'il n'apparait
        // pas deja :
        String systemClassPath = System.getProperty("java.class.path");
        String[] systemClassPathes = systemClassPath.split(File.pathSeparator);
        for (String path : systemClassPathes) {
            result.add(new File(path));
        }

        if (log.isDebugEnabled()) {
            log.debug("CLASSPATH : " + result);
        }

        return result;
    }

    /**
     * Surcharge du formatter pour générer des vrais liens pour les {@code @link}.
     */
    private static final CommentFormatter formatter = new CommentFormatter() {
        @Override
        public String format(Comment comment) {
            if (comment == null) {
                return "";
            } else {
                ToHtmlStringCommentVisitor visitor = new ToHtmlStringCommentVisitor() {
                    @Override
                    public void inlineLink(Link link) {
                        this.buf.append(createHREF(link.getReferencedClassName()));
                    }
                };
                comment.visit(visitor);
                return visitor.build();
            }
        }
    };

    /**
     * Extract documentation from interface (Equation).
     *
     * @param category
     * @param name
     * @param javaInterface
     * @return doc
     */
    public static String extractDoc(String category, String name, Class<?> javaInterface) {
        Method[] methods = javaInterface.getDeclaredMethods();
        Method interfaceMethod = methods[0];
        MethodJavadoc methodJavadoc = RuntimeJavadoc.getJavadoc(interfaceMethod);
        List<ParamJavadoc> paramsJavadoc = methodJavadoc.getParams();

        String content = "<div class=\"equation-name\"> Equation : "
                + createHREF(javaInterface.getName(), category) + " - " + name
                + "</div>";
        content += "<p>" + methodJavadoc.getComment() + "</p>";
        content += "<p class=\"heading\">" + t("isisfish.editor.parameters") + " :</p>";

        Args args = interfaceMethod.getAnnotation(Args.class);
        String[] names = args.value();
        String[] stringTypes = new String[names.length];
        Type[] types = interfaceMethod.getGenericParameterTypes();
        for (int i = 0; i < types.length; i++) {
            stringTypes[i] = types[i].getTypeName();
        }

        content += "<table class=\"params-table\">";
        content += """
                <tr>
                    <th scope="col">%s</th>
                    <th scope="col">%s</th>
                    <th scope="col">%s</th>
                </tr>
                """.formatted(t("isisfish.editor.parametersname"),
                t("isisfish.editor.parameterstype"),
                t("isisfish.editor.parametersdescription")
        );
        for (int i = 0; i < names.length; i++) {
            String paramName = names[i];
            content += """
                    <tr>
                        <td>%s</td>
                        <td>%s</td>
                        <td>%s</td>
                    </tr>
                    """.formatted(paramName, createHREF(stringTypes[i]), getParamJavadoc(paramsJavadoc, paramName));
        }
        content += "</table>";

        content += "<p class=\"heading\">" + t("isisfish.editor.returnvalue") + " :</p> ";
        content += "<p>" + formatter.format(methodJavadoc.getReturns()) + "</p>";

        try {
            Field defaultContent = javaInterface.getField("DEFAULT_CONTENT");
            content += "<p class=\"heading\">" + t("isisfish.editor.defaultcontent") + " :</p> ";
            content += "<p class=\"default-content\">" + defaultContent.get(null) + "</p>";
        } catch (NoSuchFieldException e) {
            log.info("%s has no DEFAULT_CONTENT field".formatted(javaInterface));
        } catch (IllegalAccessException e) {
            log.info("Can't access DEFAULT_CONTENT value for %s".formatted(javaInterface));
        }

        return content;
    }

    private static String getParamJavadoc(List<ParamJavadoc> paramsJavadoc, String paramName) {
        String paramJavadoc = paramsJavadoc.stream()
                .filter(param -> param.getName().equals(paramName))
                .findAny()
                .map(e -> formatter.format(e.getComment()))
                .orElse("");
        return paramJavadoc;
    }

    protected static Pattern patternGen1 = Pattern.compile("^([\\w\\.]+)<(.*)>$");

    /**
     * Create a html link to isis javadoc.
     *
     * @param type  class type
     * @param texts link display text
     * @return
     */
    protected static String createHREF(String type, String... texts) {

        // si pas de text, on affiche le type
        String text = type;
        if (texts.length > 0) {
            text = texts[0];
        }

        String result;
        Matcher matcher = patternGen1.matcher(type);
        if (matcher.matches()) {
            String mainType = matcher.group(1);
            String generics = matcher.group(2);
            StringJoiner joiner = new StringJoiner(", ", StringEscapeUtils.escapeHtml4("<"), StringEscapeUtils.escapeHtml4(">"));
            Stream.of(StringUtil.split(new Character[]{'<'}, new Character[]{'>'}, generics, ", ")).map(CompileHelper::createHREF).forEach(joiner::add);
            result = createHREF(mainType) + joiner.toString();
        } else {

            // n'affiche pas de lien pour les types
            // genre "double" qui ne sont pas dans la javadoc d'isisfish
            result = StringEscapeUtils.escapeHtml4(text);

            String url = null;
            if (type.startsWith(IsisFish.class.getPackage().getName())) {
                url = IsisFish.config.getJavadocIsisURL();
            } else if (type.startsWith(MatrixND.class.getPackage().getName())) {
                url = IsisFish.config.getJavadocMatrixURL();
            } else if (type.startsWith(TopiaContext.class.getPackage().getName())) {
                url = IsisFish.config.getJavadocTopiaURL();
            } else if (StringUtils.startsWithAny(type, "java.", "javax.")) {
                url = IsisFish.config.getJavadocJavaURL();
            }

            if (url != null) {
                String ref = url + type.replaceAll("\\.", "/") + ".html";
                String smallType = result.indexOf('.') > 0 ? StringUtils.substringAfterLast(result, ".") : result;
                result = "<a href='" + ref + "'>" + smallType + "</a>";
            }
        }
        return result;
    }
}
