/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2019 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.util;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Used by user to add informations about a code he wrote.
 * 
 * @deprecated since 4.5.0.0, for removal
 */
@Retention(value = RetentionPolicy.RUNTIME)
@Target(value = { ElementType.FIELD })
@Deprecated
public @interface Doc {

    /**
     * Get content of the documentation.
     * 
     * @return the content of the documentation
     */
    String value();

    /**
     * Get documentation date (optional).
     * 
     * @return optional date
     */
    String date() default "";

    /**
     * Get documentation version (optional).
     * 
     * @return optional version
     */
    String version() default "";

    /**
     * Get documentation author (optional).
     * 
     * @return optional author of the documentation
     */
    String author() default "";
}
