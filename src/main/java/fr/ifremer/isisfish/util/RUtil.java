/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2024 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.JavaVersion;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Utility class fo R integration with isis fish.
 */
public class RUtil {

    private static final Log log = LogFactory.getLog(RUtil.class);

    public static final String R_TYPE_PROPERTY = "R.type";

    public static final String JRI_PATH_FILE = "isisfish.jri";

    /**
     * Try to call "R" process to get location where jri is installed.
     */
    public static void initJri() {

        // set Rtype in jni mode
        System.setProperty(R_TYPE_PROPERTY, "jni");

    }

    /**
     * Parse valid Rscript output to find if rJava is installed and where it is installed.
     * 
     * @param is process output
     * @throws IOException 
     */
    public static String parseRscriptOutput(InputStream is) throws IOException {
        List<String> output = IOUtils.readLines(is, StandardCharsets.UTF_8);
        String location = null;

        // 1 lignes = commande OK, mais rJava n'est pas installé
        // 2 lignes = commande OK et rJava est installé
        if (output.size() == 1) {
            if (log.isWarnEnabled()) {
                log.warn("Can't locate rJava package using Rscript (probably not installed)");
            }
        } else if (output.size() == 2) {
            // get install location without ""
            String repository = output.get(1).trim();
            repository = StringUtils.removeStart(repository, "\"");
            repository = StringUtils.removeEnd(repository, "\"");
            // build full library path
            repository = repository.replace('/', File.separatorChar);
            location = getArchLibraryPath(repository);
        } else if (log.isErrorEnabled()) {
            log.error("Can't analyze Rscript output. was: ");
            for (String line : output) {
                log.error(line);
            }
        }

        return location;
    }

    /**
     * On some platform, rJava dll are installed into a sub directory depending on system arch.
     *
     * @param libraryRepository library base repository
     * @return rJava lib path depending on arch
     */
    protected static String getArchLibraryPath(String libraryRepository) {
        String archLibraryPath =  libraryRepository + File.separator + "rJava" + File.separator + "jri";

        String arch = SystemUtils.OS_ARCH;
        if ("x86_64".equals(arch) || "amd64".equals(arch)) {
            String x64LibraryPath = archLibraryPath + File.separator + "x64";
            if (new File(x64LibraryPath).isDirectory()) {
                archLibraryPath = x64LibraryPath;
            }
        } else {
            String i386LibraryPath = archLibraryPath + File.separator + "i386";
            if (new File(i386LibraryPath).isDirectory()) {
                archLibraryPath = i386LibraryPath;
            }
        }

        return archLibraryPath;
    }
}
