package fr.ifremer.isisfish.util.ftp;

/*-
 * #%L
 * ISIS-Fish
 * %%
 * Copyright (C) 1999 - 2021 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

import java.io.IOException;

/**
 * It's seams that it's not possible to use mutiple ftp connection to the same server. So this manager
 * will handle only one connection for all application.
 */
public class FtpManager {

    private static final Log log = LogFactory.getLog(FtpManager.class);

    protected static FtpManager instance;

    /**
     * L'identifiant de la session ouverte.
     *
     * Forme : login:password@host:port
     *
     * Si l'identifiant change, la précédente session est fermé et une autre et réouverte.
     */
    protected String currentKey;

    protected FTPClient currentSession;

    public static FtpManager getInstance() {
        if (instance == null) {
            instance = new FtpManager();
        }
        return instance;
    }

    public synchronized FTPClient getFtpClient(String host, int port, String login, String password) throws IOException {
        FTPClient result;
        String key = String.format("%s:%s@%s:%d", login, password, host, port);

        if (currentSession != null && !currentKey.equals(key)) {
            closeFtpClient();
        }

        if (key.equals(currentKey)) {
            result = currentSession;
        } else {
            result = currentSession = openFtpClient(host, port, login, password);
            currentKey = key;
        }

        return result;
    }

    protected FTPClient openFtpClient(String host, int port, String login, String password) throws IOException {
        FTPClient ftp = new FTPClient();
        ftp.setConnectTimeout(5000);

        if (log.isInfoEnabled()) {
            log.info(String.format("Try to connect on %s:%d...", host, port));
        }

        ftp.connect(host, port);
        int reply = ftp.getReplyCode();
        if (FTPReply.isPositiveCompletion(reply)) {
            if (log.isInfoEnabled()) {
                log.info(String.format("Connected to %s:%d", host, port));
            }
        } else {
            throw new IOException("can't connect");
        }

        boolean logged = ftp.login(login, password);
        if (logged) {
            if (log.isInfoEnabled()) {
                log.info(String.format("Logged in with %s", login));
            }
        } else {
            throw new IOException("can't login");
        }

        // https://stackoverflow.com/a/14035711
        ftp.enterLocalPassiveMode();

        // otherwize, end line are moved to \r\n
        ftp.setFileType(FTP.BINARY_FILE_TYPE);

        ftp.setDefaultTimeout(10000);
        ftp.setSoTimeout(10000);
        ftp.setDataTimeout(10000);
        ftp.setControlKeepAliveTimeout(30);
        ftp.setControlKeepAliveReplyTimeout(10000);

        return ftp;
    }

    public synchronized void closeFtpClient() throws IOException {
        currentSession.disconnect();
        currentSession = null;
        currentKey = null;
    }

    public static long getFileSize(FTPClient ftpClient, String remotePath) throws IOException {
        long size = -1;

        int replyCode = ftpClient.sendCommand("SIZE", remotePath);
        if (FTPReply.isPositiveCompletion(replyCode)) {
            // reply is : 213 123456789\r\n
            String reply = ftpClient.getReplyString();
            try {
                String sizeStr = StringUtils.substringAfter(reply, " ").replaceAll("\r\n", "");
                size = Long.parseLong(sizeStr);
            } catch (NumberFormatException ex) {
                // ignored
            }
        }

        return size;
    }
}
