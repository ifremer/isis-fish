/*-
 * #%L
 * ISIS-Fish
 * %%
 * Copyright (C) 2020 - 2021 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.util.ftp;

import fr.ifremer.isisfish.simulator.SimulationControl;

public class ControlCopyStreamListener extends CopyStreamListener {

    protected SimulationControl simulationControl;

    public ControlCopyStreamListener(SimulationControl simulationControl, long size) {
        this.simulationControl = simulationControl;
        simulationControl.setProgressMax(size);
    }

    @Override
    public void bytesTransferred(long totalBytesTransferred, int bytesTransferred, long streamSize) {
        simulationControl.setProgress(totalBytesTransferred);
    }
}
