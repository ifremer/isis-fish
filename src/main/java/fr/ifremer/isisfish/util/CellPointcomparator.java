/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2005 - 2010 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.util;

import java.awt.geom.Point2D;
import java.util.Comparator;

import fr.ifremer.isisfish.entities.Cell;

/**
 * Permet de comparer la position d'une Maille avec un objet Point2D, ou deux
 * Mailles, ou deux Points.
 * 
 * Created: 6 septembre 2005 00:57:33 CEST
 * 
 * @author Benjamin POUSSIN &lt;poussin@codelutin.com&gt;
 * 
 * @version $Revision$
 * 
 * Last update: $Date$ by : $Author$
 */
public class CellPointcomparator implements Comparator<Object> { // CellPointcomparator

    public int compare(Object o1, Object o2) {
        if (o1 == null && o2 == null) {
            return 0;
        }
        if (o1 == null) {
            throw new IllegalArgumentException("if o1 is null, o2 must be null");
        }
        if (o2 == null) {
            throw new IllegalArgumentException("if o2 is null, o1 must be null");
        }
        if (!(o1 instanceof Cell || o1 instanceof Point2D)) {
            throw new IllegalArgumentException("o1 is not Cell or Point2D: "
                    + o1.getClass().getName());
        }
        if (!(o2 instanceof Cell || o2 instanceof Point2D)) {
            throw new IllegalArgumentException("o2 is not Cell or Point2D: "
                    + o2.getClass().getName());
        }

        float lat1;
        float lat2;
        float lon1;
        float lon2;

        if (o1 instanceof Cell) {
            Cell cell = (Cell) o1;
            lat1 = cell.getLatitude();
            lon1 = cell.getLongitude();
        } else {
            Point2D point = (Point2D) o1;
            lat1 = (float) point.getX();
            lon1 = (float) point.getY();
        }

        if (o2 instanceof Cell) {
            Cell cell = (Cell) o2;
            lat2 = cell.getLatitude();
            lon2 = cell.getLongitude();
        } else {
            Point2D point = (Point2D) o2;
            lat2 = (float) point.getX();
            lon2 = (float) point.getY();
        }
        // o1 est superieur a o2
        int result = Double.compare(lat1, lat2);
        if (result == 0) {
            result = Double.compare(lon1, lon2);
        }

        return result;
    }

} // CellPointcomparator
