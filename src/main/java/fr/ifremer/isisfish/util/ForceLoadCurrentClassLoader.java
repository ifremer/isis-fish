package fr.ifremer.isisfish.util;

/*-
 * #%L
 * ISIS-Fish
 * %%
 * Copyright (C) 2019 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.isisfish.IsisFishRuntimeException;

import java.net.URL;
import java.net.URLClassLoader;

/**
 * Dans le cas où une classe a été modifiée et potentiellement déjà chargée, on utilise ce classloader pour forcer
 * le rechargement de la classe.
 *
 * Utile en particulier pour les equations qui peuvent changer pendant l'execution.
 */
public class ForceLoadCurrentClassLoader extends URLClassLoader {

    public ForceLoadCurrentClassLoader(URL[] urls, ClassLoader parent) {
        super(urls, parent);
    }

    public void forceReloadClass(String className) {
        try {
            findClass(className);
        } catch (ClassNotFoundException ex) {
            throw new IsisFishRuntimeException("Can't force reload class " + className, ex);
        }
    }
}
