/*
 * #%L
 * IsisFish
 *
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2020 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.util.exec;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ExecHelper {

    public static ExecResult exec(String command, String... args) throws IOException, InterruptedException {
        // build full args list
        List<String> cmd = new ArrayList<>(args.length + 1);
        cmd.add(command);
        cmd.addAll(Arrays.asList(args));

        // call
        ExecResult result = new ExecResult();
        ProcessBuilder processBuilder = new ProcessBuilder(cmd);
        long before = System.nanoTime();
        Process process = processBuilder.start();
        ExecMonitor.registerProcess(process);

        // manage output
        String output = IOUtils.toString(process.getInputStream(), StandardCharsets.UTF_8);
        result.setOutput(output);
        String error = IOUtils.toString(process.getErrorStream(), StandardCharsets.UTF_8);
        result.setError(error);

        // wait end
        int status = process.waitFor();
        long after = System.nanoTime();
        result.setReturnCode(status);
        result.setTime((after - before) / 1000000);

        return result;
    }
}
