/*
 * #%L
 * IsisFish
 *
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2020 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.util.exec;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.WeakHashMap;

/**
 * Monitor that can kill all started and not yet exited sub process.
 */
public class ExecMonitor {

    private static final Log log = LogFactory.getLog(ExecMonitor.class);

    protected static ExecMonitor instance;

    protected Set<Process> processes = Collections.newSetFromMap(new WeakHashMap<>());

    public static ExecMonitor getInstance() {
        if (instance == null) {
            instance = new ExecMonitor();
        }
        return instance;
    }

    public static void registerProcess(Process process) {
        getInstance().processes.add(process);
    }

    public void killAllProcess() {
        Set<Process> remainProcess = new HashSet<>(processes);
        for (Process process : remainProcess) {
            if (log.isInfoEnabled()) {
                log.info("Killing process " + process);
            }
            process.destroyForcibly();
        }
    }
}
