/*
 * #%L
 * IsisFish
 *
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2019 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.util;

import fr.ifremer.isisfish.IsisFish;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.FileUtil;

import javax.swing.JFileChooser;
import java.awt.Component;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * Isis util class related to file.
 */
public class IsisFileUtil {

    /** Logger for this class. */
    private static Log log = LogFactory.getLog(IsisFileUtil.class);

    @Deprecated
    protected static File currentDirectory = new File(".");

    /**
     * Retourne le nom du fichier entre dans la boite de dialogue.
     * Si le bouton annuler est utilisé, ou qu'il y a une erreur retourne null.
     *
     * @param patternOrDescriptionFilters les filtres a utiliser, les chaines doivent etre données
     *                                    par deux, le pattern du filtre + la description du filtre
     * @return le fichier accepté, ou null si rien n'est chois ou l'utilisateur a annulé
     * @see #getFile(javax.swing.filechooser.FileFilter...)
     */
    @Deprecated
    public static File getFile(String... patternOrDescriptionFilters) {
        File result;
        result = getFile(null, patternOrDescriptionFilters);
        return result;
    }

    /**
     * Retourne le nom du fichier entre dans la boite de dialogue.
     * Si le bouton annuler est utilisé, ou qu'il y a une erreur retourne null.
     *
     * @param filters les filtres a ajouter
     * @return le fichier accepté, ou null si rien n'est chois ou l'utilisateur a annulé
     */
    @Deprecated
    public static File getFile(javax.swing.filechooser.FileFilter... filters) {
        File result = getFile(null, filters);
        return result;
    }

    /**
     * Retourne le nom du fichier entre dans la boite de dialogue.
     * Si le bouton annuler est utilisé, ou qu'il y a une erreur retourne null.
     *
     * @param parent                      le component parent du dialog
     * @param patternOrDescriptionFilters les filtres a utiliser, les chaines doivent etre données
     *                                    par deux, le pattern du filtre + la description du filtre
     * @return le fichier accepté, ou null si rien n'est chois ou l'utilisateur a annulé
     * @see #getFile(javax.swing.filechooser.FileFilter...)
     */
    @Deprecated
    public static File getFile(Component parent,
                               String... patternOrDescriptionFilters) {
        File result;
        result = getFile("Ok", "Ok", parent, patternOrDescriptionFilters);
        return result;
    }

    /**
     * Retourne le nom du fichier entre dans la boite de dialogue.
     * Si le bouton annuler est utilisé, ou qu'il y a une erreur retourne null.
     *
     * @param title                       le titre de la boite de dialogue
     * @param approvalText                le label du boutton d'acceptation
     * @param parent                      le component parent du dialog
     * @param patternOrDescriptionFilters les filtres a utiliser, les chaines doivent etre données
     *                                    par deux, le pattern du filtre + la description du filtre
     * @return le fichier accepté, ou null si rien n'est chois ou l'utilisateur a annulé
     * @see #getFile(javax.swing.filechooser.FileFilter...)
     */
    @Deprecated
    public static File getFile(String title,
                               String approvalText,
                               Component parent,
                               String... patternOrDescriptionFilters) {

        if (patternOrDescriptionFilters.length % 2 != 0) {
            throw new IllegalArgumentException(
                    "Arguments must be (pattern, description) couple");
        }
        javax.swing.filechooser.FileFilter[] filters =
                new javax.swing.filechooser.FileFilter[
                        patternOrDescriptionFilters.length / 2];
        for (int i = 0; i < filters.length; i++) {
            String pattern = patternOrDescriptionFilters[i * 2];
            String description = patternOrDescriptionFilters[i * 2 + 1];
            filters[i] = new PatternChooserFilter(pattern, description);
        }
        File result;
        result = getFile(title, approvalText, parent, filters);
        return result;
    }

    @Deprecated
    public static class PatternChooserFilter extends javax.swing.filechooser.FileFilter {
        protected String pattern;

        protected String description;

        public PatternChooserFilter(String pattern, String description) {
            this.pattern = pattern;
            this.description = description;
        }

        @Override
        public boolean accept(File f) {
            return f.isDirectory() || f.getAbsolutePath().matches(pattern);
        }

        @Override
        public String getDescription() {
            return description;
        }

    }


    /**
     * Retourne le nom du fichier entre dans la boite de dialogue.
     * Si le bouton annuler est utilisé, ou qu'il y a une erreur retourne null.
     *
     * @param parent  le component parent du dialog
     * @param filters les filtres a ajouter
     * @return le fichier accepté, ou null si rien n'est chois ou l'utilisateur a annulé
     */
    @Deprecated
    public static File getFile(Component parent,
                               javax.swing.filechooser.FileFilter... filters) {
        File result = getFile("Ok", "Ok", parent, filters);
        return result;
    }

    /**
     * Retourne le nom du fichier entre dans la boite de dialogue.
     * Si le bouton annuler est utilisé, ou qu'il y a une erreur retourne null.
     *
     * @param title        le titre de la boite de dialogue
     * @param approvalText le label du boutton d'acceptation
     * @param parent       le component parent du dialog
     * @param filters      les filtres a ajouter
     * @return le fichier accepté, ou null si rien n'est chois ou l'utilisateur a annulé
     */
    @Deprecated
    public static File getFile(String title,
                               String approvalText,
                               Component parent,
                               javax.swing.filechooser.FileFilter... filters) {
        try {
            JFileChooser chooser = new JFileChooser(currentDirectory);

            chooser.setDialogType(JFileChooser.CUSTOM_DIALOG);
            if (filters.length > 0) {
                if (filters.length == 1) {
                    chooser.setFileFilter(filters[0]);
                } else {
                    for (javax.swing.filechooser.FileFilter filter : filters) {
                        chooser.addChoosableFileFilter(filter);
                    }
                }
            }
            chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            chooser.setDialogTitle(title);
            int returnVal = chooser.showDialog(parent, approvalText);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File theFile = chooser.getSelectedFile();
                if (theFile != null) {
                    currentDirectory = theFile;
                    return theFile.getAbsoluteFile();
                }
            }
        } catch (Exception eee) {
            log.warn("Erreur:", eee);
        }
        return null;
    }

    /**
     * Permet de donner une representation fichier pour une chaine de caractere.
     * Le fichier sera automatiquement effacé à la fin de la JVM.
     *
     * @param content    le contenu du fichier temporaire
     * @param fileSuffix l'extension du fichier créé
     * @return le fichier qui contient content
     * @throws IOException if any io pb
     */
    public static File getTempFile(String content, String fileSuffix) throws IOException {
        File result = createTempFile("tmp-" + IsisFileUtil.class.getName(), fileSuffix);
        result.deleteOnExit();
        FileUtils.write(result, content, StandardCharsets.UTF_8);
        return result;
    }

    public static File createTempFile(String name, String ext) throws IOException {
        File tempFile = File.createTempFile(name, ext, IsisFish.config.getCurrentTempDirectory());
        return tempFile;
    }

    public static File createTempDirectory(String name, String ext) throws IOException {
        File tempDir = FileUtil.createTempDirectory(name, ext, IsisFish.config.getCurrentTempDirectory());
        return tempDir;
    }

    public static File addExtensionIfNeeded(File file, String extension) {
        // suffix
        String suffix = extension;
        if (!suffix.startsWith(".")) {
            suffix = "." + suffix;
        }

        File result = file;
        if (!StringUtils.endsWithIgnoreCase(result.getName(), suffix)) {
            file = new File(file.getAbsolutePath() + suffix);
        }

        return file;
    }
}
