package fr.ifremer.isisfish.util;

/*-
 * #%L
 * ISIS-Fish
 * %%
 * Copyright (C) 2020 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.isisfish.entities.Season;
import fr.ifremer.isisfish.types.Month;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * IsisFish util class for domain related method.
 */
public class IsisUtil {

    public static boolean isSeasonOverlap(Collection<? extends Season> seasons) {
        boolean result = false;
        Set<Month> months = new HashSet<>();
        if (seasons != null) {
            for (Season info : seasons) {
                if (!Collections.disjoint(months, info.getMonths())) {
                    result = true;
                }
                months.addAll(info.getMonths());
            }
        }
        return result;
    }
}
