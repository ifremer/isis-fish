/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2012 Ifremer, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.util;

import org.apache.commons.lang3.StringUtils;

/**
 * Quelques methodes pour manipuler les bits d'un long. Ces methodes servent pour
 * creer des marques pour les resultats mapper
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class BitUtil {

    /**
     * Converti une chaine de maximum 8 carateres en un long representant cette
     * chaine en ascii.
     * @param s
     * @return
     */
    public static long toMark(String s) {
        long result = 0;
        s = StringUtils.rightPad(s, 8, ' ');
        s = StringUtils.substring(s, 0, 8);
        for (int i=0; i<8; i++) {
            long b = s.charAt(i);
            // le 1er caractere doit etre le plus a gauche
            // un long est sur 64 bit, on decale de 8 en 8 (byte)
            result = result | (b << (64-(i+1)*8));
        }
        return result;
    }

    /**
     * Convertie un long en sa representation String. Le long est en fait
     * l'aggregation de 8 char (8x8bits). Les chars sont dans la table ascii
     * @param l
     * @return
     */
    public static String fromMark(long l) {
        long m = 0x00000000000000FF; // mask 255
        String result = "";
        for (int i=0; i<8; i++) {   // on parcours chaque byte du long
            long t = l >> 8*i;      // on decale le caratere a lire a droite
            char v = (char)(t & m); // on applique le mask pour ne garder que ce caratere
            result = v + result;
        }
        return result;
    }

    /**
     * Montre les bits d'une nombre entier
     * @param l
     * @return
     */
    public static String showBit(long l) {
        long m = 0x0000000000000001;
        String result = "";
        for (int i=0; i<64; i++) {
            long t = l >> i;
            long v = t & m;
            result = v + result;
            if (i!= 0 && i%8 == 0) {
                result = " " + result;
            }
        }
        return result;
    }

}
