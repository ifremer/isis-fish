/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2005 - 2024 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish;

import com.bbn.openmap.MapBean;
import fr.ifremer.isisfish.config.IsisConfig;
import fr.ifremer.isisfish.config.Option;
import fr.ifremer.isisfish.cron.CronService;
import fr.ifremer.isisfish.datastore.ExportStorage;
import fr.ifremer.isisfish.datastore.FormuleStorage;
import fr.ifremer.isisfish.datastore.ObjectiveStorage;
import fr.ifremer.isisfish.datastore.OptimizationStorage;
import fr.ifremer.isisfish.datastore.RegionStorage;
import fr.ifremer.isisfish.datastore.ResultInfoStorage;
import fr.ifremer.isisfish.datastore.RuleStorage;
import fr.ifremer.isisfish.datastore.ScriptStorage;
import fr.ifremer.isisfish.datastore.SensitivityAnalysisStorage;
import fr.ifremer.isisfish.datastore.SensitivityExportStorage;
import fr.ifremer.isisfish.datastore.SimulationPlanStorage;
import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.datastore.SimulatorStorage;
import fr.ifremer.isisfish.simulator.launcher.SimulationMonitor;
import fr.ifremer.isisfish.simulator.launcher.SimulationService;
import fr.ifremer.isisfish.ui.WelcomeContext;
import fr.ifremer.isisfish.ui.WelcomeTabUI;
import fr.ifremer.isisfish.ui.WelcomeUI;
import fr.ifremer.isisfish.ui.widget.SwingSession;
import fr.ifremer.isisfish.util.ErrorHelper;
import fr.ifremer.isisfish.util.RUtil;
import fr.ifremer.isisfish.util.UIUtil;
import fr.ifremer.isisfish.util.cache.IsisCache;
import fr.ifremer.isisfish.util.matrix.IsisMatrixSemanticMapper;
import fr.ifremer.isisfish.vcs.VCS;
import fr.ifremer.isisfish.vcs.VCSActionEvent;
import fr.ifremer.isisfish.vcs.VCSException;
import fr.ifremer.isisfish.vcs.VCSFactory;
import fr.ifremer.isisfish.vcs.VetoableActionListener;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.AbstractFileFilter;
import org.apache.commons.lang3.JavaVersion;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.i18n.I18n;
import org.nuiton.i18n.init.ClassPathI18nInitializer;
import org.nuiton.i18n.init.DefaultI18nInitializer;
import org.nuiton.math.matrix.MatrixFactory;
import org.nuiton.topia.TopiaException;
import org.nuiton.version.Version;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * This is the main class of <code>IsisFish</code> application.
 * 
 * Created: 1 aout 2005 18:37:25 CEST
 *
 * @author Benjamin POUSSIN &lt;poussin@codelutin.com&gt;
 * @author chemit
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class IsisFish { // IsisFish

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(IsisFish.class);

    static public IsisConfig config = null;

    static public VCS vcs = null;
    
    static public VCS communityVcs = null;

    /**
     * ask for application quit
     */
    static public void quit() {
        System.exit(0);
    }

    public static void main(String... args) throws Exception {

        // initialisation de l'application
        init(args);

        // i18n is not inited here
        if (log.isInfoEnabled()) {
            log.info("Starting Isis-Fish " + IsisConfig.getVersion() + " with args : " + Arrays.toString(args));
            log.info("Date: " + SimpleDateFormat.getInstance().format(new Date()));
            log.info("Java version: " + SystemUtils.JAVA_RUNTIME_VERSION + ", " + SystemUtils.JAVA_VM_NAME);
            log.info("Platform/arch: " + SystemUtils.OS_NAME + "/" + SystemUtils.OS_VERSION + "/" + SystemUtils.OS_ARCH);
            log.info("java.library.path: " + SystemUtils.JAVA_LIBRARY_PATH);
            log.info("PATH: " + SystemUtils.getEnvironmentVariable("PATH", "n/a"));
        }

        // init R way for isis
        RUtil.initJri();

        // action after init
        config.doAction(IsisConfig.STEP_AFTER_INIT);

        // initVCS ask for passphrase, ui must be set before
        initLookAndFeel();

        // static vcs init (needed for some actions)
        try {
            initVCS();
            initCommunityVCS();
            checkDuplicatedFiles();
        } catch (Exception eee) {
            log.warn(t("Error during vcs initialisation"), eee);
        }

        // nuiton matrix semantics mapper
        MatrixFactory.setSemanticMapper(new IsisMatrixSemanticMapper());

        if (log.isInfoEnabled()) {
            log.info(t("isisfish.launching", config.getElapsedTimeAsString()));
        }

        // after init vcs and local data
        config.doAction(IsisConfig.STEP_AFTER_INIT_VCS);

        launchUI();

        // action after ui launched
        config.doAction(IsisConfig.STEP_AFTER_UI);

        startCronService();
    }

    /**
     * Start cron service (if enabled).
     */
    protected static void startCronService() {
        if (config.isPerformCron()) {
            // start cron service
            CronService cronService = new CronService();
            cronService.start();
        }
    }

    /**
     * Install "Nimbus" LookAndFeel if available.
     * 
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws UnsupportedLookAndFeelException
     */
    private static void initLookAndFeel() throws InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
        // must be done only if ui is activated (bug on java 7)
        if (config.isLaunchUI()) {
            for (UIManager.LookAndFeelInfo laf : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(laf.getName())) {
                    try {
                        UIManager.setLookAndFeel(laf.getClassName());
                    } catch (ClassNotFoundException e) {
                        // could not fin nimbus look-and-feel
                        log.warn("Can't install nimbus");
                    }
                }
            }
        }
    }

    /**
     * All main in other class must call this method, parse arguments,
     * load configuration file, init language and load converter.
     * 
     * @param args main args
     * @throws Exception if any exception while build configuration
     */
    public static void init(String... args) throws Exception {

        // parsing des options à partir des arguments passés
        config = new IsisConfig();
        config.parse(args);

        // to work in java webstart
        try {
            I18n.init(new DefaultI18nInitializer("isis-fish-i18n"), config.getLocale());
        } catch (Exception ex) {
            // dev mode
            I18n.init(new ClassPathI18nInitializer(), config.getLocale());
        }

        // init cache backend
        IsisCache.defaultFactory = config.getCacheBackendFactoryClass();

        // after init shutdown hook
        Runtime.getRuntime().addShutdownHook(new IsisQuitHook());
        
        if (log.isDebugEnabled()) {
            log.debug(t("isisfish.launch.init.done", config.getElapsedTimeAsString()));
        }
    }

    /**
     * Veto utilise lors de l'init du vcs pour permettre a l'utilisateur de
     * refuser certaine action. S'il refuse les switchs le vcs est passe en 
     * readonly.
     */
    static class VCSActionAsker implements VetoableActionListener {

        public boolean canDoAction(VCS vcs, VCSActionEvent action, File... files) {
            boolean result = true;
            if (action == VCSActionEvent.SWITCH_PROTOCOL) {
                result = ask(t("isisfish.vcs.switchprotocol.confirm"));
                if (!result) {
                    // l'utilisateur ne souhaite pas changer de protocol, 
                    // on force le repo en read-only pour eviter les erreurs
                    vcs.setWriteable(false);
                }
            } else if (action == VCSActionEvent.SWITCH) {
                result = ask(t("isisfish.vcs.switchversion.confirm", IsisConfig.getVersion()));
                if (!result) {
                    // l'utilisateur ne souhaite pas changer de branche,
                    // on force le repo en read-only pour eviter les erreurs
                    vcs.setWriteable(false);
                }
            } else if (action == VCSActionEvent.UPDATE_REPOSITORY) {
                
                // construit une chaine plutot qu'un Arrays.toString() qui
                // est illisible
                String modifiedFiles = "";
                String separator = "";
                for(File file : files) {
                    modifiedFiles += separator + file.toString();
                    separator = "\n";
                }
                
                // FIXME maybe make a JAXX UI ?
                JLabel labelModifiedFiles = new JLabel(t("isisfish.vcs.updaterepository.confirm"));
                JTextArea areaModifiedFiles = new JTextArea(modifiedFiles);
                areaModifiedFiles.setEditable(false);
                areaModifiedFiles.setAutoscrolls(true);
                JScrollPane sp = new JScrollPane(areaModifiedFiles);
                sp.setPreferredSize(new Dimension(500, 100)); // don't remove popup is huge
                result = ask(new Component[] { labelModifiedFiles, sp} );
            }
            return result;
        }
        
    }
    
    /**
     * Permet de faire une demande a l'utilisateur. S'il repond annuler, on 
     * quit l'application
     * 
     * @param msg question to ask
     * @return true if user confirm question
     */
    protected static boolean ask(Object msg) {
        int value = JOptionPane.showConfirmDialog(null, msg);
        if (value == JOptionPane.CANCEL_OPTION) {
            quit();
        }
        boolean result = value == JOptionPane.YES_OPTION;
        return result;
    }

    /**
     * Initialise le VCS et check s'il y a des mises à jour pour
     * prevenir l'utilisateur.
     * 
     * @throws VCSException 
     */
    static public void initVCS() throws VCSException {

        // vcs must be done is ui is enabled too
        if (config.isLaunchUI() && config.isPerformVcsUpdate()) {

            // init vcs
            // in graphical mode, real VCS
            vcs = VCSFactory.createVCS(config);

            VCSActionAsker asker = new VCSActionAsker();
            vcs.addVetoableActionListener(asker);

            // Si le repo local exist mais n'est pas du bon type, on renome ce repertoire
            File local = config.getDatabaseDirectory();
            if (log.isInfoEnabled()) {
                log.info(t("Check state of local repository: %s", local));
            }

            if (local.exists()) {
                if (!vcs.isValidLocalRepository()) {
                    if (log.isInfoEnabled()) {
                        log.info(t("Local repository exists but it's not valide for current vcs: %s",
                                config.getOption(VCS.VCS_TYPE)));
                    }
                    if (ask(t("isisfish.vcs.init.wrongprotocol", local))) {
                        File localBackup = new File(local.getParentFile(),
                                local.getName() + "-" +
                                new SimpleDateFormat("yyyy-mm-dd-HH-mm-ss").format(new java.util.Date()));
                        if (log.isInfoEnabled()) {
                            log.info(t("Rename data directory to %s", localBackup));
                        }
                        if (!local.renameTo(localBackup)) {
                            throw new IsisFishRuntimeException(
                                    "Can't rename local repository that don't use svn");
                        }
                    } else {
                        log.info(t("Switch repository type to none"));
                        config.setOption(Option.VCS_TYPE.key, VCS.TYPE_NONE);
                        config.saveForUser();
                        vcs = VCSFactory.createVCS(config);
                    }
                }
            }

            // Si le repo local n'existe pas on fait un check out complet
            if (!local.exists()) {
                if (log.isInfoEnabled()) {
                    log.info(t("Local repository don't exist"));
                }
                if (!vcs.isConnected()) {
                    ErrorHelper.showErrorDialog(t("isisfish.vcs.init.notfoundcantdownload",
                            IsisConfig.getApiVersion()), null);
                } else {
                    // Si on utilise pas le bon tag on change de tag
                    Version tag = IsisConfig.getApiVersion();
                    if (!vcs.isTag(tag)) {
                        // pas de tag pour cette version, on checkout le trunk
                        tag = null;
                    }

                    // initialise le repo local
                    vcs.checkout(tag, false);

                    // ajoute les repertoires qu'il faut
                    ExportStorage.checkout();
                    FormuleStorage.checkout();
                    ObjectiveStorage.checkout();
                    OptimizationStorage.checkout();
                    RuleStorage.checkout();
                    ResultInfoStorage.checkout();
                    ScriptStorage.checkout();
                    SensitivityAnalysisStorage.checkout();
                    SensitivityExportStorage.checkout();
                    SimulationPlanStorage.checkout();
                    SimulatorStorage.checkout();
                    

                    // on ne prend pas toutes les simu ni toutes les regions
                    vcs.update(new File(local, SimulationStorage.SIMULATION_PATH), false);
                    vcs.update(new File(local, RegionStorage.REGION_PATH), false);
                    try {
                        RegionStorage.checkout("DemoRegion");
                    } catch (TopiaException eee) {
                        log.warn("Can't checkout DemoRegion", eee);
                    }
                }
            }

            if (!local.exists()) {
                // arrive ici le repo devrait exister
                throw new IsisFishRuntimeException("Can't find local repository");
            }

            // on s'arrete la si on est pas connecte
            if (vcs.isConnected()) {

                // cleanup
                vcs.cleanup(null);
                
                // check protocol, user, host
                vcs.checkProtocol();

                // Suivant la version du logiciel et les versions de base disponible
                // il est possiblement obligatoire de ne plus etre sur le trunk, ou
                // de migrer sur un autre tag

                List<File> filesInClonflict;
                
                // si on est sur une branche, on est en developpement, on ne fait donc rien
                if (vcs.getTag().startsWith("/branches")) {
                    log.info(t("Use branches, switch not needed"));
                } else {
                    // Si on utilise pas le bon tag on change de tag
                    Version tag = IsisConfig.getApiVersion();

                    if (vcs.isTag(tag)) {
                        // un tag dispo, on a donc pas la derniere version, on switch
                        filesInClonflict = vcs.setTag(tag);
                    } else {
                        // pas de tag dispo on retourne sur le trunk
                        filesInClonflict = vcs.setTag(null);
                    }
                    
                    // TODO change code is case of conflict
                    // display a beautiful frame to manage each case
                    // for now, display a simple warning message
                    // si refus de l'utilisateur, c'est null aussi
                    if (filesInClonflict != null && !filesInClonflict.isEmpty()) {
                        warnFileListDialog(t("isisfish.error.warning.title"), t("isisfish.vcs.switchtag.warningconflict"), filesInClonflict);
                    }
                }

                // check file status
                // WARNING : this do the real svn update
                filesInClonflict = vcs.checkFileStatus();
                if (filesInClonflict != null && !filesInClonflict.isEmpty()) {
                    warnFileListDialog(t("isisfish.error.warning.title"), t("isisfish.vcs.update.warningconflict"), filesInClonflict);
                }
            }
            
            // fin de l'init on supprime le vetoable du vcs
            vcs.remoteVetoableActionListener(asker);
        }
        else {
            // VCS can't be null
            // set none if ui isn't launched
            config.setOption(VCS.VCS_TYPE, VCS.TYPE_NONE); // to make him happy
            vcs = VCSFactory.createVCS(config);
        }
    }
    
    /**
     * Initialise le VCS et check s'il y a des mises à jour pour
     * prevenir l'utilisateur.
     * 
     * @throws VCSException 
     */
    static public void initCommunityVCS() throws VCSException {

        // vcs must be done is ui is enabled too
        if (config.isLaunchUI() && config.isPerformVcsUpdate()) {
            
            // init vcs
            // in graphical mode, real VCS
            communityVcs = VCSFactory.createPublicVCS(config);

            VCSActionAsker asker = new VCSActionAsker();
            communityVcs.addVetoableActionListener(asker);

            // Si le repo local exist mais n'est pas du bon type, on renome ce repertoire
            File local = config.getCommunityDatabaseDirectory();

            if (local.exists()) {
                if (!communityVcs.isValidLocalRepository()) {
                    if (log.isInfoEnabled()) {
                        log.info(t("Local repository exists but it's not valide for current vcs: %s",
                                config.getOption(VCS.VCS_TYPE)));
                    }
                    if (ask(t("isisfish.vcs.init.wrongprotocol", local))) {
                        File localBackup = new File(local.getParentFile(),
                                local.getName() + "-" +
                                new SimpleDateFormat("yyyy-mm-dd-HH-mm-ss").format(new Date()));
                        if (log.isInfoEnabled()) {
                            log.info(t("Rename data directory to %s", localBackup));
                        }
                        if (!local.renameTo(localBackup)) {
                            throw new IsisFishRuntimeException(
                                    "Can't rename local repository that don't use svn");
                        }
                    } else {
                        log.info(t("Switch repository type to none"));
                        config.setOption(Option.VCS_COMMUNITY_TYPE.key, VCS.TYPE_NONE);
                        config.saveForUser();
                        communityVcs = VCSFactory.createPublicVCS(config);
                    }
                }
            }

            // Si le repo local n'existe pas on fait un check out complet
            if (!local.exists()) {
                if (log.isInfoEnabled()) {
                    log.info(t("Local repository don't exist"));
                }
                if (!communityVcs.isConnected()) {
                    ErrorHelper.showErrorDialog(t("isisfish.vcs.init.notfoundcantdownload",
                            "trunk"), null);
                } else {
                    // initialise le repo local
                    communityVcs.checkout(null, true);
                }
            }

            // on s'arrete la si on est pas connecte
            if (communityVcs.isConnected()) {

                // cleanup
                communityVcs.cleanup(null);

                // check protocol, user, host
                communityVcs.checkProtocol();

                // check file status
                // WARNING : this do the real svn update
                List<File> filesInClonflict = communityVcs.checkFileStatus();
                if (filesInClonflict != null && !filesInClonflict.isEmpty()) {
                    warnFileListDialog(t("isisfish.error.warning.title"), t("isisfish.vcs.update.warningconflict"), filesInClonflict);
                }
            }

            // fin de l'init on supprime le vetoable du vcs
            communityVcs.remoteVetoableActionListener(asker);
        }
        else {
            // VCS can't be null
            // set none if ui isn't launched
            config.setOption(Option.VCS_COMMUNITY_TYPE.key, VCS.TYPE_NONE); // to make him happy
            communityVcs = VCSFactory.createPublicVCS(config);
        }
    }

    /**
     * Look for duplicated file name in official repository and community
     * repository and rename duplicated in community repository.
     */
    protected static void checkDuplicatedFiles() {

        // vcs must be done is ui is enabled too
        if (config.isLaunchUI() && config.isPerformVcsUpdate()) {
            long before = System.currentTimeMillis();

            // get official file list
            Collection<File> offFiles = FileUtils.listFiles(IsisFish.config.getDatabaseDirectory(),
                    new AbstractFileFilter() {
                        @Override
                        public boolean accept(File dir, String name) {
                            return name.endsWith(".java");
                        }
                    },
                    new AbstractFileFilter() {
                        @Override
                        public boolean accept(File dir, String name) {
                            // exclude simulations directory (can take huge time)
                            return !name.equals(SimulationStorage.SIMULATION_PATH);
                        }
                    });

            // compare it with community repo
            for (File offFile : offFiles) {
                File comFile = new File(IsisFish.config.getCommunityDatabaseDirectory()
                        + StringUtils.removeStart(offFile.getAbsolutePath(), IsisFish.config.getDatabaseDirectory().getAbsolutePath()));
                if (comFile.isFile()) {
                    if (log.isWarnEnabled()) {
                        log.warn("Found file collision for " + comFile.getAbsolutePath());
                    }
                    File newFile = new File(comFile.getParentFile(), "Duplicated_" + comFile.getName());
                    comFile.renameTo(newFile);
                }
            }

            if (log.isDebugEnabled()) {
                long after = System.currentTimeMillis();
                log.debug("Check corrumpted file in " + (after - before) + " ms");
            }
        }
    }

    /**
     * Display dialog with files list, and specifique label.
     * 
     * @param dialogTitle dialog title
     * @param labelTitle labelTitle
     * @param conflictFiles conflict files
     */
    protected static void warnFileListDialog(String dialogTitle, String labelTitle, List<File> conflictFiles) {
        // construit une chaine plutot qu'un Arrays.toString() qui
        // est illisible
        String conflictFilesString = "";
        String separator = "";
        for (File file : conflictFiles) {
            conflictFilesString += separator + file.toString();
            separator = "\n";
        }
        
        JLabel labelModifiedFiles = new JLabel(labelTitle);
        JTextArea areaModifiedFiles = new JTextArea(conflictFilesString);
        areaModifiedFiles.setEditable(false);
        areaModifiedFiles.setAutoscrolls(true);
        JScrollPane sp = new JScrollPane(areaModifiedFiles);
        sp.setPreferredSize(new Dimension(500, 100)); // don't remove popup is huge
        
        JOptionPane.showMessageDialog(null, new Component[] { labelModifiedFiles, sp},
                dialogTitle,
                JOptionPane.WARNING_MESSAGE);
    }

    /**
     * Initialise et lance l'interface graphique si elle a demandé a être lancée.
     */
    public static void launchUI() {
        if (config.isLaunchUI()) {

            // recommend java 21 after 30/9/2024 (java 23 release date)
            if (LocalDate.now().isAfter(LocalDate.of(2024, 9, 30)) && SystemUtils.isJavaVersionAtMost(JavaVersion.JAVA_20)) {
                LocalDate now = LocalDate.now();
                LocalDate javaVersionCheckDate = config.getJavaVersionCheckDate();
                if (javaVersionCheckDate == null || ChronoUnit.DAYS.between(javaVersionCheckDate, now) > 30) {
                    JOptionPane.showMessageDialog(null, t("isisfish.main.updateIsisFutureRelease", SystemUtils.JAVA_VERSION,
                        JavaVersion.JAVA_21), t("isisfish.common.isisfish"), JOptionPane.WARNING_MESSAGE);
                    config.setJavaVersionCheckDate(now);
                    config.saveForUser();
                }
            }

            registerWatchers();

            SwingUtilities.invokeLater(() -> {

                // catch wall application exception
                Thread.setDefaultUncaughtExceptionHandler((t, cause) -> {
                    if (cause instanceof LinkageError) {
                        if (log.isFatalEnabled()) {
                            log.fatal("Linkage error detected", cause);
                        }

                        // a real java.lang.Error sometimes happen when
                        // using non recompiled script due to dependency changes
                        // this is the better solution found :
                        // remove isis build directory
                        try {
                            FileUtils.deleteDirectory(IsisFish.config.getCompileDirectory());
                        } catch (IOException ex) {
                            if (log.isErrorEnabled()) {
                                log.error("Can't clear build directory", ex);
                            }
                        }

                        ErrorHelper.showErrorDialog(null, t("isisfish.error.linkageerror.message"), cause);
                    } else {
                        if (log.isErrorEnabled()) {
                            log.error("Global application exception", cause);
                        }

                        // les NumberFormatException sont courantes lors de la saisie
                        // ca va etre relou de toutes les afficher
                        Throwable lastCause = cause;
                        while (lastCause.getCause() != null) {
                            lastCause = lastCause.getCause();
                        }
                        if (!(lastCause instanceof NumberFormatException)) {
                            ErrorHelper.showErrorDialog(null, cause.getMessage(), cause);
                        }
                    }
                });

                // init simulater manager
                SimulationService.getService();

                // init IsisTray
                IsisTray.getInstance();

                // OpenMap sysout
                MapBean.suppressCopyright = true;

                // configure swing session
                File sessionFile = config.getSwingSessionFile();
                final SwingSession session = new SwingSession(sessionFile, true);

                // lauch first UI (welcomeUI)
                WelcomeContext welcomeContext = new WelcomeContext();
                WelcomeUI welcome = new WelcomeUI(welcomeContext);
                welcome.setTitle(t("isisfish.welcome.title", IsisConfig.getVersion()));
                // Set to exit on close
                welcome.setDefaultCloseOperation(WelcomeUI.DO_NOTHING_ON_CLOSE);
                welcome.addWindowListener(new WindowAdapter() {
                    @Override
                    public void windowClosing(WindowEvent e) {
                        WelcomeUI ui = (WelcomeUI)e.getSource();
                        ui.getHandler().close(ui);
                    }

                    @Override
                    public void windowClosed(WindowEvent e) {
                        session.save();
                        quit();
                    }
                });

                // show main isis window
                UIUtil.setIconImage(welcome);
                welcome.setLocationRelativeTo(null);
                session.add(welcome);

                // small fix to set selected tab index always to first
                WelcomeTabUI tabUI = (WelcomeTabUI)welcome.getWelcomePanelUI().getComponent(1);
                tabUI.getSimulUI().getBodyTabbedPane().setSelectedIndex(0);
                tabUI.getSensitivityUI().getBodyTabbedPane().setSelectedIndex(0);
                // never restore iconifed state (wevy boring)
                welcome.setExtendedState(welcome.getExtendedState() & ~WelcomeUI.ICONIFIED);

                // display frame
                welcome.setVisible(true);

                //
                SimulationService service = SimulationService.getService();
                SimulationMonitor.getInstance().reloadConfig(service);
            });
        } else {
            if (log.isInfoEnabled()) {
                log.info(t("isisfish.message.launchui.notlaunch"));
            }
        }
    }

    /**
     * We need to register watchers manually after checkout.
     * Otherwize, directory are created empty by watcher and VCS think database is using a wrong protocole.
     */
    protected static void registerWatchers() {
        ExportStorage.registerWatcher();
        ObjectiveStorage.registerWatcher();
        OptimizationStorage.registerWatcher();
        RuleStorage.registerWatcher();
        ResultInfoStorage.registerWatcher();
        SensitivityAnalysisStorage.registerWatcher();
        SensitivityExportStorage.registerWatcher();
        SimulationPlanStorage.registerWatcher();
        SimulatorStorage.registerWatcher();
    }
    
} // IsisFish
