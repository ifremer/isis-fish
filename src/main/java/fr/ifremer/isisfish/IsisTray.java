/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2007 - 2010 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish;

import static org.nuiton.i18n.I18n.t;
import static org.nuiton.i18n.I18n.n;

import java.awt.AWTException;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.ImageIcon;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.Resource;

import fr.ifremer.isisfish.simulator.SimulationControl;
import fr.ifremer.isisfish.simulator.launcher.SimulationJob;
import fr.ifremer.isisfish.simulator.launcher.SimulationService;
import fr.ifremer.isisfish.simulator.launcher.SimulationServiceListener;

/**
 * Isis tray icon implementation.
 *
 * Created: 6 janv. 07 13:52:25
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public abstract class IsisTray implements SimulationServiceListener,
        PropertyChangeListener {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    protected static final Log log = LogFactory.getLog(IsisTray.class);

    /** Non running icon. */
    protected static final ImageIcon TRAY_IMAGE_STOP = Resource.getIcon("/images/BulbR.gif");
    
    /** Running icon. */
    protected static final ImageIcon TRAY_IMAGE_START = Resource.getIcon("/images/BulbG.gif");

    /** Non running simulation tooltip text. */
    private static final String NO_SIMULATION_TEXT = n("isisfish.tray.simulation.no");

    /**
     * Texte a utiliser pour afficher l'etat de la simulation en cours.
     * 
     * <li>0 = id de simulation
     * <li>1 = pas de temps courant de la simulation
     * <li>2 = nombre total de pas de temps attendu 
     */
    private static final String SIMULATION_TEXT = n("isisfish.tray.simulation");

    /** System tray tooltip when multiple simulation are running. */
    private static final String MULTI_SIMULATION_TEXT = n("isisfish.tray.simulation.multi");

    /** Tray implementation instance. */
    private static IsisTray instance = null;
    
    /**
     * Change image.
     * 
     * @param image new image
     */
    public abstract void setImage(ImageIcon image);

    /**
     * Change tooltip.
     * 
     * @param text new tooltip
     */
    public abstract void setToolTip(String text);

    /**
     * Factory method to get instance.
     * 
     * @return tray implementation
     * @see SystemTray#isSupported()
     */
    public static IsisTray getInstance() {
        if (instance == null) {
            if (AWTSystemTray.isSupported()) {
                instance = new AWTSystemTray();
            } else {
                instance = new DummySystemTray();
            }
            if (log.isInfoEnabled()) {
                log.info("Use system tray: " + instance.getClass().getName());
            }
            instance.setToolTip(t(NO_SIMULATION_TEXT));
            SimulationService.getService().addSimulationServiceListener(
                    instance);
        }
        return instance;
    }

    @Override
    public void simulationStart(SimulationService simService, SimulationJob job) {
        SimulationControl control = job.getItem().getControl();
        control.addPropertyChangeListener(this);
        getInstance().setToolTip(
                t(SIMULATION_TEXT, control.getId(), control.getProgress(),
                        control.getProgressMax()));
        getInstance().setImage(TRAY_IMAGE_START);
    }

    @Override
    public void simulationStop(SimulationService simService, SimulationJob job) {
        SimulationControl control = job.getItem().getControl();
        control.removePropertyChangeListener(this);
        getInstance().setToolTip(t(NO_SIMULATION_TEXT));
        getInstance().setImage(TRAY_IMAGE_STOP);
    }

    @Override
    public void clearJobDone(SimulationService simService) {
        // nothing to do
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        SimulationControl control = (SimulationControl) evt.getSource();
        getInstance().setToolTip(
                t(SIMULATION_TEXT, control.getId(), control.getProgress(),
                        control.getProgressMax()));
    }

    /**
     * Empty system tray implementation.
     * 
     * All method are empty, do nothing, for system without system
     * tray implementation.
     */
    static class DummySystemTray extends IsisTray {

        @Override
        public void setImage(ImageIcon image) {
        }

        @Override
        public void setToolTip(String text) {
        }

    }

    /**
     * AWT system tray implementation.
     *
     * @see SystemTray
     */
    static class AWTSystemTray extends IsisTray {
        
        /** AWT tray icon. */
        protected TrayIcon trayIcon = null;
        
        /**
         * Is swt system try supported.
         * 
         * @return {@code true} if awt systray is supported
         * 
         * @see SystemTray#isSupported()
         */
        public static boolean isSupported() {
            boolean result = SystemTray.isSupported();
            return result;
        }

        @Override
        public void setImage(ImageIcon image) {
            trayIcon.setImage(image.getImage());
        }

        @Override
        public void setToolTip(String text) {
            trayIcon.setToolTip(text);
        }

        public AWTSystemTray() {
            if (SystemTray.isSupported()) {
                // get the SystemTray instance
                SystemTray tray = SystemTray.getSystemTray();

                // construct a TrayIcon
                trayIcon = new java.awt.TrayIcon(TRAY_IMAGE_STOP.getImage(),
                        t(NO_SIMULATION_TEXT), null);
                trayIcon.setImageAutoSize(true);

                // add the tray image
                try {
                    tray.add(trayIcon);
                } catch (AWTException eee) {
                    if (log.isWarnEnabled()) {
                        log.warn(t("isisfish.error.add.tray"), eee);
                    }
                }
                // ...
            } else {
                // disable tray option in your application or
                // perform other actions
                log.info(t("isisfish.message.tray.disabled"));
            }
        }
    }
}
