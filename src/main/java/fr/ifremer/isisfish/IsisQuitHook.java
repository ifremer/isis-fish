/*-
 * #%L
 * ISIS-Fish
 * %%
 * Copyright (C) 2020 - 2021 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish;

import fr.ifremer.isisfish.config.IsisConfig;
import fr.ifremer.isisfish.util.exec.ExecMonitor;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Start daemon that monitor quit value, if value is true, quit isis. Ce
 * mecanisme permet de faire des actions avant de quitter l'application
 */
public class IsisQuitHook extends Thread {

    private static final Log log = LogFactory.getLog(IsisFish.class);

    @Override
    public void run() {
        IsisConfig config = IsisFish.config;

        try {
            config.doAction(IsisConfig.STEP_BEFORE_EXIT);
        } catch (Exception eee) {
            log.info("Error in quit daemon", eee);
        }

        FileUtils.deleteQuietly(config.getCurrentTempDirectory());

        ExecMonitor.getInstance().killAllProcess();
    }
}
