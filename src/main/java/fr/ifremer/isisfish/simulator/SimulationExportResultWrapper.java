/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2007 - 2018 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator;

import fr.ifremer.isisfish.config.IsisConfig;
import fr.ifremer.isisfish.IsisFish;

import static org.nuiton.i18n.I18n.t;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.ifremer.isisfish.IsisFishException;
import fr.ifremer.isisfish.datastore.ExportStorage;
import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.export.Export;
import fr.ifremer.isisfish.export.ExportInfo;
import fr.ifremer.isisfish.export.ExportStep;
import fr.ifremer.isisfish.export.SensitivityExport;
import fr.ifremer.isisfish.types.TimeStep;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.io.output.CountingOutputStream;
import org.apache.commons.lang3.StringUtils;

/**
 * ExportInfo simulation listener.
 * 
 * Do export after simulation ends.
 *
 * Created: 14 nov. 07 00:22:51
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class SimulationExportResultWrapper implements SimulationListener {

    /** Class logger. */
    private static Log log = LogFactory
            .getLog(SimulationExportResultWrapper.class);

    protected TimeStep lastStep;

    private static class ExportContext {
        ExportInfo export;
        File exportDir;
        Writer out;
        CountingOutputStream counter;
        long time = 0;
        long bytes = 0;
        Exception exception = null;

        /**
         * Pour que {@link #out} soit complètement écrit (flush) même si le processus est interrompu.
         */
        Thread closer;

        public ExportContext(ExportInfo export, File exportDir) {
            this.export = export;
            this.exportDir = exportDir;
        }
        public void close() {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    // ignored
                }
                Runtime.getRuntime().removeShutdownHook(closer);
            }
            if (counter != null) {
                bytes = counter.getByteCount();
            }
        }

        public Writer getOut() throws IOException {
            if (out == null) {
                String filename = export.getExportFilename();
                String extension = export.getExtensionFilename();

                if (!StringUtils.endsWithIgnoreCase(extension, IsisConfig.COMPRESSION_EXTENSION)
                        && IsisFish.config.getExportForceCompression()) {
                    extension += IsisConfig.COMPRESSION_EXTENSION;
                }

                File file = new File(exportDir, filename + extension);
                // prevent two export with same name
                // name MyExport.csv become MyExport_1.csv
                int val = 0;
                while (file.exists()) {
                    val++;
                    file = new File(exportDir, filename + extension + "_" + val);
                }


                OutputStream os = new FileOutputStream(file);
                os = counter = new CountingOutputStream(os);

                // if compression is needed by extension, add compression writer
                if (StringUtils.endsWithIgnoreCase(extension, IsisConfig.COMPRESSION_EXTENSION)) {
                    os = new GZIPOutputStream(os);
                }

                out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(os, StandardCharsets.UTF_8)));

                closer = new Thread(this::close);
                Runtime.getRuntime().addShutdownHook(closer);

            }
            return out;
        }

        public void addTime(long time) {
            this.time += time;
        }

        public void exportBegin(SimulationStorage simulation) throws Exception {
            if (export instanceof ExportStep) {
                long timeStart = System.nanoTime();
                ((ExportStep)export).exportBegin(simulation, getOut());
                addTime(System.nanoTime() - timeStart);
            }
        }

        public void export(SimulationStorage simulation, TimeStep step) throws Exception {
            if (export instanceof ExportStep) {
                long timeStart = System.nanoTime();
                ((ExportStep)export).export(simulation, step, getOut());
                addTime(System.nanoTime() - timeStart);
            }
        }

        public void exportEnd(SimulationStorage simulation) throws Exception {
            if (export instanceof ExportStep) {
                long timeStart = System.nanoTime();
                ((ExportStep)export).exportEnd(simulation, getOut());
                addTime(System.nanoTime() - timeStart);
            }
        }

        public void export(SimulationStorage simulation) throws Exception {
            if (export instanceof Export) {
                long timeStart = System.nanoTime();
                ((Export)export).export(simulation, getOut());
                addTime(System.nanoTime() - timeStart);
            }
        }

        public void setException(Exception exception) {
            this.exception = exception;
        }

    }

    protected Map<ExportInfo, ExportContext> allExports = new HashMap<>();

    @Override
    public void beforeSimulation(SimulationContext context) {
        SimulationStorage simulation = context.getSimulationStorage();
        SimulationParameter parameters = simulation.getParameter();
        File rootDirectory = simulation.getDirectory();

        File exportDir = SimulationStorage
                .getResultExportDirectory(rootDirectory);
        exportDir.mkdirs();

        // Ici on contantene les 2 listes d'exports

        // first, make export instances based on export
        // names
        if (parameters.getExportNames() != null) {
            for (String exportName : parameters.getExportNames()) {
                try {
                    ExportStorage storage = ExportStorage.getExport(exportName);
                    ExportInfo export = storage.getNewInstance();
                    allExports.put(export, new ExportContext(export, exportDir));
                } catch (IsisFishException e) {
                    if (log.isErrorEnabled()) {
                        log.error("Can't get export instance " + exportName, e);
                    }
                }
            }
        }

        // second, add already instanciated sensitivity
        // exports (with parameters)
        List<SensitivityExport> sensitivityExports = parameters.getSensitivityExport();
        if (sensitivityExports != null) {
            for (ExportInfo e : sensitivityExports) {
                allExports.put(e, new ExportContext(e, exportDir));
            }
        }

        // third, optimization export
        Set<ExportInfo> optimizationExports = parameters.getOptimizationExportsObservations().keySet();
        for (ExportInfo export : optimizationExports) {
            // exclude export already added with regular exports
            String exportName = ExportStorage.getName(export);
            if (parameters.getExportNames() == null || !parameters.getExportNames().contains(exportName)) {
                allExports.put(export, new ExportContext(export, exportDir));
            }
        }
        
        // call exportBegin for exportStep
        for (ExportContext e : allExports.values()) {
            try {
                 // pour les ExportStep
                e.exportBegin(simulation);
            } catch (Exception eee) {
                if (log.isErrorEnabled()) {
                    log.error("Can't export results", eee);
                }
                e.setException(eee);
            }
        }
    }

    @Override
    public void stepChange(SimulationContext context, TimeStep step) {
        SimulationControl control = context.getSimulationControl();

        // we keep step to use in after simulation, for last call
        lastStep = step;
        // step is next step, we need previous for export
        step = step.previous();
        // make exports
        if (!allExports.isEmpty()) {
            control.setText(t("isisfish.message.export.export"));
            SimulationStorage simulation = context.getSimulationStorage();
            for (ExportContext e : allExports.values()) {
                try {
                    e.export(simulation, step);
                } catch (Exception eee) {
                    e.setException(eee);
                }
            }
        }
    }

    @Override
    public void afterSimulation(SimulationContext context) {
        SimulationControl control = context.getSimulationControl();

        // make exports
        if (!allExports.isEmpty()) {
            control.setText(t("isisfish.message.export.export"));
            SimulationStorage simulation = context.getSimulationStorage();
            for (ExportContext e : allExports.values()) {
                // on ne met pas les deux exports dans le meme try-catch, au cas ou une seul des deux echouerait
                try {
                    // un seul des deux est reellement fait
                    // pour les ExportStep
                    e.export(simulation, lastStep);
                    // pour les Export
                    e.export(simulation);
                } catch (Exception eee) {
                    if (log.isErrorEnabled()) {
                        log.error("Can't export results", eee);
                    }
                    e.setException(eee);
                }
                try {
                     // pour les ExportStep
                    e.exportEnd(simulation);
                } catch (Exception eee) {
                    if (log.isErrorEnabled()) {
                        log.error("Can't export results", eee);
                    }
                    e.setException(eee);
                }
                // il n'y aura plus d'ecriture dans le fichier on le ferme
                e.close();
            }

            long writtenAll = 0;
            long timeAll = 0;
            for (ExportContext e : allExports.values()) {
                String exportName = ExportStorage.getName(e.export);
                writtenAll += e.bytes;
                timeAll += e.time;
                simulation.getInformation().addExportSize(exportName, e.bytes);
                simulation.getInformation().addExportTime(exportName, e.time);
                if (e.exception != null) {
                    simulation.getInformation().addExportException(exportName, e.exception);
                }
            }
            simulation.getInformation().addAllExportSize(writtenAll);
            simulation.getInformation().addAllExportTime(timeAll);
        }
    }

}
