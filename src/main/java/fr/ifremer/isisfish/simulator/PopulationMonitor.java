/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2015 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixFactory;
import org.nuiton.math.matrix.MatrixIterator;
import org.nuiton.math.matrix.MatrixND;

import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.entities.PopulationGroup;
import fr.ifremer.isisfish.entities.PopulationSeasonInfo;
import fr.ifremer.isisfish.entities.Species;
import fr.ifremer.isisfish.entities.Zone;
import fr.ifremer.isisfish.types.Month;
import fr.ifremer.isisfish.types.RecruitmentInput;
import fr.ifremer.isisfish.types.RecruitmentInputMap;
import fr.ifremer.isisfish.types.ReproductionData;
import fr.ifremer.isisfish.types.TimeStep;

import static org.nuiton.i18n.I18n.n;

/**
 * Classe permettant le suivi des populations de la simulation.
 * <p>
 * Cette classe est normalement multi-thread safe
 * <p>
 * FIXME: certain calcul sont les memes que ceux implanter dans les scripts
 * a cause du groupe des juveniles qui n'est pas un vrai groupe de population.
 * Il serait bon que ce groupe deviennent un vrai groupe et que ce code specifique
 * puisse etre supprimer.
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class PopulationMonitor {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    private static Log log = LogFactory.getLog(PopulationMonitor.class);

    /** La liste des pops monitorer par cette instance */
    protected List<Population> pops = null;
    /** current number of fish by Pop */
    protected Map<Population, MatrixND> Ns = new HashMap<>();
    /** reproduction key: &lt;date, pop&gt; value:&lt;MatrixND&gt; */
    protected Map<Population, Map<TimeStep, ReproductionData>> reproductions = new HashMap<>();
    /** discard key: &lt;date, pop&gt; value:&lt;MatrixND&gt; */
    protected Map<Population, Map.Entry<TimeStep, MatrixND>> discards = new HashMap<>();
    /** catch per population, only last catch is remaining */
    protected Map<Population, MatrixND> catchs = new HashMap<>();
    /** catch per population, only last catch is remaining */
    protected Map<Population, MatrixND> holdCatchs = new HashMap<>();

    protected double totalHoldCatch = 0;

    /**
     * Initialise le monitor pour l'ensemble de pop passe en parametre.
     * Normalement cet init est fait dans le thread principale de la simulation
     * et il n'y a pas de besoin de le synchroniser.
     *
     * Cet init permet d'avoir ensuite le maxime de variable en lecture, et donc
     * de permettre le multithreading de facon legere (pas de synchronise) et
     * sur (pas de concurent modification exception)
     *
     * @param pops
     */
    public void init(List<Population> pops) {
        this.pops = Collections.unmodifiableList(new ArrayList<>(pops));
        for (Population pop : this.pops) {
            reproductions.put(pop, new HashMap<>());
        }
    }

    /**
     * Return all population actualy in PopulationMonitor
     * @return new list of Population
     */
    public List<Population> getPopulations() {
        return pops;
    }

    /**
     * Return current biomass for species.
     * 
     * @param species species
     * @return species biomass
     */
    public double getBiomass(Species species) {
        double result = 0;

        for (Population pop : species.getPopulation()) {
            result += getBiomass(pop);
        }

        return result;
    }

    /**
     * Return current biomass for population.
     * 
     * @param pop population
     * @return population biomass
     */
    public double getBiomass(Population pop) {
        double result = 0;

        MatrixND n = getN(pop);
        if (n != null) {
            n = n.sumOverDim(1);
            for (MatrixIterator i = n.iteratorNotZero(); i.next();) {
                Object[] coord = i.getSemanticsCoordinates();
                PopulationGroup group = (PopulationGroup) coord[0];
                result += i.getValue() * group.getMeanWeight();
            }
        }

        return result;
    }

    /**
     * Return current numbers for population.
     * 
     * @param pop population
     * @return population numbers
     */
    public MatrixND getN(Population pop) {
        MatrixND result = Ns.get(pop);

        if (result != null) {
            // change semantics with list from argument to ensure that
            // semantics don't used too old context
            result.setSemantic(0, pop.getPopulationGroup());
            result.setSemantic(1, pop.getPopulationZone());
        }

        return result;
    }

    public void setN(Population pop, MatrixND mat) {
        Ns.put(pop, mat);
    }

    /**
     * Return reproduction.
     * 
     * @param step step
     * @param pop population
     * @return the reproduction
     */
    public MatrixND getReproduction(TimeStep step, Population pop) {
        return getReproductionData(step, pop).getRepro();
    }

    /**
     * Return reproduction data.
     * 
     * @param step step
     * @param pop population
     * @return the reproductionData
     */
    public ReproductionData getReproductionData(TimeStep step, Population pop) {
        return this.reproductions.get(pop).get(step);
    }

    /**
     * Set new reproduction.
     * 
     * @param step step
     * @param pop population
     * @param aN aboundance
     * @param aBiomass biomass
     * @param repro reproduction to set
     */
    public void setReproduction(TimeStep step, Population pop, MatrixND aN, MatrixND aBiomass, MatrixND repro) {
        reproductions.get(pop).put(step, new ReproductionData(aN, aBiomass, repro));
    }

    /**
     * Applique de la mortalite naturelle aux poissons qui sont pas encore
     * dans les classes de populations. (Reproduction)
     * @param pop
     */
    public void applyReproductionMortality(Population pop) {
        for (ReproductionData nAndRepro : reproductions.get(pop).values()) {
            MatrixND reproduction = nAndRepro.getRepro();
            if (log.isTraceEnabled()) {
                log.trace("Matrix repro before mortality: " + reproduction);
            }

            for (MatrixIterator mi = reproduction.iteratorNotZero(); mi.next();) {
                Object[] sems = mi.getSemanticsCoordinates();
                Zone z = (Zone) sems[0];
                double coeff = pop.getNaturalDeathBirth(z);
                if (log.isTraceEnabled()) {
                    log.trace("NaturalDeath zone " + z + "=" + coeff);
                }
                mi.setValue(mi.getValue()
                        * Math.exp(-coeff / Month.NUMBER_OF_MONTH));
            }
            if (log.isTraceEnabled()) {
                log.trace("Matrix repro after mortality: " + reproduction);
            }
        }
    }

    /**
     * 
     * TODO les reproductions qui sont trop veille pour encore servir
     * doivent être supprimées. c-a-d date &lt; currentDate - etalement - gap between repro recru
     * Une autre facon de faire est de supprimer les repro == 0 car normalement
     * toute la repro doit etre utilisé au bout d'un certain temps
     * 
     * @param step step
     * @param pop population
     * @return population recruitment
     */
    public MatrixND getRecruitment(TimeStep step, Population pop) {
        MatrixND matEtalement = pop.getRecruitmentDistribution();
        int etalement = matEtalement.getDim(0);

        MatrixND result = MatrixFactory.getInstance()
                .create(n("Recruitment"),
                        new List[] { pop.getPopulationGroup(), pop.getPopulationZone() },
                        new String[] { n("Groups"), n("Zones") });

        RecruitmentInputMap recruitmentInputMap = null;
        PopulationSeasonInfo foundReproductionSeasonInfo = null;

        // 1er pas de temps de recrutement.
        TimeStep stepRecru = null;

        // pour chaque
        for (int e = 0; e < etalement; e++) {
            // recuperation de la reproduction stucture en zone repro
            TimeStep t = new TimeStep(step.getStep() - e
                    - pop.getMonthGapBetweenReproRecrutement());
 
            ReproductionData reproductionData = reproductions.get(pop).get(t);
            // si une repro existe pour le mois
            if (reproductionData != null) {

                // on ne doit fournir des datas que si une saison de reproduction existe
                // en fonction du gap et de l'etalement
                Month reproMonth = t.getMonth();
                PopulationSeasonInfo reproductionSeasonInfo = pop.getPopulationSeasonInfo(reproMonth);
                if (reproductionSeasonInfo != null && reproductionSeasonInfo.isReproduction() && foundReproductionSeasonInfo == null) {
                    foundReproductionSeasonInfo = reproductionSeasonInfo;

                    int monthCount = foundReproductionSeasonInfo.getMonths().size();
                    recruitmentInputMap = new RecruitmentInputMap(monthCount);
                    int pos = reproductionSeasonInfo.getMonths().indexOf(reproMonth);

                    stepRecru = new TimeStep(step.getStep() - e - pos);

                    // on remplit toutes les données relativement à la position où la reproduction a été calculé
                    // donc, c'est toujours:
                    // - données 0 : données pour le premier mois de repro
                    // - données 1 : données pour le deuxieme mois de repro
                    // etc...
                    for (int p = 0; p < monthCount; p++) {
                        TimeStep t2 = new TimeStep(t.getStep() + p - pos);
                        ReproductionData reproductionData2 = reproductions.get(pop).get(t2);
                        RecruitmentInput recruitmentInput2 = reproductionData2 != null ? new RecruitmentInput(reproductionData2) : new RecruitmentInput();
                        // recruitmentInput2#recuitementContribution must be null
                        // this is the key "null information here"
                        recruitmentInputMap.put(p, recruitmentInput2);
                    }
                }

                // pour les mois suivants (la map est forcement remplie on ajuste seulement la contribution)
                if (foundReproductionSeasonInfo != null) {
                    int pos = foundReproductionSeasonInfo.getMonths().indexOf(reproMonth);

                    if (pos != -1) {
                        // si pos == -1 c'est que l'etalement + gap est au dela de la période de repro
                        // mais comme on a trouvé une periode de repro avant, on donne quand même
                        // les données avec recuitementContribution à nul
                        RecruitmentInput recruitmentInput = recruitmentInputMap.get(pos);
                        Double contribution = matEtalement.getValue(e);
                        recruitmentInput.setRecruitementContribution(contribution);
                    }
                }

                MatrixND repro = reproductionData.getRepro();

                // on fait la correspondance entre les zones repro et
                // recrutement

                PopulationGroup classe;
                int indiceClasse = 0;
                // si on a change d'annee le recrutement ne se fait pas en
                // classe 0 mais en classe 1, si on a change 2 fois d'annee
                // le recrutement se fait en age 2, etc.
                classe = pop.getPopulationGroup().get(indiceClasse);

                List<Zone> zoneRepros = pop.getReproductionZone();

                // on multiplie la repro par le coeff de recrutement
                double coeff = matEtalement.getValue(e);

                MatrixND matRepro = repro.copy();
                matRepro = matRepro.mults(coeff);

                matRepro.setSemantic(0, zoneRepros);

                MatrixND mapping = pop.getMappingZoneReproZoneRecru();
                for (Zone zoneRepro : zoneRepros) {
                    MatrixND submapping = mapping.getSubMatrix(0, zoneRepro);
                    for (MatrixIterator i = submapping.iterator(); i.hasNext();) {
                        i.next();
                        Object[] sem = i.getSemanticsCoordinates();
                        Zone zoneRecru = (Zone) sem[1];
                        double c = i.getValue();
                        result.setValue(classe, zoneRecru, c
                                * matRepro.getValue(zoneRepro)
                                + result.getValue(classe, zoneRecru));
                    }
                }
            }
        }

        // poussin 20170118 ticket #8968
        // Il faut quoi qu'il arrive que les poissons recrutés durant le 1er mois de recrutement finissent
        // en classe 0. Il se peut que pour les autres mois de recrutement il y ait besoin de changer de classe
        // mais toujours en fonction de ce 1er mois de recrutement. Si le delta entre repro et recru est de 36 mois
        // et que l'étalement du recrutement est sur 12 mois. et que la repro a lieu en juin.
        // Les 6 premiers de recrutement finisse en classe 0, les 6 dernieres en classe 1.
        // ----- implantation
        // pour cela il faut remplir usedSeasons seulement avec les mois a partir
        // du 2eme mois de recrutement (repro+gap+1) pour garantir que le 1er mois
        // de recru finisse bien dans la classe 0.
        //
        // Pour cela on utilise stepRecru qui est le 1er pas de temps de recrutement
        // pour le pas de temps courant, a partir duquel il faut faire le parcours
        if (pop.getSpecies().isAgeGroupType() && stepRecru != null) {
            log.debug(String.format("Compute CA: Pop %s - Current step is %s(month %s) recru step is %s(month %s)",
                    pop, step, step.getMonth(), stepRecru, stepRecru.getMonth()));
            // conversion et retour de la matrice en vecteur
            MatrixND N = pop.N2DToN1D(result);

            // recherche les saisons des differents mois entre les deux dates
            List<PopulationSeasonInfo> infos = pop.getPopulationSeasonInfo();
            List<PopulationSeasonInfo> usedSeasons = new ArrayList<>();

            while (step.after(stepRecru)) {
                stepRecru = stepRecru.next();
                Month month = stepRecru.getMonth();
                for (PopulationSeasonInfo info : infos) {
                    if (month.equals(info.getFirstMonth())) {
                        usedSeasons.add(info);
                        stepRecru = new TimeStep(stepRecru.getStep()
                                + info.getMonths().size() - 1); // -1 because, for have next()
                        break;
                    }
                }
            }

            for (PopulationSeasonInfo info : usedSeasons) {
                Month month = info.getFirstMonth();
                MatrixND CA = info.getGroupChangeMatrix(month);
                // poussin 20141203 never applicate migration (S.Mahevas, S.Lehuta)
                // MatrixND MI = info.getMigrationMatrix(month, result);
                N = N.mult(CA);
            }
            result = pop.split2D(N);
        }

        // calcul recrutement par l'equation
        if (recruitmentInputMap != null) {
            result = pop.getRecruitmentMatrix(step, pop, recruitmentInputMap, result);
        }

        return result;
    }

    /**
     * @param pop population
     * @param catchPerStrategyMet   
     */
    public void holdCatch(Population pop, MatrixND catchPerStrategyMet) {
        catchs.put(pop, catchPerStrategyMet);

        MatrixND holdCatch = holdCatchs.get(pop);
        if (holdCatch == null) {
            holdCatch = catchPerStrategyMet.copy();
            holdCatchs.put(pop, holdCatch);
        } else {
            holdCatch.add(catchPerStrategyMet);
        }

        // compute total
        for (MatrixIterator i = catchPerStrategyMet.iteratorNotZero(); i.next();) {
            this.totalHoldCatch += i.getValue();
        }
        // this.totalHoldCatch += catchPerStrategyMet.sumAll();
    }

    /**
     * Get population catch.
     * 
     * @param pop population
     * @return population catch
     */
    public MatrixND getCatch(Population pop) {
        MatrixND result = catchs.get(pop);
        return result;
    }

    /**
     * Get population hold catch.
     * 
     * @param pop population
     * @return population hold catch
     */
    public MatrixND getHoldCatch(Population pop) {
        MatrixND result = holdCatchs.get(pop);
        return result;
    }

    /**
     * Population total hold catch.
     * 
     * @param pop population
     * @return population total hold catch
     */
    public double getTotalHoldCatch(Population pop) {
        double result = totalHoldCatch;
        return result;
    }

    /**
     * RAZ capture cumulée de toutes les pops.
     */
    public void clearCatch() {
        catchs.clear();
        holdCatchs.clear();
        totalHoldCatch = 0;
    }

    /**
     * Get discard.
     * 
     * @param step step to get discard
     * @param pop population to get discard
     * @return le discard
     */
    public MatrixND getDiscard(TimeStep step, Population pop) {
        checkStep(step, "You can only get discard for current step simulation, for old discard you must use ResultStorage");

        MatrixND result = null;
        // seul le discard de l'annee en cours est disponible, sinon on doit
        // aller le chercher dans les resultats
        Map.Entry<TimeStep, MatrixND> discard = discards.get(pop);
        if (discard != null && discard.getKey().equals(step)) {
            result = discard.getValue();
        }
        return result;
    }

    /**
     * Set discard.
     * Only work for current step simulation
     * 
     * Force discard at date for population.
     * 
     * @param step step to set discard
     * @param pop population to set discard
     * @param discard le discard
     */
    public void setDiscard(TimeStep step, Population pop, MatrixND discard) {
        checkStep(step, "You can only change discard for current step simulation");
        // meme si discards.get(pop) != null
        // replace toujours le précédent
        MatrixND tmp = discard.copy();
        discards.put(pop, new AbstractMap.SimpleEntry<>(step, tmp));
    }

    /**
     * Only work for current step simulation
     *
     * @param step
     * @param pop
     * @param discard
     */
    public void addDiscard(TimeStep step, Population pop, MatrixND discard) {
        checkStep(step, "You can only add discard for current step simulation");

        MatrixND oneDiscard = getDiscard(step, pop);
        if (oneDiscard == null) {
            oneDiscard = discard.copy();
        } else {
            oneDiscard.add(discard);
        }
        setDiscard(step, pop, oneDiscard);
    }

    public void checkStep(TimeStep step, String msg) {
        SimulationContext sim = SimulationContext.get();
        TimeStep simStep = sim.getSimulationControl().getStep();
        if (!step.equals(simStep )) {
            throw new IsisFishRuntimeException(msg);
        }
    }
}
