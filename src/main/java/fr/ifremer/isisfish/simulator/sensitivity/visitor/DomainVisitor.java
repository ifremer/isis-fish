/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2010 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator.sensitivity.visitor;

import fr.ifremer.isisfish.simulator.sensitivity.Domain;

/**
 * Domain visitor.
 *
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public interface DomainVisitor {

    /**
     * Start.
     * 
     * @param domain domain
     */
    void start(Domain domain);
    
    /**
     * Visit domain value or property.
     * 
     * @param domain domain
     * @param label value label or property name
     * @param value value
     */
    void visit(Domain domain, Object label, Object value);
    
    /**
     * End.
     * 
     * @param domain domain
     */
    void end(Domain domain);
}
