/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2010 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator.sensitivity;

import java.io.File;
import java.util.List;

import fr.ifremer.isisfish.datastore.SimulationStorage;

/**
 * Interface commune à toutes implémentation de calcul de sensibilité.
 * 
 * Les implémentations peuvent contenir des paramètres nommé "param_xx" (pour le
 * paramètre xx) qui seront injecté par Isis.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$ By : $Author$
 */
public interface SensitivityAnalysis {

    /**
     * Return calculator description.
     * 
     * @return calculator description
     */
    String getDescription();

    /**
     * Retourne vrai si le calculateur sait gerer la cardinalité
     * des facteurs continue.
     * 
     * @return {@code true}s'il sait la gerer
     */
    boolean canManageCardinality();
    
    /**
     * Envoi un plan a faire analyser par l'outils d'analyse de sensibilité.
     * 
     * Retourne un {@link SensitivityScenarios} qui représente l'ensemble des
     * scenarios à prendre en compte pour les simulations.
     * 
     * @param plan plan a analyser
     * @param outputDirectory master sensitivity export directory
     * 
     * @return un {@link SensitivityScenarios}
     * @throws SensitivityException if calculator impl fail to execute
     * 
     * @see DesignPlan
     * @see Scenario
     * @see SensitivityScenarios
     */
    SensitivityScenarios compute(DesignPlan plan, File outputDirectory)
            throws SensitivityException;

    /**
     * Permet de renvoyer les resultats de simulations à l'outils de d'analyse
     * de sensibilité.
     * 
     * @param simulationStorages
     *            ensemble des {@link SimulationStorage} qui ont résultés des
     *            simulations
     * @param outputdirectory master sensitivity export directory
     * @throws SensitivityException
     *             if calculator impl fail to execute
     * 
     * @see SensitivityScenarios
     */
    void analyzeResult(List<SimulationStorage> simulationStorages,
            File outputdirectory) throws SensitivityException;
}
