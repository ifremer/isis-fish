/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2010 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator.sensitivity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import fr.ifremer.isisfish.simulator.sensitivity.visitor.ScenarioVisitor;

/**
 * Scenario d'execution de simulation.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class Scenario implements Serializable {

    /** serialVersionUID. */
    private static final long serialVersionUID = 4953721873692486687L;

    /**
     * Ensemble des factors a prendre en compte.
     * 
     * Chaque facteur a la {@link Factor#value} a prendre en compte pour la simulation.
     */
    protected List<Factor> factors;

    /**
     * Constructeur.
     */
    public Scenario() {
        factors = new ArrayList<>();
    }

    /**
     * Add factor.
     * 
     * Use {@link Factor#clone()} to copy a new factor instance.
     * 
     * @param factor to copy
     * @return add success
     */
    public boolean addFactor(Factor factor) {

        Factor factorCopy = (Factor) factor.clone();
        boolean success = factors.add(factorCopy);
        return success;

    }

    /**
     * Get simulation Factors.
     * 
     * @return the simulations
     */
    public List<Factor> getFactors() {
        return factors;
    }

    /**
     * Set simulation factors.
     * 
     * @param factors the simulations factors to set
     */
    public void setFactors(List<Factor> factors) {
        this.factors = factors;
    }

    /**
     * Accept a new visitor.
     * 
     * @param visitor
     */
    public void accept(ScenarioVisitor visitor) {
        visitor.start(this);
        for (Factor factor : factors) {
            visitor.visit(this, factor);
        }
        visitor.end(this);
    }
}
