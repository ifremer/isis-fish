package fr.ifremer.isisfish.simulator.sensitivity;

/*
 * #%L
 * IsisFish
 * %%
 * Copyright (C) 2014 Ifremer, Codelutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.isisfish.rule.Rule;
import fr.ifremer.isisfish.rule.RuleHelper;
import fr.ifremer.isisfish.simulator.SimulationParameter;
import fr.ifremer.isisfish.simulator.sensitivity.domain.EquationDiscreteDomain;
import fr.ifremer.isisfish.util.converter.ConverterUtil;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.ConvertUtilsBean;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixND;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Useful method for factor
 * - create prescript with factor list
 *
 * @author poussin
 * @version $Revision$
 * <p>
 * Last update: $Date$
 * by : $Author$
 */
public class FactorHelper {

    /**
     * to use log facility, just put in your code: log.info(\"...\");
     */
    private static final Log log = LogFactory.getLog(FactorHelper.class);

    /**
     * Generate prescript to set factor values before simulation start.
     *
     * @param scenario to generate prescript
     * @return simulation pre script with correct values
     */
    public static String generatePreScript(Scenario scenario) {
        return generatePreScript(scenario.getFactors());
    }

    public static String generatePreScript(Collection<Factor> factors) {
        ConvertUtilsBean beanUtils = ConverterUtil.getConverter(null);
        // n'utilise plus freemarker, car il y avait plus d'instruction
        // freemarker compliqué que de code a afficher
        StringBuffer buffer = new StringBuffer();
        buffer.append("import org.apache.commons.beanutils.BeanUtils;\n");
        buffer.append("import org.apache.commons.beanutils.ConvertUtilsBean;\n");
        buffer.append("import " + ConverterUtil.class.getName() + ";\n");
        buffer.append("import " + SimulationParameter.class.getName() + ";\n");
        buffer.append("import org.nuiton.topia.persistence.TopiaEntity;\n");
        buffer.append("ConvertUtilsBean beanUtils = ConverterUtil.getConverter(db);\n");
        buffer.append("SimulationParameter params = context.getSimulationStorage().getParameter();\n");
        generatePreScript(beanUtils, buffer, new AtomicInteger(), factors);
        String scriptContent = buffer.toString();
        if (log.isTraceEnabled()) {
            log.trace("Simulation prescript content = " + scriptContent);
        }
        return scriptContent;
    }

    /**
     * Generate prescript for a factor list that can be called recursively to
     * manage factor group.
     *
     * @param beanUtils beanUtils converter
     * @param buffer    buffer to fill
     * @param counter   call counter used to avoid variables name collision
     * @param factors   factor list to manage
     */
    protected static void generatePreScript(ConvertUtilsBean beanUtils, StringBuffer buffer, AtomicInteger counter, Collection<Factor> factors) {
        for (Factor factor : factors) {
            if (factor instanceof FactorGroup) {
                buffer.append("/* factor group : ").append(factor.getName()).append(" */\n");
                FactorGroup factorGroup = (FactorGroup) factor;
                generatePreScript(beanUtils, buffer, counter, factorGroup.getFactors());
            } else {
                int factorIndex = counter.get();
                buffer.append("/* factor : ").append(factor.getName()).append(" */\n");
                // cas special 1 : population de départ
                if (factor.getPath().matches("parameters.population\\.\\w+\\.number")) {
                    String paramName = StringUtils.removeStart(factor.getPath(), "parameters.");
                    // pas de convert, c'est fait comme ca dans
                    // SimulationParameters.toProperties();
                    MatrixND matrix = (MatrixND) factor.getValue();
                    Object stringValue = String.valueOf(matrix.toList());
                    buffer.append("params.setProperty(\"").append(paramName).append("\",\"").append(stringValue).append("\");\n");
                }
                // cas special 2 : regles
                else if (factor.getPath().equals("parameters.rules")) {
                    List<Rule> rules = (List<Rule>) factor.getValue();
                    List<String> rulesNames = new ArrayList<>();
                    int ruleIndex = 0;
                    for (Rule rule : rules) {
                        rulesNames.add(rule.getClass().getSimpleName());
                        Properties rulesProps = RuleHelper.getRuleAsProperties(ruleIndex++, null, rule);
                        for (String rulesProp : rulesProps.stringPropertyNames()) {
                            String value = rulesProps.getProperty(rulesProp);
                            buffer.append("params.setProperty(\"").append(rulesProp).append("\",\"").append(value).append("\");\n");
                        }
                    }
                    buffer.append("params.setProperty(\"rules\",\"").append(StringUtils.join(rulesNames, ",")).append("\");\n");
                }
                // cas special 3 : facteur sur les parametres des regles
                else if (factor.getPath().startsWith("parameters.rule.")) {
                    // special case for rule parameter with
                    Pattern pattern = Pattern.compile("^parameters\\.(rule\\.\\d+\\.parameter\\.\\w+)(\\..+)?$");
                    Matcher matcher = pattern.matcher(factor.getPath());
                    String paramName;
                    if (matcher.matches()) {
                        paramName = matcher.group(1);
                    } else {
                        paramName = StringUtils.removeStart(factor.getPath(), "parameters.");
                    }
                    String stringValue = ConvertUtils.convert(factor.getValue());
                    buffer.append("params.setProperty(\"").append(paramName).append("\",\"").append(stringValue).append("\");\n");
                }
                // cas special 4 : equation
                else if (StringUtils.isNotBlank(factor.getEquationVariableName())) {
                    buffer.append("context.setComputeValue(\"").append(factor.getName() + "." + factor.getEquationVariableName());
                    buffer.append("\",").append(factor.getValue()).append(");\n");
                }
                // cas pas si special
                else {
                    Object value = factor.getValue();
                    String stringValue = beanUtils.convert(value);
                    String escValue = stringValue;
                    if (factor.getDomain() instanceof EquationDiscreteDomain) {
                        // echatellier: equation can contains quotes that break
                        // prescript, only quote, not all java replacements
                        // a ne pas faire pour le reste, pour les
                        // matrice par exemple, ca passe mal
                        escValue = stringValue.replaceAll("//(.*)\n", "/* $1 */");
                        escValue = StringUtils.replace(escValue, "\n", "");
                        escValue = StringUtils.replace(escValue, "\r", "");
                        escValue = StringUtils.replace(escValue, "\"", "\\\"");
                    }
                    String path = factor.getPath();
                    String topiaId = path.substring(0, path.lastIndexOf('#'));
                    String property = path.substring(path.lastIndexOf('#') + 1);
                    // Double value123 = (Double)beanUtils.convert("mystringvalue", Double.class);
                    buffer.append(value.getClass().getName()).append(" value");
                    buffer.append(factorIndex).append(" = (").append(value.getClass().getName()).append(")beanUtils.convert(\"");
                    buffer.append(escValue).append("\", ").append(value.getClass().getName());
                    buffer.append(".class);\n");
                    // TopiaEntity entity123 = db.findByTopiaId(topiaId);
                    buffer.append("TopiaEntity entity").append(factorIndex);
                    buffer.append(" = db.findByTopiaId(\"").append(topiaId);
                    buffer.append("\");\n");
                    if (factor.getDomain() instanceof EquationDiscreteDomain) {
                        // BeanUtils.setProperty(entity123, "propertyContent", value123);
                        buffer.append("BeanUtils.setProperty(entity").append(factorIndex);
                        buffer.append(", \"").append(property).append("Content\", ");
                        buffer.append("value").append(factorIndex).append(");\n");
                    } else {
                        // BeanUtils.setProperty(entity123, "property", value123);
                        buffer.append("BeanUtils.setProperty(entity").append(factorIndex);
                        buffer.append(", \"").append(property).append("\", ");
                        buffer.append("value").append(factorIndex).append(");\n");
                    }
                }
            }
            counter.incrementAndGet();
        }
    }

}
