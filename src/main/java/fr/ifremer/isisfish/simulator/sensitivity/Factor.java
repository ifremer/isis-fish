/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2012 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator.sensitivity;

import java.io.Serializable;

import org.jdesktop.beans.AbstractBean;
import org.nuiton.math.matrix.MatrixND;

import fr.ifremer.isisfish.simulator.sensitivity.domain.ContinuousDomain;
import fr.ifremer.isisfish.simulator.sensitivity.domain.DiscreteDomain;
import fr.ifremer.isisfish.simulator.sensitivity.visitor.FactorVisitor;

/**
 * Facteur de variation des parametres de simulation.
 * 
 * La classe doit être {@link Serializable} avec ses valeurs pour permettre
 * l'export XML.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$ By : $Author$
 */
public class Factor extends AbstractBean implements Serializable, Cloneable {

    /** serialVersionUID. */
    private static final long serialVersionUID = 1643804268013964453L;

    /**
     * Nom du facteur.
     */
    protected String name;

    /**
     * Commentaire du facteur.
     */
    protected String comment;

    /**
     * The identifier used to compute value.
     */
    protected Object identifier;

    /**
     * Domaine du facteur.
     * 
     * Ensemble des valeurs possibles a prendre en compte. La clé est un label
     * qui permet d'identifier la valeur.
     * 
     * Le domain peut etre :
     * <ul>
     *  <li>discret : i.e un ensemble de valeurs</li>
     *  <li>continu : i.e, une borne min, max</li>
     * </ul>
     * 
     * @see ContinuousDomain
     * @see DiscreteDomain
     */
    protected Domain domain;

    /**
     * Value that this factor got in the database
     */
    protected Object nominalValue;

    /**
     * Factor value.
     */
    protected Object value;

    /**
     * Path permettant d'identifier l'objet et la propriete de l'objet a mettre
     * a jour.
     * 
     * Par exemple: topiaID#gear aura pour effet de recuperer l'objet
     * correspondant au topiaID fournit et d'appeler le propriete
     * {@code setGear(value)} dessus.
     */
    protected String path;
    
    /** Cardinality */
    protected int cardinality;

    /** Variable name in case of equation factor (must be java valid identifier) */
    protected String equationVariableName;

    /**
     * Constructor with name.
     * 
     * @param name factor name
     */
    public Factor(String name) {
        this.name = name;
    }

    /**
     * Get name.
     * 
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Set name.
     * 
     * @param name the name to set
     */
    public void setName(String name) {
        String oldValue = this.name;
        this.name = name;
        firePropertyChange("name", oldValue, name);
    }

    /**
     * Get comment.
     * 
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * Set comment.
     * 
     * @param comment comment to set
     */
    public void setComment(String comment) {
        String oldValue = this.comment;
        this.comment = comment;
        firePropertyChange("comment", oldValue, comment);
    }

    /**
     * Get domain.
     * 
     * @return the domain
     */
    public Domain getDomain() {
        return domain;
    }

    /**
     * Set domain.
     * 
     * @param domain the domain to set
     */
    public void setDomain(Domain domain) {
        Domain oldValue = this.domain;
        this.domain = domain;
        firePropertyChange("domain", oldValue, domain);
    }

    /**
     * Get value.
     * 
     * @return the value
     */
    public Object getValue() {
        return value;
    }

    /**
     * Get the factor value for displaying (for example in R).
     * 
     * @return the value
     */
    public Object getDisplayedValue() {
        Object result;
        // add quotes for equation, but why ?
        if (equationVariableName != null) {
            result = "\"" + getValue() + "\"";
        } else {
            result = getValue();
            
            // no idea either
            if (result instanceof MatrixND) {
                result = identifier;
            }
        }
        return result;
    }

    /**
     * Set value.
     * 
     * @param value new value
     */
    public void setValue(Object value) {
        Object oldValue = this.value;
        this.value = value;
        firePropertyChange("value", oldValue, value);
    }

    /**
     * Set value for label.
     * 
     * @param valueIdentifier new value identifier to get
     */
    public void setValueForIdentifier(Object valueIdentifier) {
        identifier = valueIdentifier;
        value = domain.getValueForIdentifier(valueIdentifier);
    }

    /**
     * Get path.
     * 
     * @return the path
     */
    public String getPath() {
        return path;
    }

    /**
     * Set path.
     * 
     * Warning, path need to be always a valid entity property reference.
     * 
     * @param path the path to set
     */
    public void setPath(String path) {
        Object oldValue = this.path;
        this.path = path;
        firePropertyChange("path", oldValue, path);
    }

    public Object getNominalValue() {
        return nominalValue;
    }

    public void setNominalValue(Object nominalValue) {
        Object oldValue = this.nominalValue;
        this.nominalValue = nominalValue;
        firePropertyChange("nominalValue", oldValue, nominalValue);
    }
    
    public int getCardinality() {
        int result = cardinality;
        if (domain instanceof DiscreteDomain) {
            result = ((DiscreteDomain)domain).getValues().size();
        }
        return result;
    }

    public void setCardinality(int cardinality) {
        int oldValue = this.cardinality;
        this.cardinality = cardinality;
        firePropertyChange("cardinality", oldValue, cardinality);
    }

    /**
     * Get variable name.
     * 
     * @return the variableName
     */
    public String getEquationVariableName() {
        return equationVariableName;
    }

    /**
     * Set variable name.
     * 
     * @param equationVariableName the variableName to set
     */
    public void setEquationVariableName(String equationVariableName) {
        String oldValue = this.equationVariableName;
        this.equationVariableName = equationVariableName;
        firePropertyChange("equationVariableName", oldValue, equationVariableName);
    }

    /**
     * Accept a new visitor.
     * 
     * @param visitor
     */
    public void accept(FactorVisitor visitor) {
        visitor.start(this);
        visitor.visit(this, domain);
        visitor.end(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object clone() {

        Factor f;
        try {
            f = (Factor)super.clone();

            if (domain != null) {
                f.domain = domain.clone();
            }
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException("Error on clone()", e);
        }

        return f;
    }

    @Override
    public String toString() {
        return "Factor : " + name + "(" + comment + ")";
    }
}
