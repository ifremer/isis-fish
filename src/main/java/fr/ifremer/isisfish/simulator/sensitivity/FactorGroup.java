/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2013 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator.sensitivity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import fr.ifremer.isisfish.simulator.sensitivity.domain.ContinuousDomain;
import fr.ifremer.isisfish.simulator.sensitivity.domain.DiscreteDomain;
import fr.ifremer.isisfish.simulator.sensitivity.visitor.FactorGroupVisitor;

/**
 * Factor group. Used for group screening.
 * 
 * A factor mixed can accept both discrete and continuous factors (used
 * for convenience : factor tree root).
 * 
 * A factor group without factors is untyped.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * @since 3.4.0.0
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class FactorGroup extends Factor {

    /** serialVersionUID. */
    private static final long serialVersionUID = 1893573810633639712L;

    /** Factors collection. */
    protected List<Factor> factors;

    /**
     * Factor group is typed discrete/continuous.
     * 
     * Continuous group can accept only continuous factors.
     * Discrete group can accept both discrete and continuous factors.
     */
    protected boolean continuous;

    protected static class FactorComparator implements Comparator<Factor> {
        @Override
        public int compare(Factor o1, Factor o2) {
            int result = 1;
            
            if (o1 instanceof FactorGroup) {
                if (o2 instanceof FactorGroup) {
                    result = o1.getName().compareTo(o2.getName());
                }
                else {
                    result = -1;
                }
            }
            else { // factor
                if (o2 != null) {
                    result = o1.getName().compareTo(o2.getName());
                }
            }
            return result;
        }
    }

    /**
     * Init factor group.
     * 
     * @param name group name
     */
    public FactorGroup(String name) {
        super(name);
        factors = new ArrayList<>();
    }
    
    /**
     * Init factor group.
     * 
     * @param name group name
     * @param continuous continuous domain
     */
    public FactorGroup(String name, boolean continuous) {
        this(name);
        this.continuous = continuous;
    }

    /**
     * Add all checked factors.
     * 
     * @param allFactors factors to add
     */
    public void addAllFactors(List<Factor> allFactors) {

        for (Factor factor : allFactors) {
            checkFactor(factor);
            factors.add(factor);
        }

        // sort : factor group, by name, and factor by name
        factors.sort(new FactorComparator());
    }

    /**
     * Check factor type and add it into factor collection.
     * 
     * @param factor
     */
    public void addFactor(Factor factor) {

        checkFactor(factor);
        factors.add(factor);

        // sort : factor group, by name, and factor by name
        factors.sort(new FactorComparator());
    }

    /**
     * Check factor type with other factor collection types.
     * 
     * @param factor factor to check
     * @throws IllegalArgumentException if factor doesn't match other factor type
     */
    protected void checkFactor(Factor factor) {
        // basiquement, il doit être du même type que le
        // premier element
        if (factor.getDomain() == null) {
            throw new IllegalArgumentException("Factor domain is null");
        }

        // discrete accept all
        // continuous accepts only continous
        if (isContinuous() && !(factor.getDomain() instanceof ContinuousDomain)) {
            throw new IllegalArgumentException("Factor type is not in same type as other factor in group");
        }
    }

    /**
     * Remove single factor.
     * 
     * @param factor factor to remove
     */
    public void remove(Factor factor) {
        factors.remove(factor);
    }

    /**
     * Remove factors collection.
     * 
     * @param allFactors factors to remove
     */
    public void removeAll(Collection<Factor> allFactors) {
        factors.removeAll(allFactors);
    }

    /**
     * Get groups factors.
     * 
     * @return unmodifiable factors list
     */
    public List<Factor> getFactors() {
        return Collections.unmodifiableList(factors);
    }

    /**
     * Clear all factor group sub factors.
     */
    public void clearFactors() {
        factors.clear();
    }

    /**
     * Convenient method to access specific factor.
     * 
     * @param index index
     * @return factor at index
     */
    public Factor get(int index) {
        return factors.get(index);
    }
    
    /**
     * Get factor list size.
     * 
     * @return factor list size
     */
    public int size() {
        return factors.size();
    }
    
    /**
     * Returns the index of the first occurrence of the specified element in this
     * group.
     * @param o element to search for
     * @return the index of the first occurrence of the specified element in this
     * group, or -1 if this list does not contain the element
     */
    public int indexOf(Object o) {
        return factors.indexOf(o);
    }

    /**
     * Return {@code true} if factor group is discrete.
     * 
     * @return {@code true} if factor group is discrete
     */
    public boolean isDiscrete() {
        return !continuous;
    }

    /**
     * Return {@code true} if factor group is continuous.
     * 
     * @return {@code true} if factor group is continuous
     */
    public boolean isContinuous() {
        return continuous;
    }

    /**
     * Return factor group domain depending of factor group type (discrete/continuous).
     * 
     * @return factor group domain
     */
    @Override
    public Domain getDomain() {
        Domain domain = null;
        if (isContinuous()) {
            domain = new ContinuousDomain(Distribution.QUNIFMM);
            ((ContinuousDomain)domain).addDistributionParam("min", 0.0);
            ((ContinuousDomain)domain).addDistributionParam("max", 1.0);
        } else if (isDiscrete()) {
            // les domaines doivent avoir les même domaines
            // et pour l'utilisation quand fera l'appelant
            // de getDomain(), on peut en retourner un au hazard
            if (factors.size() > 0) {
                domain = factors.get(0).getDomain();
            } else {
                domain = new DiscreteDomain();
            }
        }
        // else pas de factor dans le group
        return domain;
    }

    /**
     * Accept a new visitor.
     * 
     * @param visitor
     */
    public void accept(FactorGroupVisitor visitor) {
        visitor.start(this);
        for (Factor factor : factors) {
            visitor.visit(this, factor);
        }
        visitor.end(this);
    }

    @Override
    public String toString() {
        return "FactorGroup(" + name + ")";
    }

    /**
     * Set value for label.
     * 
     * @param valueIdentifier new value identifier to get
     */
    @Override
    public void setValueForIdentifier(Object valueIdentifier) {
        identifier = valueIdentifier;
        for (Factor factor : factors) {
            factor.setValueForIdentifier(valueIdentifier);
        }
    }

    /**
     * Get the factor value for displaying (for example in R).
     *
     * @return the value
     */
    @Override
    public Object getDisplayedValue() {
        return identifier;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public Object clone() {

        FactorGroup f = (FactorGroup)super.clone();

        // deep copy of subfactors
        f.factors = new ArrayList<>(factors.size());
        for (Factor subFactor : factors) {
            f.factors.add((Factor)subFactor.clone());
        }

        return f;
    }
}
