/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator.sensitivity;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.topia.persistence.TopiaEntity;

import fr.ifremer.isisfish.entities.Equation;
import fr.ifremer.isisfish.types.Month;
import fr.ifremer.isisfish.types.RangeOfValues;
import fr.ifremer.isisfish.types.TimeStep;
import fr.ifremer.isisfish.types.TimeUnit;

/**
 * Utility methods for sensitivity analysis.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class SensitivityUtils {

    private static final Log log = LogFactory.getLog(SensitivityUtils.class);

    /** Properties file resource path. */
    protected final static String PROPERTIES_PATH = "/sensitivity.properties";

    /** Cached properties. */
    protected static Properties sensitivityProperties;

    /**
     * Escape factor name.
     * 
     * R don't like factor name with special characters such as "." and accents.
     * @param factorName factor name to escape
     * @return escaped factor name
     * @since 4.0.0.2
     */
    public static String espaceFactorName(String factorName) {
        String name = StringUtils.stripAccents(factorName);
        name = name.replaceAll("\\W", "_");
        return name;
    }

    /**
     * Is field sensitivity enabled.
     * 
     * Return true, if field name is present in properties file.
     * 
     * @param sensitivityFactorName name
     * @return {@code true} if factor name is sensitivity enabled
     */
    public static boolean isSensitivityFactorEnabled(String sensitivityFactorName) {
        Properties data = getProperties();
        return data.getProperty(sensitivityFactorName) != null;

    }

    /**
     * Return cached properties file.
     * Load it at first call.
     * 
     * @return properties
     */
    public static Properties getProperties() {

        if (sensitivityProperties == null) {
            sensitivityProperties = new Properties();
            try (InputStream stream = SensitivityUtils.class.getResourceAsStream(PROPERTIES_PATH)) {
                sensitivityProperties.load(stream);
            } catch (IOException ex) {
                if (log.isErrorEnabled()) {
                    log.error("Can't load sensitivity properties file", ex);
                }
            }
        }
        return sensitivityProperties;
    }
    
    /**
     * Return true if value can be defined in continuous factor.
     * 
     * Il serait plus interessant de le faire sur les types et non sur les
     * valeur mais pour {@link RangeOfValues} par exemple, seule la valeur
     * a un type Float... donc pas evident.
     * 
     * @param value value
     * @return continuous enabled
     */
    public static boolean canBeContinue(Object value) {
        boolean result = false;

        if (value instanceof Double) {
            result = true;
        } else if (value instanceof Long) {
            result = true;
        } else if (value instanceof Equation) {
            result = true;
        } else if (value instanceof MatrixND) {
            result = true;
        } else if (value instanceof RangeOfValues) {
            RangeOfValues rangeOfValues = (RangeOfValues)value;
            if (rangeOfValues.getType().equals("Float")) {
                result = true;
            }
        } else if (value instanceof TimeUnit) {
            result = true;
        } else if (value instanceof String) {
            // todo fix string value :(
            result = true;
        }

        return result;
    }

    /**
     * Return if value is is continue factor enable.
     * 
     * @param value value to test
     * @return {@code true} if value is is continue factor enable
     */
    public static boolean isContinue(Object value) {
        boolean result = false;
        if (value instanceof RangeOfValues) {
            RangeOfValues range = (RangeOfValues)value;
            if (range.getType().equals(RangeOfValues.TYPE_FLOAT)) {
                String textValue = range.getValues();
                // TODO need comment !!!
                if (textValue.matches("^\\ *[0-9]*\\ *\\-\\ *[0-9]*\\ *$")) {
                    result = true;
                }
            }
        }
        return result;
    }
    
    /**
     * Return if type can be defined as a factor.
     * 
     * @param type type
     * @return {@code true} if type can be defined as a factor
     */
    public static boolean canBeFactor(Class type) {
        boolean result = false;
        
        if (TopiaEntity.class.isAssignableFrom(type)) {
            result = true;
        } else if (double.class.isAssignableFrom(type)) {
            result = true;
        } else if (Number.class.isAssignableFrom(type)) {
            result = true;
        } else if (TimeStep.class.isAssignableFrom(type)) {
            result = true;
        } else if (Month.class.isAssignableFrom(type)) {
            result = true;
        }
        
        return result;
    }
}
