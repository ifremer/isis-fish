/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2010 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator.sensitivity.visitor;

import fr.ifremer.isisfish.simulator.sensitivity.DesignPlan;
import fr.ifremer.isisfish.simulator.sensitivity.FactorGroup;

/**
 * DesignPlan visitor.
 *
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public interface DesignPlanVisitor {

    /**
     * Start.
     * 
     * @param designPlan design plan
     */
    void start(DesignPlan designPlan);
    
    /**
     * Visit factor.
     * 
     * @param designPlan design plan
     * @param factorGroup factor group
     */
    void visit(DesignPlan designPlan, FactorGroup factorGroup);
    
    /**
     * End.
     * 
     * @param designPlan design plan
     */
    void end(DesignPlan designPlan);
}
