/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2012 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator.sensitivity;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

import fr.ifremer.isisfish.simulator.sensitivity.visitor.DesignPlanVisitor;

/**
 * Design plan contenant toutes les valeurs de facteurs possible.
 * 
 * It's equivalent to "experimentalDesign" in mexico.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class DesignPlan implements Serializable {

    /** serialVersionUID. */
    private static final long serialVersionUID = 977975461743758075L;

    /** Groupe de facteur principal du design plan. */
    protected FactorGroup factorGroup;

    /**
     * Constructor.
     */
    public DesignPlan() {
        factorGroup = new FactorGroup(null);
    }

    /**
     * Get factors list, first level of factor groups (not modifiable).
     * 
     * @return factors list
     */
    public List<Factor> getFactors() {
        return Collections.unmodifiableList(factorGroup.getFactors());
    }

    /**
     * Return design plan main factor group.
     * 
     * @return design plan factor group
     */
    public FactorGroup getFactorGroup() {
        return factorGroup;
    }

    /**
     * Add factor.
     * 
     * @param f factor to add
     */
    public void addFactor(Factor f) {
        factorGroup.addFactor(f);
    }

    /**
     * Set factors group.
     * 
     * @param factorGroup main factor group
     */
    public void setFactorGroup(FactorGroup factorGroup) {
        this.factorGroup = factorGroup;
    }

    /**
     * Accept a new visitor.
     * 
     * @param visitor
     */
    public void accept(DesignPlanVisitor visitor) {
        visitor.start(this);
        visitor.visit(this, factorGroup);
        visitor.end(this);
    }
}
