/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2012 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator.sensitivity.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;

import org.apache.commons.beanutils.MethodUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixND;

import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.simulator.sensitivity.Distribution;
import fr.ifremer.isisfish.simulator.sensitivity.Distribution.DistributionParam;
import fr.ifremer.isisfish.simulator.sensitivity.Domain;
import fr.ifremer.isisfish.simulator.sensitivity.visitor.DomainVisitor;

/**
 * All the continuous domains are based on distribution definition.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class ContinuousDomain implements Domain {

    private static Log log = LogFactory.getLog(ContinuousDomain.class);

    /** serialVersionUID. */
    private static final long serialVersionUID = -2037768174807839046L;

    /** Continuous domain distribution. */
    protected Distribution distribution;

    /** Distribution params (param name &gt; param value). */
    protected Map<String, Object> distributionParameters = new HashMap<>();

    /**
     * Default constructor.
     */
    public ContinuousDomain() {

    }

    /**
     * Constructor with distribution.
     * 
     * @param distribution distribution
     */
    public ContinuousDomain(Distribution distribution) {
        this();
        this.distribution = distribution;
    }

    /**
     * Constructor with distribution and parameters.
     * 
     * @param distribution distribution
     * @param distributionParameters distribution params
     */
    public ContinuousDomain(Distribution distribution, SortedMap<String, Object> distributionParameters) {
        this(distribution);
        setDistributionParams(distributionParameters);
    }

    /**
     * Get distribution.
     * 
     * @return distribution
     */
    public Distribution getDistribution() {
        return distribution;
    }

    /**
     * Get distribution parameters.
     * 
     * @return distribution parameters
     */
    public Map<String, Object> getDistributionParameters() {
        return distributionParameters;
    }

    /**
     * Return distribution parameter value by parameter name.
     * 
     * @param name name
     * @return param value
     */
    public Object getDistributionParameter(String name) {
        return distributionParameters.get(name);
    }

    /**
     * Set distribution parameters (clear previous).
     * 
     * @param distributionParameters distribution params
     */
    public void setDistributionParams(SortedMap<String, Object> distributionParameters) {
        distributionParameters.clear();
        addDistributionParams(distributionParameters);
    }

    /**
     * Add distribution parameters.
     * 
     * @param distributionParameters distribution parameters.
     */
    public void addDistributionParams(SortedMap<String, Object> distributionParameters) {
        for (Entry<String, Object> entry : distributionParameters.entrySet()) {
            addDistributionParam(entry.getKey(), entry.getValue());
        }
    }

    /**
     * Add distribution parameter.
     * 
     * Parameter name must be in current distribution parameter list.
     * 
     * @param name parameter name
     * @param value parameter value
     */
    public void addDistributionParam(String name, Object value) {
        
        if (distribution == null) {
            throw new IllegalStateException("Distribution not set");
        }

        // get current parameter definition
        boolean found = false;
        DistributionParam[] defDistribParams = distribution.getDistibutionParams();
        for (DistributionParam defDistribParam : defDistribParams) {
            if (defDistribParam.getName().equals(name)) {
                found = true;
                break;
            }
        }
        if (!found) {
            throw new IllegalArgumentException(String.format("Wrong parameter name, %s not supported by distribution", name));
        }

        distributionParameters.put(name, value);
    }

    @Override
    public Object getValueForIdentifier(Object identifier) {

        // first quantite args is always identifier value
        List<Object> args = new ArrayList<>();
        args.add(identifier);

        // build distribution param args list
        // QUNIFPC est un cas particulier dans toutes les distributions possibles
        if (distribution == Distribution.QUNIFPC) {
            // special management : % to min/max
            Object reference = distributionParameters.get(Distribution.QUNIFPC.getDistibutionParams()[0].getName());
            double coef = (Double)distributionParameters.get(Distribution.QUNIFPC.getDistibutionParams()[1].getName());
            if (reference instanceof MatrixND) {
                args.add(1.0 - coef); // min
                args.add(1.0 + coef); // max
            } else {
                double ref = (Double)reference;
                // Math.abs(ref) est utilisé pour avoir un resultat correct même avec une
                // valeur de référence négative
                args.add(ref - coef * Math.abs(ref)); // min
                args.add(ref + coef * Math.abs(ref)); // max
            }
        } else {
            for (DistributionParam param : distribution.getDistibutionParams()) {
                String name = param.getName();
                Object value = distributionParameters.get(name);
                args.add(value);
            }
        }

        // add default value for lower.tail = TRUE
        args.add(true);
        // add default value forlog.p = FALSE
        args.add(false);

        // invoke quantile method
        Class distClass = distribution.getDistClass();
        Object result = null;
        try {
            result = MethodUtils.invokeStaticMethod(distClass, "quantile", args.toArray());
        } catch (Exception ex) {
            if (log.isErrorEnabled()) {
                log.error("Can't invoke quantile method : args was " + args, ex);
            }
        }

        // pour les matrices, on retourne la matrice multipliée par la valeur données par R
        if (distribution == Distribution.QUNIFPC) {
            Object reference = distributionParameters.get(Distribution.QUNIFPC.getDistibutionParams()[0].getName());
            if (reference instanceof MatrixND) {
                MatrixND ref = ((MatrixND)reference).copy();
                result = ref.mults((Double)result);
            }
        }
        return result;
    }
    
    /**
     * Accept a new visitor.
     * 
     * @param visitor the visitor
     */
    public void accept(DomainVisitor visitor) {
        visitor.start(this);
        visitor.end(this);
    }
    
    @Override
    public ContinuousDomain clone() {
        ContinuousDomain cloned;
        try {
            cloned = (ContinuousDomain)super.clone();
        } catch (CloneNotSupportedException e) {
            throw new IsisFishRuntimeException("Can't clone domain", e);
        }
        return cloned;
    }
}
