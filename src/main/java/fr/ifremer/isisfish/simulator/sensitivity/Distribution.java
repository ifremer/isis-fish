/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator.sensitivity;

import static org.nuiton.i18n.I18n.t;
import jdistlib.Binomial;
import jdistlib.Cauchy;
import jdistlib.ChiSquare;
import jdistlib.Exponential;
import jdistlib.F;
import jdistlib.Gamma;
import jdistlib.Geometric;
import jdistlib.HyperGeometric;
import jdistlib.Normal;
import jdistlib.Uniform;

/**
 * Distribution used in sensitivity analysis (for continuous domain).
 * 
 * @author chatellier
 * @version $Revision$
 * @since 4.0.1.0
 * 
 * Last update : $Date$
 * By : $Author$
 */
public enum Distribution {

    QUNIFPC(t("QUnif %"),
            "runif",
            Uniform.class,
            new DistributionParam("reference", t("Reference value"), true),
            new DistributionParam("coefficient", t("Coefficient"), false)),
    QUNIFMM(t("QUnif Min/Max"),
            "runif",
            Uniform.class,
            new DistributionParam("min", t("Minimum value"), true),
            new DistributionParam("max", t("Maximum value"), true)),
    DBINOM(t("Dbimom"),
            "rbinom",
            Binomial.class,
            new DistributionParam("size", t("number of trials (zero or more)")),
            new DistributionParam("prob", t("probability of success on each trial"))),
    DCAUCHY(t("dcauchy"),
            "rcauchy",
            Cauchy.class,
            new DistributionParam("location", t("location and scale parameters. (location = 0, scale = 1)")),
            new DistributionParam("scale", t("location and scale parameters. (location = 0, scale = 1)"))),
    DCHISQ(t("dchisq"),
            "rchisq",
            ChiSquare.class,
            new DistributionParam("df", t("degrees of freedom (non-negative, but can be non-integer)"))),
    DEXP(t("dexp"),
            "rexp",
            Exponential.class,
            new DistributionParam("rate", t("vector of rates"))),
    DF(t("df"),
            "rf",
            F.class,
            new DistributionParam("df1", t("degrees of freedom. 'Inf' is allowed")),
            new DistributionParam("df2", t("degrees of freedom. 'Inf' is allowed")),
            new DistributionParam("ncp", t("non-centrality parameter. If omitted the central F is assumed"))),
    DGAMMA(t("dgamma"),
            "rgamma",
            Gamma.class,
            new DistributionParam("shape", t("shape and scale parameters. Must be positive, 'scale' strictly")),
            new DistributionParam("rate", t("an alternative way to specify the scale.")),
            new DistributionParam("scale", t("shape and scale parameters. Must be positive, 'scale' strictly"))),
    DGEOM(t("dgeom"),
            "rgeom",
            Geometric.class,
            new DistributionParam("prob", t("probability of success in each trial. '0 < prob <= 1'"))),
    DHYPER(t("dhyper"),
            "rhyper",
            HyperGeometric.class,
            new DistributionParam("m", t("the number of white balls in the urn.")),
            new DistributionParam("n", t("the number of black balls in the urn.")),
            new DistributionParam("k", t("the number of balls drawn from the urn."))),
    DLNORM(t("dlnorm"),
            "rlnorm",
            Normal.class,
            new DistributionParam("meanlog", t("mean and standard deviation of the distribution on the log scale with default values of '0' and '1' respectively."), false),
            new DistributionParam("sdlog", t("mean and standard deviation of the distribution on the log scale with default values of '0' and '1' respectively."), false));

    protected String description;

    protected String instruction;

    protected Class distClass;

    protected DistributionParam[] params;

    Distribution(String description, String instruction, Class distClass, DistributionParam... params) {
        this.description = description;
        this.instruction = instruction;
        this.distClass = distClass;
        this.params = params;
    }

    public String getDescription() {
        return description;
    }

    public String getInstruction() {
        return instruction;
    }

    public Class getDistClass() {
        return distClass;
    }

    public DistributionParam[] getDistibutionParams() {
        return params;
    }

    public static class DistributionParam {
        protected String name;

        protected String description;

        /** If original value must be set in UI (matrix). */
        protected boolean originalValue;

        public DistributionParam(String name, String description, boolean originalValue) {
            this.name = name;
            this.description = description;
            this.originalValue = originalValue;
        }
        
        public DistributionParam(String name, String description) {
            this(name, description, false);
        }

        public String getName() {
            return name;
        }

        public String getDescription() {
            return description;
        }

        public boolean isOriginalValue() {
            return originalValue;
        }
    }
}
