/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2010 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator.sensitivity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import fr.ifremer.isisfish.simulator.sensitivity.visitor.SensitivityScenariosVisitor;

/**
 * Ensemble de {@link Scenario}.
 *
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class SensitivityScenarios implements Serializable {

    /** serialVersionUID. */
    private static final long serialVersionUID = 4953721873692486687L;

    /**
     * Ensemble des scenarios.
     */
    protected List<Scenario> scenarios;
    
    /**
     * Constructeur.
     */
    public SensitivityScenarios() {
        scenarios = new ArrayList<>();
    }

    /**
     * Get scenarios.
     * 
     * @return the scenarios
     */
    public List<Scenario> getScenarios() {
        return scenarios;
    }

    /**
     * Set scenarios.
     * 
     * @param scenarios the scenarios
     */
    public void setScenarios(List<Scenario> scenarios) {
        this.scenarios = scenarios;
    }
    
    /**
     * Accept a new visitor.
     * 
     * @param visitor
     */
    public void accept(SensitivityScenariosVisitor visitor) {
        visitor.start(this);
        for(Scenario scenario : scenarios) {
            visitor.visit(this, scenario);
        }
        visitor.end(this);
    }
}
