/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2010 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator.sensitivity.domain;

import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.simulator.sensitivity.Domain;
import fr.ifremer.isisfish.simulator.sensitivity.visitor.DomainVisitor;

/**
 * Un domain discret a un ensemble de valeur.
 * Chaque valeur est identifiée par un label pour son utilisation par les
 * script d'AS.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class DiscreteDomain implements Domain {

    /** serialVersionUID. */
    private static final long serialVersionUID = -192647757737396585L;

    /**
     * Value for this domain.
     */
    protected SortedMap<Object, Object> values;

    /**
     * Constructor.
     */
    public DiscreteDomain() {
        values = new TreeMap<>();
    }

    /**
     * Get domain values.
     * 
     * @return the values
     */
    public SortedMap<Object, Object> getValues() {
        return values;
    }

    /**
     * Set domain values.
     * 
     * @param values the values
     */
    public void setValues(SortedMap<Object, Object> values) {
        this.values = values;
    }

    /**
     * Set domain value.
     * 
     * @param key the key
     * @param value the value
     */
    public void putValue(Object key, Object value) {
        this.values.put(key, value);
    }

    /**
     * Get domain value count.
     * 
     * @return domain value count
     */
    public int getValuesCount() {
        return values.size();
    }

    /**
     * {@inheritDoc}.
     * 
     * @throws IllegalArgumentException if identifier is not a valid key
     */
    public Object getValueForIdentifier(Object identifier)
            throws IllegalArgumentException {

        if (values == null || !values.containsKey(identifier)) {
            throw new IllegalArgumentException(
                    "Can't get value for identifier " + identifier);
        }

        return values.get(identifier);
    }

    /**
     * Accept a new visitor.
     * 
     * @param visitor
     */
    public void accept(DomainVisitor visitor) {
        visitor.start(this);
        for (Map.Entry<Object, Object> value : values.entrySet()) {
            visitor.visit(this, value.getKey(), value.getValue());
        }
        visitor.end(this);
    }

    @Override
    public DiscreteDomain clone() {
        DiscreteDomain cloned;
        try {
            cloned = (DiscreteDomain)super.clone();
            // special copy for values
            cloned.setValues(new TreeMap<>(this.values));
        } catch (CloneNotSupportedException e) {
            throw new IsisFishRuntimeException("Can't clone domain", e);
        }
        return cloned;
    }
}
