/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2011 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator.sensitivity;

import java.io.Serializable;

import fr.ifremer.isisfish.simulator.sensitivity.domain.ContinuousDomain;
import fr.ifremer.isisfish.simulator.sensitivity.domain.DiscreteDomain;
import fr.ifremer.isisfish.simulator.sensitivity.visitor.DomainVisitor;

/**
 * Domaine du facteur.
 * 
 * Ensemble des valeurs possibles a prendre en compte.
 * La clé est un label qui permet d'identifier la valeur.
 * 
 * Le domain peut etre :
 *  - discret : i.e un ensemble de valeurs
 *  - continu : i.e, une borne min, max
 * 
 * @see DiscreteDomain
 * @see ContinuousDomain
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public interface Domain extends Serializable, Cloneable {

    /**
     * Return value associated to identifier.
     * 
     * @param identifier
     * @return found value or {@code null} if not found
     */
    Object getValueForIdentifier(Object identifier);

    /**
     * Accept a new visitor.
     * 
     * @param visitor
     */
    void accept(DomainVisitor visitor);

    /**
     * Clone the domain
     * 
     * @return the domain cloned
     */
    Domain clone();

}
