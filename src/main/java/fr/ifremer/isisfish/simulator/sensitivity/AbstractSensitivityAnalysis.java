/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2012 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator.sensitivity;

import static org.nuiton.i18n.I18n.t;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.j2r.REngine;
import org.nuiton.j2r.RException;
import org.nuiton.j2r.RProxy;
import org.nuiton.math.matrix.MatrixND;

import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.export.SensitivityExport;
import fr.ifremer.isisfish.simulator.sensitivity.domain.ContinuousDomain;
import fr.ifremer.isisfish.simulator.sensitivity.domain.DiscreteDomain;

public abstract class AbstractSensitivityAnalysis implements
        SensitivityAnalysis {

    /** to use log facility, just put in your code: log.info("..."); */
    private static Log log = LogFactory.getLog(AbstractSensitivityAnalysis.class);

    /**
     * Method to create the isis.factors dataframe in R.
     * 
     * @param plan Design plan of the sensitivity analysis
     * @param outputdirectory the directory on which we will calculate the destination RData
     *            file. The RData file will be named directory.RData and saved
     *            in the directory parent.
     * 
     * @throws SensitivityException
     *             if an error occur while talking to R
     */
    public void setIsisFactorsR(DesignPlan plan, File outputdirectory)
            throws SensitivityException {

        String nomFacteur = "nomFacteur<-c(%s)";
        String factorNames = "";
        String nominal = "Nominal<-c(%s)";
        String nominalValues = "";
        String continu = "Continu<-c(%s)";
        String continuValues = "";
        String binf = "Binf<-c(%s)";
        String binfValues = "";
        String bsup = "Bsup<-c(%s)";
        String bsupValues = "";
        String attribute = "attr(isis.factors,\"%s\")<-\"%s\"";
        String isisfactors = "isis.factors<-data.frame("
                + "\"nomFacteur\"=nomFacteur," + "\"Nominal\"=Nominal,"
                + "\"Continu\"=Continu," + "\"Binf\"=Binf," + "\"Bsup\"=Bsup)";

        // Create the vectors
        List<Factor> factors = plan.getFactors();

        for (Factor factor : factors) {
            if (factor instanceof FactorGroup
                    && ((FactorGroup) factor).isDiscrete()) {
                List<Factor> subfactors = ((FactorGroup) factor).getFactors();
                int cardinality = subfactors.get(0).getCardinality();
                for (Factor subfactor : subfactors) {
                    if (subfactor.getCardinality() != cardinality) {
                        throw new SensitivityException(
                                "All the factors of group " + factor.getName()
                                        + " do not have the same cardinality");
                    }
                }
            }
        }

        for (int i = 0; i < factors.size(); i++) {
            Factor factor = factors.get(i);
            if (i != 0) {
                factorNames += ",";
                nominalValues += ",";
                continuValues += ",";
                binfValues += ",";
                bsupValues += ",";
            }

            // Factor names
            factorNames += "\"" + factor.getName() + "\"";

            if (factor.getDomain() instanceof ContinuousDomain) {
                // Continu values, binf values, bsup values and discrete factors
                // attributes

                nominalValues += "\"" + factor.getNominalValue() + "\"";

                // Continu values
                continuValues += "TRUE";

                // Binf values

                // FIXME echatellier to replace since 4.0.1
                binfValues += "0.0"; //((ContinuousDomain) factor.getDomain()).getCalculatorMinBound();
                //Bsup values bsupValues += ((ContinuousDomain)
                bsupValues += "0.0"; //factor.getDomain()) .getCalculatorMaxBound();

            } else {
                nominalValues += "\"" + factor.getNominalValue() + "\"";

                // Continu values
                continuValues += "FALSE";

                // Binf values
                binfValues += "0.0";

                // Bsup values
                bsupValues += ((DiscreteDomain) factor.getDomain()).getValues()
                        .size();
            }
        }

        String Rcall;

        try {
            REngine engine = new RProxy();

            // clear the engine (to be sure it's empty)
            engine.clearSession();

            Rcall = String.format(nomFacteur, factorNames);
            engine.voidEval(Rcall);

            Rcall = String.format(nominal, nominalValues);
            engine.voidEval(Rcall);

            Rcall = String.format(continu, continuValues);
            engine.voidEval(Rcall);

            Rcall = String.format(binf, binfValues);
            engine.voidEval(Rcall);

            Rcall = String.format(bsup, bsupValues);
            engine.voidEval(Rcall);

            engine.voidEval(isisfactors);

            Rcall = String.format(attribute, "nomModel", "isis-fish-externeR");
            engine.voidEval(Rcall);

            for (Factor factor : factors) {
                if (factor.getDomain() instanceof DiscreteDomain) {
                    String attributeValues = "list(";
                    Collection<Object> values = ((DiscreteDomain) factor
                            .getDomain()).getValues().values();
                    for (Object j : values) {
                        attributeValues += j + ",";
                    }
                    attributeValues = attributeValues.substring(0,
                            attributeValues.length() - 1);
                    attributeValues += ")";

                    Rcall = String.format(attribute, factor.getName(),
                            attributeValues);
                    engine.voidEval(Rcall);
                }
            }

            // Save the Isis R session
            engine.saveRData(outputdirectory.getParentFile(),
                    outputdirectory.getName());

        } catch (RException eee) {
            if (log.isErrorEnabled()) {
                log.error("R evaluation failed", eee);
            }
            throw new SensitivityException("R evaluation failed", eee);
            // Error while retrieving scenario
        }

    }

    /**
     * Open R engine and load RData file.
     * 
     * @param outputDirectory save directory
     * @return rengine
     * @throws RException
     */
    protected REngine openEngine(File outputDirectory) throws RException {
        REngine engine = new RProxy();

        // Clear session
        engine.clearSession();

        String sensitivityAnalysisName = outputDirectory.getName();
        // Get Isis R session
        engine.loadRData(outputDirectory.getParentFile(), sensitivityAnalysisName);
        
        // - is a non allowed character name in R
        String prefixName = sensitivityAnalysisName.replaceAll("-", "");

        // Rename R objects for manipulation purpose (if exists)
        String renameIsisSimule = "if (exists(\"%1$s.isis.simule\")) isis.simule<-%1$s.isis.simule";
        String renameIsisFactorDistribution = "if (exists(\"%1$s.isis.factor.distribution\")) isis.factor.distribution<-%1$s.isis.factor.distribution";
        String renameIsisFactor = "if (exists(\"%1$s.isis.factors\")) isis.factors<-%1$s.isis.factors";
        String renameIsisMethodExp = "if (exists(\"%1$s.isis.methodExp\")) isis.methodExp<-%1$s.isis.methodExp";
        String renameIsisMethodAnalyse = "if (exists(\"%1$s.isis.methodAnalyse\")) isis.methodAnalyse<-%1$s.isis.methodAnalyse";
        engine.voidEval(String.format(renameIsisSimule, prefixName));
        engine.voidEval(String.format(renameIsisFactorDistribution, prefixName));
        engine.voidEval(String.format(renameIsisFactor, prefixName));
        engine.voidEval(String.format(renameIsisMethodExp, prefixName));
        engine.voidEval(String.format(renameIsisMethodAnalyse, prefixName));

        return engine;
    }

    /**
     * Save RData file and close Rengine.
     * 
     * @param engine r engine
     * @param outputDirectory save directory
     * @throws RException
     */
    protected void closeEngine(REngine engine, File outputDirectory) throws RException {

        String sensitivityAnalysisName = outputDirectory.getName();
        // - is a non allowed character name in R
        String prefixName = sensitivityAnalysisName.replaceAll("-", "");

        // Rename R objects for saving purpose
        String renameIsisSimule = "%s.isis.simule<-isis.simule";
        String renameIsisFactorDistribution = "%s.isis.factor.distribution<-isis.factor.distribution";
        String renameIsisFactor = "%s.isis.factors<-isis.factors";
        String renameIsisMethodExp = "%s.isis.methodExp<-isis.methodExp";
        String renameIsisMethodAnalyse = "%s.isis.methodAnalyse<-isis.methodAnalyse";
        engine.voidEval(String.format(renameIsisSimule, prefixName));
        engine.voidEval(String.format(renameIsisFactorDistribution, prefixName));
        engine.voidEval(String.format(renameIsisFactor, prefixName));
        engine.voidEval(String.format(renameIsisMethodExp, prefixName));
        engine.voidEval(String.format(renameIsisMethodAnalyse, prefixName));

        // Clean temporary R objects
        for (String object : engine.ls()) {
            if (!object.startsWith(prefixName)) {
                engine.remove(object);
            }
        }

        // Save Isis R session
        engine.saveRData(outputDirectory.getParentFile(), sensitivityAnalysisName);

        engine.terminate();
    }

    /**
     * Affiche une boite de dialogue modale permettant de modifier l'expression
     * R.
     * 
     * @param rCall r instruction to edit
     * @return user edited r instruction
     */
    protected String editRInstruction(String rCall) {
        JLabel label = new JLabel(t("Modifier le code R envoyé si vous le souhaitez"));
        JTextPane text = new JTextPane();
        text.setText(rCall);
        text.setSize(400, 400);
        text.setPreferredSize(text.getSize());

        Box box = Box.createVerticalBox();
        box.add(label);
        box.add(new JScrollPane(text));

        JOptionPane.showMessageDialog(null, box, t("R modif"), JOptionPane.QUESTION_MESSAGE);
        return text.getText();
    }

    /**
     * Test que tous les facteurs sont continues. Leve une exception si un
     * facteur discret est présent.
     * 
     * @param factors factor list to check
     * @throws SensitivityException
     */
    protected void checkAllFactorContinuous(List<Factor> factors)
            throws SensitivityException {
        for (Factor factor : factors) {
            if (factor.getDomain() instanceof DiscreteDomain) {
                throw new SensitivityException(t("%s has a discrete domain, this is not acceptable for this method.", factor.getName()));
            }
        }
    }

    /**
     * Test que tous les facteurs sont continues. Leve une exception si un
     * facteur discret est présent.
     * 
     * @param factors factor list to check
     * @throws SensitivityException
     */
    protected void checkAllUniformDistribution(List<Factor> factors)
            throws SensitivityException {
        for (Factor factor : factors) {
            if (factor.getDomain() instanceof ContinuousDomain) {
                ContinuousDomain domain = (ContinuousDomain)factor.getDomain();
                Distribution distribution = domain.getDistribution();
                if (distribution != Distribution.QUNIFPC && distribution != Distribution.QUNIFMM) {
                    throw new SensitivityException(t("%s has a non uniform distribution, this is not acceptable for this method.", factor.getName()));
                }
            } else {
                throw new SensitivityException(t("%s has a discrete domain, this is not acceptable for this method.", factor.getName()));
            }
        }
    }

    /**
     * Cree l'instruction R qui permet de relire le contenu d'un fichier genere
     * par un Export Isis.
     * 
     * @param export export to get filename to read
     * @param simulationStorages simulation list to read export
     * @return r instruction
     */
    protected String createImportInstruction(SensitivityExport export,
            List<SimulationStorage> simulationStorages) {

        // the filename to read
        String name = export.getExportFilename();
        // the extension to read
        String extension = export.getExtensionFilename();
        // the rInstruction
        String rInstruction = name + "<-c(";

        // iterate on all the simulations and read the value for each one
        for (int l = 0; l < simulationStorages.size(); l++) {

            SimulationStorage simulationStorage = simulationStorages.get(l);
            File directory = simulationStorage.getDirectory();

            String directoryPath = directory.toString() + File.separator
                    + SimulationStorage.RESULT_EXPORT_DIRECTORY;

           
            File importFile = new File(directoryPath, name + extension);

            String simulResult = "";
            try {
                simulResult = FileUtils.readFileToString(importFile, StandardCharsets.UTF_8);
            } catch (IOException ioe) {
                log.error("An error occured trying to read a result file : ", ioe);
            }

            double simulationResult = Double.parseDouble(simulResult);

            if (l < simulationStorages.size() - 1) {
                rInstruction += simulationResult + ",";
            } else {
                rInstruction += simulationResult;
            }
        }
        rInstruction += ")";

        return rInstruction;
    }

    /**
     * Get factor domain min bound depending on distribution.
     */
    protected double getMinBound(Factor factor) {
        return getBound(factor, true);
    }

    /**
     * Get factor domain max bound depending on distribution.
     */
    protected double getMaxBound(Factor factor) {
        return getBound(factor, false);
    }
    
    /**
     * Get factor domain bound depending on distribution.
     * 
     * @param min min or max
     */
    protected double getBound(Factor factor, boolean min) {
        ContinuousDomain domain = (ContinuousDomain)factor.getDomain();
        Distribution distribution = domain.getDistribution();
        double result = 0;
        if (distribution == Distribution.QUNIFPC) {
            Object reference = domain.getDistributionParameter(Distribution.QUNIFPC.getDistibutionParams()[0].getName());
            if (reference instanceof MatrixND) {
                result = min ? 0 : 1;
            } else {
                Double coefficient = (Double)domain.getDistributionParameter(Distribution.QUNIFPC.getDistibutionParams()[1].getName());
                if (min) {
                    result = (Double)reference - ((Double)reference * coefficient);
                } else {
                    result = (Double)reference + ((Double)reference * coefficient);
                }
            }
        } else if (distribution == Distribution.QUNIFMM) {
            Object value = domain.getDistributionParameter(Distribution.QUNIFMM.getDistibutionParams()[min ? 0 : 1].getName());
            // pour les matrices, on prend 1 et on multipliera le retour
            // de R sur la matrice pour faire varier la matrice completement
            // note: on ne fait pas ca pour les réels car les utilisateurs
            // prefere avoir les valeurs réelles dans le RData.
            if (value instanceof MatrixND) {
                result = min ? 0 : 1;
            } else {
                result = (Double)value;
            }
        } else {
            if (log.isWarnEnabled()) {
                log.warn("Can't call getBound for distribution " + distribution);
            }
        }
        return result;
    }

    protected String getIsisFactorDistribution(List<Factor> factors){
        String isisFactorDistribution = "isis.factor.distribution<-" +
                "data.frame(NomFacteur=c(%s)," +
                "NomDistribution=c(%s)," +
                "ParametreDistribution=c(%s))";

        // Creating the vectors.
        String distribution = "";
        String parameters = "";
        String factorNames = "";

        for (int i = 0; i < factors.size(); i++) {
            Factor factor = factors.get(i);
            ContinuousDomain domain = (ContinuousDomain)factor.getDomain();
            if (i != 0) {
                distribution += ",";
                parameters += ",";
                factorNames += ",";
            }

            Distribution r_distribution = domain.getDistribution();

            distribution += "\""+ r_distribution.getInstruction() +"\"";
            parameters += "\"[";

            for (Map.Entry<String,Object> param:domain.getDistributionParameters().entrySet()){
                parameters += param.getKey() + "=" + param.getValue() + ";";
            }

            parameters = StringUtils.removeEnd(parameters, ";");
            parameters += "]\"";

            factorNames += "\"" + factor.getName() + "\"";
        }

        return String.format(isisFactorDistribution, factorNames, distribution, parameters);
    }
}
