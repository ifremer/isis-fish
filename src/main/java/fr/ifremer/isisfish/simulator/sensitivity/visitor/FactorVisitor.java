/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2010 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator.sensitivity.visitor;

import fr.ifremer.isisfish.simulator.sensitivity.Domain;
import fr.ifremer.isisfish.simulator.sensitivity.Factor;

/**
 * Factor visitor.
 *
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public interface FactorVisitor {

    /**
     * Start.
     * 
     * @param factor factor
     */
    void start(Factor factor);
    
    /**
     * Visit domain.
     * 
     * @param factor factor
     * @param domain domain
     */
    void visit(Factor factor, Domain domain);
    
    /**
     * End.
     * 
     * @param factor factor
     */
    void end(Factor factor);
}
