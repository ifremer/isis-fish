/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2007 - 2011 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator;

import org.nuiton.math.matrix.MatrixND;

import fr.ifremer.isisfish.types.TimeStep;
import java.util.List;

/**
 * Some SimulationResultListener can implement this interface to show that can
 * be used to retrieve result
 * 
 * @author poussin
 */
public interface SimulationResultGetter {

    /**
     * Retourne la matrice stocke pour un pas de temps.
     *
     * @param step le pas de temps que l'on souhaite
     * @param name le nom des resultats dont on veut la matrice
     * @return La matrice demandée ou null si aucune matrice ne correspond a
     * la demande.
     */
    MatrixND getMatrix(SimulationContext context, TimeStep step, String name);

    /**
     * Retourne la matrice stocke pour un ensemble de pas de temps.
     *
     * @param steps les pas de temps que l'on souhaite
     * @param name le nom des resultats dont on veut la matrice
     * @return La matrice demandée ou null si aucune matrice ne correspond a
     * la demande.
     */
    MatrixND getMatrix(SimulationContext context, List<TimeStep> steps, String name);

     /**
      * Retourne une matrice contenant tous les pas de temps.
      * 
      * @param name le nom des resultats dont on veut une matrice globale.
      */
     MatrixND getMatrix(SimulationContext context, String name);
     
}


