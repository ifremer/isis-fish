/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2018 Ifremer, Code Lutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.lang.ref.SoftReference;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import fr.ifremer.isisfish.util.IsisFileUtil;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.topia.TopiaException;

import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.datastore.RegionStorage;
import fr.ifremer.isisfish.entities.Observation;
import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.entities.Strategy;
import fr.ifremer.isisfish.export.ExportInfo;
import fr.ifremer.isisfish.export.SensitivityExport;
import fr.ifremer.isisfish.rule.Rule;
import fr.ifremer.isisfish.simulator.sensitivity.SensitivityAnalysis;

/**
 * SimulationParameter soft reference implementation.
 * 
 * Store simulation file on disk, and a soft reference of delegate real
 * SimulationParameter.
 * 
 * If soft reference has been cleared by garbage collector, reload it from
 * disk.
 * 
 * Warning, use only this class for parameter reading. Using setters cause
 * parameters to be written on disk. Use with caution.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class SimulationParameterCache implements SimulationParameter {

    /** Parameter file (disk storage). */
    protected File paramFile;

    /** Parameter memory reference. */
    protected SoftReference<SimulationParameter> ref;

    /**
     * 
     * @param param param to cache
     */
    public SimulationParameterCache(SimulationParameter param) {

        try {
            paramFile = IsisFileUtil.createTempFile("isis-fish-param", ".properties");
            paramFile.deleteOnExit();
            store(param);
        } catch (IOException eee) {
            throw new IsisFishRuntimeException("Can't cache param to disk", eee);
        }

        ref = new SoftReference<>(param);
    }

    protected void store(SimulationParameter param) {
        Properties prop = param.toProperties();
        try (Writer writer = new BufferedWriter(new FileWriter(paramFile))) {
            prop.store(writer, "Parameter cache");
        } catch (IOException eee) {
            throw new IsisFishRuntimeException("Can't cache param to disk", eee);
        }
    }
    
    protected SimulationParameter getParam() {
        SimulationParameter result = ref.get();
        if (result == null) {
            try (Reader reader = new BufferedReader(new FileReader(paramFile))) {
                Properties prop = new Properties();
                prop.load(reader);
                result = new SimulationParameterImpl();
                result.fromProperties(prop);
                ref = new SoftReference<>(result);
            } catch (IOException eee) {
                throw new IsisFishRuntimeException("Can't cache param to disk", eee);
            }
        }
        return result;
    }

    @Override
    public List<String> check() {
        SimulationParameter param = getParam();
        return param.check();
    }

    @Override
    public String getIsisFishVersion() {
        SimulationParameter param = getParam();
        return param.getIsisFishVersion();
    }

    @Override
    public void setIsisFishVersion(String isisFishVersion) {
        SimulationParameter param = getParam();
        param.setIsisFishVersion(isisFishVersion);
        store(param);
    }

    @Override
    public String getDescription() {
        SimulationParameter param = getParam();
        return param.getDescription();
    }

    @Override
    public void setDescription(String description) {
        SimulationParameter param = getParam();
        param.setDescription(description);
        store(param);
    }

    @Override
    public RegionStorage getRegion() {
        SimulationParameter param = getParam();
        return param.getRegion();
    }

    @Override
    public MatrixND getNumberOf(Population pop) {
        SimulationParameter param = getParam();
        return param.getNumberOf(pop);
    }

    @Override
    public List<String> getExtraRules() {
        SimulationParameter param = getParam();
        return param.getExtraRules();
    }

    @Override
    public void addExtraRules(String... extraRules) {
        SimulationParameter param = getParam();
        param.addExtraRules(extraRules);
        store(param);
    }

    @Override
    public List<Population> getPopulations() {
        SimulationParameter param = getParam();
        return param.getPopulations();
    }

    @Override
    public void setPopulations(List<Population> populations) {
        SimulationParameter param = getParam();
        param.setPopulations(populations);
        store(param);
    }

    @Override
    public List<Strategy> getStrategies() {
        SimulationParameter param = getParam();
        return param.getStrategies();
    }

    @Override
    public void setStrategies(List<Strategy> strategies) {
        SimulationParameter param = getParam();
        param.setStrategies(strategies);
        store(param);
    }

    @Override
    public void addSimulationPlan(SimulationPlan plan) {
        SimulationParameter param = getParam();
        param.addSimulationPlan(plan);
        store(param);
    }

    @Override
    public boolean removeSimulationPlan(SimulationPlan plan) {
        SimulationParameter param = getParam();
        boolean result = param.removeSimulationPlan(plan);
        store(param);
        return result;
    }

    @Override
    public List<SimulationPlan> getSimulationPlans() {
        SimulationParameter param = getParam();
        return param.getSimulationPlans();
    }

    @Override
    public void setSimulationPlans(List<SimulationPlan> plans) {
        SimulationParameter param = getParam();
        param.setSimulationPlans(plans);
        store(param);
    }

    @Override
    public boolean isIndependentPlan() {
        SimulationParameter param = getParam();
        return param.isIndependentPlan();
    }

    @Override
    public void clearPlans() {
        SimulationParameter param = getParam();
        param.clearPlans();
        store(param);
    }

    @Override
    public void addRule(Rule rule) {
        SimulationParameter param = getParam();
        param.addRule(rule);
        store(param);
    }

    @Override
    public boolean removeRule(Rule rule) {
        SimulationParameter param = getParam();
        boolean result = param.removeRule(rule);
        store(param);
        return result;
    }

    @Override
    public List<Rule> getRules() {
        SimulationParameter param = getParam();
        return param.getRules();
    }

    @Override
    public void setRules(List<Rule> rules) {
        SimulationParameter param = getParam();
        param.setRules(rules);
        store(param);
    }

    @Override
    public void clearRules() {
        SimulationParameter param = getParam();
        param.clearRules();
        store(param);
    }

    @Override
    public boolean getUseOptimization() {
        SimulationParameter param = getParam();
        return param.getUseOptimization();
    }

    @Override
    public void setUseOptimization(boolean useOptimization) {
        SimulationParameter param = getParam();
        param.setUseOptimization(useOptimization);
        store(param);
    }

    @Override
    public boolean getUseComputeResult() {
        SimulationParameter param = getParam();
        return param.getUseComputeResult();
    }

    @Override
    public void setUseComputeResult(boolean useComputeResult) {
        SimulationParameter param = getParam();
        param.setUseComputeResult(useComputeResult);
        store(param);
    }

    @Override
    public boolean getUseStatistic() {
        SimulationParameter param = getParam();
        return param.getUseStatistic();
    }

    @Override
    public void setUseStatistic(boolean useStatistic) {
        SimulationParameter param = getParam();
        param.setUseStatistic(useStatistic);
        store(param);
    }

    @Override
    public List<String> getExportNames() {
        SimulationParameter param = getParam();
        return param.getExportNames();
    }

    @Override
    public void setExportNames(List<String> exportNames) {
        SimulationParameter param = getParam();
        param.setExportNames(exportNames);
        store(param);
    }

    @Override
    public int getNumberOfSensitivitySimulation() {
        SimulationParameter param = getParam();
        return param.getNumberOfSensitivitySimulation();
    }

    @Override
    public void setNumberOfSensitivitySimulation(
            int numberOfSensitivitySimulation) {
        SimulationParameter param = getParam();
        param.setNumberOfSensitivitySimulation(numberOfSensitivitySimulation);
        store(param);
    }

    @Override
    public SensitivityAnalysis getSensitivityAnalysis() {
        SimulationParameter param = getParam();
        return param.getSensitivityAnalysis();
    }

    @Override
    public void setSensitivityAnalysis(SensitivityAnalysis sensitivityAnalysis) {
        SimulationParameter param = getParam();
        param.setSensitivityAnalysis(sensitivityAnalysis);
        store(param);
    }

    @Override
    public List<SensitivityExport> getSensitivityExport() {
        SimulationParameter param = getParam();
        return param.getSensitivityExport();
    }

    @Override
    public void setSensitivityExport(List<SensitivityExport> sensitivityExport) {
        SimulationParameter param = getParam();
        param.setSensitivityExport(sensitivityExport);
        store(param);
    }

    @Override
    public boolean getUseSimulationPlan() {
        SimulationParameter param = getParam();
        return param.getUseSimulationPlan();
    }

    @Override
    public void setUseSimulationPlan(boolean useSimulationPlan) {
        SimulationParameter param = getParam();
        param.setUseSimulationPlan(useSimulationPlan);
        store(param);
    }

    @Override
    public int getSimulationPlanNumber() {
        SimulationParameter param = getParam();
        return param.getSimulationPlanNumber();
    }

    @Override
    public void setSimulationPlanNumber(int simulationPlanNumber) {
        SimulationParameter param = getParam();
        param.setSimulationPlanNumber(simulationPlanNumber);
        store(param);
    }

    @Override
    public int getOptimizationGeneration() {
        SimulationParameter param = getParam();
        return param.getOptimizationGeneration();
    }

    @Override
    public void setOptimizationGeneration(int optimizationGeneration) {
        SimulationParameter param = getParam();
        param.setOptimizationGeneration(optimizationGeneration);
        store(param);
    }

    @Override
    public int getOptimizationGenerationIndividual() {
        SimulationParameter param = getParam();
        return param.getOptimizationGenerationIndividual();
    }

    @Override
    public void setOptimizationGenerationIndividual(int optimizationGenerationIndividual) {
        SimulationParameter param = getParam();
        param.setOptimizationGenerationIndividual(optimizationGenerationIndividual);
        store(param);
    }

    @Override
    public boolean isSensitivityAnalysisOnlyKeepFirst() {
        SimulationParameter param = getParam();
        return param.isSensitivityAnalysisOnlyKeepFirst();
    }

    @Override
    public void setSensitivityAnalysisOnlyKeepFirst(boolean onlyKeepFirst) {
        SimulationParameter param = getParam();
        param.setSensitivityAnalysisOnlyKeepFirst(onlyKeepFirst);
        store(param);
    }

    @Override
    public boolean isResultDeleteAfterExport() {
        SimulationParameter param = getParam();
        return param.isResultDeleteAfterExport();
    }
    
    @Override
    public void setResultDeleteAfterExport(boolean deleteAfterExport) {
        SimulationParameter param = getParam();
        param.setResultDeleteAfterExport(deleteAfterExport);
        store(param);
    }

    @Override
    public int getNumberOfYear() {
        SimulationParameter param = getParam();
        return param.getNumberOfYear();
    }

    @Override
    public void setNumberOfYear(int numberOfYear) {
        SimulationParameter param = getParam();
        param.setNumberOfYear(numberOfYear);
        store(param);
    }

    @Override
    public int getNumberOfMonths() {
        SimulationParameter param = getParam();
        return param.getNumberOfMonths();
    }

    @Override
    public void setNumberOfMonths(int numberOfMonths) {
        SimulationParameter param = getParam();
        param.setNumberOfMonths(numberOfMonths);
        store(param);
    }

    @Override
    public boolean getUsePreScript() {
        SimulationParameter param = getParam();
        return param.getUsePreScript();
    }

    @Override
    public void setUsePreScript(boolean usePreScript) {
        SimulationParameter param = getParam();
        param.setUsePreScript(usePreScript);
        store(param);
    }

    @Override
    public String getPreScript() {
        SimulationParameter param = getParam();
        return param.getPreScript();
    }

    @Override
    public void setPreScript(String preScript) {
        SimulationParameter param = getParam();
        param.setPreScript(preScript);
        store(param);
    }

    @Override
    public String getGeneratedPreScript() {
        SimulationParameter param = getParam();
        return param.getGeneratedPreScript();
    }

    @Override
    public void setGeneratedPreScript(String preScript) {
        SimulationParameter param = getParam();
        param.setGeneratedPreScript(preScript);
        store(param);
    }

    @Override
    public String getRegionName() {
        SimulationParameter param = getParam();
        return param.getRegionName();
    }

    @Override
    public void setRegionName(String regionName) {
        SimulationParameter param = getParam();
        param.setRegionName(regionName);
        store(param);
    }

    @Override
    public String getSimulatorName() {
        SimulationParameter param = getParam();
        return param.getSimulatorName();
    }

    @Override
    public void setSimulatorName(String simulatorName) {
        SimulationParameter param = getParam();
        param.setSimulatorName(simulatorName);
        store(param);
    }

    @Override
    public Collection<String> getResultEnabled() {
        SimulationParameter param = getParam();
        return param.getResultEnabled();
    }

    @Override
    public void setResultEnabled(Collection<String> resultEnabled) {
        SimulationParameter param = getParam();
        param.setResultEnabled(resultEnabled);
        store(param);
    }

    @Override
    public Map<String, String> getTagValue() {
        SimulationParameter param = getParam();
        return param.getTagValue();
    }

    @Override
    public void setTagValue(Map<String, String> tagValue) {
        SimulationParameter param = getParam();
        param.setTagValue(tagValue);
        store(param);
    }

    @Override
    public String getSimulLogLevel() {
        SimulationParameter param = getParam();
        return param.getSimulLogLevel();
    }

    @Override
    public void setSimulLogLevel(String logLevel) {
        SimulationParameter param = getParam();
        param.setSimulLogLevel(logLevel);
        store(param);
    }

    @Override
    public String getScriptLogLevel() {
        SimulationParameter param = getParam();
        return param.getScriptLogLevel();
    }

    @Override
    public void setScriptLogLevel(String logLevel) {
        SimulationParameter param = getParam();
        param.setScriptLogLevel(logLevel);
        store(param);
    }

    @Override
    public String getLibLogLevel() {
        SimulationParameter param = getParam();
        return param.getLibLogLevel();
    }

    @Override
    public void setLibLogLevel(String logLevel) {
        SimulationParameter param = getParam();
        param.setLibLogLevel(logLevel);
        store(param);
    }

    @Override
    public boolean isSimulErrorLevel() {
        SimulationParameter param = getParam();
        return param.isSimulErrorLevel();
    }

    @Override
    public boolean isSimulWarnLevel() {
        SimulationParameter param = getParam();
        return param.isSimulErrorLevel();
    }

    @Override
    public boolean isSimulInfoLevel() {
        SimulationParameter param = getParam();
        return param.isScriptInfoLevel();
    }

    @Override
    public boolean isSimulDebugLevel() {
        SimulationParameter param = getParam();
        return param.isSimulDebugLevel();
    }

    @Override
    public boolean isScriptErrorLevel() {
        SimulationParameter param = getParam();
        return param.isScriptErrorLevel();
    }

    @Override
    public boolean isScriptWarnLevel() {
        SimulationParameter param = getParam();
        return param.isScriptWarnLevel();
    }

    @Override
    public boolean isScriptInfoLevel() {
        SimulationParameter param = getParam();
        return param.isScriptInfoLevel();
    }

    @Override
    public boolean isScriptDebugLevel() {
        SimulationParameter param = getParam();
        return param.isScriptDebugLevel();
    }

    @Override
    public boolean isLibErrorLevel() {
        SimulationParameter param = getParam();
        return param.isLibErrorLevel();
    }

    @Override
    public boolean isLibWarnLevel() {
        SimulationParameter param = getParam();
        return param.isLibWarnLevel();
    }

    @Override
    public boolean isLibInfoLevel() {
        SimulationParameter param = getParam();
        return param.isLibInfoLevel();
    }

    @Override
    public boolean isLibDebugLevel() {
        SimulationParameter param = getParam();
        return param.isLibDebugLevel();
    }

    @Override
    public void setProperty(String key, String value) {
        SimulationParameter param = getParam();
        param.setProperty(key, value);
    }

    @Override
    public void setProperties(Properties props) {
        SimulationParameter param = getParam();
        param.setProperties(props);
    }

    @Override
    public SimulationParameter copy() {
        SimulationParameter param = getParam();
        return param.copy();
    }

    @Override
    public SimulationParameter deepCopy() {
        SimulationParameter param = getParam();
        return param.deepCopy();
    }

    @Override
    public Properties toProperties() {
        SimulationParameter param = getParam();
        return param.toProperties();
    }

    @Override
    public void fromProperties(Properties props) {
        SimulationParameter param = getParam();
        param.fromProperties(props);
        store(param);
    }

    @Override
    public void reloadContextParameters() throws TopiaException {
        SimulationParameter param = getParam();
        param.reloadContextParameters();
        store(param);
    }

    @Override
    public void reloadRegionChangeParameter() {
        SimulationParameter param = getParam();
        param.reloadRegionChangeParameter();
        store(param);
    }

    @Override
    public Optimization getOptimization() {
        SimulationParameter param = getParam();
        return param.getOptimization();
    }

    @Override
    public Objective getObjective() {
        SimulationParameter param = getParam();
        return param.getObjective();
    }

    @Override
    public void setOptimization(Optimization optimization) {
        SimulationParameter param = getParam();
        param.setOptimization(optimization);
        store(param);
    }

    @Override
    public void setObjective(Objective objective) {
        SimulationParameter param = getParam();
        param.setObjective(objective);
        store(param);
    }

    @Override
    public Map<ExportInfo, Observation> getOptimizationExportsObservations() {
        SimulationParameter param = getParam();
        return param.getOptimizationExportsObservations();
    }

    @Override
    public void setOptimizationExportsObservations(Map<ExportInfo, Observation> exportsObservations) {
        SimulationParameter param = getParam();
        param.setOptimizationExportsObservations(exportsObservations);
        store(param);
    }

    @Override
    public boolean getUseCache() {
        SimulationParameter param = getParam();
        return param.getUseCache();
    }

    @Override
    public void setUseCache(boolean useCache) {
        SimulationParameter param = getParam();
        param.setUseCache(useCache);
        store(param);
    }

    @Override
    public void fixReloadContext(boolean sensitivityContext) {
        SimulationParameter param = getParam();
        param.fixReloadContext(sensitivityContext);
        store(param);
    }
}
