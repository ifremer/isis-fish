/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2007 - 2014 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator;

import static org.nuiton.i18n.I18n.t;

import org.apache.commons.lang3.StringUtils;
import org.nuiton.topia.TopiaContext;

import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.types.TimeStep;
import fr.ifremer.isisfish.util.EvaluatorHelper;

/**
 * SimulationPreScript.
 * 
 * Created: 14 nov. 07 00:06:18
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class SimulationPreScriptListener implements SimulationListener {

    @Override
    public void afterSimulation(SimulationContext context) {
    }

    @Override
    public void beforeSimulation(SimulationContext context) {
        try {
            context.message(t("isisfish.message.presimulation.script.execution"));

            SimulationStorage simulation = context.getSimulationStorage();
            SimulationParameter parameters = simulation.getParameter();

            String generatedPreScript = parameters.getGeneratedPreScript();
            String presimulationScript = parameters.getPreScript();
            
            if (StringUtils.isNotBlank(generatedPreScript) ||
                    (parameters.getUsePreScript() && StringUtils.isNotBlank(presimulationScript))) {

                // utilisation de la db en memoire que l'on commitera
                TopiaContext tx = context.getDB();

                if (StringUtils.isNotBlank(generatedPreScript)) {
                    EvaluatorHelper.evaluate(SimulationPreScript.class.getPackage().getName(),
                            "GeneratedPreScript", SimulationPreScript.class, generatedPreScript, context, tx);
                }

                if (parameters.getUsePreScript() && StringUtils.isNotBlank(presimulationScript)) {
                    EvaluatorHelper.evaluate(SimulationPreScript.class.getPackage().getName(),
                            "PreScript", SimulationPreScript.class, presimulationScript, context, tx);
                }

                tx.commitTransaction();
            }
        } catch (Exception eee) {
            throw new SimulationException(
                    t("Can't evaluate simulation prescript"), eee);
        }
    }

    @Override
    public void stepChange(SimulationContext context, TimeStep step) {
    }
}
