/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2007 - 2010 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator;

import fr.ifremer.isisfish.types.TimeStep;

/**
 * You can create new SimulationListener and add it for simulation. To do that
 * add tag/value in advanced parameter simulation launcher like
 * 
 * tag: SimulationListener
 * value: fr.ifremer.simulator.SimulationResultXML(outFilename="/tmp/export-isis/mexico.xml"), ...
 * 
 * @author poussin
 */
public interface SimulationListener {

    /**
     * called before simulation
     * 
     * @param context
     */
    void beforeSimulation(SimulationContext context);

    /**
     * Receive event when simulation change step
     * 
     * @param context
     * @param step new Step
     */
    void stepChange(SimulationContext context, TimeStep step);

    /**
     * called after simulation
     * 
     * @param context
     */
    void afterSimulation(SimulationContext context);
    
}


