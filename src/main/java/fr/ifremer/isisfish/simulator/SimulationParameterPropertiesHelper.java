package fr.ifremer.isisfish.simulator;

/*
 * #%L
 * IsisFish
 * %%
 * Copyright (C) 2014 Ifremer, Codelutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.ifremer.isisfish.types.Month;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixHelper;
import org.nuiton.version.Versions;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Stream;

import static org.nuiton.i18n.I18n.t;

/**
 * Class utilitaire permettant de travailler avec les parametres sous forme de 
 * properties
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class SimulationParameterPropertiesHelper {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    final static private Log log = LogFactory.getLog(SimulationParameterPropertiesHelper.class);

    final public static String DOT = ".";
    final public static String LIST_SEPARATOR = ",";

    final public static String ISIS_FISH_VERSION_KEY = "isisFishVersion";
    final public static String SIMULATOR_NAME_KEY = "simulatorName";
    final public static String POPULATIONS_KEY = "populations";
    final public static String RULES_KEY = "rules";
    final public static String RULE_KEY = "rule";
    final public static String RESULT_NAMES_KEY = "resultNames";
    final public static String EXPORTS_KEY = "exports";
    final public static String PLANS_KEY = "plans";
    final public static String PLAN_KEY = "plan";
    final public static String STRATEGIES_KEY = "strategies";
    final public static String DESCRIPTION_KEY = "description";
    final public static String REGION_NAME_KEY = "regionName";
    final public static String USE_SIMULATION_PLAN_KEY = "useSimulationPlan";
    final public static String SIMULATION_PLAN_NUMBER_KEY = "simulationPlanNumber";
    final public static String USE_OPTIMIZATION_KEY = "useOptimization";
    final public static String OPTIMIZATION_KEY = "optimization";
    final public static String OPTIMIZATION_GENERATION_KEY = "optimizationGeneration";
    final public static String OPTIMIZATION_GENERATION_INDIVIDUAL_KEY = "optimizationGenerationIndividual";
    final public static String OBJECTIVE_KEY = "objective";
    final public static String OPTIMIZATION_EXPORTS_KEY = "optimizationexports";
    final public static String OPTIMIZATION_OBSERVATION_KEY = "optimizationobservation";
    final public static String USE_CACHE_KEY = "useCache";
    final public static String USE_STATISTIC_KEY = "useStatistic";
    final public static String USE_COMPUTE_RESULT_KEY = "useComputeResult";
    final public static String NUMBER_OF_YEAR_KEY = "numberOfYear";
    final public static String NUMBER_OF_MONTHS_KEY = "numberOfMonths";
    final public static String GENERATED_PRE_SCRIPT_KEY = "generatedPreScript";
    final public static String USE_PRE_SCRIPT_KEY = "usePreScript";
    final public static String PRE_SCRIPT_KEY = "preScript";
    final public static String SIMUL_LOG_LEVEL_KEY = "simulLogLevel";
    final public static String SCRIPT_LOG_LEVEL_KEY = "scriptLogLevel";
    final public static String LIB_LOG_LEVEL_KEY = "libLogLevel";
    final public static String SENSITIVITY_ANALYSIS_KEY = "sensitivityanalysis";
    final public static String SENSITIVITY_KEY = "sensitivity";
    final public static String SENSITIVITY_EXPORTS_KEY = "sensitivityexports";
    final public static String SENSITIVITY_EXPORT_KEY = "sensitivityexport";
    final public static String POPULATION_KEY = "population";
    final public static String NUMBER_KEY = "number";
    final public static String NUMBER_OF_SENSITIVITY_SIMULATION_KEY = "numberOfSensitivitySimulation";
    final public static String SENSITIVITY_ANALYSIS_ONLY_KEEP_FIRST_KEY = "sensitivityAnalysisOnlyKeepFirst";
    final public static String RESULT_DELETE_AFTER_EXPORT_KEY = "resultDeleteAfterExport";
    final public static String TAG_VALUE_KEY = "tagValue";
    final public static String PARAMETERS_KEY = "parameters";
    final public static String PARAMETER_KEY = "parameter";

    public static String getIsisFishVersion(Properties prop) {
        String result = prop.getProperty(ISIS_FISH_VERSION_KEY, "");
        return result;
    }

    public static String getSimulatorName(Properties prop) {
        String result = prop.getProperty(SIMULATOR_NAME_KEY, "DefaultSimulator.java");
        return result;
    }

    public static String getDescription(Properties prop) {
        String result = prop.getProperty(DESCRIPTION_KEY, "");
        return result;
    }

    public static boolean getUseSimulationPlan(Properties prop) {
        boolean result = Boolean.parseBoolean(prop.getProperty(USE_SIMULATION_PLAN_KEY, "false"));
        return result;
    }

    public static int getSimulationPlanNumber(Properties prop) {
        int result = Integer.parseInt(prop.getProperty(SIMULATION_PLAN_NUMBER_KEY, "-1"));
        return result;
    }

    public static int getOptimizationGeneration(Properties prop) {
        int result = Integer.parseInt(prop.getProperty(OPTIMIZATION_GENERATION_KEY, "-1"));
        return result;
    }

     public static int getOptimizationGenerationIndividual(Properties prop) {
        int result = Integer.parseInt(prop.getProperty(OPTIMIZATION_GENERATION_INDIVIDUAL_KEY, "-1"));
        return result;
    }

   public static boolean getUseCache(Properties prop) {
        boolean result;
        if (Versions.smallerThan(getIsisFishVersion(prop), "4.3.0.0")) {
            result = Boolean.parseBoolean(prop.getProperty(USE_OPTIMIZATION_KEY, "true"));
        } else {
            result = Boolean.parseBoolean(prop.getProperty(USE_CACHE_KEY, "true"));
        }
        return result;
    }

    public static int getNumberOfMonths(Properties prop) {
        int result;
        String propValue = prop.getProperty(NUMBER_OF_MONTHS_KEY);
        if (propValue == null) {
            propValue = prop.getProperty(NUMBER_OF_YEAR_KEY, "1");
            result = Integer.parseInt(propValue) * Month.NUMBER_OF_MONTH;

        } else {
            result = Integer.parseInt(propValue);
        }
        return result;
    }

    public static String getRegionName(Properties prop) {
        String result = prop.getProperty(REGION_NAME_KEY, "");
        return result;
    }

    public static String[] getPopulationNames(Properties prop) {
        String[] result = StringUtils.split(prop.getProperty(POPULATIONS_KEY, ""), LIST_SEPARATOR);
        return result;
    }

    /**
     * Retourne les effectifs sous forme d'une liste de liste de double
     * [[2, 3, 4], [5, 6, 7]]
     *
     * @param prop
     * @param popName nom de la population dont on souhaite les effectifs
     * @return
     */
    public static List getPopulationNumbers(Properties prop, String popName) {
        String numberAsString = prop.getProperty(
                POPULATION_KEY + DOT + popName + DOT + NUMBER_KEY);
        List result = MatrixHelper.convertStringToList(numberAsString);
        return result;
    }

   public static String[] getStrategieNames(Properties prop) {
        String[] result = StringUtils.split(prop.getProperty(STRATEGIES_KEY, ""), LIST_SEPARATOR);
        return result;
    }

    public static String[] getRuleNames(Properties prop) {
        String[] result = StringUtils.split(prop.getProperty(RULES_KEY, ""), LIST_SEPARATOR);
        return result;
    }

     public static String[] getResultNames(Properties prop) {
        String[] result = StringUtils.split(prop.getProperty(RESULT_NAMES_KEY, ""), LIST_SEPARATOR);
        return result;
    }

   public static String[] getExportNames(Properties prop) {
        String[] result = StringUtils.split(prop.getProperty(EXPORTS_KEY, ""), LIST_SEPARATOR);
        return result;
    }

    public static String[] getSimulationPlanNames(Properties prop) {
        String[] result = StringUtils.split(prop.getProperty(PLANS_KEY, ""), LIST_SEPARATOR);
        return result;
    }

    public static boolean getUseOptimization(Properties prop) {
        boolean result;
        // en version < 4.3, le parametre était nommé 'useOptimization'
        // qualifiant le cache, il a été renommé ensuite
        if (Versions.smallerThan(getIsisFishVersion(prop), "4.3.0.0")) {
            result = Boolean.FALSE;
        } else {
            result = Boolean.parseBoolean(prop.getProperty(USE_OPTIMIZATION_KEY, "false"));
        }
        return result;
    }

    public static String getOptimizationName(Properties prop) {
        String result = prop.getProperty(OPTIMIZATION_KEY, "");
        return result;
    }

    public static String getObjectiveName(Properties prop) {
        String result = prop.getProperty(OBJECTIVE_KEY, "");
        return result;
   }

    public static String[] getOptimizationExportNames(Properties prop) {
        String[] result = StringUtils.split(prop.getProperty(OPTIMIZATION_EXPORTS_KEY, ""), LIST_SEPARATOR);
        return result;
    }

    public static String getOptimizationObservationName(Properties prop, int index) {
        String result = prop.getProperty(OPTIMIZATION_OBSERVATION_KEY + DOT + index, "");
        if (result.matches("[\\w.]+#[\\d.]+#[\\d.]+:(.+)")) {
            result = result.replaceFirst("[\\w.]+#[\\d.]+#[\\d.]+:(.+)", "$1");
        }

        return result;
    }

    public static String getSensitivityAnalysis(Properties prop) {
        String result = prop.getProperty(SENSITIVITY_ANALYSIS_KEY);
        return result;
    }

    public static String[] getSensitivityExportNames(Properties prop) {
        String[] result = StringUtils.split(prop.getProperty(SENSITIVITY_EXPORTS_KEY, ""), LIST_SEPARATOR);
        return result;
    }

    public static int getNumberOfSensitivitySimulation(Properties prop) {
        int result = Integer.parseInt(prop.getProperty(
                NUMBER_OF_SENSITIVITY_SIMULATION_KEY, "-1"));
        return result;
    }

    public static boolean isSensitivityAnalysisOnlyKeepFirst(Properties prop) {
        boolean result = Boolean.parseBoolean(prop.getProperty(
                SENSITIVITY_ANALYSIS_ONLY_KEEP_FIRST_KEY, "false"));
        return result;
    }
    
    public static Boolean isResultDeleteAfterExport(Properties prop) {
        boolean result = Boolean.parseBoolean(prop.getProperty(RESULT_DELETE_AFTER_EXPORT_KEY, "false"));
        return result;
    }


    public static String getGeneratedPreScript(Properties prop) {
        String result = prop.getProperty(GENERATED_PRE_SCRIPT_KEY, "");
        return result;
    }

    public static boolean getUsePreScript(Properties prop) {
        boolean result = Boolean.parseBoolean(prop.getProperty(USE_PRE_SCRIPT_KEY, "false"));
        return result;
    }

    public static String getPreScript(Properties prop) {
        String result = prop.getProperty(PRE_SCRIPT_KEY, "");
        return result;
    }

    public static boolean getUseStatistic(Properties prop) {
        boolean result = Boolean.parseBoolean(prop.getProperty(USE_STATISTIC_KEY, "false"));
        return result;
    }

    public static boolean getUseComputeResult(Properties prop) {
        boolean result = Boolean.parseBoolean(prop.getProperty(USE_COMPUTE_RESULT_KEY, "false"));
        return result;
    }

    public static String getSimulLogLevel(Properties prop) {
        String result = prop.getProperty(SIMUL_LOG_LEVEL_KEY, "info");
        return result;
    }

    public static String getScriptLogLevel(Properties prop) {
        String result = prop.getProperty(SCRIPT_LOG_LEVEL_KEY, "info");
        return result;
    }

    public static String getLibLogLevel(Properties prop) {
        String result = prop.getProperty(LIB_LOG_LEVEL_KEY, "info");
        return result;
    }

    public static Map<String, String> getTagValue(Properties prop) {
        int tagValueLength = (TAG_VALUE_KEY + DOT).length();
        // preserve order
        Map<String, String> result = new LinkedHashMap<>();
        for (String k : prop.stringPropertyNames()) {
            if (k.startsWith(TAG_VALUE_KEY + DOT)) {
                String key = k.substring(tagValueLength);
                String value = prop.getProperty(k);
                result.put(key, value);
            }
        }
        return result;
    }

    /**
     * Retourne les parametres sous forme de String pour une regles. Les regles
     * sont numeroter dans l'ordre dans lequel on les recupere via #getRuleNames().
     *
     * @param prop
     * @param prefix can be 'rule', or other
     * @param index l'indice de la regle dont on veut les parametre en String
     * @return
     */
    public static Map<String, String> getParamAsString(Properties prop, String prefix, int index) {
        Map<String, String> result = new HashMap<>();
        if (prop != null) {
            String paramTag = prefix + DOT + index + DOT + PARAMETER_KEY + DOT;
            int paramTagLength = paramTag.length();

            for (String key : prop.stringPropertyNames()) {
                if (StringUtils.startsWith(key, paramTag)) {
                    String paramName = key.substring(paramTagLength);
                    String value = prop.getProperty(key);
                    // get only string representation of entities
                    if (value.matches("[\\w.]+#[\\d.]+#[\\d.]+:(.+)")) {
                        value = value.replaceFirst("[\\w.]+#[\\d.]+#[\\d.]+:(.+)", "$1");
                    }
                    result.put(paramName, value);
                }
            }
        }

        return result;
    }

    /**
     * Copy all entry beginning with prefix from source to target
     * 
     * @param source
     * @param target
     * @param prefix
     */
    public static void copy(Properties source, Properties target, String prefix) {
        for (String key : source.stringPropertyNames()) {
            if (StringUtils.startsWith(key, prefix)) {
                target.setProperty(key, source.getProperty(key));
            }
        }
    }

    public static String toString(Properties prop) {
        StringBuilder result = new StringBuilder();
        result.append(
                t("isisfish.params.toString.simulation.done",
                        getIsisFishVersion(prop))).append(" (").append(getSimulatorName(prop)).append(")").append('\n');
        result.append("--------------------\n");
        result.append(getDescription(prop)).append('\n');
        result.append("--------------------\n");

        if (getUseSimulationPlan(prop)) {
            result.append(t("isisfish.params.toString.plan.number",
                    getSimulationPlanNumber(prop)));
            result.append("\n");
        }

        result.append("\n");

        boolean useCache = getUseCache(prop);
        if (useCache) {
            result.append(t("isisfish.params.toString.use.cache",
                    useCache));
            result.append("\n");
        }

        result.append(t("isisfish.params.toString.fishery", getRegionName(prop)));
        result.append("\n\n");
        result.append(t("isisfish.params.toString.number.months",
                        getNumberOfMonths(prop)));
        result.append("\n\n");

        // startegies
        String[] strs = getStrategieNames(prop);
        result.append(t("isisfish.params.toString.strategies"));
        String sep = "";
        for (String str : strs) {
            result.append(sep).append(str);
            sep = LIST_SEPARATOR;
        }
        result.append("\n\n");

        // populations
        result.append(t("isisfish.params.toString.populations"));
        sep = "";
        for (String pop : getPopulationNames(prop)) {
            result.append(sep).append(pop);
            sep = LIST_SEPARATOR;
        }
        result.append("\n\n");

        // rules
        int cpt = 0;
        for (String name : getRuleNames(prop)) {
            result.append(t("isisfish.params.toString.rule", name));
            result.append('\n');
            for (Map.Entry<String, String> e : getParamAsString(prop, RULE_KEY, cpt++).entrySet()) {
                result.append("\t").append(e.getKey()).append(" : ").append(e.getValue()).append("\n");
            }
            result.append("\n");

        }
        result.append("\n\n");

        // simulation plans
        cpt = 0;
        for (String plan : getSimulationPlanNames(prop)) {
            result.append(t("isisfish.params.toString.plan", plan));
            result.append('\n');
            for (Map.Entry<String, String> e : getParamAsString(prop, PLAN_KEY, cpt++).entrySet()) {
                result.append("\t").append(e.getKey()).append(" : ").append(e.getValue()).append("\n");
            }
            result.append('\n');

        }

        boolean useOptimization = getUseOptimization(prop);

        if (useOptimization) {
            result.append(t("isisfish.params.toString.use.optimization", useOptimization));

            result.append(" ").append(t("isisfish.params.toString.optimization.generation", getOptimizationGeneration(prop)));
            result.append(" ").append(t("isisfish.params.toString.optimization.generation.individual",
                    getOptimizationGenerationIndividual(prop)));
            result.append("\n");

            String objective = getObjectiveName(prop);
            if (StringUtils.isNotBlank(objective)) {
                result.append(t("isisfish.params.toString.objective", objective));
                result.append('\n');
                for (Map.Entry<String, String> e : getParamAsString(prop, OBJECTIVE_KEY, 0).entrySet()) {
                    result.append("\t").append(e.getKey()).append(" : ").append(e.getValue()).append("\n");
                }
                result.append('\n');
            }

            String optimization = getOptimizationName(prop);
            if (StringUtils.isNotBlank(optimization)) {
                result.append(t("isisfish.params.toString.optimization", optimization));
                result.append('\n');
                for (Map.Entry<String, String> e : getParamAsString(prop, OPTIMIZATION_KEY, 0).entrySet()) {
                    result.append("\t").append(e.getKey()).append(" : ").append(e.getValue()).append("\n");
                }
                result.append('\n');
            }

            cpt = 0;
            for (String export : getOptimizationExportNames(prop)) {
                String observation = getOptimizationObservationName(prop, cpt++);

                result.append(t("isisfish.params.toString.optimizationExport", export));
                if (StringUtils.isNotBlank(observation)) {
                    result.append("\t->").append(t("isisfish.params.toString.optimizationObservation", observation));
                }
                result.append('\n');
            }
        }

        String genScript = getGeneratedPreScript(prop);
        if (StringUtils.isNotBlank(genScript)) {
            result.append('\n');
            result.append(t("isisfish.params.toString.script.generatedpresimulation")).append(":\n");
            result.append(genScript);
        }
        if (getUsePreScript(prop)) {
            String prescript = getPreScript(prop);
            if (StringUtils.isNotBlank(prescript)) {
                result.append('\n');
                result.append(t("isisfish.params.toString.script.presimulation")).append(":\n");
                result.append(prescript);
            }
        }
        
        result.append('\n');
        result.append(t("isisfish.params.toString.simul.logger.level", getSimulLogLevel(prop)));
        result.append('\n');
        result.append(t("isisfish.params.toString.script.logger.level", getScriptLogLevel(prop)));
        result.append('\n');
        result.append(t("isisfish.params.toString.lib.logger.level", getLibLogLevel(prop)));
        result.append('\n');
        return result.toString();
    }

    public static void removeProperties(Properties prop, String... propNames) {
        Stream.of(propNames).forEach(prop::remove);
    }

    public static void removePropertiesStartingWith(Properties prop, String... propNames) {
        Set<String> strings = prop.stringPropertyNames();
        strings.stream().filter(p -> StringUtils.startsWithAny(p, propNames)).forEach(prop::remove);

    }
}
