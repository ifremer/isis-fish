package fr.ifremer.isisfish.simulator;

/*
 * #%L
 * ISIS-Fish
 * %%
 * Copyright (C) 2014 - 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.isisfish.datastore.SimulationStorage;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public interface SimulationPlanContext {

    /**
     * Get id.
     *
     * @return the id
     */
    String getId();

    /**
     * Get last simulation storage.
     *
     * Used in script.
     *
     * @return last simulation storage
     */
    SimulationStorage getLastSimulation();

    /**
     * Get current simulation plan simulation count.
     *
     * Warning, in after simulation, refer to generated simulation count,
     * not ended simulation number.
     * Depreciated, but no remove this method, it's just to not be used
     * by user script.
     *
     * @return the number
     *
     * @deprecated use {@code nextSimulation.getParameter().getSimulationPlanNumber()}
     */
    int getNumber();

    /**
     * Get simulation parameters.
     *
     * @return simulation parameters
     */
    SimulationParameter getParam();

    /**
     * Get {@link SimulationStorage} for specified simulation plan number.
     *
     * @param number number
     * @return {@link SimulationStorage}
     */
    SimulationStorage getSimulation(int number);

    /**
     * Get plan context value.
     *
     * Used in script.
     *
     * @param key key
     * @return value for key.
     */
    Object getValue(String key);

    /**
     * Set plan context value.
     *
     * Used in script.
     *
     * @param key key
     * @param value value
     */
    void setValue(String key, Object value);

    Historic getHistoric();

}
