/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2011 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator;

import static org.nuiton.i18n.I18n.n;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections4.keyvalue.MultiKey;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixFactory;
import org.nuiton.math.matrix.MatrixND;

import fr.ifremer.isisfish.entities.Metier;
import fr.ifremer.isisfish.entities.Strategy;
import fr.ifremer.isisfish.types.TimeStep;
import fr.ifremer.isisfish.types.Month;

/**
 * ATTENTION: l'implantation de cette classe n'est peut-etre pas correct. Il faut
 * absolument verifier le FIXME, mais il est difficile de le faire car
 * la plupart des methodes de cette objets ne semble plus utilisé dans la
 * version 2.3.x de isis
 * 
 * Created: 21 août 2006 15:43:19
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class MetierMonitor {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    private static Log log = LogFactory.getLog(MetierMonitor.class);

    protected Set<Metier> forbiddenMetier = new HashSet<>();
    protected Set<MultiKey<Object>> forbiddenMetierMonth = new HashSet<>();
    protected Map<TimeStep, MatrixND> noActivity = new HashMap<>();
    
    /**
     * clear all data
     * 
     * can be used at end of each simulation step
     */
    public void clear() {
        forbiddenMetier.clear();
    }
    
    /**
     * Add forbidden metier, this metier is remove when {@link #clear()} is called
     * 
     * @param metier
     */
    public void addforbiddenMetier(Metier metier) {
        forbiddenMetier.add(metier);
    }

    /**
     * Add forbidden metier, this metier is not remove when {@link #clear()} is called
     * 
     * @param metier
     * @param month
     */
    public void addforbiddenMetier(Metier metier, Month month) {
        forbiddenMetierMonth.add(new MultiKey<>(metier, month));
    }
    
    public void removeforbiddenMetier(Metier metier, Month month) {
        forbiddenMetierMonth.remove(new MultiKey<Object>(metier, month));
    }

    public void addforbiddenMetier(Collection<Metier> metiers) {
        forbiddenMetier.addAll(metiers);
    }
        
    /**
     * @return Returns the forbiddenMetier.
     */
    public Set<Metier> getForbiddenMetier() {
        return this.forbiddenMetier;
    }
    
    /**
     * Metier is forbidden is can be found in {@link #forbiddenMetier} or in
     * {@link #forbiddenMetierMonth}.
     * 
     * @param metier
     * @param month
     * @return {@code true} if metier forbidden
     */
    public boolean isForbidden(Metier metier, Month month) {
        boolean result = forbiddenMetier.contains(metier);
        if (!result) {
            result = forbiddenMetierMonth.contains(new MultiKey<Object>(metier, month));
        }
        return result;
    }

    
    /**
     * @return Returns the nonActivity.
     */
    public MatrixND getNoActivity(TimeStep step) {
        return this.noActivity.get(step);
    }
    
    public MatrixND getOrCreateNoActivity(TimeStep step, String name, List<Strategy> strategies, List<Metier> metiers) {
        MatrixND result = getNoActivity(step);
        if (result == null){
            if (strategies.size() == 0) {
                strategies = new ArrayList<>();
                strategies.add(null);
            }
            
            if (metiers.size() == 0) {
                metiers = new ArrayList<>();
                metiers.add(null);
            }
            
            result = MatrixFactory.getInstance().create(
                    name,
                    new List[] {strategies, metiers},
                    new String[] {n("isisfish.metierMonitor.strategies"), n("isisfish.metierMonitor.metiers")});
            setNoActivity(step, result);
        }
        return result;
    }
    
    /**
     * @param nonActivity The nonActivity to set.
     */
    public void setNoActivity(TimeStep step, MatrixND nonActivity) {
        this.noActivity.put(step, nonActivity);
    }
}
