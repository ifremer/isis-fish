/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2007 - 2015 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator;

import static org.nuiton.i18n.I18n.t;

import fr.ifremer.isisfish.IsisFishException;
import fr.ifremer.isisfish.datastore.ExportStorage;
import fr.ifremer.isisfish.entities.EffortDescription;
import fr.ifremer.isisfish.entities.Equation;
import fr.ifremer.isisfish.entities.Gear;
import fr.ifremer.isisfish.entities.Metier;
import fr.ifremer.isisfish.entities.MetierSeasonInfo;
import fr.ifremer.isisfish.entities.Observation;
import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.entities.PopulationSeasonInfo;
import fr.ifremer.isisfish.entities.Selectivity;
import fr.ifremer.isisfish.entities.SetOfVessels;
import fr.ifremer.isisfish.entities.Strategy;
import fr.ifremer.isisfish.entities.TargetSpecies;
import fr.ifremer.isisfish.entities.Variable;
import fr.ifremer.isisfish.export.ExportInfo;
import fr.ifremer.isisfish.export.SensitivityExport;
import fr.ifremer.isisfish.result.ResultInfoHelper;
import fr.ifremer.isisfish.rule.Rule;
import fr.ifremer.isisfish.types.TimeStep;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixND;

/**
 * Used to store and retrieve result during simulation. This class manage
 * result listener
 * 
 * Created: 13 nov. 07 11:54:47
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class ResultManager {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(ResultManager.class);

    protected SimulationContext context = null;
    protected Set<SimulationResultListener> listeners = new LinkedHashSet<>();
    protected SimulationResultGetter getter = null;
    
    /** result enabled */
    transient protected Set<String> enabledResult = null;

    /**
     * 
     */
    public ResultManager(SimulationContext context) {
        this.context = context;
    }

    /**
     * Don't use this method to add listener, use
     * {@link SimulationContext#addSimulationListener(SimulationListener)}
     * @param l
     */
    protected void addSimulationResultListener(SimulationResultListener l) {
        listeners.add(l);
        if (l instanceof SimulationResultGetter) {
            getter = (SimulationResultGetter) l;
        }
    }
    
    /**
     * Don't use this method to remove listener, use
     * {@link SimulationContext#removeSimulationListener(SimulationListener)}
     * @param l
     */
    protected void removeSimulationResultListener(SimulationResultListener l) {
        listeners.remove(l);
        if (l == getter) {
            getter = null;
            // search new getter
            for (SimulationResultListener i : listeners) {
                if (i instanceof SimulationResultGetter) {
                    getter = (SimulationResultGetter) i;
                    break;
                }
            }
        }
    }
    
    /**
     * Permet de savoir si lorsque l'on ajoutera ce resultat, il sera
     * sauvé ou non.
     */
    public boolean isEnabled(String name) {
        name = name.trim();

        if (enabledResult == null) {
            SimulationParameter param = context.getSimulationStorage().getParameter();

            Set<String> requestedResult = ResultInfoHelper.cleanResultNames(param.getResultEnabled());

            List<String> exportNames = param.getExportNames();
            if (exportNames != null) {
                for (String exportName : exportNames) {
                    ExportStorage storage = ExportStorage.getExport(exportName);
                    try {
                        ExportInfo export = storage.getNewInstance();
                        requestedResult.addAll(Arrays.asList(export.getNecessaryResult()));
                    } catch (IsisFishException eee) {
                        if (log.isWarnEnabled()) {
                            log.warn(t("Can't instanciate export %1$s", exportName), eee);
                        }
                    }
                }
            }
            List<SensitivityExport> sensitivityExports = param.getSensitivityExport();
            if (sensitivityExports != null) {
                for (SensitivityExport sensitivityExport : sensitivityExports) {
                    requestedResult.addAll(Arrays.asList(sensitivityExport.getNecessaryResult()));
                }
            }
            List<Rule> rules = param.getRules();
            if (rules != null) {
                for (Rule rule : rules) {
                    requestedResult.addAll(Arrays.asList(rule.getNecessaryResult()));
                }
            }
            List<SimulationPlan> plans = param.getSimulationPlans();
            if (plans != null) {
                for (SimulationPlan plan : plans) {
                    requestedResult.addAll(Arrays.asList(plan.getNecessaryResult()));
                }
            }
    
            // on objective and optimization
            Objective objective = param.getObjective();
            if (objective != null) {
                requestedResult.addAll(Arrays.asList(objective.getNecessaryResult()));
            }
    
            Optimization optimization = param.getOptimization();
            if (optimization != null) {
                requestedResult.addAll(Arrays.asList(optimization.getNecessaryResult()));
            }
    
            // optimization export
            Map<ExportInfo, Observation> optimizationExportsObservations = param.getOptimizationExportsObservations();
            if (optimizationExportsObservations != null) {
                for (ExportInfo export : optimizationExportsObservations.keySet()) {
                    requestedResult.addAll(Arrays.asList(export.getNecessaryResult()));
                }
            }

            // il faut rechercher toutes les equations dans la base les charger
            // en java et ajouter les résultats nécessaires de chaque équation
            // qui compile
            requestedResult.addAll(simulationEquationsResults());

            // recursive result extraction
            enabledResult = ResultInfoHelper.extractAllNecessaryResults(requestedResult);
    
            if (log.isInfoEnabled()) {
                log.info("Enabled result: " + enabledResult);
            }
        }
    
        boolean result = enabledResult.contains(name);
        return result;
    }

    /**
     * Pour eviter de compiler inutilement toutes les equations de la base de données, on inspect seulement ceux
     * lié aux paramatres de la simulations.
     *
     * @return les necessary result des equations
     */
    protected Set<String> simulationEquationsResults() {
        Set<String> equationResults = new HashSet<>();

        SimulationParameter param = context.getSimulationStorage().getParameter();

        // population entry point
        List<Population> populations = param.getPopulations();
        for (Population population : populations) {
            extractEquationResult(equationResults,
                    population.getGrowth(),
                    population.getMaturityOgiveEquation(),
                    population.getMeanWeight(),
                    population.getNaturalDeathRate(),
                    population.getPrice(),
                    population.getReproductionEquation(),
                    population.getFishingMortalityOtherFleets(),
                    population.getGrowthReverse(),
                    population.getReproductionRateEquation(),
                    population.getRecruitmentEquation(),
                    population.getCapturabilityEquation());
            List<PopulationSeasonInfo> populationSeasonInfos = population.getPopulationSeasonInfo();
            for (PopulationSeasonInfo populationSeasonInfo : populationSeasonInfos) {
                extractEquationResult(equationResults,
                        populationSeasonInfo.getEmigrationEquation(),
                        populationSeasonInfo.getImmigrationEquation(),
                        populationSeasonInfo.getMigrationEquation());
            }

        }

        // strategies entry point
        List<Strategy> strategies = param.getStrategies();
        for (Strategy strategy : strategies) {
            extractEquationResult(equationResults, strategy.getInactivityEquation());
            SetOfVessels setOfVessels = strategy.getSetOfVessels();
            if (setOfVessels != null) {
                extractEquationResult(equationResults, setOfVessels.getTechnicalEfficiencyEquation());
                Collection<EffortDescription> possibleMetiers = orEmpty(setOfVessels.getPossibleMetiers());
                for (EffortDescription possibleMetier : possibleMetiers) {
                    Metier metier = possibleMetier.getPossibleMetiers();
                    if (metier != null) {
                        Gear gear = metier.getGear();
                        if (gear != null) {
                            Collection<Selectivity> populationSelectivity = orEmpty(gear.getPopulationSelectivity());
                            for (Selectivity selectivity : populationSelectivity) {
                                extractEquationResult(equationResults, selectivity.getEquation());
                            }
                            List<MetierSeasonInfo> metierSeasonInfos = orEmpty(metier.getMetierSeasonInfo());
                            for (MetierSeasonInfo metierSeasonInfo : metierSeasonInfos) {
                                Collection<TargetSpecies> speciesTargetSpecies = orEmpty(metierSeasonInfo.getSpeciesTargetSpecies());
                                for (TargetSpecies speciesTargetSpecy : speciesTargetSpecies) {
                                    extractEquationResult(equationResults, speciesTargetSpecy.getTargetFactorEquation());
                                }
                            }
                        }
                    }
                }
            }
        }

        // variables entry point
        List<Variable> variables = context.getVariableDAO().findAll();
        for (Variable variable : variables) {
            extractEquationResult(equationResults, variable.getEquationValue());
        }

        return equationResults;
    }

    protected <T> List<T> orEmpty(List<T> coll) {
        return coll == null ? Collections.emptyList() : coll;
    }

    protected <T> Collection<T> orEmpty(Collection<T> coll) {
        return coll == null ? Collections.emptyList() : coll;
    }

    private void extractEquationResult(Set<String> equationResults, Equation... equations) {
        for (Equation equation : equations) {
            if (equation != null) {
                try {
                    String[] results = equation.evaluateNecessaryResult();
                    equationResults.addAll(Arrays.asList(results));
                }  catch (Exception ex) {
                    if (log.isDebugEnabled()) {
                        log.debug("Equation is not compilable", ex);
                    }
                }
            }
        }
    }

    public void addActiveRule(TimeStep step, Rule rule) throws IsisFishException {
        log.debug("addActiveRule not implemented");
        // FIXME: addActiveRule not implemented
    }
    
    public void addResult(TimeStep step, MatrixND mat) throws IsisFishException{
        addResult(false, step, mat.getName(), mat);
    }

    public void addResult(TimeStep step, Population pop, MatrixND mat) throws IsisFishException{
        addResult(false, step, mat.getName(), pop, mat);
    }

    public void addResult(boolean force, TimeStep step, MatrixND mat) throws IsisFishException{
        addResult(force, step, mat.getName(), mat);
    }

    public void addResult(boolean force, TimeStep step, Population pop, MatrixND mat) throws IsisFishException{
        addResult(force, step, mat.getName(), pop, mat);
    }

    public void addResult(TimeStep step, String name, Population pop, MatrixND mat) throws IsisFishException{
        addResult(false, step, name, pop, mat);
    }

    public void addResult(TimeStep step, String name, MatrixND mat) throws IsisFishException{
        addResult(false, step, name, mat);
    }
     
    public void addResult(boolean force, TimeStep step, String name, Population pop, MatrixND mat) throws IsisFishException{
        // don't call generic addResult, because pop name is added
        if (force || isEnabled(name)) {
            for (SimulationResultListener l : listeners) {
                l.addResult(context, step, name + " " + pop, mat);
            }
        }
    }

    public void addResult(boolean force, TimeStep step, String name, MatrixND mat) throws IsisFishException{
        if (force || isEnabled(name)) {
            for (SimulationResultListener l : listeners) {
                l.addResult(context, step, name, mat);
            }
        }
    }

    /**
     * Retourne la matrice stocke pour un pas de temps
     * @param step le pas de temps que l'on souhaite
     * @param pop la population pour lequelle on souhaite le resultat
     * @param name le nom des resultats dont on veut la matrice
     * @return La matrice demandée ou null si aucune matrice ne correspond a
     * la demande.
     */
    public MatrixND getMatrix(TimeStep step, Population pop, String name){
        name += " " + pop;
        return getMatrix(step, name);
    }

    /**
     * Retourne la matrice stocke pour un pas de temps
     * @param step le pas de temps que l'on souhaite
     * @param name le nom des resultats dont on veut la matrice
     * @return La matrice demandée ou null si aucune matrice ne correspond a
     * la demande.
     */
    public MatrixND getMatrix(TimeStep step, String name){
        MatrixND result = null;
        if (getter != null) {
            result = getter.getMatrix(context, step, name);
        }
        return result;
    }

    /**
     * Retourne la matrice stocke pour des pas de temps
     * @param steps les pas de temps que l'on souhaite
     * @param pop la population pour lequelle on souhaite le resultat
     * @param name le nom des resultats dont on veut la matrice
     * @return La matrice demandée ou null si aucune matrice ne correspond a
     * la demande.
     */
    public MatrixND getMatrix(List<TimeStep> steps, Population pop, String name){
        name += " " + pop;
        return getMatrix(steps, name);
    }

    /**
     * Retourne la matrice stocke pour des pas de temps
     * @param steps les pas de temps que l'on souhaite
     * @param name le nom des resultats dont on veut la matrice
     * @return La matrice demandée ou null si aucune matrice ne correspond a
     * la demande.
     */
    public MatrixND getMatrix(List<TimeStep> steps, String name){
        MatrixND result = null;
        if (getter != null) {
            result = getter.getMatrix(context, steps, name);
        }
        return result;
    }

    /**
     * Retourne une matrice contenant tous les pas de temps.
     * @param pop la population pour lequel on souhaite la matrice
     * @param name le nom des resultats dont on veut une matrice globale.
     */
    public MatrixND getMatrix(Population pop, String name){
        name += " " + pop;
        return getMatrix(name);
    }

    /**
     * Retourne une matrice contenant tous les pas de temps.
     * @param name le nom des resultats dont on veut une matrice globale.
     */
    public MatrixND getMatrix(String name){
        MatrixND result = null;
        if (getter != null) {
            result = getter.getMatrix(context, name);
        }
        return result;
    }
}
