/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2010 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Objects;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.types.TimeStep;

/**
 * Permit to communicate with user interface from simulation thread.
 * 
 * Created: Fri Oct  6 2000
 *
 * @author POUSSIN Benjamin &lt;poussin@codelutin.com&gt;
 * @version $Revision$
 *
 * Mise a jour: $Date$
 * par : $Author$
 */
public class SimulationControl implements PropertyChangeListener { // SimulationControl

    /** to use log facility, just put in your code: log.info(\"...\"); */
    private static Log log = LogFactory.getLog(SimulationControl.class);

    protected PropertyChangeSupport listeners = new PropertyChangeSupport(this);
    protected boolean inUpdateFromProperties = false;
    protected transient SimulationStorage simulation = null;

    protected String id = null;
    protected boolean started = false;
    protected boolean running = true;
    protected boolean stop = false;
    protected TimeStep step;
    protected long progressMax = 0;
    protected long progress = 0;
    protected String text = "";
    // temps moyen d'un pas de temps vaut 0 tant que pas mis a jour
    protected double timeStepMeanTime;

    /** Control save mecanism each time a property is changed (default to false). */
    protected transient boolean autoSaveState = false;

    /**
     * Init simulation control with simulation id.
     * 
     * @param id simulation id
     */
    public SimulationControl(String id) {
        this.id = id;
        addPropertyChangeListener(this);
    }

    /**
     * Change auto save state on property change.
     * 
     * Usefull to disable autosave in simulation monitoring.
     * 
     * @param autoSaveState new state
     */
    public void setAutoSaveState(boolean autoSaveState) {
        this.autoSaveState = autoSaveState;
    }

    /**
     * Get current auto save state.
     * 
     * @return auto save state
     */
    public boolean isAutoSaveState() {
        return autoSaveState;
    }

    /**
     * Reset control values for job restart.
     */
    public void reset() {
        started = false;
        running = true;
        stop = false;
        step = null;
        progressMax = 0;
        progress = 0;
    }

    /**
     * Retourne la simulation associe a ce control
     * @return retourne null si la simulation n'existe pas encore
     */
    public SimulationStorage getSimulation() {
        if (simulation == null) {
            simulation = SimulationStorage.getSimulation(id);
        }
        return simulation;
    }

    public void addPropertyChangeListener(String propName,
            PropertyChangeListener l) {
        listeners.addPropertyChangeListener(propName, l);
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        listeners.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(String propName,
            PropertyChangeListener l) {
        listeners.removePropertyChangeListener(propName, l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        listeners.removePropertyChangeListener(l);
    }

    public boolean isStarted() {
        return started;
    }

    /**
     * Appelee juste avant de reellement demarrer le thread de simulation
     * indique que cette simulation est en cours et qu'il ne faut plus la lancer
     * 
     * @param started The running to set.
     */
    public void setStarted(boolean started) {
        boolean oldValue = this.started;
        this.started = started;
        listeners.firePropertyChange("started", oldValue, this.started);
    }

    /**
     * Method running.
     *
     * @return faux une fois que stopSimulation a ete appele.
     */
    public boolean isRunning() {
        return running;
    }

    /**
     * @param running The running to set.
     */
    private void setRunning(boolean running) {
        boolean oldValue = this.running;
        this.running = running;
        listeners.firePropertyChange("running", oldValue, this.running);
    }

    /**
     * Appele lors du debut de la simulation.
     */
    public void startSimulation() {
        setRunning(true);
    }

    /**
     * Appele lors de la fin reelle de la simulation.
     */
    public void stopSimulation() {
        setRunning(false);
    }

    /**
     * @return Returns the text.
     */
    public String getText() {
        return this.text;
    }

    /**
     * @param text The text to set.
     */
    public void setText(String text) {
        // pevent multiples logging of same string during remote simulations
        if (!Objects.equals(this.text, text) && log.isInfoEnabled()) {
            log.info(text);
        }
        String oldValue = this.text;
        this.text = text;
        listeners.firePropertyChange("text", oldValue, this.text);
    }

    /**
     * @return Returns the progressMax.
     */
    public long getProgressMax() {
        return this.progressMax;
    }

    /**
     * Set max progress.
     * 
     * @param progressMax progressMax to set.
     */
    public void setProgressMax(long progressMax) {
        long oldValue = this.progressMax;
        this.progressMax = progressMax;
        listeners.firePropertyChange("progressMax", oldValue, this.progressMax);
    }

    /**
     * Get current progress.
     * 
     * @return the progress.
     */
    public long getProgress() {
        return this.progress;
    }

    /**
     * Set current progress.
     * 
     * @param progress progress to set.
     */
    public void setProgress(long progress) {
        long oldValue = this.progress;
        this.progress = progress;
        if (log.isTraceEnabled()) {
            log.trace("control fire event 'progress' (" + oldValue + "/"
                    + this.progress);
        }
        listeners.firePropertyChange("progress", oldValue, this.progress);
    }

    /**
     * Demande l'arret de la simulation.
     * 
     * @param val {@code true} to stop
     */
    public void setStopSimulationRequest(boolean val) {
        boolean oldValue = this.stop;
        this.stop = val;
        listeners.firePropertyChange("stop", oldValue, this.stop);
    }

    /**
     * Is stop request been asked.
     * 
     * @return retourne vrai si l'arret de la simulation a ete demandee
     */
    public boolean isStopSimulationRequest() {
        return stop;
    }

    /**
     * Method getId donne id de la simulation.
     *
     * @return l'id de la simulation
     */
    public String getId() {
        return id;
    }

    /**
     * Method getDate donne la date utiliser par la simulation.
     *
     * @return donne la date utiliser par la simulation
     */
    public TimeStep getStep() {
        return step;
    }

    /**
     * Modifie la date de la simulation.
     * 
     * @param t la nouvelle etape
     */
    public void setStep(TimeStep t) {
        int oldValue = 0;
        if (this.step != null) {
            oldValue = this.step.getStep();
        }
        this.step = t;
        listeners.firePropertyChange("step", oldValue, this.step.getStep());
    }

    /**
     * Set mean time for one step in second
     * @param timeStepMeanTime 
     */
    public void setTimeStepMeanTime(double timeStepMeanTime) {
        double oldValue = this.timeStepMeanTime;
        this.timeStepMeanTime = timeStepMeanTime;
        listeners.firePropertyChange("timeStepMeanTime", oldValue, this.timeStepMeanTime);
    }

    /**
     * Get mean time for one step in second
     * @return
     */
    public double getTimeStepMeanTime() {
        return timeStepMeanTime;
    }


    /**
     * Met dans un Properties tous les champs
     * @return un Properties avec tous les champs
     */
    public Properties getProperties() {
        Properties result = new Properties();
        result.setProperty("id", id);
        result.setProperty("started", String.valueOf(started));
        result.setProperty("running", String.valueOf(running));
        result.setProperty("stop", String.valueOf(stop));
        if (step != null) {
            result.setProperty("step", String.valueOf(step.getStep()));
        }
        result.setProperty("progressMax", String.valueOf(progressMax));
        result.setProperty("progress", String.valueOf(progress));
        result.setProperty("text", text);
        result.setProperty("timeStepMeanTime", String.valueOf(timeStepMeanTime));

        return result;
    }

    /**
     * update current object from Properties representation
     * 
     * @param props new properties to update from
     */
    public void updateFromProperties(Properties props) {
        inUpdateFromProperties = true;

        if (log.isTraceEnabled()) {
            log.trace("updateFromProperties properties = " + props.toString());
        }

        // warning : props.contains("started")
        // seems to not works on properties :(
        // use containsKey()
        try {
            if (props.containsKey("started")) {
                boolean started = "true".equalsIgnoreCase(props
                        .getProperty("started"));
                setStarted(started);
            }
            if (props.containsKey("running")) {
                boolean running = "true".equalsIgnoreCase(props
                        .getProperty("running"));
                setRunning(running);
            }
            if (props.containsKey("stop")) {
                boolean stop = "true".equalsIgnoreCase(props
                        .getProperty("stop"));
                setStopSimulationRequest(stop);
            }
            if (props.containsKey("step")) {
                TimeStep step = new TimeStep(Integer.parseInt(props
                        .getProperty("step")));
                setStep(step);
            }
            if (props.containsKey("progressMax")) {
                long progressMax = Long.parseLong(props
                        .getProperty("progressMax"));
                setProgressMax(progressMax);
            }
            if (props.containsKey("progress")) {
                long progress = Long.parseLong(props.getProperty("progress"));
                setProgress(progress);
            }
            if (props.containsKey("text")) {
                String text = props.getProperty("text");
                setText(text);
            }
            if (props.containsKey("timeStepMeanTime")) {
                double timeStepMeanTime = Double.parseDouble(props.getProperty("timeStepMeanTime"));
                setTimeStepMeanTime(timeStepMeanTime);
            }
        } finally {
            inUpdateFromProperties = false;
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent e) {
        if (!inUpdateFromProperties) {
            if (isAutoSaveState() && getSimulation() != null) {
                getSimulation().saveControl(this);
            }
        }
    }

} // SimulationControl
