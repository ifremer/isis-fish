/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.simulator;

import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.simulator.launcher.SimulationJob;
import fr.ifremer.isisfish.simulator.sensitivity.Factor;
import fr.ifremer.isisfish.simulator.sensitivity.FactorHelper;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.apache.commons.collections4.CollectionUtils;

/**
 * Context utilisé pour gérer les différentes simulation lancées par un script d'optimisation.
 *
 * @since 4.3.0.0
 */
public class OptimizationContextInternal extends SimulationPlanContextInternal implements OptimizationContext {

    protected SimulationJob job;

    protected int generationNumber = 0;
    protected List<List<SimulationStorage>> generations = new ArrayList<>();

    /** Simulation to do during next generation simulation */
    protected List<SimulationStorage> nextSimulations;

    public OptimizationContextInternal(String id, SimulationParameter param, SimulationJob job) {
        super(id, param);
        this.job = job;
    }

    @Override
    public List<SimulationStorage> getNextSimulations() {
        if (nextSimulations == null) {
            nextSimulations = new ArrayList<>();
        }
        return nextSimulations;
    }

    /**
     * Get simulation to do, put it generations and increment currentGeneration
     * This method must be call be Simulator when it want to get next simulations
     * to do.
     * @return null or empty collection if no more simulation to do
     */
    public List<SimulationStorage> clearNextSimulation() {
        // /!\ la collection renvoyee est modifiee par l'appelant (OptimizationPrepareJob)
        // les simulations ancienne et temporaire on ete supprimer
        // et remplacer par les simulations qui ont ete faites
        List<SimulationStorage> result = nextSimulations;

        if (CollectionUtils.isNotEmpty(result)) {
            generations.add(result);
            nextSimulations = null;
        }

        return result;
    }

    @Override
    public List<SimulationStorage> getLastSimulations() {
        List<SimulationStorage> result = getGeneration(generations.size() - 1);
        return result;
    }

    /**
     * Return new simulation. This new simulation is automaticaly added to
     * next simulation.
     * 
     * @return new Simulation
     */
    @Override
    public SimulationStorage newSimulation() {
        try {
            String simId = TEMP_PREFIX + id + "_" + number;
            SimulationParameter childParam = param.copy();
            childParam.setSimulationPlanNumber(number);
            childParam.setOptimizationGeneration(generations.size());
            childParam.setOptimizationGenerationIndividual(getNextSimulations().size());
            SimulationStorage result = SimulationStorage.importAndRenameZip(job.getItem().getSimulationZip(), simId);
            result.setParameter(childParam);
            
            incNumber();
            addSimulation(result);

            return result;
        } catch (Exception eee) {
            throw new IsisFishRuntimeException("can't create new simulation", eee);
        }
    }

    protected void addSimulation(SimulationStorage s) {
        getNextSimulations().add(s);
    }

    /**
     * Create simulation. This new simulation is automatically added to
     * next simulations. Database will be modified with factors in parameters
     *
     * @param factors factors used to modify simulation
     * @return simulation modified with factors
     */
    @Override
    public SimulationStorage newSimulation(Factor ... factors) {
        SimulationStorage s = newSimulation();

        Collection<Factor> colFactors = new ArrayList<>();
        Collections.addAll(colFactors, factors);
        String script = FactorHelper.generatePreScript(colFactors);

        SimulationParameter childParam = s.getParameter();
        childParam.setGeneratedPreScript(script);

        return s;
    }

    /**
     * Return the current generation.
     * 0 for no generation
     * 1 for one generation simulation
     * ...
     * @return
     */
    @Override
    public int getCurrentGeneration() {
        return generationNumber;
    }

    public void incGenerationNumber() {
        generationNumber++;
    }

    public int getSimulationNumber() {
        return number;
    }
    
    @Override
    public List<SimulationStorage> getGeneration(int n) {
        List<SimulationStorage> result = null;
        if (generations.size() == n) {
            result = getNextSimulations();
        } else if (n >= 0 && CollectionUtils.isNotEmpty(generations)) {
            result = generations.get(n);
        }

        return result;
    }

}
