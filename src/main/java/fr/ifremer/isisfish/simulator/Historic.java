package fr.ifremer.isisfish.simulator;

/*
 * #%L
 * ISIS-Fish
 * %%
 * Copyright (C) 2014 - 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.ifremer.isisfish.datastore.SimulationStorage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixFactory;
import org.nuiton.math.matrix.MatrixIterator;
import org.nuiton.math.matrix.MatrixND;

/**
 * Useful class to stock some double value for simulation, and export all
 * value in CSV format
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class Historic {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(Historic.class);

    protected String id;
    protected Map<Integer, Experience> experiences = new LinkedHashMap<>();

    public Historic(String id) {
        this.id = id;
    }

    public Experience get(SimulationStorage sim) {
        int simNumber = sim.getParameter().getSimulationPlanNumber();
        Experience result = experiences.get(simNumber);
        if (result == null) {
            result = new Experience();
            experiences.put(simNumber, result);
            result.set("SimulationNumber", simNumber);
            result.set("Generation", sim.getParameter().getOptimizationGeneration());
            result.set("Individual", sim.getParameter().getOptimizationGenerationIndividual());
        }
        return result;
    }

    public MatrixND toMatrix(String ... fieldNames) {
        List semVar = new ArrayList();
        semVar.add("Generation");
        semVar.add("Individual");
        semVar.addAll(Arrays.asList(fieldNames));
        
        List semExp = new ArrayList();
        for (Experience e : experiences.values()) {
            semExp.add(e.get("SimulationNumber"));
        }

        List[] sems = new List[2];
        sems[0] = semExp;
        sems[1] = semVar;

        String[] dimNames = new String[]{"Experience", "Variables"};

        MatrixND result = MatrixFactory.getInstance().create(id, sems, dimNames);

        for (MatrixIterator i = result.iterator(); i.hasNext();) {
            i.next();
            Object[] s = i.getSemanticsCoordinates();
            Object exp = s[0];
            String var = (String)s[1];

            Double v = experiences.get(exp).get(var);
            if (v != null) {
                i.setValue(v);
            }
        }

        return result;
    }

    public String toCSV(String ... fieldNames) {
        StringBuilder result = new StringBuilder();
        String sep = ";";

        result.append("SimulationNumber");
        result.append(sep);
        result.append("Generation");
        result.append(sep);
        result.append("Individual");
        for (String field: fieldNames) {
            result.append(sep).append(field);
        }

        for (Map.Entry<Integer, Experience> e: experiences.entrySet()) {
            Experience exp = e.getValue();
            result.append(e.getKey());
            result.append(sep);
            result.append(exp.get("Generation"));
            result.append(sep);
            result.append(exp.get("Individual"));

            for (String field : fieldNames) {
                result.append(sep);
                Double value = exp.get(field);
                if (value != null) {
                    result.append(value);
                } else {
                    result.append("NA");
                }
            }
        }

        return result.toString();
    }

    public static class Experience {

        Map<String, Double> values = new HashMap<>();

        public Experience() {
        }

        public Double get(String fieldName) {
            Double result = values.get(fieldName);
            return result;
        }

        public void set(String fieldName, double value) {
            values.put(fieldName, value);
        }
    }
}
