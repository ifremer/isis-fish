/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2012 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator;

import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.IsisFishDAOHelper;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import fr.ifremer.isisfish.aspect.ComputeResultTrace;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.persistence.TopiaEntity;

import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.entities.*;
import fr.ifremer.isisfish.simulator.sensitivity.SensitivityUtils;
import fr.ifremer.isisfish.types.TimeStep;
import fr.ifremer.isisfish.util.cache.IsisCache;

import java.beans.PropertyChangeListener;

import org.nuiton.config.ApplicationConfig;
import org.nuiton.config.OverwriteApplicationConfig;
import org.nuiton.math.matrix.MatrixFactory;
import org.nuiton.profiling.Statistic;
import org.nuiton.profiling.Trace;
import org.nuiton.profiling.Unit;

/**
 * Keep all information on one simulation.
 * 
 * <ul>
 * <li> Launch parameter</li>
 * <li> Database (TopiaContext)</li>
 * <li> SimulationControl</li>
 * <li> Effective by pop (N)</li>
 * <li> Result</li>
 * </ul>
 * 
 * Created: 3 juil. 2006 17:05:27
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class SimulationContext {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    private static Log log = LogFactory.getLog(SimulationContext.class);

    /** configuration for current simulation (some value of config is overwrited for simulation) */
    protected ApplicationConfig config;
    protected Map<String, Object> values = new HashMap<>();
    protected SimulationStorage simulation = null;
    protected SimulationControl simulationControl = null;
    protected PopulationMonitor populationMonitor = null;
    protected MetierMonitor metierMonitor = null;
    protected RuleMonitor ruleMonitor = null;
    protected ResultManager resultManager = null;
    protected Set<SimulationListener> simulationListeners = new LinkedHashSet<>();
    protected PropertyChangeListener stepListener = evt -> {
        Integer i = (Integer)evt.getNewValue();
        SimulationContext.this.fireStepChange(new TimeStep(i));

        // met a jour les stats sur le temps des pas de temps
        Statistic stat = SimulationContext.this.timeStepStat;
        stat.add(i);
        SimulationContext.this.getSimulationControl().setTimeStepMeanTime(
                Unit.Time.nano.convertTo(Unit.Time.s, stat.getMean()));
    };
    
    protected Statistic timeStepStat = new Statistic("Step time"){
        protected long lastTime = 0;
        // add delta not value
        @Override
        public void add(long value) {
            if (lastTime == 0) {
                lastTime = System.nanoTime();
            } else {
                long newTime = System.nanoTime();
                long delta = newTime - lastTime;
                lastTime = newTime;
                super.add(delta);
            }

        }

        @Override
        public String formatValue(long value) {
            String result = String.valueOf(Unit.Time.nano.convertTo(Unit.Time.s, value));
            return result;
        }
    };

    protected ClassLoader classLoader = null;
    protected Map<String, ClassLoader> equationClassLoaders = new HashMap<>();

    protected File scriptDirectory;
    /** l'objet trace qui conserve les donnees statistiques des appels de methodes */
    protected Trace trace;
    /** l'objet trace qui conserve les donnees statistiques des appels de methodes */
    protected ComputeResultTrace computeResultTrace;
    /** cache used by aspect for this simulation */
    protected IsisCache cache;

    /** TopiaContext must be used by rule action to modify data */
    protected TopiaContext db = null;

    /** TopiaContext must be used to save result */
    protected TopiaContext dbResult = null;

    /** CacheAspect des variables d'entités. Topia id &gt; map of attributes. */
    protected Map<TopiaEntity, SimulationVariable> variablesCache = new HashMap<>();

    /** Context value used in equation. */
    protected Map<String, Double> contextEquationValue = new HashMap<>();

    private static ThreadLocal<SimulationContext> simulationContext = new ThreadLocal<SimulationContext>() {
        protected synchronized SimulationContext initialValue() {
            return new SimulationContext();
        }
    };

    protected SimulationContext() {
    }

    /**
     * init SimulationContext for simulation.
     *
     */
    public void initForSimulation() {
        // must be done after setSimulationStorage because config looking for
        // value in simulation parameter via SimulationContext

        // force matrix backend for this simulationcontext
        MatrixFactory.initMatrixFactoryThreadLocal(
                IsisFish.config.getSimulationMatrixVectorClass(),
                IsisFish.config.getSimulationMatrixVectorSparseClass(),
                IsisFish.config.getSimulationMatrixThresholdUseSparse(),
                IsisFish.config.getSimulationMatrixdUseLazyVector());
    }

    /**
     * Return SimulationContext if not created, create new context without
     * initialisation needed for simulation
     * @return simulation context for the current simulation (current thread)
     */
    public static SimulationContext get() {
        return simulationContext.get();
    }

    /**
     * remove simulation associate with current thread
     */
    public static void remove() {
        SimulationContext current = get();
        current.getCache().clear();
        current.cache = null;
        current.getTrace().clearStatistics();
        current.trace = null;
        current.computeResultTrace = null;
        current.values.clear();
        current.classLoader = null;
        current.equationClassLoaders = null;
        simulationContext.remove();
    }

    /**
     * Get specifique ApplicationConfig for current thread.simulation.
     * Needed for simulation some configurations values are overwrited by
     * simulation parameter tag/value
     *
     * @return
     */
    public ApplicationConfig getConfig() {
        if (config == null) {
            if (getSimulationStorage()!= null) {
                // in simulation, tag/value must be in config for simulation option
                Map<String, String> tv = getSimulationStorage().getParameter().getTagValue();
                config = new OverwriteApplicationConfig(IsisFish.config, tv);
            } else {
                config = IsisFish.config;
            }
        }
        return config;
    }


    /**
     * Add simulation listener, if listener is {@link SimulationResultListener}, it's 
     * automatically added as listener on {@link ResultManager}
     * @param l
     */
    public void addSimulationListener(SimulationListener l) {
        simulationListeners.add(l);
        if (l instanceof SimulationResultListener) {
            getResultManager().addSimulationResultListener(
                    (SimulationResultListener) l);
        }
    }

    public void closeDB() {
        if (db != null) {
            try {
                db.closeContext();
            } catch (TopiaException eee) {
                if (log.isDebugEnabled()) {
                    log.debug("Can't close simulation topia context", eee);
                }
            }
        }
    }

    /**
     * @deprecated a supprimer lorsqu'on aura supprimer ResultDatabaseStorage
     */
    @Deprecated
    public void closeDBResult() {
        if (dbResult != null) {
            try {
                dbResult.commitTransaction();
                dbResult.closeContext();
            } catch (TopiaException eee) {
                if (log.isDebugEnabled()) {
                    log.debug("Can't close simulation result topia context",
                            eee);
                }
            }
        }
    }

    /**
     * Remove simulation listener, if listener is {@link SimulationResultListener}, it's 
     * Automatically removed as listener on {@link ResultManager}
     * @param l
     */
    public void removeSimulationListener(SimulationListener l) {
        simulationListeners.remove(l);
        if (l instanceof SimulationResultListener) {
            getResultManager().addSimulationResultListener(
                    (SimulationResultListener) l);
        }
    }

    public void fireBeforeSimulation() {
        for (SimulationListener l : simulationListeners) {
            l.beforeSimulation(this);
        }
    }

    public void fireStepChange(TimeStep step) {
        for (SimulationListener l : simulationListeners) {
            l.stepChange(this, step);
        }
    }

    public void fireAfterSimulation() {
        for (SimulationListener l : simulationListeners) {
            l.afterSimulation(this);
        }
    }

    /**
     * @return Returns the classLoader.
     */
    public ClassLoader getClassLoader() {
        return this.classLoader;
    }

    public Map<String, ClassLoader> getEquationClassLoaders() {
        return equationClassLoaders;
    }

    /**
     * @param classLoader The classLoader to set.
     */
    public void setClassLoader(ClassLoader classLoader) {
        this.classLoader = classLoader;
    }

    public Trace getTrace() {
        if (trace == null) {
            trace = new Trace(false, false);
        }
        return trace;
    }

    public ComputeResultTrace getComputeResultTrace() {
        if (computeResultTrace == null) {
            computeResultTrace = new ComputeResultTrace(false, false);
        }
        return computeResultTrace;
    }

    public IsisCache getCache() {
        if (cache == null) {
            cache = new IsisCache();
        }
        return cache;
    }

    /**
     * Permet de recuperer une valeur prealablement stocker avec un setValue
     * @param name le nom de la valeur souhaitée
     * @return la valeur ou null si aucune valeur ne porte se nom
     */
    public Object getValue(String name) {
        Object result = values.get(name);
        return result;
    }

    /**
     * Permet de stocker une valeur en fonction d'une cle, cela peut-etre util
     * pour partager des informations entre Rule ou d'autre objet.
     * @param name le nom de la valeur
     * @param value la valeur
     */
    public void setValue(String name, Object value) {
        values.put(name, value);
    }

    /**
     * @return Returns the topiaContext.
     */
    public SimulationStorage getSimulationStorage() {
        return this.simulation;
    }

    /**
     * @param simulation The simulation storage to set.
     */
    public void setSimulationStorage(SimulationStorage simulation) {
        this.simulation = simulation;
    }

    public Statistic getTimeStepStat() {
        return timeStepStat;
    }

    /**
     * @return Returns the simulationControl.
     */
    public SimulationControl getSimulationControl() {
        return this.simulationControl;
    }

    /**
     * @param simulationControl The simulationControl to set.
     */
    public void setSimulationControl(SimulationControl simulationControl) {
        if (this.simulationControl != simulationControl) {
            if (this.simulationControl != null) {
                this.simulationControl.removePropertyChangeListener("step", stepListener);
            }
            this.simulationControl = simulationControl;
            if (simulationControl != null) {
                simulationControl.addPropertyChangeListener("step", stepListener);
            }
        }
    }

    /**
     * @return Returns the populationMonitor.
     */
    public PopulationMonitor getPopulationMonitor() {
        if (this.populationMonitor == null) {
            this.populationMonitor = new PopulationMonitor();
        }
        return this.populationMonitor;
    }

    /**
     * @return Returns the metierMonitor.
     */
    public MetierMonitor getMetierMonitor() {
        if (this.metierMonitor == null) {
            this.metierMonitor = new MetierMonitor();
        }
        return this.metierMonitor;
    }

    /**
     * @return Returns the ruleMonitor.
     */
    public RuleMonitor getRuleMonitor() {
        if (this.ruleMonitor == null) {
            this.ruleMonitor = new RuleMonitor();
        }
        return this.ruleMonitor;
    }

    /**
     * @return Returns the resultManager.
     */
    public ResultManager getResultManager() {
        if (this.resultManager == null) {
            this.resultManager = new ResultManager(this);
        }
        return this.resultManager;
    }

    /**
     * This method return TopiaContext that Rule action must used to modify
     * data. This TopiaContext is rollbacked after each step.
     * @return TopiaContext that Rule action must used
     * @throws TopiaException 
     */
    public TopiaContext getDB() throws TopiaException {
        if (db == null && getSimulationStorage() != null) {
            db = getSimulationStorage().getMemStorage().beginTransaction();
        }
        return db;
    }

    /**
     * this method return TopiaContext that must be used to save result
     * @return Returns the dbResult.
     * @throws TopiaException
     *
     * @deprecated ne sert plus a rien lorsque ResultDatabaseStorage sera supprime
     */
    @Deprecated
    public TopiaContext getDbResult() throws TopiaException {
        if (dbResult == null && getSimulationStorage() != null) {
            dbResult = getSimulationStorage().getStorage().beginTransaction();
        }
        return this.dbResult;
    }

    /**
     * Clear isis fish cache for specified time step.
     * 
     * @param step time step to remove data
     * @since 4.1.1.0
     * @throws TopiaException
     * @deprecated ne sert plus a rien, le cache est directement listener de la simulation
     */
    @Deprecated
    public void clearCache(TimeStep step) throws TopiaException {
        getCache().clear(step);
    }

    /**
     * @return Returns the scriptDirectory.
     */
    public File getScriptDirectory() {
        return this.scriptDirectory;
    }

    /**
     * Script directory to use.
     * 
     * Used to change isis-database-3 directory in running simulation context.
     * 
     * @param scriptDirectory
     */
    public void setScriptDirectory(File scriptDirectory) {
        this.scriptDirectory = scriptDirectory;
    }

    /**
     * @param message message
     */
    public void message(String message) {
        log.info(message);
        if (getSimulationControl() != null) {
            getSimulationControl().setText(message);
        }
    }

    /**
     * Add value/key pair into context (computed value).
     * 
     * @param key key
     * @param value value
     */
    public void setComputeValue(String key, Double value) {

        // on echappe (encore) le nom du facteur, car pour une raison ou pour une autre,
        // l'echapement de la méthode fr.ifremer.isisfish.simulator.launcher.SimulationService.fillDesignPlanNominalValue(TopiaContext, DesignPlan)
        // n'a pas l'air de suffir pour les equations :(

        String localKey = SensitivityUtils.espaceFactorName(key);
        contextEquationValue.put(localKey, value);
    }

    /**
     * Return value from context.
     * 
     * @param key key
     * @param defaultValue default value or value to compute
     * @return computed value or {@code defaultValue} if not found
     */
    public double getValueAndCompute(String key, double defaultValue) {

        double result;

        // for equation, name is set not R escaped in database by swing ui
        // but is set escaped in prescripts because of r escape
        // need to be escaped here
        String localKey = SensitivityUtils.espaceFactorName(key);

        if (contextEquationValue.containsKey(localKey)) {

            Double value = contextEquationValue.get(localKey);

            if (log.isTraceEnabled()) {
                log.trace("Found key '" + localKey + "' current value = " + value);
            }

            // since 4.0.0.0, operator is always *
            result = value;
        }
        else {
            result = defaultValue;
            if (log.isTraceEnabled()) {
                log.trace("No key defined for key '" + localKey + "' in context");
            }
        }

        return result;
    }

    /**
     * Get object containing variable for given entity.
     * 
     * @param entity entity
     * @return map object for this class
     * @since 4.1.0.0
     */
    public SimulationVariable get(TopiaEntity entity) {
        SimulationVariable v = variablesCache.getOrDefault(entity, new SimulationVariable(this, entity));
        return v;
    }

    /**
     * Rollback region database transaction.
     * 
     * @throws TopiaException
     * @since 4.3.1.0
     */
    public void rollbackRuleChanges() throws TopiaException {
        if (db != null) {
            db.rollbackTransaction();
        }
    }

    /**
     * Valide (commit) database modification
     */
    public void validateDBChanges() throws TopiaException {
        if (db != null) {
            db.commitTransaction();
        }
    }

    /**
     * Commit result storage transaction.
     * 
     * @throws TopiaException
     * @since 4.3.1.0
     *
     * @deprecated ne sert plus a rien lorsque ResultDatabaseStorage sera supprime
     */
    @Deprecated
    public void commitResults() throws TopiaException {
        TopiaContext tx = getDbResult();
        tx.commitTransaction();
    }

    /**
     * Get {@link ActiveRuleDAO} on region database.
     * 
     * @return ActiveRuleDAO
     * @throws TopiaException
     * @since 4.3.1.0
     */
    public ActiveRuleDAO getActiveRuleDAO() throws TopiaException {
        return IsisFishDAOHelper.getActiveRuleDAO(getDB());
    }

    /**
     * Get {@link CellDAO} on region database.
     * 
     * @return CellDAO
     * @throws TopiaException
     * @since 4.3.1.0
     */
    public CellDAO getCellDAO() throws TopiaException {
        return IsisFishDAOHelper.getCellDAO(getDB());
    }

    /**
     * Get {@link EffortDescriptionDAO} on region database.
     * 
     * @return EffortDescriptionDAO
     * @throws TopiaException
     * @since 4.3.1.0
     */
    public EffortDescriptionDAO getEffortDescriptionDAO() throws TopiaException {
        return IsisFishDAOHelper.getEffortDescriptionDAO(getDB());
    }

    /**
     * Get {@link EquationDAO} on region database.
     * 
     * @return EquationDAO
     * @throws TopiaException
     * @since 4.3.1.0
     */
    public EquationDAO getEquationDAO() throws TopiaException {
        return IsisFishDAOHelper.getEquationDAO(getDB());
    }

    /**
     * Get {@link FisheryRegionDAO} on region database.
     * 
     * @return FisheryRegionDAO
     * @throws TopiaException
     * @since 4.3.1.0
     */
    public FisheryRegionDAO getFisheryRegionDAO() throws TopiaException {
        return IsisFishDAOHelper.getFisheryRegionDAO(getDB());
    }

    /**
     * Get {@link GearDAO} on region database.
     * 
     * @return GearDAO
     * @throws TopiaException
     * @since 4.3.1.0
     */
    public GearDAO getGearDAO() throws TopiaException {
        return IsisFishDAOHelper.getGearDAO(getDB());
    }

    /**
     * Get {@link MetierDAO} on region database.
     * 
     * @return MetierDAO
     * @throws TopiaException
     * @since 4.3.1.0
     */
    public MetierDAO getMetierDAO() throws TopiaException {
        return IsisFishDAOHelper.getMetierDAO(getDB());
    }

    /**
     * Get {@link MetierSeasonInfoDAO} on region database.
     * 
     * @return MetierSeasonInfoDAO
     * @throws TopiaException
     * @since 4.3.1.0
     */
    public MetierSeasonInfoDAO getMetierSeasonInfoDAO() throws TopiaException {
        return IsisFishDAOHelper.getMetierSeasonInfoDAO(getDB());
    }

    /**
     * Get {@link ObservationDAO} on region database.
     * 
     * @return ObservationDAO
     * @throws TopiaException
     * @since 4.3.1.0
     */
    public ObservationDAO getObservationDAO() throws TopiaException {
        return IsisFishDAOHelper.getObservationDAO(getDB());
    }

    /**
     * Get {@link PopulationDAO} on region database.
     * 
     * @return PopulationDAO
     * @throws TopiaException
     * @since 4.3.1.0
     */
    public PopulationDAO getPopulationDAO() throws TopiaException {
        return IsisFishDAOHelper.getPopulationDAO(getDB());
    }

    /**
     * Get {@link PopulationGroupDAO} on region database.
     * 
     * @return PopulationGroupDAO
     * @throws TopiaException
     * @since 4.3.1.0
     */
    public PopulationGroupDAO getPopulationGroupDAO() throws TopiaException {
        return IsisFishDAOHelper.getPopulationGroupDAO(getDB());
    }

    /**
     * Get {@link PopulationSeasonInfoDAO} on region database.
     * 
     * @return PopulationSeasonInfoDAO
     * @throws TopiaException
     * @since 4.3.1.0
     */
    public PopulationSeasonInfoDAO getPopulationSeasonInfoDAO() throws TopiaException {
        return IsisFishDAOHelper.getPopulationSeasonInfoDAO(getDB());
    }

    /**
     * Get {@link PortDAO} on region database.
     * 
     * @return PortDAO
     * @throws TopiaException
     * @since 4.3.1.0
     */
    public PortDAO getPortDAO() throws TopiaException {
        return IsisFishDAOHelper.getPortDAO(getDB());
    }

    /**
     * Get {@link ResultDAO} on region database.
     * 
     * @return ResultDAO
     * @throws TopiaException
     * @since 4.3.1.0
     */
    public ResultDAO getResultDAO() throws TopiaException {
        return IsisFishDAOHelper.getResultDAO(getDB());
    }

    /**
     * Get {@link SeasonDAO} on region database.
     * 
     * @return SeasonDAO
     * @throws TopiaException
     * @since 4.3.1.0
     */
    public SeasonDAO getSeasonDAO() throws TopiaException {
        return IsisFishDAOHelper.getSeasonDAO(getDB());
    }

    /**
     * Get {@link SelectivityDAO} on region database.
     * 
     * @return SelectivityDAO
     * @throws TopiaException
     * @since 4.3.1.0
     */
    public SelectivityDAO getSelectivityDAO() throws TopiaException {
        return IsisFishDAOHelper.getSelectivityDAO(getDB());
    }

    /**
     * Get {@link SetOfVesselsDAO} on region database.
     * 
     * @return SetOfVesselsDAO
     * @throws TopiaException
     * @since 4.3.1.0
     */
    public SetOfVesselsDAO getSetOfVesselsDAO() throws TopiaException {
        return IsisFishDAOHelper.getSetOfVesselsDAO(getDB());
    }

    /**
     * Get {@link SpeciesDAO} on region database.
     * 
     * @return SpeciesDAO
     * @throws TopiaException
     * @since 4.3.1.0
     */
    public SpeciesDAO getSpeciesDAO() throws TopiaException {
        return IsisFishDAOHelper.getSpeciesDAO(getDB());
    }

    /**
     * Get {@link StrategyDAO} on region database.
     * 
     * @return StrategyDAO
     * @throws TopiaException
     * @since 4.3.1.0
     */
    public StrategyDAO getStrategyDAO() throws TopiaException {
        return IsisFishDAOHelper.getStrategyDAO(getDB());
    }

    /**
     * Get {@link StrategyMonthInfoDAO} on region database.
     * 
     * @return StrategyMonthInfoDAO
     * @throws TopiaException
     * @since 4.3.1.0
     */
    public StrategyMonthInfoDAO getStrategyMonthInfoDAO() throws TopiaException {
        return IsisFishDAOHelper.getStrategyMonthInfoDAO(getDB());
    }

    /**
     * Get {@link TargetSpeciesDAO} on region database.
     * 
     * @return TargetSpeciesDAO
     * @throws TopiaException
     * @since 4.3.1.0
     */
    public TargetSpeciesDAO getTargetSpeciesDAO() throws TopiaException {
        return IsisFishDAOHelper.getTargetSpeciesDAO(getDB());
    }

    /**
     * Get {@link TripTypeDAO} on region database.
     * 
     * @return TripTypeDAO
     * @throws TopiaException
     * @since 4.3.1.0
     */
    public TripTypeDAO getTripTypeDAO() throws TopiaException {
        return IsisFishDAOHelper.getTripTypeDAO(getDB());
    }

    /**
     * Get {@link VariableDAO} on region database.
     * 
     * @return VariableDAO
     * @throws TopiaException
     * @since 4.3.1.0
     */
    public VariableDAO getVariableDAO() throws TopiaException {
        return IsisFishDAOHelper.getVariableDAO(getDB());
    }

    /**
     * Get {@link VesselTypeDAO} on region database.
     * 
     * @return VesselTypeDAO
     * @throws TopiaException
     * @since 4.3.1.0
     */
    public VesselTypeDAO getVesselTypeDAO() throws TopiaException {
        return IsisFishDAOHelper.getVesselTypeDAO(getDB());
    }

    /**
     * Get {@link ZoneDAO} on region database.
     * 
     * @return ZoneDAO
     * @throws TopiaException
     * @since 4.3.1.0
     */
    public ZoneDAO getZoneDAO() throws TopiaException {
        return IsisFishDAOHelper.getZoneDAO(getDB());
    }
}
