/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.simulator;

import fr.ifremer.isisfish.result.NecessaryResult;

/**
 * Script d'optimisation.
 *
 * @since 4.3.0.0
 */
public interface Optimization extends NecessaryResult {

    /**
     * Return plan description.
     *
     * @return plan description
     * @throws Exception
     */
    String getDescription() throws Exception;

    /**
     * Appele lors de l'initialisation. La premiere generation doit etre construite
     * dans l'init.
     *
     * @param context
     */
    void init(OptimizationContext context) throws Exception;

    /**
     * La premiere generation doit etre construite dans cette methode
     * dans l'init.
     *
     * @param context
     */
    void firstSimulation(OptimizationContext context) throws Exception;

    /**
     * Génère une nouvelle série de simulation suivant le context d'optimisation.
     * 
     * @param context context
     */
    void nextSimulation(OptimizationContext context) throws Exception;

    /**
     * Cette methode est appelee après chaque serie de simulation soit apres firstSimulation et
     * nextSimulation.
     * 
     * @param context
     */
    void endSimulation(OptimizationContext context) throws Exception;

    /**
     * Cette methode est appelee lorsqu'il n'y a plus de simulation a faire
     * (init ou nextSimulation n'ont pas fait appel a context.addSimulation)
     * @param context
     */
    void finish(OptimizationContext context) throws Exception;
}
