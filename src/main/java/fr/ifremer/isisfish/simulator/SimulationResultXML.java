/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2007 - 2011 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixIterator;
import org.nuiton.math.matrix.MatrixND;
import static org.nuiton.i18n.I18n.t;

import fr.ifremer.isisfish.IsisFishException;
import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.types.TimeStep;


/**
 * &lt;!ELEMENT outData (m,d,i,s)*&gt;
 * &lt;!ELEMENT m (dim+, (d|i|s)+)&gt;
 * &lt;!ELEMENT dim (label)+)&gt;
 * &lt;!ELEMENT d (#PCDATA)&gt;
 * &lt;!ELEMENT i (#PCDATA)&gt;
 * &lt;!ELEMENT s (#PCDATA)&gt;
 * 
 * &lt;!ATTLIST m 
 *     name CDATA #REQUIRED
 *     step CDATA #IMPLIED&gt;
 *
 * &lt;!ATTLIST dim 
 *     name CDATA #REQUIRED
 *     size CDATA #REQUIRED&gt;
 * 
 * &lt;!ATTLIST d 
 *     step CDATA #IMPLIED&gt;
 * 
 * &lt;!ATTLIST i
 *     step CDATA #IMPLIED&gt;
 * 
 * &lt;!ATTLIST s
 *     step CDATA #IMPLIED&gt;
 * 
 * @author poussin
 */
public class SimulationResultXML implements SimulationResultListener {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(SimulationResultXML.class);

    protected Writer out = null;
    protected File outFilename = null;
    
    public SimulationResultXML() {
    }

    public SimulationResultXML(File outfilename) {
        setOutFilename(outfilename);
    }

    
    /**
     * @param outfilename The outfilename to set.
     */
    public void setOutFilename(File outfilename) {
        this.outFilename = outfilename;
    }
        
    /**
     * @return Returns the outfilename.
     */
    public File getOutFilename() {
        //if (outFilename == null) {
        //    outFilename = new File(IsisConfig.SIMULATION_RESULT_XML_FILE);
        //}
        return outFilename;
    }
    
    @Override
    public void afterSimulation(SimulationContext context) {
        try {
            out.write("</outData>\n");
            out.close();
        } catch (IOException eee) {
            if (log.isWarnEnabled()) {
                log.warn(t("isisfish.error.simulation.resultXml.close",eee.getMessage()), eee);
            }
        }
    }

    @Override
    public void beforeSimulation(SimulationContext context) {
        try {
            File file = getOutFilename();
            //TODO validate this modification
            if (file==null) {
                File simulationDir = context.getSimulationStorage().getDirectory();
                file = SimulationStorage.getSimulationResultXmlFile(simulationDir);
                setOutFilename(file);
            }
            file.getParentFile().mkdirs();
            out = new BufferedWriter(new FileWriter(file));
            out.write("<outData>\n");
        } catch (IOException eee) {
            if (log.isWarnEnabled()) {
                log.warn(t("isisfish.error.simulation.resultXml.open",eee.getMessage()), eee);
            }
        }
    }

    @Override
    public void addResult(SimulationContext context, TimeStep step, String name,
            MatrixND mat) throws IsisFishException {
        try {
            out.write("  <m name='" + name + "' step='" + step + "'>\n");
            for (int dim=0; dim<mat.getDimCount(); dim++) {
                out.write("    <dimension name='" + mat.getDimensionName(dim) + "' size='" + mat.getDim(dim) + "'>");
                for (Object sem : mat.getSemantic(dim)) {
                    out.write("      <label>" + sem + "</label>");
                }
                out.write("    </dimension>");
            }
            for(MatrixIterator i=mat.iterator();i.hasNext();) {
                out.write("    <d>" + i.getValue() + "</d>");
            }
            out.write("  </m>\n");
        } catch (IOException eee) {
            if (log.isWarnEnabled()) {
                log.warn(t("isisfish.error.simulation.resultXml.write",eee.getMessage()), eee);
            }
        }
        
    }

    @Override
    public void stepChange(SimulationContext context, TimeStep step) {
    }

}


