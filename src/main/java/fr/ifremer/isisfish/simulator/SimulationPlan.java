/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator;

import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.result.NecessaryResult;

/**
 * Simulation plan.
 * 
 * Replace old {@code AnalysePlan}.
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public interface SimulationPlan extends NecessaryResult {

    /**
     * Return plan description.
     * 
     * @return plan description
     * @throws Exception
     */
    String getDescription() throws Exception;

    /**
     * Use only once before first simulation.
     * 
     * You can modified Param for all simulation or put value in context.values.
     * 
     * @param context plan context
     * @throws Exception
     */
    void init(SimulationPlanContext context) throws Exception;

    /**
     * Call before each simulation.
     * 
     * @param context plan context
     * @param nextSimulation storage used for next simulation
     * @return true if we must do next simulation, false to stop plan
     * @throws Exception
     */
    boolean beforeSimulation(SimulationPlanContext context,
                             SimulationStorage nextSimulation) throws Exception;

    /**
     * Call after each simulation.
     * 
     * @param context plan context
     * @param lastSimulation storage used for simulation
     * @return true if we must do next simulation, false to stop plan
     * @throws Exception
     */
    boolean afterSimulation(SimulationPlanContext context,
                            SimulationStorage lastSimulation) throws Exception;

}
