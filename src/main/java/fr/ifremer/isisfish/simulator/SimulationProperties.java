/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2007 - 2018 Ifremer, Code Lutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator;

import static org.nuiton.i18n.I18n.t;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.ifremer.isisfish.datastore.SimulationStorage;

/**
 * This class contains all usefull informations about a simulation.
 * 
 * This is used for filter or search a simulation.
 * <p>
 * This is a mix of SimulationParameter and SimulationInformation.
 * <p>
 * The class is only a properties container, no method to access a data directly
 * (except to obtain name of underlying simulation).
 */
public class SimulationProperties {

    private static final Log log = LogFactory.getLog(SimulationProperties.class);

    protected String name;
    protected Properties data;

    public SimulationProperties(String name) throws IOException {
        File simulationDirectory = SimulationStorage.getSimulationDirectory(name);
        this.name = simulationDirectory.getName();
        this.data = new Properties();
        data.put("simulationName", this.name);
        loadProperties(SimulationStorage.getSimulationParametersFile(simulationDirectory));
        loadProperties(SimulationStorage.getSimulationInformationFile(simulationDirectory));
    }

    private void loadProperties(File file) {
        if (file.exists()) {
            try (Reader reader = new BufferedReader(new FileReader(file))) {
                data.load(reader);
                
                // FIXME fix a memory leak with huge prescript
                data.remove("preScript");
            } catch (IOException e) {
                if (log.isWarnEnabled()) {
                    log.warn(t("isisfish.error.load.file", file));
                }
            }
        }
    }

    public Properties getData() {
        return data;
    }

    public String getName() {
        return name;
    }
}
