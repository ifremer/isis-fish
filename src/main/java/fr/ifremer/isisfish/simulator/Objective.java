/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.simulator;

import java.util.List;

import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.result.NecessaryResult;
import org.nuiton.math.matrix.MatrixND;

/**
 * Interface des fonctions d'objectifs.
 *
 * @since 4.3.0.0
 */
public interface Objective extends NecessaryResult {

    /**
     * Return plan description.
     *
     * @return plan description
     * @throws Exception
     */
    String getDescription() throws Exception;

    /**
     * Effectue une evaluation entre les exports et les observations.
     * 
     * @param context optimisation context
     * @param simulation current simulation
     * @param exports exports
     * @param observations observations
     * @return double value
     */
    double eval(OptimizationContext context, SimulationStorage simulation, List<MatrixND> exports, List<MatrixND> observations);
}
