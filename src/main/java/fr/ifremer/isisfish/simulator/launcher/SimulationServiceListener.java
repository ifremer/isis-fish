/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2010 Ifremer, Code Lutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator.launcher;

/**
 * Ecouteur sur les evenenements de changement d'états des simulations.
 * 
 * @author poussin
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public interface SimulationServiceListener {

    /**
     * Notify start of simulation.
     * 
     * @param simService le simulation service qui a lance la simulation
     * @param job job that responsible to simulation
     */
    void simulationStart(SimulationService simService, SimulationJob job);

    /**
     * Notify end of simulation.
     * 
     * @param simService le simulation service qui a arrete la simulation
     * @param job job that responsible to simulation
     */
    void simulationStop(SimulationService simService, SimulationJob job);

    /**
     * Previent que le simulation service a vide ca liste de simulation faites.
     * 
     * @param simService le simulation service dont la liste des jobs finis
     * a ete vide
     */
    void clearJobDone(SimulationService simService);
}
