/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2024 Ifremer, Code Lutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator.launcher;

import static org.nuiton.i18n.I18n.t;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import fr.ifremer.isisfish.aspect.ComputeResultAspect;
import fr.ifremer.isisfish.datastore.FormuleStorage;
import fr.ifremer.isisfish.util.EvaluatorHelper;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.ThreadContext;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.event.TopiaTransactionEvent;
import org.nuiton.topia.event.TopiaTransactionListener;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.ObjectUtil;
import org.nuiton.util.StringUtil;

import fr.ifremer.isisfish.config.IsisConfig;
import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.aspect.AspectJUrlClassLoader;
import fr.ifremer.isisfish.aspect.CacheAspect;
import fr.ifremer.isisfish.aspect.RuleAspect;
import fr.ifremer.isisfish.aspect.TraceAspect;
import fr.ifremer.isisfish.datastore.ResultStorage;
import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.datastore.SimulatorStorage;
import fr.ifremer.isisfish.logging.SimulationThresholdFilter;
import fr.ifremer.isisfish.simulator.SimulationContext;
import fr.ifremer.isisfish.simulator.SimulationControl;
import fr.ifremer.isisfish.simulator.SimulationException;
import fr.ifremer.isisfish.simulator.SimulationExportResultWrapper;
import fr.ifremer.isisfish.simulator.SimulationListener;
import fr.ifremer.isisfish.simulator.SimulationParameter;
import fr.ifremer.isisfish.simulator.SimulationPreScriptListener;
import fr.ifremer.isisfish.simulator.Simulator;
import fr.ifremer.isisfish.types.TimeStep;
import fr.ifremer.isisfish.util.CompileHelper;

import org.nuiton.math.matrix.MatrixFactory;

/**
 * Fait une simulation dans la meme jvm.
 * 
 * @author poussin
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class InProcessSimulatorLauncher implements SimulatorLauncher {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    private static Log log = LogFactory
            .getLog(InProcessSimulatorLauncher.class);

    protected SimulationStorage simulation;
    
    @Override
    public void simulate(SimulationService simulationService, SimulationItem simulationItem) throws RemoteException {

        // get real simulation informations for this launcher
        SimulationControl control = simulationItem.getControl();
        File simulationZip = simulationItem.getSimulationZip();
        String generatedPrescript = simulationItem.getGeneratedPrescriptContent();
        
        String id = control.getId();
        if (log.isInfoEnabled()) {
            log.info(t("simulate %s with file %s", id, simulationZip));
        }

        try {

            // remove simulation if already exists
            if (SimulationStorage.localyExists(id)) {
                if (log.isWarnEnabled()) {
                    log.warn("Warning , simulation " + id + " aleady exists");
                    log.warn("Deleting it before doing simulation");
                }
                // storage can be opened (result UI)
                SimulationStorage storage = SimulationStorage.getSimulation(id);
                storage.closeStorage();
                
                FileUtils.deleteQuietly(storage.getFile());
            }

            if (log.isDebugEnabled()) {
                log.debug("Timing : before import zip : " + new java.util.Date());
            }
            simulation = SimulationStorage
                    .importAndRenameZip(simulationZip, id);
            if (log.isDebugEnabled()) {
                log.debug("Timing : after import zip : " + new java.util.Date());
            }

            // WARNING: make sure to not open Region before being in simulation context
            // add missing control informations
            SimulationParameter param = simulation.getParameter();
            control.setStep(new TimeStep());
            control.setProgress(0);
            control.setStarted(true);

            // replace prescript if specified on method
            if (StringUtils.isNotBlank(generatedPrescript)) {
                param.setGeneratedPreScript(generatedPrescript);
            }

            int lastDate = param.getNumberOfMonths();
            control.setProgressMax(lastDate);

            simulation = localSimulate(control, simulation);
        } catch (Exception eee) {
            log.error(t("Can't do simulation %s", id), eee);
            if (simulation != null) {
                simulation.getInformation().setException(eee);
            }
        }
    }

    @Override
    public SimulationStorage getSimulationStorage(
            SimulationService simulationService, SimulationControl control)
            throws RemoteException {

        return simulation;

    }

    @Override
    public void updateControl(SimulationService simulationService,
            SimulationControl control) throws RemoteException {
        // in this case, control is set directly by main thread
    }

    @Override
    public int maxSimulationThread() {
        return IsisFish.config.getSimulatorInMaxThreads();
    }

    @Override
    public int getCheckProgressionInterval() {

        // par defaut, pour les in process 5 secondes
        int interval = 1;
        return interval;
    }

    @Override
    public String toString() {
        return t("isisfish.simulator.launcher.inprocess");
    }

    /**
     * Display message both in commons-logging and control text progress.
     * 
     * @param control control
     * @param message message to display
     */
    protected void message(SimulationControl control, String message) {
        // log
        if (log.isInfoEnabled()) {
            log.info(message);
        }
        // control
        if (control != null) {
            control.setText(message);
        }
    }

    /**
     * fait la simulation en local dans un nouveau thread, cela permet
     * pour chaque simulation d'avoir les bons scripts dans le classloader
     * et non pas d'utiliser les scripts d'une autre simulation
     * 
     * @param control le controleur de simulation, peut-etre null si on ne 
     * souhaite pas controler la simulation
     * @param simulation la simulation a faire
     * @return le storage après simulation en locale
     */
    protected SimulationStorage localSimulate(SimulationControl control,
            SimulationStorage simulation) {
        SimThread simThread = new SimThread(control, simulation);
        simThread.start();
        try {
            simThread.join();
        } catch (InterruptedException eee) {
            if (log.isWarnEnabled()) {
                log.warn(t("isisfish.error.wait.simThread"), eee);
            }
        }

        return simulation;
    }

    protected class SimThread extends Thread {
        protected SimulationControl control;
        protected SimulationStorage simulation;

        public SimThread(SimulationControl control, SimulationStorage simulation) {
            super("SimThread " + (control != null ? control.getId() : ""));
            this.control = control;
            this.simulation = simulation;
        }

        @Override
        public void run() {

            // logger configuration can only be changed with log4j MDC using thread local
            String simulLogLevel = simulation.getParameter().getSimulLogLevel();
            String scriptLogLevel = simulation.getParameter().getScriptLogLevel();
            String libLogLevel = simulation.getParameter().getLibLogLevel();

            String standardSimulLogLevel = Level.toLevel(simulLogLevel, Level.INFO).name();
            String standardLibLogLevel = Level.toLevel(libLogLevel, Level.INFO).name();
            String standardScriptLogLevel = Level.toLevel(scriptLogLevel, Level.WARN).name();

            ThreadContext.put(SimulationThresholdFilter.SIMULATION_ID, simulation.getName());
            ThreadContext.put(SimulationThresholdFilter.SIMULATION_LOG_FILE, simulation.getSimulationLogFile());
            ThreadContext.put(SimulationThresholdFilter.SIMULATION_APPLICATION_LEVEL, standardSimulLogLevel);
            ThreadContext.put(SimulationThresholdFilter.SIMULATION_SCRIPT_LEVEL, standardScriptLogLevel);
            ThreadContext.put(SimulationThresholdFilter.SIMULATION_LIB_LEVEL, standardLibLogLevel);

            simulation = localSimulateSameThread(control, simulation);
        }
    }

    /**
     * Modifie le classloader du thread passé en paramètre. 
     * <p>
     * Sert pour les simulations pour qu'elles puissent trouver les scripts, rule et export
     *  
     * @param thread le thread dont on souhaite modifier le classloader ou null
     * @param directory le répertoire qui servira pour le classloader
     * @return le class loader modifié
     */
    protected AspectJUrlClassLoader changeClassLoader(Thread thread, File directory) {
        /*
        FIXME echatellier 20140419 : normalement c'est ce code original qui est bon
        il fonctionne sur eclipse, mais pas en java -jar ...
        try {
            URL[] classpath = new URL[] { directory.toURI().toURL(),
            // poussin 20080821 : il semble ne plus trouve les formules,
                    // est-ce mieux avec le compile dir ?
                    IsisFish.config.getCompileDirectory().toURI().toURL() };
            AspectJUrlClassLoader loader = new AspectJUrlClassLoader(classpath,
                    IsisFish.class.getClassLoader());
            thread.setContextClassLoader(loader);
            return loader;
        } catch (MalformedURLException eee) {
            // on leve un runtime, car normalement cette erreur est pratiquement
            // impossible car on creer l'url a partir d'un File ce qui ne pose
            // normalement pas de probleme
            throw new IsisFishRuntimeException(t("isisfish.error.change.classloader", directory), eee);
        }*/

        // FIXME echatellier 20140419 : pour que aspectj fonctionne, on lui construit un urlclassloader
        // complet avec toutes les urls
        try {
            List<URL> urls = new ArrayList<>();
            for (Enumeration<?> e = CompileHelper.class.getClassLoader()
                    .getResources("META-INF/MANIFEST.MF"); e.hasMoreElements();) {
                URL url = (URL) e.nextElement();
                if (log.isDebugEnabled()) {
                    log.debug("Found manifest : " + url);
                }
                if ((url != null) && url.getFile().startsWith("file:/")) {
                    String jarPath = url.getPath().substring(0,
                            url.getPath().indexOf("!"));
                    urls.add(new URL(jarPath));
                }
            }

            // le dossier de compilation contient des données plus fraiches que le dossier de simulation (equation recompilée)
            urls.add(IsisFish.config.getCompileDirectory().toURI().toURL());
            // dossier de simulation (moins prioritaire que compile directory)
            urls.add(directory.toURI().toURL());

            AspectJUrlClassLoader loader = new AspectJUrlClassLoader(urls.toArray(new URL[0]),
                    IsisFish.class.getClassLoader());
            thread.setContextClassLoader(loader);
            return loader;
        } catch (Exception eee) {
            // on leve un runtime, car normalement cette erreur est pratiquement
            // impossible car on creer l'url a partir d'un File ce qui ne pose
            // noralement pas de probleme
            throw new IsisFishRuntimeException(t("isisfish.error.change.classloader", directory), eee);
        }
    }

    /**
     * Cree le simulation context, creer le ClassLoader, met en place les AOP,
     * met a jour des informations sur la simulation et lance la simulation en
     * local
     * 
     * @param control le controleur de simulation, peut-etre null si on ne 
     * souhaite pas controler la simulation
     * @param simulation la simulation a faire
     * @return le storage après simulation en locale
     */
    protected SimulationStorage localSimulateSameThread(SimulationControl control, SimulationStorage simulation) {
        SimulationStorage result = null;
        String jvmVersion = System.getProperty("java.runtime.version");
        log.info(SimpleDateFormat.getInstance().format(new java.util.Date())
                + " Java version: " + jvmVersion + " Isis-fish version: "
                + IsisConfig.getVersion());
        long start = System.nanoTime();
        simulation.getInformation().setSimulationStart(new java.util.Date());
        AspectJUrlClassLoader classLoader = null;
        try {
            File rootDirectory = simulation.getDirectory();

            //
            // Creation et initialisation du context de simulation
            //
            SimulationContext context = SimulationContext.get();

            control = control != null ? control
                    : new SimulationControl(simulation.getName());
            context.setSimulationControl(control);

            // make compile directory outside getCompileDirectory() to improve performance
            File compileDirectory = IsisFish.config.getCompileDirectory();
            compileDirectory.mkdirs();

            // changement de classloader
            // IMPORTANT : must be set AFTER :
            //  - SimulationContext.get();
            //  - context.setSimulationControl()
            classLoader = changeClassLoader(Thread.currentThread(), rootDirectory);
            context.setClassLoader(classLoader);
            
            // this directory is used to change isis-database root directory
            // is simulation context
            context.setScriptDirectory(rootDirectory);
            context.setSimulationStorage(simulation);

            // reload formule cache (if present)
            File formuleDir = new File(rootDirectory, FormuleStorage.FORMULE_PATH);
            EvaluatorHelper.importCache(formuleDir);

            // Warning : Rule have to be instanciated after aspect definition
            classLoader.deploy(RuleAspect.class);

            SimulationParameter parameters = simulation.getParameter();
            parameters.setIsisFishVersion(IsisConfig.getVersion());

            // AOP : compute result
            if (parameters.getUseComputeResult()) {
                message(control, t("isisfish.message.setting.compute.aspects"));
                classLoader.deploy(ComputeResultAspect.class);
            }

            // forceReload, save all modification in parameter and reread it
            parameters = simulation.getForceReloadParameter();

            // must be done after setSimulationStorage because config looking for
            // value in simulation parameter via SimulationContext
            context.initForSimulation();

            // Activation de l'OAP Statistic
            if (parameters.getUseStatistic()) {
                message(control, t("isisfish.message.setting.trace.aspects"));
                classLoader.deploy(TraceAspect.class);
            }
            // Activation de l'OAP Cache
            if (parameters.getUseCache()) {
                message(control, t("isisfish.message.setting.cache.aspects"));
                classLoader.deploy(CacheAspect.class);
            }

            classLoader.prepare();

            // recherche du simulateur a utiliser
            String simulatorName = parameters.getSimulatorName();
            SimulatorStorage simulator = SimulatorStorage.getSimulator(simulatorName);
            Simulator simulatorObject = simulator.getNewInstance();

            // on se met listener sur tc pour connaitre tous les nouveaux objets
            ObjectCreationListener objectCreationListener = new ObjectCreationListener();
            context.getDB().addTopiaTransactionListener(objectCreationListener);

            // 
            // Ajout des listeners pour la simulation
            // 
            initSimulationListener(context);

            //
            // Lancement du script de simulation selectionné
            //
            message(control, t("isisfish.message.simulation.execution"));

            //
            // Call listener simulation (used per example for prescript)
            //
            context.fireBeforeSimulation();
            String matrixBackend = MatrixFactory.getInstance().getVectorClass().getName();
            String matrixSparseBackend = MatrixFactory.getInstance().getSparseVectorClass().getName();
            int threshold = MatrixFactory.getInstance().getThresholdSparse();
            log.info("Matrix backend: " + matrixBackend +  " and " + matrixSparseBackend + " threshold: " + threshold);
            simulatorObject.simulate(context);

            simulation.getInformation().addInformation(
                    context.getTimeStepStat().exportText(null).toString());

            // on ajoute les info sur le resultStorage avant les exports qui
            // peuvent changer les stats
            ResultStorage resultStorage = simulation.getResultStorage();
            simulation.getInformation().addInformation(resultStorage.getInfo());

            //
            // Ajout des nouveaux objets créés durant la simulation
            //            
            message(control, t("isisfish.message.add.objets.simulation"));
            // on ajoute sur le DBResult car pour les exports peut-etre auront
            // nous besoin de ces nouveaux objets, et durant la simulation
            // les resultats sont recuperer dans le DBResult
            TopiaContext add = context.getDbResult();
            for (TopiaEntity e : objectCreationListener.getNewObjects()) {
                if (log.isDebugEnabled()) {
                    log.debug("Add new object: " + e + "(" + e.getClass().getName() + ")");
                }
                add.add(e);
            }
            add.commitTransaction();

            //
            // Call listener simulation (used per example for export)
            //
            context.fireAfterSimulation();

            // suppression des résultats si l'utilisateur a demande à ne conserver
            // que les resultats de seulement la première simulation d'une AS
            if (control.getId().startsWith("as_")) {
                if (parameters.isSensitivityAnalysisOnlyKeepFirst() && !control.getId().endsWith("_0")) {
                    resultStorage.delete();
                }
            }
            if (control.getId().startsWith("sim_")) {
                if (parameters.isResultDeleteAfterExport()) {
                    resultStorage.delete();
                }
            }

            resultStorage.close();

            // copie des formules compilées dans le cache de region
            File compiledFormuleDir = new File(IsisFish.config.getCompileDirectory(), FormuleStorage.FORMULE_PATH);
            if (compiledFormuleDir.isDirectory()) {
                File regionCacheDir = new File(IsisFish.config.getRegionCacheDirectory(), parameters.getRegionName());
                File regionCacheFormuleDir = new File(regionCacheDir, FormuleStorage.FORMULE_PATH);
                regionCacheFormuleDir.mkdirs();
                FileUtils.copyDirectory(compiledFormuleDir, regionCacheFormuleDir);
                EvaluatorHelper.exportCache(regionCacheFormuleDir); // json
            }

        } catch (OutOfMemoryError eee) {
            log.error(t("isisfish.error.during.simulation"), eee);
            simulation.getInformation().setException(eee);
            throw new SimulationException(t("isisfish.error.out.memory"), eee);
        } catch (Exception eee) {
            log.error(t("isisfish.error.during.simulation"), eee);
            simulation.getInformation().setException(eee);
            throw new SimulationException(t("isisfish.error.during.simulation"), eee);
        } finally {
            //
            // Finaly close all TopiaContext used during simulation
            //
            SimulationContext context = SimulationContext.get();

            context.closeDB();
            context.closeDBResult();

            try {
                // close all transaction (root context)
                if (context.getSimulationStorage() != null) {
                    context.getSimulationStorage().closeMemStorage();
                    context.getSimulationStorage().closeStorage();
                }
            } catch (TopiaException eee) {
                if (log.isWarnEnabled()) {
                    log.warn("Can't close all transaction", eee);
                }
            }

            //
            // Affichage des statistiques
            //
            long end = System.nanoTime();
            log.info("Simulation time: "
                    + DurationFormatUtils.formatDuration(
                            (end - start) / 1000000, "s'.'S"));
            SimulationParameter param = simulation.getParameter();
            if (param.getUseStatistic()) {
                String trace = context.getTrace().getStatisticsCSV();
                simulation.getInformation().setStatistic(trace);
            }
            if (param.getUseCache()) {
                String cache = context.getCache().printStatistiqueAndClear();
                simulation.getInformation().setOptimizationUsage(cache);
            }
            if (param.getUseComputeResult()) {
                String trace = context.getComputeResultTrace().getStatisticsText();
                simulation.getInformation().setComputeResult(trace);
            }

            // try to fix memory leak
            LogFactory.release(classLoader);

            // close classloader to release resource
            if (classLoader != null) {
                try {
                    classLoader.close();
                } catch (IOException e) {
                    // ignored
                }
            }

            // cleanup specific context build directory
            File simulationBuildDirectory = IsisFish.config.getCompileDirectory();
            if (log.isDebugEnabled()) {
                log.debug("Delete simulation build directory : " + simulationBuildDirectory.getAbsolutePath());
            }
            FileUtils.deleteQuietly(simulationBuildDirectory);


            simulation.getInformation().setSimulationEnd(new java.util.Date());

            // la simulation est termine on avance la progress au dernier cran
            // attention on utilise ca aussi pour detecter la fin d'une simulation
            // quand date =progressMax
            control.setProgress(control.getProgress() + 1);

            message(control, t("isisfish.message.simulation.ended"));
            control.stopSimulation();

            // context is used in TraceAspect.printStatistiqueAndClear()
            SimulationContext.remove();
        }
        return result;
    }

    protected void initSimulationListener(SimulationContext context)
            throws Exception {
        SimulationStorage simulation = context.getSimulationStorage();
        // enregistrement des listeners de resultat
        // - ResultStorage
        // TODO poussin 20141219 ca ne sert plus a rien que le result storage
        // soit listener avec ResultStorageCSV, donc a supprimer et l'implement
        // dans ResultStorage de SimulationListener
        context.addSimulationListener(simulation.getResultStorage());

        // - TODO: mexico xml result
        // - TODO: vle result
        String simListener = context.getSimulationStorage().getParameter()
                .getTagValue().get("SimulationListener");
        if (simListener != null) {
            String[] simListeners = StringUtil.split(simListener, ",");
            for (String l : simListeners) {
                context.addSimulationListener((SimulationListener) ObjectUtil
                        .create(l));
            }
        }

        // enregistrement des listeners de simulation
        // - prescript (before simulation)
        // - export (after simulation)
        context.addSimulationListener(new SimulationPreScriptListener());
        // si le simulateur est de type SimulationStep il faut ajouter les regles 
        context.addSimulationListener(new SimulationExportResultWrapper());
    }

    protected static class ObjectCreationListener implements TopiaTransactionListener {

        /** List qui contient tous les objets creer durant la simulation */
        protected List<TopiaEntity> newObjects = new ArrayList<>();

        /**
         * @return Returns the newObjects.
         */
        public List<TopiaEntity> getNewObjects() {
            return this.newObjects;
        }

        @Override
        public void commit(TopiaTransactionEvent event) {
            // rien a faire, car normalement toujours rollback en fin de mois

        }

        @Override
        public void rollback(TopiaTransactionEvent event) {
            log.debug("Transaction rollback " + event.getEntities().size()
                    + " object(s)");
            // FIXME le jour ou on aura l'isolation on pourra directement
            // ajouter dans un autre TopiaContext les objets ajouté durant la
            // simulation de cette maniere les objets creer au pas de temps
            // N seront dispo pour etre utilisé au pas de temps N+1
            // Sinon une autre methode est de faire cette ajout
            // dans l'event rollback qui est leve a la fin de chaque pas de temps
            for (TopiaEntity entity : event.getEntities()) {
                if (event.isCreate(entity)) {
                    log.debug("New object detected during simulation: "
                            + entity + "(" + entity.getClass().getName() + ")");
                    newObjects.add(entity);
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     * 
     * Do nothing (no restriction on inprocess launcher).
     */
    @Override
    public void simulationStopRequest(SimulationJob job) {

    }

}
