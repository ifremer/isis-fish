/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2010 Ifremer, Code Lutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator.launcher;

import java.rmi.RemoteException;

import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.simulator.SimulationControl;

/**
 * Interface devant etre implantée par les classes souhaitants etre utilisees
 * comme plugin de simulation (InProcess, SubProcess, Datarmor, ...)
 * 
 * @author poussin
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public interface SimulatorLauncher {

    /**
     * Execute une simulation.
     * 
     * @param simulationService le {@link SimulationService} qui a initie
     *      la simulation
     * @param simulationItem l'item contenant toutes les infos nécessaire au
     *      lancement de la simulation
     * 
     * <b>IMPORTANT</b> Quoi qu'il arrive 
     * @throws RemoteException Si pour l'execution de la simulation
     * on utilise des resources distantes (serveurs) et que ceux si non pas pu
     * etre contacte. Dans ce cas la simulation n'a pas ete faite et cette
     * exception est levee.
     */
    void simulate(SimulationService simulationService,
                  SimulationItem simulationItem) throws RemoteException;

    /**
     * Retourne le nombre maximal de thread de simulations simultanées supporté.
     * 
     * @return un nombre superieur a 0
     */
    int maxSimulationThread();

    /**
     * Retourne l'intervalle de temps a utiliser entre deux
     * verification de progresssion.
     * 
     * @return intervalle (en secondes);
     */
    int getCheckProgressionInterval();

    /**
     * Retourne le storage où est stockée la simulation.
     * 
     * @param simulationService le {@link SimulationService} qui a initie
     * la simulation
     * @param control le control de la simulation
     * 
     * @return le storage contenant la simulation qui vient d'etre faite
     * ou null en fait au lieu de faire une seul simulation, plusieurs ou ete
     * faite par exemple tout un plan de simulation Cette classe est tout de meme
     * responsable du stockage de chaque simulation dans des storages.
     * 
     * @throws RemoteException Si pour l'execution de la simulation
     * on utilise des resources distantes (serveurs) et que ceux si non pas pu
     * etre contacte.
     */
    SimulationStorage getSimulationStorage(SimulationService simulationService, SimulationControl control)
            throws RemoteException;

    /**
     * Met à jour la progression.
     * 
     * @param simulationService le {@link SimulationService} qui a initie
     * la simulation
     * @param control le control de la simulation
     * 
     * @throws RemoteException Si pour l'execution de la simulation
     * on utilise des resources distantes (serveurs) et que ceux si non pas pu
     * etre contacte.
     */
    void updateControl(SimulationService simulationService,
                       SimulationControl control) throws RemoteException;

    /**
     * Called if a started simulation has been ask to stop. Tell launcher to
     * stop simulation if possible.
     * 
     * @param job job to stop
     * 
     * @throws RemoteException Si pour l'execution de la simulation
     * on utilise des resources distantes (serveurs) et que ceux si non pas pu
     * etre contacte.
     */
    void simulationStopRequest(SimulationJob job) throws RemoteException;
}
