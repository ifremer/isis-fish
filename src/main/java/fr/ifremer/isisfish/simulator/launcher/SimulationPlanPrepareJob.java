/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 - 2017 Ifremer, Code Lutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator.launcher;

import static org.nuiton.i18n.I18n.t;

import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.logging.SimulationLoggerUtil;
import fr.ifremer.isisfish.simulator.SimulationControl;
import fr.ifremer.isisfish.simulator.SimulationParameter;
import fr.ifremer.isisfish.simulator.SimulationPlan;
import fr.ifremer.isisfish.simulator.SimulationPlanContextInternal;
import java.io.File;
import java.util.Iterator;
import java.util.List;

import fr.ifremer.isisfish.util.IsisFileUtil;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.i18n.I18n;

/**
 * Permet de genere les sous simulations d'un plan de simulation. Pour les
 * plan independant, on l'utilise en Runnable pour genere tous les plans
 * possible et les soumettre a la queue. Pour les plans dependant
 * on l'utilise seulement comme iterator. La methode afterSimulation des plans
 * est appelee automatiquement a la fin de la simulation grace au mecanisme
 * de PostAction sur les {@link SimulationJob}.
 */
public class SimulationPlanPrepareJob implements Runnable, Iterator<SimulationJob>, SimulationJob.PostAction {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    private static final Log log = LogFactory.getLog(SimulationPlanPrepareJob.class);

    protected SimulationService simulationService;
    protected SimulationPlanContextInternal planContext;
    protected SimulationJob job;
    protected SimulationJob nextJob;
    protected boolean doNext = true;
    protected String id;
    protected SimulationControl control;
    protected SimulationParameter param;
    protected int done = 0;
    protected List<SimulationPlan> simulationPlan;

    public SimulationPlanPrepareJob(SimulationService simulationService, SimulationJob job) {
        this.simulationService = simulationService;
        this.job = job;
        id = job.getItem().getControl().getId();
        control = job.getItem().getControl();
        param = job.getItem().getParameter();
        // take a copy of simulation plan list
        // because they a freed during simulation (soft reference)
        // is there is not enought memory available
        simulationPlan = param.getSimulationPlans();
        this.planContext = new SimulationPlanContextInternal(control.getId(), param);
        try {
            // appel de init sur chaque plan
            for (SimulationPlan plan : simulationPlan) {
                plan.init(planContext);
            }
        } catch (Exception eee) {
            // add manual log
            // we are in a thread, IsisFishRuntimeException is displayed
            // outside a log
            if (log.isErrorEnabled()) {
                log.error(t("isisfish.error.evaluate.preplan.script"), eee);
            }
            throw new IsisFishRuntimeException(t("isisfish.error.evaluate.preplan.script"), eee);
        }
    }

    /**
     * Genere toutes les sous simulations et les places dans la queue.
     *
     * Cette methode {@code run()} est appelée seulement dans le cas de la
     * génération de plans indépendants.
     *
     * @see SimulationJob#run() pour les plans dépendants
     */
    public void run() {
        try {
            SimulationLoggerUtil.setPlanLoggingContext(id);
            runJob();
        } finally {
            SimulationLoggerUtil.clearPlanLoggingContext();
        }
    }

    public void runJob() {
        // New iteration remember always simulation N and N+1
        // needed to know witch simulation is the last one
        // get simulation N
        SimulationJob subJobN = null;
        if (hasNext()) {
            subJobN = next();
        }
        while (subJobN != null) {
            try {
                if (log.isInfoEnabled()) {
                    log.info("Simulation generated: " + subJobN.getId());
                }
                // set item additionnal informations
                SimulationItem itemN = subJobN.getItem();
                itemN.setStandaloneSimulation(false); // independant plan
                // - 1 because planContext.getNumber() is set to next simulation to generate
                itemN.setSimulationNumber(planContext.getNumber() - 1);
                // job N+1
                // carefull call this next after itemN.setSimulationNumber()
                SimulationJob subJobNp1 = next();
                if (subJobNp1 == null) {
                    // there is no N+1 job, N is the last one
                    itemN.setLastSimulation(true);
                }
                simulationService.submitSubJob(subJobN);
                subJobN = subJobNp1;
            } catch (Exception eee) {
                if (log.isErrorEnabled()) {
                    log.error(t("Can't add simulation: %s", job.getItem().getControl().getId()), eee);
                }
            }
        }
    }

    /**
     * Indique s'il y a encore des simulations dans le plan. Par defaut pour
     * Eviter les plans sans fin, le nombre de plan genere par simulation
     * est limite a {@link SimulationService#MAX_PLAN_SIMULATION}
     *
     * @return {@code true} if has next
     */
    public boolean hasNext() {
        try {
            // if user request stop simulation, stop all futur planned simulation
            // and if last doNext is false not do next simulation
            boolean result = !control.isStopSimulationRequest() && doNext;
            if (result) {
                // hasNext() est appelee par un autre thread concurrent
                // via la methode finished(SimulationJob, SimulationStorage)
                synchronized (this) {
                    // si deja creer on ne le refait pas
                    if (nextJob == null) {
                        // Prepration de la simulation a faire
                        // create next id simulation
                        // this start a 0
                        int planNumber = planContext.getNumber();
                        if (planNumber > SimulationService.MAX_PLAN_SIMULATION) {
                            log.error(t("Analyse plan error, too many simulation for %s : %s", id, planNumber));
                            doNext = false;
                            result = false;
                        } else {
                            String simId = id + "_" + planNumber;
                            param.setSimulationPlanNumber(planNumber);
                            File tmpDirectory = IsisFileUtil.createTempDirectory("isisfish-simulation-", "-preparation");
                            SimulationStorage sim = SimulationStorage.importAndRenameZip(tmpDirectory, job.getItem().getSimulationZip(), simId);
                            sim.getParameter().setSimulationPlanNumber(planNumber);
                            // appel de tous les plans pour modifier la simulation
                            for (SimulationPlan plan : simulationPlan) {
                                result = result && plan.beforeSimulation(planContext, sim);
                                if (!result) {
                                    nextJob = null;
                                    break;
                                }
                            }
                            doNext = result;
                            if (result) {
                                File zip = sim.createZip();
                                SimulationControl childControl = new SimulationControl(simId);
                                SimulationParameter childParam = param.copy();
                                SimulationItem item = new SimulationItem(childControl, childParam);
                                item.setSimulationZip(zip);
                                nextJob = new SimulationJob(simulationService, job, item, job.getPriority());
                                nextJob.setLauncher(job.getLauncher());
                                // FIXME on retire la post action pour les plan dépendants
                                // sera appelé directement par le job de preparations
                                if (param.isIndependentPlan()) {
                                    nextJob.addPostAction(this); // pour l'appel des after des plans
                                }
                            }
                            // close context for plan generator
                            sim.closeStorage();
                            sim.closeMemStorage();

                            // quoi qu'il arrive on supprime le repertoire temporaire
                            if (!FileUtils.deleteQuietly(tmpDirectory)) {
                                log.warn(t("isisfish.error.remove.directory", tmpDirectory));
                            }
                        }
                        // increment number for next simulation job
                        planContext.incNumber();
                    }
                }
            }
            return result;
        } catch (Exception eee) {
            throw new IsisFishRuntimeException(I18n.t("isisfish.error.evalute.plan.script"), eee);
        }
    }

    public SimulationJob next() {
        hasNext(); // pour etre sur qu'il a ete appele au moins une fois
        SimulationJob result;
        // next est appelee par un autre thread concurrent
        // via la methode finished(SimulationJob, SimulationStorage)
        synchronized (this) {
            result = nextJob;
            nextJob = null;
        }
        return result;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public void finished(SimulationJob job, SimulationStorage sim) {
        // doNext = true;
        // appel de tous les plans pour modifier la simulation
        // EC20090716 : use param.getSimulationPlans() instances,
        // not sim.getParameters().getSimulationPlans() not sames !!!
        for (SimulationPlan plan : simulationPlan) {
            try {
                SimulationLoggerUtil.setPlanLoggingContext(id);
                boolean result = plan.afterSimulation(planContext, sim);
                doNext = doNext && result;
            } catch (Exception eee) {
                log.error(t("Stop simulation plan, because can't call afterSimulation correctly on plan %s", plan.getClass().getName()), eee);
                doNext = false;
            } finally {
                SimulationLoggerUtil.clearPlanLoggingContext();
            }
        }
        // une sim vient de se finir, on incremente le compteur
        done++;
        if (!hasNext() && (done + 1 == planContext.getNumber())) {
            // on enleve le master plan des simulations en cours, vu que
            // toutes les simu sont terminees
            simulationService.fireStopEvent(this.job);
        }
    }

    @Override
    public void exception(SimulationJob job, Throwable eee) {
        // il y a une simulation d'echoue, on ne fait pas les suivantes
        // cela n'impacte pas les plan independant puisque toutes les
        // simulation on deja ete generee
        doNext = false;
        simulationService.fireStopEvent(this.job);
    }

}
