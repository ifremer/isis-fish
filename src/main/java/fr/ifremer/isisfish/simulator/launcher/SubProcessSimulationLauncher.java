/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2020 Ifremer, Code Lutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator.launcher;

import static org.nuiton.i18n.I18n.t;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLClassLoader;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import fr.ifremer.isisfish.util.IsisFileUtil;
import fr.ifremer.isisfish.util.RUtil;
import fr.ifremer.isisfish.util.exec.ExecMonitor;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.simulator.SimulationControl;
import fr.ifremer.isisfish.util.CompileHelper;

/**
 * Lanceur de simulation dans un sous processus.
 * 
 * Usefull article about sub process management :
 * http://www.javaworld.com/javaworld/jw-12-2000/jw-1229-traps.html?page=4
 * 
 * @see ProcessBuilder
 * @see Process
 * 
 * @author poussin
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class SubProcessSimulationLauncher implements SimulatorLauncher {

    /** Class logger (protected for inner classes) */
    protected static Log log = LogFactory.getLog(SubProcessSimulationLauncher.class);

    @Override
    public String toString() {
        return t("isisfish.simulator.launcher.subprocess");
    }

    @Override
    public void simulate(SimulationService simulationService, SimulationItem simulationItem) throws RemoteException {

        // get simulation information for this launcher
        SimulationControl control = simulationItem.getControl();
        File simulationZip = simulationItem.getSimulationZip();
        String simulationPrescript = simulationItem.getGeneratedPrescriptContent();
            
        String simulationId = control.getId();
        SimulationStorage simulation = null;
        try {
            simulation = subProcessSimulate(control, simulationZip, simulationPrescript);
        } catch (Exception eee) {
            if (log.isErrorEnabled()) {
                log.error(t("Can't do simulation %s", simulationId), eee);
            }
            // FIXME simulation is always null if exception
            if (simulation != null) {
                simulation.getInformation().setException(eee);
            }
        }
    }

    @Override
    public int maxSimulationThread() {

        int result = IsisFish.config.getSimulatorSubMaxProcess();

        // s'il n'y a pas de configuration utilisateur particulière
        // on utilise le nombre de processeur physique
        if (result <= 0) {
            result = Runtime.getRuntime().availableProcessors();
        }

        return result;
    }

    @Override
    public int getCheckProgressionInterval() {

        // par defaut, pour les subprocess 5 secondes
        // FIXME rendre configurable

        int interval = 5;
        return interval;
    }

    /**
     * Display both message on UI (listeners and log).
     * 
     * @param control
     * @param message
     */
    protected void message(SimulationControl control, String message) {
        log.info(message);
        if (control != null) {
            control.setText(message);
        }
    }

    public SimulationStorage subProcessSimulate(SimulationControl control, File simulationZip, String simulationPrescript) throws Exception {

        message(control, t("isisfish.message.simulation.prepare"));

        String simulationId = control.getId();
        // on ferme le SimulationStorage pour ne pas interferer avec le process
        //simulation.closeStorage();

        // write prescript in a temporary file
        File tempPrescriptFile = null;
        if (!StringUtils.isEmpty(simulationPrescript)) {
            tempPrescriptFile = IsisFileUtil.createTempFile("isis", ".prescript");
            tempPrescriptFile.deleteOnExit();
            FileUtils.writeStringToFile(tempPrescriptFile, simulationPrescript, "utf-8");
        }
        
        String java = System.getProperty("java.home") + File.separator + "bin" + File.separator + "java";

        // TODO make a method to get runtime classpath
        String classpath = CompileHelper.getClassPathAsString(Collections.emptyList());
        
        if (classpath == null) {
            // could not find the jvm property (jnlp context for example)
            // rebuild the classpath as in {@link AspectClassLoader}
            ClassLoader loader = IsisFish.class.getClassLoader();
            if (loader instanceof URLClassLoader) {
                URLClassLoader urlLoader = (URLClassLoader) loader;
                classpath = Arrays.stream(urlLoader.getURLs()).map(URL::getPath).collect(Collectors.joining(File.pathSeparator));
            }
        }
        
        if (log.isDebugEnabled()) {
            log.debug("classpath to use : " + classpath);
        }

        // common args
        List<String> command = new ArrayList<>();
        command.add(java);
        command.add("-Xmx" + IsisFish.config.getSimulatorSubMaxMemory());

        // jri args
        String libraryPath = System.getProperty("java.library.path");
        if (StringUtils.isNotBlank(libraryPath)) {
            // il semble y avoir un problème de path sous windows,
            // il faut mettre le chemin complet sinon les lib
            // ne sont pas trouvées
            libraryPath = System.getProperty("user.dir") + File.pathSeparator + libraryPath;
            command.add("-Djava.library.path=" + libraryPath + "");
        }
        String rType = System.getProperty(RUtil.R_TYPE_PROPERTY);
        if (StringUtils.isNotBlank(rType)) {
            command.add("-D" + RUtil.R_TYPE_PROPERTY + "=" + rType);
        }

        // common args
        command.add("-classpath");
        command.add(classpath);
        command.add(IsisFish.class.getName());
        command.add("--option");
        command.add("launch.ui");
        command.add("false");

        if (tempPrescriptFile != null) {
            command.add("--simulateWithSimulationAndScript");
            command.add(simulationId);
            command.add(simulationZip.getAbsolutePath());
            command.add(tempPrescriptFile.getAbsolutePath());
        }
        else {
            command.add("--simulateWithSimulation");
            command.add(simulationId);
            command.add(simulationZip.getAbsolutePath());
        }

        // prepare le process
        ProcessBuilder processBuilder = new ProcessBuilder(command);
        processBuilder.redirectErrorStream(true);

        // demarrage du process
        Process process = processBuilder.start();
        ExecMonitor.registerProcess(process);
        if (log.isInfoEnabled()) {
            log.info(t("SubProcess start: %s %s", process, processBuilder.command()));
        }

        // prepare de thread de surveillance du process si control n'est pas null
        Thread monitor = new SimulationCheckpointExternalProcessThread(control, simulationId, process);
        monitor.start();

        // on attend que la simulation soit fini
        int status = process.waitFor();

        if (log.isInfoEnabled()) {
            log.info("SubProcess finished (status = " + status + ")");
        }

        //finished = true;
        SimulationStorage result = SimulationStorage.getSimulation(simulationId);
        return result;
    }

    /**
     * This thread is responsible to synchronized SimulationControl used locally with
     * remote simulation control for remote simulation.
     *
     * This thread dead when {@link SimulationControl#isRunning()} is false
     * 
     * @author poussin
     */
    protected class SimulationCheckpointExternalProcessThread extends Thread { // SimulationCheckpointThread

        protected SimulationControl control = null;
        protected String simulationId = null;
        protected Process process = null;
        // on l'appel plutot out que in, car c le output du process lance
        protected InputStream out = null;

        public SimulationCheckpointExternalProcessThread(SimulationControl control, String simulationId, Process process) {
            if (log.isInfoEnabled()) {
                log.info("Lancement du thread de surveillance des simulations externes");
            }
            this.control = control;
            this.simulationId = simulationId;
            this.process = process;
            this.out = process.getInputStream();
        }

        @Override
        public void run() {
            InputStreamReader osr = new InputStreamReader(out);
            BufferedReader br = new BufferedReader(osr);

            //int sleepTime = 2000;

            // use tip available at
            // https://www.javaworld.com/article/2071275/when-runtime-exec---won-t.html
            // for reading subprocess output
            String line;
            try {
                while ((line = br.readLine()) != null) {
                    // add reading line to logging output
                    if (log.isInfoEnabled()) {
                        log.info(SubProcessSimulationLauncher.this.toString()
                                + ">" + line);
                    }

                    //Thread.sleep(sleepTime);

                    // on ne lit pas le stop, car le stop ne peut-etre appeler
                    // que par l'utilisateur qui est de ce cote de la machine
                    //SimulationStorage.readControl(simulationId, control, "stop");

                    if (control.isStopSimulationRequest()) {
                        // FIXME, un destroy du process est peut-etre un peu violent ?
                        process.destroy();
                        // passe artificiellement le control a fini
                        control.stopSimulation();
                    }

                    if (!control.isRunning()) {
                        return;
                    }
                }
            } catch (IOException e) {
                if (log.isErrorEnabled()) {
                    log.error("Can't read subprocess output", e);
                }
            }
        }
    }

    @Override
    public SimulationStorage getSimulationStorage(SimulationService simulationService, SimulationControl control) throws RemoteException {

        String simulationId = control.getId();

        // In sub process, simulation is locally stored
        SimulationStorage result = SimulationStorage.getSimulation(simulationId);
        return result;

    }

    @Override
    public void updateControl(SimulationService simulationService, SimulationControl control) throws RemoteException {

        String simulationId = control.getId();
        // on ne lit pas le stop, car le stop ne peut-etre appeler
        // que par l'utilisateur qui est de ce cote de la machine
        SimulationStorage.readControl(simulationId, control, "stop");

    }

    /**
     * {@inheritDoc}
     * 
     * Do nothing (no restriction on subprocess launcher).
     */
    @Override
    public void simulationStopRequest(SimulationJob job) {
        // TODO kill -9 sub process
    }
}
