/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2024 Ifremer, Code Lutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator.launcher;

import static org.nuiton.i18n.I18n.t;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import fr.ifremer.isisfish.datastore.FormuleStorage;
import fr.ifremer.isisfish.logging.SimulationLoggerUtil;
import fr.ifremer.isisfish.util.IsisFileUtil;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.MutableInt;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.FileUtil;
import org.nuiton.util.ListenerSet;
import org.nuiton.util.ObjectUtil;
import org.nuiton.util.ZipUtil;

import fr.ifremer.isisfish.config.IsisConfig;
import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.IsisFishException;
import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.datastore.ExportStorage;
import fr.ifremer.isisfish.datastore.ObjectiveStorage;
import fr.ifremer.isisfish.datastore.OptimizationStorage;
import fr.ifremer.isisfish.datastore.RegionStorage;
import fr.ifremer.isisfish.datastore.ResultInfoStorage;
import fr.ifremer.isisfish.datastore.RuleStorage;
import fr.ifremer.isisfish.datastore.ScriptStorage;
import fr.ifremer.isisfish.datastore.SensitivityAnalysisStorage;
import fr.ifremer.isisfish.datastore.SensitivityExportStorage;
import fr.ifremer.isisfish.datastore.SimulationPlanStorage;
import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.datastore.SimulatorStorage;
import fr.ifremer.isisfish.export.ExportInfo;
import fr.ifremer.isisfish.export.SensitivityExport;
import fr.ifremer.isisfish.mexico.MexicoHelper;
import fr.ifremer.isisfish.result.ResultInfoHelper;
import fr.ifremer.isisfish.rule.Rule;
import fr.ifremer.isisfish.simulator.Objective;
import fr.ifremer.isisfish.simulator.Optimization;
import fr.ifremer.isisfish.simulator.SimulationControl;
import fr.ifremer.isisfish.simulator.SimulationException;
import fr.ifremer.isisfish.simulator.SimulationParameter;
import fr.ifremer.isisfish.simulator.SimulationPlan;
import fr.ifremer.isisfish.simulator.sensitivity.DesignPlan;
import fr.ifremer.isisfish.simulator.sensitivity.Domain;
import fr.ifremer.isisfish.simulator.sensitivity.Factor;
import fr.ifremer.isisfish.simulator.sensitivity.FactorHelper;
import fr.ifremer.isisfish.simulator.sensitivity.Scenario;
import fr.ifremer.isisfish.simulator.sensitivity.SensitivityAnalysis;
import fr.ifremer.isisfish.simulator.sensitivity.SensitivityScenarios;
import fr.ifremer.isisfish.simulator.sensitivity.SensitivityUtils;
import fr.ifremer.isisfish.simulator.sensitivity.domain.RuleDiscreteDomain;
import fr.ifremer.isisfish.util.CompileHelper;
import fr.ifremer.isisfish.util.DependencyUtil;

/**
 * Cette classe est responsable de conservation de toutes les simulations faites
 * ou a faire. Pour ajouter une nouvelle simulation on appelle une des méthodes
 * {@code submit}.
 * <p>
 * Cette classe sert aussi de modele pour le moniteur de queue
 * <p>
 * Il existe une instance unique pour toute l'application
 * <p>
 * Lors de l'instanciation de la classe, l'ensemble des
 * {@link SimulatorLauncher} disponible est recherche dans la configuration
 * et un executor est cree pour chaque.
 * <p>
 * Si un {@link SimulatorLauncher} genere trop d'erreur (RemoteException)
 * Il est alors suspendu pour ne plus etre utilise pour les simulations.
 * 
 * @author poussin
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class SimulationService {

    public static final String SIMULATION_LAUNCHER = "simulation.launcher";
    /** nombre maximal de simulation autoriser pour un plan */
    // FIXME a rendre configurable MAX_PLAN_SIMULATION
    public static final int MAX_PLAN_SIMULATION = 20000;

    /** to use log facility, just put in your code: log.info(\"...\"); */
    private static Log log = LogFactory.getLog(SimulationService.class);

    protected static SimulationService instance = new SimulationService();

    /**
     * Retourne l'instance du {@link SimulationService} a utiliser
     * @return l'instance a utiliser
     */
    public static SimulationService getService() {
        return instance;
    }

    protected PropertyChangeSupport propertyListeners = new PropertyChangeSupport(
            this);
    protected ListenerSet<SimulationServiceListener> listeners = new ListenerSet<>();

    // FIXME pouvoir configurer ceci en fichier de config
    // FIXME ainsi que la mise a false, lors de la simulation de la derniere
    // (sans doute ajoute un boolean, simulationListAsQueue = true|false)
    protected boolean autoLaunch = true;

    /** L'executor utilise pour creer toutes les sous simulations des plans
     * independants */
    protected ExecutorService subSimulationComputationExecutor = Executors
            .newSingleThreadExecutor();
    /** Tous les types de {@link SimulatorLauncher} disponibles, et leur
     * executors associe */
    protected Map<SimulatorLauncher, SimulationExecutor> executors = new LinkedHashMap<>();
    /** Le nombre d'erreur pour les SimulatorLauncher */
    protected Map<SimulatorLauncher, MutableInt> launcherError = new HashMap<>();
    /** La queue contenant toutes les simulations a faire */
    protected SimulationQueue queue = new SimulationQueue();

    /** Contient les identifiants des simulations presentes dans {@link #jobs}*/
    protected Set<String> idJobs = new HashSet<>();
    /** La liste des jobs existant (queue + job demarre) */
    protected Set<SimulationJob> jobs = new TreeSet<>();

    /** La liste des jobs termines */
    protected Set<SimulationJob> jobDones = new TreeSet<>();

    /**
     * Cree une instance et initialise les executors ainsi que le launcherError
     */
    protected SimulationService() {
        // on cree un executor par type SimulatorLauncher
        Properties prop = IsisFish.config
                .getOptionStartsWith(SIMULATION_LAUNCHER);

        // sort simulation names by... names
        // this solve "local" "sub", "remote" order
        // default "local" is in first in UI.
        List<String> simulationKeys = new ArrayList<>(prop.stringPropertyNames());
        Collections.sort(simulationKeys);

        for (String key : simulationKeys) {
            String value = prop.getProperty(key);
            try {
                SimulatorLauncher sl = (SimulatorLauncher) ObjectUtil
                        .newInstance(value);
                addSimulationLauncher(sl);
            } catch (Exception eee) {
                log.warn(t("Can't instantiate %s", value), eee);
            }
        }
    }

    /**
     * Permet d'ajouter un nouveau SimulatorLauncher. Cela cree automatiquement
     * un executor pour ce SimulatorLauncher. S'il y avait deja un
     * SimulatorLauncher de ce type un nouveau est ajoute.
     * @param sl le SimulatorLauncher a ajouter
     */
    public void addSimulationLauncher(SimulatorLauncher sl) {
        SimulationQueue executorQueue = new SimulationQueue(queue);
        SimulationExecutor se = new SimulationExecutor(this, sl, executorQueue);
        executors.put(sl, se);
        launcherError.put(sl, new MutableInt(0));
    }

    /**
     * Retour les lanceurs ayant un executor.
     * 
     * @return les SimulatorLauncher
     */
    public List<SimulatorLauncher> getSimulationLaunchers() {
        List<SimulatorLauncher> result = new ArrayList<>(executors.keySet());
        return result;
    }

    public Collection<SimulationExecutor> getSimulationExecutors() {
        return executors.values();
    }

    public synchronized Set<SimulationJob> getJobs() {
        return jobs;
    }

    public synchronized Set<SimulationJob> getJobDones() {
        return jobDones;
    }

    /**
     * Retourne la liste de tous les {@link SimulatorLauncher} disponible.
     * 
     * @return {@link SimulatorLauncher}s
     */
    protected Set<SimulatorLauncher> getSimulatorLaunchers() {
        return executors.keySet();
    }

    public void addSimulationServiceListener(SimulationServiceListener l) {
        listeners.add(l);
    }

    public void removeSimulationServiceListener(SimulationServiceListener l) {
        listeners.remove(l);
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyListeners.addPropertyChangeListener(listener);
    }

    public void addPropertyChangeListener(String propertyName,
            PropertyChangeListener listener) {
        propertyListeners.addPropertyChangeListener(propertyName, listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyListeners.removePropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(String propertyName,
            PropertyChangeListener listener) {
        propertyListeners.removePropertyChangeListener(propertyName, listener);
    }

    /**
     * Ajoute le job au job en cours previent s'il n'etait pas deja present
     * les listeners
     * {@link SimulationServiceListener#simulationStart(SimulationService, SimulationJob)}.
     * @param job
     */
    protected synchronized void fireStartEvent(SimulationJob job) {
        // on ajoute au cas on il n'y serait pas
        if (jobs.add(job)) {
            idJobs.add(job.getItem().getControl().getId());
            for (SimulationServiceListener l : listeners) {
                l.simulationStart(this, job);
            }
        }
    }

    /**
     * Supprime le job de la liste des jobs en cours et l'ajoute le job si
     * besoin a la liste des jobs faits (s'il y a une erreur ou qu'il ne sagit
     * pas d'une simulation lancer par un plan de simulation).
     * Les listener sont prevenu par la méthode
     * {@link SimulationServiceListener#simulationStop(SimulationService, SimulationJob)}.
     * @param job
     */
    protected synchronized void fireStopEvent(SimulationJob job) {
        jobs.remove(job);
        idJobs.remove(job.getItem().getControl().getId());

        // on ajout au jobDones que les simulations avec erreur ou les
        // simulation reellement demande par l'utilisateur (pas de simulation
        // de plan
        jobDones.add(job);
        for (SimulationServiceListener l : listeners) {
            l.simulationStop(this, job);
        }
    }

    /**
     * Nettoie la liste des jobs faits
     */
    public void clearJobDone() {
        jobDones.clear();
        for (SimulationServiceListener l : listeners) {
            l.clearJobDone(this);
        }
    }

    /**
     * @return Returns the autoLaunch.
     */
    public boolean isAutoLaunch() {
        return this.autoLaunch;
    }

    /**
     * @param autoLaunch The autoLaunch to set.
     */
    public void setAutoLaunch(boolean autoLaunch) {
        synchronized (this) {
            boolean oldValue = this.autoLaunch;
            this.autoLaunch = autoLaunch;
            if (this.autoLaunch) {
                this.notifyAll();
            }
            propertyListeners.firePropertyChange("autoLaunch", oldValue,
                    autoLaunch);
        }
    }

    /**
     * Permet de mettre en attente les threads de simulation si l'utilisateur
     * a suspendu la queue
     *
     * @param job le job pour lequel on vu savoir s'il faut l'executer ou non
     */
    protected void waitAutoLaunch(SimulationJob job) {
        synchronized (this) {
            while (!isAutoLaunch()) {
                try {
                    log.info("autoLaunch is false waiting queue start");
                    job.getItem().getControl().setText(t("Pause"));
                    this.wait();
                } catch (InterruptedException eee) {
                    log.warn("Error during wait autoLaunch flag", eee);
                }
            }
        }
    }

    /**
     * Permet d'ajouter une nouvelle simulation a la queue. Si la simulation est
     * simple ou avec un plan d'experience dependant, un nouveau job est cree
     * et directement ajout a la queue de jobs. S'il la simulation est un plan
     * ou les simulations sont independantes, toutes les simulations sont
     * genere et toute ajoute a la queue de simulation. Mais dans ce cas seul
     * la simulation de depart est ajoute au jobs en cours, les simulations
     * generees seront ajoutees lors de leur reel execution
     * 
     * @param id l'identifiant de la simulation
     * @param param les parametres de la simulation
     * @param launcher le launcher a utiliser pour lancer la simulation
     * @param priority la priorite de la simulation
     */
    public void submit(String id, SimulationParameter param,
            SimulatorLauncher launcher, int priority) {

        // make deep copy, ui, still work with
        // copy need to be deep for simulation plan to reload
        SimulationParameter localParameters = param.deepCopy();
        
        // for migration purpose, we need to override IsisFishVersion here
        localParameters.setIsisFishVersion(IsisConfig.getVersion());

        // on l'ajoute tout de suite a la liste des simulations demandee
        SimulationControl control = new SimulationControl(id);
        SimulationItem item = new SimulationItem(control, localParameters);
        SimulationJob job = new SimulationJob(this, item, priority);
        job.setLauncher(launcher);
        fireStartEvent(job);

        // Attention, dans le cas d'un plan de simulation, le new SimulationPlanPrepareJob
        // doit etre fait AVANT de faire le prepareSimulationZipFile
        // car, il est possible que l'init des plans ajoute des regles !!!
        SimulationPlanPrepareJob task = null;
        if (localParameters.getUseSimulationPlan()) {
            try {
                SimulationLoggerUtil.setPlanLoggingContext(id);
                task = new SimulationPlanPrepareJob(this, job);
            } finally {
                SimulationLoggerUtil.clearPlanLoggingContext();
            }
        }

        // on construit le zip de la simulation
        File zip = prepareSimulationZipFile(localParameters, null, control, null, null);
        item.setSimulationZip(zip);

        if (localParameters.getUseSimulationPlan() && localParameters.isIndependentPlan()) {
            // c un plan de simulation independant, on construit toute les sous simu
            subSimulationComputationExecutor.execute(task);
        } else {
            // l'item est fini d'etre initialise, on peut l'ajouter a la queue
            // sauf si c'etait un plan independant ou se seront les sous simu
            // qui seront dans la queue
            submit(job);
        }

    }

    /**
     * Permet d'ajouter une simulation avec des factors a prendre en compte.
     * 
     * @param id l'identifiant de la simulation
     * @param param les parametres de la simulation
     * @param launcher le launcher a utiliser pour lancer la simulation
     * @param priority la priorite de la simulation
     * @param sensitivityAnalysis l'implementation du calculateur de sensibilite
     * @param designPlan le design plan qui contient les facteurs
     */
    public void submit(String id, SimulationParameter param,
            SimulatorLauncher launcher, int priority,
            SensitivityAnalysis sensitivityAnalysis, DesignPlan designPlan) {

        // make deep copy, ui, still work with
        // copy need to be deep for simulation plan to reload
        SimulationParameter localParameters = param.deepCopy();

        // for migration purpose, we need to override IsisFishVersion here
        localParameters.setIsisFishVersion(IsisConfig.getVersion());

        try {

            // build master sensitivity export directory
            File masterExportDirectory = new File(SimulationStorage.getSensitivityResultsDirectory(), id);
            if (!masterExportDirectory.isDirectory()) {
                masterExportDirectory.mkdirs();
            }

            // export designplan as XML
            // get as XML (before compute)
            // compute do bad modification on design plan
            String xmlDesignPlan = MexicoHelper.getDesignPlanAsXML(designPlan);

            // get a TopiaContext on region database, for discret domain
            // to get original database value (Nominal value)
            RegionStorage regionStorage = RegionStorage.getRegion(param.getRegionName());
            TopiaContext context = null;
            try {
                context = regionStorage.getStorage().beginTransaction();
                fillDesignPlanNominalValue(context, designPlan);
            } finally {
                if (context != null) {
                    context.closeContext();
                }
            }

            // Sensitivity scenario returned contains same factor references
            // as input design plan. Factor group are still present
            SensitivityScenarios sensitivityScenarios;
            try {
                SimulationLoggerUtil.setPlanLoggingContext(id);
                sensitivityScenarios = sensitivityAnalysis.compute(designPlan, masterExportDirectory);
            } finally {
                SimulationLoggerUtil.clearPlanLoggingContext();
            }

            // ajout des parametres de simulation
            localParameters.setSensitivityAnalysis(sensitivityAnalysis);
            localParameters.setNumberOfSensitivitySimulation(sensitivityScenarios.getScenarios().size());

            // on l'ajoute tout de suite a la liste des simulations demandee
            // init with default "id" must been changed for designPlan
            // simulation
            SimulationControl controlJustForZip = new SimulationControl(id);

            // on construit le zip de la simulation
            // in zip, there will be param "without" prescript
            File zip = prepareSimulationZipFile(localParameters, null, controlJustForZip, xmlDesignPlan, sensitivityScenarios);

            // for each simulation, we neeed to launch a specific simulation
            // new simulation ids will be id + "_" + x
            // where x is a int starting at 0
            int simulationIndex = 0;
            for (Scenario scenario : sensitivityScenarios.getScenarios()) {

                // modify modification parameter
                // subParam only convenient for add pre script
                // for launcher
                SimulationParameter subParam = localParameters.copy();
                String generatedScriptContent = FactorHelper.generatePreScript(scenario);
                // usefull next two line ?
                subParam.setGeneratedPreScript(generatedScriptContent);

                // build new simulation id
                String subsimulationId = id + "_" + simulationIndex;
                SimulationControl realSimulationControl = new SimulationControl(subsimulationId);

                // new item, with zip
                SimulationItem item = new SimulationItem(realSimulationControl, subParam);
                item.setSimulationZip(zip);
                item.setStandaloneSimulation(false); // AS = always false
                item.setStandaloneSimulationZip(false); // AS = same zip
                item.setSimulationNumber(simulationIndex);
                item.setLastSimulation(simulationIndex == sensitivityScenarios.getScenarios().size() - 1);

                SimulationJob job = new SimulationJob(this, item, priority);
                job.setLauncher(launcher);
                fireStartEvent(job);

                if (subParam.getUseSimulationPlan() && subParam.isIndependentPlan()) {
                    // FIXME on est dans une AS là, pas sur que l'on puisse tomber dans ce cas là
                    // c un plan de simulation independant, on construit toute les sous simu
                    Runnable task = new SimulationPlanPrepareJob(this, job);
                    subSimulationComputationExecutor.execute(task);
                } else {
                    // l'item est fini d'etre initialise, on peut l'ajouter a la queue
                    // sauf si c'etait un plan independant ou se seront les sous simu
                    // qui seront dans la queue
                    submit(job);
                }

                simulationIndex++;
            }

        } catch (Exception e) {
            throw new IsisFishRuntimeException("Can't get scenarios from calculator", e);
        }
    }

    /**
     * Explore design plan factor tree, and set factor nominal value
     * for each factor.
     * 
     * Also fix factor name for R (without special chars in name)
     * 
     * @param context opened database context
     * @param designPlan design plan
     */
    protected void fillDesignPlanNominalValue(TopiaContext context, DesignPlan designPlan) {
        // take care only on first level of factor tree
        for (Factor factor : designPlan.getFactors()) {
            // get nominal value
            String path = factor.getPath();
            if (StringUtils.contains(path, "#")) {
                String topiaId = path.substring(0, path.lastIndexOf("#"));
                String propertyName=path.substring(path.lastIndexOf("#") + 1);
                try {
                    TopiaEntity entity = context.findByTopiaId(topiaId);
                    Object result = BeanUtils.getProperty(entity, propertyName);
                    factor.setNominalValue(result);
                } catch (Exception ex) {
                    if (log.isErrorEnabled()) {
                        log.error("An error occurred while trying to get nominal value", ex);
                    }
                }
            } else {
                factor.setNominalValue("NaN");
            }
            
            // also fix factor name for sensitivity calculator
            String name = SensitivityUtils.espaceFactorName(factor.getName());
            factor.setName(name);
        }
    }

    protected void submit(SimulationJob job) {
        SimulatorLauncher launcher = job.getLauncher();
        // on ajoute a la queue qui utilise le launcher defini dans le job
        if (launcher != null) {
            for (SimulatorLauncher l : executors.keySet()) {
                if (launcher == l) {
                    SimulationExecutor executor = executors.get(l);
                    executor.execute(job);
                    return;
                }
            }
        }
        // dernier recours on ajoute a la queue sans launcher
        log.info(t("Add to default queue"));
        queue.add(job);
    }

    /**
     * Permet de resoumettre un job qui a ete pris par un thread mais qu'il 
     * ne peut pas traiter. Cela arrive lorsque l'executor est en pause, ou
     * que le launcher de l'executor ne fonctionne plus (il se met en pause
     * tout seul)
     * 
     * @param job l'item a resoumettre
     */
    protected void resubmit(SimulationJob job) {
        submit(job);
    }

    /**
     * Permet de soumettre a la queue un job provenant d'un plan.
     * Ce plan ne doit pas apparaitre dans la console de queue.
     * 
     * @param job
     */
    protected void submitSubJob(SimulationJob job) {
        submit(job);
    }

    /**
     * Resoumet une simulation qui a deja été démarrée, mais
     * on ne faisant que du control de monitoring.
     * 
     * @param job job to submit
     */
    public void submitForCheckOnly(SimulationJob job) {
        job.setOnlyCheckControl(true);
        fireStartEvent(job);
        submit(job);
    }

    /**
     * Restart a job.
     * 
     * This method fire "start" event.
     * 
     * @param job job to retstart
     */
    public void restart(SimulationJob job) {
        // lorsqu'un job est resoumit, il redevient forcement standalone
        job.getItem().setStandaloneSimulation(true);
        // case were we restart a reloaded job
        job.setOnlyCheckControl(false);

        job.getItem().getControl().reset();
        fireStartEvent(job);
        submit(job);
    }

    /**
     * Supprime un job de la queue (annulation d'une simulation). Appele
     * depuis {@link SimulationJob#stop}
     * 
     * @param job le job a annuler
     * @return vrai si la simulation a pu etre annulee avant sont lancement 
     * (encore presente dans la queue), faux si la simulation a deja ete
     * prise par un thread de simulation ou quelle est terminee
     */
    protected boolean cancel(SimulationJob job) {
        boolean result = queue.remove(job);
        return result;
    }

    public boolean exists(String id) {
        boolean result = idJobs.contains(id);
        return result;
    }

    /**
     * This method must be call in thread simulation
     *
     * Report une erreur pour un launcher, on resoumet le job en supprimant
     * le launcher utilise
     * 
     * @param launcher le launcher posant probleme
     * @param job le job qui n'a pas reussi a se faire
     */
    protected void reportError(SimulatorLauncher launcher, SimulationJob job) {
        MutableInt i = launcherError.get(launcher);
        i.setValue(i.intValue() + 1);
        // si on a plus de N error, on stop l'executor associe
        if (i.intValue() >= 50) {
            log.error(t("Launcher %s will be stopped because there are too many error (%s)",
                            launcher, i.intValue()));
            SimulationExecutor e = executors.get(launcher);
            e.pause();
            // reset error to 0
            i.setValue(0);
        }
        // il faut bien penser a supprimer le launcher pour qu'un autre executor
        // puisse y mettre le sien.
        // FIXME: est ce le bon choix si l'utilisateur avait force un launcher particulier, ne faudrait t'il pas prevenir l'utilisateur ?
        // FIXME: disabled since datarmor is not our friend. Retry on datarmor in the limit of 50 errors
        //job.setLauncher(null);
        try {
            // not resubmit immediatly, wait some time (max 10 min and about 4,2 hours in total)
            Thread.sleep(i.intValue() / 5 * 60 * 1000);
        } catch (InterruptedException eee) {
            log.info("Can't sleep before resubmit job, never mind", eee);
        }
        resubmit(job);
    }

    /**
     * Prepare les fichiers qui seront utilsé à la simulation:
     * <ul>
     * <li> scripts</li>
     * <li> rules</li>
     * <li> resultinfos</li>
     * <li> exports</li>
     * <li> simulators</li>
     * <li> optimisations</li>
     * <li> objectives</li>
     * <li> export de la database de la region</li>
     * <li> simultionplan</li>
     * </ul>
     * <p>
     * Le tout est zippé et le zip est retourné, il peut-être directement
     * importé dans le {@link SimulationStorage} (en le renommant comme
     * il faut {@link SimulationStorage#importAndRenameZip(File, String)}.
     * <p>
     * Ce zip est automatiquement supprimé à la fin de l'application.
     *
     * @param param les paramètre de la simulation
     * @param outputZilFile output zip file containing simulation (can be {@code null}, temp file will be created)
     * @param control le controleur (can be {@code null})
     * @param xmlDesignPlan contenu xml des design plan (can be {@code null})
     * @param sensitivityScenarios used to add some extra files (such as rules) (can be {@code null})
     * 
     * @return un zip de simulation pour une simulation pret a être faite
     * 
     * @throws SimulationException pour tout problème rencontré (IO,Topia,Script compilation...)
     */
    public File prepareSimulationZipFile(SimulationParameter param, File outputZilFile, SimulationControl control,
                                         String xmlDesignPlan, SensitivityScenarios sensitivityScenarios)
            throws SimulationException {

        try {
            File tmpDirectory = IsisFileUtil.createTempDirectory("isisfish-simulation-", "-preparation");

            // sauvegarde des parametres
            Properties prop = param.toProperties();
            File f = new File(tmpDirectory, SimulationStorage.PARAMETERS_FILENAME);
            try (FileOutputStream out = new FileOutputStream(f)) {
                prop.store(out, "Parameters");
            }

            // backup pour toutes les simulations, pour eviter que l'utilisateur
            // ne puisse le modifier en meme temps
            if (control != null) {
                control.setText(t("isisfish.message.backup.database.progress"));
            }
            File regionBackup = new File(tmpDirectory, SimulationStorage.DATA_BACKUP_FILENAME);
            RegionStorage region = RegionStorage.getRegion(param.getRegionName());
            TopiaContext tc = region.getStorage().beginTransaction();
            tc.backup(regionBackup, true);
            tc.closeContext();
            if (control != null) {
                control.setText(t("isisfish.message.backup.database.finished"));
            }

            // result are in various scripts
            Set<String> necessaryResults = new HashSet<>();

            // bug recent 2021, les résultats explicitement demandé n'étaient pas copié
            // et sur caparmor, on recherche le fichier .java dans
            // "extractRecursiveResults" > ResultInfoStorage.getResultInfo
            final Set<String> collect = param.getResultEnabled().stream().map(r -> StringUtils.removeEnd(r, ".java")).collect(Collectors.toSet());
            necessaryResults.addAll(collect);

            // extract Rules defined in factors
            List<Rule> rules = param.getRules();
            if (sensitivityScenarios != null) {
                for (Scenario sensitivityScenario : sensitivityScenarios.getScenarios()) {
                    List<Factor> factors = sensitivityScenario.getFactors();
                    for (Factor factor : factors) {
                        Domain domain = factor.getDomain();
                        if (domain instanceof RuleDiscreteDomain) {
                            rules.addAll((List<Rule>)factor.getValue());
                        }
                    }
                }
            }
            for (Rule rule : rules) {
                String name = RuleStorage.getName(rule);
                File ruleFile = new File(RuleStorage.getRuleDirectory(), name + ".java");
                if (!ruleFile.isFile()) {
                    ruleFile = new File(RuleStorage.getCommunityRuleDirectory(), name + ".java");
                }
                FileUtils.copyFile(ruleFile, new File(tmpDirectory, RuleStorage.RULE_PATH
                                + File.separator + name + ".java"));

                // results
                if (rule.getNecessaryResult() != null) {
                    Collections.addAll(necessaryResults, rule.getNecessaryResult());
                }
            }

            // copie des regles reclamées par les plans de simulation
            for (String name : param.getExtraRules()) {
                RuleStorage ruleStorage = RuleStorage.getRule(name);
                Rule rule = ruleStorage.getNewInstance();

                // copy
                File ruleFile = new File(RuleStorage.getRuleDirectory(), name + ".java");
                if (!ruleFile.isFile()) {
                    ruleFile = new File(RuleStorage.getCommunityRuleDirectory(), name + ".java");
                }
                FileUtils.copyFile(ruleFile, new File(tmpDirectory, RuleStorage.RULE_PATH
                                + File.separator + name + ".java"));
                
                // results
                if (rule.getNecessaryResult() != null) {
                    Collections.addAll(necessaryResults, rule.getNecessaryResult());
                }
            }

            // copie de tous les plans a utiliser
            List<SimulationPlan> plans = param.getSimulationPlans();
            for (SimulationPlan plan : plans) {
                String name = SimulationPlanStorage.getName(plan);
                File planFile = new File(SimulationPlanStorage.getSimulationPlanDirectory(), name + ".java");
                if (!planFile.isFile()) {
                    planFile = new File(SimulationPlanStorage.getCommunitySimulationPlanDirectory(), name + ".java");
                }
                FileUtils.copyFile(planFile, new File(tmpDirectory, SimulationPlanStorage.SIMULATION_PLAN_PATH
                                + File.separator + name + ".java"));
                
                // results
                if (plan.getNecessaryResult() != null) {
                    Collections.addAll(necessaryResults, plan.getNecessaryResult());
                }
            }

            // copie de tous les exports a utiliser
            for (String name : param.getExportNames()) {
                // new instance
                ExportStorage exportStorage = ExportStorage.getExport(name);
                ExportInfo export = exportStorage.getNewInstance();

                // copy
                name = name.endsWith(".java") ? name : name + ".java";
                File exportFile = new File(ExportStorage.getExportDirectory(), name);
                if (!exportFile.isFile()) {
                    exportFile = new File(ExportStorage.getCommunityExportDirectory(), name);
                }
                FileUtils.copyFile(exportFile,
                        new File(tmpDirectory, ExportStorage.EXPORT_PATH
                                + File.separator + name));
                
                // results
                if (export.getNecessaryResult() != null) {
                    Collections.addAll(necessaryResults, export.getNecessaryResult());
                }
            }
            
            // copie de tous les exports de sensitivity a utiliser
            for (SensitivityExport sensitivity : param.getSensitivityExport()) {
                String name = SensitivityExportStorage.getName(sensitivity);
                name = name.endsWith(".java") ? name : name + ".java";
                File sensitivityFile = new File(SensitivityExportStorage.getSensitivityExportDirectory(), name);
                if (!sensitivityFile.isFile()) {
                    sensitivityFile = new File(SensitivityExportStorage.getCommunitySensitivityExportDirectory(), name);
                }
                FileUtils.copyFile(sensitivityFile,
                        new File(tmpDirectory, SensitivityExportStorage.SENSITIVITY_EXPORT_PATH
                                + File.separator + name));
                
                // results
                if (sensitivity.getNecessaryResult() != null) {
                    Collections.addAll(necessaryResults, sensitivity.getNecessaryResult());
                }
            }

            // copie de la fonction d'objectif
            Objective objective = param.getObjective();
            if (objective != null) {
                String name = ObjectiveStorage.getName(objective);
                File objectiveFile = new File(ObjectiveStorage.getObjectiveDirectory(), name + ".java");
                if (!objectiveFile.isFile()) {
                    objectiveFile = new File(ObjectiveStorage.getCommunityObjectiveDirectory(), name + ".java");
                }
                FileUtils.copyFile(objectiveFile, new File(tmpDirectory, ObjectiveStorage.OBJECTIVE_PATH
                                + File.separator + name + ".java"));
                
                // results
                if (objective.getNecessaryResult() != null) {
                    Collections.addAll(necessaryResults, objective.getNecessaryResult());
                }
            }

            // copie du script d'optimisation
            Optimization optimization = param.getOptimization();
            if (optimization != null) {
                String name = OptimizationStorage.getName(optimization);
                File optimizationFile = new File(OptimizationStorage.getOptimizationDirectory(), name + ".java");
                if (!optimizationFile.isFile()) {
                    optimizationFile = new File(OptimizationStorage.getCommunityOptimizationDirectory(), name + ".java");
                }
                FileUtils.copyFile(optimizationFile, new File(tmpDirectory, OptimizationStorage.OPTIMIZATION_PATH
                                + File.separator + name + ".java"));
                
                // results
                if (optimization.getNecessaryResult() != null) {
                    Collections.addAll(necessaryResults, optimization.getNecessaryResult());
                }
            }

            // copie des export d'optimisation
            for (ExportInfo export : param.getOptimizationExportsObservations().keySet()) {
                String name = ExportStorage.getName(export);
                if (!param.getExportNames().contains(name)) {
                    // duplicated code
                    name = name.endsWith(".java") ? name : name + ".java";
                    File exportFile = new File(ExportStorage.getExportDirectory(), name);
                    if (!exportFile.isFile()) {
                        exportFile = new File(ExportStorage.getCommunityExportDirectory(), name);
                    }
                    FileUtils.copyFile(exportFile,
                            new File(tmpDirectory, ExportStorage.EXPORT_PATH
                                    + File.separator + name));
                    
                    // results
                    if (export.getNecessaryResult() != null) {
                        Collections.addAll(necessaryResults, export.getNecessaryResult());
                    }
                }
            }

            // copie de tous les simulateurs a utiliser
            File simulatorFile = new File(SimulatorStorage.getSimulatorDirectory(), param.getSimulatorName());
            if (!simulatorFile.isFile()) {
                simulatorFile = new File(SimulatorStorage.getCommunitySimulatorDirectory(), param.getSimulatorName());
            }
            FileUtils.copyFile(simulatorFile, new File(tmpDirectory,
                    SimulatorStorage.SIMULATOR_PATH + File.separator + param.getSimulatorName()));

            // copie de tous les résultats
            Set<String> allNecessaryResults = ResultInfoHelper.extractAllNecessaryResults(necessaryResults);
            for (String result : allNecessaryResults) {
                String filename = result.endsWith(".java") ? result : result + ".java";
                // copy
                File resultFile = new File(ResultInfoStorage.getResultInfoDirectory(), filename);
                if (!resultFile.isFile()) {
                    resultFile = new File(ResultInfoStorage.getCommunityResultInfoDirectory(), filename);
                }
                FileUtils.copyFile(resultFile,
                        new File(tmpDirectory, ResultInfoStorage.RESULT_INFO_PATH
                                + File.separator + filename));
            }

            // copie du cache de formules
            File src = new File(new File(IsisFish.config.getRegionCacheDirectory(), param.getRegionName()), FormuleStorage.FORMULE_PATH);
            if (src.isDirectory()) {
                FileUtils.copyDirectoryToDirectory(src, tmpDirectory);
            }

            // compile and add dependencies
            // used results will be added here
            compileAllFile(control, tmpDirectory);

            // sauvegarde du design plan en XML
            if (!StringUtils.isEmpty(xmlDesignPlan)) {
                File simulationDesignPlanFile = SimulationStorage.getMexicoDesignPlan(tmpDirectory);
                FileUtils.writeStringToFile(simulationDesignPlanFile, xmlDesignPlan, "utf-8");
            }
            
            // creation du zip
            if (outputZilFile == null) {
                outputZilFile = new File(tmpDirectory.getPath() + ".zip");
                outputZilFile.deleteOnExit();
            }
            ZipUtil.compress(outputZilFile, tmpDirectory, null);

            // poussin 20071015: remove temp directory
            if (!FileUtils.deleteQuietly(tmpDirectory)) {
                log.warn(t("isisfish.error.remove.directory", tmpDirectory));
            }

            return outputZilFile;
        } catch (IOException|IsisFishException|TopiaException eee) {
            throw new SimulationException(
                    t("isisfish.error.prepare.information.simulation"), eee);
        }
    }

    /**
     * Compile les fichiers présent dans le répertoire passé en
     * parametre, ce répertoire est hiérarchisé en: rules, exports, simulators
     * et scripts. Seuls les fichiers des 3 premiers répertoires sont compilés
     * les fichiers du dernier sont compilé par les dépendances qu'on les autres 
     * <p>
     * Il permet donc de compiler facilement tous les fichiers pour une
     * simulation
     *  
     * @param control le controleur (can be {@code null})
     * @param directory le répertoire où compiler
     * @throws IOException 
     */
    protected void compileAllFile(SimulationControl control, File directory) throws IOException {

        if (control != null) {
            control.setText(t("isisfish.simulation.message.scriptscompilation"));
        }
        long currentTime = System.nanoTime();

        // Recherche des fichiers a compiler
        // On ne prend pas les scripts, car ils sont tous copiés mais pas
        // forcément util. Lors de la compilation des autres fichiers, les
        // script servant réellement seront automatiquement compilé

        List<File> fileToCompile = new ArrayList<>(); // *.java
        List<File> fileToInpect = new ArrayList<>(); // *.class
        Set<String> dependencies = new HashSet<>(); // packages

        String[] modules = {
                ExportStorage.EXPORT_PATH,
                RuleStorage.RULE_PATH,
                SensitivityExportStorage.SENSITIVITY_EXPORT_PATH,
                SensitivityAnalysisStorage.SENSITIVITY_ANALYSIS_PATH,
                ScriptStorage.SCRIPT_PATH,
                SimulationPlanStorage.SIMULATION_PLAN_PATH,
                SimulatorStorage.SIMULATOR_PATH
        };
        
        for (String module : modules) {
            File moduleDir = new File(directory, module);
            List<File> tmp = FileUtil.find(moduleDir, ".*\\.java$", true);
            for (File srcFile : tmp) {
                // source
                fileToCompile.add(srcFile);
                // class
                File classFile = new File(srcFile.getAbsolutePath().replaceFirst("\\.java", ".class"));
                fileToInpect.add(classFile);
                // package
                dependencies.add(module + "." + FilenameUtils.getBaseName(srcFile.getName()));
            }
        }

        //
        // Compilation
        //
        CompileHelper.compile(directory, fileToCompile, directory, null);

        //
        // Check dependencies
        //
        Set<String> allDeps = DependencyUtil.extractDependencies(directory, fileToInpect);
        Collection<String> missingDeps = CollectionUtils.subtract(allDeps, dependencies);
        for (String missingDep : missingDeps) {
            String relativePath = missingDep.replace('.', File.separatorChar) + ".java";
            // try official first
            File srcFile = new File(IsisFish.config.getContextDatabaseDirectory(), relativePath);
            if (!srcFile.isFile()) {
                // if official file not exists, look for community file
                srcFile = new File(IsisFish.config.getCommunityDatabaseDirectory(), relativePath);
            }
            File destFile = new File(directory, relativePath);
            FileUtils.copyFile(srcFile, destFile);
        }
        // there is no need to compile this dependancies, they have been compiled by first compilation
        // only add missing java files

        if (control != null) {
            long time = System.nanoTime() - currentTime;
            control.setText(t("isisfish.message.compilation.time",
                    DurationFormatUtils.formatDuration(time / 1000000, "s'.'S")));
        }

    }

}
