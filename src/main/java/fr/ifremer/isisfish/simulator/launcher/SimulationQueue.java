/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2010 Ifremer, Code Lutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator.launcher;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Multi tail PriorityBlockingQueue.
 *
 * @author poussin
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class SimulationQueue extends JDKPriorityBlockingQueue<SimulationJob> {

    /** serialVersionUID. */
    private static final long serialVersionUID = -6130030747211387382L;

    protected SimulationQueue parent = null;
    protected List<SimulationQueue> childs = new LinkedList<>();

    public SimulationQueue() {
    }

    public SimulationQueue(SimulationQueue parent) {
        parent.addChild(this);
    }

    public void addChild(SimulationQueue c) {
        childs.add(c);
        c.parent = this;
    }

    @Override
    public boolean remove(Object o) {
        final ReentrantLock lock = this.lock;
        lock.lock();
        try {
            boolean result = false;
            for (SimulationQueue child : childs) {
                // il faut bien faire le remove sur chaque fils,
                // donc le || result doit etre en dernier
                result = child.remove(o) || result;
            }
            result = q.remove(o) || result;
            return result;
        } finally {
            lock.unlock();
        }
    }

    @Override
    public boolean offer(SimulationJob e) {
        boolean result = super.offer(e);
        for (SimulationQueue child : childs) {
            final ReentrantLock lock = child.lock;
            lock.lock();
            try {
                child.notEmpty.signal();
            } finally {
                lock.unlock();
            }
        }
        return result;
    }

    @Override
    public SimulationJob peek() {
        SimulationJob result = super.peek();
        if (result == null && parent != null) {
            result = parent.peek();
        }
        return result;
    }

    @Override
    public SimulationJob poll() {
        SimulationJob result = super.poll();
        if (result == null && parent != null) {
            result = parent.poll();
        }
        return result;
    }

    @Override
    public SimulationJob take() throws InterruptedException {
        final ReentrantLock lock = this.lock;
        lock.lockInterruptibly();
        SimulationJob result = null;
        try {
            try {
                // on fait bien un poll, sur le pere, car on ne souhaite pas 
                // rester bloquer dessus
                while (q.size() == 0
                        && (parent == null || null == (result = parent.poll()))) {
                    notEmpty.await();
                }
            } catch (InterruptedException ie) {
                notEmpty.signal(); // propagate to non-interrupted thread
                throw ie;
            }
            if (result == null) {
                result = q.poll();
            }
            assert result != null;
            return result;
        } finally {
            lock.unlock();
        }
    }

    @Override
    public SimulationJob poll(long timeout, TimeUnit unit)
            throws InterruptedException {
        long nanos = unit.toNanos(timeout);
        final ReentrantLock lock = this.lock;
        lock.lockInterruptibly();
        try {
            for (;;) {
                SimulationJob result = poll();
                if (result != null) {
                    return result;
                }
                if (nanos <= 0) {
                    return null;
                }
                try {
                    nanos = notEmpty.awaitNanos(nanos);
                } catch (InterruptedException ie) {
                    notEmpty.signal(); // propagate to non-interrupted thread
                    throw ie;
                }
            }
        } finally {
            lock.unlock();
        }
    }

}
