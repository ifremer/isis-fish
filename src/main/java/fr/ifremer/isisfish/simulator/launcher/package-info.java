/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 1999 - 2014 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
/**
 * <h2>Isis fish simulation launchers.</h2>
 * 
 * <h3>To do</h3>
 * <ul>
 * <li> faire une interface graphique de monitoring des Executors (nombre de
 * threads actif, nombre de simulations terminer, nombre de simulation
 * explicitement pour cet executor, afficher l'etat pause/resume/error et
 * permettre de modifier l'etat. Avec le nombre
 * de jobs dans la queue pour tous les Executors. Permettre de soumettre toutes
 * les simulations d'un executor sur un autre executor. (par exemple si un
 * executor est mis en pause a cause des erreurs, cela permet a l'utilisateur
 * d'utiliser un autre executor sans devoir annuler/relancer toutes ces
 * simulations.
 * <li> ajouter dans l'interface de vision des simulations en cours, de
 * modifier le launcher des simulations non lancee.
 * <li> trouver une solution pour que les jobs soumis pour un launcher
 * particulier soit bien executer par l'executor de ce launcher. (penser au
 * changement de priority avec resoumission si ce choix est pris, c-a-d que le
 * job s'appercoit qu'il va etre utilise par le mauvais Executor, il ne fait
 * pas la simu et la resoumet a queue en augmentant la priority pour que le job
 * qui devait etre fait maintenant ne le soit pas dans 10ans).
 * Une autre possibilite d'implantation est que chaque executor est une queue
 * propre, et lorsque celle-ci est vide il vont piocher dans la queue commune.
 * Pour cela, simplement implanter un nouveau type de queue qui permette
 * d'avoir une queue mere. Lors de la soumission au SimulationService soit le
 * job est directement soumis au bon executor soit ajoute a la queue general.
 * (ajouter un bool qui permette lors de report d'erreur de resoumettre un job
 * avec launcher fixe par l'utilisateur sur la queue general ou non.
 * </ul>
 * 
 * <h3>Architecture global</h3>
 * <img src="doc-files/isis-simulation.png" alt="archi">
 * 
 * <h3>Principe general</h3>
 * <p>
 * Les simulations sont soumises au {@link fr.ifremer.isisfish.simulator.launcher.SimulationService} via sa methode
 * submit. Un objet {@link fr.ifremer.isisfish.simulator.launcher.SimulationJob} est alors cree et ajoute a la liste
 * des simulations presentes ({@link fr.ifremer.isisfish.simulator.launcher.SimulationService#getJobs()}). Si la
 * simulation est une simple simulation ou une simulation avec plan de simulation
 * dependant, elle est alors directement ajoutee a la queue de simulation
 * (simulation a faire). Si
 * la simulation utilise un plan de simulation independant, un thread est
 * specialement utilise pour generer toutes les simulations du plan, celles-ci
 * sont alors ajoutee a la queue, mais n'apparaitront dans la liste des
 * simulations qu'au moment ou un thread de simulation executera reellement le
 * job.
 * </p>
 * 
 * <p>
 * Lorsqu'un thread recupere un job dans la queue, il leve un event {@link
 * fr.ifremer.isisfish.simulator.launcher.SimulationServiceListener#simulationStart(SimulationService, SimulationJob)},
 * la simulation est alors ajoutee a la liste des simulations visibles si elle
 * ne l'etait pas encore.
 * </p>
 * 
 * <p>
 * Une fois terminees, les simulations finissent dans la liste des simulations
 * terminees.
 * </p>
 * 
 * <p>
 * Le {@link fr.ifremer.isisfish.simulator.launcher.SimulationService#autoLaunch} permet d'indique si le service est
 * actif ou non. S'il n'est pas actif, il accepte les simulations mais ne les
 * execute pas (elles sont en attente). S'il est actif alors les differents
 * {@link fr.ifremer.isisfish.simulator.launcher.SimulationExecutor}) prenent les jobs de la queue pour faire les
 * simulations.
 * </p>
 * 
 * <h3>SimulationExecutor</h3>
 * <p>
 * Lors de sa creation le {@link fr.ifremer.isisfish.simulator.launcher.SimulationService} a initialise different
 * {@link fr.ifremer.isisfish.simulator.launcher.SimulationExecutor} en fonction de la configuration. Ces {@link
 * fr.ifremer.isisfish.simulator.launcher.SimulationExecutor} sont responsable de l'execution des simulations de la
 * queue. Chaque {@link fr.ifremer.isisfish.simulator.launcher.SimulationExecutor} a un
 * {@link fr.ifremer.isisfish.simulator.launcher.SimulatorLauncher} qu'il utilise
 * si la simulation n'a pas encore de {@code SimulatorLauncher}
 * d'assigne.
 * </p>
 * <p>
 * Un {@link fr.ifremer.isisfish.simulator.launcher.SimulationExecutor} peut etre
 * mis en pause puis relance. Lorsqu'il est en pause, il termine les simulations
 * en cours mais n'en reprend pas de nouvelle. Cela permet d'arrete un
 * {@link fr.ifremer.isisfish.simulator.launcher.SimulationExecutor} particulier
 * sans devoir arreter tout le service de simulation.
 * </p>
 * <p>Si un {@link fr.ifremer.isisfish.simulator.launcher.SimulationExecutor}
 * prend un job ayant deja un {@link fr.ifremer.isisfish.simulator.launcher.SimulatorLauncher}
 * d'assigne, il utilise alors ce launcher plutot que le sien. Ce choix est derangeant
 * lorsque l'on souhaite utilise un nombre de thread limite pour un launcher particulier,
 * mais il est le plus raisonnable car l'autre possibilite est que le job soit
 * resoumis au {@link fr.ifremer.isisfish.simulator.launcher.SimulationService}
 * jusqu'a ce que le bon {@link fr.ifremer.isisfish.simulator.launcher.SimulationExecutor} le
 * prenne pour l'executer. On risque dans ce cas d'arriver a une forte
 * consommation CPU si le seul {@link fr.ifremer.isisfish.simulator.launcher.SimulationExecutor}
 * disponible ne gere pas les jobs en queue.
 * </p>
 * 
 * <h3>SimulationJob</h3>
 * 
 * <p>
 * Le simulation Job encapsule l'appel pour que les implantantations des {@link
 * fr.ifremer.isisfish.simulator.launcher.SimulatorLauncher} soit la plus simple
 * possible. Il gere les simulations avec plan dependant, les exports depandes
 * par l'utilisateur, ainsi que l'effacement des simulations si seul les exports
 * interessait l'utilisateur.
 * </p>
 * 
 * <p>
 * Si le job n'arrive pas a utilise le {@link fr.ifremer.isisfish.simulator.launcher.SimulatorLauncher}
 * il en notifie le {@link fr.ifremer.isisfish.simulator.launcher.SimulationService}
 * qui resoumet le job dans la queue pour qu'un autre {@link fr.ifremer.isisfish.simulator.launcher.SimulationExecutor}
 * prenne ce job. Si trop d'erreurs sont notifiees pour un meme {@link fr.ifremer.isisfish.simulator.launcher.SimulatorLauncher},
 * le {@link fr.ifremer.isisfish.simulator.launcher.SimulationService} prend alors la decision d'arreter l'executor associe.
 * </p>
 * <p>
 * Pour les simulations ou l'utilisateur avait fixe un {@link
 * fr.ifremer.isisfish.simulator.launcher.SimulatorLauncher} particulier en cas
 * de notification d'erreur au {@link fr.ifremer.isisfish.simulator.launcher.SimulationService}
 * ce {@link fr.ifremer.isisfish.simulator.launcher.SimulatorLauncher} n'est plus pris en compte et
 * n'importe quel {@link fr.ifremer.isisfish.simulator.launcher.SimulatorLauncher} peut faire cette simulation.
 * </p>
 */
package fr.ifremer.isisfish.simulator.launcher;

