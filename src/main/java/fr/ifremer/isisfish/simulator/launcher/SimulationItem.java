/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2014 Ifremer, Code Lutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator.launcher;

import java.io.File;

import fr.ifremer.isisfish.simulator.SimulationControl;
import fr.ifremer.isisfish.simulator.SimulationParameter;
import fr.ifremer.isisfish.simulator.SimulationParameterCache;

/**
 * Objet representant une simulation qui doit être faite.
 * 
 * Elle est composée :
 * <ul>
 *  <li>d'un fichier zip (la simulation a simuler)
 *  <li>les parametres de la simulation
 *  <li>d'un pre-script
 *  <li>d'informations supplémentaires utiles à certains launcher :
 *  <ul>
 *    <li>standaloneSimulation : la simulation doit être lancée seule, sinon 
 *    les simulations sont indépendantes et peuvent être lancées groupées
 *    ({@code true} par defaut)
 *    <li>simulationStandaloneZip : si le fichier zip est different pour toute
 *    les simulations ou s'il peut être commun à un ensemble de simulation
 *    <li>simulationNumber : simulation number in non standalone simulation
 *    <li>simulationsCount : total simulation count
 *    <li>lastSimulation : last simulation in a simulation pool
 *  </ul>
 * </ul>
 * 
 * Les valeurs par defaut sont positionnée de telle sorte qu'une simulation
 * est considérée par defaut comme totalement indépendante.
 * 
 * @author poussin
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class SimulationItem {

    /** Le control pour la simulation. */
    protected SimulationControl control;

    /** Les parametres de simulation. */
    protected SimulationParameter parameter;

    /** Le zip contenant la simulation prete a etre executee. */
    protected File simulationZip;

    /**
     * Si vrai, le fichier zip n'est pas commun à un ensemble de simulations.
     * 
     * Default to {@code true}.
     */
    protected boolean standaloneSimulationZip = true;

    /** Simulation prescript. */
    protected String generatedPrescriptContent;

    /** 
     * La simulation doit être lancée seule, sinon les simulations sont
     * indépendantes et peuvent être lancées groupées.
     * 
     * Default to {@code true}.
     */
    protected boolean standaloneSimulation = true;

    /**
     * Simulation number in non standalone simulation.
     */
    protected int simulationNumber = -1;

    /**
     * last simulation in a simulation pool.
     */
    protected boolean lastSimulation;

    /**
     * Constructor.
     * 
     * @param control le control pour la simulation.
     * @param param les parametres de simulation (can be null)
     */
    public SimulationItem(SimulationControl control, SimulationParameter param) {
        this.control = control;
        
        // use soft reference to improve minimum memory usage
        if (param != null) {
            this.parameter = new SimulationParameterCache(param);
        }
    }

    /**
     * Retourne le control pour la simulation.
     * 
     * @return le control pour la simulation.
     */
    public SimulationControl getControl() {
        return control;
    }

    /**
     * Retourne les parametres de simulation.
     * 
     * Attention, ce ne sont pas forcement les parametres de la simulation
     * en cours, mais ceux ayant servit à la générer.
     * 
     * @return les parametres de simulation.
     */
    public SimulationParameter getParameter() {
        return parameter;
    }

    /**
     * Permet de mettre le zip apres construction de l'objet. Cela est util
     * car la construction du zip prend du temps, et on souhaite pouvoir le
     * monitorer et donc il faut que l'item exist pour etre vu dans la console
     * de queue de simulation.
     * 
     * @param simulationZip zip to set
     */
    public void setSimulationZip(File simulationZip) {
        this.simulationZip = simulationZip;
    }

    /**
     * Retourne le zip contenant la simulation prete a etre executée.
     * 
     * @return le zip contenant la simulation prete a etre executee.
     */
    public File getSimulationZip() {
        return simulationZip;
    }

    /**
     * @return the standaloneSimulationZip
     */
    public boolean isStandaloneSimulationZip() {
        return standaloneSimulationZip;
    }

    /**
     * @param standaloneSimulationZip the standaloneSimulationZip to set
     */
    public void setStandaloneSimulationZip(boolean standaloneSimulationZip) {
        this.standaloneSimulationZip = standaloneSimulationZip;
    }

    /**
     * Get simulation prescript.
     * 
     * @return the generatedPrescriptContent
     */
    public String getGeneratedPrescriptContent() {

        String preScript = generatedPrescriptContent;

        // simulationPrescriptContent is not null when isis is lauched with
        // simulation on comand line
        // If preScript but params is not null, simulation has been launched by
        // UI, use cached simulation parameters
        if (preScript == null && parameter != null) {
            preScript = parameter.getGeneratedPreScript();
        }

        return preScript;
    }

    /**
     * Set simulation prescript.
     * 
     * @param generatedPrescriptContent the generatedPrescriptContent to set
     */
    public void setGeneratedPrescriptContent(String generatedPrescriptContent) {
        this.generatedPrescriptContent = generatedPrescriptContent;
    }

    /**
     * @return the standaloneSimulation
     */
    public boolean isStandaloneSimulation() {
        return standaloneSimulation;
    }

    /**
     * @param standaloneSimulation the standaloneSimulation to set
     */
    public void setStandaloneSimulation(boolean standaloneSimulation) {
        this.standaloneSimulation = standaloneSimulation;
    }

    /**
     * @return the simulationNumber
     */
    public int getSimulationNumber() {
        return simulationNumber;
    }

    /**
     * @param simulationNumber the simulationNumber to set
     */
    public void setSimulationNumber(int simulationNumber) {
        this.simulationNumber = simulationNumber;
    }

    /**
     * @return the lastSimulation
     */
    public boolean isLastSimulation() {
        return lastSimulation;
    }

    /**
     * @param lastSimulation the lastSimulation to set
     */
    public void setLastSimulation(boolean lastSimulation) {
        this.lastSimulation = lastSimulation;
    }
}
