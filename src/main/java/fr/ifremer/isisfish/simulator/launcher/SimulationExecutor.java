/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2010 Ifremer, Code Lutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator.launcher;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import static org.nuiton.i18n.I18n.t;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Un executor qui utilise un certain type de {@link SimulatorLauncher} pour
 * executer les jobs soumis dans la queue de {@link SimulationService}
 * <p>
 * Les fonctionnalites en plus par rapport a un {@link ThreadPoolExecutor} sont:
 * <ul>
 * <li> la possibilite de suspendre l'executor puis ensuite de le relancer</li>
 * <li> l'utilisation de beforeExecute pour fixer le {@link SimulatorLauncher}
 * du job</li>
 * </ul>
 * <p>
 * Il est aussi possible d'ecoute l'etat de l'attribut pause
 * 
 * @author poussin
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class SimulationExecutor extends ThreadPoolExecutor {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    private static Log log = LogFactory.getLog(SimulationExecutor.class);

    protected PropertyChangeSupport propertyListeners = new PropertyChangeSupport(
            this);
    /** vrai si cet executor est en pause */
    protected boolean pause = false;
    private ReentrantLock pauseLock = new ReentrantLock();
    private Condition unpaused = pauseLock.newCondition();

    /** le nombre de thread utilise avant la pause */
    protected int lastCorePoolSize;
    /** le simulation service qui a cree cet executor */
    protected SimulationService simulationService;
    /** le launcher a utilise pour les simulations */
    protected SimulatorLauncher launcher;

    public SimulationExecutor(SimulationService simulationService,
            SimulatorLauncher launcher, SimulationQueue workQueue) {
        // need 2 threads for Optimization simulation, ensure that executor has 2 threads (Math.max)
        super(Math.max(2, launcher.maxSimulationThread()), Math.max(2, launcher.maxSimulationThread()),
                0L, TimeUnit.MILLISECONDS, (BlockingQueue) workQueue);
        this.simulationService = simulationService;
        this.launcher = launcher;
        lastCorePoolSize = prestartAllCoreThreads();
        log.info(t("SimulationExecutor started with %s thread for %s",
                lastCorePoolSize, launcher));
    }

    public SimulationService getSimulationService() {
        return simulationService;
    }

    public SimulatorLauncher getLauncher() {
        return launcher;
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyListeners.addPropertyChangeListener(listener);
    }

    public void addPropertyChangeListener(String propertyName,
            PropertyChangeListener listener) {
        propertyListeners.addPropertyChangeListener(propertyName, listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyListeners.removePropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(String propertyName,
            PropertyChangeListener listener) {
        propertyListeners.removePropertyChangeListener(propertyName, listener);
    }

    /**
     * Met les threads en attente si pause est vrai, et fixe le
     * {@link SimulatorLauncher} a utiliser pour par la tache.
     * Si la tache a deja un {@link SimulatorLauncher} d'assigne, on ne fait
     * pas la tache
     * 
     * @param t le thread qui va executer la tache
     * @param r
     */
    @Override
    protected void beforeExecute(Thread t, Runnable r) {
        super.beforeExecute(t, r);

        if (r instanceof SimulationJob) {
            SimulationJob job = (SimulationJob) r;

            // en premier lieu il faut que SimulationService autorise le lancement
            getSimulationService().waitAutoLaunch(job);

            // wait if this executor is in pause status
            pauseLock.lock();
            try {
                while (isPause()) {
                    job.getItem().getControl().setText(t("Pause"));
                    unpaused.await();
                }
            } catch (InterruptedException ie) {
                t.interrupt();
            } finally {
                pauseLock.unlock();
            }

//        // ensuite si cet executor est en pause, on tu le thread apres avoir
//        // resoumis la simulation
//        if (isPause()) {
//            // on est en pause, il faut remettre la tache et tuer le thread
//            // un simple suspend du thread n'est pas bon, car la tache ne
//            // serait alors jamais executer et ce n'est pas forcement ce que
//            // l'on souhaite
//            if (r instanceof SimulationJob) {
//                SimulationService.getService().resubmit((SimulationJob) r);
//            }
//            throw new RuntimeException(
//                    t("Normal stop thread, this is not an error"));
//        }

            if (job.getLauncher() == null) {
                // si la tache a deja un launcher, on l'utilise sinon on lui
                // indique d'utilise le notre. C'est seul facon propre et sans
                // risque de pouvoir choisir un launcher (en l'assignant au job
                // avant d'arriver ici). Car l'autre solution serait de remettre
                // la tache dans la queue et d'attendre que le bon executor
                // la prenne, mais en fait cela impliquerait que dans le 
                // launcher on puisse refaire un test pour savoir si on doit
                // reellement faire la tache ou non (trop compliquer pour
                // l'implantation de launcher). et surtout on risquerait de 
                // boucler sans jamais executer la tache, s'il n'existe pas
                // d'executor avec ce launcher ou qu'il est arrete. Dans ce 
                // dernier cas s'etait a l'utilisateur d'etre coherant, c-a-d
                // de ne pas arreter un executor puis ensuite de demander
                // explicitement a l'utiliser
                job.setLauncher(launcher);
            }
        } else {
            log.warn(t("Jobs submited is not ItemSimulation but was %s", r
                    .getClass().getName()));
        }
    }

    public boolean isPause() {
        return pause;
    }

    /**
     * demande au thread d'arreter de prendre de nouvelle tache
     */
    public void pause() {
        setPause(true);
    }

    /**
     * indique au thread de recommencer a prendre de nouvelle tache
     */
    public void resume() {
        setPause(false);
    }

    /**
     * Change la valeur de la variable pause, si pause est alors vrai
     * notifie tous les threads en attente
     * @param pause pause value to set
     */
    public void setPause(boolean pause) {
        pauseLock.lock();
        try {

            if (this.pause == pause) {
                // same state, do nothing
                return;
            }
            boolean oldValue = this.pause;
            this.pause = pause;
            if (!this.pause) {
                unpaused.signalAll();
            }
            propertyListeners.firePropertyChange("pause", oldValue, pause);
        } finally {
            pauseLock.unlock();
        }

//        boolean oldValue = this.pause;
//        synchronized (this) {
//            this.pause = pause;
//            if (this.pause) {
//                lastCorePoolSize = getCorePoolSize();
//                setCorePoolSize(0);
//            } else {
//                setCorePoolSize(lastCorePoolSize);
//                // on recree tous les threads qui avait ete tue par la pause
//                prestartAllCoreThreads();
//            }
//        }
//        propertyListeners.firePropertyChange("pause", oldValue, this.pause);
    }

}
