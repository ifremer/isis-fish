/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2012 Ifremer, Code Lutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator.launcher;

import static org.nuiton.i18n.I18n.t;

import fr.ifremer.isisfish.datastore.SimulationInformation;
import fr.ifremer.isisfish.simulator.SimulationControl;
import fr.ifremer.isisfish.simulator.SimulationParameter;
import fr.ifremer.isisfish.types.TimeStep;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.profiling.Unit;

/**
 * Model de table pour suivre l'evolution des differentes simulations en cours.
 * 
 * <p>
 * <b>ATTENTION</b> Cette classe doit supporter les acces concurrents car
 * plusieurs threads peuvent etre simultanement en train de faire des
 * simulations
 *
 * @author poussin
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class SimulationServiceTableModel extends AbstractTableModel {

    /** serialVersionUID */
    private static final long serialVersionUID = 2414926794815727974L;

    /** to use log facility, just put in your code: log.info(\"...\"); */
    private final static Log log = LogFactory
            .getLog(SimulationServiceTableModel.class);

    /** Columns names. */
    protected final static String[] COLUMN_HEADER = new String[] {
            t("isisfish.queue.id"),
            t("isisfish.queue.plan"),
            t("isisfish.queue.launcher"),
            t("isisfish.queue.status"),
            t("isisfish.queue.progression"), };

    /** Columns types. */
    protected Class<?>[] columnClass = new Class[] {
            String.class, // id
            String.class, // simulation plan number
            String.class, // local, remote, batch
            String.class, // text
            JProgressBar.class // progress
    };

    protected SimulationService simulationService;
    protected ArrayList<SimulationJob> jobs;
    protected Map<String, SimulationJob> jobIds;
    /** progress bar (one for each row) */
    protected Map<SimulationJob, JProgressBar> progress = new WeakHashMap<>();

    protected AbstractJobListener jobListener;
    protected ControlListener controlListener;

    public SimulationServiceTableModel(SimulationService simulationService, boolean forDoToJobs) {
        this.simulationService = simulationService;
        if (forDoToJobs) {
            jobListener = new JobToDoListener(this);
        } else {
            jobListener = new JobDoneListener(this);
        }
        controlListener = new ControlListener(this);
        simulationService.addSimulationServiceListener(jobListener);
        //jobListener.setData();
    }

    public void addJob(SimulationJob job) {
        String id = job.getItem().getControl().getId();
        synchronized (jobs) {
            if (!contains(job)) {
                jobs.add(job);
                jobIds.put(id, job);
                SwingUtilities.invokeLater(() -> fireTableRowsInserted(jobs.size() - 1, jobs.size() - 1));
            }
        }
    }

    public void removeJob(SimulationJob job) {
        String id = job.getItem().getControl().getId();
        synchronized (jobs) {
            final int index = jobs.indexOf(job);
            if (index >= 0) {
                jobs.remove(index);
                jobIds.remove(id);
                SwingUtilities.invokeLater(() -> fireTableRowsDeleted(index, index));
            }
        }
    }

    public void clearJob() {
        jobs.clear();
        SwingUtilities.invokeLater(this::fireTableDataChanged);
    }

    public ArrayList<SimulationJob> getJobs() {
        return jobs;
    }

    public void setJobs(ArrayList<SimulationJob> jobs) {
        this.jobs = jobs;
        synchronized (jobs) {
            jobIds = new HashMap<>(jobs.size());
            for (SimulationJob job : jobs) {
                jobIds.put(job.getItem().getControl().getId(), job);
            }
            fireTableDataChanged();
        }
    }

    public boolean contains(SimulationJob job) {
        String id = job.getItem().getControl().getId();
        synchronized (jobs) {
            boolean result = jobIds.containsKey(id);
            return result;
        }
    }

    protected JProgressBar getProgressBar(SimulationJob job) {
        JProgressBar result = progress.get(job);
        if (result == null) {
            result = new JProgressBar(JProgressBar.HORIZONTAL, 0, 100);
            result.setStringPainted(true);
            progress.put(job, result);
        }
        return result;
    }

    public SimulationJob getJob(int row) {
        SimulationJob result = jobs.get(row);
        return result;
    }

    @Override
    public int getRowCount() {
        int result = jobs.size();
        return result;
    }

    @Override
    public int getColumnCount() {
        int result = COLUMN_HEADER.length;
        return result;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return columnClass[columnIndex];
    }

    @Override
    public String getColumnName(int column) {
        return COLUMN_HEADER[column];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        // FIXME echatellier : hack ArrayIndexOutOfBoundException for unknown
        // reason (very hard to fix properly)
        if (rowIndex >= jobs.size()) {
            if (columnIndex == 4) {
                return new JProgressBar(); // NPE
            } else {
                return null;
            }
        }

        Object result = "";

        SimulationJob job = jobs.get(rowIndex);
        SimulationControl control = job.getItem().getControl();
        String id = control.getId();
        SimulationParameter param = job.getItem().getParameter();

        if (log.isTraceEnabled()) {
            log.trace("Update table model : " + "id = " + control.getId()
                    + ", " + "control.getProgress() = " + control.getProgress()
                    + ", " + "control.getProgressMax() = "
                    + control.getProgressMax() + ", " + "control.getDate() = "
                    + control.getStep());
        }

        switch (columnIndex) {
        case 0:
            result = id;
            break;
        case 1:
            if (param.getUseSimulationPlan()) {
                int number = param.getSimulationPlanNumber();
                if (number >= 0) {
                    result = number;
                }
            }
            break;
        case 2:
            if (job.getLauncher() == null) {
                if (param.getUseSimulationPlan()) {
                    result = t("isisfish.queue.masterplan");
                } else {
                    result = t("isisfish.queue.notstarted");
                }
            } else {
                result = job.getLauncher().toString();
            }
            break;
        case 3:
            if (control.isStopSimulationRequest()) {
                result = t("isisfish.launch.stop");
            } else {
                result = control.getText();
            }
            break;
        case 4:
            JProgressBar pb = getProgressBar(job);

            int value = (int) control.getProgress();
            int max = (int) control.getProgressMax();
            double meanTime = control.getTimeStepMeanTime();

            int rest = max - value;
            double time = rest * meanTime;
            if (time <= 0) {
                // fin de simulation, on met le temps de simulation
                SimulationInformation info = control.getSimulation().getInformation();
                Date start = info.getSimulationStart();
                Date end = info.getSimulationEnd();
                if (start != null && end != null) {
                    time = (end.getTime() - start.getTime()) / 1000;
                }
            }

            long milli = (long)Unit.Time.s.convertTo(Unit.Time.milli, time);
            String restTime;
            if (milli >= 0) {
                restTime = DurationFormatUtils.formatDurationHMS(milli);
            } else {
                restTime = "";
            }
            
            pb.setMaximum(max);
            pb.setValue(value);

            // progress can be used for other things
            TimeStep step = control.getStep();
            if (step != null) {
                pb.setString(step.getMonth() + "/" + step.getYear() + " ("+ restTime +")");
            }
            else {
                pb.setString("");
            }
            result = pb;
            break;
        }
        return result;
    }

    protected interface AbstractJobListener extends SimulationServiceListener {
        void setData();
    }

    class JobDoneListener implements AbstractJobListener, SimulationServiceListener {

        protected SimulationServiceTableModel model;

        public JobDoneListener(SimulationServiceTableModel model) {
            this.model = model;
            setData();
        }

        @Override
        public void setData() {
            model.setJobs(new ArrayList<>(simulationService.getJobDones()));
        }

        @Override
        public void simulationStart(SimulationService simService,
                SimulationJob job) {
            // this happens when job is restarted
            // remove it from here
            model.removeJob(job);
        }

        @Override
        public void simulationStop(SimulationService simService,
                SimulationJob job) {
            model.addJob(job);
        }

        @Override
        public void clearJobDone(SimulationService simService) {
            model.clearJob();
        }
    }

    class JobToDoListener implements AbstractJobListener, SimulationServiceListener {

        protected SimulationServiceTableModel model;

        public JobToDoListener(SimulationServiceTableModel model) {
            this.model = model;
            setData();
        }

        @Override
        public void setData() {
            ArrayList<SimulationJob> jobs = new ArrayList<>(simulationService.getJobs());
            for (SimulationJob job : jobs) {
                job.getItem().getControl().addPropertyChangeListener(controlListener);
            }
            model.setJobs(jobs);
        }

        @Override
        public void simulationStart(SimulationService simService, SimulationJob job) {
            model.addJob(job);
            job.getItem().getControl().addPropertyChangeListener(
                    model.controlListener);
        }

        @Override
        public void simulationStop(SimulationService simService, SimulationJob job) {
            model.removeJob(job);
            job.getItem().getControl().removePropertyChangeListener(
                    model.controlListener);
        }

        @Override
        public void clearJobDone(SimulationService simService) {
            // nothing to do
        }

    }

    protected class ControlListener implements PropertyChangeListener {

        protected SimulationServiceTableModel model;

        public ControlListener(SimulationServiceTableModel model) {
            this.model = model;
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            SimulationControl control = (SimulationControl) evt.getSource();
            String id = control.getId();
            synchronized (model.jobs) {
                SimulationJob job = model.jobIds.get(id);
                final int index = model.getJobs().indexOf(job);
                if (index >= 0) {
                    SwingUtilities.invokeLater(() -> fireTableRowsUpdated(index, index));
                }
            }
        }

    }
}
