/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2018 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator.launcher;

import static org.nuiton.i18n.I18n.t;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.AbstractMap.SimpleEntry;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.SortedSet;
import java.util.Timer;
import java.util.TreeSet;
import java.util.function.Function;

import fr.ifremer.isisfish.logging.SimulationLoggerUtil;
import fr.ifremer.isisfish.util.IsisFileUtil;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;

import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.datastore.SimulationInformation;
import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.simulator.SimulationControl;
import fr.ifremer.isisfish.simulator.SimulationParameter;
import fr.ifremer.isisfish.simulator.SimulationParameterImpl;
import fr.ifremer.isisfish.simulator.launcher.SimulationJob.PostAction;
import fr.ifremer.isisfish.simulator.sensitivity.SensitivityAnalysis;
import fr.ifremer.isisfish.simulator.sensitivity.SensitivityException;

/**
 * Moniteur singleton pour pouvoir sauvegarder localement la liste des
 * simulations démarrées et permettre de continuer le monitoring au
 * relancemenent d'Isis.
 * 
 * Depuis la version 3.3, elle sauvegarde en plus les zip pour pouvoir
 * relancer une simulation même si isis a été arreté entre temps.
 *
 * Cette classe n'implemente pas {@link SimulationServiceListener}
 * car elle ne doit pas écouter toutes les simulations, mais
 * seulement celle dit simple, c'est-à-dire "master plan" du plan
 * d'analyse.
 * 
 * Il faut bien faire attention lors du relancement des simulations a copier
 * le zip a un autre endroit. Il est important d'avoir deux zip car celui
 * de la sauvegarde est supprimé des que la simulation est arrete.
 * 
 * @since 3.2.0.4
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class SimulationMonitor extends Thread {

    /** Class logger. */
    private static Log log = LogFactory.getLog(SimulationMonitor.class);

    /** Simulation start date format. */
    private static final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    
    protected static final String PROPERTIES_FILE = "monitoring.properties";
    protected static final String PROPERTY_LAUNCHER = "simulation.launcher";
    protected static final String PROPERTY_DATE = "simulation.date";
    protected static final String PROPERTY_ZIP = "simulation.zip";
    protected static final String PROPERTY_STANDALONE = "simulation.standalone";
    protected static final String PROPERTY_LASTSIMULATION = "simulation.last";

    /** Instance. */
    protected static SimulationMonitor instance = new SimulationMonitor();

    /**
     * Simulation information saving file.
     */
    protected File monitorFolder;

    /**
     * Map to remember when a simulation have to be checked.
     * 
     * Sorted on date ASC.
     */
    protected SortedSet<SimpleEntry<Date, SimulationJob>> checkSet;

    /**
     * Check scheduler.
     */
    protected Timer checkScheduler;

    /**
     * Constructeur.
     */
    protected SimulationMonitor() {

        // nomme le thread
        super("monitor-thread");

        monitorFolder = IsisFish.config.getMonitoringDirectory();

        // init monitor check map
        // TODO change this, it's a set with duplicated entries !!!
        // with bad luck (or windows system) date can be equals :(
        checkSet = new TreeSet<>(Comparator.comparing((Function<SimpleEntry<Date, SimulationJob>, Date>) SimpleEntry::getKey).thenComparing(SimpleEntry::getValue));

        // init timer
        checkScheduler = new Timer();
    }

    /**
     * Instance getter.
     * 
     * @return instance
     */
    public static SimulationMonitor getInstance() {
        return instance;
    }

    /**
     * Reload config and read non simulation job into simulation service.
     * 
     * @param service SimulationService
     */
    public void reloadConfig(final SimulationService service) {
        // in a thread
        // don't temporize UI loading
        new Thread(() -> restartSimulationProgression(service)).start();
    }

    /**
     * Take all simulation in loaded file.
     * And add it into simulation service.
     */
    protected void restartSimulationProgression(SimulationService service) {

        File[] files = monitorFolder.listFiles();
        for (File file : files) {
            // folder is composed of one folder for each simulation
            if (file.isDirectory()) {

                String simulationId = file.getName();
                if (log.isInfoEnabled()) {
                    log.info("Restart monitoring of " + simulationId);
                }

                // FIXME date remove simulation that are older than a week ?

                File propertiesFile = new File(file, PROPERTIES_FILE);

                if (propertiesFile.isFile()) {

                    try (InputStream propertiesFileIS = new BufferedInputStream(new FileInputStream(propertiesFile))) {
                        Properties simulationProperties = new Properties();
                        simulationProperties.load(propertiesFileIS);

                        restartSimulation(service, file, simulationId, simulationProperties);
                    } catch (IOException e) {
                        if (log.isErrorEnabled()) {
                            log.error("Can't get launcher for this simulation", e);
                        }
                    }
                } else {
                    if (log.isWarnEnabled()) {
                        log.warn("No " + PROPERTIES_FILE + " found in "
                                + file.getAbsolutePath()
                                + "(skipping restarting)");
                    }
                }
            }
        }
    }

    /**
     * Restart a single simulation.
     * 
     * @param service simulation service
     * @param simulationFolder simulation information folder
     * @param simulationId simulation id
     * @param propertiesFile simulation properties file
     * 
     * @throws IOException 
     */
    protected void restartSimulation(SimulationService service,
            File simulationFolder, String simulationId,
            Properties propertiesFile) throws IOException {

        try {
            // launcher
            String simulationLauncher = propertiesFile.getProperty(PROPERTY_LAUNCHER);
            SimulatorLauncher launcher = (SimulatorLauncher) Class.forName(simulationLauncher).newInstance();

            // zip (copy to temp in mandatory)
            String zipFileName = propertiesFile.getProperty(PROPERTY_ZIP);
            File zipFile = new File(simulationFolder, zipFileName);
            File tempZipFile = IsisFileUtil.createTempFile("simulation-" + simulationId, ".zip");
            FileUtils.copyFile(zipFile, tempZipFile);
            tempZipFile.deleteOnExit();

            // On construit le job.
            // il faut qu'il soit suffisament valide pour pourvoir démarrer
            // il contient :
            //  - un launcher
            //  - un zip (pour relancement)
            // Et il est relance avec un parametre "checkonly"
            SimulationParameter params = new SimulationParameterImpl();
            SimulationControl control = new SimulationControl(simulationId);

            SimulationItem item = new SimulationItem(control, params);
            item.setSimulationZip(tempZipFile); // tempZipFile, important !

            // set additionnal parameters (null safe, defaut parameter safe)
            String standalone = propertiesFile.getProperty(PROPERTY_STANDALONE);
            item.setStandaloneSimulation(!"false".equalsIgnoreCase(standalone));
            String lastSimulation = propertiesFile.getProperty(PROPERTY_LASTSIMULATION);
            item.setLastSimulation("true".equalsIgnoreCase(lastSimulation));

            SimulationJob job = new SimulationJob(service, item, 0);
            job.setLauncher(launcher);
            service.submitForCheckOnly(job);
        } catch (ClassNotFoundException e) {
            if (log.isErrorEnabled()) {
                log.error("Can't found launcher for this simulation", e);
            }
        } catch (InstantiationException | IllegalAccessException e) {
            if (log.isErrorEnabled()) {
                log.error("Can't get launcher for this simulation", e);
            }
        }
    }

    /**
     * Save simulation informations for started simulations.
     * 
     * @param job started job
     */
    public synchronized void simulationStart(SimulationJob job) {

        String simulationId = job.getId();
        SimulationItem simulationItem = job.getItem();
        File simulationMonitoringFolder = new File(monitorFolder, simulationId);
        if (!simulationMonitoringFolder.exists()) {
            simulationMonitoringFolder.mkdirs();
        }
        File simulationPropertiesFile = new File(simulationMonitoringFolder, PROPERTIES_FILE);

        if (!simulationPropertiesFile.exists()) {
            Properties simulationProperties = new Properties();

            // launcher
            SimulatorLauncher launcher = job.getLauncher();
            String launcherName = launcher.getClass().getName();
            simulationProperties.setProperty(PROPERTY_LAUNCHER, launcherName);

            // date
            String simulationDate = dateFormat.format(new Date());
            simulationProperties.setProperty(PROPERTY_DATE, simulationDate);

            // zip
            File zipFile = simulationItem.getSimulationZip();

            try {
                // original zip must be keeped outside monitoring folder

                // for standalone, set it in parent directory
                if (simulationItem.isStandaloneSimulationZip()) {
                    FileUtils.copyFileToDirectory(zipFile, simulationMonitoringFolder);
                    simulationProperties.setProperty(PROPERTY_ZIP, zipFile.getName());
                }
                else {
                    String shortSimulationId = simulationId.substring(0, simulationId.lastIndexOf('_'));
                    // zip common to multiples simulations
                    // saved in monitoring folder at root
                    File savedZipFile = new File(simulationMonitoringFolder.getParent(), shortSimulationId + ".zip");
                    // the first simulation is in charge to save the common zip file
                    if (simulationItem.getSimulationNumber() == 0) {
                        FileUtils.copyFile(zipFile, savedZipFile);
                    }
                    simulationProperties.setProperty(PROPERTY_ZIP, ".." + File.separator + savedZipFile.getName());
                }

            } catch (IOException e) {
                if (log.isErrorEnabled()) {
                    log.error("Can't copy simulation zip", e);
                }
            }

            // additionnal infos
            simulationProperties.setProperty(PROPERTY_STANDALONE, String.valueOf(simulationItem.isStandaloneSimulation()));
            simulationProperties.setProperty(PROPERTY_LASTSIMULATION, String.valueOf(simulationItem.isLastSimulation()));

            // save property file
            try (OutputStream out = new BufferedOutputStream(new FileOutputStream(simulationPropertiesFile))) {
                simulationProperties.store(out, "Simulation added : " + simulationId);
            } catch (IOException e) {
                if (log.isErrorEnabled()) {
                    log.error("Can't save monitor file", e);
                }
            }

            if (log.isInfoEnabled()) {
                log.info("Saving simulation " + simulationId + " as started");
            }
        } else {
            if (log.isDebugEnabled()) {
                log.debug("We already know information about this simulation");
                log.debug("Skip simulation information saving");
            }
        }

        // put interval in launcher ?
        Date checkDate = new Date();
        // do check at start
        //long intervalInSeconds = launcher.getCheckProgressionInterval();
        //checkDate.setTime(checkDate.getTime() + intervalInSeconds);

        // add simulation to check queue
        checkSet.add(new SimpleEntry<>(checkDate, job));

        // start thread if not already started
        if (!this.isAlive()) {
            this.start();
        }
    }

    /**
     * Mark a simulation as stopped.
     * Remove it from save file.
     * 
     * @param job job that control the simulation
     */
    public synchronized void simulationStop(SimulationJob job) {

        String simulationId = job.getId();
        File simulationMonitoringFolder = new File(monitorFolder, simulationId);

        try {
            FileUtils.deleteDirectory(simulationMonitoringFolder);
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Can't remove simulation informations", e);
            }
        }

        if (log.isInfoEnabled()) {
            log.info("Saving simulation " + simulationId + " as stopped");
        }
    }

    @Override
    public void run() {

        // main loop to monitor "all" running simulation

        while (true) {

            // quoi qu'il arrive, le thread ne doit jamais s'arreter
            try {
                waitAndCheckProgression();
            } catch (Exception e) {
                if (log.isErrorEnabled()) {
                    log.error("An exception occurs during monitoring", e);
                }
            }
        }
    }

    /**
     * Wait a little time, and check for first
     * simulation progression, if first simulation time
     * is reached.
     */
    protected void waitAndCheckProgression() {

        // wait a minimum time
        // 1s is to long to stop 1000 simulations
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            if (log.isErrorEnabled()) {
                log.error("Monitor thread has been interrupted", e);
            }
        }

        // take first job in map
        if (!checkSet.isEmpty()) {
            SimpleEntry<Date, SimulationJob> firstEntry = checkSet.first();
            Date date = firstEntry.getKey();
            Date now = new Date();

            if (date.before(now)) {
                // first key date in map is over...
                // check is needed
                SimulationJob job = firstEntry.getValue();
                SimulatorLauncher launcher = job.getLauncher();

                if (log.isDebugEnabled()) {
                    log.debug("Check simulation progression : " + job.getId());
                }

                // retourne true if :
                //  - simulation is finished
                //  - simulation end has been asked
                boolean jobIsFinished = checkProgression(job, launcher);

                // always remove this simulation from map
                checkSet.remove(firstEntry);

                // if not finished, re-add it at end
                if (jobIsFinished) {
                    if (log.isInfoEnabled()) {
                        log.info("Job " + job.getId() + " finished");
                    }
                    doPostSimulationOperation(job, launcher);
                }

                // Si le job est reelement fini
                // et que les post operation ont réussie
                else {
                    // next check date
                    long nextJobTimeMs = date.getTime() + launcher.getCheckProgressionInterval() * 1000;
                    Date nextJobDate = new Date(nextJobTimeMs);
                    checkSet.add(new SimpleEntry<>(nextJobDate, job));
                }

            } else {
                //checkScheduler.schedule(this, date);
                if (log.isTraceEnabled()) {
                    log.trace("Time to check not reached, skip check, " + date + " < " + now);
                }
            }
        }
    }

    /**
     * Check progression.
     * 
     * @param job job to check progression
     * @param launcher SimulatorLauncher that manage simulation
     * @return {@code true} is simulation is finished, {@code false} otherwise
     */
    protected boolean checkProgression(SimulationJob job, SimulatorLauncher launcher) {

        boolean simulationEnded = false;

        SimulationService service = SimulationService.getService();
        SimulationControl control = job.getItem().getControl();

        // log
        if (log.isDebugEnabled()) {
            log.debug("Checking simulation progression : " + control.getId());
            log.debug(" with launcher = " + launcher);
        }

        // update
        try {
            if (control.isStopSimulationRequest()) {
                launcher.simulationStopRequest(job);
                simulationEnded = true;
            } else {
                launcher.updateControl(service, control);
    
                // by default, Progress = ProgressMax = 0
                // WARNING this condition is VERY important
                // and set by end of
                // fr.ifremer.isisfish.simulator.launcher.InProcessSimulatorLauncher#localSimulateSameThread(SimulationControl, SimulationStorage)

                File simulationRoot = SimulationStorage.getSimulationDirectory(control.getId());
                File localInformationFile = SimulationStorage.getSimulationInformationFile(simulationRoot);

                // si la simulation n'est pas localement presente, c'est pas fini
                // la simulation peut être finie, mais non téléchargées
                // en testant le fichier d'information, on s'assure que la simulation est physiquement
                // présente en local
                if (localInformationFile.isFile()) {
                    // cas d'une simulation terminée
                    if (control.getProgress() > 0 && control.getProgress() >= control.getProgressMax()) {
                        simulationEnded = true;
                    } else {
                        // cas d'une simulation arretee par exception
                        SimulationInformation information = new SimulationInformation(localInformationFile);
                        if (StringUtils.isNotEmpty(information.getException())) {
                            if (log.isWarnEnabled()) {
                                log.warn("Simulation exception : " + information.getException());
                            }
                            simulationEnded = true;
                        }
                    }
                }
            }
        } catch (RemoteException e) {
            if (log.isErrorEnabled()) {
                log.error("Progression thread update error", e);
            }
        }

        return simulationEnded;
    }

    /**
     * Operation to do after simulation end.
     * 
     * <ul>
     *  <li>get simulation result</li>
     *  <li>do registred post actions</li>
     *  <li>do sensitivity analisys second pass</li>
     * </ul>
     * 
     * @param job job
     * @param launcher {@link SimulatorLauncher} that manage simulation
     */
    protected void doPostSimulationOperation(SimulationJob job, SimulatorLauncher launcher) {

        SimulationService service = SimulationService.getService();
        SimulationControl control = job.getItem().getControl();

        if (log.isInfoEnabled()) {
            log.info("Do post simulation operation for " + control.getId());
        }

        try {

            // get storage
            // meme si StopSimulationRequest
            // force le download dans le cas de ssh
            SimulationStorage simulation = launcher.getSimulationStorage(service, control);
            
            //TODO simulation is null here when a in process simulation
            //TODO has been restarted

            if (simulation != null) {

                simulation.getInformation().reloadLocal();

                // on ne fait de post traitement que si
                // la simulation n'a pas été arretée
                if (!control.isStopSimulationRequest()) {
                    // post manage this storage
                    boolean simulationAvailable = true; //exportResult(job, simulation);
    
                    // do posts actions
                    for (PostAction action : job.getPostActions()) {
                        try {
                            action.finished(job, simulation);
                        } catch (Exception eee) {
                            if (log.isErrorEnabled()) {
                                log.error(t("Can't do post action %s", action), eee);
                            }
                        }
                    }
    
                    // sensitivity analysis result call
                    // can't do analyze second pass if simulation
                    // has been deleted
                    if (simulationAvailable) {
                        analyzeSensitivityResult(job, simulation);
                    }
                }
                
                try {
                    // FIXME poussin 20140729 je pense que ca pose probleme qu'un
                    // thread autre que le thread de simulation ferme les storages
                    // surtout que la condition est stopRequest (simulation pas forcement fini)
                    // ca ne poserait pas de probleme si la condition etait isRunning() == false
                    // Et surtout c'est tres bizarre que ce soit le travail d'un Monitor :(
                    simulation.closeStorage();
                    simulation.closeMemStorage();
                } catch (TopiaException ex) {
                    if (log.isErrorEnabled()) {
                        log.error("Can't close simulation", ex);
                    }
                }
            }
            else {
                // allow simulation to restart to be properly removed
                if (log.isWarnEnabled()) {
                    log.warn("Check null simulation check (launcher return null simulation)");
                }
            }

            // notify simulation ended
            // to not check later...
            // don't do in finally, getSimulationStorage may fail (md5 on ssh)
            simulationStop(job);
            service.fireStopEvent(job);

        } catch (RemoteException e) {
            if (log.isErrorEnabled()) {
                log.error("Can't get simulation results after simulation end", e);
            }
        }
    }

    /**
     * For each finished simulation, check that all sensitivity
     * simulation are available (in case of sensitivity simulation only).
     * 
     * @param job job
     * @param simulation simulation
     */
    protected void analyzeSensitivityResult(SimulationJob job, SimulationStorage simulation) {

        SimulationParameter params = simulation.getParameter();

        int numberOfSimulation = params.getNumberOfSensitivitySimulation();

        if (numberOfSimulation > 0) {

            // display user text "Looking for last simulation"
            SimulationControl control = job.getItem().getControl();
            control.setText(t("isisfish.simulation.message.lookingforlast"));

            // this is a sensitivity simulation
            String simulationId = job.getId();
            String simulationCommonPrefix = simulationId.substring(0, simulationId.lastIndexOf("_"));
            int numberFinished = getNumberOfFinishedSimulation(simulationCommonPrefix, numberOfSimulation);

            // si on a toutes les simulation est qu'elles sont finie
            // on construit la liste des simulation storage
            // et on l'envoie au script d'analyse de sensibilite
            if (numberFinished == numberOfSimulation) {

                control.setText(t("isisfish.simulation.message.analyzeResults"));

                SensitivityAnalysis sensitivityAnalysis = params.getSensitivityAnalysis();
                if (sensitivityAnalysis != null) {
                    if (log.isDebugEnabled()) {
                        log.debug("Call analyzeResult on sensitivity script " + sensitivityAnalysis.getClass().getSimpleName());
                    }
                    try {
                        SimulationLoggerUtil.setPlanLoggingContext(simulationCommonPrefix);

                        // get full storage list
                        List<SimulationStorage> simulationStorageForAnalyze = getStorageListForSecondPass(
                                simulationCommonPrefix, numberOfSimulation);
                        // build master sensitivity export directory
                        File masterExportDirectory = new File(SimulationStorage.getSensitivityResultsDirectory(), simulationCommonPrefix);
                        // directory must already exists !!!
                        sensitivityAnalysis.analyzeResult(simulationStorageForAnalyze, masterExportDirectory);
                    } catch (SensitivityException e) {
                        if (log.isErrorEnabled()) {
                            log.error("Can't call analyse result", e);
                        }
                    } finally {
                        SimulationLoggerUtil.clearPlanLoggingContext();
                    }
                }
            }
            
            control.setText(t("isisfish.message.simulation.ended"));
        }
    }

    /**
     * Parcourt le dossier de toutes les simulation commencant par le prefix donné,
     * et retourne le nombre de simulation terminées.
     * 
     * @param asPrefixName simulation name (without number suffix)
     * @param numberOfSimulation total simulation number
     * 
     * @return le nombre de simulation réellement terminée
     */
    protected int getNumberOfFinishedSimulation(String asPrefixName,
            int numberOfSimulation) {

        int numberOfFinishedSimulation = 0;

        // en sens inverse, il le fera carrement moins souvent
        for (int simulationIndex = numberOfSimulation - 1; simulationIndex >= 0; --simulationIndex) {
            String currentId = asPrefixName + "_" + simulationIndex;
            if (SimulationStorage.getSimulationControlFile(currentId).isFile()) {
                SimulationControl currentSimulationControl = new SimulationControl(currentId);
                SimulationStorage.readControl(currentId, currentSimulationControl);

                // condition for simulation "end"
                if (currentSimulationControl.getProgress() > 0 && currentSimulationControl.getProgress() >= currentSimulationControl.getProgressMax()) {

                    // new finished simulation found
                    numberOfFinishedSimulation++;
                } else {
                    if (log.isDebugEnabled()) {
                        log.debug("Miss simulation number = " + simulationIndex);
                    }

                    // no need to continue
                    // if only miss one, break
                    break;
                }
            } else {
                break;
            }
        }

        return numberOfFinishedSimulation;
    }

    /**
     * Parcourt les dossiers et instancie les storages une fois
     * qu'on est sur qu'on a toutes les simulations.
     * 
     * @param asPrefixName simulation name (without number suffix)
     * @param numberOfSimulation total simulation number
     * 
     * @return la liste des storages
     */
    protected List<SimulationStorage> getStorageListForSecondPass(String asPrefixName, int numberOfSimulation) {

        // simulation start at 0
        List<SimulationStorage> simulationStorageForAnalyze = new LinkedList<>();
        // en sens inverse, il le fera carrement moins souvent
        for (int simulationIndex = 0; simulationIndex < numberOfSimulation; ++simulationIndex) {
            String currentId = asPrefixName + "_" + simulationIndex;
            SimulationStorage storage = SimulationStorage.getSimulation(currentId);
            // always add first (loop in reverse order)
            // result list is keeped sorted
            simulationStorageForAnalyze.add(storage);
        }

        return simulationStorageForAnalyze;
    }
} // SimulationMonitor
