/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2008 - 2020 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator.launcher;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.config.IsisConfig;
import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.simulator.SimulationControl;
import fr.ifremer.isisfish.ui.keystore.PasswordManager;
import fr.ifremer.isisfish.util.IsisFileUtil;
import fr.ifremer.isisfish.util.ftp.ControlCopyStreamListener;
import fr.ifremer.isisfish.util.ftp.FtpManager;
import fr.ifremer.isisfish.util.ssh.InvalidPassphraseException;
import fr.ifremer.isisfish.util.ssh.SSHAgent;
import fr.ifremer.isisfish.util.ssh.SSHException;
import fr.ifremer.isisfish.util.ssh.SSHUserInfo;
import fr.ifremer.isisfish.util.ssh.SSHUtils;
import freemarker.cache.ClassTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.net.ftp.FTPClient;
import org.nuiton.util.MD5InputStream;
import org.nuiton.util.StringUtil;
import org.nuiton.util.ZipUtil;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.net.ConnectException;
import java.nio.charset.StandardCharsets;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import static org.nuiton.i18n.I18n.t;

/**
 * Use a remote simulation server.
 *
 * Data are uploaded into /scratch/isis-temp-4/ directory on FTP server. Then, on SSH side, this path is referenced
 * as $SCRATCH/eftp/isis-temp-4.
 * On the SSH server, the internal directory $SCRATCH/isis-fish-4 on not available threw FTP path, so we can't download
 * files from it.
 * 
 * ISIS-Fish must be installed on remote server (configuation option path).
 * 
 * Datarmor file layout ($i = plan/as increment) :
 * <ul>
 *  <li>/scratch/isis-temp-4/simulation-$id-preparation.zip : Simulation input zip file</li>
 *  <li>/scratch/isis-temp-4/simulation-$id-prescript.bsh : Simulation prescript (optional)</li>
 *  <li>/scratch/isis-temp-4/simulation-$id-script.seq : Qsub script</li>
 * </ul>
 * <ul>
 *  <li>/scratch/isis-temp-4/simulation-$id-result.zip : Simulation output zip</li>
 *  <li>/scratch/isis-temp-4/simulation-$id-result.zip.md5 : Simulation output zip md5 checksum</li>
 *  <li>/scratch/isis-temp-4/simulation-$id-output.txt</li>
 *  <li>/scratch/isis-temp-4/simulation-$id-pbs.id</li>
 *  <li>/scratch/isis-temp-4/simulation-$id-pbs.out</li>
 *  <li>/scratch/isis-temp-4/simulation-$id-pbs.err/li>
 * </ul>
 * All /scratch/isis-temp-4/simulation-$id-* files are deleted on FTP server after result download.
 * 
 * Special case :
 * <ul>
 *  <li>standalone zip : /scratch/isis-temp-4/simulation-$shortid-result.zip
 *  (uploaded at first simulation)</li>
 *  <li>standalone simulations : /scratch/isis-temp-4/simulation-$shortid-script.seq
 *  (uploaded at last simulation)</li>
 * </ul>
 * where {@code $shortid} is id of parent job (without increment), they are not
 * deleted after result download.
 * 
 * @see JSch
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class SSHSimulatorLauncher implements SimulatorLauncher {

    /** Class logger */
    protected static Log log = LogFactory.getLog(SSHSimulatorLauncher.class);

    /** Freemarker configuration */
    protected Configuration freemarkerConfiguration;

    /** Freemarker qsub template. */
    protected static final String QSUB_SCRIPT_TEMPLATE = "templates/ssh/qsub-script.ftl";

    /** Nom du repertoire où sont stockés les fichiers temporaires. */
    protected static final String REMOTE_TEMP_DIR = "isis-temp-" + IsisConfig.ISIS_FISH_MAJOR_VERSION;

    /** Path absolu du repertoire temporaire. */
    protected static final String SSH_TEMP_PATH = "$SCRATCH/eftp/" + REMOTE_TEMP_DIR + "/";

    /** Path d'upload sur le FTP qui pointe en fait sur {@code $REMOTE_SSH_TEMP_PATH} du point de vue SSH. */
    protected static final String FTP_TEMP_PATH = "/scratch/" + REMOTE_TEMP_DIR + "/";

    /**
     * Opened session to ssh service. Stored in static context to not reask passphrase at each
     * connection.
     */
    protected static Session sshSession;

    /**
     * Constructor.
     * 
     * Init freemarker.
     */
    public SSHSimulatorLauncher() {
        initFreemarker();
    }

    /**
     * Init freemarker configuration.
     */
    protected void initFreemarker() {

        freemarkerConfiguration = new Configuration(Configuration.VERSION_2_3_30);

        // needed to overwrite "Defaults to default system encoding."
        // fix encoding issue on some systems
        freemarkerConfiguration.setDefaultEncoding(StandardCharsets.UTF_8.name());

        // specific template loader to get template from jars (classpath)
        ClassTemplateLoader templateLoader = new ClassTemplateLoader(SSHSimulatorLauncher.class, "/");
        freemarkerConfiguration.setTemplateLoader(templateLoader);

    }

    /**
     * Display message both in commons-logging and control text progress.
     * 
     * @param control control
     * @param message message to display
     */
    protected void message(SimulationControl control, String message) {
        // log
        if (log.isInfoEnabled()) {
            log.info(message);
        }
        // control
        if (control != null) {
            control.setText(message);
        }
    }

    @Override
    public int maxSimulationThread() {

        // mais le serveur les lance quand il veut
        int maxSimulationThread = IsisFish.config.getSimulatorSshMaxThreads();
        
        if (maxSimulationThread <= 0) {
            // always set a minimun of 1
            maxSimulationThread = 1;
        }

        return maxSimulationThread;
    }

    @Override
    public int getCheckProgressionInterval() {

        // par defaut, pour ssh, on utilise 20 secondes

        int interval = IsisFish.config.getSimulatorSshControlCheckInterval();
        return interval;
    }

    @Override
    public String toString() {
        return t("isisfish.simulator.launcher.remote");
    }

    /**
     * {@inheritDoc}
     * 
     * Try to send a qdel command.
     */
    @Override
    public void simulationStopRequest(SimulationJob job) throws RemoteException {

        // make sure user is connected
        try {
            getSSHSession();
        }
        catch(JSchException e) {
            throw new RemoteException("Can't connect", e);
        }
        
        // get simulation item info
        SimulationItem simulationItem = job.getItem();
        if (simulationItem.isStandaloneSimulation()) {
            try {
                sendStopSimulationRequest(sshSession, simulationItem.getControl().getId());
            } catch (SSHException e) {
                throw new RemoteException("Can't connect", e);
            }
        }
        else {
            // for multi job process, try to optimize process
            // by send stop request only fo last job
            
            // multi jobs has been started with last one
            // must be killed with last one too
            if (simulationItem.isLastSimulation()) {
                String simulationid = simulationItem.getControl().getId();
                String shortSimulationId = simulationid.substring(0, simulationid.lastIndexOf('_'));
                try {
                    sendStopSimulationRequest(sshSession, shortSimulationId);
                } catch (SSHException e) {
                    throw new RemoteException("Can't connect", e);
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     * 
     * Dans le cas de ssh: 
     * <ul>
     *  <li>upload la simulation</li>
     *  <li>construit le script pour qsub</li>
     *  <li>upload le script qsub</li>
     *  <li>ajoute le script a qsub</li>
     * </ul>
     * 
     * Et : 
     * <ul>
     *  <li>lance le thread de control de la simulation</li>
     * </ul>
     */
    @Override
    public void simulate(SimulationService simulationService, SimulationItem simulationItem) throws RemoteException {

        // get simulation information for this launcher
        SimulationControl control = simulationItem.getControl();
        File simulationZip = simulationItem.getSimulationZip();
        String simulationPrescript = simulationItem.getGeneratedPrescriptContent();

        // check username
        if (StringUtils.isBlank(IsisFish.config.getSimulatorSshUsername())) {
            throw new RemoteException("Username is empty");
        }

        // start ssh session
        try {

            String simulationid = control.getId();

            // connection
            message(control, t("isisfish.simulation.remote.message.connection"));
            Session sshSession = getSSHSession();
            FTPClient ftp = getFtpClient();

            // upload simulation on server
            message(control, t("isisfish.simulation.remote.message.upload"));
            String simulationRemotePath = uploadSimulationIfNecessary(ftp, simulationItem, simulationid, simulationZip);

            // build du contenu du script
            message(control,t("isisfish.simulation.remote.message.waitingstart"));
            String simulationPreScriptPath = uploadPreScriptIfNecessary(ftp, control.getId(), simulationPrescript);
            
            // start simulation if necessary (multi jobs) ...
            String remoteResultZip = SSH_TEMP_PATH + getResultArchiveName(simulationid);
            startSimulation(sshSession, ftp, simulationItem, simulationid, simulationRemotePath, remoteResultZip, simulationPreScriptPath);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error(t("isisfish.error.simulation.remote.global"));
            }
            throw new RemoteException(t("isisfish.error.simulation.remote.global"), e);
        }
    }

    /**
     * {@inheritDoc}
     * 
     * Se connecte au serveur distant et télécharge les résultats de la
     * simulation.
     * 
     * Simulation must have been downloaded with
     * {@link #updateControl(SimulationService, SimulationControl)} before calling
     * this method.
     */
    @Override
    public SimulationStorage getSimulationStorage(SimulationService simulationService, SimulationControl control)
            throws RemoteException {

        // make sure that simulation has been downloaded by #updateControl()
        // before calling this method
        String simulationId = control.getId();

        SimulationStorage simulationStorage = SimulationStorage.getSimulation(simulationId);

        return simulationStorage;
    }

    /**
     * {@inheritDoc}
     * 
     * Se connecte au serveur distant et télécharge le fichier de control.
     * Injecte ensuite ce fichier dans le {@link SimulationControl}.
     * 
     * Essaye aussi de telecharger le fichier md5 de la simulation, et, s'il
     * est present, l'archive de résultat.
     * Supprime tous les fichiers de la simulations apres avoir télécharger les
     * résultats.
     */
    @Override
    public void updateControl(SimulationService simulationService, SimulationControl control) throws RemoteException {

        // make sure user is connected
        try {
            getSSHSession();
        } catch(JSchException e) {
            throw new RemoteException("Can't connect", e);
        }
        FTPClient ftpClient;
        try {
            ftpClient = getFtpClient();
        } catch (IOException e) {
            throw new RemoteException("Can't connect", e);
        }

        try {

            // can be because, simulation has not begun
            // or simulation is ended
            // try do download md5 control file

            // MD5 + SIMULATION zip file
            // FIXME echatellier 20140402 check not yet existing md5ControlFile and simulation ended
            File md5ControlFile = downloadResultsMD5File(ftpClient, control.getId());

            if (md5ControlFile != null) {
                control.setText(t("isisfish.simulation.remote.message.downloadresults"));

                String md5sum = FileUtils.readFileToString(md5ControlFile, StandardCharsets.UTF_8);

                if (log.isDebugEnabled()) {
                    log.debug("MD5 Control file have been downloaded : " + md5ControlFile.getAbsolutePath());
                }

                File resultArchiveFile = downloadResultsArchive(ftpClient, control, md5sum);

                if (resultArchiveFile != null) {

                    ZipUtil.uncompressFiltred(resultArchiveFile, SimulationStorage.getSimulationDirectory());

                    if (log.isDebugEnabled()) {
                        log.debug("Simulation imported : " + resultArchiveFile.getAbsolutePath());
                    }

                    resultArchiveFile.delete();

                    // read control from downloaded simulation
                    synchronized (control) {
                        SimulationStorage.readControl(control.getId(), control, "stop");
                    }

                    // files can be deleted
                    clearSimulationFiles(ftpClient, control);
                } else {
                    if (log.isWarnEnabled()) {
                        log.warn("Simulation zip download failed");
                    }
                }

                // remove temp file
                md5ControlFile.delete();
            }

            // INFORMATION file
            try {
                File infoFile = downloadSimulationInformationFile(ftpClient, control.getId());
                if (infoFile != null) {
                    if (log.isDebugEnabled()) {
                        log.debug("Information have been downloaded : " + infoFile.getAbsolutePath());
                    }


                    // s'il y a une exception,
                    // on dit juste que la simulation a eu une demande
                    // d'arret pour qu'elle s'arrete dans l'UI
                    Properties infoProperties = new Properties();

                    try (InputStream isInfoFile = new FileInputStream(infoFile)) {
                        infoProperties.load(isInfoFile);
                    }
                    if (!StringUtils.isEmpty(infoProperties.getProperty("exception"))) {
                        synchronized (control) {
                            control.setStopSimulationRequest(true);
                        }
                    }

                    // deleteTempFile
                    infoFile.delete();
                }
            } catch (IOException e) {
                // file doesn't exist
                if (log.isDebugEnabled()) {
                    // not add ,e plz :)
                    log.debug(t("Remote information file doesn't exists %s", e.getMessage()));
                }
            }
        } catch (ConnectException e) {
            if (log.isErrorEnabled()) {
                log.error("Can't connect", e);
            }
            // pour reset complementement la connexion et en ouvrir une nouvelle
            try {
                FtpManager.getInstance().closeFtpClient();
            } catch (IOException e2) {
                if (log.isErrorEnabled()) {
                    // not add ,e plz :)
                    log.error(t("Can't close ftp connection"), e2);
                }
            }
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                // not add ,e plz :)
                log.error(t("Can't download file"), e);
            }
        }
    }

    /**
     * Get opened ssh session or try to open a new one.
     * 
     * This method must synchronized.
     * 
     * @return opened ssh session.
     * @throws JSchException
     */
    protected synchronized Session getSSHSession() throws JSchException {

        if (sshSession == null || !sshSession.isConnected()) {
            sshSession = openSSHSession();
        }

        return sshSession;
    }

    /**
     * Connect to remote server throw SSH, and return session.
     * 
     * @return valid opened session
     * 
     * @throws JSchException
     */
    protected Session openSSHSession() throws JSchException {

        JSch jsch = new JSch();

        // extract connection infos
        String host = IsisFish.config.getSimulatorSshServer();
        String username = IsisFish.config.getSimulatorSshUsername();

        int port = 22; // by default, 22

        if (host.contains(":")) {
            String sPort = host.substring(host.indexOf(':') + 1);
            try {
                port = Integer.parseInt(sPort);
            } catch (NumberFormatException e) {
                if (log.isWarnEnabled()) {
                    log.warn(t("isisfish.error.simulation.remote.wrongportvalue",
                            sPort));
                }
            }
            host = host.substring(0, host.indexOf(':'));
        }

        if (log.isInfoEnabled()) {
            log.info(t("Try to log on %s@%s:%d", username, host, port));
        }

        // add ssh key
        boolean sshKeyUsed = false;
        File sshKey = IsisFish.config.getSSHPrivateKeyFilePath();
        if (sshKey.isFile() && sshKey.canRead()) {
            if (log.isInfoEnabled()) {
                log.info(t("Ssh key found '%s' will be used to connect to",
                        sshKey.getAbsoluteFile(), host));
            }
            jsch.addIdentity(sshKey.getAbsolutePath());
            sshKeyUsed = true;
        }
        else {
            if (log.isInfoEnabled()) {
                log.info(t("Can't read ssh key : %s", sshKey));
            }
        }

        Session session = jsch.getSession(username, host, port);

        // username and password will be given via UserInfo interface.
        SSHUserInfo ui = new SSHUserInfo();
        if (sshKeyUsed) {
            try {
                char[] passChars = SSHAgent.getAgent().getPassphrase(sshKey);
                if (passChars != null) {
                    String passphrase = String.valueOf(passChars);
                    ui.setPassphrase(passphrase);
                }
            } catch (InvalidPassphraseException e) {
                if (log.isWarnEnabled()) {
                    log.warn("Can't key passphrase for key", e);
                }
            }
        }
        session.setUserInfo(ui);
        session.connect(20000); // timeout

        // test here, if password has been asked to user
        if (session.isConnected() && sshKeyUsed && ui.getPassword() != null) {
            putSshKeyOnRemoteServer(session, sshKey);
        }
        return session;
    }

    /**
     * Close ssh session.
     * 
     * @param session session to close
     */
    protected void closeSSHSession(Session session) {
        if (session != null) {
            session.disconnect();
        }
    }

    /**
     * Open new FTP client connected to server and user loggued in.
     */
    protected FTPClient getFtpClient() throws IOException {
        return openFtpClient();
    }

    /**
     * Open new FTP client connected to server and user logged in.
     */
    protected FTPClient openFtpClient() throws IOException {

        // extract connection infos
        String host = IsisFish.config.getSimulatorFtpServer();
        int port = 21; // by default, 21
        if (host.contains(":")) {
            String sPort = host.substring(host.indexOf(':') + 1);
            try {
                port = Integer.parseInt(sPort);
            } catch (NumberFormatException e) {
                if (log.isWarnEnabled()) {
                    log.warn(t("isisfish.error.simulation.remote.wrongportvalue", sPort));
                }
            }
            host = host.substring(0, host.indexOf(':'));
        }

        // credential info
        String login = IsisFish.config.getSimulatorFtpLogin();
        String password = PasswordManager.getInstance().getPassword(String.format("%s@%s", login, host));

        FTPClient ftp = FtpManager.getInstance().getFtpClient(host, port, login, password);
        return ftp;
    }

    /**
     * Close ftp session.
     */
    protected void closeFTPClient(FTPClient ftpClient) throws IOException {
        FtpManager.getInstance().closeFtpClient();
    }

    /**
     * Add ssh key into $HOME/.ssh/authorized_keys file.
     * 
     * Just connect and do an "echo xx &gt;&gt; .ssh/authorized_keys"
     * 
     * @param session opened session
     * @param sshKey
     */
    protected void putSshKeyOnRemoteServer(Session session, File sshKey) {

        // get public key for argument private key file
        File publicKey = new File(sshKey.getAbsoluteFile() + ".pub");

        // command to :
        // - make ssh directory
        // - add key to authorized_keys

        // tested on bash/csh
        String command = "test -d .ssh||mkdir .ssh;echo \"%s\" >> .ssh/authorized_keys";

        try {
            // use usefull readLines from commons-io
            List<String> contents = FileUtils.readLines(publicKey, StandardCharsets.UTF_8);

            // only one line
            if (CollectionUtils.size(contents) == 1) {
                command = String.format(command, contents.get(0));

                if (log.isInfoEnabled()) {
                    log.info("Add key on remote authorized keys");
                }
                if (log.isDebugEnabled()) {
                    log.debug("command is : " + command);
                }

                SSHUtils.exec(session, command);
            }
        } catch (IOException | SSHException e) {
            if (log.isErrorEnabled()) {
                log.error(t("Error while uploading public key to remote serveur authorized_keys"), e);
            }
        }
    }

    /**
     * Upload simulation if necessary and always return the remote
     * simulation zip path to use.
     * 
     * @param ftpClient already open valid ftp session
     * @param simulationItem simulation item
     * @param simulationid simulation id
     * @param simulationFile simulation file to upload
     * 
     * @return remote file path or {@code null} if errors
     */
    protected String uploadSimulationIfNecessary(FTPClient ftpClient, SimulationItem simulationItem, String simulationid, File simulationFile)
            throws IOException {

        // first check that remote directory exists
        String remoteTemp = FTP_TEMP_PATH;
        String localSimulationZip;
        String remotePath;

        if (!simulationItem.isStandaloneSimulationZip()) {
            // get simulation file path for each simulation...
            String shortSimulationId = simulationid.substring(0, simulationid.lastIndexOf('_'));
            localSimulationZip = "simulation-" + shortSimulationId + "-preparation.zip";
            remotePath = remoteTemp + localSimulationZip;

            // ...but perform real upload only for frist one !
            if (simulationItem.getSimulationNumber() == 0) {
                uploadSimulation(ftpClient, remoteTemp, remotePath, simulationFile);
            }
        } else {
            // not standalone, name always different
            localSimulationZip = "simulation-" + simulationid + "-preparation.zip";
            remotePath = remoteTemp + localSimulationZip;
            // perform upload ech time
            uploadSimulation(ftpClient, remoteTemp, remotePath, simulationFile);
        }

        return SSH_TEMP_PATH + localSimulationZip;
    }

    /**
     * Perform simulation upload.
     * 
     * Create remote temp directory if not exists.
     * 
     * @param ftpClient already open valid ftp session
     * @param remoteDirectory
     * @param remoteSimulationZipPath
     */
    protected void uploadSimulation(FTPClient ftpClient, String remoteDirectory, String remoteSimulationZipPath, File simulationFile) throws IOException {

        synchronized (ftpClient) {
            // make sure directoy exists
            ftpClient.makeDirectory(remoteDirectory);

            try (InputStream is = new BufferedInputStream(new FileInputStream(simulationFile))) {
                ftpClient.storeFile(remoteSimulationZipPath, is);
            }
        }
    }

    /**
     * Download simulation zip results.
     * 
     * MD5 control check sum if done, return null, if checkSum fail.
     * 
     * File if configured to auto delete at JVM shutdown.
     *
     * @throws IOException if download fail (can happen if remote file doesn't exist)
     */
    protected File downloadResultsArchive(FTPClient ftpClient, SimulationControl simulationControl, String md5sum)
            throws IOException {

        File localFile = IsisFileUtil.createTempFile("simulation-results", ".zip");
        localFile.deleteOnExit();

        if (log.isDebugEnabled()) {
            log.debug("Downloading results in " + localFile.getAbsolutePath());
        }

        // build remote file path
        String simulationId = simulationControl.getId();
        String remoteFile = FTP_TEMP_PATH + getResultArchiveName(simulationId);

        synchronized (ftpClient) {
            long size = FtpManager.getFileSize(ftpClient, remoteFile);
            ftpClient.setCopyStreamListener(new ControlCopyStreamListener(simulationControl, size));
            try (OutputStream out = new BufferedOutputStream(new FileOutputStream(localFile))) {
                ftpClient.retrieveFile(remoteFile, out);
            } catch (IOException e) {
                localFile.delete();
                throw e;
            } finally {
                ftpClient.setCopyStreamListener(null);
            }
        }

        if (!StringUtils.isEmpty(md5sum)) {
            String localMd5 = StringUtil.asHex(MD5InputStream.hash(new BufferedInputStream(new FileInputStream(localFile))));
            if (!localMd5.equals(md5sum)) {
                if (log.isWarnEnabled()) {
                    log.warn("Warning md5 checksum failed (got " + localMd5 + ", expected : " + md5sum + ")");
                }
                localFile.delete();
                localFile = null;
            }
        }

        return localFile;
    }

    /**
     * Download remote simulation md5 control file and store its content into temp
     * file.
     * 
     * @param ftpClient valid opened ftp session
     * @param simulationId id de la simulation
     * @return downloaded temp file (file have to be manually deleted)
     * @throws IOException
     */
    protected File downloadResultsMD5File(FTPClient ftpClient, String simulationId) throws IOException {

        // build remote file path
        // this path is not configurable
        // same as fr.ifremer.isisfish.actions.SimulationAction.simulateRemotellyWithPreScript(String, File, File, File)
        // defined by org.nuiton.util.ZipUtil.compressFiles(File, File, Collection<File>, boolean)
        String remoteFile = FTP_TEMP_PATH + getResultArchiveName(simulationId) + ".md5";

        // local tmp file
        File localFile = IsisFileUtil.createTempFile(simulationId, ".md5");
        File resultFile = null;
        synchronized (ftpClient) {
            try (OutputStream out = new BufferedOutputStream(new FileOutputStream(localFile))) {
                boolean result = ftpClient.retrieveFile(remoteFile, out);
                if (result) {
                    resultFile = localFile;
                }
            } catch (IOException e) {
                localFile.delete();
                throw e;
            }
        }

        return resultFile;
    }

    /**
     * Download remote information.
     *
     * @param ftpClient valid opened ftp session
     * @param simulationId id de la simulation
     * @return downloaded temp file (file have to be manually deleted)
     * @throws IOException
     */
    protected File downloadSimulationInformationFile(FTPClient ftpClient, String simulationId) throws IOException {

        // build remote file path
        String remoteFile = FTP_TEMP_PATH + "simulation-" + simulationId + "information.txt";

        // local tmp file
        File localFile = IsisFileUtil.createTempFile(simulationId, ".md5");
        File resultFile = null;
        synchronized (ftpClient) {
            try (OutputStream out = new BufferedOutputStream(new FileOutputStream(localFile))) {
                boolean result = ftpClient.retrieveFile(remoteFile, out);
                if (result) {
                    resultFile = localFile;
                }
            } catch (IOException e) {
                localFile.delete();
                throw e;
            }
        }

        return resultFile;
    }

    /**
     * Remove all {@code $ISIS-TMP/simulation-$id-*} files on datarmor.
     *
     * @param ftpClient valid opened ssh session
     * @param control simulation control
     */
    protected void clearSimulationFiles(FTPClient ftpClient, SimulationControl control) throws IOException {

        control.setText(t("isisfish.simulation.remote.message.deletingfiles"));

        // execute rm -f "isis-tmp/simulation-$id-"*
        // on remote. Note * outside quotes !!!
        String simulationId = control.getId();

        synchronized (ftpClient) {
            // delete huge files (all others remains, but scratch is cleared each 15 days, and 10Tb size)
            ftpClient.deleteFile(FTP_TEMP_PATH + "simulation-" + simulationId + "-output.txt");
            ftpClient.deleteFile(FTP_TEMP_PATH + getResultArchiveName(simulationId));
            ftpClient.deleteFile(FTP_TEMP_PATH + "simulation-" + simulationId + "-preparation.zip");
        }
    }

    /**
     * Upload script on remote server.
     *
     * @param simulationScript file to upload
     */
    protected String uploadSimulationScript(FTPClient ftpClient, String simulationid, String simulationScript) throws IOException {

        // remote temp directory should have been created
        // by #uploadSimulation(Session, String)

        // always rename uploaded script to "simulation-$id-script.seq"
        String remoteFileName = "simulation-" + simulationid + "-script.seq";

        synchronized (ftpClient) {
            try (InputStream is = new ByteArrayInputStream(simulationScript.getBytes(StandardCharsets.UTF_8))) {
                ftpClient.storeFile(FTP_TEMP_PATH + remoteFileName, is);
            }
        }

        return SSH_TEMP_PATH + remoteFileName;
    }

    /**
     * Get remote simulation zip name.
     * 
     * @param simulationId simulation id
     */
    protected String getResultArchiveName(String simulationId) {
        String remoteName = "simulation-" + simulationId + "-result.zip";
        return remoteName;
    }

    /**
     * Upload pre script on remote server.
     * 
     * Return path if uploaded or null if no upload needed.
     * 
     * @param ftpClient valid opened ftp session
     * @param simulationId simulation id
     * @param simulationPreScript script content
     *
     * @throws IOException if upload fail
     */
    protected String uploadPreScriptIfNecessary(FTPClient ftpClient, String simulationId, String simulationPreScript)
            throws IOException {

        // if there is no pre script, do nothings
        if (StringUtils.isEmpty(simulationPreScript)) {
            return null;
        }

        // always rename simulation prescript to "simulation-$id-prescript.bsh"
        String prescriptName = "simulation-" + simulationId + "-prescript.bsh";

        // remote temp directory should have been created
        // by #uploadSimulation(Session, String)
        String remotePath = FTP_TEMP_PATH + prescriptName;

        synchronized (ftpClient) {
            try (InputStream is = new ByteArrayInputStream(simulationPreScript.getBytes(StandardCharsets.UTF_8))) {
                ftpClient.storeFile(remotePath, is);
            }
        }

        return SSH_TEMP_PATH + prescriptName;
    }

    /**
     * Start simulation if necessary.
     * 
     * Current simulation can be started later with a PBS multi job.
     * 
     * @param session ssh session
     * @param simulationItem simulation item (needed for additionnal info, simulation, number, indenpendant, etc...)
     * @param simulationid simulation id
     * @param simulationRemoteZipPath simulation preparation (input) zip path
     * @param remoteResultZip simulation result (output) zip path
     * @param simulationPreScriptPath simulation prescript
     * 
     * @throws Exception 
     */
    protected void startSimulation(Session session, FTPClient ftpClient, SimulationItem simulationItem, String simulationid,
                                   String simulationRemoteZipPath, String remoteResultZip, String simulationPreScriptPath) throws Exception {

        // standalone simulation
        // no question, generate script, launch it each time
        if (simulationItem.isStandaloneSimulation()) {
            // single simulation
            String simulationPSBScript = getLaunchSimulationScriptContent(simulationid,
                    simulationRemoteZipPath, true, remoteResultZip, simulationPreScriptPath, false);
            String scriptRemotePath = uploadSimulationScript(ftpClient, simulationid, simulationPSBScript);

            sendStartSimulationRequest(session, simulationid, scriptRemotePath, -1);
        }
        else {
            // standalone, on do it for last simulation
            if (simulationItem.isLastSimulation()) {
                String shortSimulationId = simulationid.substring(0, simulationid.lastIndexOf('_'));

                if (log.isDebugEnabled()) {
                    log.debug("Last simulation start requested, send multijob start request for " + shortSimulationId);
                }

                // multiple simulation disabled for single simulation in plan
                boolean multipleSimulation = simulationItem.getSimulationNumber() > 0;

                // multiples jobs simulation
                String simulationPSBScript = getLaunchSimulationScriptContent(shortSimulationId,
                        simulationRemoteZipPath, simulationItem.isStandaloneSimulationZip(), remoteResultZip, simulationPreScriptPath, multipleSimulation);
                String scriptRemotePath = uploadSimulationScript(ftpClient, shortSimulationId, simulationPSBScript);

                // file will be named with shortSimulationId (instead of simulationId)
                sendStartSimulationRequest(session, shortSimulationId, scriptRemotePath, simulationItem.getSimulationNumber());
            }
            else {
                if (log.isDebugEnabled()) {
                    log.debug("Current simulation is not last simulation in pool, skip start");
                }
            }
        }
    }

    /**
     * Retourne un fichier temporaire contenant le script de lancement de
     * simulation.
     * 
     * Le fichier temporaire est configuré pour se supprimer tout seul.
     * 
     * @param simuationId id de la simulation
     * @param simulationZip zip de la simulation
     * @param standaloneZip standalone simulation zip
     * @param preScriptPath simulation pre script path (can be null)
     * @param multipleSimulationScript if {@code true} build a multijob simulation script
     * 
     * @return un Fichier temporaire ou {@code null} en cas d'exception
     * 
     * @throws IOException if can't build script
     */
    protected String getLaunchSimulationScriptContent(String simuationId,
            String simulationZip, boolean standaloneZip, String simulationResultZip, String preScriptPath, boolean multipleSimulationScript) throws IOException {
        String fileContent = getSimulationScriptLaunchContent(
                QSUB_SCRIPT_TEMPLATE, simuationId, simulationZip, standaloneZip, simulationResultZip, preScriptPath, multipleSimulationScript);

        return fileContent;
    }

    /**
     * Utilise freemarker pour recuperer le contenu du script.
     * 
     * Remplace aussi la variable $simulation du template.
     * 
     * @param templateName url du template
     * @param simuationId id de la simulation
     * @param simulationZip zip de la simulation
     * @param standaloneZip standalone simulation zip
     * @param simulationZipResult zip resultat de la simulation
     * @param preScriptPath simulation pre script path (can be null)
     * @param multipleSimulationScript if {@code true} build a multijob simulation script
     * 
     * @throws IOException if can't get script content
     */
    protected String getSimulationScriptLaunchContent(String templateName,
            String simuationId, String simulationZip, boolean standaloneZip,
            String simulationZipResult, String preScriptPath, boolean multipleSimulationScript)
            throws IOException {

        String scriptContent;

        // test null values for prescript
        String remotePreScript = preScriptPath;
        if (remotePreScript == null) {
            remotePreScript = "";
        }

        try {
            // get template
            Template template = freemarkerConfiguration.getTemplate(templateName);

            // context values
            Map<String, Object> root = new HashMap<>();
            root.put("ISIS_INSTALL", IsisFish.config.getSimulatorSshIsisHome());
            root.put("ISIS_TEMP", SSH_TEMP_PATH);
            root.put("JAVA_BIN_PATH", IsisFish.config.getSimulatorSshJavaPath());
            root.put("JAVA_MEMORY", IsisFish.config.getSimulatorSshMaxMemory());
            root.put("SIMULATION_ID", simuationId);
            root.put("SIMULATION_ZIP", simulationZip);
            root.put("SIMULATION_STANDALONE_ZIP", standaloneZip);
            root.put("SIMULATION_RESULT_ZIP", simulationZipResult);
            root.put("SIMULATION_PRESCRIPT", remotePreScript);
            root.put("QSUB_MULTIPLE_JOB", multipleSimulationScript);

            // process template
            Writer out = new StringWriter();
            template.process(root, out);
            out.flush();
            scriptContent = out.toString();

        } catch (TemplateException e) {
            if (log.isErrorEnabled()) {
                log.error(t("Process template error"), e);
            }

            throw new IOException(t("Process template error"), e);
        }

        return scriptContent;
    }

    /**
     * Add script in remote qsub queue.
     * 
     * @param session valid opened session
     * @param simulationId simulation id (short version for a multiple job)
     * @param scriptRemotePath remote script path
     * @param lastSimulationNumber if {@code >=0} start a multiple pbs job form 0 to {@code lastSimulationNumber}
     * 
     * @throws SSHException if call fail
     */
    protected void sendStartSimulationRequest(Session session, String simulationId, String scriptRemotePath, int lastSimulationNumber)
            throws SSHException {

        // command to :
        // - add script in qsub queue
        String command = "qsub";
        command += " -o \"" + SSH_TEMP_PATH + "simulation-" + simulationId + "-pbs.out\"";
        command += " -e \"" + SSH_TEMP_PATH + "simulation-" + simulationId + "-pbs.err\"";

        // add qsub options 
        String qsubOptions = IsisFish.config.getSimulatorSshPbsQsubOptions();
        if (StringUtils.isNotEmpty(qsubOptions)) {
            command += " " + qsubOptions;
        }

        // multi job specific
        if (lastSimulationNumber > 0 ) {
            command+= " -J 0-" + lastSimulationNumber;
        }

        // end with squb script path and redirect id into file (to stop it later)
        String remoteFilenameId = SSH_TEMP_PATH + "simulation-" + simulationId + "-pbs.id";
        command += " \"" + scriptRemotePath + "\"|tee \"" + remoteFilenameId + "\"";

        if (log.isDebugEnabled()) {
            log.debug("Send qsub job starting command : " + command);
        }

        Writer output = new StringWriter();
        int exit = SSHUtils.exec(session, command, output);

        if (exit != 0) {
            throw new SSHException(t("Command '%s' fail to execute", command));
        }

        String out = output.toString();
        // multiple jobs id are like : 78600[].service4 ou 6199230.service0
        if (out.trim().matches("\\d+(\\[\\])?\\.\\w+") && log.isInfoEnabled()) {
            log.info("Job submitted with job id : " + out);
        }
    }

    /**
     * Send qdel request on job.
     * 
     * @param session valid opened session
     * @param simulationId simulation id
     * 
     * @throws SSHException if call fail
     */
    protected void sendStopSimulationRequest(Session session, String simulationId)
            throws SSHException {

        // command to :
        String remoteFilenameId = SSH_TEMP_PATH + "simulation-" + simulationId + "-pbs.id";
        String command = "qdel `cat \"" + remoteFilenameId + "\"`";

        // and delete simulation
        //command += " && rm -rf \"" + IsisFish.config.getSimulatorSshDataPath() + "/simulations/" + simulationId + "\"";

        if (log.isDebugEnabled()) {
            log.debug("Send stop request : " + command);
        }

        SSHUtils.exec(session, command);

        // can fail, already stopped
        /*if (exit != 0) {
            throw new SSHException(t("Command '%s' fail to execute", command));
        }*/
    }
}
