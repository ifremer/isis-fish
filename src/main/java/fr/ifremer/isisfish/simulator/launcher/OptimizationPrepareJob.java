package fr.ifremer.isisfish.simulator.launcher;

/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 - 2018 Ifremer, Code Lutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.isisfish.config.IsisConfig;

import static org.nuiton.i18n.I18n.t;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import fr.ifremer.isisfish.logging.SimulationLoggerUtil;
import fr.ifremer.isisfish.simulator.*;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixND;

import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.entities.Observation;
import fr.ifremer.isisfish.export.ExportInfo;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.zip.GZIPInputStream;
import org.apache.commons.lang3.StringUtils;

/**
 * Permet de generer l'enchainement des simulations d'optimisation.
 * Gere les differentes generation (plusieurs simulations par generation)
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class OptimizationPrepareJob implements Runnable, SimulationJob.PostAction {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private final Log log = LogFactory.getLog(OptimizationPrepareJob.class);

    protected SimulationService simulationService;
    protected OptimizationContextInternal optimizationContext;
    protected SimulationJob job;
    protected String id;
    protected SimulationControl control;
    protected SimulationParameter param;
    protected Objective objective;
    protected Optimization optimization;
    
    /** nombre de simulation faite ou en cours */
    protected int simNumber = 0;
    /** nombre de simulation faite */
    protected int done = 0;
    /** true if exception occure in one simulation */
    protected boolean exception = false;

    public OptimizationPrepareJob(SimulationService simulationService, SimulationJob job) {
        this.simulationService = simulationService;
        this.job = job;

        id = job.getItem().getControl().getId();
        control = job.getItem().getControl();
        param = job.getItem().getParameter();

        objective = param.getObjective();
        optimization = param.getOptimization();
        optimizationContext = new OptimizationContextInternal(id, param, job);
    }

    @Override
    public void run() {
        try {
            optimization.init(optimizationContext);
            optimization.firstSimulation(optimizationContext);
            
            List<SimulationStorage> sims = optimizationContext.clearNextSimulation();
            while (!exception && !control.isStopSimulationRequest()
                    && CollectionUtils.isNotEmpty(sims)) {
                List<String> simulationNames = new ArrayList<>(sims.size());
                for (SimulationStorage s : sims) {
                    if (!exception && !control.isStopSimulationRequest()) {
                        // on renomme en la version final
                        String simId = s.getName().replaceFirst(OptimizationContext.TEMP_PREFIX, "");
                        simulationNames.add(simId);

                        File zip = s.createZip();
                        // on ferme et on supprime les simulations temporaire utilisee pour la configuration
                        s.closeStorage();
                        s.delete(false);

                        SimulationParameter childParam = s.getParameter();
                        int childNumber = childParam.getSimulationPlanNumber();
                        SimulationControl childControl = new SimulationControl(simId);
                        SimulationItem item = new SimulationItem(childControl, childParam);
                        // FIXME set to true for now, but must be changed to run
                        // simulation optimisation by generation (not one by one)
                        item.setStandaloneSimulation(true);
                        item.setSimulationNumber(childNumber);
                        item.setSimulationZip(zip);

                        SimulationJob subJob = new SimulationJob(simulationService, job, item, job.getPriority());
                        subJob.setLauncher(job.getLauncher());

                        subJob.addPostAction(this);
                        simulationService.submitSubJob(subJob);

                        simNumber++;
                    }
                }

                control.setProgressMax(simNumber);

                // wait until wall simulation for this generation are done
                while(!exception && !control.isStopSimulationRequest()
                        && simNumber > done) {
                    Thread.sleep(2000);
                }

                // FIXME echatellier 20140418 exception is always false, it's value never changes
                if (!exception && !control.isStopSimulationRequest()) {
                    // tous les simulations ont ete faites, on recreer les storages avec les nouveaux nom en local
                    // /!\ on modifie la collection qu'on nous a envoyer, on utilise un effet de bord
                    // le probleme est que la simulation qui etait precedement dans la collection n'est plus valide
                    sims.clear();
                    for (String name : simulationNames) {
                        sims.add(SimulationStorage.getSimulation(name));
                    }

                    optimization.endSimulation(optimizationContext);
                    optimizationContext.incGenerationNumber();
                    // close current generation simulations
                    for (SimulationStorage sim : sims) {
                        sim.closeStorage();
                        sim.closeMemStorage();
                    }
                    optimization.nextSimulation(optimizationContext);
                    sims = optimizationContext.clearNextSimulation();
                }
            }

            if (!exception && !control.isStopSimulationRequest()) {
                optimization.finish(optimizationContext);
            }

        } catch (Exception eee) {
            // add manual log
            // we are in a thread, IsisFishRuntimeException is displayed
            // outside a log
            if (log.isErrorEnabled()) {
                log.error(t("isisfish.error.evaluate.optimization.script"), eee);
            }
            throw new IsisFishRuntimeException(t("isisfish.error.evaluate.optimization.script"), eee);
        } finally {
            // on enleve le master plan des simulations en cours, vu que
            // toutes les simu sont terminees (correctement ou echouee)
            simulationService.fireStopEvent(this.job);
        }
    }

    @Override
    public void finished(SimulationJob job, SimulationStorage sim) {
        if (objective != null) {

            // appel de la methode d'objectif
            try {
                SimulationLoggerUtil.setPlanLoggingContext(id);
                List<MatrixND> exports = new ArrayList<>();
                List<MatrixND> observations = new ArrayList<>();

                // recuperation des exports et observations
                Map<ExportInfo, Observation> exportsObservations = sim.getParameter().getOptimizationExportsObservations();
                for (Map.Entry<ExportInfo, Observation> exportObservation : exportsObservations.entrySet()) {
                    ExportInfo export = exportObservation.getKey();
                    Observation observation = exportObservation.getValue();

                    File rootDirectory = sim.getDirectory();
                    File resultExportDirectory = SimulationStorage.getResultExportDirectory(rootDirectory);
                    String fullFilename = export.getExportFilename() + export.getExtensionFilename();
                    File exportFile = new File(resultExportDirectory, fullFilename);
                    
                    if (!exportFile.isFile() && !StringUtils.endsWithIgnoreCase(fullFilename, IsisConfig.COMPRESSION_EXTENSION)) {
                        // file doesn't exist, perhaps gzipped version exist, we try with .gz
                        fullFilename += ".gz";
                        exportFile = new File(resultExportDirectory, fullFilename);
                    }
                    
                    Reader reader;
                    if (StringUtils.endsWithIgnoreCase(fullFilename, IsisConfig.COMPRESSION_EXTENSION)) {
                        reader = new BufferedReader(new InputStreamReader(
                                new GZIPInputStream(new FileInputStream(exportFile)), StandardCharsets.UTF_8));
                    } else {
                        reader = new BufferedReader(new FileReader(exportFile));
                    }

                    // read matrix file
                    MatrixND exportMatrix = null;
                    if (observation.getValue() != null) {

                        // on fait ici un clone, car on ne connait pas les dimensions de la
                        // matrix à importer; cela n'a peut être aucun sens.
                        exportMatrix = observation.getValue().clone();

                        // import
                        exportMatrix.importCSV(reader, new int[] {0, 0});
                    }

                    exports.add(exportMatrix);
                    observations.add(observation.getValue());
                }

                double obj = objective.eval(optimizationContext, sim, exports, observations);
                sim.setObjective(obj);
            } catch (IOException eee) {
                throw new IsisFishRuntimeException("Can't evaluate objective", eee);
            } finally {
                SimulationLoggerUtil.clearPlanLoggingContext();
            }
        }
        done++;
        control.setProgress(done);
    }

    @Override
    public void exception(SimulationJob job, Throwable eee) {
        // il y a une simulation d'echoue, on ne fait pas les suivantes
        // cela n'impacte pas les plan independant puisque toutes les
        // simulation on deja ete generee
        exception = true;
    }

}
