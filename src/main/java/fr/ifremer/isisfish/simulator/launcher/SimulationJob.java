/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2011 Ifremer, Code Lutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator.launcher;

import static org.nuiton.i18n.I18n.t;

import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Set;

import fr.ifremer.isisfish.logging.SimulationLoggerUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;

import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.simulator.SimulationControl;
import fr.ifremer.isisfish.simulator.SimulationParameter;

/**
 * Classe responsable de la simulation d'un {@link SimulationItem}. Pour cela
 * il utilise le {@link SimulatorLauncher}. Si la simulation echoue
 * a cause d'une RemoteException alors le job est resoumis dans la queue
 * de simulation par l'appel de la methode
 * {@link SimulationService#reportError}.
 *
 * @author poussin
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class SimulationJob implements Runnable, Comparable<SimulationJob> {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    protected static Log log = LogFactory.getLog(SimulationJob.class);

    transient protected String id;
    /** l'ensemble des post actions a effectuer pour ce job */
    protected Set<PostAction> postActions = new HashSet<>();
    /** Le {@link SimulationService} dans lequel a ete cree ce job */
    protected SimulationService simulationService;
    /** si non null contient le {@link SimulationJob} qui a genere ce job, ca
     * veut dire que ce job est du a un plan de simulation*/
    protected SimulationJob parentJob;
    /** item contenant les infos de la simulation */
    protected SimulationItem item;
    /** la priorite de cet item dans la queue */
    protected int priority;
    /** Le launcher a utiliser pour simuler cet item */
    protected SimulatorLauncher launcher;
    /** Set it to true to restart simulation checking without restarting reel simulation */
    protected boolean onlyCheckControl;
    
    public SimulationJob(SimulationService simulationService,
            SimulationItem item, int priority) {
        this(simulationService, null, item, priority);
    }

    public SimulationJob(SimulationService simulationService,
            SimulationJob parentJob, SimulationItem item, int priority) {
        this.simulationService = simulationService;
        this.parentJob = parentJob;
        this.item = item;
        this.priority = priority;
    }

    public void addPostAction(PostAction postAction) {
        this.postActions.add(postAction);
    }

    public void removePostAction(PostAction postAction) {
        this.postActions.remove(postAction);
    }

    public Set<PostAction> getPostActions() {
        return this.postActions;
    }

    public String getId() {
        if (id == null) {
            id = getItem().getControl().getId();
        }
        return id;
    }

    /** 
     * demande l'annulation/arret de ce job. Si ce job n'etait pas encore actif
     * un {@link SimulationServiceListener#simulationStop} est leve. Sinon
     * il le sera lorsque la simulation se sera convenablement arretee.
     * <p>
     * Dans tous les cas une demande d'arret sur le control de la simulation
     * est fait.
     */
    public void stop() {
        // on essaie d'enlever ce job de la queue, au cas ou il ne serait pas
        // encore lance
        if (simulationService.cancel(this)) {
            // on a pu annuler avant le lancement, on notify un stop puisqu'on
            // ne passera jamais dans le run()
            simulationService.fireStopEvent(this);
        }
        item.getControl().setStopSimulationRequest(true);
    }
    
    /**
     * Resoumet un job.
     */
    public void restart() {
        simulationService.restart(this);
    }

    public SimulationJob getParentJob() {
        return parentJob;
    }

    public SimulationItem getItem() {
        return item;
    }

    public void setLauncher(SimulatorLauncher launcher) {
        this.launcher = launcher;
    }

    public SimulatorLauncher getLauncher() {
        return launcher;
    }

    public int getPriority() {
        return priority;
    }

    /**
     * @param onlyCheckControl the onlyCheckControl to set
     */
    public void setOnlyCheckControl(boolean onlyCheckControl) {
        this.onlyCheckControl = onlyCheckControl;
    }

    /**
     * L'ordre depend :
     *  - de la priorite
     *  - si le nom fini par un chiffre :
     *    - du nom avant le chiffre
     *    - du chiffre
     *  - sinon du nom
     */
    @Override
    public int compareTo(SimulationJob o) {
        int result = this.priority - o.priority;
        if (result == 0) {
            if (this.getId().matches(".*_\\d+") && o.getId().matches(".*_\\d+")) {
                String firstString = this.getId().substring(0, this.getId().lastIndexOf("_"));
                String secondString = o.getId().substring(0, o.getId().lastIndexOf("_"));
                result = firstString.compareTo(secondString);
                if (result == 0) {
                    int firstNumber = Integer.parseInt(this.getId().substring(this.getId().lastIndexOf("_") + 1));
                    int secondNumber = Integer.parseInt(o.getId().substring(o.getId().lastIndexOf("_") + 1));
                    result = firstNumber - secondNumber;
                }
            }
            if (result == 0) {
                result = this.getId().compareTo(o.getId());
            }
        }
        return result;
    }

    /**
     * Fait la simulation. La simulation en elle meme est delegue au 
     * {@link SimulatorLauncher}. Le travail restant ici est le nettoyage,
     * la gestion des erreurs ou l'iteration s'il sagit de plan de simulation
     * dependant.
     */
    public void run() {
        try {
            SimulationControl control = item.getControl();
            String id = control.getId();
            if (control.isStopSimulationRequest() || (getParentJob() != null && getParentJob().getItem().getControl().isStopSimulationRequest())) {
                log.info(t("Not start simulation %s because user ask stop", id));
                return;
            }
            SimulationParameter param = item.getParameter();
            if (log.isInfoEnabled()) {
                log.info("Start simulation: " + id);
            }
            simulationService.fireStartEvent(this);

            if (!onlyCheckControl && getParentJob() == null && param.getUseSimulationPlan() && !param.isIndependentPlan()) {
                // on est sur un plan de simulation dependant, il faut generer les
                // simulation les unes apres les autres
                try {
                    SimulationLoggerUtil.setPlanLoggingContext(id);
                    SimulationPlanPrepareJob i = new SimulationPlanPrepareJob(simulationService, this);
                    while (!control.isStopSimulationRequest() && i.hasNext()) {
                        log.info(t("Generate next simulation"));
                        SimulationJob subjob = i.next();
                        subjob.setLauncher(getLauncher());
                        // c'est bloquant seulement le temps du lancement
                        // (envoie sur datarmor par exemple)
                        // l'appel peut ensuite passer alors que la simulation
                        // n'a pas effectivement demarré
                        subjob.run();

                        // FIXME temp fix les thread des launchers
                        // ne sont plus bloquants
                        // on bloque le thread par un sleep
                        // tant que le sous thread n'est pas fini
                        SimulationItem subItem = subjob.getItem();
                        SimulationControl subControl = subItem.getControl();
                        do {
                            Thread.sleep(2000);

                            // on boucle tant que:
                            // - la simulation n'a pas été arreté
                            // - la simulation n'est pas a 0
                            // - la simulation n'est aps à la fin
                            // - la simulation n'existe pas localement
                        } while (!subControl.isStopSimulationRequest() &&
                                (subControl.getProgress() == 0 || subControl.getProgress() < subControl.getProgressMax()
                                        || !SimulationStorage.exists(subControl.getId())));

                        // FIXME on fait manuellement le post simulation pour
                        // plans dépendant
                        if (!subControl.isStopSimulationRequest()) {
                            SimulationStorage simulation = subjob.getLauncher().getSimulationStorage(simulationService, subControl);
                            i.finished(subjob, simulation);
                            try {
                                simulation.closeStorage();
                                simulation.closeMemStorage();
                            } catch (TopiaException ex) {
                                if (log.isErrorEnabled()) {
                                    log.error("Can't close simulation", ex);
                                }
                            }
                        }
                    }
                } finally {
                    SimulationLoggerUtil.clearPlanLoggingContext();
                }
            } else if (!onlyCheckControl && getParentJob() == null
                    && param.getUseOptimization()) {
                // on est sur une optimisation, il faut faire toutes les simulations demandees
                try {
                    SimulationLoggerUtil.setPlanLoggingContext(id);
                    OptimizationPrepareJob optiPreJob = new OptimizationPrepareJob(simulationService, this);
                    optiPreJob.run();
                } finally {
                    SimulationLoggerUtil.clearPlanLoggingContext();
                }
            } else {
                // on est sur une simple simulation, ou le resultat d'un plan

                // Dans le cas d'un simulation simple seulement
                // on mémorise la simulation comme "lancée" et
                // devant être re monitorée au lancement d'isis
                // FIXME try to remove static call
                SimulationMonitor.getInstance().simulationStart(this);

                //SimulationStorage simulation = null;
                if (!onlyCheckControl) {
                    // Warn : blocker or non blocker call depending on launcher
                    launcher.simulate(simulationService, item);
                }
            }

        } catch (Throwable eee) {
            log.warn(t("Can't simulate %s", item.getControl().getId()), eee);
            if (eee instanceof RemoteException) {
                simulationService.reportError(getLauncher(), this);
            } else {
                // on ne le fait pas pour RemoteException, car la simulation
                // va etre resoumise
                for (PostAction action : postActions) {
                    action.exception(this, eee);
                }
            }
        } /*finally {
            simulationService.fireStopEvent(this);
        } */
    }

    /**
     * Interface permettant d'implanter des actions a faire apres la simulation.
     * Ces actions ne se declenchent pas pour les job de plan de simulation pere.
     */
    public interface PostAction {
        
        /**
         * Appelé lorsque la simulation s'arrete normalement.
         * 
         * @param job le job qui a fait la simulation
         * @param sim la simulation qui vient d'etre faite
         */
        void finished(SimulationJob job, SimulationStorage sim);

        /**
         * Appeler lorsque la simulation a echoué.
         * 
         * @param job le job qui a fait la simulation
         * @param eee l'exception qui a ete levee
         */
        void exception(SimulationJob job, Throwable eee);
    }

}
