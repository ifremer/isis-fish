package fr.ifremer.isisfish.simulator;

/*
 * #%L
 * ISIS-Fish
 * %%
 * Copyright (C) 2014 - 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.simulator.sensitivity.Factor;
import java.util.List;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public interface OptimizationContext extends SimulationPlanContext {

    String TEMP_PREFIX = "__tmp__";

    /**
     * Return the current generation.
     * 0 for no generation (in firstGeneration)
     * 1 for one generation simulation
     * ...
     *
     * @return
     */
    int getCurrentGeneration();

    /**
     * Return simulation for generation n
     * @param n generation number
     * @return
     */
    List<SimulationStorage> getGeneration(int n);

    /**
     * Return last generation simulations.
     * @return
     */
    List<SimulationStorage> getLastSimulations();

    /**
     * Return next generation, in endGeneration, this method return all time
     * empty list.
     * @return
     */
    List<SimulationStorage> getNextSimulations();

    /**
     * Return new simulation. This new simulation is automaticaly added to
     * next simulation.
     *
     * @return new Simulation
     */
    SimulationStorage newSimulation();

    /**
     * Create simulation. This new simulation is automatically added to
     * next simulations. Database will be modified with factors in parameters
     *
     * @param factors factors used to modify simulation
     * @return simulation modified with factors
     */
    SimulationStorage newSimulation(Factor... factors);

}
