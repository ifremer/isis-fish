/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator;

import java.util.HashMap;
import java.util.Map;

import fr.ifremer.isisfish.datastore.SimulationStorage;

/**
 * Simulation plan context.java
 * 
 * Replace old {@code AnalysePlanContext}.
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class SimulationPlanContextInternal implements SimulationPlanContext {

    /** Simulation id. */
    protected String id;

    /** Simulation plan number. */
    protected int number = 0;

    /** Simulation parameters. */
    protected SimulationParameter param;

    protected Historic historic;

    /** Additional context values. */
    protected Map<String, Object> values = new HashMap<>();

    public SimulationPlanContextInternal(String id, SimulationParameter param) {
        this.id = id;
        this.param = param;
        this.historic = new Historic(id);
    }

    /**
     * Get id.
     * 
     * @return the id
     */
    @Override
    public String getId() {
        return id;
    }

    /**
     * Get current simulation plan simulation count.
     * 
     * Warning, in after simulation, refer to generated simulation count,
     * not ended simulation number.
     * Depreciated, but no remove this method, it's just to not be used
     * by user script.
     * 
     * @return the number
     * 
     * @deprecated use {@code nextSimulation.getParameter().getSimulationPlanNumber()}
     */
    @Override
    public int getNumber() {
        return number;
    }

    /**
     * Must be call when new simulation is generated from plan.
     */
    public void incNumber() {
        number++;
    }

    /**
     * Get simulation parameters.
     * 
     * @return simulation parameters
     */
    @Override
    public SimulationParameter getParam() {
        return param;
    }

    /**
     * Get plan context value.
     * 
     * Used in script.
     * 
     * @param key key
     * @return value for key.
     */
    @Override
    public Object getValue(String key) {
        return this.values.get(key);
    }

    /**
     * Set plan context value.
     * 
     * Used in script.
     * 
     * @param key key
     * @param value value
     */
    @Override
    public void setValue(String key, Object value) {
        this.values.put(key, value);
    }

    /**
     * Get last simulation storage.
     * 
     * Used in script.
     * 
     * @return last simulation storage
     */
    @Override
    public SimulationStorage getLastSimulation() {
        SimulationStorage result = getSimulation(getNumber() - 1);
        return result;
    }

    /**
     * Get {@link SimulationStorage} for specified simulation plan number.
     * 
     * @param number number
     * @return {@link SimulationStorage}
     */
    @Override
    public SimulationStorage getSimulation(int number) {
        SimulationStorage result = null;
        if (0 <= number && number <= getNumber()) {
            result = SimulationStorage.getSimulation(id + "_" + number);
        }
        return result;
    }

    @Override
    public Historic getHistoric() {
        return historic;
    }

}
