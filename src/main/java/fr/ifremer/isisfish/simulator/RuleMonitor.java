/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2014 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator;

import org.apache.commons.collections4.map.MultiKeyMap;

import fr.ifremer.isisfish.entities.Metier;
import fr.ifremer.isisfish.rule.Rule;
import fr.ifremer.isisfish.types.TimeStep;

/**
 * This class keep trace of rule evalution condition for all step and all metier.
 * 
 * Created: 21 août 2006 15:45:03
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class RuleMonitor {

    protected MultiKeyMap<Object, Boolean> evalutionCondition = new MultiKeyMap<>();

    public boolean getEvalutionCondition(TimeStep step, Rule rule, Metier metier) {
        return this.evalutionCondition.get(step, rule, metier);
    }

    public void setEvaluationCondition(TimeStep step, Rule rule, Metier metier,
            boolean evalutionCondition) {
        this.evalutionCondition.put(step, rule, metier, evalutionCondition);
    }

}
