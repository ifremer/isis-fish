/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2018 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.nuiton.math.matrix.MatrixND;
import org.nuiton.topia.TopiaException;

import fr.ifremer.isisfish.config.IsisConfig;
import fr.ifremer.isisfish.datastore.RegionStorage;
import fr.ifremer.isisfish.entities.Observation;
import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.entities.Strategy;
import fr.ifremer.isisfish.export.ExportInfo;
import fr.ifremer.isisfish.export.SensitivityExport;
import fr.ifremer.isisfish.rule.Rule;
import fr.ifremer.isisfish.simulator.sensitivity.SensitivityAnalysis;

/**
 * Contains all parameters for one simulation.
 *
 * {@link SimulationParameter} must now be loaded from a {@link Properties}
 * object.
 * {@link #toProperties()} method, now return a new {@link Properties} object
 * filled with :
 * <ul>
 *  <li>non modified original properties
 *  <li>new properties if some objects has been instantiated by getters().
 * </ul>
 * 
 * Created: 10 janv. 2006 17:03:37
 *
 * @author poussin
 * @author chatellier
 * 
 * @version $Revision$
 *
 * Last update: $Date$
 * By : $Author$
 */
public interface SimulationParameter {

    /**
     * Permet de verifier que tous les parametres sont bon. Si ce n'est pas le
     * cas la liste retournée contient la liste des messages d'erreur.
     *
     * @return la liste d'erreur
     */
    List<String> check();

    /**
     * Get isis fish version for the simulation.
     * 
     * Default value to : {@link IsisConfig#getVersion()}.
     * 
     * @return the isisFishVersion.
     */
    String getIsisFishVersion();

    /**
     * Set isis fish Version.
     * 
     * @param isisFishVersion The isisFishVersion to set.
     */
    void setIsisFishVersion(String isisFishVersion);

    /**
     * Get description property.
     *
     * Default value to : "".
     * 
     * @return Description property.
     */
    String getDescription();

    /**
     * Set simulation description.
     * 
     * @param description the description to set.
     */
    void setDescription(String description);

    /**
     * Load region corresponding to {@link #getRegionName()}.
     * 
     * @return the region
     */
    RegionStorage getRegion();

    MatrixND getNumberOf(Population pop);

    /**
     * Get extra rules list.
     * 
     * @return Returns the extraRules.
     */
    List<String> getExtraRules();

    /**
     * Add extra rules.
     * 
     * Appelé par les plan de simulation, pour ajouter des regles supplémentaires
     * dans être integrés au zip.
     * 
     * @param extraRules extra rules to add
     */
    void addExtraRules(String... extraRules);

    /**
     * Get instantiated population list.
     * 
     * @return Returns the populations.
     */
    List<Population> getPopulations();

    /**
     * @param populations The populations to set.
     */
    void setPopulations(List<Population> populations);

    /**
     * Get instantiated strategies list.
     * 
     * Defaut to : empty array list
     * 
     * @return Returns the strategies.
     */
    List<Strategy> getStrategies();

    /**
     * Set strategies list.
     * 
     * @param strategies strategies list to set
     */
    void setStrategies(List<Strategy> strategies);

    /**
     * Add new simulation plan.
     * 
     * @param plan simulation plan to add
     */
    void addSimulationPlan(SimulationPlan plan);

    /**
     * Remove a plan.
     * 
     * @param plan
     * @return {@code true} if this list contained the specified element
     */
    boolean removeSimulationPlan(SimulationPlan plan);

    /**
     * Get instantiated simulation plan list.
     * 
     * Default to empty list.
     * 
     * @return the plans.
     */
    List<SimulationPlan> getSimulationPlans();

    /**
     * Set plans list.
     * 
     * @param plans plans to set
     */
    void setSimulationPlans(List<SimulationPlan> plans);

    /**
     * Return {@code true} if simulation is composed of independent plan only.
     * 
     * @return {@code true} if all {@link SimulationPlan} are {@link SimulationPlanIndependent}
     */
    boolean isIndependentPlan();

    /**
     * Clear plan list.
     */
    void clearPlans();

    /**
     * Add new rule to rules list.
     * @param rule rule to add
     */
    void addRule(Rule rule);

    /**
     * Remove a rule.
     * 
     * @param rule rule to remove
     * @return {@code true} if this list contained the specified element
     */
    boolean removeRule(Rule rule);

    /**
     * Get parameters rules list.
     *
     * @return the rules
     */
    List<Rule> getRules();

    /**
     * Set simulation rules.
     * 
     * @param rules rules to set
     */
    void setRules(List<Rule> rules);

    /**
     * Clear rule list.
     */
    void clearRules();

    /**
     * Return if cache should be used.
     * 
     * Default to : true.
     * 
     * @return use cache
     */
    boolean getUseCache();

    /**
     * Change use optimization parameter.
     * 
     * @param useCache use cache to set
     */
    void setUseCache(boolean useCache);

    /**
     * Return if statistic should be used.
     * 
     * Default to false;
     * 
     * @return use statistic
     */
    boolean getUseStatistic();

    /**
     * Change use statistic property.
     * 
     * @param useStatistic use statistic to set
     */
    void setUseStatistic(boolean useStatistic);

    boolean getUseComputeResult();

    void setUseComputeResult(boolean useComputeResult);

    /**
     * Get export names list.
     * 
     * @return export names list
     */
    List<String> getExportNames();

    /**
     * Set export names list.
     * 
     * @param exportNames export names list to set
     */
    void setExportNames(List<String> exportNames);

    /**
     * Get number of sensitivity simulation.
     * 
     * Default value to : -1
     * 
     * @return the numberOfSensitivitySimulation
     */
    int getNumberOfSensitivitySimulation();

    /**
     * Set number of sensitivity simulation.
     * 
     * @param numberOfSensitivitySimulation number of sensitivity simulation to set
     */
    void setNumberOfSensitivitySimulation(
            int numberOfSensitivitySimulation);

    /**
     * Get instantiated sensitivity analysis.
     * 
     * Default to : null.
     * 
     * @return the sensitivityAnalysis
     */
    SensitivityAnalysis getSensitivityAnalysis();

    /**
     * @param sensitivityAnalysis the sensitivityAnalysis to set
     */
    void setSensitivityAnalysis(SensitivityAnalysis sensitivityAnalysis);
    
    /**
     * Get only keep first result policy.
     * 
     * @return only keep first result policy
     * @since 4.1.1.0
     */
    boolean isSensitivityAnalysisOnlyKeepFirst();

    /**
     * Change only keep first result policy.
     * 
     * @param onlyKeepFirst only keep first result policy
     * @since 4.1.1.0
     */
    void setSensitivityAnalysisOnlyKeepFirst(boolean onlyKeepFirst);

    /**
     * Delete result after export policy.
     * 
     * @since 4.4
     */
    boolean isResultDeleteAfterExport();
    
    /**
     * Delete result after export policy.
     * 
     * @param deleteAfterExport delete result after export policy
     * @since 4.4
     */
    void setResultDeleteAfterExport(boolean deleteAfterExport);

    /**
     * Return loaded sensitivity export.
     * 
     * If exports are null or empty, try to load it from last ready parameters.
     * 
     * @return the sensitivityExportNames
     */
    List<SensitivityExport> getSensitivityExport();

    /**
     * @param sensitivityExport the sensitivityExportNames to set
     */
    void setSensitivityExport(List<SensitivityExport> sensitivityExport);

    boolean getUseOptimization();
    
    void setUseOptimization(boolean useOptimization);

    /**
     * Return used optimization script.
     * 
     * @return optimization method
     */
    Optimization getOptimization();

    /**
     * Set simulation optimization.
     *
     * @param optimization optimization
     */
    void setOptimization(Optimization optimization);

    /**
     * Return used objective.
     * 
     * @return
     */
    Objective getObjective();

    /**
     * Set objective.
     * 
     * @param objective objective
     */
    void setObjective(Objective objective);

    /**
     * Get exports and observations map.
     * 
     * @return exports and observations map
     */
    Map<ExportInfo, Observation> getOptimizationExportsObservations();

    /**
     * Set exports and observations map.
     * 
     * @param exportsObservations exports and observations map
     */
    void setOptimizationExportsObservations(Map<ExportInfo, Observation> exportsObservations);

    /**
     * If this simulation is generated by Optimization, return generation number
     */
    int getOptimizationGeneration();

    /**
     * If this simulation is generated by Optimization, set generation number
     */
    void setOptimizationGeneration(int optimizationGeneration);

    /**
     * If this simulation is generated by Optimization, return individual number in generation
     */
    int getOptimizationGenerationIndividual();

    /**
     * If this simulation is generated by Optimization, set individual number in generation
     */
    void setOptimizationGenerationIndividual(int optimizationGenerationIndividual);

    /**
     * Get use simulation plans property.
     * 
     * @return use simulation plan.
     */
    boolean getUseSimulationPlan();

    /**
     * Set use simulation plans property.
     * 
     * @param useSimulationPlan use simulation plan to set
     */
    void setUseSimulationPlan(boolean useSimulationPlan);

    /**
     * Get simulation number in simulation plan.
     * 
     * @return simulation number in simulation plan
     */
    int getSimulationPlanNumber();

    /**
     * Set simulation number in simulation plan.
     * 
     * @param simulationPlanNumber simulation plan number to set
     */
    void setSimulationPlanNumber(int simulationPlanNumber);

    /**
     * Get number of year to run to simulate.
     *
     * @return number of year
     */
    int getNumberOfYear();

    /**
     * Set number of year to run to simulate.
     *
     * @param numberOfYear number of year to set
     */
    void setNumberOfYear(int numberOfYear);

    /**
     * Get number of year to run to simulate.
     *
     * @return number of year
     */
    int getNumberOfMonths();

    /**
     * Set number of months to run to simulate.
     *
     * @param numberOfMonths number of months to set
     */
    void setNumberOfMonths(int numberOfMonths);

    /**
     * Get use prescript.
     * 
     * Default to false.
     * 
     * @return Returns the usePreScript.
     */
    boolean getUsePreScript();

    /**
     * Set use prescript property
     * 
     * @param usePreScript use preScript to set
     */
    void setUsePreScript(boolean usePreScript);

    /**
     * Get prescript content.
     * 
     * @return preScript content
     */
    String getPreScript();

    /**
     * Set pre script content.
     *
     * @param preScript prescript content
     */
    void setPreScript(String preScript);

    /**
     * Generated pre script is internal script generated by isis. This script
     * must be executed before user defined preScript.
     * 
     * @return generated pre script
     */
    String getGeneratedPreScript();

    /**
     * Generated pre script is internal script generated by isis. This script
     * must be executed before user defined preScript
     *
     * @param preScript prescript content
     */
    void setGeneratedPreScript(String preScript);

    /**
     * Get region name.
     * 
     * @return region name
     */
    String getRegionName();

    /**
     * Set region name.
     * 
     * @param regionName region name to set
     */
    void setRegionName(String regionName);

    /**
     * Set simulator name.
     * 
     * @return simulator name.
     */
    String getSimulatorName();

    /**
     * Set simulator name.
     * 
     * @param simulatorName simulator name to set
     */
    void setSimulatorName(String simulatorName);

    /**
     * Get enabled result names list.
     * 
     * @return enabled result names list
     */
    Collection<String> getResultEnabled();

    /**
     * Set enabled result names list.
     * 
     * @param resultEnabled enabled result names list
     */
    void setResultEnabled(Collection<String> resultEnabled);

    /**
     * Get tag values.
     * 
     * Default to empty map
     * 
     * @return Returns the tagValue.
     */
    Map<String, String> getTagValue();

    /**
     * Set tag values.
     * 
     * @param tagValue tagValues to set.
     */
    void setTagValue(Map<String, String> tagValue);

    /**
     * Get simulator log level.
     * 
     * Default to "info".
     * 
     * @return simulator log level
     */
    String getSimulLogLevel();

    /**
     * Set simulator log level.
     * 
     * @param logLevel simulator log level
     */
    void setSimulLogLevel(String logLevel);

    /**
     * Get script log level.
     * 
     * Default to "info".
     * 
     * @return script log level
     */
    String getScriptLogLevel();

    /**
     * Set script log level.
     * 
     * @param logLevel script log level
     */
    void setScriptLogLevel(String logLevel);

    /**
     * Get librairies log level.
     * 
     * Default to "error".
     * 
     * @return librairies log level
     */
    String getLibLogLevel();

    /**
     * Set lib log level.
     * 
     * @param logLevel
     */
    void setLibLogLevel(String logLevel);

    boolean isSimulErrorLevel();

    boolean isSimulWarnLevel();

    boolean isSimulInfoLevel();

    boolean isSimulDebugLevel();

    boolean isScriptErrorLevel();

    boolean isScriptWarnLevel();

    boolean isScriptInfoLevel();

    boolean isScriptDebugLevel();

    boolean isLibErrorLevel();

    boolean isLibWarnLevel();

    boolean isLibInfoLevel();

    boolean isLibDebugLevel();

    /**
     * Permet d'ajouter des parametres directement à partir de leur
     * representation chaine.
     * 
     * A ne pas utiliser normalement, sert uniquement dans les prescripts des
     * simulation des AS.
     * 
     * @param key key
     * @param value value
     * @since 3.4.0.0
     */
    void setProperty(String key, String value);

    /**
     * Set multiples properties.
     *
     * @param props props
     * @since 4.4.2.1
     */
    void setProperties(Properties props);

    /**
     * A copy instance of SimulationParameter.
     * 
     * Warning, this is not a deep copy, already instancied objects are
     * not duplicated.
     * 
     * @return a copy of this instance
     */
    SimulationParameter copy();

    /**
     * Make a deep copy of current parameters.
     * 
     * Bump all current parameters to properties and make a new one with
     * those propeties.
     * 
     * @return new parameters instance
     */
    SimulationParameter deepCopy();

    /**
     * The toString() method call getters.
     * 
     * So make instances of rules/export/plans...
     */
    String toString();

    /**
     * Permet de convertir l'objet SimulationParameter en un objet Properties
     * Cela permet de le stocker facilement sur le disque.
     * 
     * Recopie certaines proprietes si elle n'ont pas été instancié :
     * <ul>
     * <li>strategies</li>
     * <li>rules</li>
     * <li>simulationplans</li>
     * <li>sensitivityexports</li>
     * <li>sensitivityanalysis</li>
     * </ul>
     *
     * @return L'objet Properties representant les parametres
     * @see #fromProperties(Properties)
     */
    Properties toProperties();

    /**
     * Load properties from file.
     *
     * @param props property to read
     */
    void fromProperties(Properties props);

    /**
     * Reload parameters du to context change.
     * 
     * ie : in simulators when rollbacking transaction
     * 
     * Actually : reload rules parameters
     * 
     * @throws TopiaException
     */
    void reloadContextParameters() throws TopiaException;

    /**
     * Reset some cached data when region is changed.
     */
    void reloadRegionChangeParameter();

    /**
     * Remove all parameters non relative to given context.
     */
    void fixReloadContext(boolean sensitivityContext);
}
