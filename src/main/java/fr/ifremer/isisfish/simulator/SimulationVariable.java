/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nuiton.math.matrix.MatrixND;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.persistence.TopiaEntity;

import fr.ifremer.isisfish.entities.Equation;
import fr.ifremer.isisfish.entities.EquationImpl;
import fr.ifremer.isisfish.entities.Variable;
import fr.ifremer.isisfish.equation.VariableEquation;
import fr.ifremer.isisfish.util.EvaluatorHelper;

/**
 * Object containing cached variable value for a specific entity.
 * 
 * @author chatellier
 * @version $Revision$
 * @since 4.1.0.0
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class SimulationVariable {

    /** Simulation context (to get db). */
    protected SimulationContext simulationContext;

    /** Managed entity id. */
    protected TopiaEntity topiaEntity;

    /** Variable name &gt; variable entity. */
    protected Map<String, Variable> variablesCache = new HashMap<>();

    public SimulationVariable(SimulationContext simulationContext, TopiaEntity topiaEntity) {
        this.simulationContext = simulationContext;
        this.topiaEntity = topiaEntity;
    }

    /**
     * Return variable entity from cache or database.
     * 
     * @param name variable name to get
     * @return variable entity
     * @throws TopiaException if can't restore variable from db
     */
    protected Variable getVariableEntity(String name) throws TopiaException {
       Variable v = variablesCache.get(name);
       if (v == null) {
           TopiaContext topiaContext = simulationContext.getDB();
           List<Variable> vindb = topiaContext.findAll("FROM " + Variable.class.getName() +
                   " WHERE " + Variable.PROPERTY_ENTITY_ID + " = :id" +
                   " AND " + Variable.PROPERTY_NAME + " = :name",
                   "id", topiaEntity.getTopiaId(),
                   "name", name);
           if (!vindb.isEmpty()) {
               v = vindb.get(0);
               variablesCache.put(name, v);
           }
       }
       return v;
    }

    /**
     * Return variable value as double.
     * 
     * @param name variable name
     * @return value as double
     * @throws TopiaException if can't restore variable from db
     */
    public double getAsDouble(String name) throws TopiaException {
        Variable v = getVariableEntity(name);
        double result = v.getDoubleValue();
        return result;
    }

    /**
     * Return variable value as matrix.
     * 
     * @param name variable name
     * @return value as matrix
     * @throws TopiaException if can't restore variable from db
     */
    public MatrixND getAsMatrix(String name) throws TopiaException {
        Variable v = getVariableEntity(name);
        MatrixND result = v.getMatrixValue();
        return result;
    }

    /**
     * Set variable value.
     * 
     * @param name variable name
     * @param value new value
     * @throws TopiaException if can't restore variable from db
     */
    public void set(String name, Object value) throws TopiaException {
        Variable v = getVariableEntity(name);
        if (double.class.isAssignableFrom(value.getClass()) || Double.class.isAssignableFrom(value.getClass())) {
            v.setDoubleValue((Double)value);
        } else if (value instanceof MatrixND) {
            v.setMatrixValue((MatrixND)value);
        } else if (value instanceof String) {
            Equation eq = v.getEquationValue();
            if (eq == null) {
                eq = new EquationImpl();
                eq.setContent((String)value);
            }
            v.setEquationValue(eq);
        }
    }

    /**
     * Eval current variable equation.
     * 
     * @param name variable name
     * @return equation result
     * @throws TopiaException if can't restore variable from db
     */
    public double eval(String name) throws TopiaException {
        Variable v = getVariableEntity(name);
        return eval(v);
    }

    /**
     * Eval current variable equation.
     * 
     * @param v variable
     * @return equation result
     */
    protected double eval(Variable v) {
        Equation eq = v.getEquationValue();

        Map<String, Object> args = new HashMap<>();
        args.put("context", simulationContext);
        args.put("entity", topiaEntity);
        args.put("step", simulationContext.getSimulationControl().getStep());

        Object val = EvaluatorHelper.evaluate("fr.ifremer.isisfish.equation",
                topiaEntity.getTopiaId() + "#" + v.getName(), VariableEquation.class,
                eq.getContent(), args);

        double result = 0.0;
        if (val instanceof Number) {
            result = ((Number) val).doubleValue();
        }
        return result;
    }
}
