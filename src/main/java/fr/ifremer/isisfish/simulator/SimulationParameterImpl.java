/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2019 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator;

import fr.ifremer.isisfish.IsisFishDAOHelper;
import fr.ifremer.isisfish.config.IsisConfig;
import fr.ifremer.isisfish.datastore.ExportStorage;
import fr.ifremer.isisfish.datastore.ObjectiveStorage;
import fr.ifremer.isisfish.datastore.OptimizationStorage;
import fr.ifremer.isisfish.datastore.RegionStorage;
import fr.ifremer.isisfish.datastore.RuleStorage;
import fr.ifremer.isisfish.datastore.SensitivityAnalysisStorage;
import fr.ifremer.isisfish.datastore.SensitivityExportStorage;
import fr.ifremer.isisfish.datastore.SimulationPlanStorage;
import fr.ifremer.isisfish.datastore.StorageHelper;
import fr.ifremer.isisfish.entities.Observation;
import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.entities.PopulationDAO;
import fr.ifremer.isisfish.entities.Strategy;
import fr.ifremer.isisfish.entities.StrategyDAO;
import fr.ifremer.isisfish.export.ExportHelper;
import fr.ifremer.isisfish.export.ExportInfo;
import fr.ifremer.isisfish.export.SensitivityExport;
import fr.ifremer.isisfish.rule.Rule;
import fr.ifremer.isisfish.rule.RuleHelper;
import fr.ifremer.isisfish.simulator.sensitivity.SensitivityAnalysis;
import fr.ifremer.isisfish.types.Month;
import fr.ifremer.isisfish.util.converter.ConverterUtil;
import org.apache.commons.beanutils.ConvertUtilsBean;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixFactory;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.SortedProperties;
import org.nuiton.util.StringUtil;
import org.nuiton.version.Versions;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import static fr.ifremer.isisfish.simulator.SimulationParameterPropertiesHelper.DOT;
import static fr.ifremer.isisfish.simulator.SimulationParameterPropertiesHelper.LIST_SEPARATOR;
import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Real {@link SimulationParameter} implementation.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class SimulationParameterImpl implements SimulationParameter {

    /** Logger for this class. */
    final private static Log log = LogFactory.getLog(SimulationParameterImpl.class);

    /** Remember last read properties. */
    protected Properties propertiesParameters;

    /**
     * Isis-fish version that permit to do the simulation.
     * Must be set just before simulation by simulator.
     */
    protected String isisFishVersion;

    /** Description de la simulation. */
    protected String description;

    /** Nom de la region sur lequel on simule. */
    protected String regionName;

    /** Nombre de mois sur lequel on souhaite simuler. */
    protected Integer numberOfMonths;

    /** Le nom de l'objet script a utiliser pour faire la simulation. */
    protected String simulatorName;

    /** Utilisation ou non du cache. */
    protected Boolean useCache;

    /** Utilisation ou non des statistiques. */
    protected Boolean useStatistic;

    /** Utilisation ou non du timing du calcul des resultats. */
    protected Boolean useComputeResult;

    /** La liste des strategies a utiliser pour la simulation. */
    protected List<Strategy> strategies;

    /** La liste des populations a utiliser pour la simulation. */
    protected List<Population> populations;

    /** Les effectifs initiaux des différentes population. */
    protected Map<Population, MatrixND> numbers;

    /** La liste des regles de gestions a utiliser pour la simulation. */
    protected List<Rule> rules;

    /** La liste des regles potentiellement ajoutée par les plans de simulation. */
    protected List<String> extraRules;

    /** La liste des plans a utiliser pour la simulation. */
    protected List<SimulationPlan> simulationPlans;

    /** Ma liste des exports automatique a faire en fin de simulation. */
    protected List<String> exportNames;

    /** Utilisation du script de pre simulation. */
    protected Boolean usePreScript;

    /** Le script de pre simulation a utiliser. */
    protected String preScript;

    /** Le script de pre simulation defini par Isis */
    protected String generatedPreScript;

    /** Utilisation du plan de simulation. */
    protected Boolean useSimulationPlan;

    /**
     * Le numero de sequence de la liste des plans de simulation. Le premier
     * élement du plan doit etre 0. Si la simulation ne fait pas partie d'un
     * plan la valeur est -1. */
    protected Integer simulationPlanNumber;

    /** ExportInfo utilisés pour les analyses de sensibilités. */
    protected List<SensitivityExport> sensitivityExports;

    /**
     * Nombre de simulation constituant l'analyse de sensibilité.
     * 
     * FIXME ce parametre ne devrait pas ce trouver à cet endroit.
     * Ce n'est pas un parametres, mais une infos de simulation.
     * Cela permet de savoir combien de simulation il y a au total
     * pour savoir quand elle sont terminées.
     */
    protected Integer numberOfSensitivitySimulation;

    /**
     * Dans une analyze de sensibilite, conserver les résultats de toutes
     * les simulations est inutile, seule les résultats de la premières
     * sont nécéssaires. Les resultats peuvent être supprimés après les
     * export de données.
     */
    protected Boolean sensitivityAnalysisOnlyKeepFirst;

    /**
     * Parametre utilisé pour supprimer les résultats à la fin d'une simulation après les exports.
     */
    protected Boolean resultDeleteAfterExport;

    /** Script utilisé pour les analyses de sensibilités. */
    protected SensitivityAnalysis sensitivityAnalysis;

    /** Utilisation d'un script de simulation. */
    protected Boolean useOptimization;

    /** Script d'optimisation. */
    protected Optimization optimization;

    /** Fonction d'objectif. */
    protected Objective objective;

    /** Exports et observations (optimisation). */
    protected Map<ExportInfo, Observation> optimizationExportsObservations;

    /** If this simulation is generated by Optimization, generation number */
    protected Integer optimizationGeneration;

    /** If this simulation is generated by Optimization, individual number in generation */
    protected Integer optimizationGenerationIndividual;

    /** La liste des resultats qui nous interesse. */
    protected Collection<String> resultEnabled;

    /** Autre parametre defini par l'utilisateur. */
    protected Map<String, String> tagValue;

    /** Le niveau de log du simulateur à utiliser pendant la simulation. Par defaut info. */
    protected String simulLogLevel;
    /** Le niveau de log des scripts à utiliser pendant la simulation. Par defaut info. */
    protected String scriptLogLevel;
    /** Le niveau de log des librairies à utiliser pendant la simulation. Par defaut error. */
    protected String libLogLevel;

    /** La region corespondant au parametre {@link #regionName}. */
    protected transient RegionStorage region;

    @Override
    public List<String> check() {
        List<String> result = new ArrayList<>();
        // TODO faire la verif et pour chaque erreur mettre un message
        // dans la liste result (par exemple si pas d'effectif
        // pour une pop
        return result;
    }

    @Override
    public String getIsisFishVersion() {
        // if not already set
        if (isisFishVersion == null) {
            if (propertiesParameters != null) {
                isisFishVersion = SimulationParameterPropertiesHelper.getIsisFishVersion(propertiesParameters);
            } else {
                // default value
                isisFishVersion = IsisConfig.getVersion();
            }
        }
        return isisFishVersion;
    }

    @Override
    public void setIsisFishVersion(String isisFishVersion) {
        this.isisFishVersion = isisFishVersion;
    }

    @Override
    public String getDescription() {

        if (description == null) {
            if (propertiesParameters != null) {
                description = SimulationParameterPropertiesHelper
                        .getDescription(propertiesParameters);
            } else {
                // default value
                description = "";
            }
        }
        return this.description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public RegionStorage getRegion() {

        if (region == null || !region.getName().equals(getRegionName())) {
            // bien faire attention ici que la bonne region est ouverte
            // - isis-database-3 dans le cas d'une region normale
            // - simulation dans le cas d'une simulation
            region = RegionStorage.getRegion(getRegionName());
        } else {
            if (log.isDebugEnabled()) {
                log.debug(t("Region %s already inited", getRegionName()));
            }
        }
        return this.region;
    }

    @Override
    public MatrixND getNumberOf(Population pop) {
        if (numbers == null) {
            numbers = new LinkedHashMap<>();
        }
        MatrixND result = numbers.get(pop);
        if (result == null) {
            List groups = pop.getPopulationGroup();
            List zones = pop.getPopulationZone();

            if (CollectionUtils.isEmpty(groups)) {
                groups = Collections.singletonList("No group");
            }
            if (CollectionUtils.isEmpty(zones)) {
                zones = Collections.singletonList("No zone");
            }
            List[] semantics = new List[] { groups, zones };
            result = MatrixFactory.getInstance().create(
                    n("matrixAbundance"),
                    semantics,
                    new String[] { n("Groups"), n("Zones") });

            numbers.put(pop, result);
        }
        return result;
    }

    @Override
    public List<String> getExtraRules() {

        if (extraRules == null) {
            // no properties in config ?
            extraRules = new ArrayList<>();
        }
        return extraRules;
    }

    @Override
    public void addExtraRules(String... extraRules) {
        getExtraRules().addAll(Arrays.asList(extraRules));
    }

    @Override
    public List<Population> getPopulations() {
        if (populations == null) {
            populations = new ArrayList<>();

            if (propertiesParameters != null) {

                try {
                    // On verifie tout d'abord que l'on ai pas dans une simulation
                    // si on y es, on utilise le context static non null du thread local
                    // Resoud les lazy exceptions des parametres des regles
                    boolean mustClose = false;
                    TopiaContext tx = SimulationContext.get().getDB();

                    if (tx == null) {
                        // not in simulation, create transaction
                        tx = getRegion().getStorage().beginTransaction();
                        mustClose = true;
                    }

                    PopulationDAO populationDAO = IsisFishDAOHelper
                            .getPopulationDAO(tx);
                    String[] populationList = SimulationParameterPropertiesHelper
                            .getPopulationNames(propertiesParameters);
                    for (String name : populationList) {
                        if (StringUtils.isNotEmpty(name)) {
                            try {
                                Population population = populationDAO.findByName(name);
                                if (population != null) { // can happen when reloading parameter for a different region
                                    populations.add(population);

                                    List number = SimulationParameterPropertiesHelper.getPopulationNumbers(propertiesParameters, name);
                                    MatrixND mat = getNumberOf(population);
                                    mat.fromList(number);
                                }
                            } catch (TopiaException eee) {
                                if (log.isWarnEnabled()) {
                                    log.warn("Can't find population: " + name, eee);
                                }
                            }
                        }
                    }

                    // si la transaction a été ouverte (pas dans une simulation)
                    // on la referme
                    if (mustClose) {
                        tx.closeContext();
                    }
                } catch (TopiaException eee1) {
                    if (log.isWarnEnabled()) {
                        log.warn("Can't get PopulationDAO", eee1);
                    }
                }
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("No last read properties, skip population reloading");
                }
            }
        }
        return populations;
    }

    @Override
    public void setPopulations(List<Population> populations) {
        this.populations = populations;
    }

    @Override
    public List<Strategy> getStrategies() {

        if (strategies == null) {

            strategies = new ArrayList<>();

            if (propertiesParameters != null) {
                strategies = new ArrayList<>();

                try {

                    // On verifie tout d'abord que l'on ai pas dans une simulation
                    // si on y es, on utilise le context static non null du thread local
                    // Resoud les lazy exceptions des parametres des regles
                    boolean mustClose = false;
                    TopiaContext tx = SimulationContext.get().getDB();

                    if (tx == null) {
                        // not in simulation, create transaction
                        tx = getRegion().getStorage().beginTransaction();
                        mustClose = true;
                    }

                    StrategyDAO strategyDAO = IsisFishDAOHelper.getStrategyDAO(tx);
                    String[] strategyList = SimulationParameterPropertiesHelper
                            .getStrategieNames(propertiesParameters);
                    for (String name : strategyList) {
                        if (StringUtils.isNotEmpty(name)) {
                            try {
                                Strategy str = strategyDAO.findByName(name);
                                if (str != null) { // can happen when reloading parameter for another region
                                    strategies.add(str);
                                }
                            } catch (TopiaException eee) {
                                if (log.isWarnEnabled()) {
                                    log.warn("Can't find strategy: " + name, eee);
                                }
                            }
                        }
                    }

                    // si la transaction a été ouverte (pas dans une simulation)
                    // on la referme
                    if (mustClose) {
                        tx.closeContext();
                    }
                } catch (TopiaException eee1) {
                    if (log.isWarnEnabled()) {
                        log.warn("Can't get StrategyDAO", eee1);
                    }
                }
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("No last read properties, skip strategies reloading");
                }
            }
        }
        return strategies;
    }

    @Override
    public void setStrategies(List<Strategy> strategies) {
        this.strategies = strategies;
    }

    @Override
    public void addSimulationPlan(SimulationPlan plan) {
        getSimulationPlans().add(plan);
    }

    @Override
    public boolean removeSimulationPlan(SimulationPlan plan) {
        return getSimulationPlans().remove(plan);
    }

    @Override
    public List<SimulationPlan> getSimulationPlans() {
        if (simulationPlans == null) {

            simulationPlans = new ArrayList<>();

            if (propertiesParameters != null) {
                // simulation plan
                String[] planList = SimulationParameterPropertiesHelper
                        .getSimulationPlanNames(propertiesParameters);
                int planIndex = 0;
                for (String name : planList) {
                    if (StringUtils.isNotEmpty(name)) {
                        try {
                            SimulationPlanStorage planStorage = SimulationPlanStorage.getSimulationPlan(name);
                            if (planStorage != null) { // since 4.0.0.3 can return null
                                SimulationPlan plan = planStorage.getNewInstance();
                                StorageHelper.populateStorageParams(planIndex++,
                                        getRegion().getStorage(), plan, propertiesParameters,
                                        SimulationParameterPropertiesHelper.PLAN_KEY);
                                simulationPlans.add(plan);
                            }
                        } catch (Exception eee) {
                            if (log.isWarnEnabled()) {
                                log.warn("Can't find plan: " + name, eee);
                            }
                        }
                    }
                }
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("No last read properties, skip plans reloading");
                }
            }
        }
        return this.simulationPlans;
    }

    @Override
    public void setSimulationPlans(List<SimulationPlan> plans) {
        this.simulationPlans = plans;
    }

    @Override
    public boolean isIndependentPlan() {
        boolean result = true;
        for (SimulationPlan plan : getSimulationPlans()) {
            if (!(plan instanceof SimulationPlanIndependent)) {
                result = false;
                break;
            }
        }
        return result;
    }

    @Override
    public void clearPlans() {
        if (simulationPlans != null) {
            simulationPlans.clear();
        }
    }

    @Override
    public void addRule(Rule rule) {
        getRules().add(rule);
    }

    @Override
    public boolean removeRule(Rule rule) {
        return getRules().remove(rule);
    }

    @Override
    public List<Rule> getRules() {
        if (rules == null) {
            rules = new ArrayList<>();

            if (propertiesParameters != null) {
                try {
                    // On verifie tout d'abord que l'on ai pas dans une simulation
                    // si on y es, on utilise le context static non null du thread local
                    // Resoud les lazy exceptions des parametres des regles
                    boolean mustClose = false;
                    TopiaContext tx = SimulationContext.get().getDB();

                    if (tx == null) {
                        // not in simulation, create transaction
                        tx = getRegion().getStorage().beginTransaction();
                        mustClose = true;
                    }

                    // rules
                    String[] ruleList = SimulationParameterPropertiesHelper
                            .getRuleNames(propertiesParameters);
                    int ruleIndex = 0;
                    for (String name : ruleList) {
                        if (StringUtils.isNotEmpty(name)) {
                            try {
                                Rule rule = RuleStorage.getRule(name).getNewInstance();
                                RuleHelper.populateRule(ruleIndex++, tx, rule, propertiesParameters);
                                rules.add(rule);
                            } catch (Exception eee) {
                                if (log.isWarnEnabled()) {
                                    log.warn("Can't find rule: " + name, eee);
                                }
                            }
                        }
                    }

                    // si la transaction a été ouverte (pas dans une simulation)
                    // on la referme
                    if (mustClose) {
                        tx.closeContext();
                    }
                } catch (TopiaException eee1) {
                    if (log.isWarnEnabled()) {
                        log.warn("Can't get TopiaContext", eee1);
                    }
                }
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("No last read properties, skip rule reloading");
                }
            }
        }
        return rules;
    }

    @Override
    public void setRules(List<Rule> rules) {
        this.rules = rules;
    }

    @Override
    public void clearRules() {
        if (rules != null) {
            rules.clear();
        }
    }

    @Override
    public boolean getUseCache() {
        if (useCache == null) {
            if (propertiesParameters != null) {
                // en version < 4.3, le parametre était nommé 'useOptimization'
                // qualifiait le cache, il été renommé ensuite
                if (Versions.smallerThan(getIsisFishVersion(), "4.3.0.0")) {
                    useCache = Boolean.valueOf(propertiesParameters.getProperty(
                            SimulationParameterPropertiesHelper.USE_OPTIMIZATION_KEY, "true"));
                } else {
                    useCache = Boolean.valueOf(propertiesParameters.getProperty(
                            SimulationParameterPropertiesHelper.USE_CACHE_KEY, "true"));
                }
            } else {
                useCache = Boolean.TRUE;
            }
        }
        return useCache;
    }

    @Override
    public void setUseCache(boolean useCache) {
        this.useCache = useCache;
    }

    @Override
    public boolean getUseStatistic() {

        if (useStatistic == null) {
            if (propertiesParameters != null) {
                useStatistic = SimulationParameterPropertiesHelper.getUseStatistic(propertiesParameters);
            } else {
                useStatistic = Boolean.FALSE;
            }
        }
        return this.useStatistic;
    }

    @Override
    public void setUseStatistic(boolean useStatistic) {
        this.useStatistic = useStatistic;
    }

    @Override
    public boolean getUseComputeResult() {

        if (useComputeResult == null) {
            if (propertiesParameters != null) {
                useComputeResult = SimulationParameterPropertiesHelper.getUseComputeResult(propertiesParameters);
            } else {
                useComputeResult = Boolean.FALSE;
            }
        }
        return this.useComputeResult;
    }

    @Override
    public void setUseComputeResult(boolean useComputeResult) {
        this.useComputeResult = useComputeResult;
    }

    @Override
    public List<String> getExportNames() {
        if (exportNames == null) {
            exportNames = new ArrayList<>();

            if (propertiesParameters != null) {
                // exports
                String[] exportList = SimulationParameterPropertiesHelper.getExportNames(propertiesParameters);
                for (String name : exportList) {
                    if (StringUtils.isNotEmpty(name)) {
                        exportNames.add(name);
                    }
                }
            }
        }
        return this.exportNames;
    }

    @Override
    public void setExportNames(List<String> exportNames) {
        this.exportNames = exportNames;
    }

    @Override
    public int getNumberOfSensitivitySimulation() {

        if (numberOfSensitivitySimulation == null) {

            if (propertiesParameters != null) {
                numberOfSensitivitySimulation =
                        SimulationParameterPropertiesHelper.getNumberOfSensitivitySimulation(propertiesParameters);
            } else {
                numberOfSensitivitySimulation = -1;
            }
        }

        return numberOfSensitivitySimulation;
    }

    @Override
    public void setNumberOfSensitivitySimulation(
            int numberOfSensitivitySimulation) {
        this.numberOfSensitivitySimulation = numberOfSensitivitySimulation;
    }

    @Override
    public SensitivityAnalysis getSensitivityAnalysis() {

        if (sensitivityAnalysis == null) {
            if (propertiesParameters != null) {
                String analysisName = SimulationParameterPropertiesHelper
                        .getSensitivityAnalysis(propertiesParameters);
                if (StringUtils.isNotEmpty(analysisName)) {
                    try {
                        SensitivityAnalysisStorage sensitivityStorage = SensitivityAnalysisStorage.getSensitivityAnalysis(analysisName);
                        sensitivityAnalysis = sensitivityStorage.getNewInstance();
                        // 0 = only single sensitivity
                        StorageHelper.populateStorageParams(0, getRegion().getStorage(),
                                sensitivityAnalysis, propertiesParameters,
                                SimulationParameterPropertiesHelper.SENSITIVITY_KEY);
                    } catch (Exception eee) {
                        sensitivityAnalysis = null;
                        if (log.isWarnEnabled()) {
                            log.warn("Can't find sensitivity: " + sensitivityAnalysis, eee);
                        }
                    }
                }
            }
        }

        return sensitivityAnalysis;
    }

    @Override
    public void setSensitivityAnalysis(SensitivityAnalysis sensitivityAnalysis) {
        this.sensitivityAnalysis = sensitivityAnalysis;
    }

    @Override
    public List<SensitivityExport> getSensitivityExport() {
        if (sensitivityExports == null) {
            sensitivityExports = new ArrayList<>();

            if (propertiesParameters != null) {
                try {
                    // On verifie tout d'abord que l'on ai pas dans une simulation
                    // si on y es, on utilise le context static non null du thread local
                    // Resoud les lazy exceptions des parametres des regles
                    boolean mustClose = false;
                    TopiaContext tx = SimulationContext.get().getDbResult();

                    if (tx == null) {
                        // not in simulation, create transaction
                        tx = getRegion().getStorage().beginTransaction();
                        mustClose = true;
                    }

                    // sensitivity export
                    String[] sensitivityExportList = SimulationParameterPropertiesHelper
                            .getSensitivityExportNames(propertiesParameters);
                    int sensitivityExportIndex = 0;
                    for (String name : sensitivityExportList) {
                        try {
                            if (!StringUtils.isEmpty(name)) {
                                SensitivityExport sensitivityExport = SensitivityExportStorage
                                        .getSensitivityExport(name)
                                        .getNewInstance();
                                ExportHelper.populateSensitivityExport(
                                                sensitivityExportIndex++, tx,
                                                sensitivityExport,
                                                propertiesParameters);
                                sensitivityExports.add(sensitivityExport);
                            }
                        } catch (Exception eee) {
                            if (log.isWarnEnabled()) {
                                log.warn("Can't find SensitivityExport: " + name, eee);
                            }
                        }
                    }

                    // si la transaction a été ouverte (pas dans une simulation)
                    // on la referme
                    if (mustClose) {
                        tx.closeContext();
                    }
                } catch (TopiaException eee1) {
                    if (log.isWarnEnabled()) {
                        log.warn("Can't get TopiaContext", eee1);
                    }
                }
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("No last read properties, skip sensitivity exports reloading");
                }
            }
        }
        return sensitivityExports;
    }

    @Override
    public void setSensitivityExport(List<SensitivityExport> sensitivityExport) {
        this.sensitivityExports = sensitivityExport;
    }

    @Override
    public Optimization getOptimization() {
        if (optimization == null) {
            if (propertiesParameters != null) {
                String optimizationName = SimulationParameterPropertiesHelper
                        .getOptimizationName(propertiesParameters);
                if (!StringUtils.isEmpty(optimizationName)) {
                    try {
                        OptimizationStorage optimizationStorage =
                                OptimizationStorage.getOptimization(optimizationName);
                        optimization = optimizationStorage.getNewInstance();
                        // 0 = only single optimization
                        StorageHelper.populateStorageParams(0, getRegion().getStorage(),
                                optimization, propertiesParameters,
                                SimulationParameterPropertiesHelper.OPTIMIZATION_KEY);
                    } catch (Exception eee) {
                        optimization = null;
                        if (log.isWarnEnabled()) {
                            log.warn("Can't find optimization: " + optimizationName, eee);
                        }
                    }
                }
            }
        }

        return optimization;
    }

    @Override
    public boolean getUseOptimization() {
        if (useOptimization == null) {
            if (propertiesParameters != null) {
                // en version < 4.3, le parametre était nommé 'useOptimization'
                // qualifiant le cache, il a été renommé ensuite
                if (Versions.smallerThan(getIsisFishVersion(), "4.3.0.0")) {
                    useOptimization = Boolean.FALSE;
                } else {
                    useOptimization = Boolean.valueOf(propertiesParameters.getProperty(
                            SimulationParameterPropertiesHelper.USE_OPTIMIZATION_KEY, "false"));
                }
            } else {
                useOptimization = Boolean.FALSE;
            }
        }
        return useOptimization;
    }

    @Override
    public void setUseOptimization(boolean useOptimization) {
        this.useOptimization = useOptimization;
    }

    @Override
    public void setOptimization(Optimization optimization) {
        this.optimization = optimization;
    }

    @Override
    public Objective getObjective() {
        if (objective == null) {
            if (propertiesParameters != null) {
                String objectiveName = SimulationParameterPropertiesHelper
                        .getObjectiveName(propertiesParameters);
                if (!StringUtils.isEmpty(objectiveName)) {
                    try {
                        ObjectiveStorage objectiveStorage =
                                ObjectiveStorage.getObjective(objectiveName);
                        objective = objectiveStorage.getNewInstance();
                        // 0 = only single objective
                        StorageHelper.populateStorageParams(0, getRegion().getStorage(),
                                objective, propertiesParameters,
                                SimulationParameterPropertiesHelper.OBJECTIVE_KEY);
                    } catch (Exception eee) {
                        objective = null;
                        if (log.isWarnEnabled()) {
                            log.warn("Can't find objective: " + objective, eee);
                        }
                    }
                }
            }
        }
        return objective;
    }

    @Override
    public void setObjective(Objective objective) {
        this.objective = objective;
    }

    @Override
    public Map<ExportInfo, Observation> getOptimizationExportsObservations() {
        if (optimizationExportsObservations == null) {
            // must be sorted for ui (LinkedHashMap)
            optimizationExportsObservations = new LinkedHashMap<>();

            if (propertiesParameters != null) {
                try {
                    // On verifie tout d'abord que l'on ai pas dans une simulation
                    // si on y est, on utilise le context static non null du thread local
                    // Resoud les lazy exceptions des parametres des regles
                    boolean mustClose = false;
                    TopiaContext tx = SimulationContext.get().getDB();

                    if (tx == null) {
                        // not in simulation, create transaction
                        tx = getRegion().getStorage().beginTransaction();
                        mustClose = true;
                    }

                    // rules
                    String[] optimizationExportList = SimulationParameterPropertiesHelper
                            .getOptimizationExportNames(propertiesParameters);
                    ConvertUtilsBean beanUtils = ConverterUtil.getConverter(tx);
                    int optimizationExportIndex = 0;
                    for (String name : optimizationExportList) {
                        if (StringUtils.isNotBlank(name)) {
                            try {
                                ExportInfo export = ExportStorage.getExport(name).getNewInstance();
                                Observation observation = null;
                                String observationId = propertiesParameters.getProperty(
                                        SimulationParameterPropertiesHelper.
                                                OPTIMIZATION_OBSERVATION_KEY
                                                + DOT + optimizationExportIndex);
                                if (StringUtils.isNotBlank(observationId)) {
                                    observation = (Observation)beanUtils.convert(observationId, TopiaEntity.class);
                                }
                                optimizationExportsObservations.put(export, observation);
                            } catch (Exception eee) {
                                if (log.isWarnEnabled()) {
                                    log.warn("Can't find rule: " + name, eee);
                                }
                            }
                        }
                        optimizationExportIndex++;
                    }

                    // si la transaction a été ouverte (pas dans une simulation)
                    // on la referme
                    if (mustClose) {
                        tx.closeContext();
                    }
                } catch (TopiaException eee) {
                    if (log.isWarnEnabled()) {
                        log.warn("Can't get TopiaContext", eee);
                    }
                }
            }
        }

        return optimizationExportsObservations;
    }

    @Override
    public void setOptimizationExportsObservations(Map<ExportInfo, Observation> optimizationExportsObservations) {
        this.optimizationExportsObservations = optimizationExportsObservations;
    }

    @Override
    public boolean getUseSimulationPlan() {

        if (useSimulationPlan == null) {

            if (propertiesParameters != null) {
                useSimulationPlan = SimulationParameterPropertiesHelper.getUseSimulationPlan(propertiesParameters);
            } else {
                useSimulationPlan = Boolean.FALSE;
            }
        }
        return useSimulationPlan;
    }

    @Override
    public void setUseSimulationPlan(boolean useSimulationPlan) {
        this.useSimulationPlan = useSimulationPlan;
    }

    @Override
    public int getSimulationPlanNumber() {

        if (simulationPlanNumber == null) {

            if (propertiesParameters != null) {
                simulationPlanNumber = SimulationParameterPropertiesHelper.getSimulationPlanNumber(propertiesParameters);
            } else {
                simulationPlanNumber = -1;
            }
        }

        return simulationPlanNumber;
    }

    @Override
    public void setSimulationPlanNumber(int simulationPlanNumber) {
        this.simulationPlanNumber = simulationPlanNumber;
    }

    /**
     * If this simulation is generated by Optimization, return generation number
     */
    @Override
    public int getOptimizationGeneration()  {

        if (optimizationGeneration == null) {

            if (propertiesParameters != null) {
                optimizationGeneration = SimulationParameterPropertiesHelper.getOptimizationGeneration(propertiesParameters);
            } else {
                optimizationGeneration = -1;
            }
        }

        return optimizationGeneration;
    }

    /**
     * If this simulation is generated by Optimization, set generation number
     */
    @Override
    public void setOptimizationGeneration(int optimizationGeneration) {
        this.optimizationGeneration = optimizationGeneration;
    }

    /**
     * If this simulation is generated by Optimization, return individual number in generation
     */
    @Override
    public int getOptimizationGenerationIndividual()  {

        if (optimizationGenerationIndividual == null) {

            if (propertiesParameters != null) {
                optimizationGenerationIndividual = SimulationParameterPropertiesHelper.getOptimizationGenerationIndividual(propertiesParameters);
            } else {
                optimizationGenerationIndividual = -1;
            }
        }

        return optimizationGenerationIndividual;
    }

    /**
     * If this simulation is generated by Optimization, set individual number in generation
     */
    @Override
    public void setOptimizationGenerationIndividual(int optimizationGenerationIndividual) {
        this.optimizationGenerationIndividual = optimizationGenerationIndividual;
    }


    @Override
    public boolean isSensitivityAnalysisOnlyKeepFirst() {
        if (sensitivityAnalysisOnlyKeepFirst == null) {

            if (propertiesParameters != null) {
                sensitivityAnalysisOnlyKeepFirst = SimulationParameterPropertiesHelper.isSensitivityAnalysisOnlyKeepFirst(propertiesParameters);
            } else {
                sensitivityAnalysisOnlyKeepFirst = Boolean.FALSE;
            }
        }

        return sensitivityAnalysisOnlyKeepFirst;
    }

    @Override
    public void setSensitivityAnalysisOnlyKeepFirst(boolean onlyKeepFirst) {
        sensitivityAnalysisOnlyKeepFirst = onlyKeepFirst;
    }
    
    @Override
    public boolean isResultDeleteAfterExport() {
        if (resultDeleteAfterExport == null) {

            if (propertiesParameters != null) {
                resultDeleteAfterExport = SimulationParameterPropertiesHelper.isResultDeleteAfterExport(propertiesParameters);
            } else {
                resultDeleteAfterExport = Boolean.FALSE;
            }
        }

        return resultDeleteAfterExport;
    }
    
    @Override
    public void setResultDeleteAfterExport(boolean resultDeleteAfterExport) {
        this.resultDeleteAfterExport = resultDeleteAfterExport;
    }

    @Override
    public int getNumberOfYear() {
        return (int) Math.ceil((double) getNumberOfMonths() / Month.NUMBER_OF_MONTH);
    }

    @Override
    public void setNumberOfYear(int numberOfYear) {
        this.numberOfMonths = numberOfYear * Month.NUMBER_OF_MONTH;
    }

    @Override
    public int getNumberOfMonths() {

        if (numberOfMonths == null) {

            if (propertiesParameters != null) {
                numberOfMonths = SimulationParameterPropertiesHelper
                        .getNumberOfMonths(propertiesParameters);
            } else {
                numberOfMonths = Month.NUMBER_OF_MONTH;
            }
        }
        return numberOfMonths;
    }

    @Override
    public void setNumberOfMonths(int numberOfMonths) {
        this.numberOfMonths = numberOfMonths;
    }

    @Override
    public boolean getUsePreScript() {

        if (usePreScript == null) {

            if (propertiesParameters != null) {
                usePreScript = SimulationParameterPropertiesHelper.getUsePreScript(propertiesParameters);
            } else {
                usePreScript = Boolean.FALSE;
            }
        }

        return usePreScript;
    }

    @Override
    public void setUsePreScript(boolean usePreScript) {
        this.usePreScript = usePreScript;
    }

    @Override
    public String getPreScript() {

        if (preScript == null) {

            if (propertiesParameters != null) {
                preScript = SimulationParameterPropertiesHelper.getPreScript(propertiesParameters);
            } else {
                preScript = "";
            }
        }

        return preScript;
    }

    @Override
    public void setPreScript(String preScript) {
        this.preScript = preScript;
    }

    @Override
    public String getGeneratedPreScript() {

        if (generatedPreScript == null) {

            if (propertiesParameters != null) {
                generatedPreScript = SimulationParameterPropertiesHelper.getGeneratedPreScript(propertiesParameters);
            } else {
                generatedPreScript = "";
            }
        }

        return generatedPreScript;
    }

    @Override
    public void setGeneratedPreScript(String preScript) {
        this.generatedPreScript = preScript;
    }

    @Override
    public String getRegionName() {

        if (regionName == null) {
            if (propertiesParameters != null) {
                regionName = SimulationParameterPropertiesHelper.getRegionName(propertiesParameters);
            } else {
                // defaut value
                regionName = "";
            }
        }

        return regionName;
    }

    @Override
    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    @Override
    public String getSimulatorName() {

        if (simulatorName == null) {
            if (propertiesParameters != null) {
                simulatorName = SimulationParameterPropertiesHelper.getSimulatorName(propertiesParameters);
            } else {
                simulatorName = "DefaultSimulator.java";
            }
        }
        return simulatorName;
    }

    @Override
    public void setSimulatorName(String simulatorName) {
        this.simulatorName = simulatorName;
    }

    @Override
    public Collection<String> getResultEnabled() {

        if (resultEnabled == null) {

            resultEnabled = new LinkedList<>();

            if (propertiesParameters != null) {
                String[] resultList = SimulationParameterPropertiesHelper.getResultNames(propertiesParameters);
                
                // en version < 4.4, les noms de resultats commencait par une minuscule
                // il doivent etre capitalized pour matcher la classe correspondante
                if (Versions.smallerThan(getIsisFishVersion(), "4.4.0.0")) {
                    for (String result : resultList) {
                        String migrated = StringUtils.capitalize(result);
                        // .java must be added to be like others properties
                        if (!migrated.endsWith(".java")) {
                            migrated += ".java";
                        }
                        resultEnabled.add(migrated);
                    }
                } else {
                    Collections.addAll(resultEnabled, resultList);
                }
            }
        }
        return resultEnabled;
    }

    @Override
    public void setResultEnabled(Collection<String> resultEnabled) {
        this.resultEnabled = resultEnabled;
    }

    @Override
    public Map<String, String> getTagValue() {

        if (tagValue == null) {
            if (propertiesParameters != null) {
                tagValue = SimulationParameterPropertiesHelper.getTagValue(propertiesParameters);
            } else {
                tagValue = new LinkedHashMap<>();
            }
        }

        return tagValue;
    }

    @Override
    public void setTagValue(Map<String, String> tagValue) {
        this.tagValue = tagValue;
    }

    @Override
    public String getSimulLogLevel() {

        if (simulLogLevel == null) {

            if (propertiesParameters != null) {
                simulLogLevel = SimulationParameterPropertiesHelper.getSimulLogLevel(propertiesParameters);
            } else {
                simulLogLevel = "info";
            }
        }

        return simulLogLevel;
    }

    @Override
    public void setSimulLogLevel(String logLevel) {
        if (log.isDebugEnabled()) {
            log.debug(t("isisfish.params.changeLogLev", simulLogLevel, logLevel));
        }
        this.simulLogLevel = logLevel;
    }

    @Override
    public String getScriptLogLevel() {

        if (scriptLogLevel == null) {
            if (propertiesParameters != null) {
                scriptLogLevel = SimulationParameterPropertiesHelper.getScriptLogLevel(propertiesParameters);
            } else {
                scriptLogLevel = "info";
            }
        }

        return scriptLogLevel;
    }

    @Override
    public void setScriptLogLevel(String logLevel) {
        if (log.isDebugEnabled()) {
            log.debug(t("isisfish.params.changeLogLev", scriptLogLevel,
                    logLevel));
        }
        this.scriptLogLevel = logLevel;
    }

    @Override
    public String getLibLogLevel() {
        if (libLogLevel == null) {
            if (propertiesParameters != null) {
                libLogLevel = SimulationParameterPropertiesHelper.getLibLogLevel(propertiesParameters);
            } else {
                libLogLevel = "error";
            }
        }

        return libLogLevel;
    }

    @Override
    public void setLibLogLevel(String logLevel) {
        if (log.isDebugEnabled()) {
            log.debug(t("isisfish.params.changeLogLev", libLogLevel, logLevel));
        }
        this.libLogLevel = logLevel;
    }

    @Override
    public boolean isSimulErrorLevel() {
        return "error".equals(getSimulLogLevel());
    }

    @Override
    public boolean isSimulWarnLevel() {
        return "warn".equals(getSimulLogLevel());
    }

    @Override
    public boolean isSimulInfoLevel() {
        return "info".equals(getSimulLogLevel());
    }

    @Override
    public boolean isSimulDebugLevel() {
        return "debug".equals(getSimulLogLevel());
    }

    @Override
    public boolean isScriptErrorLevel() {
        return "error".equals(getScriptLogLevel());
    }

    @Override
    public boolean isScriptWarnLevel() {
        return "warn".equals(getScriptLogLevel());
    }

    @Override
    public boolean isScriptInfoLevel() {
        return "info".equals(getScriptLogLevel());
    }

    @Override
    public boolean isScriptDebugLevel() {
        return "debug".equals(getScriptLogLevel());
    }

    @Override
    public boolean isLibErrorLevel() {
        return "error".equals(getLibLogLevel());
    }

    @Override
    public boolean isLibWarnLevel() {
        return "warn".equals(getLibLogLevel());
    }

    @Override
    public boolean isLibInfoLevel() {
        return "info".equals(getLibLogLevel());
    }

    @Override
    public boolean isLibDebugLevel() {
        return "debug".equals(getLibLogLevel());
    }

    /**
     * Permet d'ajouter des parametres directement à partir de leur
     * representation chaine.
     * 
     * A ne pas utiliser normalement, sert uniquement dans les prescripts des
     * simulation des AS.
     * 
     * @param key key
     * @param value value
     * @since 3.4.0.0
     */
    @Override
    public void setProperty(String key, String value) {
        propertiesParameters.setProperty(key, value);
    }

    @Override
    public void setProperties(Properties props) {
        propertiesParameters.putAll(props);
    }

    @Override
    public SimulationParameter copy() {
        SimulationParameterImpl result = new SimulationParameterImpl();

        if (propertiesParameters != null) {
            result.propertiesParameters = new SortedProperties();
            // I think that iteration work better than parameters in contructor
            result.propertiesParameters.putAll(propertiesParameters);
        }

        // still needed to be copied, if there is no propertiesParameters
        result.isisFishVersion = getIsisFishVersion();
        result.description = getDescription();
        result.regionName = getRegionName();
        result.numberOfMonths = getNumberOfMonths();
        result.simulatorName = getSimulatorName();
        result.useCache = getUseCache();
        result.useStatistic = getUseStatistic();
        result.usePreScript = getUsePreScript();
        result.preScript = getPreScript();
        result.generatedPreScript = getGeneratedPreScript();
        result.useSimulationPlan = getUseSimulationPlan();
        result.simulationPlanNumber = getSimulationPlanNumber();
        result.optimizationGeneration = getOptimizationGeneration();
        result.optimizationGenerationIndividual = getOptimizationGenerationIndividual();
        if (exportNames != null) {
            result.exportNames = new LinkedList<>(exportNames);
        }
        if (strategies != null) {
            result.strategies = new LinkedList<>(strategies);
        }
        if (populations != null) {
            result.populations = new LinkedList<>(populations);
        }
        if (rules != null) {
            result.rules = new LinkedList<>(rules);
        }
        if (simulationPlans != null) {
            result.simulationPlans = new LinkedList<>(simulationPlans);
        }
        if (objective != null) {
            result.objective = objective;
        }
        result.useOptimization = getUseOptimization();
        if (optimization != null) {
            result.optimization = optimization;
        }
        if (optimizationExportsObservations != null) {
            result.optimizationExportsObservations = new LinkedHashMap<>(optimizationExportsObservations);
        }
        if (resultEnabled != null) {
            result.resultEnabled = new LinkedList<>(resultEnabled);
        }
        if (numbers != null) {
            result.numbers = new HashMap<>(numbers);
        }
        if (tagValue != null) {
            result.tagValue = new HashMap<>(tagValue);
        }
        result.simulLogLevel = getSimulLogLevel();
        result.scriptLogLevel = getScriptLogLevel();
        result.libLogLevel = getLibLogLevel();
        result.numberOfSensitivitySimulation = getNumberOfSensitivitySimulation();
        if (sensitivityExports != null) {
            result.sensitivityExports = new LinkedList<>(sensitivityExports);
        }
        result.sensitivityAnalysis = sensitivityAnalysis;

        return result;
    }

    @Override
    public SimulationParameter deepCopy() {
        Properties props = toProperties();
        SimulationParameter newInstance = new SimulationParameterImpl();
        newInstance.fromProperties(props);
        return newInstance;
    }

    @Override
    public String toString() {
        Properties prop = toProperties();
        return SimulationParameterPropertiesHelper.toString(prop);
    }

    @Override
    public Properties toProperties() {
        Properties result = new SortedProperties();

        result.setProperty(SimulationParameterPropertiesHelper.ISIS_FISH_VERSION_KEY, getIsisFishVersion());
        result.setProperty(SimulationParameterPropertiesHelper.DESCRIPTION_KEY, getDescription());
        result.setProperty(SimulationParameterPropertiesHelper.REGION_NAME_KEY, getRegionName());
        result.setProperty(SimulationParameterPropertiesHelper.NUMBER_OF_MONTHS_KEY, String.valueOf(getNumberOfMonths()));
        result.setProperty(SimulationParameterPropertiesHelper.SIMULATOR_NAME_KEY, getSimulatorName());
        result.setProperty(SimulationParameterPropertiesHelper.USE_CACHE_KEY, String.valueOf(getUseCache()));
        result.setProperty(SimulationParameterPropertiesHelper.USE_STATISTIC_KEY, String.valueOf(getUseStatistic()));
        result.setProperty(SimulationParameterPropertiesHelper.USE_COMPUTE_RESULT_KEY, String.valueOf(getUseComputeResult()));

        // strategies
        if (strategies != null) {
            String strategyList = "";
            for (Strategy str : getStrategies()) {
                strategyList += str.getName() + SimulationParameterPropertiesHelper.LIST_SEPARATOR;
            }
            result.setProperty(SimulationParameterPropertiesHelper.STRATEGIES_KEY, StringUtil.substring(strategyList, 0, -1));
        } else {
            if (propertiesParameters != null) {
                String strs = propertiesParameters.getProperty(SimulationParameterPropertiesHelper.STRATEGIES_KEY);
                if (strs != null) {
                    result.setProperty(SimulationParameterPropertiesHelper.STRATEGIES_KEY, strs);
                }
            }
        }

        // populations
        if (populations != null) {
            String populationList = "";
            for (Population pop : getPopulations()) {
                populationList += pop.getName() + LIST_SEPARATOR;
                MatrixND number = getNumberOf(pop);
                String numberAsString = String.valueOf(number.toList());
                result.setProperty(
                        SimulationParameterPropertiesHelper.POPULATION_KEY+ DOT + pop.getName()
                                + DOT + SimulationParameterPropertiesHelper.NUMBER_KEY, numberAsString);
            }
            result.setProperty(SimulationParameterPropertiesHelper.POPULATIONS_KEY,
                    StringUtil.substring(populationList, 0, -1));
        } else {
            if (propertiesParameters != null) {
                String pops = propertiesParameters.getProperty(
                        SimulationParameterPropertiesHelper.POPULATIONS_KEY);
                if (pops != null) {
                    result.setProperty(SimulationParameterPropertiesHelper.POPULATIONS_KEY, pops);
                    SimulationParameterPropertiesHelper.copy(propertiesParameters, result, 
                            SimulationParameterPropertiesHelper.POPULATION_KEY + DOT);
                }
            }
        }

        // rules
        if (rules != null) {
            String ruleList = "";
            int ruleIndex = 0;
            for (Rule rule : getRules()) {
                ruleList += RuleStorage.getName(rule) + LIST_SEPARATOR;
                Properties ruleProp = RuleHelper.getRuleAsProperties(
                        ruleIndex++, getRegion().getStorage(), rule);
                result.putAll(ruleProp);
            }
            result.setProperty(SimulationParameterPropertiesHelper.RULES_KEY, ruleList);
        } else {
            if (propertiesParameters != null) {
                String rules = propertiesParameters.getProperty(
                        SimulationParameterPropertiesHelper.RULES_KEY);
                if (rules != null) {
                    result.setProperty(SimulationParameterPropertiesHelper.RULES_KEY, rules);
                    SimulationParameterPropertiesHelper.copy(propertiesParameters, result,
                            SimulationParameterPropertiesHelper.RULE_KEY + DOT);
                }
            }
        }

        // anaylyse plans
        if (simulationPlans != null) {
            String planList = "";
            int planIndex = 0;
            for (SimulationPlan plan : getSimulationPlans()) {
                planList += SimulationPlanStorage.getName(plan) + LIST_SEPARATOR;
                Properties planProp = StorageHelper.getParamsAsProperties(planIndex++,
                        getRegion().getStorage(), plan, SimulationParameterPropertiesHelper.PLAN_KEY);
                result.putAll(planProp);
            }
            result.setProperty(SimulationParameterPropertiesHelper.PLANS_KEY, planList);
        } else {
            if (propertiesParameters != null) {
                String plans = propertiesParameters.getProperty(SimulationParameterPropertiesHelper.PLANS_KEY);
                if (plans != null) {
                    result.setProperty(SimulationParameterPropertiesHelper.PLANS_KEY, plans);
                    SimulationParameterPropertiesHelper.copy(propertiesParameters, result,
                            SimulationParameterPropertiesHelper.PLAN_KEY + DOT);
                }
            }
        }

        // objective
        if (objective != null) {
            String objectiveName = ObjectiveStorage.getName(objective);
            Properties objectiveProp = StorageHelper.getParamsAsProperties(0,
                    getRegion().getStorage(), objective,
                    SimulationParameterPropertiesHelper.OBJECTIVE_KEY);
            result.putAll(objectiveProp);
            result.setProperty(SimulationParameterPropertiesHelper.OBJECTIVE_KEY,
                    objectiveName);
        } else {
            if (propertiesParameters != null) {
                String objective = propertiesParameters.getProperty(
                        SimulationParameterPropertiesHelper.OBJECTIVE_KEY);
                if (objective != null) {
                    result.setProperty(SimulationParameterPropertiesHelper.OBJECTIVE_KEY, objective);
                    SimulationParameterPropertiesHelper.copy(propertiesParameters, result,
                            SimulationParameterPropertiesHelper.OBJECTIVE_KEY + DOT);
                }
                for (String key : propertiesParameters.stringPropertyNames()) {
                    if (key.startsWith(SimulationParameterPropertiesHelper.OBJECTIVE_KEY + DOT)) {
                        result.setProperty(key, propertiesParameters.getProperty(key));
                    }
                }
            }
        }

        // optimization
        if (optimization != null) {
            String optimizationName = ObjectiveStorage.getName(optimization);
            Properties optimizationProp = StorageHelper.getParamsAsProperties(0,
                    getRegion().getStorage(), optimization,
                    SimulationParameterPropertiesHelper.OPTIMIZATION_KEY);
            result.putAll(optimizationProp);
            result.setProperty(SimulationParameterPropertiesHelper.OPTIMIZATION_KEY,
                    optimizationName);
        } else {
            if (propertiesParameters != null) {
                String optimization = propertiesParameters.getProperty(
                        SimulationParameterPropertiesHelper.OPTIMIZATION_KEY);
                if (optimization != null) {
                    result.setProperty(SimulationParameterPropertiesHelper.OPTIMIZATION_KEY, optimization);
                    SimulationParameterPropertiesHelper.copy(propertiesParameters, result,
                            SimulationParameterPropertiesHelper.OPTIMIZATION_KEY + DOT);
                }
            }
        }

        // optimization export and observations
        if (optimizationExportsObservations != null) {
            String optimizationExportsList = "";
            int optimizationExportIndex = 0;
            ConvertUtilsBean beanUtils = ConverterUtil.getConverter(null);
            for (Map.Entry<ExportInfo, Observation> exportObservationEntry : optimizationExportsObservations.entrySet()) {
                ExportInfo export = exportObservationEntry.getKey();
                optimizationExportsList += ExportStorage.getName(export) + LIST_SEPARATOR;

                // add in props observation id export index
                Observation observation = exportObservationEntry.getValue();
                if (observation != null) {
                    result.setProperty(
                            SimulationParameterPropertiesHelper.OPTIMIZATION_OBSERVATION_KEY + DOT
                                    + optimizationExportIndex,
                            beanUtils.convert(observation));
                }
                optimizationExportIndex++;
            }
            result.setProperty(SimulationParameterPropertiesHelper.OPTIMIZATION_EXPORTS_KEY,
                    optimizationExportsList);
        } else {
            if (propertiesParameters != null) {
                String optimizationExports = propertiesParameters.getProperty(
                        SimulationParameterPropertiesHelper.OPTIMIZATION_EXPORTS_KEY);
                if (optimizationExports != null) {
                    result.setProperty(SimulationParameterPropertiesHelper.OPTIMIZATION_EXPORTS_KEY,
                            optimizationExports);
                }
                SimulationParameterPropertiesHelper.copy(propertiesParameters, result,
                        SimulationParameterPropertiesHelper.OPTIMIZATION_OBSERVATION_KEY + DOT);
            }
        }
        
        // export names
        String exportList = StringUtils.join(getExportNames(), LIST_SEPARATOR);
        result.setProperty(SimulationParameterPropertiesHelper.EXPORTS_KEY, exportList);

        // number of sensitivity simulation
        result.setProperty(SimulationParameterPropertiesHelper.NUMBER_OF_SENSITIVITY_SIMULATION_KEY,
                String.valueOf(getNumberOfSensitivitySimulation()));

        // analysis name
        if (sensitivityAnalysis != null) {
            String analysisName = SensitivityAnalysisStorage
                    .getName(getSensitivityAnalysis());
            result.setProperty(SimulationParameterPropertiesHelper.SENSITIVITY_ANALYSIS_KEY,
                    analysisName);

            // analysis parameter
            Properties analysisParams = StorageHelper.getParamsAsProperties(0,
                    getRegion().getStorage(), getSensitivityAnalysis(),
                    SimulationParameterPropertiesHelper.SENSITIVITY_KEY);
            result.putAll(analysisParams);
        } else {
            if (propertiesParameters != null) {
                String sensitivityanalysis = propertiesParameters.getProperty(
                        SimulationParameterPropertiesHelper.SENSITIVITY_ANALYSIS_KEY);
                if (sensitivityanalysis != null) {
                    result.setProperty(SimulationParameterPropertiesHelper.SENSITIVITY_ANALYSIS_KEY,
                            sensitivityanalysis);
                    SimulationParameterPropertiesHelper.copy(propertiesParameters, result,
                            SimulationParameterPropertiesHelper.SENSITIVITY_KEY + DOT);
                }
            }
        }

        // sensitivity exports
        if (sensitivityExports != null) {
            String sensitivityExportList = "";
            int sensitivityExportIndex = 0;
            for (SensitivityExport sensitivityExport : getSensitivityExport()) {
                sensitivityExportList += SensitivityExportStorage
                        .getName(sensitivityExport) + LIST_SEPARATOR;
                Properties exportProp = ExportHelper.getSensitivityExportAsProperties(
                        sensitivityExportIndex++, getRegion().getStorage(), sensitivityExport);
                result.putAll(exportProp);
            }
            result.setProperty(SimulationParameterPropertiesHelper.SENSITIVITY_EXPORTS_KEY,
                    sensitivityExportList);
        } else {
            if (propertiesParameters != null) {
                String sensitivityexports = propertiesParameters.getProperty(
                        SimulationParameterPropertiesHelper.SENSITIVITY_EXPORTS_KEY);
                if (sensitivityexports != null) {
                    result.setProperty(SimulationParameterPropertiesHelper.SENSITIVITY_EXPORTS_KEY,
                            sensitivityexports);
                    SimulationParameterPropertiesHelper.copy(propertiesParameters, result,
                            SimulationParameterPropertiesHelper.SENSITIVITY_EXPORT_KEY + DOT);
                }
            }
        }

        // result
        result.setProperty(SimulationParameterPropertiesHelper.RESULT_DELETE_AFTER_EXPORT_KEY,
                String.valueOf(isResultDeleteAfterExport()));

        // sensitivity params
        result.setProperty(SimulationParameterPropertiesHelper.SENSITIVITY_ANALYSIS_ONLY_KEEP_FIRST_KEY,
                String.valueOf(isSensitivityAnalysisOnlyKeepFirst()));

        result.setProperty(SimulationParameterPropertiesHelper.GENERATED_PRE_SCRIPT_KEY,
                getGeneratedPreScript());
        result.setProperty(SimulationParameterPropertiesHelper.USE_PRE_SCRIPT_KEY,
                String.valueOf(getUsePreScript()));
        result.setProperty(SimulationParameterPropertiesHelper.PRE_SCRIPT_KEY,
                getPreScript());
        result.setProperty(SimulationParameterPropertiesHelper.USE_SIMULATION_PLAN_KEY,
                String.valueOf(getUseSimulationPlan()));
        result.setProperty(SimulationParameterPropertiesHelper.SIMULATION_PLAN_NUMBER_KEY,
                String.valueOf(getSimulationPlanNumber()));
        result.setProperty(SimulationParameterPropertiesHelper.USE_OPTIMIZATION_KEY,
                String.valueOf(getUseOptimization()));
        result.setProperty(SimulationParameterPropertiesHelper.OPTIMIZATION_GENERATION_KEY,
                String.valueOf(getOptimizationGeneration()));
        result.setProperty(SimulationParameterPropertiesHelper.OPTIMIZATION_GENERATION_INDIVIDUAL_KEY,
                String.valueOf(getOptimizationGenerationIndividual()));

        String resultList = StringUtils.join(getResultEnabled(),
                SimulationParameterPropertiesHelper.LIST_SEPARATOR);
        result.setProperty(SimulationParameterPropertiesHelper.RESULT_NAMES_KEY, resultList);

        for (Map.Entry<String, String> e : getTagValue().entrySet()) {
            result.setProperty(SimulationParameterPropertiesHelper.TAG_VALUE_KEY+ DOT
                    + e.getKey(), e.getValue());
        }

        result.setProperty(SimulationParameterPropertiesHelper.SIMUL_LOG_LEVEL_KEY,
                getSimulLogLevel());
        result.setProperty(SimulationParameterPropertiesHelper.SCRIPT_LOG_LEVEL_KEY,
                getScriptLogLevel());
        result.setProperty(SimulationParameterPropertiesHelper.LIB_LOG_LEVEL_KEY,
                getLibLogLevel());
        return result;
    }

    @Override
    public void fromProperties(Properties props) {

        // save properties (use full to read again parameter)
        // for exports, or rules....
        this.propertiesParameters = props;

    }

    @Override
    public void reloadContextParameters() throws TopiaException {

        // On verifie tout d'abord que l'on ai pas dans une simulation
        // si on y es, on utilise le context static non null du thread local
        // Resoud les lazy exceptions des parametres des regles
        boolean mustClose = false;
        TopiaContext tx = SimulationContext.get().getDB();

        if (tx == null) {
            // not in simulation, create transaction
            tx = getRegion().getStorage().beginTransaction();
            mustClose = true;
        }

        // reload rules
        if (log.isDebugEnabled()) {
            log.debug("Reloading rules");
        }
        for (Rule rule : getRules()) {
            try {
                for (Field field : rule.getClass().getFields()) {
                    // le champ ne doit pas être privé
                    if (Modifier.isPublic(field.getModifiers())) {
                        // si le type est topia entity (ou sous classe)
                        if (TopiaEntity.class.isAssignableFrom(field.getType())) {
                            TopiaEntity entity = (TopiaEntity) field.get(rule);
                            // il est possible que les parametres soient null
                            // dans ce cas, il ne faut pas essayer de les recharger
                            if (entity != null) {
                                // reloading
                                TopiaEntity newEntity = tx.findByTopiaId(entity.getTopiaId());
                                field.set(rule, newEntity);
                            }
                        }
                    }
                }
            } catch (IllegalArgumentException|IllegalAccessException e) {
                if (log.isErrorEnabled()) {
                    log.error("Can't refresh rule field", e);
                }
            }
        }

        // si la transaction a été ouverte (pas dans une simulation)
        // on la referme
        if (mustClose) {
            tx.commitTransaction();
            tx.closeContext();
        }
    }

    /**
     * Remove from 'cached' parameter data referring to another region. Strategies, populations, and objects with
     * potential parameters values.
     */
    @Override
    public void reloadRegionChangeParameter() {
        populations = null; // will be reloaded
        strategies = null; // will be reloaded
        rules = null;
        simulationPlans = null;
        optimization = null;
        objective = null;
        numbers = null;
    }

    @Override
    public void fixReloadContext(boolean sensitivityContext) {
        if (sensitivityContext) {
            useSimulationPlan = null;
            simulationPlans = null;
            useOptimization = null;
            optimization = null;
            optimizationExportsObservations = null;
            optimizationGeneration = null;
            optimizationGenerationIndividual = null;
            objective = null;

            SimulationParameterPropertiesHelper.removeProperties(propertiesParameters,
                    SimulationParameterPropertiesHelper.USE_SIMULATION_PLAN_KEY,
                    SimulationParameterPropertiesHelper.PLANS_KEY,
                    SimulationParameterPropertiesHelper.USE_OPTIMIZATION_KEY,
                    SimulationParameterPropertiesHelper.OPTIMIZATION_KEY,
                    SimulationParameterPropertiesHelper.OPTIMIZATION_GENERATION_INDIVIDUAL_KEY,
                    SimulationParameterPropertiesHelper.OPTIMIZATION_GENERATION_INDIVIDUAL_KEY,
                    SimulationParameterPropertiesHelper.OBJECTIVE_KEY);
            SimulationParameterPropertiesHelper.removePropertiesStartingWith(propertiesParameters,
                    SimulationParameterPropertiesHelper.PLAN_KEY,
                    SimulationParameterPropertiesHelper.OPTIMIZATION_KEY,
                    SimulationParameterPropertiesHelper.OPTIMIZATION_OBSERVATION_KEY,
                    SimulationParameterPropertiesHelper.OBJECTIVE_KEY);

        } else {
            sensitivityExports = null;
            sensitivityAnalysis = null;
            sensitivityAnalysisOnlyKeepFirst = null;
            numberOfSensitivitySimulation = null;

            SimulationParameterPropertiesHelper.removeProperties(propertiesParameters,
                    SimulationParameterPropertiesHelper.SENSITIVITY_EXPORTS_KEY,
                    SimulationParameterPropertiesHelper.SENSITIVITY_ANALYSIS_KEY,
                    SimulationParameterPropertiesHelper.SENSITIVITY_ANALYSIS_ONLY_KEEP_FIRST_KEY,
                    SimulationParameterPropertiesHelper.NUMBER_OF_SENSITIVITY_SIMULATION_KEY);
            SimulationParameterPropertiesHelper.removePropertiesStartingWith(propertiesParameters,
                    SimulationParameterPropertiesHelper.SENSITIVITY_KEY,
                    SimulationParameterPropertiesHelper.SENSITIVITY_EXPORT_KEY);
        }

        // common
        generatedPreScript = null;
        SimulationParameterPropertiesHelper.removeProperties(propertiesParameters,
                SimulationParameterPropertiesHelper.GENERATED_PRE_SCRIPT_KEY);
    }
}
