/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2010 Ifremer, Code Lutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.actions;

import fr.ifremer.isisfish.config.IsisAction;
import fr.ifremer.isisfish.config.IsisConfig;
import fr.ifremer.isisfish.config.Option;

import java.util.Arrays;

/**
 * Other actions.
 * 
 * @author poussin
 * @version $Revision$
 * 
 * Last update: $Date$
 * by : $Author$
 */
public class OtherAction {

    protected IsisConfig config = null;

    public OtherAction(IsisConfig config) {
        this.config = config;
    }

    /**
     * Display help and exit action.
     */
    public void help() {
        System.out.println("Usage: isis-fish [-h]");
        System.out.println("Options (set with --option <key> <value>) :");
        for (Option o : Option.values()) {
            System.out.println("\t" + o.key + "(" + o.defaultValue + ") : " + o.description);
        }
        
        System.out.println("Actions:");
        for (IsisAction a : IsisAction.values()) {
            System.out.println("\t" + Arrays.toString(a.getAliases()) + "(" + a.getAction() + ") : " + a.getDescription());
        }
        System.exit(0);
    }

    /**
     * Display version and exit.
     */
    public void version() {
        System.out.println("Isis-Fish : " + IsisConfig.getVersion());
        System.exit(0);
    }
}
