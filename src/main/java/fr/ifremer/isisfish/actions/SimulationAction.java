/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2018 Ifremer, Code Lutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.actions;

import fr.ifremer.isisfish.config.IsisConfig;
import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.datastore.RegionStorage;
import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.simulator.SimulationControl;
import fr.ifremer.isisfish.simulator.SimulationParameter;
import fr.ifremer.isisfish.simulator.SimulationParameterImpl;
import fr.ifremer.isisfish.simulator.launcher.InProcessSimulatorLauncher;
import fr.ifremer.isisfish.simulator.launcher.SimulationItem;
import fr.ifremer.isisfish.simulator.launcher.SimulationService;
import fr.ifremer.isisfish.simulator.launcher.SimulatorLauncher;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig.Action.Step;
import org.nuiton.util.ZipUtil;

import java.io.File;
import java.io.FileInputStream;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

import static org.nuiton.i18n.I18n.t;

/**
 * Actions des simulations.
 * 
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class SimulationAction {

    /** to use log facility, just put in your code: log.info("..."); */
    private static Log log = LogFactory.getLog(SimulationAction.class);

    protected IsisConfig config = null;

    public SimulationAction(IsisConfig config) {
        this.config = config;
    }

    /**
     * Simulate with region file.
     *
     * @param simulId simulation unique id
     * @param parameterFile parameter file
     * @param region region file to import
     * @throws Exception
     */
    public static void simulateWithRegion(String simulId, File parameterFile,
            File region) throws Exception {
        Properties props = new Properties();
        try (FileInputStream inStream = new FileInputStream(parameterFile)) {
            props.load(inStream);
        }

        SimulationParameter params = new SimulationParameterImpl();
        params.fromProperties(props);

        // try to import region if needed
        String regionName = params.getRegionName();
        if (region != null) {
            if (!RegionStorage.exists(regionName)) {
                RegionStorage.importAndRenameZip(region, regionName);
            } else {
                if (log.isWarnEnabled()) {
                    log.warn(t("Region %s allready exist in repository. Can't import",
                        regionName));
                }
            }
        }

        SimulationService.getService().submit(simulId, params, null, 0);
    }

    /**
     * Run a simulation with only a region name that already exists and a prescript file.
     *
     * @param simulationId simulation unique id
     * @param regionName region name
     * @param simulationPrescript simulation prescript
     * @since 4.4.2.1
     * @deprecated since 4.4.2.3 with no replacement
     */
    @Step(IsisConfig.STEP_AFTER_INIT_VCS)
    public static void simulationWithRegionNameAndScript(String simulationId, String regionName, File simulationPrescript) throws Exception {

        // read prescript as string
        String generatedPrescriptContent = null;
        if (simulationPrescript != null && simulationPrescript.canRead()) {
            generatedPrescriptContent = FileUtils.readFileToString(simulationPrescript, StandardCharsets.UTF_8);
        }

        // build params
        SimulationParameter params = new SimulationParameterImpl();
        params.setRegionName(regionName);
        params.setPreScript(generatedPrescriptContent);
        params.setUsePreScript(true);

        // prepare simulation
        SimulationService simulationService = SimulationService.getService();
        SimulationControl control = new SimulationControl(simulationId);
        SimulationItem item = new SimulationItem(control, null);
        File simulationZipFile = simulationService.prepareSimulationZipFile(params, null, control, null, null);
        item.setSimulationZip(simulationZipFile);

        // run it in current thread
        SimulatorLauncher launcher = new InProcessSimulatorLauncher();
        launcher.simulate(simulationService, item);

        // FIXME integrer ca dans le process normal d'init
        IsisFish.quit();
    }

    /**
     * Launch a simulation with specified simulationId and simulation zip.
     * 
     * @param simulationId id de simulation
     * @param simulationZip Zip de la simulation
     * @throws Exception 
     */
    @Step(IsisConfig.STEP_AFTER_INIT_VCS)
    public static void simulateWithSimulation(String simulationId, File simulationZip)
            throws Exception {
        // just call with null pre script
        simulateWithSimulationAndScript(simulationId, simulationZip, null);
    }
    
    /**
     * Launch a simulation with specified simulationId, simulationZip
     * and simulationPrescript.
     * 
     * @param simulationId id de simulation
     * @param simulationZip Zip de la simulation
     * @param simulationPrescript simulation prescript
     * @throws Exception 
     */
    @Step(IsisConfig.STEP_AFTER_INIT_VCS)
    public static void simulateWithSimulationAndScript(String simulationId, File simulationZip, File simulationPrescript)
            throws Exception {

        if (log.isInfoEnabled()) {
            log.info("Command line action : simulateWithSimulationAndScript (" + simulationId + ", " + simulationZip.getAbsolutePath() + ", " + simulationPrescript + ")");
        }

        // ne pas mettre la date, car le sub process la met deja  + " " + new SimpleDateFormat("yyyy-MM-dd-HH-mm").format(new Date());
        String name = simulationId; 

        //SimulationStorage simulation = launcher.simulate(null, control, simulationZip);
        //simulation.getStorage().closeContext();
        String generatedPrescriptContent = null;
        if (simulationPrescript != null) {
            if (simulationPrescript.canRead()) {
                generatedPrescriptContent = FileUtils.readFileToString(simulationPrescript, StandardCharsets.UTF_8);
            } else {
                if (log.isWarnEnabled()) {
                    log.warn("Can't read prescript file: " + simulationPrescript);
                }
            }
        }

        SimulationControl control = new SimulationControl(name);
        control.setAutoSaveState(true); // needed for remote simulation

        SimulationItem item = new SimulationItem(control, null);
        item.setSimulationZip(simulationZip);
        item.setGeneratedPrescriptContent(generatedPrescriptContent);
        
        // lancement de la simulation
        SimulatorLauncher launcher = new InProcessSimulatorLauncher();
        launcher.simulate(SimulationService.getService(), item);
        SimulationStorage simulation = launcher.getSimulationStorage(SimulationService.getService(), control);
        simulation.getStorage().closeContext();

        // FIXME integrer ca dans le process normal d'init
        IsisFish.quit();
    }
    
    /**
     * Launch a simulation specialized for remote launch (caparmor).
     * 
     * Done operations : 
     *  - simulation zip import
     *  - pre script set
     *  - simulation
     *  - zip creation
     *  - checksum creation (zip creation name + .md5 extension)
     *  - delete simulation
     * 
     * @param simulationId id de simulation
     * @param simulationZip zip de la simulation
     * @param simulationResultZip simulation result zip
     * @throws Exception 
     */
    @Step(IsisConfig.STEP_AFTER_INIT_VCS)
    public static void simulateRemotelly(String simulationId, File simulationZip, File simulationResultZip) throws Exception {
        simulateRemotellyWithPreScript(simulationId, simulationZip, simulationResultZip, null);
    }

    /**
     * Launch a simulation specialized for remote launch (caparmor).
     * 
     * Done operations : 
     *  - simulation zip import
     *  - pre script set
     *  - simulation
     *  - zip creation
     *  - checksum creation (zip creation name + .md5 extension)
     *  - delete simulation
     * 
     * @param simulationId id de simulation
     * @param simulationZip zip de la simulation
     * @param simulationPrescript simulation prescript (can be null, empty)
     * @param simulationResultArchive simulation result archive (tbz2 or zip)
     * @throws Exception 
     */
    @Step(IsisConfig.STEP_AFTER_INIT_VCS)
    public static void simulateRemotellyWithPreScript(String simulationId, File simulationZip, File simulationResultArchive, File simulationPrescript)
            throws Exception {

        if (log.isInfoEnabled()) {
            log.info("Command line action : simulateRemotelly");
            log.info(" simulation id : " + simulationId);
            log.info(" simulation zip : " + simulationZip);
            log.info(" result archive : " + simulationResultArchive);
            log.info(" prescript file : " + simulationPrescript);
        }

        if (log.isDebugEnabled()) {
            log.debug("Timing : init isis simulation action : " + new java.util.Date());
        }
        
        String name = simulationId; // ne pas mettre la date, car le sub process la met deja  + " " + new SimpleDateFormat("yyyy-MM-dd-HH-mm").format(new Date());

        //SimulationStorage simulation = launcher.simulate(null, control, simulationZip);
        //simulation.getStorage().closeContext();
        String generatedPrescriptContent = null;
        if (simulationPrescript != null && simulationPrescript.canRead()) {
            generatedPrescriptContent = FileUtils.readFileToString(simulationPrescript, StandardCharsets.UTF_8);
        }

        SimulationControl control = new SimulationControl(name);
        control.setAutoSaveState(true); // needed for remote simulation

        SimulationItem item = new SimulationItem(control, null);
        item.setSimulationZip(simulationZip);
        item.setGeneratedPrescriptContent(generatedPrescriptContent);
        
        // lancement de la simulation
        SimulatorLauncher launcher = new InProcessSimulatorLauncher();
        launcher.simulate(SimulationService.getService(), item);
        SimulationStorage simulationStorage = launcher.getSimulationStorage(SimulationService.getService(), control);
        
        // make result archive after simulation
        if (log.isInfoEnabled()) {
            log.info("Compressing simulation as zip : " + simulationResultArchive);
            if (log.isDebugEnabled()) {
                log.debug("Timing : before zipping results : " + new java.util.Date());
            }
        }
        // md5 file name in complete simulationStorage.getFile() + ".md5"
        ZipUtil.compress(simulationResultArchive, simulationStorage.getFile(), null, true);
        if (log.isDebugEnabled()) {
            log.debug("Timing : after zipping results : " + new java.util.Date());
        }

        simulationStorage.closeStorage();
        
        // remove simulation
        if (log.isInfoEnabled()) {
            log.info("Deleting simulation : " + simulationStorage.getRoot());
        }
        simulationStorage.delete(false);

        if (log.isDebugEnabled()) {
            log.debug("Timing : end isis simulation action : " + new java.util.Date());
        }
        
        // FIXME integrer ca dans le process normal d'init
        IsisFish.quit();
    }
}
