/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2014 Ifremer, Code Lutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.actions;

import fr.ifremer.isisfish.config.IsisConfig;
import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.datastore.ResultStorage;
import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.simulator.SimulationControl;
import fr.ifremer.isisfish.simulator.launcher.InProcessSimulatorLauncher;
import fr.ifremer.isisfish.simulator.launcher.SimulationItem;
import fr.ifremer.isisfish.simulator.launcher.SimulationService;
import fr.ifremer.isisfish.simulator.launcher.SimulatorLauncher;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig.Action.Step;
import org.nuiton.math.matrix.MatrixND;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Check that there are no regressions.
 * Monitor execution.
 *
 */
public class ValidateSimulationAction {

    /** to use log facility, just put in your code: log.info("..."); */
    private static Log log = LogFactory.getLog(ValidateSimulationAction.class);

    private static final SimpleDateFormat df = new SimpleDateFormat("dd-MM-yy-HH:mm:ss.SS");

    protected IsisConfig config = null;

    /**
     * results from validated simulation
     */
    protected static ResultStorage validatedResultStorage;

    public ValidateSimulationAction(IsisConfig config) {
        this.config = config;
    }


    /**
     * Launch a simulation
     * with simulation params the similutaion at the given path.
     *
     * @param simulationName name of the simulation reference.
     * @throws Exception
     */
    @Step(IsisConfig.STEP_AFTER_INIT_VCS)
    public static void validateSimulation(String simulationName)
            throws Exception {
        SimulationStorage simulation = SimulationStorage.getSimulation(simulationName);
        validatedResultStorage = simulation.getResultStorage();

        simulateWithSimulation("Validate-From-" + simulationName + "-" + df.format(new Date()), simulation.createZip());
    }

    /**
     * Launch a simulation with specified simulationId and simulation zip.
     * 
     * @param simulationId id de simulation
     * @param simulationZip Zip de la simulation
     * @throws Exception 
     */
    @Step(IsisConfig.STEP_AFTER_INIT_VCS)
    public static void simulateWithSimulation(String simulationId, File simulationZip)
            throws Exception {
        // just call with null pre script
        validateWithSimulationAndScript(simulationId, simulationZip, null);
    }

    /**
     * Launch a simulation with specified simulationId, simulationZip
     * and simulationPrescript.
     *
     * @param simulationId id de simulation
     * @param simulationZip Zip de la simulation
     * @param simulationPrescript simulation prescript
     * @throws Exception
     */
    @Step(IsisConfig.STEP_AFTER_INIT_VCS)
    public static void validateWithSimulationAndScript(String simulationId, File simulationZip, File simulationPrescript)
            throws Exception {

        if (log.isInfoEnabled()) {
            log.info("Command line action : validateWithSimulationAndScript (" + simulationId + ", " + simulationZip.getAbsolutePath() + ", " + simulationPrescript + ")");
        }

        // ne pas mettre la date, car le sub process la met deja  + " " + new SimpleDateFormat("yyyy-MM-dd-HH-mm").format(new Date());
        String name = simulationId;

        //SimulationStorage simulation = launcher.simulate(null, control, simulationZip);
        //simulation.getStorage().closeContext();
        String generatedPrescriptContent = null;
        if (simulationPrescript != null && simulationPrescript.canRead()) {
            generatedPrescriptContent = FileUtils.readFileToString(simulationPrescript, StandardCharsets.UTF_8);
        }

        SimulationControl control = new SimulationControl(name);
        control.setAutoSaveState(true); // needed for remote simulation

        SimulationItem item = new SimulationItem(control, null);
        item.setSimulationZip(simulationZip);
        item.setGeneratedPrescriptContent(generatedPrescriptContent);

        // lancement de la simulation
        SimulatorLauncher launcher = new InProcessSimulatorLauncher();
        launcher.simulate(SimulationService.getService(), item);
        SimulationStorage simulation = launcher.getSimulationStorage(SimulationService.getService(), control);

        validateResults(simulation.getResultStorage());

        simulation.getStorage().closeContext();

        // FIXME integrer ca dans le process normal d'init
        IsisFish.quit();
    }

    /**
     * Compare result simulation from the validated ones.
     * @param newResultStorage
     * @throws Exception
     */
    public static void validateResults(ResultStorage newResultStorage) throws Exception {

        if (newResultStorage.getResultName().size() == validatedResultStorage.getResultName().size()) {
            log.info(t("isisfish.common.ok") + " nombre de résultats obtenus équivalent :" + newResultStorage.getResultName().size() + "/" + validatedResultStorage.getResultName().size());
        } else {
            log.error(t("isisfish.common.ko") + " nombre de résultats obtenus différent :" + newResultStorage.getResultName().size() + "/" + validatedResultStorage.getResultName().size());
        }

        List<String> simuNames = validatedResultStorage.getResultName();
        for (String simuName : simuNames) {
            MatrixND validatedMatrix = validatedResultStorage.getMatrix(simuName);
            MatrixND newMatrix = newResultStorage.getMatrix(simuName);
            if (newMatrix == null) {
                log.error(t("isisfish.common.ko") + " " + t("isisfish.error.validation.matrixNotComputed", validatedMatrix.getName()));
            } else {
                if (validatedMatrix.equalsValues(newMatrix)) {
                    log.info(t("isisfish.common.ok") + " MatrixND:" + validatedMatrix.getName());
                } else {
                    log.error(t("isisfish.common.ko") + " MatrixND:" + validatedMatrix.getName());
                }
            }
        }
    }

}
