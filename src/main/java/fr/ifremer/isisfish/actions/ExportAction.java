/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2012 Ifremer, Code Lutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.actions;

import static org.nuiton.i18n.I18n.t;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig.Action.Step;

import fr.ifremer.isisfish.config.IsisConfig;
import fr.ifremer.isisfish.datastore.SimulationPlanStorage;
import fr.ifremer.isisfish.datastore.DataStorage;
import fr.ifremer.isisfish.datastore.ExportStorage;
import fr.ifremer.isisfish.datastore.FormuleStorage;
import fr.ifremer.isisfish.datastore.RegionStorage;
import fr.ifremer.isisfish.datastore.RuleStorage;
import fr.ifremer.isisfish.datastore.ScriptStorage;
import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.datastore.SimulatorStorage;

/**
 * Export actions.
 * 
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class ExportAction {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    private static Log log = LogFactory.getLog(ExportAction.class);

    protected IsisConfig config = null;

    public ExportAction(IsisConfig config) {
        this.config = config;
    }

    protected void applyFilter(String filter, List<String> names) {
        if (filter != null && !"*".equals(filter)) {
            Pattern p = Pattern.compile(filter);
            // apply filter on names found
            names.removeIf(name -> !p.matcher(name).matches());
        }
    }
    
    protected void display(String filter, List<String> names) {
        StringBuilder sb = new StringBuilder();
        sb.append(" found ").append(names.size());
        sb.append(" with filter ").append(filter);
        sb.append(" in IsisFish");
        for (String name : names) {
            sb.append("\n").append(name);
        }
        System.out.println(sb.toString());
    }

    @Step(IsisConfig.STEP_AFTER_INIT_VCS)
    public void listSimulationPlan(String filter) throws Exception {
        List<String> names = SimulationPlanStorage.getSimulationPlanNames();
        applyFilter(filter, names);
        display(filter, names);
    }
    
    @Step(IsisConfig.STEP_AFTER_INIT_VCS)
    public void listExport(String filter) throws Exception {
        List<String> names = ExportStorage.getExportNames();
        applyFilter(filter, names);
        display(filter, names);
    }
    
    @Step(IsisConfig.STEP_AFTER_INIT_VCS)
    public void listRule(String filter) throws Exception {
        List<String> names = RuleStorage.getRuleNames();
        applyFilter(filter, names);
        display(filter, names);
    }

    @Step(IsisConfig.STEP_AFTER_INIT_VCS)
    public void listScript(String filter) throws Exception {
        List<String> names = ScriptStorage.getScriptNames();
        applyFilter(filter, names);
        display(filter, names);
    }

    @Step(IsisConfig.STEP_AFTER_INIT_VCS)
    public void listRegion(String filter) throws Exception {
        List<String> names = RegionStorage.getRegionNames();
        applyFilter(filter, names);
        display(filter, names);
    }

    @Step(IsisConfig.STEP_AFTER_INIT_VCS)
    public void listSimulation(String filter) throws Exception {
        List<String> names = SimulationStorage.getSimulationNames();
        applyFilter(filter, names);
        display(filter, names);
    }

    @Step(IsisConfig.STEP_AFTER_INIT_VCS)
    public void listSimulator(String filter) throws Exception {
        List<String> names = SimulatorStorage.getSimulatorNames();
        applyFilter(filter, names);
        display(filter, names);
    }

    @Step(IsisConfig.STEP_AFTER_INIT_VCS)
    public void listFormula(String filter) throws Exception {
        List<String> names = new ArrayList<>();
        for (String name : FormuleStorage.getCategories()) {
            names.addAll(FormuleStorage.getFormuleNames(name));
        }
        applyFilter(filter, names);
        display(filter, names);
    }

    //////////////////////////////////////////////////////////////////////////
    //
    //                           E X P O R T
    //
    //////////////////////////////////////////////////////////////////////////

    @Step(IsisConfig.STEP_AFTER_INIT_VCS)
    public void exportRegion(File file, String name, boolean force) throws Exception {
        log.info("file:" + file + ", name:" + name);
        if (!force && file.exists()) {
            throw new IllegalArgumentException(t("destination already exists %s use 'force' argument to force overwrite", file));
        }

        if (!RegionStorage.exists(name)) {
            // fatal error
            throw new IllegalArgumentException(t("could not found region %s", name));
        }
        DataStorage data = RegionStorage.getRegion(name);
        if (data != null) {
            data.createZip(file);
        }
    }
    
    @Step(IsisConfig.STEP_AFTER_INIT_VCS)
    public void exportSimulation(File file, String name, boolean force) throws Exception {
        log.info("exportSimulation in file " + file + " ( " + name + ", force = " + force + ")");
        if (!force && file.exists()) {
            throw new IllegalArgumentException(t("destination already exists %s use 'force' argument to force overwrite", file));
        }

        if (!SimulationStorage.exists(name)) {
            // fatal error
            throw new IllegalArgumentException(t("could not found simulation %s", name));
        }
        DataStorage data = SimulationStorage.getSimulation(name);
        if (data != null) {
            data.createZip(file);
        }
    }

    @Step(IsisConfig.STEP_AFTER_INIT_VCS)
    public void exportSimulationPlan(File file, String name, boolean force) throws Exception {
        if (!force && file.exists()) {
            throw new IllegalArgumentException(t("destination already exists %s use 'force' argument to force overwrite", file));
        }
        SimulationPlanStorage storage = SimulationPlanStorage.getSimulationPlan(name);
        if (!storage.exists()) {
            throw new IllegalArgumentException(t("could not found %s", name));
        }
        String content = storage.getContent();
        FileUtils.writeStringToFile(file, content, StandardCharsets.UTF_8);
    }

    @Step(IsisConfig.STEP_AFTER_INIT_VCS)
    public void exportRule(File file, String name, boolean force) throws Exception {
        if (!force && file.exists()) {
            throw new IllegalArgumentException(t("destination already exists %s use 'force' argument to force overwrite", file));
        }
        RuleStorage storage = RuleStorage.getRule(name);
        if (!storage.exists()) {
            throw new IllegalArgumentException(t("could not found %s", name));
        }
        String content = storage.getContent();
        FileUtils.writeStringToFile(file, content, StandardCharsets.UTF_8);
    }
    
    @Step(IsisConfig.STEP_AFTER_INIT_VCS)
    public void exportScript(File file, String name, boolean force) throws Exception {
        if (!force && file.exists()) {
            throw new IllegalArgumentException(t("destination already exists %s use 'force' argument to force overwrite", file));
        }
        ScriptStorage storage = ScriptStorage.getScript(name);
        if (!storage.exists()) {
            throw new IllegalArgumentException(t("could not found %s", name));
        }
        String content = storage.getContent();
        FileUtils.writeStringToFile(file, content, StandardCharsets.UTF_8);
    }
    
    @Step(IsisConfig.STEP_AFTER_INIT_VCS)
    public void exportExport(File file, String name, boolean force) throws Exception {
        if (!force && file.exists()) {
            throw new IllegalArgumentException(t("destination already exists %s use 'force' argument to force overwrite", file));
        }
        ExportStorage storage = ExportStorage.getExport(name);
        if (!storage.exists()) {
            throw new IllegalArgumentException(t("could not found %s", name));
        }
        String content = storage.getContent();
        FileUtils.writeStringToFile(file, content, StandardCharsets.UTF_8);
    }
    
    @Step(IsisConfig.STEP_AFTER_INIT_VCS)
    public void exportSimulator(File file, String name, boolean force) throws Exception {
        if (!force && file.exists()) {
            throw new IllegalArgumentException(t("destination already exists %s use 'force' argument to force overwrite", file));
        }
        SimulatorStorage storage = SimulatorStorage.getSimulator(name);
        if (!storage.exists()) {
            throw new IllegalArgumentException(t("could not found %s", name));
        }
        String content = storage.getContent();
        FileUtils.writeStringToFile(file, content, StandardCharsets.UTF_8);
    }
    
    @Step(IsisConfig.STEP_AFTER_INIT_VCS)
    public void exportFormula(File file, String category, String name, boolean force) throws Exception {
        if (!force && file.exists()) {
            throw new IllegalArgumentException(t("destination already exists %s use 'force' argument to force overwrite", file));
        }
        FormuleStorage storage = FormuleStorage.getFormule(category, name);
        if (!storage.exists()) {
            throw new IllegalArgumentException(t("could not found %s", name));
        }
        String content = storage.getContent();
        FileUtils.writeStringToFile(file, content, StandardCharsets.UTF_8);
    }
    
}
