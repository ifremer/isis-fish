/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2012 Ifremer, Code Lutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.actions;

import static org.nuiton.i18n.I18n.t;

import java.io.File;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.FileUtils;

import fr.ifremer.isisfish.config.IsisConfig;
import fr.ifremer.isisfish.datastore.SimulationPlanStorage;
import fr.ifremer.isisfish.datastore.ExportStorage;
import fr.ifremer.isisfish.datastore.FormuleStorage;
import fr.ifremer.isisfish.datastore.RegionStorage;
import fr.ifremer.isisfish.datastore.RuleStorage;
import fr.ifremer.isisfish.datastore.ScriptStorage;
import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.datastore.SimulatorStorage;

/**
 * Import actions.
 * 
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class ImportAction {

    protected IsisConfig config = null;

    public ImportAction(IsisConfig config) {
        this.config = config;
    }
    
    public void importSimulationPlan(boolean force, File file) throws Exception {
        String name = file.getName();
        SimulationPlanStorage storage = SimulationPlanStorage.getSimulationPlan(name);
        if(!force && storage.exists()) {
            throw new IllegalArgumentException(t("destination already exists %s use \'force\' argument to force overwrite", storage.getFile()));
        }
        String content = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
        storage.setContent(content);
    }

    public void importExport(boolean force, File file) throws Exception {
        String name = file.getName();
        ExportStorage storage = ExportStorage.getExport(name);
        if(!force && storage.exists()) {
            throw new IllegalArgumentException(t("destination already exists %s use \'force\' argument to force overwrite", storage.getFile()));
        }
        String content = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
        storage.setContent(content);
    }
    
    public void importRule(boolean force, File file) throws Exception {
        String name = file.getName();
        RuleStorage storage = RuleStorage.getRule(name);
        if(!force && storage.exists()) {
            throw new IllegalArgumentException(t("destination already exists %s use \'force\' argument to force overwrite", storage.getFile()));
        }
        String content = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
        storage.setContent(content);
    }
    
    public void importScript(boolean force, File file) throws Exception {
        String name = file.getName();
        ScriptStorage storage = ScriptStorage.getScript(name);
        if(!force && storage.exists()) {
            throw new IllegalArgumentException(t("destination already exists %s use \'force\' argument to force overwrite", storage.getFile()));
        }
        String content = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
        storage.setContent(content);
    }
    
    public void importSimulator(boolean force, File file) throws Exception {
        String name = file.getName();
        SimulatorStorage storage = SimulatorStorage.getSimulator(name);
        if(!force && storage.exists()) {
            throw new IllegalArgumentException(t("destination already exists %s use \'force\' argument to force overwrite", storage.getFile()));
        }
        String content = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
        storage.setContent(content);
    }

    public void importFormula(boolean force, String formulaType, File file) throws Exception {
        if (!FormuleStorage.getCategories().contains(formulaType)) {
            throw new IllegalArgumentException(t("Could not found formule type %s autorised type are %s", formulaType, FormuleStorage.getCategories()));
        }
        String name = file.getName();
        FormuleStorage storage = FormuleStorage.getFormule(formulaType, name);
        if(!force && storage.exists()) {
            throw new IllegalArgumentException(t("destination already exists %s use \'force\' argument to force overwrite", storage.getFile()));
        }
        String content = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
        storage.setContent(content);
    }
    
    /**
     * 
     * @param file zip file containing data
     * @param force
     * @throws java.lang.Exception
     */
    public void importRegion(boolean force, File file) throws Exception {
        //TODO test if region exists
        RegionStorage.importZip(file);
    }
    
    /**
     * 
     * @param file zip file containing data
     * @param name new region name
     * @param force
     * @throws java.lang.Exception
     */
    public void importRegionAndRename(boolean force, File file, String name) throws Exception {
        if (RegionStorage.exists(name) && !force) {
            // fatal error
            throw new IllegalArgumentException(t("region already exists %s use 'force' argument to force overwrite", name));
        }
        RegionStorage.importAndRenameZip(file, name);
    }

    /**
     * 
     * @param file zip file containing data
     * @param force
     * @throws java.lang.Exception
     */
    public void importSimulation(boolean force, File file) throws Exception {
        //TODO test if simulation exists
        SimulationStorage.importZip(file);
    }

    /**
     * Import zipped file containing all directory structure
     * 
     * @param file zip file containing data
     * @param force
     * @throws java.lang.Exception
     */
    public void importScriptModule(boolean force, File file) throws Exception {
//        ScriptActionHelper.importScript(file, force, false, null);
    }
    
}
