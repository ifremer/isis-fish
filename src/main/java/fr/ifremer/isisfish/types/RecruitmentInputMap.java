/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 Ifremer, Code Lutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.types;

import java.util.HashMap;

/**
 * Map instance to use a simplier object in model.
 * 
 * @since 4.4.0.0
 */
public class RecruitmentInputMap extends HashMap<Integer, RecruitmentInput> {

    /** serialVersionUID. */
    private static final long serialVersionUID = 2765995771062359295L;

    public RecruitmentInputMap() {
        super();
    }

    public RecruitmentInputMap(int initialCapacity) {
        super(initialCapacity);
    }
}
