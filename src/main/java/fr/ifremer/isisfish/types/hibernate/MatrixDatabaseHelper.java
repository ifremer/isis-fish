/*
 * #%L
 * IsisFish
 *
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2018 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.types.hibernate;

import fr.ifremer.isisfish.entities.Observation;
import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.entities.PopulationSeasonInfo;
import fr.ifremer.isisfish.entities.Result;
import fr.ifremer.isisfish.entities.StrategyMonthInfo;
import fr.ifremer.isisfish.entities.Variable;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Helper form database operation related to matrix.
 */
public class MatrixDatabaseHelper {

    protected static Map<Class, Collection<String>> datas = new HashMap<>();

    static {
        addColumn(Variable.class, Variable.PROPERTY_MATRIX_VALUE);
        addColumn(Result.class, Result.PROPERTY_MATRIX);
        addColumn(StrategyMonthInfo.class, StrategyMonthInfo.PROPERTY_PROPORTION_METIER);
        addColumn(Observation.class, Observation.PROPERTY_VALUE);
        addColumn(Population.class, Population.PROPERTY_RECRUITMENT_DISTRIBUTION);
        addColumn(Population.class, Population.PROPERTY_MAPPING_ZONE_REPRO_ZONE_RECRU);
        addColumn(Population.class, Population.PROPERTY_CAPTURABILITY);
        addColumn(PopulationSeasonInfo.class, PopulationSeasonInfo.PROPERTY_REPRODUCTION_DISTRIBUTION);
        addColumn(PopulationSeasonInfo.class, PopulationSeasonInfo.PROPERTY_LENGTH_CHANGE_MATRIX);
        addColumn(PopulationSeasonInfo.class, PopulationSeasonInfo.PROPERTY_MIGRATION_MATRIX);
        addColumn(PopulationSeasonInfo.class, PopulationSeasonInfo.PROPERTY_EMIGRATION_MATRIX);
        addColumn(PopulationSeasonInfo.class, PopulationSeasonInfo.PROPERTY_IMMIGRATION_MATRIX);
    }

    private static void addColumn(Class<?> keyClass, String column) {
        Collection<String> columns = datas.computeIfAbsent(keyClass, v -> new ArrayList<>());
        columns.add(column);
    }

    public static List<TopiaEntity> checkUsedObjects(TopiaContext tx, TopiaEntity entity) {
        List<TopiaEntity> usedEntities = new ArrayList<>();

        datas.forEach((clazz, columns) -> {
            String hql = "FROM " + clazz.getName() + " WHERE " +
            columns.stream() //
                .map(column -> column + MatrixType.HIBERNATE_COLUMN_NAME_SEPARATOR + MatrixType.PROPERTY_SEMANTICS + " LIKE :entityId") //
                .collect(Collectors.joining( " OR "));

            List entities = tx.findAll(hql, "entityId", "%" + entity.getTopiaId() + "%");
            usedEntities.addAll(entities);
        });

        return usedEntities;
    }
}
