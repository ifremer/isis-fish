/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2019 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.types.hibernate;

import fr.ifremer.isisfish.util.converter.ConverterUtil;
import fr.ifremer.isisfish.util.converter.TopiaEntityConverter;
import org.apache.commons.beanutils.ConvertUtilsBean;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.engine.jdbc.NonContextualLobCreator;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.hibernate.usertype.CompositeUserType;
import org.nuiton.math.matrix.MatrixFactory;
import org.nuiton.math.matrix.MatrixHelper;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.StringUtil;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * MatrixType.java
 *
 * Created: 23 janv. 2006
 *
 * @author Arnaud Thimel &lt;thimel@codelutin.com&gt;
 * @version $Revision$
 *
 * Mise a jour: $Date$
 * par : $Author$
 */
public class MatrixType implements CompositeUserType {

    /**
     * Logger for this class
     */
    private static final Log log = LogFactory.getLog(MatrixType.class);

    public static final String HIBERNATE_COLUMN_NAME_SEPARATOR = "_";

    @Override
    public boolean isMutable() {
        return true;
    }

    private String getDimToString(int[] dimArray) {
        String result = "[";
        String sep = "";
        for (int aDimArray : dimArray) {
            result += sep + aDimArray;
            sep = ", ";
        }
        result += "]";
        return result;
    }

    private int[] getDimFromString(String str) {
        str = str.trim();
        if(str.startsWith("[") && str.endsWith("]")) { 
            str = str.substring(1, str.length()-1); // remove [ and ]
        }
        String [] dimAsString = StringUtil.split(str, ",");
        int[] result = new int[dimAsString.length];
        int i = 0;
        for (String dim : dimAsString) {
            int val = Integer.parseInt(dim.trim()); 
            result[i++] = val;
        }
        return result;
    }

    private String getDimNamesToString(String[] dimNamesArray) {
        String result = "[";
        String sep = "";
        for (String aDimNamesArray : dimNamesArray) {
            result += sep + '"' + aDimNamesArray + '"';
            sep = ", ";
        }
        result += "]";
        return result;
    }

    private String[] getDimNamesFromString(String str) {
        str = str.trim();
        if(str.startsWith("[") && str.endsWith("]")) { 
            str = str.substring(1, str.length()-1); // remove [ and ]
        }
        String [] result = StringUtil.split(str, ",");
        for (int i=0; i<result.length; i++) {
            result[i] = result[i].trim();
            if(result[i].startsWith("\"") && result[i].endsWith("\"")) { 
                result[i] = result[i].substring(1, result[i].length()-1); // remove " and "
            }
        }
        return result;
    }

    private String getSemanticsToString(List[] semanticsArray) {
        StringBuffer result = new StringBuffer("[");
        for (int i=0; i<semanticsArray.length; i++) {
            result.append("[");
            List semantics = semanticsArray[i];
            for (Iterator it = semantics.iterator(); it.hasNext(); ) {
                appendString(result, it.next());
                if (it.hasNext()) {
                    result.append(", ");
                }
            }
            result.append("]");
            if ((i+1) < semanticsArray.length) {
                result.append(", ");
            }
        }
        return result.append("]").toString();
    }

    /**
     * 
     * @param name nom de la matrice
     * @param str la chaine representant la semantique
     * @param session la session dans lequel on lit la matrice de la base
     * @return
     */
    private List[] getSemanticsFromString(String name, String str, SharedSessionContractImplementor session) {
        str = str.trim();
        if(str.startsWith("[") && str.endsWith("]")) { 
            str = str.substring(1, str.length()-1); // remove [ and ]
        }
        String [] sems = StringUtil.split(str, ",");
        
        List[] result = new List[sems.length];
        
        for (int i=0; i<sems.length; i++) {
            result[i] = splitObjects(name, sems[i], session);
        }

        return result;
    }

    /**
     * Recréé chaque object de la chaine de caractere et l'ajoute dans une liste
     * la chaine est de la forme
     * 
     * [null(), java.lang.String("toto"), ...]
     * @param name le nom de la matrice
     * @param str la chaine representant
     */
    private List splitObjects(String name, String str, SharedSessionContractImplementor session) {
        List<Object> result = new LinkedList<>();
        str = str.trim();
        if (str.startsWith("[") && str.endsWith("]")) {
            str = str.substring(1, str.length() - 1);
        }
        String [] elems = StringUtil.split(str, ",");
        for (String elem : elems) {
            elem = elem.trim();
            int openbrace = elem.indexOf('(');
            String objectType = elem.substring(0, openbrace);
            String objectString = elem.substring(openbrace + 1, elem.length() - 1);
            
            if ("null".equals(objectType)) {
                result.add(null);
            } else {
                ConvertUtilsBean converter = getConverter(session);
                Object o;
                try {
                    o = converter.convert(objectString, Class.forName(objectType));
                } catch (Exception eee) {
                    // if can't create objet, put String representation as semantics
                    o = objectType + "(" + objectString + ")";
                    if (log.isWarnEnabled()) {
                        log.warn("Continuing but can't convert object in matrix " + name + " from String: '" + o + "'");
                    }
                    if (log.isDebugEnabled()) {
                        log.debug("Continuing can't convert object in matrix " + name + " from String: '" + o + "'", eee);
                    }
                }
                result.add(o);
            }
        }
        
        return result;
    }

    private static final Type[] hibernateTypes = {
        StandardBasicTypes.STRING, //name (String)
        StandardBasicTypes.STRING, //dim (int[])
        StandardBasicTypes.CLOB,   //dimNames (String[])
        StandardBasicTypes.CLOB,   //semantics (List[])
        StandardBasicTypes.CLOB    //data (List<List<...>>)
    };

    static final String PROPERTY_SEMANTICS = "semantics";
    private static final String[] propertyNames = {
        "name",
        "dim",
        "dimNames",
        PROPERTY_SEMANTICS,
        "data"
    };

    private StringBuffer appendString(StringBuffer buffer, Object o) {
        if (o == null) {
            buffer.append("null()");
        } else {
            String qualifiedName = o.getClass().getName();
            if (o instanceof TopiaEntity) {
                qualifiedName = TopiaEntity.class.getName();
            }
            buffer.append(qualifiedName).append("(");
            ConvertUtilsBean converter = getConverter(null);
            buffer.append(converter.convert(o));
            buffer.append(")");
        }
        return buffer;
    }

    private ConvertUtilsBean getConverter(SharedSessionContractImplementor session) {
        ConvertUtilsBean cub = ConverterUtil.getConverter(null);
        if (session != null) {
            // others type is already registered during application init
            cub.register(new TopiaEntityConverter(session), TopiaEntity.class);
        }
        return cub;
    }

    @Override
    public Class returnedClass() {
        return MatrixND.class;
    }

    @Override
    public boolean equals(Object x, Object y) throws HibernateException {
        if (x == y) {
            return true;
        } else if (x != null && y != null) {
            return x.equals(y);
        } else {
            return false;
        }
    }

    @Override
    public int hashCode(Object x) throws HibernateException {
        return x.hashCode();
    }

    @Override
    public Object deepCopy(Object value) throws HibernateException {
        if (value == null) {
            return null;
        }
        return MatrixFactory.getInstance().create((MatrixND)value);
    }

    @Override
    public String[] getPropertyNames() {
        return propertyNames;
    }

    @Override
    public Type[] getPropertyTypes() {
        return hibernateTypes;
    }

    @Override
    public Object getPropertyValue(Object component, int property) throws HibernateException {
        MatrixND matrix = (MatrixND)component;
        switch (property) {
        case 0 : return matrix.getName();
        case 1 : return matrix.getDim();
        case 2 : return matrix.getDimensionNames();
        case 3 : return matrix.getSemantics();
        case 4 : return matrix.toList();
        default : throw new HibernateException("Property index invalid : " + property);
        }
    }

    @Override
    public void setPropertyValue(Object component, int property, Object value) throws HibernateException {
        MatrixND matrix = (MatrixND)component;
        switch (property) {
        case 0 : matrix.setName((String)value); break;
        case 1 : throw new HibernateException("Modification of the dimension forbidden");
        case 2 : matrix.setDimensionNames((String[])value); break;
        case 3 :
            int index = 0;
            for (List semantics : (List[])value) {
                matrix.setSemantic(index, semantics);
                index++;
            }
            break;
        case 4 : matrix.fromList((List)value); break;
        default : throw new HibernateException("Property index invalid : " + property);
        }
    }

    @Override
    public Object nullSafeGet(ResultSet rs, String[] names, SharedSessionContractImplementor session, Object owner) throws HibernateException, SQLException {
        MatrixND matrix;
        
        String nameAsString = rs.getString(names[0]);
        String dimAsString = rs.getString(names[1]);
        String dimNamesAsString = rs.getString(names[2]);
        String semanticsAsString = rs.getString(names[3]);
        String datasAsString = rs.getString(names[4]);
        
        if (nameAsString == null && dimAsString == null && dimNamesAsString == null
                && semanticsAsString == null) { // no now we have "" for that && datasAsString == null) {
            matrix = null;
        } else {
            String name = nameAsString;
            int[] dim = getDimFromString(dimAsString);
            String[] dimNames = getDimNamesFromString(dimNamesAsString);
            List[] semantics = getSemanticsFromString(name, semanticsAsString, session);
            List datas = MatrixHelper.convertStringToList(datasAsString);
            
            matrix = MatrixFactory.getInstance().create(name, dim);
            matrix.setDimensionNames(dimNames);
            for (int i=0; i<semantics.length; i++) {
                matrix.setSemantic(i, semantics[i]);
            }
            matrix.fromList(datas);
            session.getPersistenceContext();
        }
        return matrix;
    }

    @Override
    public void nullSafeSet(PreparedStatement st, Object value, int index, SharedSessionContractImplementor session) throws HibernateException, SQLException {
        if (value == null) {
            st.setString(index, null);
            st.setString(index + 1, null);
            st.setString(index + 2, null);
            st.setString(index + 3, null);
            st.setClob(index + 4, NonContextualLobCreator.INSTANCE.createClob(""));
            
        } else {
            MatrixND matrix = MatrixFactory.getInstance().create((MatrixND)value);
            st.setString(index, matrix.getName());
            st.setString(index + 1, getDimToString(matrix.getDim()));
            st.setString(index + 2, getDimNamesToString(matrix.getDimensionNames()));
            st.setString(index + 3, getSemanticsToString(matrix.getSemantics()));
            st.setClob(index + 4, NonContextualLobCreator.INSTANCE.createClob(matrix.toList().toString()));
        }
    }

    @Override
    public Serializable disassemble(Object value, SharedSessionContractImplementor session) throws HibernateException {
        MatrixND matrix = (MatrixND)value;
        return new Serializable[] {
                matrix.getName(),
                getDimToString(matrix.getDim()),
                getDimNamesToString(matrix.getDimensionNames()),
                getSemanticsToString(matrix.getSemantics()),
                matrix.toList().toString()
        };
    }

    @Override
    public Object assemble(Serializable cached, SharedSessionContractImplementor session, Object owner) throws HibernateException {
        Serializable[] o = (Serializable[])cached;
        String name = (String)o[0];
        int[] dim = getDimFromString((String)o[1]);
        String[] dimNames = getDimNamesFromString((String)o[2]);
        List[] semantics = getSemanticsFromString(name, (String)o[3], session);
        List datas = MatrixHelper.convertStringToList((String)o[4]);
        MatrixND matrix = MatrixFactory.getInstance().create(name, dim);
        matrix.setDimensionNames(dimNames);
        for (int i=0; i<semantics.length; i++) {
            matrix.setSemantic(i, semantics[i]);
        }
        matrix.fromList(datas);
        return matrix;
    }

    @Override
    public Object replace(Object original, Object target, SharedSessionContractImplementor session, Object owner) throws HibernateException {
        return assemble( disassemble(original, session), session, owner);
    }

} //MatrixType
