/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2010 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.types;

import static org.nuiton.i18n.I18n.t;

import java.io.Serializable;

/**
 * TimeUnit.java
 *
 * Created: 20 janv. 2006 03:03:15
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class TimeUnit implements Cloneable, Serializable, Comparable<TimeUnit> {
    
    static public final double HOUR_PER_DAY = 24;
    
    /** serialVersionUID. */
    private static final long serialVersionUID = 1L;
    
    /** le temps en seconde */
    protected double time; 
    
    /**
     * @param time le temps en seconde
     */
    public TimeUnit(double time) {
        this.time = time;
    }
    
    
    /**
     * @return Returns the time in second
     */
    public double getTime() {
        return this.time;
    }
    
    /**
     * @param time The time to set in second
     */
    public TimeUnit setTime(double time) {
        this.time = time;
        return this;
    }
    
    public double getHour() {
        return time / 60.0 / 60.0;
    }
    
    public TimeUnit setHour(double hour) {
        this.time = hour * 60.0 * 60.0;
        return this;
    }
    
    public double getDay() {
        return getHour() / 24.0;
    }
    
    public TimeUnit setDay(double day) {
        setHour(day * 24.0);
        return this;
    }
    
    @Override
    public int compareTo(TimeUnit o) {
        return Double.compare(time, o.time);
    }

    @Override
    public String toString() {
        String result = "";
        if (getDay() >= 1) {
            result += getDay() + t("isisfish.timeUnit.day");
        } else {
            result += getHour() + t("isisfish.timeUnit.hours");
        }
        return result;
        
    }

}


