/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 Ifremer, Code Lutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.types;

/**
 * Data used to compute recruitement equation.
 * 
 * Contains:
 * <ul>
 * <li>Aboundance (for related month)</li>
 * <li>Biomass (for related month)</li>
 * <li>Repro (for related month)</li>
 * <li>recuitementContribution (can be null if outside recruitemnent period)</li>
 * </ul>
 *
 * @since 4.4.0.0
 */
public class RecruitmentInput extends ReproductionData {

    /**
     * RecuitementContribution (can be {@code null} if outside recruitement period).
     */
    protected Double recruitementContribution;

    public RecruitmentInput() {
        super(null, null, null);
    }

    public RecruitmentInput(ReproductionData data) {
        super(data.getAboundance(),
                data.getBiomass(),
                data.getRepro());
        
    }

    /**
     * RecuitementContribution
     * 
     * @return recuitementContribution (can be {@code null} if outside recruitement period).
     */
    public Double getRecruitementContribution() {
        return recruitementContribution;
    }
    
    /**
     * Set RecruitementContribution
     * 
     * @param recruitementContribution recuitementContribution (can be {@code null} if outside recruitement period).
     */
    public void setRecruitementContribution(Double recruitementContribution) {
        this.recruitementContribution = recruitementContribution;
    }
}
