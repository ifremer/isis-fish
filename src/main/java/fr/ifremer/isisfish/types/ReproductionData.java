/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 Ifremer, Code Lutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.types;

import org.nuiton.math.matrix.MatrixND;

/**
 * Aboundance, biomass and reproduction used during population migration.
 * 
 * @since 4.4.0.0
 */
public class ReproductionData {

    /** Aboundance. */
    protected MatrixND aboundance;
    
    /** Biomass. */
    protected MatrixND biomass;
    
    /** Reproduction. */
    protected MatrixND repro;
    
    public ReproductionData(MatrixND aboundance, MatrixND biomass, MatrixND repro) {
        this.aboundance = aboundance;
        this.biomass = biomass;
        this.repro = repro;
    }
    
    public MatrixND getAboundance() {
        return aboundance;
    }
    
    public MatrixND getBiomass() {
        return biomass;
    }
    
    public MatrixND getRepro() {
        return repro;
    }
}
