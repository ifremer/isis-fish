/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2015 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.types;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * RangeOfValues.java
 *
 * Created: 7 mars 2006 17:16:20
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class RangeOfValues implements Cloneable, Serializable {

    /** serialVersionUID. */
    private static final long serialVersionUID = 1L;

    public static final String TYPE_STRING = "String";
    public static final String TYPE_INTEGER = "Integer";
    public static final String TYPE_FLOAT = "Float";

    static final public String [] TYPES = new String[]{TYPE_STRING, TYPE_INTEGER, TYPE_FLOAT};

    static public String [] getPossibleTypes() {
        return TYPES;
    }
    
    protected String type = TYPE_STRING;
    protected boolean continueRange = false;
    protected String values = "";
    protected List<Object> possibleValues = null;
    
    public RangeOfValues(String typeValue) {
        setTypeValues(typeValue);
    }
    
    public RangeOfValues(String type, String value) {
        setType(type);
        setValues(value);
    }

    /**
     * @return Returns the type.
     */
    public String getType() {
        return this.type;
    }
    
    /**
     * @param type The type to set.
     */
    protected void setType(String type) {
        this.type = type;
    }
    
    /**
     * @return Returns the values.
     */
    public String getValues() {
        return this.values;
    }
    
    /**
     * @param values The values to set.
     */
    protected void setValues(String values) {
        this.values = values;
        possibleValues = null;
    }
    
    public boolean isContinueRange() {
        getPossibleValues(); // to compute continueRange
        return continueRange;
    }
    
    /**
     * Change type and values. String must be something like
     * Integer[1;3;4]
     * @param typeValues
     */
    protected void setTypeValues(String typeValues) {
        if (typeValues != null) {
            int begin = typeValues.indexOf('[');
            int end = typeValues.indexOf(']');

            String type = typeValues.substring(0, begin);
            String values = typeValues.substring(begin + 1, end);

            setType(type);
            setValues(values);
        }
    }
    
    public boolean contains(String val) {
        boolean result = false;
        if ("Integer".equals(getType())) {
            result = contains(Integer.parseInt(val));
        } else if ("Float".equals(getType())) {
            result = contains(Float.parseFloat(val));            
        } else {
            getPossibleValues().contains(val);
        }
        return result;
    }
    
    public boolean contains(int val) {
        boolean result;
        if (! isContinueRange()){
            result = getPossibleValues().contains(val);
        }else{
            int first = (Integer)getPossibleValues().get(0);
            int last = (Integer)getPossibleValues().get(1);
            result = first <= val && val <= last;
        }
        return result;
    }
    
    public boolean contains(float val) {
        boolean result;
        if (! isContinueRange()){
            result = getPossibleValues().contains(val);
        }else{
            double first = (Double)getPossibleValues().get(0);
            double last = (Double)getPossibleValues().get(1);
            result = first <= val && val <= last;
        }
        return result;
    }
    
    private Object stringToObject(String s){
        if("Integer".equals(getType()))
            return Integer.valueOf(s);
        else if("Float".equals(getType()))
            return Double.valueOf(s);
        else
            return s;
    }

    public List<Object> getPossibleValues() {
        if (possibleValues == null) {
            possibleValues = new ArrayList<>();
            
            String g = getValues();
            
            if(g.contains(";") || g.matches("^-?[^-]+$")){
                continueRange = false;
                String [] val = g.split(";");
                for (String aVal : val) {
                    possibleValues.add(stringToObject(aVal));
                }
            }else{
                continueRange = true;
                int first = 0;
                if(g.startsWith("-")){
                    first = 1;
                }
                first = g.indexOf("-", first);
                String min = g.substring(0, first);
                String max = g.substring(first+1);

                possibleValues.add(stringToObject(min));
                possibleValues.add(stringToObject(max));
            }

        }
        return possibleValues;
    }
    
    public String getAsString() {
        String result = getType() + "[" + getValues() + "]";
        return result;
    }

    @Override
    public String toString() {
        String result = getAsString();
        return result;
    }

}
