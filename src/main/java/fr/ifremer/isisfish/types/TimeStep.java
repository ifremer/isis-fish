/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2005 - 2011 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.types;

import java.io.Serializable;
import static org.nuiton.i18n.I18n.t;

/**
 * This object can't be modified. If this behavior is modified pay attention
 * if you used TimeStep as key in Map, in this case you will have error
 * 
 * @author poussin
 */
public class TimeStep implements Cloneable, Serializable, Comparable<TimeStep> { // TimeStep

    /** serialVersionUID. */
    private static final long serialVersionUID = 1L;

    protected int step = 0;

    public TimeStep() {
    }

    public TimeStep(int step) {
        this.step = step;
    }

    public int compareTo(TimeStep t) {
        if (this.before(t)) {
            return -1;
        } else if (this.after(t)) {
            return 1;
        } else {
            return 0;
        }
    }

    public int getYear() {
        return step / 12;
    }

    public Month getMonth() {
        int tmp = step;
        while (tmp < 0) {
            tmp += 12;
        }
        Month result = Month.MONTH[tmp % 12];
        return result;
    }

    /**
     * @deprecated since 4.0.0.0, use {@link #getStep()} instead
     * @return date
     */
    @Deprecated
    public int getDate() {
        return step;
    }

    public int getStep() {
        return step;
    }

    /**
     * Return new TimeStep equals to this.step + number
     * @param number number of step to add
     * @return new TimeStep
     */
    public TimeStep add(int number) {
        return new TimeStep(step + number);
    }

    /**
     * Return new TimeStep equals to this.step - number
     * @param number number of step to remove
     * @return new TimeStep
     */
    public TimeStep minus(int number) {
        return new TimeStep(step - number);
    }

    /**
     * Return number of step between current TimeStep and other TimeStep in argument
     * (this - other)
     * @param other
     * @return 
     */
    public int gap(TimeStep other) {
        return step - other.step;
    }

    /**
     * Method next retourne une nouvelle date qui est la date suivante de la
     * date courante. la date courante n'est pas modifier.
     * 
     * @return Date
     */
    public TimeStep next() {
        return new TimeStep(this.step + 1);
    }

    /**
     * Method preview retourne une nouvelle date qui est la date precedente de
     * la date courante la date courante n'est pas modifier.
     * 
     * @return Date
     */
    public TimeStep previous() {
        return new TimeStep(this.step - 1);
    }

    /**
     * Method next retourne une nouvelle date qui est le meme mois de l'annee
     * suivante de la date courante. la date courante n'est pas modifier.
     * 
     * @return Date
     */
    public TimeStep nextYear() {
        return new TimeStep(this.step + 12);
    }

    /**
     * Method preview retourne une nouvelle date qui est le meme mois de l'annee
     * precedente de la date courante la date courante n'est pas modifier.
     * 
     * @return Date
     */
    public TimeStep previousYear() {
        return new TimeStep(this.step - 12);
    }

    /**
     * compare 2 dates.
     * 
     * @return retourne vrai si t est strictement superieur
     */
    public boolean before(TimeStep t) {
        return this.step < t.step;
    }

    public boolean beforeOrEquals(TimeStep t) {
        return this.step <= t.step;
    }

    /**
     * compare 2 dates.
     * 
     * @return retourne vrai si t est strictement inferieur
     */
    public boolean after(TimeStep t) {
        return this.step > t.step;
    }

    public boolean afterOrEquals(TimeStep d) {
        return this.step >= d.step;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof TimeStep) {
            return step == ((TimeStep) o).step;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return step;
    }

    /**
     * @param timeStep2
     * @return month array
     */
    public Month[] getMonthBetweenDate(TimeStep timeStep2) {
        if (timeStep2.getStep() - getStep() < 0) {
            return new Month[0];
        }

        Month[] result = new Month[timeStep2.getStep() - getStep()];
        Month month = getMonth();
        for (int i = 0; i < result.length; i++) {
            month = month.next();
            result[i] = month;
        }
        return result;
    }

    @Override
    public String toString() {
        String result = t("isisfish.date.toString", getMonth(), getYear());
        return result;
    }

} // Date

