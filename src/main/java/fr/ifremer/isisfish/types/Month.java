/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2005 - 2010 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.types;

import static org.nuiton.i18n.I18n.t;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Month.
 *
 * Created: 7 nov. 2005
 *
 * @author Code Lutin Dev
 * @version $Revision$
 */
public class Month implements Cloneable, Serializable, Comparable<Month> {
    
    /** serialVersionUID. */
    private static final long serialVersionUID = 1L;
    
    public final static Month JANUARY = new Month(0);
    public final static Month FEBRUARY = new Month(1);
    public final static Month MARCH = new Month(2);
    public final static Month APRIL = new Month(3);
    public final static Month MAY = new Month(4);
    public final static Month JUNE = new Month(5);
    public final static Month JULY = new Month(6);
    public final static Month AUGUST = new Month(7);
    public final static Month SEPTEMBER = new Month(8);
    public final static Month OCTOBER = new Month(9);
    public final static Month NOVEMBER = new Month(10);
    public final static Month DECEMBER = new Month(11);

    public final static Month[] MONTH = new Month[] {
        JANUARY, 
        FEBRUARY, 
        MARCH,
        APRIL, 
        MAY, 
        JUNE, 
        JULY,
        AUGUST,
        SEPTEMBER,
        OCTOBER,
        NOVEMBER,
        DECEMBER     
    };

    public final static int [] NUMBERS_OF_DAYS = new int[]{
        31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
    };
    
    public final static int NUMBER_OF_MONTH = MONTH.length;
    public final static int MAX_MONTH_NUMBER = NUMBER_OF_MONTH - 1;

    protected int monthNumber = 0;
        
    public Month(int monthNumber) {
        setMonthNumber(monthNumber);
    }
        
    /**
     * @return Returns the monthNumber.
     */
    public int getMonthNumber() {
        return this.monthNumber;
    }
        
    /**
     * @param monthNumber The monthNumber to set.
     */
    public void setMonthNumber(int monthNumber) {
        this.monthNumber = monthNumber;
    }
    
    public Month next() {
        Month nextMonth = MONTH[(getMonthNumber() + 1) % NUMBER_OF_MONTH];
        return nextMonth;
    }

    public Month previous() {
        Month nextMonth = MONTH[(NUMBER_OF_MONTH + getMonthNumber() - 1)
                                    % NUMBER_OF_MONTH];
        return nextMonth;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + monthNumber;
        return result;
    }

    @Override
    public boolean equals(Object o) {    
        if (this == o) {
            return true;
        }
        if (o instanceof Month) {
            Month m = (Month)o;
            return this.getMonthNumber() == m.getMonthNumber();
        }
        return false;
    }

    @Override
    public String toString() {
        switch (getMonthNumber()) {
        case 0:
            return t("isisfish.month.january");
        case 1:
            return t("isisfish.month.february");
        case 2:
            return t("isisfish.month.march");
        case 3:
            return t("isisfish.month.april");
        case 4:
            return t("isisfish.month.may");
        case 5:
            return t("isisfish.month.june");
        case 6:
            return t("isisfish.month.july");
        case 7:
            return t("isisfish.month.august");
        case 8:
            return t("isisfish.month.september");
        case 9:
            return t("isisfish.month.october");
        case 10:
            return t("isisfish.month.november");
        case 11:
            return t("isisfish.month.december");

        default:
            return "" + getMonthNumber();
        }
    }

    public boolean before(Month d) {
        return this.getMonthNumber() < d.getMonthNumber();
    }
    
    public boolean after(Month d) {
        return this.getMonthNumber() > d.getMonthNumber();
    }
    
    public boolean beforeOrEquals(Month d) {
        return this.getMonthNumber() <= d.getMonthNumber();
    }
    
    public boolean afterOrEquals(Month d) {
        return this.getMonthNumber() >= d.getMonthNumber();
    }
    
    @Override
    public int compareTo(Month d) {
        if (this.before(d)) {
            return -1;
        } else if (this.after(d)) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * Get number of days.
     * 
     * @return number of day
     */
    public int getNumbersOfDays() {
        int result = NUMBERS_OF_DAYS[getMonthNumber()];
        return result;
    }
    
    /**
     * Return all month from first to last.
     * example: november, february return [november, decemer, january, february]
     * 
     * @param first
     * @param last
     * @return month list
     */
    static public List<Month> getMonths(Month first, Month last) {
        List<Month> result = new ArrayList<>();
        result.add(first);
        while (first.before(last)) {
            first = first.next();
            result.add(first);
        }
        return result;
    }

    
}
