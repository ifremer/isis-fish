/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2015 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.entities;

import fr.ifremer.isisfish.util.EvaluatorHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LazyInitializationException;

import fr.ifremer.isisfish.types.Month;

/**
 * PopulationGroupImpl.
 *
 * Created: 3 janv. 2006 15:26:53
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class PopulationGroupImpl extends PopulationGroupAbstract {

    /** Logger for this class. */
    private static final Log log = LogFactory.getLog(PopulationGroupImpl.class);

    /** serialVersionUID. */
    private static final long serialVersionUID = 1L;

    @Override
    public double getAge() {
        double result = 0;
        if (getPopulation() != null && getPopulation().getSpecies() != null) {
            if (getPopulation().getSpecies().isAgeGroupType()) {
                result = super.getAge();
            } else {
                result = getPopulation().getAge(getLength(), this)
                        / Month.NUMBER_OF_MONTH;
            }
        }
        return result;
    }

    @Override
    public double getLength() {
        double result = 0.0;
        if (getPopulation() != null && getPopulation().getSpecies() != null) {
            if (!getPopulation().getSpecies().isAgeGroupType()) {
                result = (getMinLength() + getMaxLength()) / 2.0;
            } else {
                result = getPopulation().getLength(
                        getAge() * Month.NUMBER_OF_MONTH, this);
            }
        }
        return result;
    }

    @Override
    public boolean isInLength(double length) {
        boolean result = getMinLength() <= length && length <= getMaxLength();
        return result;
    }

    @Override
    public double getMeanWeight() {
        double result = 0;
        Equation eq = getPopulation().getMeanWeight();
        if (eq != null) {
            try {
                result = eq.evaluate("group", this);
            } catch (Exception ex) {
                EvaluatorHelper.catchEvaluateException(ex, log);
            }
        }
        return result;
    }

    @Override
    public double getNaturalDeathRate(Zone zone) {
        double result = 0;
        Equation eq = getPopulation().getNaturalDeathRate();
        if (eq != null) {
            try {
                result = eq.evaluate("pop", this.getPopulation(), "group",
                        this, "zone", zone);
            } catch (Exception ex) {
                EvaluatorHelper.catchEvaluateException(ex, log);
            }
        }
        return result;
    }

    @Override
    public double getFishingMortalityOtherFleets(Zone zone) {
        double result = 0;
        Equation eq = getPopulation().getFishingMortalityOtherFleets();
        if (eq != null) {
            try {
                result = eq.evaluate("pop", this.getPopulation(), "group",
                    this, "zone", zone);
            } catch (Exception ex) {
                EvaluatorHelper.catchEvaluateException(ex, log);
            }
        }
        return result;
    }

    @Override
    public double getPrice() {
        double result = 0;
        Equation eq = getPopulation().getPrice();
        if (eq != null) {
            try {
                result = eq.evaluate("group", this);
            } catch (Exception ex) {
                EvaluatorHelper.catchEvaluateException(ex, log);
            }
        }
        return result;
    }

    @Override
    public double getMaturityOgive() {
        double result = 0;
        Equation eq = getPopulation().getMaturityOgiveEquation();
        if (eq != null) {
            try {
                result = eq.evaluate("group", this);
            } catch (Exception ex) {
                EvaluatorHelper.catchEvaluateException(ex, log);
            }
        }
        return result;
    }

    @Override
    public double getReproductionRate() {
        double result = 0;
        Equation eq = getPopulation().getReproductionRateEquation();
        if (eq != null) {
            try {
                result = eq.evaluate("group", this);
            } catch (Exception ex) {
                EvaluatorHelper.catchEvaluateException(ex, log);
            }
        }
        return result;
    }

    private String toStringCache;
    @Override
    public String toString() {
        // trop couteux d'appeler i18n, appele des millions de fois
        // et est vraiment raisonnable d'avoir des string != suivant le langage
        // quel impact dans les scripts utilisateurs ?
//        return t("isisfish.populationGroup.toString", this.getPopulation(),
//                this.getId());
        if (toStringCache == null) {
            try {
                toStringCache = this.getPopulation() + " Group " + this.getId();
            } catch (LazyInitializationException e) {
                // cette exception se produit quand le toString est appelé par exemple lors
                // du lancement d'une population mais que l'entité d'origine a été rechargée
                // depuis une précédente transation fermé (rechargement d'une simulation
                // avec ses facteurs)
                toStringCache = "Unknow Population Group " + this.getId();
            }
        }
        return toStringCache;
    }
}
