package fr.ifremer.isisfish.entities;

/*
 * #%L
 * ISIS-Fish
 * %%
 * Copyright (C) 2016 - 2017 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import fr.ifremer.isisfish.config.IsisConfig;
import fr.ifremer.isisfish.types.Month;
import fr.ifremer.isisfish.types.RangeOfValues;
import fr.ifremer.isisfish.types.TimeUnit;
import fr.ifremer.isisfish.util.matrix.EntitySemanticsDecorator;
import fr.ifremer.isisfish.util.matrix.MatrixCSVHelper;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.topia.persistence.EntityVisitor;
import org.nuiton.topia.persistence.TopiaEntity;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Classe permettant d'exporter en Json une region
 *
 * Format du JSON:
 *
 * {
 *   "#info": {
 *     "formatVersion": int
 *     "isisVersion": String
 *     "region": boolean if true contains one region, if false contains one Entity
 *     "root": toString representation of root entity
 *     "rootId": id of root entity
 *   },
 *   "#entities": {
 *    " &lt;id&gt;": {&lt;entity field&gt;},
 *    ...
 *   },
 *   "FisheryRegion" : [&lt;id&gt;],
 *   "Cell": [&lt;id&gt;, ...],
 *   "Zone": [&lt;id&gt;, ...],
 *    ...
 * }
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class RegionExportJson implements EntityVisitor {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(RegionExportJson.class);

    final static public int FORMAT_VERSION = 1;
    final static public String FORMAT_EXTENSION = ".isis.json.gz";

    protected LinkedList<TopiaEntity> toVisit;
    protected Set<String> visited;
    protected LinkedHashMap<String, List<String>> isisEntities;
    protected boolean doVisit = false;
    protected MatrixCSVHelper matrixCSVHelper;
    protected EntitySemanticsDecorator decorator;
    protected JsonGenerator g;

    public RegionExportJson(Writer w) {
        try {
            JsonFactory f = new JsonFactory();
            g = f.createGenerator(w)/*.useDefaultPrettyPrinter()*/;

            toVisit = new LinkedList<>();
            visited = new HashSet<>();
            matrixCSVHelper = new MatrixCSVHelper(
                    decorator = new EntitySemanticsDecorator());

            isisEntities = new LinkedHashMap<>();
            isisEntities.put("FisheryRegion", new ArrayList<>());
            isisEntities.put("Cell", new ArrayList<>());
            isisEntities.put("Zone", new ArrayList<>());
            isisEntities.put("Port", new ArrayList<>());
            isisEntities.put("Species", new ArrayList<>());
            isisEntities.put("Population", new ArrayList<>());
            isisEntities.put("Gear", new ArrayList<>());
            isisEntities.put("Metier", new ArrayList<>());
            isisEntities.put("TripType", new ArrayList<>());
            isisEntities.put("VesselType", new ArrayList<>());
            isisEntities.put("SetOfVessels", new ArrayList<>());
            isisEntities.put("Strategy", new ArrayList<>());
            isisEntities.put("Observation", new ArrayList<>());
        } catch (IOException eee) {
            throw new RuntimeException(eee);
        }
    }

    /**
     * Export la region et tous les elements de la region.
     * @param fisheryRegion
     */
    public void export(FisheryRegion fisheryRegion) {
        toVisit.addAll(fisheryRegion.getCell());
        toVisit.addAll(fisheryRegion.getZone());
        toVisit.addAll(fisheryRegion.getPort());
        toVisit.addAll(fisheryRegion.getSpecies());
        toVisit.addAll(fisheryRegion.getGear());
        toVisit.addAll(fisheryRegion.getMetier());
        toVisit.addAll(fisheryRegion.getTripType());
        toVisit.addAll(fisheryRegion.getVesselType()) ;
        toVisit.addAll(fisheryRegion.getSetOfVessels());
        toVisit.addAll(fisheryRegion.getStrategy());
        toVisit.addAll(fisheryRegion.getObservations());

        export(fisheryRegion, true);
    }

    /**
     * Export seulement l'entity et ses dépendances.
     * @param entity
     */
    public void export(TopiaEntity entity) {
        if (entity instanceof FisheryRegion) {
            export((FisheryRegion)entity);
        } else {
            export(entity, false);
        }
    }

    protected void export(TopiaEntity entity, boolean isRegion) {
        try {
            this.start();
            g.writeObjectFieldStart("#info");
            g.writeNumberField("formatVersion", FORMAT_VERSION);
            g.writeStringField("isisVersion", IsisConfig.getVersion());
            g.writeBooleanField("region", isRegion);
            g.writeStringField("root", entity.toString());
            g.writeStringField("rootId", entity.getTopiaId());
            g.writeEndObject();

            g.writeObjectFieldStart("#entities");

            toVisit.push(entity);

            TopiaEntity e;
            while(toVisit.peek() != null) {
                while((e = toVisit.poll()) != null) {
                    e.accept(this);
                }

                // add all TopiaEntity in matrix semantics
                Collection<Object> decoratedObject = decorator.getDecoratedObject();
                for (Object o : decoratedObject) {
                    if (o instanceof TopiaEntity && !visited.contains(((TopiaEntity)o).getTopiaId())) {
                        toVisit.add((TopiaEntity)o);
                    }
                }
            }

            g.writeEndObject();
            this.end();
        } catch (IOException eee) {
            throw new RuntimeException(eee);
        }
    }

    protected void writeValue(Class<?> type, Object value) {
        try {
            if (value == null) {
                g.writeNull();
            } else if (TopiaEntity.class.isAssignableFrom(type)) {
                g.writeString(((TopiaEntity)value).getTopiaId());
                toVisit.add((TopiaEntity)value);
            } else if (MatrixND.class.isAssignableFrom(type)) {
                MatrixND m = (MatrixND)value;
                String s = matrixCSVHelper.writeMatrix(m.getName(), m);
                g.writeString(s);
            } else if (TimeUnit.class.isAssignableFrom(type)) {
                g.writeNumber(((TimeUnit)value).getTime());
            } else if (RangeOfValues.class.isAssignableFrom(type)) {
                g.writeString(((RangeOfValues)value).getAsString());
            } else if (Month.class.isAssignableFrom(type)) {
                g.writeNumber(((Month)value).getMonthNumber());
            } else if (Number.class.isAssignableFrom(type) || value instanceof Number) {
                g.writeNumber(((Number)value).doubleValue());
            } else if (String.class.isAssignableFrom(type)) {
                g.writeString(((String)value));
            } else if (Boolean.class.isAssignableFrom(type) || value instanceof Boolean) {
                g.writeBoolean(((Boolean)value));
            } else if (Class.class.isAssignableFrom(type)) {
                g.writeString(((Class)value).getName());
            } else {
                String message = String.format("Unsupported type: '%s' value class '%s' value '%s'", type, value.getClass().getName(), value);
                log.error(message);
                g.writeString(message);
//                throw new RuntimeException(message);
            }
        } catch (IOException eee) {
            throw new RuntimeException(eee);
        }
    }

    public void start() {
        try {
            g.writeStartObject();
        } catch (IOException eee) {
            throw new RuntimeException(eee);
        }
    }
    
    public void end() {
        try {
            for (Map.Entry<String, List<String>> e : isisEntities.entrySet()) {
                g.writeArrayFieldStart(e.getKey());
                for (String id : e.getValue()) {
                    g.writeString(id);
                }
                g.writeEndArray();
            }
            g.writeEndObject();
            g.close();
        } catch (IOException eee) {
            throw new RuntimeException(eee);
        }
    }

    /**
     * Start the visit of the given entity.
     *
     * @param entity the visited entity
     */
    @Override
    public void start(TopiaEntity entity) {
        try {
            String id = entity.getTopiaId();
            doVisit = !visited.contains(id);
            if (doVisit) {
                visited.add(id);
                g.writeObjectFieldStart(id);
                String className = entity.getClass().getName();
                String name = StringUtils.removeEnd(entity.getClass().getSimpleName(), "Impl");
                List<String> list = isisEntities.get(name);
                if (list != null) {
                    list.add(id);
                }

                g.writeStringField("#class", className);
                g.writeStringField("#id", id);
                g.writeStringField("#toString", entity.toString());
            }
        } catch (IOException eee) {
            throw new RuntimeException(eee);
        }
    }

    /**
     * Ends the visit of the given entity.
     *
     * @param entity the visited entity
     */
    @Override
    public void end(TopiaEntity entity) {
        try {
            if (doVisit) {
                g.writeEndObject();
            }
        } catch (IOException eee) {
            throw new RuntimeException(eee);
        }
    }

    /**
     * Visit a none indexed property for the given entity.
     *
     * The property visited is defined by the other parameters.
     *
     * @param entity       the visited entity
     * @param propertyName the name of the visited property
     * @param type         the type of the visited property
     * @param value        the value of the visited property
     */
    @Override
    public void visit(TopiaEntity entity, String propertyName, Class<?> type, Object value) {
        try {
            if (doVisit) {
                g.writeFieldName(propertyName);
                writeValue(type, value);
            }
        } catch (IOException eee) {
            throw new RuntimeException(eee);
        }
    }

    /**
     * Visit a collection property for the given entity.
     *
     * The property visited is defined by the other parameters.
     *
     * @param entity         the visited entity
     * @param propertyName   the name of the visited property
     * @param collectionType the type of the visited collection
     * @param type           the type of the visited property
     * @param value          the value of the visited property
     */
    @Override
    public void visit(TopiaEntity entity, String propertyName,
               Class<?> collectionType, Class<?> type, Object value) {
        try {
            if (doVisit) {
                g.writeFieldName(propertyName);
                g.writeStartArray();
                if (value != null) {
                    for (Object currentValue : (Collection<?>)value) {
                        writeValue(type, currentValue);
                    }
                }
                g.writeEndArray();
            }
        } catch (IOException eee) {
            throw new RuntimeException(eee);
        }
    }

    /**
     * Visit a indexed value from a collection property for the given entity.
     *
     * The property visited is defined by the other parameters.
     *
     * @param entity         the visited entity
     * @param propertyName   the name of the visited property
     * @param collectionType the type of the container of the visited property
     * @param type           the type of the visited property
     * @param index          the index of the visited property in his container
     * @param value          the value of the visited property
     */
    @Override
    public void visit(TopiaEntity entity, String propertyName,
               Class<?> collectionType, Class<?> type, int index, Object value) {
        if (doVisit) {
            throw new UnsupportedOperationException("FIXME a faire, implantation des properties array");
        }
    }

    /**
     * Reset all states of the visitor.
     *
     * If you use internal states inside the visitor, this method should clean
     * all of them.
     *
     * This method should be invoked after usage of the visitor.
     */
    @Override
    public void clear() {
        toVisit = null;
        visited = null;
        isisEntities = null;
        matrixCSVHelper = null;
        decorator = null;
    }

}
