/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2010 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.entities;

import static org.nuiton.i18n.I18n.t;

import fr.ifremer.isisfish.util.EvaluatorHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;

import fr.ifremer.isisfish.IsisFishDAOHelper;
import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.equation.Language;
import fr.ifremer.isisfish.equation.SelectivityEquation;

/**
 * SelectivityImpl.
 *
 * Created: 20 janv. 2006 03:29:14
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class SelectivityImpl extends SelectivityAbstract {

    /** serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** to use log facility, just put in your code: log.info(\"...\"); */
    private static Log log = LogFactory.getLog(SelectivityImpl.class);

    @Override
    public double getCoefficient(Population pop, PopulationGroup group,
            Metier metier) {
        double result = 0;
        try {
            Equation eq = getEquation();
            // poussin 20060823: modification par rapport a la version 2, on ne passe
            // plus age et length mais pop et group sur lequel on pourra recuperer
            // age et length. Ca evite de calculer age et length si on ne les utilise
            // pas dans l'equation
            result = eq.evaluate("pop", pop, "group", group, "metier",
                    metier);
        } catch (Exception ex) {
            EvaluatorHelper.catchEvaluateException(ex, log);
        }
        return result;
    }

    @Override
    public Equation getEquation() {
        if (super.getEquation() == null) {
            Equation eq;
            try {
                EquationDAO dao = IsisFishDAOHelper
                        .getEquationDAO(getTopiaContext());
                eq = dao.create();
                eq.setCategory("Selectivity");
                eq.setLanguage(Language.JAVA);
                eq.setJavaInterface(SelectivityEquation.class);
                eq.update();
            } catch (TopiaException eee) {
                throw new IsisFishRuntimeException("Can't create equation");
            }
            super.setEquation(eq);
        }
        return super.getEquation();
    }

    /**
     * Equation content setter for Equation.
     * 
     * @param content content to set
     */
    public void setEquationContent(String content) {
        try {
            Equation eq = getEquation();

            // can't be null

            // Fire
            String _oldValue = eq.getContent();
            fireOnPreWrite("content", _oldValue, content);

            eq.setContent(content);
            eq.update();

            fireOnPostWrite("content", _oldValue, content);

        } catch (TopiaException eee) {
            throw new IsisFishRuntimeException(
                    t("isisfish.error.change.equation"), eee);
        }
    }

    @Override
    public void setGear(Gear value) {
        super.setGear(value);
        setEquationName(getEquation());
    }

    @Override
    public void setPopulation(Population value) {
        super.setPopulation(value);
        setEquationName(getEquation());
    }

    protected void setEquationName(Equation eq) {
        if (getGear() != null && getPopulation() != null) {
            eq.setName(getGear().getName() + " - " + getPopulation().getName()
                    + "(" + getPopulation().getSpecies().getName() + ")");
        }
    }

    @Override
    public String toString() {
        String result = "" + getEquation();
        return result;
    }

}
