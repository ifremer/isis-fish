/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2012 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.entities;

import static org.nuiton.i18n.I18n.t;

import org.apache.commons.beanutils.BeanUtils;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.persistence.TopiaEntity;

import fr.ifremer.isisfish.IsisFishDAOHelper;
import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.equation.Language;
import fr.ifremer.isisfish.equation.VariableEquation;

public class VariableImpl extends VariableAbstract {

    private static final long serialVersionUID = 3978428224373810278L;

    /*
    private static Log log = LogFactory.getLog(VariableImpl.class); */

    public VariableImpl() {
        setType(VariableType.DOUBLE);
    }

    /*@Override
    public void setDoubleValue(double doubleValue) {
        super.setDoubleValue(doubleValue);
        super.setMatrixValue(null);
        
        deletePreviousEquation();
        super.setEquationValue(null);
    }

    /*
     * Manually delete equation since topia (or hibernate) can't delete
     * orphan object with many-to-one relations.
     *
    protected void deletePreviousEquation() {
        if (super.getEquationValue() != null) {
            try {
                IsisFishDAOHelper.getEquationDAO(getTopiaContext()).delete(super.getEquationValue());
            } catch (TopiaException ex) {
                if (log.isErrorEnabled()) {
                    log.error("Can't delete equation", ex);
                }
            }
        }
    }

    @Override
    public void setEquationValue(Equation equationValue) {
        super.setEquationValue(equationValue);
        super.setDoubleValue(0);
        super.setMatrixValue(null);
    }

    @Override
    public void setMatrixValue(MatrixND matrixValue) {
        super.setMatrixValue(matrixValue);
        super.setDoubleValue(0);
        
        deletePreviousEquation();
        super.setEquationValue(null);
    }*/

    public void setEquationValueContent(String content) {
        try {
            Equation eq = getEquationValue();

            if (eq == null) {
                EquationDAO dao = IsisFishDAOHelper
                        .getEquationDAO(getTopiaContext());
                // create Growth equation
                eq = dao.create();
                eq.setCategory("Variable");
                eq.setJavaInterface(VariableEquation.class);
                eq.setLanguage(Language.JAVA);
                setEquationValue(eq);
            }

            eq.setName(getName());

            // Fire
            String _oldValue = eq.getContent();
            fireOnPreWrite("content", _oldValue, content);

            eq.setContent(content);
            eq.update();

            fireOnPostWrite("content", _oldValue, content);

        } catch (TopiaException eee) {
            throw new IsisFishRuntimeException(t("isisfish.change.equation"), eee);
        }
    }

    @Override
    public String toString() {

        String result;
        try {
            TopiaEntity entity = getTopiaContext().findByTopiaId(getEntityId());
            result = BeanUtils.getProperty(entity, "name");
        } catch (Exception ex) {
            result = entityId;
        }
        
        result += "." + name;
        return result;
    }

} //VariableImpl
