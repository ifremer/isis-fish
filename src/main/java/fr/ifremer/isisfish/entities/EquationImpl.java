/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2010 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.entities;

import static org.nuiton.i18n.I18n.t;

import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.datastore.FormuleStorage;
import fr.ifremer.isisfish.equation.Language;
import fr.ifremer.isisfish.simulator.SimulationContext;
import fr.ifremer.isisfish.util.EvaluatorHelper;

/**
 * EquationImpl.
 *
 * Created: 20 janv. 2006 03:43:01
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class EquationImpl extends EquationAbstract {

    /** serialVersionUID. */
    private static final long serialVersionUID = -2195404062556810283L;

    /**
     * Evalue l'equation et retourne le résultat.
     *
     * @param args equation args (arg name and arg value)
     * @return equation evaluation
     */
    public double evaluate(Object... args) {
        String eq = getContent();
        try {

            // default is BSH
            if (Language.JAVA.equals(getLanguage()) || getJavaInterface() != null) {

                // in Java, we don't need args names, only args value ordered
                // build a new array with SimulationContext in first position
                Object[] params = new Object[args.length / 2 + 1];
                params[0] = SimulationContext.get();
                for (int i = 0; i < args.length / 2; i++) {
                    params[i + 1] = args[i*2 + 1];
                }

                // default Java if there are javaInterface
                Object val = EvaluatorHelper.evaluate(
                        FormuleStorage.FORMULE_PATH, getTopiaId(),
                        getJavaInterface(), eq, params);
                if (val instanceof Number) {
                    double result = ((Number) val).doubleValue();
                    return result;
                } else {
                    throw new IsisFishRuntimeException(t("isisfish.error.equation.return.number", eq));
                }
            } else {
                // TODO other language support
                throw new IsisFishRuntimeException(t("isisfish.error.unsupported.equation.langage",
                        getLanguage(), eq));
            }
        } catch (Exception eee) {
            throw new IsisFishRuntimeException(t("isisfish.error.evaluate.equation", eq), eee);
        }
    }

    /**
     * Evalue l'equation et retourne le necessaryResult
     *
     * @return equation evaluation
     */
    public String[] evaluateNecessaryResult() {
        String eq = getContent();
        try {
            if (Language.JAVA.equals(getLanguage()) || getJavaInterface() != null) {

                // default Java if there are javaInterface
                String[] result = EvaluatorHelper.evaluateNecessaryResult(
                        FormuleStorage.FORMULE_PATH, getTopiaId(),
                        getJavaInterface(), getContent());
                return result;
            } else {
                // TODO other language support
                throw new IsisFishRuntimeException(t("isisfish.error.unsupported.equation.langage", getLanguage(), eq));
            }
        } catch (Exception eee) {
            String message = "Can't evaluate equation";
            if (getJavaInterface() != null) {
                message += " (" + getJavaInterface().getSimpleName() + ")";
            }
            throw new IsisFishRuntimeException(message + " : " + eq, eee);
        }
    }

    @Override
    public String toString() {
        String result = getName() + "(" + getCategory() + ")";
        return result;
    }
}
