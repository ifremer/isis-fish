/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2015 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.entities;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import fr.ifremer.isisfish.util.EvaluatorHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixException;
import org.nuiton.math.matrix.MatrixFactory;
import org.nuiton.math.matrix.MatrixIterator;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.topia.TopiaException;

import fr.ifremer.isisfish.IsisFishDAOHelper;
import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.equation.Language;
import fr.ifremer.isisfish.equation.PopulationCapturabilityEquation;
import fr.ifremer.isisfish.equation.PopulationFishingMortalityOtherFleets;
import fr.ifremer.isisfish.equation.PopulationGrowth;
import fr.ifremer.isisfish.equation.PopulationGrowthReverse;
import fr.ifremer.isisfish.equation.PopulationMaturityOgiveEquation;
import fr.ifremer.isisfish.equation.PopulationMeanWeight;
import fr.ifremer.isisfish.equation.PopulationNaturalDeathRate;
import fr.ifremer.isisfish.equation.PopulationPrice;
import fr.ifremer.isisfish.equation.PopulationRecruitmentEquation;
import fr.ifremer.isisfish.equation.PopulationReproductionEquation;
import fr.ifremer.isisfish.equation.PopulationReproductionRateEquation;
import fr.ifremer.isisfish.types.Month;
import fr.ifremer.isisfish.types.RecruitmentInputMap;
import fr.ifremer.isisfish.types.TimeStep;

/**
 * PopulationImpl.
 *
 * Created: 11 janv. 2006 20:16:27
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class PopulationImpl extends PopulationAbstract {

    /** Logger for this class. */
    private static final Log log = LogFactory.getLog(PopulationImpl.class);

    /** serialVersionUID */
    private static final long serialVersionUID = 1L;

    /**
     * Surcharge car avec une aggregation (lien fort) il ne faut pas remplacer
     * la collection, mais vider celle qui existe et y mettre les nouveaux elements
     * sinon hibernate perd la tete et leve des exceptions (detecter lors de l'implantation
     * de l'import json)
     */
    @Override
    public void setPopulationGroup(List<PopulationGroup> populationGroup) {
        clearPopulationGroup();
        addAllPopulationGroup(populationGroup);
    }

    /**
     * Surcharge car avec une aggregation (lien fort) il ne faut pas remplacer
     * la collection, mais vider celle qui existe et y mettre les nouveaux elements
     * sinon hibernate perd la tete et leve des exceptions (detecter lors de l'implantation
     * de l'import json)
     */
    @Override
    public void setPopulationSeasonInfo(List<PopulationSeasonInfo> populationSeasonInfo) {
        clearPopulationSeasonInfo();
        addAllPopulationSeasonInfo(populationSeasonInfo);
    }

    /**
     * Surcharge car avec une aggregation (lien fort) il ne faut pas remplacer
     * la collection, mais vider celle qui existe et y mettre les nouveaux elements
     * sinon hibernate perd la tete et leve des exceptions (detecter lors de l'implantation
     * de l'import json)
     */
    @Override
    public void setPopulationZone(List<Zone> populationZone) {
        clearPopulationZone();
        addAllPopulationZone(populationZone);
    }

    /**
     * Overwrite delete.
     * @throws TopiaException 
     */
    @Override
    public void delete() throws TopiaException {
        // chatellier 20090317 fix hibernate exception
        // object will be resaved ...
        // FIXME should be fixed on topia maybe ?
        this.getSpecies().getPopulation().remove(this);

        super.delete();
    }

    @Override
    public String toString() {
        return getName();
    }

    /**
     * Override to change name of equation.
     * 
     * @see fr.ifremer.isisfish.entities.PopulationAbstract#setName(java.lang.String)
     */
    @Override
    public void setName(String value) {
        super.setName(value);
        updateEquationName();
    }

    private void updateEquationName() {
        Equation eq;
        eq = getGrowth();
        if (eq != null) {
            eq.setName(getName());
        }
        eq = getGrowthReverse();
        if (eq != null) {
            eq.setName(getName());
        }
        eq = getMeanWeight();
        if (eq != null) {
            eq.setName(getName());
        }
        eq = getNaturalDeathRate();
        if (eq != null) {
            eq.setName(getName());
        }
        eq = getPrice();
        if (eq != null) {
            eq.setName(getName());
        }
        eq = getReproductionEquation();
        if (eq != null) {
            eq.setName(getName());
        }
        eq = getRecruitmentEquation();
        if (eq != null) {
            eq.setName(getName());
        }
        eq = getMaturityOgiveEquation();
        if (eq != null) {
            eq.setName(getName());
        }
        eq = getReproductionRateEquation();
        if (eq != null) {
            eq.setName(getName());
        }
    }

    public void setReproductionEquationContent(String content) {
        try {
            Equation eq = getReproductionEquation();

            if (eq == null) {
                EquationDAO dao = IsisFishDAOHelper
                        .getEquationDAO(getTopiaContext());
                // create equation
                eq = dao.create();
                eq.setCategory("Reproduction");
                eq.setJavaInterface(PopulationReproductionEquation.class);
                eq.setLanguage(Language.JAVA);
                setReproductionEquation(eq);
            }

            eq.setName(getName());

            // Fire
            String _oldValue = eq.getContent();
            fireOnPreWrite("content", _oldValue, content);

            eq.setContent(content);
            eq.update();

            fireOnPostWrite("content", _oldValue, content);

        } catch (TopiaException eee) {
            throw new IsisFishRuntimeException(
                    t("isisfish.error.change.equation"), eee);
        }
    }
    
    public void setRecruitmentEquationContent(String content) {
        try {
            Equation eq = getRecruitmentEquation();

            if (eq == null) {
                EquationDAO dao = IsisFishDAOHelper
                        .getEquationDAO(getTopiaContext());
                // create equation
                eq = dao.create();
                eq.setCategory("Recruitment");
                eq.setJavaInterface(PopulationRecruitmentEquation.class);
                eq.setLanguage(Language.JAVA);
                setRecruitmentEquation(eq);
            }

            eq.setName(getName());

            // Fire
            String _oldValue = eq.getContent();
            fireOnPreWrite("content", _oldValue, content);

            eq.setContent(content);
            eq.update();

            fireOnPostWrite("content", _oldValue, content);

        } catch (TopiaException eee) {
            throw new IsisFishRuntimeException(
                    t("isisfish.error.change.equation"), eee);
        }
    }

    public void setGrowthContent(String content) {
        try {
            Equation eq = getGrowth();

            if (eq == null) {
                EquationDAO dao = IsisFishDAOHelper
                        .getEquationDAO(getTopiaContext());
                // create Growth equation
                eq = dao.create();
                eq.setCategory("Growth");
                eq.setJavaInterface(PopulationGrowth.class);
                eq.setLanguage(Language.JAVA);
                setGrowth(eq);
            }

            eq.setName(getName());

            // Fire
            String _oldValue = eq.getContent();
            fireOnPreWrite("content", _oldValue, content);

            eq.setContent(content);
            eq.update();

            fireOnPostWrite("content", _oldValue, content);

        } catch (TopiaException eee) {
            throw new IsisFishRuntimeException(t("isisfish.change.equation"),
                    eee);
        }
    }

    public void setGrowthReverseContent(String content) {
        try {
            Equation eq = getGrowthReverse();

            if (eq == null) {
                EquationDAO dao = IsisFishDAOHelper
                        .getEquationDAO(getTopiaContext());
                // create equation
                eq = dao.create();
                eq.setCategory("GrowthReverse");
                eq.setJavaInterface(PopulationGrowthReverse.class);
                eq.setLanguage(Language.JAVA);
                setGrowthReverse(eq);
            }

            eq.setName(getName());

            // Fire
            String _oldValue = eq.getContent();
            fireOnPreWrite("content", _oldValue, content);

            eq.setContent(content);
            eq.update();

            fireOnPostWrite("content", _oldValue, content);

        } catch (TopiaException eee) {
            throw new IsisFishRuntimeException(
                    t("isisfish.error.change.equation"), eee);
        }
    }

    public void setNaturalDeathRateContent(String content) {
        try {
            Equation eq = getNaturalDeathRate();

            if (eq == null) {
                EquationDAO dao = IsisFishDAOHelper
                        .getEquationDAO(getTopiaContext());
                // create equation
                eq = dao.create();
                eq.setCategory("NaturalDeathRate");
                eq.setJavaInterface(PopulationNaturalDeathRate.class);
                eq.setLanguage(Language.JAVA);
                setNaturalDeathRate(eq);
            }

            eq.setName(getName());

            // Fire
            String _oldValue = eq.getContent();
            fireOnPreWrite("content", _oldValue, content);

            eq.setContent(content);
            eq.update();

            fireOnPostWrite("content", _oldValue, content);

        } catch (TopiaException eee) {
            throw new IsisFishRuntimeException(
                    t("isisfish.error.change.equation"), eee);
        }
    }
    
    public void setFishingMortalityOtherFleetsContent(String content) {
        try {
            Equation eq = getFishingMortalityOtherFleets();

            if (eq == null) {
                EquationDAO dao = IsisFishDAOHelper
                        .getEquationDAO(getTopiaContext());
                // create equation
                eq = dao.create();
                eq.setCategory("FishingMortalityOtherFleets");
                eq.setJavaInterface(PopulationFishingMortalityOtherFleets.class);
                eq.setLanguage(Language.JAVA);
                setFishingMortalityOtherFleets(eq);
            }

            eq.setName(getName());

            // Fire
            String _oldValue = eq.getContent();
            fireOnPreWrite("content", _oldValue, content);

            eq.setContent(content);
            eq.update();

            fireOnPostWrite("content", _oldValue, content);

        } catch (TopiaException eee) {
            throw new IsisFishRuntimeException(
                    t("isisfish.error.change.equation"), eee);
        }
    }

    public void setMeanWeightContent(String content) {
        try {
            Equation eq = getMeanWeight();

            if (eq == null) {
                EquationDAO dao = IsisFishDAOHelper
                        .getEquationDAO(getTopiaContext());
                // create equation
                eq = dao.create();
                eq.setCategory("MeanWeight");
                eq.setJavaInterface(PopulationMeanWeight.class);
                eq.setLanguage(Language.JAVA);
                setMeanWeight(eq);
            }

            eq.setName(getName());

            // Fire
            String _oldValue = eq.getContent();
            fireOnPreWrite("content", _oldValue, content);

            eq.setContent(content);
            eq.update();

            fireOnPostWrite("content", _oldValue, content);

        } catch (TopiaException eee) {
            throw new IsisFishRuntimeException(
                    t("isisfish.error.change.equation"), eee);
        }
    }
    
    public void setMaturityOgiveEquationContent(String content) {
        try {
            Equation eq = getMaturityOgiveEquation();

            if (eq == null) {
                EquationDAO dao = IsisFishDAOHelper
                        .getEquationDAO(getTopiaContext());
                // create MaturityOgive equation
                eq = dao.create();
                eq.setCategory("MaturityOgive");
                eq.setJavaInterface(PopulationMaturityOgiveEquation.class);
                eq.setLanguage(Language.JAVA);
                setMaturityOgiveEquation(eq);
            }

            eq.setName(getName());

            // Fire
            String _oldValue = eq.getContent();
            fireOnPreWrite("content", _oldValue, content);

            eq.setContent(content);
            eq.update();

            fireOnPostWrite("content", _oldValue, content);

        } catch (TopiaException eee) {
            throw new IsisFishRuntimeException(t("isisfish.change.equation"),
                    eee);
        }
    }
    
    public void setReproductionRateEquationContent(String content) {
        try {
            Equation eq = getReproductionRateEquation();

            if (eq == null) {
                EquationDAO dao = IsisFishDAOHelper
                        .getEquationDAO(getTopiaContext());
                // create ReproductionRate equation
                eq = dao.create();
                eq.setCategory("ReproductionRate");
                eq.setJavaInterface(PopulationReproductionRateEquation.class);
                eq.setLanguage(Language.JAVA);
                setReproductionRateEquation(eq);
            }

            eq.setName(getName());

            // Fire
            String _oldValue = eq.getContent();
            fireOnPreWrite("content", _oldValue, content);

            eq.setContent(content);
            eq.update();

            fireOnPostWrite("content", _oldValue, content);

        } catch (TopiaException eee) {
            throw new IsisFishRuntimeException(t("isisfish.change.equation"),
                    eee);
        }
    }

    public void setPriceContent(String content) {
        try {
            Equation eq = getPrice();

            if (eq == null) {
                EquationDAO dao = IsisFishDAOHelper
                        .getEquationDAO(getTopiaContext());
                // create equation
                eq = dao.create();
                eq.setCategory("Price");
                eq.setJavaInterface(PopulationPrice.class);
                eq.setLanguage(Language.JAVA);
                setPrice(eq);
            }

            eq.setName(getName());

            // Fire
            String _oldValue = eq.getContent();
            fireOnPreWrite("content", _oldValue, content);

            eq.setContent(content);
            eq.update();

            fireOnPostWrite("content", _oldValue, content);

        } catch (TopiaException eee) {
            throw new IsisFishRuntimeException(
                    t("isisfish.error.change.equation"), eee);
        }
    }

    @Override
    public double getNaturalDeathBirth(Zone zone) {
        double result = 0;
        Equation eq = getNaturalDeathRate();
        if (eq != null) {
            try {
                result = eq.evaluate("pop", this, "group", null, "zone", zone);
            } catch (Exception ex) {
                EvaluatorHelper.catchEvaluateException(ex, log);
            }
        }
        return result;
    }

    /**
     * Return Matrix [groups x zones] of Natural death rate.
     * 
     * @return a matrix
     */
    @Override
    public MatrixND getNaturalDeathRateMatrix() {
        List<PopulationGroup> groups = getPopulationGroup();
        List<Zone> zones = getPopulationZone();

        MatrixND result = MatrixFactory.getInstance().create(
                "Natural death rate", new List[] { groups, zones },
                new String[] { "Groups", "Zones" });

        for (MatrixIterator i = result.iterator(); i.hasNext();) {
            i.next();
            PopulationGroup group = (PopulationGroup) i
                    .getSemanticsCoordinates()[0];
            Zone zone = (Zone) i.getSemanticsCoordinates()[1];

            double value = group.getNaturalDeathRate(zone);
            i.setValue(value);
        }

        return result;
    }
    
    /**
     * Return Matrix [groups x zones] of Fishing mortality other fleets.
     * 
     * @return a matrix
     */
    @Override
    public MatrixND getFishingMortalityOtherFleetsMatrix() {
        List<PopulationGroup> groups = getPopulationGroup();
        List<Zone> zones = getPopulationZone();

        MatrixND result = MatrixFactory.getInstance().create(
                "Fishing mortality other fleets", new List[] { groups, zones },
                new String[] { "Groups", "Zones" });

        for (MatrixIterator i = result.iterator(); i.hasNext();) {
            i.next();
            PopulationGroup group = (PopulationGroup) i
                    .getSemanticsCoordinates()[0];
            Zone zone = (Zone) i.getSemanticsCoordinates()[1];

            double value = group.getFishingMortalityOtherFleets(zone);
            i.setValue(value);
        }

        return result;
    }

    @Override
    public PopulationSeasonInfo getPopulationSeasonInfo(Month month) {
        Collection<PopulationSeasonInfo> all = getPopulationSeasonInfo();
        PopulationSeasonInfo result = null;
        for (PopulationSeasonInfo psi : all) {
            if (psi.containsMonth(month)) {
                result = psi;
                break;
            }
        }
        return result;
    }

    /**
     * Compute age of group with length
     * @param length length of group
     * @param group group
     * @return age in month
     */
    @Override
    public double getAge(double length, PopulationGroup group) {
        double result = 0;
        Equation growth = getGrowthReverse();
        if (growth != null) {
            try {
                result = growth.evaluate("length", length, "group", group);
            } catch (Exception ex) {
                EvaluatorHelper.catchEvaluateException(ex, log);
            }
        }
        return result;
    }

    /**
     * Compute length of group with age.
     * 
     * @param age age of group in month
     * @param group group
     * @return length of group
     */
    @Override
    public double getLength(double age, PopulationGroup group) {
        double result = 0;
        Equation growth = getGrowth();
        if (growth != null) {
            try {
                result = growth.evaluate("age", age, "group", group);
            } catch (Exception ex) {
                EvaluatorHelper.catchEvaluateException(ex, log);
            }
        }
        return result;
    }

    @Override
    public void setCapturability(MatrixND value) {
        List[] sems = value.getSemantics();

        boolean allNull = true;
        for (List l : sems) {
            for (Object o : l) {
                allNull = (o == null);
            }
        }

        // toutes les semantiques de la matrice sont null
        // on cree une nouvelle matrice avec les bonnes dimensions et les
        // bonnes semantiques avec les valeurs de la matrice passee en parametre
        if (allNull) {
            List[] newsems = new List[]{getPopulationGroup(),
                getPopulationSeasonInfo(),};

            MatrixND newmat = MatrixFactory.getInstance().create(
                n("isisfish.population.capturability"),
                newsems,
                new String[] { n("isisfish.population.group"),
                            n("isisfish.population.season") });
            newmat.paste(value);
            value = newmat;
        }
        super.setCapturability(value);
    }

    @Override
    public MatrixND getCapturability() {
        MatrixND mat = super.getCapturability();
        // check the validity
        if (sizePopulationGroup() == 0 || sizePopulationSeasonInfo() == 0) {
            log.warn("Population doesn't have population group or population season info");
            return null;
        }
        List<PopulationGroup> groups = getPopulationGroup();
        List<PopulationSeasonInfo> seasons = getPopulationSeasonInfo();
        List[] sems = new List[] {groups, seasons};
        if (mat == null) {
            log.debug("Capturability is null, create new matrix");
            mat = MatrixFactory.getInstance().create(
                    n("isisfish.population.capturability"),
                    sems,
                    new String[] {  n("isisfish.population.group"),
                        n("isisfish.population.season") });
            // we don't call setCapturability because is better to create a valid
            // matrix when capturability is null instead check validity and 
            // create new one and paste the old
        } else if (!Arrays.equals(mat.getSemantics(), sems)) {
            MatrixND newmat = MatrixFactory.getInstance().create(
                    n("isisfish.population.capturability"),
                    sems,
                    new String[] {  n("isisfish.population.group"),
                        n("isisfish.population.season") });

            boolean allNull = true;
            for (List l : mat.getSemantics()) {
                for (Object o : l) {
                    allNull = (o == null);
                }
            }

            // les dimensions de la matrice en base sont toutes null, on
            // fait en simple copy de matrice sans utiliser la semantique
            if (allNull) {
                newmat.paste(mat);
            } else {
                newmat.pasteSemantics(mat);
            }
            mat = newmat;
            // perhaps call setCapturability, but if possible wait the user
            // call setCapturability explicitly with this new matrix
        }

        // compute equation
        if (isCapturabilityEquationUsed()) {
            try {
                for (int g = 0; g < groups.size(); g++) {
                    PopulationGroup group = groups.get(g);
                    for (int s = 0; s < seasons.size(); s++) {
                        PopulationSeasonInfo season = seasons.get(s);

                        Equation eq = getCapturabilityEquation();
                        double capturability = eq.evaluate("pop", this,
                                "group", group, "season", season);

                        mat.setValue(g, s, capturability);
                    }
                }
            } catch (Exception ex) {
                EvaluatorHelper.catchEvaluateException(ex, log);
            }
        }

        return mat;
    }

    public void setCapturabilityEquationContent(String content) {
        try {
            Equation eq = getCapturabilityEquation();

            if (eq == null) {
                EquationDAO dao = IsisFishDAOHelper
                        .getEquationDAO(getTopiaContext());
                // create equation
                eq = dao.create();
                eq.setCategory("Capturability");
                eq.setJavaInterface(PopulationCapturabilityEquation.class);
                eq.setLanguage(Language.JAVA);
                setCapturabilityEquation(eq);
            }

            eq.setName(getName());

            // Fire
            String _oldValue = eq.getContent();
            fireOnPreWrite("content", _oldValue, content);

            eq.setContent(content);
            eq.update();

            fireOnPostWrite("content", _oldValue, content);

        } catch (TopiaException eee) {
            throw new IsisFishRuntimeException(
                    t("isisfish.error.change.equation"), eee);
        }
    }

    @Override
    public MatrixND getMappingZoneReproZoneRecru() {
        if (sizeReproductionZone() == 0 || sizeRecruitmentZone() == 0) {
            log.warn("Population doesn't have zone reproduction or zone recruitment");
            return null;
        }
        MatrixND mat = super.getMappingZoneReproZoneRecru();
        List[] sems = new List[] { getReproductionZone(), getRecruitmentZone(), };
        if (mat == null) {
            mat = MatrixFactory.getInstance().create(
                    n("isisfish.population.mappingZoneReproZoneRecru"),
                    sems,
                    new String[] {  n("isisfish.population.reproduction"),
                        n("isisfish.population.recruitment") });
            // we don't call setMappingZoneReproZoneRecru because is better to create a valid
            // matrix when MappingZoneReproZoneRecru is null instead check validity and 
            // create new one and paste the old
        } else if (!Arrays.equals(mat.getSemantics(), sems)) {
            MatrixND newmat = MatrixFactory.getInstance().create(
                    n("isisfish.population.mappingZoneReproZoneRecru"),
                    sems,
                    new String[] {  n("isisfish.population.reproduction"),
                        n("isisfish.population.recruitment") });
            newmat.paste(mat);
            mat = newmat;
        }
        return mat;
    }

    /**
     * Convertie une matrice N 2D en une matrice N 1D strucutre pour les calculer.
     * 
     * N2D[classes, zones] --&gt; N1D[classe1(zone1, zone2, ...), classe2(zone1, zone2, ...)]
     *  
     * @see #split2D(MatrixND)
     */
    public MatrixND N2DToN1D(MatrixND N) {
        int maxX = N.getDim(0);
        int maxY = N.getDim(1);

        MatrixND result = MatrixFactory.getInstance().create(
                n("isisfish.population.matrixAbundance1D"),
                new int[] { 1, maxX * maxY });

        for (int x = 0; x < maxX; x++) {
            for (int y = 0; y < maxY; y++) {
                result.setValue(0, x * maxY + y, N.getValue(x, y));
            }
        }
        return result;
    }

    /**
     * A partir d'une matrice N structure selon l'ecriture matricielle
     * en classe puis en zone, construit une matrice 2D en Classe(X)
     * et en Zone(Y)
     * 
     * @see #N2DToN1D(MatrixND)
     */
    public MatrixND split2D(MatrixND N) {
        List<Zone> zones = getPopulationZone();
        List<PopulationGroup> groups = getPopulationGroup();
        int nbZone = zones.size();

        MatrixND result = MatrixFactory.getInstance().create(
                n("isisfish.population.matrixAbundance"),
                new List[] { groups, zones },
                new String[] {  n("isisfish.population.groups"),
                    n("isisfish.population.zones") });

        try {
            int c = 0;
            int z = 0;
            for (MatrixIterator mi = N.iterator(); mi.next();) {
                result.setValue(c, z, mi.getValue());
                if (z + 1 == nbZone) {
                    c++;
                }
                z = (z + 1) % nbZone;
            }
        } catch (MatrixException eee) {
            log.warn("MatrixException", eee);
        }
        return result;
    }

    @Override
    public MatrixND getRecruitmentMatrix(TimeStep step, Population pop, RecruitmentInputMap recruitmentInputs,
            MatrixND result) {

        Equation e = pop.getRecruitmentEquation();
        if (e != null) {
            try {
                // l'equation doit mettre les resultats dans la matrice result
                e.evaluate("step", step, "pop", pop, "recruitmentInputs", recruitmentInputs,
                    "result", result);
            } catch (Exception ex) {
                EvaluatorHelper.catchEvaluateException(ex, log);
            }
        }

        return result;
    }

}
