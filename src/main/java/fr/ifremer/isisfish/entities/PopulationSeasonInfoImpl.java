/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2010 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.entities;

import static org.nuiton.i18n.I18n.t;
import static org.nuiton.i18n.I18n.n;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fr.ifremer.isisfish.util.EvaluatorHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixFactory;
import org.nuiton.math.matrix.MatrixHelper;
import org.nuiton.math.matrix.MatrixIterator;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.topia.TopiaException;

import fr.ifremer.isisfish.IsisFishDAOHelper;
import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.equation.EmigrationEquation;
import fr.ifremer.isisfish.equation.ImmigrationEquation;
import fr.ifremer.isisfish.equation.Language;
import fr.ifremer.isisfish.equation.MigrationEquation;
import fr.ifremer.isisfish.types.Month;

/**
 * PopulationSeasonInfoImpl.java
 *
 * Created: 18 mars 2006 15:24:56
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class PopulationSeasonInfoImpl extends PopulationSeasonInfoAbstract {

    /** serialVersionUID. */
    private static final long serialVersionUID = 1375563617129149755L;

    /** to use log facility, just put in your code: log.info(\"...\"); */
    private static Log log = LogFactory.getLog(PopulationSeasonInfoImpl.class);

    public PopulationSeasonInfoImpl() {
        setSimpleLengthChangeMatrix(true);
    }

    @Override
    public MatrixND getReproductionDistribution() {
        MatrixND result = super.getReproductionDistribution();
        List oldMonths = null;

        if (result != null) {
            oldMonths = result.getSemantic(0);
        }

        List<Month> months = getMonths();

        if (!months.equals(oldMonths)) {
            MatrixND tmp = MatrixFactory.getInstance().create(
                    n("isisfish.populationSeasonInfo.distributionSpawing"),
                    new List[] { months },
                    new String[] {  n("isisfish.populationSeasonInfo.months") });
            tmp.pasteSemantics(result);
            result = tmp;
        }

        return result;
    }

    /**
     * overload to return matrix if no matrix available. The matrix returned
     * by this method can't be used directly because it contains only the
     * migration (departure != arrival) and not the coefficient that stay on
     * the same zone. This coefficient is calculated with over coefficient to
     * have 1 as sum of all coefficient.
     * 
     * @see fr.ifremer.isisfish.entities.PopulationSeasonInfoAbstract#getMigrationMatrix()
     */
    @Override
    public MatrixND getMigrationMatrix() {
        // check the validity
        MatrixND mat = super.getMigrationMatrix();
        if (getPopulation() != null) {
            List groups = getPopulation().getPopulationGroup();
            List zones = getPopulation().getPopulationZone();
            List[] sems = new List[] { groups, zones, zones };

            if (mat == null) {
                mat = MatrixFactory.getInstance().create(
                        n("isisfish.populationSeasonInfo.migration"),
                        sems,
                        new String[] {
                            n("isisfish.populationSeasonInfo.group"),
                            n("isisfish.populationSeasonInfo.departure"),
                            n("isisfish.populationSeasonInfo.arrival") });
                // we don't call setCapturability because is better to create a valid
                // matrix when capturability is null instead check validity and 
                // create new one and paste the old
            } else if (!Arrays.equals(mat.getSemantics(), sems)) {
                MatrixND newmat = MatrixFactory.getInstance().create(
                        n("isisfish.populationSeasonInfo.migration"),
                        sems,
                        new String[] {
                            n("isisfish.populationSeasonInfo.group"),
                            n("isisfish.populationSeasonInfo.departure"),
                            n("isisfish.populationSeasonInfo.arrival") });
                newmat.pasteSemantics(mat);
                mat = newmat;
                // perhaps call setCapturability, but if possible wait the user
                // call setCapturability explicitly with this new matrix
            }
        }
        return mat;
    }

    @Override
    public MatrixND getEmigrationMatrix() {
        // check the validity
        MatrixND mat = super.getEmigrationMatrix();
        if (getPopulation() != null) {
            List groups = getPopulation().getPopulationGroup();
            List zones = getPopulation().getPopulationZone();
            List[] sems = new List[] { groups, zones };

            if (mat == null) {
                mat = MatrixFactory.getInstance().create(
                        n("isisfish.populationSeasonInfo.emigration"),
                        sems,
                        new String[] {
                            n("isisfish.populationSeasonInfo.group"),
                            n("isisfish.populationSeasonInfo.departure") });
                // we don't call setCapturability because is better to create a valid
                // matrix when capturability is null instead check validity and 
                // create new one and paste the old
            } else if (!Arrays.equals(mat.getSemantics(), sems)) {
                MatrixND newmat = MatrixFactory.getInstance().create(
                        n("isisfish.populationSeasonInfo.emigration"),
                        sems,
                        new String[] {
                            n("isisfish.populationSeasonInfo.group"),
                            n("isisfish.populationSeasonInfo.departure") });
                newmat.pasteSemantics(mat);
                mat = newmat;
                // perhaps call setCapturability, but if possible wait the user
                // call setCapturability explicitly with this new matrix
            }
        }
        return mat;
    }

    @Override
    public MatrixND getImmigrationMatrix() {
        // check the validity
        MatrixND mat = super.getImmigrationMatrix();
        if (getPopulation() != null) {
            List groups = getPopulation().getPopulationGroup();
            List zones = getPopulation().getPopulationZone();
            List[] sems = new List[] { groups, zones };

            if (mat == null) {
                mat = MatrixFactory.getInstance().create(
                        n("isisfish.populationSeasonInfo.immigration"),
                        sems,
                        new String[] {
                            n("isisfish.populationSeasonInfo.group"),
                            n("isisfish.populationSeasonInfo.arrival") });
                // we don't call setCapturability because is better to create a valid
                // matrix when capturability is null instead check validity and 
                // create new one and paste the old
            } else if (!Arrays.equals(mat.getSemantics(), sems)) {
                MatrixND newmat = MatrixFactory.getInstance().create(
                        n("isisfish.populationSeasonInfo.immigration"),
                        sems,
                        new String[] {
                            n("isisfish.populationSeasonInfo.group"),
                            n("isisfish.populationSeasonInfo.arrival") });
                newmat.pasteSemantics(mat);
                mat = newmat;
                // perhaps call setCapturability, but if possible wait the user
                // call setCapturability explicitly with this new matrix
            }
        }
        return mat;
    }

    @Override
    public double getCapturability(PopulationGroup group) {
        double result = 0;
        if (getPopulation() != null) {
            MatrixND captu = getPopulation().getCapturability();
            result = captu.getValue(group, this);
        }
        return result;
    }

    /**
     * Create not initialized no spacialized change group matrix.
     * 
     * @return not initialized no spacialized change group matrix
     */
    public MatrixND createNoSpacializedChangeGroupMatrix() {
        Population pop = getPopulation();

        List<PopulationGroup> groups = pop.getPopulationGroup();

        MatrixND result = MatrixFactory.getInstance().create(
                new List[] { groups, groups });
        return result;
    }

    @Override
    public MatrixND getLengthChangeMatrix() {
        MatrixND matrix = super.getLengthChangeMatrix();

        // moved here dur to init bug in isis 3.3.0.x
        if (matrix == null && getPopulation() != null) {
            matrix = createNoSpacializedChangeGroupMatrix();
        }

        return matrix;
    }

    /**
     * Convert spacialized matrix to non spacialized matrix.
     * 
     * @param mat spacialized matrix
     * @return non spacialized matrix
     */
    public MatrixND unspacializeLengthChangeMatrix(MatrixND mat) {

        Population pop = getPopulation();

        int nbsecteurs = pop.sizePopulationZone();
        int nbclasses = pop.sizePopulationGroup();

        MatrixND smallmat = createNoSpacializedChangeGroupMatrix();

        for (int i = 0; i < nbclasses; i++) {
            for (int j = 0; j < nbclasses; j++) {
                double val = mat.getValue(i * nbsecteurs, j * nbsecteurs);
                smallmat.setValue(i, j, val);
            }
        }
        return smallmat;
    }

    /**
     * Create not initialized spacialized change group matrix.
     * 
     * @return not initialized spacialized change group matrix
     */
    protected MatrixND createSpacializedChangeGroupMatrix() {
        Population pop = getPopulation();

        List<Zone> zones = pop.getPopulationZone();
        List<PopulationGroup> groups = pop.getPopulationGroup();

        List<String> semantique = new ArrayList<>();

        for (PopulationGroup group : groups) {
            for (Zone zone : zones) {
                semantique.add(group + "/" + zone);
            }
        }
        
        // EC-20090710, changement de l'ordre
        // les semantiques sont maintenant regroupée
        // par zone (auparavent par population)
        /*for (Zone zone : zones) {
            for (PopulationGroup group : groups) {
                semantique.add(group + "/" + zone);
            }
        }*/

        MatrixND result = MatrixFactory.getInstance().create(
                new List[] { semantique, semantique });
        return result;
    }

    /**
     * Convert no spacialized matrix to spacialized matrix.
     * 
     * @param mat no spacialized matrix
     * @return spacialized matrix
     */
    public MatrixND spacializeLengthChangeMatrix(MatrixND mat) {

        Population pop = getPopulation();

        int nbsecteurs = pop.sizePopulationZone();
        int nbclasses = pop.sizePopulationGroup();

        MatrixND bigmat = createSpacializedChangeGroupMatrix();

        for (int i = 0; i < nbclasses; i++) {
            for (int j = 0; j < nbclasses; j++) {
                MatrixND matId = MatrixFactory.getInstance().matrixId(
                        nbsecteurs);
                matId.mults(mat.getValue(i, j));
                bigmat.paste(new int[] { i * nbsecteurs, j * nbsecteurs },
                        matId);
            }
        }

        // EC-20090710, changement de l'ordre
        // les semantiques sont maintenant regroupée
        // par zone (auparavent par population)
        /*for (int i = 0; i < nbsecteurs; i++) {
            for (int j = 0; j < nbsecteurs; j++) {
                MatrixND matId = MatrixFactory.getInstance().matrixId(
                        nbclasses);
                matId.mults(mat.getValue(i, j));
                bigmat.paste(new int[] { i * nbclasses, j * nbclasses },
                        matId);
            }
        }*/
        return bigmat;
    }

    /**
     * Create default non spacialized matrix group changement.
     */
    public MatrixND computeLengthChangeMatrix() {

        int NB_DISCRETISATION = 10;

        Population pop = getPopulation();
        List<PopulationGroup> groups = pop.getPopulationGroup();

        MatrixND result = createNoSpacializedChangeGroupMatrix();

        for (int i = 0; i < groups.size(); i++) {
            PopulationGroup group = groups.get(i);
            double min = group.getMinLength();
            double max = group.getMaxLength();

            // creation de la matrice de longueurs pour la classe
            double step = (max - min) / NB_DISCRETISATION;
            double[] length = new double[NB_DISCRETISATION];
            length[0] = min;
            for (int l = 1; l < NB_DISCRETISATION; l++) {
                length[l] = length[l - 1] + step;
            }

            // calcul des nouvelles longueurs pour la classe
            for (int l = 0; l < NB_DISCRETISATION; l++) {
                double age = pop.getAge(length[l], group);
                length[l] = pop.getLength(age + 1, group); // + 1 mois
            }

            // modification de la matrice de changement d'age en fonction
            // de la vitesse croissance de la classe.

            for (int l = 0; l < NB_DISCRETISATION; l++) {
                int c = 0;
                while (c < groups.size()
                        && !groups.get(c).isInLength(length[l])) {
                    c++;
                }
                if (c >= groups.size()) {
                    log.warn(t("isisfish.error.acceptable.population")
                            + " (group=" + group + " new length: " + length[l]
                            + ")");
                    // pas retrouver une classe pour cette longueur, on le laisse dans la meme classe.
                    result.setValue(i, i, result.getValue(i, i) + 1);
                } else {
                    result.setValue(i, c, result.getValue(i, c) + 1); // on ajoute des 1
                    // on divisera par NB_DISCRETISATION juste avant de retourner le
                    // resultat
                }
            }
        }
        result = result.divs(NB_DISCRETISATION);
        return result;
    }

    /**
     * Return change group matrix for the given month.
     * 
     * for species in Length, matrix returned is all time spacialized (needed 
     * for simulation)
     */
    public MatrixND getGroupChangeMatrix(Month month) {
        Population pop = getPopulation();

        if (log.isDebugEnabled()) {
            log.debug("pop: " + pop + " AgeGroup: "
                    + pop.getSpecies().isAgeGroupType() + " groupChange: "
                    + isGroupChange() + " month: " + month + " firstMonth: "
                    + getFirstMonth());
        }

        // si en longueur
        if (!pop.getSpecies().isAgeGroupType()) {
            if (isSimpleLengthChangeMatrix()) {
                return spacializeLengthChangeMatrix(getLengthChangeMatrix());
            } else {
                return getLengthChangeMatrix();
            }
        } else if (isGroupChange()
                && month.getMonthNumber() == getFirstMonth().getMonthNumber()) {
            // create identity matrix with special 1 for plus group if necessary
            int nbrAge = pop.sizePopulationGroup();
            int nbrZone = pop.sizePopulationZone();
            boolean groupplus = pop.isPlusGroup();

            MatrixND result = createSpacializedChangeGroupMatrix();
            for (MatrixIterator mi = result.iterator(); mi.next();) {
                int[] dim = mi.getCoordinates();
                int i = dim[0];
                int j = dim[1];

                if (
                // un element de la diagonale dans le block choisi
                (i + nbrZone == j) || // calcul pour savoir s'il y a le groupe plus
                        (groupplus
                        // regarde si on est bien dans le dernier block
                                && (nbrAge - 1 == i / nbrZone)
                        // regarde si on est bien sur la diagonal
                        && (i == j))) {
                    mi.setValue(1);
                }
            }
            return result;
        } else {
            int nbsecteurs = pop.sizePopulationZone();
            int nbclasses = pop.sizePopulationGroup();

            int dim = nbsecteurs * nbclasses;

            return MatrixFactory.getInstance().matrixId(dim);
        }
    }

    @Override
    public MatrixND getMigrationMatrix(Month month, MatrixND N) {
        if (month.getMonthNumber() == getFirstMonth().getMonthNumber()) {
            return getMigrationMatrix(N);
        } else {
            MatrixND result = createSpacializedChangeGroupMatrix();
            MatrixHelper.convertToId(result);
            return result;
        }
    }

    protected MatrixND getMigrationMatrix(MatrixND aN) {
        MatrixND mat = null;
        try {
            Population population = getPopulation();

            List<Zone> zones = population.getPopulationZone();
            List<PopulationGroup> groups = population.getPopulationGroup();

            int nbSecteur = zones.size();

            mat = createSpacializedChangeGroupMatrix();
            MatrixHelper.convertToId(mat);

            if (isUseEquationMigration()) {
                Equation eq = getMigrationEquation();
                if (eq != null) {
                    // on commence par faire une copie pour ne pas modifier N
                    MatrixND N = MatrixFactory.getInstance().create(aN);

                    for (int c = 0; c < groups.size(); c++) {
                        PopulationGroup group = groups.get(c);
                        for (int d = 0; d < zones.size(); d++) {
                            Zone departureZone = zones.get(d);
                            for (int a = 0; a < zones.size(); a++) {
                                Zone arrivalZone = zones.get(a);
                                if (departureZone.equals(arrivalZone)) {
                                    // pour zd == za on ne fait pas le calcul
                                    // car au retire automatiquement les autres
                                    // valeur de celle-ci
                                    continue;
                                }

                                double coef = eq.evaluate("N", N, "pop",
                                        population, "group", group,
                                        "departureZone", departureZone,
                                        "arrivalZone", arrivalZone);

                                int x = c * nbSecteur + d;
                                int y = c * nbSecteur + a;
                                double ancienneVal = mat.getValue(x, x) - coef;
                                mat.setValue(x, x, ancienneVal);
                                if (ancienneVal < 0.000001) {
                                    log.warn("Erreur dans la migration, un coef est negatif");
                                }
                                mat.setValue(x, y, coef);
                            }
                        }
                    }
                }
            } else {

                // matrix 3D: Group, departure, arrival
                MatrixND mig = getMigrationMatrix();

                for (MatrixIterator i = mig.iterator(); i.hasNext();) {
                    i.next();
                    double coef = i.getValue();
                    if (coef != 0) {
                        int[] coord = i.getCoordinates();

                        int posClasse = coord[0];
                        int posSecteurD = coord[1];
                        int posSecteurA = coord[2];

                        int x = posClasse * nbSecteur + posSecteurD;
                        int y = posClasse * nbSecteur + posSecteurA;
                        // toutes les migrations doivent etre precisees.
                        // Peut-etre faire un teste pour que les migrations ne depassent pas 1
                        double ancienneVal = mat.getValue(x, x);
                        mat.setValue(x, x, ancienneVal - coef);
                        if (ancienneVal < 0.000001) {
                            log.info("Erreur dans la migration, un coef est negatif");
                        }
                        mat.setValue(x, y, coef);
                    }
                }
            }
        } catch (Exception ex) {
            EvaluatorHelper.catchEvaluateException(ex, log);
        }
        return mat;
    }

    @Override
    public MatrixND getEmigrationMatrix(Month month, MatrixND N) {
        if (month.getMonthNumber() == getFirstMonth().getMonthNumber()) {
            return getEmigrationMatrix(N);
        } else {
            MatrixND result = createSpacializedChangeGroupMatrix();
            return result;
        }
    }

    protected MatrixND getEmigrationMatrix(MatrixND aN) {
        MatrixND mat = null;
        try {
            Population population = getPopulation();

            List<Zone> zones = population.getPopulationZone();
            List<PopulationGroup> groups = population.getPopulationGroup();

            int nbSecteur = zones.size();

            mat = createSpacializedChangeGroupMatrix();

            if (isUseEquationMigration()) {
                Equation eq = getEmigrationEquation();
                if (eq != null) {
                    // on commence par faire une copie pour ne pas modifier N
                    MatrixND N = MatrixFactory.getInstance().create(aN);

                    for (int c = 0; c < groups.size(); c++) {
                        PopulationGroup group = groups.get(c);
                        for (int d = 0; d < zones.size(); d++) {
                            Zone departureZone = zones.get(d);

                            double coef = eq.evaluate("N", N, "pop", population,
                                    "group", group, "departureZone", departureZone);

                            int x = c * nbSecteur + d;
                            mat.setValue(x, x, coef);
                        }
                    }
                }
            } else {

                // matrix 3D: Group, departure, arrival
                MatrixND mig = getEmigrationMatrix();

                for (MatrixIterator i = mig.iterator(); i.hasNext();) {
                    i.next();
                    double coef = i.getValue();
                    if (coef != 0) {
                        int[] coord = i.getCoordinates();

                        int posClasse = coord[0];
                        int posSecteurD = coord[1];

                        int x = posClasse * nbSecteur + posSecteurD;
                        mat.setValue(x, x, coef);
                    }
                }
            }
        } catch (Exception ex) {
            EvaluatorHelper.catchEvaluateException(ex, log);
        }
        return mat;
    }

    @Override
    public MatrixND getImmigrationMatrix(Month month, MatrixND N) {
        if (month.getMonthNumber() == getFirstMonth().getMonthNumber()) {
            return getImmigrationMatrix(N);
        } else {
            MatrixND result = createSpacializedChangeGroupMatrix();
            return result;
        }
    }

    protected MatrixND getImmigrationMatrix(MatrixND aN) {
        MatrixND mat = null;
        try {
            Population population = getPopulation();

            List<Zone> zones = population.getPopulationZone();
            List<PopulationGroup> groups = population.getPopulationGroup();

            int nbSecteur = zones.size();

            mat = MatrixFactory.getInstance().create(
                    new int[] { groups.size() * nbSecteur });

            if (isUseEquationMigration()) {
                Equation eq = getImmigrationEquation();
                if (eq != null) {
                    // on commence par faire une copie pour ne pas modifier N
                    MatrixND N = MatrixFactory.getInstance().create(aN);

                    for (int c = 0; c < groups.size(); c++) {
                        PopulationGroup group = groups.get(c);
                        for (int a = 0; a < zones.size(); a++) {
                            Zone arrivalZone = zones.get(a);

                            double coef = eq.evaluate("N", N, "pop", population,
                                    "group", group, "arrivalZone", arrivalZone);

                            int y = c * nbSecteur + a;
                            mat.setValue(y, coef);
                        }
                    }
                }
            } else {

                // matrix 3D: Group, departure, arrival
                MatrixND mig = getImmigrationMatrix();

                for (MatrixIterator i = mig.iterator(); i.hasNext();) {
                    i.next();
                    double coef = i.getValue();
                    if (coef != 0) {
                        int[] coord = i.getCoordinates();

                        int posClasse = coord[0];
                        int posSecteurA = coord[1];

                        int y = posClasse * nbSecteur + posSecteurA;
                        mat.setValue(y, coef);
                    }
                }
            }
        } catch (Exception ex) {
            EvaluatorHelper.catchEvaluateException(ex, log);
        }
        return mat;
    }

    public void setMigrationEquationContent(String content) {
        try {
            Equation eq = getMigrationEquation();

            if (eq == null) {
                EquationDAO dao = IsisFishDAOHelper
                        .getEquationDAO(getTopiaContext());
                // create equation
                eq = dao.create();
                eq.setCategory("Migration");
                eq.setLanguage(Language.JAVA);
                eq.setJavaInterface(MigrationEquation.class);
                setMigrationEquation(eq);
            }

            if (getPopulation() != null) {
                eq.setName(getPopulation().getName() + "(" + toString() + ")");
            }

            // Fire
            String _oldValue = eq.getContent();
            fireOnPreWrite("content", _oldValue, content);

            eq.setContent(content);
            eq.update();

            fireOnPostWrite("content", _oldValue, content);

        } catch (TopiaException eee) {
            throw new IsisFishRuntimeException(
                    t("isisfish.error.change.equation"), eee);
        }
    }

    public void setEmigrationEquationContent(String content) {
        try {
            Equation eq = getEmigrationEquation();

            if (eq == null) {
                EquationDAO dao = IsisFishDAOHelper
                        .getEquationDAO(getTopiaContext());
                // create equation
                eq = dao.create();
                eq.setCategory("Emigration");
                eq.setLanguage(Language.JAVA);
                eq.setJavaInterface(EmigrationEquation.class);
                setEmigrationEquation(eq);
            }

            if (getPopulation() != null) {
                eq.setName(getPopulation().getName() + "(" + toString() + ")");
            }

            // Fire
            String _oldValue = eq.getContent();
            fireOnPreWrite("content", _oldValue, content);

            eq.setContent(content);
            eq.update();

            fireOnPostWrite("content", _oldValue, content);

        } catch (TopiaException eee) {
            throw new IsisFishRuntimeException(
                    t("isisfish.error.change.equation"), eee);
        }
    }

    public void setImmigrationEquationContent(String content) {
        try {
            Equation eq = getImmigrationEquation();

            if (eq == null) {
                EquationDAO dao = IsisFishDAOHelper
                        .getEquationDAO(getTopiaContext());
                // create equation
                eq = dao.create();
                eq.setCategory("Immigration");
                eq.setLanguage(Language.JAVA);
                eq.setJavaInterface(ImmigrationEquation.class);
                setImmigrationEquation(eq);
            }

            if (getPopulation() != null) {
                eq.setName(getPopulation().getName() + "(" + toString() + ")");
            }

            // Fire
            String _oldValue = eq.getContent();
            fireOnPreWrite("content", _oldValue, content);

            eq.setContent(content);
            eq.update();

            fireOnPostWrite("content", _oldValue, content);

        } catch (TopiaException eee) {
            throw new IsisFishRuntimeException(
                    t("isisfish.error.change.equation"), eee);
        }
    }

    /**
     * Construit la matrice de reproduction. C'est un vecteur contenant le
     * nombre d'individu creer par la reproduction. Le vecteur est structure
     * en zone de reproduction.
     */
    public MatrixND getReproductionMatrix(Month month, MatrixND aN) {
        Population pop = getPopulation();
        List<Zone> zoneRepro = pop.getReproductionZone();
        MatrixND result = MatrixFactory.getInstance().create(n("Reproduction"),
                new List[] { zoneRepro }, new String[] { n("Zones") });

        // on commence par faire une copie pour ne pas modifier N
        MatrixND N = aN.copy();

        if (isReproduction()) {
            //recuperation de l'equation de reproduction
            Equation e = pop.getReproductionEquation();
            if (e != null) {

                //le coeff de repro pour le mois demande
                MatrixND coeff = getReproductionDistribution().copy();

                try {
                    // l'equation doit mettre les resultats dans la matrice result
                    e.evaluate("N", N, "pop", pop, "month", month, "prepro", coeff
                                .getValue(month), "zoneRepro", zoneRepro, "groups", N
                                .getSemantic(0), "zones", N.getSemantic(1), "result", result);
                } catch (Exception ex) {
                    EvaluatorHelper.catchEvaluateException(ex, log);
                }
            }
        }
        return result;
    }

    public String toString() {
        String result = t("isisfish.populationSeasonInfo.toString", this
                .getPopulation(), this.getFirstMonth(), this.getLastMonth());
        return result;
    }

}
