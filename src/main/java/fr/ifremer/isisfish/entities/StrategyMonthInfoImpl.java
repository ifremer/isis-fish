/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2011 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.entities;

import static org.nuiton.i18n.I18n.t;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixFactory;
import org.nuiton.math.matrix.MatrixND;

import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.datastore.RegionStorage;
import fr.ifremer.isisfish.datastore.StorageException;
import java.util.Collections;
import org.nuiton.math.matrix.MatrixIterator;

/**
 * StrategyMonthInfoImpl.
 *
 * Created: 29 août 06 11:30:27
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class StrategyMonthInfoImpl extends StrategyMonthInfoAbstract {

    /** serialVersionUID. */
    private static final long serialVersionUID = 4266095355294103240L;

    /** Class logger. */
    private static Log log = LogFactory.getLog(StrategyMonthInfoImpl.class);

    public FisheryRegion getFisheryRegion() {
        try {
            FisheryRegion result = RegionStorage
                    .getFisheryRegion(getTopiaContext());
            return result;
        } catch (StorageException eee) {
            throw new IsisFishRuntimeException("Can't get fishery region", eee);
        }
    }

    @Override
    public List<Metier> getMetierWithProportion() {
        MatrixND mat = getProportionMetier();
        List<Metier> result;
        if (mat == null) {
            result = Collections.EMPTY_LIST;
        } else {
            result = new ArrayList<>(mat.getDim(0));
            for (MatrixIterator i=mat.iteratorNotZero(); i.next();) {
                result.add((Metier)i.getSemanticsCoordinates()[0]);
            }
        }
        return result;
    }

    public double getProportionMetier(Metier metier) {
        double result = 0;
        // on prend le super car ca ne sert a rien de mettre a jour,
        // se le metier n'y ait pas on renvera 0;
        MatrixND mat = super.getProportionMetier();
        if (mat != null) {
            int indice = mat.getSemantic(0).indexOf(metier);
            if (indice >= 0) {
                result = mat.getValue(indice);
            }
        }
        return result;
    }

    public void setProportionMetier(Metier metier, double value) {
        MatrixND mat = getProportionMetier();
        if (mat != null) {
            mat = mat.copy();
            mat.setValue(metier, value);
            setProportionMetier(mat);
        } else {
            log.warn("Can't set proportion of metier, not enough information available");
        }
    }

    @Override
    public MatrixND getProportionMetier() {
        if (getStrategy() == null || getStrategy().getSetOfVessels() == null
                || getStrategy().getSetOfVessels().getPossibleMetiers() == null) {
            return null;
        }

        Collection<EffortDescription> efforts = getStrategy().getSetOfVessels()
                .getPossibleMetiers();

        List<Metier> metiers = new ArrayList<>(efforts.size());
        for (EffortDescription effort : efforts) {
            metiers.add(effort.getPossibleMetiers());
        }

        if (metiers.size() <= 0) {
            return null;
        }

        MatrixND result = super.getProportionMetier();

        if (result == null) {
            result = MatrixFactory.getInstance().create(
                    t("isisfish.strategyMonthInfo.proportion"),
                    new List[] { metiers },
                    new String[] { t("isisfish.strategyMonthInfo.metier") });
        } else if (!result.getSemantic(0).equals(metiers)) {
            MatrixND tmp = MatrixFactory.getInstance().create(
                    t("isisfish.strategyMonthInfo.proportion"),
                    new List[] { metiers },
                    new String[] { t("isisfish.strategyMonthInfo.metier") });
            tmp.pasteSemantics(result);
            result = tmp;
        }

        return result;
    }

    @Override
    public void setTripType(TripType value) {
        super.setTripType(value);
        computeNumberOfTrips();
    }

    @Override
    public void setMinInactivityDays(double value) {
        super.setMinInactivityDays(value);
        computeNumberOfTrips();
    }

    @Override
    public double getMinInactivityDays() {
        double result;
        if (getStrategy().isInactivityEquationUsed()) {
            result = getStrategy().getInactivityDays(getMonth());
        } else {
            result = super.getMinInactivityDays();
        }
        return result;
    }

    public void computeNumberOfTrips() {
        TripType tripType = getTripType();
        // remplace un simple getMinInactivityDays(), pour pouvoir utiliser une
        // equation si besoin (tout est pret, il ne manque que l'interface
        // graphique pour saisir l'equation dans strategie
        if (getStrategy() != null) { // json import error
            double inactivity = getMinInactivityDays();
            if (tripType != null && tripType.getTripDuration() != null && inactivity >= 0) {
                int nbDayMonth = getMonth().getNumbersOfDays();
                double result = (nbDayMonth - inactivity) / tripType.getTripDuration().getDay();
                setNumberOfTrips(result);
            }
        }
    }

    @Override
    public String toString() {
        String result = t("isisfish.strategyMonthInfo.toString", this
                .getStrategy(), this.getMonth());
        return result;
    }

}
