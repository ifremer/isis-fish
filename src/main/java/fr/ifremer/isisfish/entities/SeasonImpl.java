/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2010 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.entities;

import java.util.ArrayList;
import java.util.List;

import fr.ifremer.isisfish.types.Month;
import static org.nuiton.i18n.I18n.t;

/***
 * SeasonImpl.
 *
 * Created: 3 janv. 2006 18:28:19
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class SeasonImpl extends SeasonAbstract {

    /** serialVersionUID. */
    private static final long serialVersionUID = 1L;

    public SeasonImpl() {
        setFirstMonth(Month.JANUARY);
        setLastMonth(Month.JANUARY);
    }

    @Override
    public List<Month> getMonths() {
        List<Month> result = new ArrayList<>();
        Month m = getFirstMonth();
        while (!m.equals(getLastMonth())) {
            result.add(m);
            m = m.next();
        }
        result.add(m); // add last month because we exit loop before
        return result;
    }

    @Override
    public void setMonths(List<Month> listMonth) {
        if (listMonth == null || listMonth.size() == 0) {
            throw new IllegalArgumentException(
                    "listMonth must contains one month or more");
        }
        Month first = listMonth.get(0);
        Month last = listMonth.get(listMonth.size() - 1);

        setFirstMonth(first);
        setLastMonth(last);
    }

    @Override
    public boolean containsMonth(Month month) {
        boolean result = month.equals(getLastMonth()); // check first last month, because loop don't do that

        Month m = getFirstMonth();
        while (!result && !m.equals(getLastMonth())) {
            result = m.equals(month);
            m = m.next();
        }
        return result;
    }

    @Override
    public String toString() {
        String result = t("isisfish.season.toString", this.getFirstMonth(),
                this.getLastMonth());
        return result;
    }

}
