package fr.ifremer.isisfish.entities;

/*
 * #%L
 * ISIS-Fish
 * %%
 * Copyright (C) 2016 - 2017 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ifremer.isisfish.IsisFishDAOHelper;
import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.types.Month;
import fr.ifremer.isisfish.types.RangeOfValues;
import fr.ifremer.isisfish.types.TimeUnit;
import fr.ifremer.isisfish.util.matrix.EntitySemanticsDecorator;
import fr.ifremer.isisfish.util.matrix.MatrixCSVHelper;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.persistence.EntityVisitor;
import org.nuiton.topia.persistence.TopiaDAO;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaId;

import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import java.awt.FlowLayout;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Classe permettant d'importer des données dans une region a partir d'un
 * export Json.
 * L'import se fait le plus possible de façon automatique, s'il y a des
 * choix a faire durant la fusion, les questions sont posées au travers de
 * l'objet passé en paramètre
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class RegionImportJson {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(RegionImportJson.class);

    protected RegionMerge merge;
    protected JsonNode json;
    protected Map<String, TopiaEntity> entities;
    protected JsonNode jsonEntities;

    /**
     *
     * @param r reader contains json
     * @param merge use to choice entity in Json or entity in current region,
     * if no merge object is passed in argument "import all" object in reader
     */
    public RegionImportJson(Reader r, RegionMerge merge) {
        try {
            this.merge = merge != null ? merge : new RegionMergeImportAll();

            entities = new LinkedHashMap<>();
            this.merge.setEntities(entities);

            ObjectMapper m = new ObjectMapper();
            json = m.readTree(r);
            jsonEntities = json.get("#entities");
        } catch (IOException eee) {
            throw new RuntimeException(eee);
        }
    }

    /**
     * Normalement on ne peut pas importer des cellules parce qu'avec des résolutions spatiales différentes,
     * ca donnerait vraiment n'import quoi.
     * Mais dans de très rare cas, on pourrait surcharger cette méthode.
     */
    protected boolean canImportCell() {
        return false;
    }

    /**
     * Return all entities available in Json reader
     * @return
     */
    public Collection<TopiaEntity> getEntities() {
        try {
            JsonNode info = json.with("#info");
            boolean isRegion = info.get("region").asBoolean(false);
            if (!isRegion) {
                // il n'y a qu'un objet dans le json, on le deserialize
                String rootId = info.get("rootId").asText();
                RegionVisitor v = new RegionVisitor(merge, null, null, rootId, entities, jsonEntities, canImportCell());
                // no assigment, collect of entities is done during deserialize
                v.loadEntity();
            } else {
                // il y a toute une region, on lit les listes d'id des objets
                // principaux (cell, zone, port, ...)
                for (Iterator<Entry<String, JsonNode>> i = json.fields(); i.hasNext();) {
                    Entry<String, JsonNode> field = i.next();
                    // les champs commencant par des # sont #info et #entities
                    if (!field.getKey().startsWith("#")) {
                        for (JsonNode id : field.getValue()) {
                            RegionVisitor v = new RegionVisitor(merge, null, null, id.asText(), entities, jsonEntities, canImportCell());
                            // no assigment, collect of entities is done during deserialize
                            v.loadEntity();
                        }
                    }
                }
            }

            return entities.values();
        } catch (Exception eee) {
            throw new RuntimeException(eee);
        }
    }

    static protected class RegionVisitor implements EntityVisitor {

        protected RegionMerge merge;
        protected JsonNode nodeEntity;
        protected TopiaEntity parentEntity;
        protected String propertyName;
        protected TopiaEntity currentEntity;
        protected Map<String, TopiaEntity> entities;
        protected JsonNode jsonEntities;
        protected boolean canImportCell;

        public RegionVisitor(RegionMerge merge,
                TopiaEntity parentEntity, String propertyName,
                String idEntity,
                Map<String, TopiaEntity> entities, JsonNode jsonEntities, boolean canImportCell) {
            this.merge = merge;
            this.parentEntity = parentEntity;
            this.propertyName = propertyName;
            this.nodeEntity = jsonEntities.get(idEntity);
            this.entities = entities;
            this.jsonEntities = jsonEntities;
            this.canImportCell = canImportCell;
        }

        public TopiaEntity loadEntity() {
            String id = nodeEntity.get("#id").asText();
            currentEntity = entities.get(id);
            if (currentEntity == null) {
                currentEntity = ask(id);
            }
            return currentEntity;
        }

        protected TopiaEntity convertJsonToEntity(JsonNode node, TopiaEntity result) {
            String id = node.get("#id").asText();
            String className = node.get("#class").asText();
            try {
                // try to reuse entity
                if (result == null) {
                    // if no entity with same id, create new one, with new Id
                    Class clazz = Class.forName(className);
                    result = (TopiaEntity)clazz.newInstance();
                    result.setTopiaId(TopiaId.create(TopiaId.getClassName(id)));
                }
                // add in list save after import
                // id and result.id can be different. id is used to keep link in json
                entities.put(id, result);
                
                result.accept(this);
            } catch (Exception eee) {
                throw new IsisFishRuntimeException("Can't instanciate entity: " + className, eee);
            }            
            return result;
        }

        /**
         * Normalement on ne peut pas importer des cellules parce qu'avec des résolutions spatiales différentes,
         * ca donnerait vraiment n'import quoi.
         * Mais dans de très rare cas, on pourrait surcharger cette méthode.
         */
        public boolean canImportCell() {
            return canImportCell;
        }

        protected TopiaEntity ask(String id) {
            JsonNode node = jsonEntities.get(id);

            Map<String, Object> details = new HashMap<>();
            for (Iterator<Entry<String, JsonNode>> i = node.fields(); i.hasNext();) {
                Entry<String, JsonNode> e = i.next();
                if (!e.getKey().startsWith("#")) {
                    details.put(e.getKey(), e.getValue().asText());
                }
            }

            TopiaEntity result;
            String toString = node.get("#toString").asText();

            EnumSet<RegionMerge.AnswerType> disallow = EnumSet.noneOf(RegionMerge.AnswerType.class);

            // specifique rule for some Entities
            Class currentEntityClass = TopiaId.getClassName(id);
            if (Species.class.isAssignableFrom(currentEntityClass) && Population.PROPERTY_SPECIES.equals(propertyName)) {
                // une population a forcement une espece
                disallow.add(RegionMerge.AnswerType.NONE);
            } else if (Cell.class.isAssignableFrom(currentEntityClass) && !canImportCell()) {
                // on ne peut pas importer une Cellule d'une autre Region
                disallow.add(RegionMerge.AnswerType.CREATE);
            }

            RegionMerge.Answer answer = merge.choice(id, toString, details,
                    disallow.toArray(new RegionMerge.AnswerType[0]));

            switch (answer.type) {
                case ABORT:
                    throw new IsisFishRuntimeException("Import aborted by user");
                case NONE:
                    result = null;
                    break;
                case USE:
                    result = answer.entity;
                    break;
                case REPLACE:
                    result = convertJsonToEntity(node, answer.entity);
                    break;
                case CREATE:
                    result = convertJsonToEntity(node, null);
                    break;
                default:
                    throw new IsisFishRuntimeException("Unsupported action");
            }
            return result;
        }

        protected Object readValue(final String propertyName, Class<?> type, JsonNode value) {
            Object result;
            try {
                if (value == null || value.isNull() || value.isMissingNode()) {
                    result = null;
                } else if (TopiaEntity.class.isAssignableFrom(type)) {
                    String id = value.asText();
                    RegionVisitor child = new RegionVisitor(merge, currentEntity, propertyName, id, entities, jsonEntities, canImportCell());
                    result = child.loadEntity();
                } else if (MatrixND.class.isAssignableFrom(type)) {
                    String mat = value.asText();

                    MatrixCSVHelper matrixCSVHelper = new MatrixCSVHelper(new EntitySemanticsDecorator(
                            id -> {
                                RegionVisitor child = new RegionVisitor(merge, currentEntity, propertyName, id, entities, jsonEntities, canImportCell());
                                Object result1 = child.loadEntity();
                                return result1;
                            }));

                    result = matrixCSVHelper.readMatrix(mat);

                } else if (TimeUnit.class.isAssignableFrom(type)) {
                    result = new TimeUnit(value.asDouble());
                } else if (RangeOfValues.class.isAssignableFrom(type)) {
                    String v = value.asText();
                    result = new RangeOfValues(v);
                } else if (Month.class.isAssignableFrom(type)) {
                    int m = value.asInt();
                    result = Month.MONTH[m];
                } else if (Class.class.isAssignableFrom(type)) {
                    String className = value.asText();
                    result = Class.forName(className);
                } else if (String.class.isAssignableFrom(type)) {
                    result = value.asText();
                } else if (Boolean.class.isAssignableFrom(type) || Boolean.TYPE.isAssignableFrom(type)) {
                    result = value.asBoolean();
                } else if (Number.class.isAssignableFrom(type) || type.isPrimitive()) { // Number must be after Boolean
                    // all other possible primitive type is number (void and char are not admited in json)
                    result = value.asDouble();
                } else {
                    String message = String.format("Unsupported type: '%s' value class '%s' value '%s'", type, value.getClass().getName(), value);
                    log.error(message);
                    throw new IsisFishRuntimeException(message);
                }
            } catch (Exception eee) {
                throw new IsisFishRuntimeException("Can't convert json value:" + value, eee);
            }
            return result;
        }

        @Override
        public void start(TopiaEntity entity) {
        }

        @Override
        public void end(TopiaEntity entity) {
        }

        @Override
        public void visit(TopiaEntity entity, String propertyName, Class<?> type, Object value) {
            JsonNode jsonValue = nodeEntity.get(propertyName);
            try {
                value = readValue(propertyName, type, jsonValue);
                BeanUtils.setProperty(entity, propertyName, value);
            } catch (Exception eee) {
                throw new IsisFishRuntimeException(String.format("Can't set property '%s' from json value: '%s'", propertyName, jsonValue), eee);
            }
        }

        @Override
        public void visit(TopiaEntity entity, String propertyName, Class<?> collectionType, Class<?> type, Object value) {
            JsonNode jsonValue = nodeEntity.get(propertyName);
            try {
                if (jsonValue != null) {
                    Collection c;
                    if (Set.class.isAssignableFrom(collectionType)) {
                        c = new HashSet();
                    } else {
                        c = new ArrayList();
                    }

                    for (JsonNode currentValue : jsonValue) {
                        Object v = readValue(propertyName, type, currentValue);
                        if (v != null) {
                            c.add(v);
                        }
                    }
                    BeanUtils.setProperty(entity, propertyName, c);
                }
            } catch (Exception eee) {
                throw new IsisFishRuntimeException(String.format("Can't set property '%s' from json value: '%s'", propertyName, jsonValue), eee);
            }
        }

        @Override
        public void visit(TopiaEntity entity, String propertyName, Class<?> collectionType, Class<?> type, int index, Object value) {
            throw new UnsupportedOperationException("FIXME a faire, implantation des properties array");
        }

        @Override
        public void clear() {
        }
    }

    /**
     * Use during import to merge imported data in existing Region
     */
    public interface RegionMerge {
        void setEntities(Map<String, TopiaEntity> entities);

        enum AutoAnswerType {
            ONCE(t("isisfish.import.json.merge.autoanswer.once")),
            TYPE(t("isisfish.import.json.merge.autoanswer.type")),
            ALL(t("isisfish.import.json.merge.autoanswer.all"));

            private final String toString;
            AutoAnswerType(String toString) {
                this.toString = toString;
            }

            @Override
            public String toString() {
                return toString;
            }

        }
        
        enum AnswerType {
            NONE(t("isisfish.import.json.merge.option.none"), t("isisfish.import.json.merge.option.none.desc")),
            USE(t("isisfish.import.json.merge.option.use"), t("isisfish.import.json.merge.option.use.desc")),
            REPLACE(t("isisfish.import.json.merge.option.replace"), t("isisfish.import.json.merge.option.replace.desc")),
            CREATE(t("isisfish.import.json.merge.option.create"), t("isisfish.import.json.merge.option.create.desc")),
            ABORT(t("isisfish.import.json.merge.option.abort"), t("isisfish.import.json.merge.option.abort.desc"));

            private final String toString;
            private final String description;
            AnswerType(String toString, String description) {
                this.toString = toString;
                this.description = description;
            }

            public String getDescription() {
                return description;
            }

            @Override
            public String toString() {
                return toString;
            }
        }

        class Answer {
            public AnswerType type;
            public TopiaEntity entity;

            public Answer(AnswerType type, TopiaEntity entity) {
                this.type = type;
                this.entity = entity;
            }
        }

        /**
         * Ask how to merge entity
         * @param id
         * @param toString
         * @param details
         * @param disallow
         * @return
         */
        Answer choice(String id, String toString, Map<String, Object> details, AnswerType... disallow);

        boolean isAbort();
    }

    static public class RegionMergeDatabase implements RegionMerge {
//        static public enum ComplexeAnswerType {
//            NONE("None", "don't import nor reuse an object."),
//            NONE_THIS_TYPE("None same type", "auto reply None for this answer and all next answer for same object type."),
//            NONE_ALL("None all", "auto reply None for this answer and all next answer for all object type."),
//
//            USE("Use", "use (don't import) object already in current Region (make your choice in next combobox)."),
//            USE_THIS_TYPE("Use same type", "auto reply Use for this answer and all next answer if only one object found ('id' or 'name') in current Region for this type."),
//            USE_ALL("Use all", "auto reply Use for this answer and all next answer if only one object found ('id' or 'name') in current Region for all object."),
//
//            REPLACE("Replace", "replace object in current Region with imported object from file (make your choice in next combobox"),
//            REPLACE_THIS_TYPE("Replace same type", "auto reply Replace for this answer and all next answer if only one object found ('id' or 'name') in current Region for this type."),
//            REPLACE_ALL("Replace all", "auto reply Replace for this answer and all next answer if only one object found ('id' or 'name') in current Region for all object."),
//
//            CREATE("Create", "create new object from file."),
//            CREATE_THIS_TYPE("Create same type", "auto reply Create for this answer and all next answer for same object type."),
//            CREATE_ALL("Create All", "auto reply Create for this answer and all next answer for all object."),
//
//            ABORT("Abort", "cancel this import (import nothing).");
//
//            private final String toString;
//            private final String description;
//            ComplexeAnswerType(String toString, String description) {
//                this.toString = toString;
//                this.description = description;
//            }
//
//            public String getDescription() {
//                return description;
//            }
//
//            @Override
//            public String toString() {
//                return toString;
//            }
//        }

        /**
         * Object used to keep recurent answer (abort, import All, reuse All, ...)
         */
        static public class RegionMergeContext {
            protected boolean abort = false;

            protected EnumSet<AnswerType> replyAll = EnumSet.noneOf(AnswerType.class);
            protected HashMap<AnswerType, Set<Class>> replyType = new HashMap<AnswerType, Set<Class>>() {
                @Override
                public Set<Class> get(Object key) {
                    Set<Class> result = super.get(key);
                    if (result == null) {
                        result = new HashSet<>();
                        put((AnswerType)key, result);
                    }
                    return result;
                }
            };
//            protected boolean noneAll = false;
//            protected Set<Class> noneType = new HashSet<Class>();
//            protected boolean useAll = false;
//            protected Set<Class> useType = new HashSet<Class>();
//            protected boolean replaceAll = false;
//            protected Set<Class> replaceType = new HashSet<Class>();
//            protected boolean createAll = false;
//            protected Set<Class> createType = new HashSet<Class>();

            protected AnswerType lastAnswerType = null;
            protected TopiaEntity reuseEntity;
            protected Class currentType;

            public RegionMergeContext() {
            }

            public RegionMergeContext(boolean importAll) {
                this.replyAll.add(AnswerType.CREATE);
            }

            public RegionMerge.Answer getAnswer() {
                RegionMerge.Answer result = null;
                if (lastAnswerType != null) {
                    result = new RegionMerge.Answer(lastAnswerType, reuseEntity);
                }
                return result;
            }

            public Class getCurrentType() {
                return currentType;
            }

            protected void setLastAnswer(AnswerType lastAnswer, AutoAnswerType autoType) {
                this.setLastAnswer(lastAnswer, autoType, null);
            }

            protected void setLastAnswer(AnswerType lastAnswer, AutoAnswerType autoType, TopiaEntity e) {
                this.reuseEntity = e;
                this.lastAnswerType = lastAnswer;

                if (AnswerType.ABORT == lastAnswer) {
                    abort = true;
                }

                switch (autoType) {
                    case TYPE:
                        Set<Class> set = replyType.get(lastAnswer);
                        set.add(currentType);
                        break;
                    case ALL:
                        replyAll.add(lastAnswer);
                        break;
                }
            }

            public AnswerType initAnswer(Class type) {
                lastAnswerType = null;
                currentType = type;
                if (abort) {
                    lastAnswerType = AnswerType.ABORT;
                } else if (replyAll.contains(AnswerType.NONE) || replyType.get(AnswerType.NONE).contains(type)) {
                    lastAnswerType = AnswerType.NONE;
                } else if (replyAll.contains(AnswerType.CREATE) || replyType.get(AnswerType.CREATE).contains(type)) {
                    lastAnswerType = AnswerType.CREATE;
                }
                return lastAnswerType;
            }

            public AnswerType initAnswer(Class type, Set<TopiaEntity> possible) {
                lastAnswerType = null;
                currentType = type;
                if (possible != null && possible.size() == 1 &&
                        (replyAll.contains(AnswerType.USE) || replyType.get(AnswerType.USE).contains(type))) {
                    lastAnswerType = AnswerType.USE;
                    this.reuseEntity = possible.iterator().next();
                } else if (possible != null && possible.size() == 1 && 
                        (replyAll.contains(AnswerType.REPLACE) || replyType.get(AnswerType.REPLACE).contains(type))) {
                    lastAnswerType = AnswerType.REPLACE;
                    this.reuseEntity = possible.iterator().next();
                }
                return lastAnswerType;
            }
        }


        protected TopiaContext tx;
        protected RegionMergeContext context;
        protected Answer lastAnswer;
        protected Map<String, TopiaEntity> entities;

        public RegionMergeDatabase(TopiaContext tx) {
            this.tx = tx;
        }

        protected boolean isAcceptableAnswer(AnswerType type, AnswerType ... disallow) {
            boolean result = type != null && !Arrays.asList(disallow).contains(type);
            return result;
        }

        @Override
        public void setEntities(Map<String, TopiaEntity> entities) {
            this.entities = entities;
        }

        @Override
        public Answer choice(String id, String toString, Map<String, Object> details, AnswerType ... disallow) {
            Class type = TopiaId.getClassName(id);
            if (context == null) {
                context = new RegionMergeContext();
            }

            if (!isAcceptableAnswer(context.initAnswer(type), disallow)) {
                TopiaDAO<TopiaEntity> dao = IsisFishDAOHelper.getDAO(tx, type);

                // order matters here
                Set<TopiaEntity> possible = new LinkedHashSet<>();

                // from context (most match)
                for (TopiaEntity entity : entities.values()) {
                    if (type.isAssignableFrom(entity.getClass())) {
                        possible.add(entity);
                    }
                }

                // from db with same id
                TopiaEntity byTopiaIdFromDatabase = dao.findByTopiaId(id);
                if (byTopiaIdFromDatabase != null) {
                    possible.add(byTopiaIdFromDatabase);
                }
                if (Equation.class.isAssignableFrom(context.getCurrentType())) {
                    if (details.containsKey("name") && details.containsKey("category")) {
                        possible.addAll(dao.findAllByProperties("name", details.get("name"), "category", details.get("category")));
                    }
                } else {
                    if (details.containsKey("name")) {
                        possible.addAll(dao.findAllByProperty("name", details.get("name")));
                    }
                }

                if (!isAcceptableAnswer(context.initAnswer(type, possible), disallow)) {
                    if (Equation.class.isAssignableFrom(context.getCurrentType())) {
                        if (details.containsKey("category")) {
                            possible.addAll(dao.findAllByProperty("category", details.get("category")));
                        }
                    } else {
                        possible.addAll(dao.findAll());
                    }

                    ask(context, toString, details, possible, disallow);
                }
            }

            return lastAnswer = context.getAnswer();
        }

        protected AnswerType ask(RegionMergeContext context, String toString,
                Map<String, Object> details, Collection<TopiaEntity> possible,
                AnswerType ... disallow) {

            JPanel radioPanel = new JPanel(new FlowLayout());
            ButtonGroup group = new ButtonGroup();
            for (AutoAnswerType auto : AutoAnswerType.values()) {
                JRadioButton radio = new JRadioButton(auto.toString);
                radio.setSelected(AutoAnswerType.ONCE == auto);
                radio.setActionCommand(auto.name());
                group.add(radio);
                radioPanel.add(radio);
            }

            JComboBox<TopiaEntity> select = new JComboBox<>(possible.toArray(new TopiaEntity[0]));

            List<AnswerType> allowedOptions = new ArrayList<>(Arrays.asList(AnswerType.values()));
            for (AnswerType t : disallow) {
                allowedOptions.remove(t);
            }
            Object[] options = allowedOptions.toArray();

            List<Object> descriptions = new ArrayList<>();
            descriptions.add(t("isisfish.import.json.merge.info", toString, ClassUtils.getShortClassName(context.getCurrentType())));
            descriptions.add(t("isisfish.import.json.merge.possibleanswers"));
            for (AnswerType t : allowedOptions) {
                descriptions.add(t.toString() + ": " + t.getDescription());
            }
            descriptions.add(select);
            // answer only for this answer
            // make same answer for ask about same type
            // make same answer all time
            descriptions.add(radioPanel);

            int result = JOptionPane.showOptionDialog(null,
                    descriptions.toArray(),
                    t("isisfish.import.json.merge.title"),
                    JOptionPane.DEFAULT_OPTION,
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    options,
                    options[0]);

            TopiaEntity selectEntity = (TopiaEntity)select.getSelectedItem();
            if (result >= 0) {
                AnswerType reply = allowedOptions.get(result);
                AutoAnswerType auto = AutoAnswerType.valueOf(group.getSelection().getActionCommand());
                context.setLastAnswer(reply, auto, selectEntity);
            } else {
                ask(context, toString, details, possible);
            }
            return context.lastAnswerType;
        }

        @Override
        public boolean isAbort() {
            boolean result = lastAnswer != null && lastAnswer.type == AnswerType.ABORT;
            return result;
        }

    }

    static public class RegionMergeImportAll implements RegionMerge {
        protected Answer lastAnswer;
        protected Map<String, TopiaEntity> entities;

        @Override
        public boolean isAbort() {
            return false;
        }

        @Override
        public void setEntities(Map<String, TopiaEntity> entities) {
            this.entities = entities;
        }

        @Override
        public Answer choice(String id, String toString, Map<String, Object> details, AnswerType ... disallow) {
            return lastAnswer = new Answer(AnswerType.CREATE, null);
        }

    }

}
