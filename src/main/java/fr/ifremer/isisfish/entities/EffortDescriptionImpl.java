/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2010 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.entities;

import fr.ifremer.isisfish.types.TimeUnit;

import static org.nuiton.i18n.I18n.t;

/**
 * EffortDescriptionImpl.
 *
 * Created: 19 oct. 06 16:30:35
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class EffortDescriptionImpl extends EffortDescriptionAbstract {

    /** serialVersionUID. */
    private static final long serialVersionUID = 3668146790780517650L;

    @Override
    public String toString() {
        String result = t("isisfish.effortDescription.toString",
                getSetOfVessels(), getPossibleMetiers());
        return result;
    }

    public EffortDescriptionImpl() {
        fishingOperation = 1;
        gearsNumberPerOperation = 1;
        fishingOperationDuration = new TimeUnit(3600);
    }

}
