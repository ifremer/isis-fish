/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2010 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.entities;

import static org.nuiton.i18n.I18n.t;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import fr.ifremer.isisfish.util.EvaluatorHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixFactory;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.topia.TopiaException;

import fr.ifremer.isisfish.IsisFishDAOHelper;
import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.datastore.RegionStorage;
import fr.ifremer.isisfish.datastore.StorageException;
import fr.ifremer.isisfish.equation.Language;
import fr.ifremer.isisfish.equation.StrategyInactivityEquation;
import fr.ifremer.isisfish.types.Month;

/**
 * Implantation des operations pour l'entité Strategy.
 * 
 * Created: 20 janv. 2006 03:29:14
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class StrategyImpl extends StrategyAbstract {

    /** Class logger. */
    private static Log log = LogFactory.getLog(StrategyImpl.class);

    /** serialVersionUID. */
    private static final long serialVersionUID = 1L;

    public FisheryRegion getFisheryRegion() {
        try {
            FisheryRegion result = RegionStorage
                    .getFisheryRegion(getTopiaContext());
            return result;
        } catch (StorageException eee) {
            throw new IsisFishRuntimeException("Can't get fishery region", eee);
        }
    }

    /**
     * Override to change name of equation.
     * 
     * @see StrategyAbstract#setName(java.lang.String)
     */
    @Override
    public void setName(String value) {
        super.setName(value);
        Equation eq = getInactivityEquation();
        if (eq != null) {
            eq.setName(getName());
        }
    }

    /**
     * Set inactivityEquation. Do not forget to specify inactivity equation use.
     * @see #setInactivityEquationUsed(boolean)
     *
     * @param inactivityEquation
     */
    @Override
    public void setInactivityEquation(Equation inactivityEquation) {
        super.setInactivityEquation(inactivityEquation);
        computeNumberOfTrips();
    }

    @Override
    public List<StrategyMonthInfo> getStrategyMonthInfo() {
        try {
            List<StrategyMonthInfo> result = super.getStrategyMonthInfo();
            if (result == null || result.size() != Month.NUMBER_OF_MONTH) {
                if (result != null) {
                    log.warn(t("isisfish.error.strategy.order", getName(),
                            result.size()));
                    clearStrategyMonthInfo();
                } else {
                    result = new ArrayList<>(Month.NUMBER_OF_MONTH);
                    setStrategyMonthInfo(result);
                }
                for (Month month : Month.MONTH) {
                    StrategyMonthInfoDAO dao = IsisFishDAOHelper
                            .getStrategyMonthInfoDAO(getTopiaContext());
                    StrategyMonthInfo smi = dao.create();
                    smi.setMonth(month);
                    smi.setStrategy(this);
                    smi.update();
                    addStrategyMonthInfo(smi);
                }
                this.update();
                result = super.getStrategyMonthInfo();
            }
            return result;
        } catch (TopiaException eee) {
            throw new IsisFishRuntimeException(
                    "Can't create StrategyMonthInfo", eee);
        }
    }

    @Override
    public StrategyMonthInfo getStrategyMonthInfo(Month month) {
        List<StrategyMonthInfo> smis = getStrategyMonthInfo();
        StrategyMonthInfo result = smis.get(month.getMonthNumber());
        if (!month.equals(result.getMonth())) {
            // normalement les StrategyMonthInfo sont dans l'ordre mais 
            // pour etre sur que ca fonctionne on fait un petit test
            log.warn("StrategyMonthInfo not in month order, i take time to find good StrategyMonthInfo");
            result = null;
            for (StrategyMonthInfo smi : smis) {
                if (month.equals(smi.getMonth())) {
                    result = smi;
                    break;
                }
            }
        }
        return result;
    }

    /**
     * Compute or return inactivity days.
     * 
     * @param month month
     * @return age in month
     */
    public double getInactivityDays(Month month) {
        double result = 0;

        StrategyMonthInfo info = getStrategyMonthInfo(month);
        if (isInactivityEquationUsed()) {
            try {
                Equation eq = getInactivityEquation();
                result = eq.evaluate("month", month, "info", info);
            } catch (Exception ex) {
                EvaluatorHelper.catchEvaluateException(ex, log);
            }
        } else {
            result = info.getMinInactivityDays();
        }
        return result;
    }

    /**
     * Set inactivityEquationUsed and launch numberOfTrips computation.
     * @see StrategyImpl#computeNumberOfTrips()
     *
     * @param inactivityEquationUsed wether to use inactivityEquation or not
     */
    @Override
    public void setInactivityEquationUsed(boolean inactivityEquationUsed) {
        super.setInactivityEquationUsed(inactivityEquationUsed);
        computeNumberOfTrips();
    }

    protected void computeNumberOfTrips() {
        if (getTopiaContext() != null && getStrategyMonthInfo() != null) { // getTopiaContext() != null => json import error
            getStrategyMonthInfo().stream().map(StrategyMonthInfoImpl.class::cast).forEach(StrategyMonthInfoImpl::computeNumberOfTrips);
        }
    }

    /**
     * Set inactivityEquation content. Do not forget to specify inactivity equation use.
     * @see #setInactivityEquationUsed(boolean)
     *
     * @param content inactivityEquation content
     */
    public void setInactivityEquationContent(String content) {
        try {
            Equation eq = getInactivityEquation();

            if (eq == null) {
                EquationDAO dao = IsisFishDAOHelper
                        .getEquationDAO(getTopiaContext());
                // create Growth equation
                eq = dao.create();
                eq.setCategory("Inactivity");
                eq.setJavaInterface(StrategyInactivityEquation.class);
                eq.setLanguage(Language.JAVA);
                setInactivityEquation(eq);
            }

            eq.setName(getName());

            // Fire
            String _oldValue = eq.getContent();
            fireOnPreWrite("content", _oldValue, content);

            eq.setContent(content);
            eq.update();

            computeNumberOfTrips();

            fireOnPostWrite("content", _oldValue, content);

        } catch (TopiaException eee) {
            throw new IsisFishRuntimeException(t("isisfish.change.equation"),
                    eee);
        }
    }

    /**
     * Return a single matrix with all sub strategymonthinfo's proportionMetier
     * to be displayed in a single matrix in UI.
     * 
     * <ul>
     * <li>dim 0 : metier</li>
     * <li>dim 1 : month</li>
     * </ul>
     * @since 4.0.0.0 
     * @return
     */
    public MatrixND getProportionMetier() {

        if (getSetOfVessels() == null || getSetOfVessels().getPossibleMetiers() == null) {
            return null;
        }

        Collection<EffortDescription> efforts = getSetOfVessels().getPossibleMetiers();

        // dim 1 : metier
        List<Metier> metiers = new ArrayList<>(efforts.size());
        for (EffortDescription effort : efforts) {
            metiers.add(effort.getPossibleMetiers());
        }

        // dim 2 : month
        List<Month> months = Arrays.asList(Month.MONTH);

        MatrixND result = MatrixFactory.getInstance().create(
                t("isisfish.strategy.proportionMetier"),
                new List[] { metiers, months},
                new String[] { t("isisfish.strategyMonthInfo.metier"),
                    t("isisfish.common.months")});

        // copy sub StrategyMonthInfo's proportionMetier to current
        if (getStrategyMonthInfo() != null) {
            for (StrategyMonthInfo smi : getStrategyMonthInfo()) {
                if (smi.getProportionMetier() != null) {
                    // pasteSemantics ne fonctionne pas (pas de month)
                    // copie à la mano
                    MatrixND mat = smi.getProportionMetier();
                    for (Metier metier : metiers) {
                        double value = mat.getValue(metier);
                        result.setValue(metier, smi.getMonth(), value);
                    }
                }
            }
        }
        return result;
    }

    public void setProportionMetier(MatrixND strategyProportionMetier) {

        if (getSetOfVessels() == null || getSetOfVessels().getPossibleMetiers() == null) {
            return;
        }

        List<Month> months = (List<Month>)strategyProportionMetier.getSemantic(1);
        
        for (Month month : months) {
            MatrixND proportionMetier = strategyProportionMetier.getSubMatrix(1, month).reduce();
            
            StrategyMonthInfo smi = getStrategyMonthInfo(month);
            smi.setProportionMetier(proportionMetier);
        }
    }

    @Override
    public String toString() {
        String result = getName();
        return result;
    }

} //StrategyImpl
