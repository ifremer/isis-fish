/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2010 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.entities;

import static org.nuiton.i18n.I18n.t;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * MetierSeasonInfoImpl.
 *
 * Created: 23 août 2006 16:25:40
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class MetierSeasonInfoImpl extends MetierSeasonInfoAbstract {

    /** serialVersionUID. */
    private static final long serialVersionUID = 4554500494092542926L;

    /** Logger for this class. */
    private static final Log log = LogFactory.getLog(MetierSeasonInfoImpl.class);

    @Override
    public double getTargetFactor(PopulationGroup group) {
        // Par defaut on retourne 1 si dans les donnes rien n'est
        // precisé (20041108: Stef + dom)
        double result = 1;

        Species species = group.getPopulation().getSpecies();
        TargetSpecies targetSpecies = this.getSpeciesTargetSpecies(species);
        if (targetSpecies != null) {
            result = targetSpecies.getTargetFactor(group);
        } else {
            if (log.isDebugEnabled()) {
                log.debug(t("isisfish.error.no.target.species", this, group));
            }
        }
        return result;
    }

    @Override
    public List<Cell> getCells() {
        List<Cell> result = new ArrayList<>();
        for (Zone zone : getZone()) {
            result.addAll(zone.getCell());
        }
        return result;
    }

    @Override
    public String toString() {
        String result = t("isisfish.metierSeasonInfo.toString", this
                .getMetier(), this.getFirstMonth(), this.getLastMonth());
        return result;
    }

}
