/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2010 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.entities;

import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.datastore.RegionStorage;
import fr.ifremer.isisfish.datastore.StorageException;
import java.util.Collection;

/**
 * Implantation des operations pour l'entité Species.
 * 
 * Created: 20 janv. 2006 03:29:14
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class SpeciesImpl extends SpeciesAbstract {

    /** serialVersionUID. */
    private static final long serialVersionUID = 1L;

    public FisheryRegion getFisheryRegion() {
        try {
            FisheryRegion result = RegionStorage
                    .getFisheryRegion(getTopiaContext());
            return result;
        } catch (StorageException eee) {
            throw new IsisFishRuntimeException("Can't get fishery region", eee);
        }
    }

    /**
     * Surcharge car avec une aggregation (lien fort) il ne faut pas remplacer
     * la collection, mais vider celle qui existe et y mettre les nouveaux elements
     * sinon hibernate perd la tete et leve des exceptions (detecter lors de l'implantation
     * de l'import json)
     */
    @Override
    public void setPopulation(Collection<Population> population) {
        clearPopulation();
        addAllPopulation(population);
    }

    @Override
    public String toString() {
        String result = getName();
        return result;
    }

} //SpeciesImpl
