/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2010 Ifremer, Code Lutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.entities;

import java.util.Collection;

import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.datastore.RegionStorage;
import fr.ifremer.isisfish.datastore.StorageException;
import fr.ifremer.isisfish.types.Month;
import java.util.List;


/**
 * Implantation des operations pour l'entité Metier.
 * 
 * Created: 14 févr. 2006 11:32:40
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class MetierImpl extends MetierAbstract {

    /** serialVersionUID. */
    private static final long serialVersionUID = 1L;

    @Override
    public FisheryRegion getFisheryRegion() {
        try {
            FisheryRegion result = RegionStorage
                    .getFisheryRegion(getTopiaContext());
            return result;
        } catch (StorageException eee) {
            throw new IsisFishRuntimeException("Can't get fishery region", eee);
        }
    }

    /**
     * Surcharge car avec une aggregation (lien fort) il ne faut pas remplacer
     * la collection, mais vider celle qui existe et y mettre les nouveaux elements
     * sinon hibernate perd la tete et leve des exceptions (detecter lors de l'implantation
     * de l'import json)
     */
    @Override
    public void setMetierSeasonInfo(List<MetierSeasonInfo> metierSeasonInfo) {
        clearMetierSeasonInfo();
        addAllMetierSeasonInfo(metierSeasonInfo);
    }

    @Override
    public MetierSeasonInfo getMetierSeasonInfo(Month month) {
        Collection<MetierSeasonInfo> all = getMetierSeasonInfo();
        MetierSeasonInfo result = null;
        for (MetierSeasonInfo msi : all) {
            if (msi.containsMonth(month)) {
                result = msi;
                break;
            }
        }
        return result;
    }

    @Override
    public double getGearParameterValueAsDouble() {
        String val = getGearParameterValue();
        double result = 0;
        if (val != null && !"".equals(val)) {
            result = Double.parseDouble(val);
        }
        return result;
    }

    @Override
    public String toString() {
        String result = getName();
        return result;
    }

} //MetierImpl
