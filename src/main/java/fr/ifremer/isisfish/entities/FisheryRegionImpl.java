/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2010 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.entities;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import fr.ifremer.isisfish.ui.input.InputContext;
import org.nuiton.topia.TopiaException;

import fr.ifremer.isisfish.IsisFishDAOHelper;
import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.datastore.RegionStorage;

/**
 * FisheryRegionImpl.
 *
 * Created: 3 janv. 2006 17:14:52
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class FisheryRegionImpl extends FisheryRegionAbstract {

    /** serialVersionUID */
    private static final long serialVersionUID = 1L;

    @Override
    public List<String> getMapFileList() {
        List<String> result = new ArrayList<>();
        String mapfiles = getMapFiles();
        if (mapfiles != null) {
            String[] mapNames = mapfiles.split(",");
            for (String mapName : mapNames) {
                if (!"".equals(mapName.trim())) {
                    result.add(mapName.trim());
                }
            }
        }
        return result;
    }

    @Override
    public void setMapFileList(List<String> maps) {
        StringBuilder result = new StringBuilder();
        String sep = "";
        if (maps != null) {
            for (String filename : maps) {
                result.append(sep + filename);
                sep = ",";
            }
        }
        setMapFiles(result.toString());
    }

    @Override
    public List<String> getMapFilePath() {
        List<String> mapNames = getMapFileList();
        List<String> result = new ArrayList<>();
        // not working with different name
        // RegionStorage regionStorage = RegionStorage.getRegion(getName());
        RegionStorage regionStorage = InputContext.getStorage();
        for (String mapName : mapNames) {
            result.add(regionStorage.getMapRepository().getPath()
                    + File.separator + mapName);
        }
        return result;
    }

    @Override
    public List<Port> getPort() {
        try {
            PortDAO dao = IsisFishDAOHelper.getPortDAO(getTopiaContext());
            List<Port> result = dao.findAll();
            return result;
        } catch (TopiaException eee) {
            throw new IsisFishRuntimeException("Can't get port", eee);
        }
    }

    @Override
    public List<Cell> getCell() {
        try {
            CellDAO dao = IsisFishDAOHelper.getCellDAO(getTopiaContext());
            List<Cell> result = dao.findAll();
            return result;
        } catch (TopiaException eee) {
            throw new IsisFishRuntimeException("Can't get cell", eee);
        }
    }

    @Override
    public List<VesselType> getVesselType() {
        try {
            VesselTypeDAO dao = IsisFishDAOHelper
                    .getVesselTypeDAO(getTopiaContext());
            List<VesselType> result = dao.findAll();
            return result;
        } catch (TopiaException eee) {
            throw new IsisFishRuntimeException("Can't get vessel type", eee);
        }
    }

    @Override
    public List<Gear> getGear() {
        try {
            GearDAO dao = IsisFishDAOHelper.getGearDAO(getTopiaContext());
            List<Gear> result = dao.findAll();
            return result;
        } catch (TopiaException eee) {
            throw new IsisFishRuntimeException("Can't get gear", eee);
        }
    }

    @Override
    public List<Metier> getMetier() {
        try {
            MetierDAO dao = IsisFishDAOHelper.getMetierDAO(getTopiaContext());
            List<Metier> result = dao.findAll();
            return result;
        } catch (TopiaException eee) {
            throw new IsisFishRuntimeException("Can't get metier", eee);
        }
    }

    @Override
    public List<SetOfVessels> getSetOfVessels() {
        try {
            SetOfVesselsDAO dao = IsisFishDAOHelper
                    .getSetOfVesselsDAO(getTopiaContext());
            List<SetOfVessels> result = dao.findAll();
            return result;
        } catch (TopiaException eee) {
            throw new IsisFishRuntimeException("Can't get setOfVessels", eee);
        }
    }

    @Override
    public List<Species> getSpecies() {
        try {
            SpeciesDAO dao = IsisFishDAOHelper.getSpeciesDAO(getTopiaContext());
            List<Species> result = dao.findAll();
            return result;
        } catch (TopiaException eee) {
            throw new IsisFishRuntimeException("Can't get species", eee);
        }
    }

    @Override
    public List<Strategy> getStrategy() {
        try {
            StrategyDAO dao = IsisFishDAOHelper
                    .getStrategyDAO(getTopiaContext());
            List<Strategy> result = dao.findAll();
            return result;
        } catch (TopiaException eee) {
            throw new IsisFishRuntimeException("Can't get strategy", eee);
        }
    }

    @Override
    public List<TripType> getTripType() {
        try {
            TripTypeDAO dao = IsisFishDAOHelper
                    .getTripTypeDAO(getTopiaContext());
            List<TripType> result = dao.findAll();
            return result;
        } catch (TopiaException eee) {
            throw new IsisFishRuntimeException("Can't get tripType", eee);
        }
    }

    @Override
    public List<Zone> getZone() {
        try {
            ZoneDAO dao = IsisFishDAOHelper.getZoneDAO(getTopiaContext());
            List<Zone> result = dao.findAll();
            return result;
        } catch (TopiaException eee) {
            throw new IsisFishRuntimeException("Can't get zone", eee);
        }
    }

    @Override
    public List<Observation> getObservations() {
        try {
            ObservationDAO dao = IsisFishDAOHelper.getObservationDAO(getTopiaContext());
            List<Observation> result = dao.findAll();
            return result;
        } catch (TopiaException eee) {
            throw new IsisFishRuntimeException("Can't get observations", eee);
        }
    }

    @Override
    public String toString() {
        String result = getName();
        return result;
    }
}
