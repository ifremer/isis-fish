/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2010 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.entities;

import static org.nuiton.i18n.I18n.t;

import fr.ifremer.isisfish.util.EvaluatorHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;

import fr.ifremer.isisfish.IsisFishDAOHelper;
import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.equation.Language;
import fr.ifremer.isisfish.equation.TargetSpeciesTargetFactorEquation;

/**
 * TargetSpeciesImpl.
 *
 * Created: 23 août 2006 16:38:23
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class TargetSpeciesImpl extends TargetSpeciesAbstract {

    /** serialVersionUID. */
    private static final long serialVersionUID = 8028733540961872729L;

    /** Class logger. */
    private static Log log = LogFactory.getLog(TargetSpeciesImpl.class);

    @Override
    public Equation getTargetFactorEquation() {
        if (super.getTargetFactorEquation() == null) {
            try {
                EquationDAO dao = IsisFishDAOHelper
                        .getEquationDAO(getTopiaContext());
                // create equation
                Equation eq = dao.create();
                eq.setCategory("TargetFactor");
                eq.setLanguage(Language.JAVA);
                eq.setJavaInterface(TargetSpeciesTargetFactorEquation.class);
                eq.update();
                setTargetFactorEquation(eq);
            } catch (TopiaException eee) {
                throw new IsisFishRuntimeException(
                        t("isisfish.error.create.equation"), eee);
            }

        }
        return super.getTargetFactorEquation();
    }

    /**
     * Equation content setter for TargetFactorEquation.
     * 
     * @param content content to set
     */
    public void setTargetFactorEquationContent(String content) {
        try {
            Equation eq = getTargetFactorEquation();

            // can't be null

            // Fire
            String _oldValue = eq.getContent();
            fireOnPreWrite("content", _oldValue, content);

            eq.setContent(content);
            eq.update();

            fireOnPostWrite("content", _oldValue, content);

        } catch (TopiaException eee) {
            throw new IsisFishRuntimeException(
                    t("isisfish.error.change.equation"), eee);
        }
    }

    @Override
    public void setMetierSeasonInfo(MetierSeasonInfo value) {
        super.setMetierSeasonInfo(value);
        setEquationName(getTargetFactorEquation());
    }

    @Override
    public void setSpecies(Species value) {
        super.setSpecies(value);
        setEquationName(getTargetFactorEquation());
    }

    protected void setEquationName(Equation eq) {
        if (getMetierSeasonInfo() != null
                && getMetierSeasonInfo().getMetier() != null
                && getSpecies() != null) {
            eq.setName(getMetierSeasonInfo().getMetier().getName() + "-"
                    + getSpecies().getName() + "("
                    + getMetierSeasonInfo().toString() + ")");
        }
    }

    @Override
    public double getTargetFactor(PopulationGroup group) {
        double result = 0;
        try {
            Equation eq = getTargetFactorEquation();
            if (eq != null) {
                Species species = getSpecies();
                MetierSeasonInfo msi = getMetierSeasonInfo();
                boolean primaryCatch = isPrimaryCatch();
                result = eq.evaluate("group", group, "species", species,
                        "infoMetier", msi, "primaryCatch", primaryCatch);
            }
        } catch (Exception ex) {
            EvaluatorHelper.catchEvaluateException(ex, log);
        }
        return result;
    }

    @Override
    public String toString() {
        String result = "" + getTargetFactorEquation();
        return result;
    }

}
