/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2011 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.entities;

import java.util.List;

import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;

import fr.ifremer.isisfish.IsisFishDAOHelper;
import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.datastore.RegionStorage;
import fr.ifremer.isisfish.datastore.StorageException;
import fr.ifremer.isisfish.types.TimeUnit;

/**
 * Implantation des operations pour l'entité TripType.
 * 
 * Created: 23 août 2006 16:38:23
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class TripTypeImpl extends TripTypeAbstract {

    /** serialVersionUID. */
    private static final long serialVersionUID = 1L;

    public TripTypeImpl() {
        setMinTimeBetweenTrip(new TimeUnit(0));
        setTripDuration(new TimeUnit(0));
    }

    @Override
    public void setTripDuration(TimeUnit tripDuration) {
        super.setTripDuration(tripDuration);

        // compute number of trip for all StrategyMonthInfo
        // using current trip type
        TopiaContext context = getTopiaContext();
        if (context != null) { // can be if just new TripTypeImpl()
            try {
                StrategyMonthInfoDAO smiDAO = IsisFishDAOHelper.getStrategyMonthInfoDAO(context);
                List<StrategyMonthInfo> smis = smiDAO.findAllByTripType(this);
                for (StrategyMonthInfo smi : smis) {
                    StrategyMonthInfoImpl smiImpl = (StrategyMonthInfoImpl)smi;
                    smiImpl.computeNumberOfTrips();
                    smi.update();
                }
            } catch (TopiaException eee) {
                throw new IsisFishRuntimeException("Can't get fishery region", eee);
            }
        }
    }

    @Override
    public FisheryRegion getFisheryRegion() {
        try {
            FisheryRegion result = RegionStorage
                    .getFisheryRegion(getTopiaContext());
            return result;
        } catch (StorageException eee) {
            throw new IsisFishRuntimeException("Can't get fishery region", eee);
        }
    }

    @Override
    public String toString() {
        String result = getName();
        return result;
    }

} //TripTypeImpl
