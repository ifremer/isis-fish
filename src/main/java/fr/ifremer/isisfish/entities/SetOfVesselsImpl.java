/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2010 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.entities;

import static org.nuiton.i18n.I18n.t;

import fr.ifremer.isisfish.util.EvaluatorHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;

import fr.ifremer.isisfish.IsisFishDAOHelper;
import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.datastore.RegionStorage;
import fr.ifremer.isisfish.datastore.StorageException;
import fr.ifremer.isisfish.equation.Language;
import fr.ifremer.isisfish.equation.SoVTechnicalEfficiencyEquation;

/**
 * Implantation des operations pour l'entité SetOfVessels.
 * 
 * Created: 20 janv. 2006 03:29:14
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class SetOfVesselsImpl extends SetOfVesselsAbstract {

    /** serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** Logger for this class. */
    private static final Log log = LogFactory.getLog(SetOfVesselsImpl.class);

    public FisheryRegion getFisheryRegion() {
        try {
            FisheryRegion result = RegionStorage
                    .getFisheryRegion(getTopiaContext());
            return result;
        } catch (StorageException eee) {
            throw new IsisFishRuntimeException("Can't get fishery region", eee);
        }
    }

    /**
     * Override to change name of equation.
     * 
     * @see SetOfVesselsAbstract#setName(java.lang.String)
     */
    @Override
    public void setName(String value) {
        super.setName(value);
        Equation eq = getTechnicalEfficiencyEquation();
        if (eq != null) {
            eq.setName(getName());
        }
    }

    /**
     * Evaluate {@link SetOfVesselsAbstract#technicalEfficiencyEquation} with 
     * {@link Gear} and {@link Metier}.
     * 
     * By default return {@code 1} if equation can't be evaluated.
     */
    @Override
    public double getTechnicalEfficiency(Metier metier) {
        double result = 1;
        try {
            Equation eq = getTechnicalEfficiencyEquation();
            if (eq != null) {
                Gear gear = metier.getGear();
                result = eq.evaluate("metier", metier, "gear", gear);
            }
        } catch (Exception ex) {
            EvaluatorHelper.catchEvaluateException(ex, log);
        }
        return result;
    }

    public void setTechnicalEfficiencyEquationContent(String content) {
        try {
            Equation eq = getTechnicalEfficiencyEquation();

            if (eq == null) {
                EquationDAO dao = IsisFishDAOHelper
                        .getEquationDAO(getTopiaContext());
                // create Growth equation
                eq = dao.create();
                eq.setCategory("TechnicalEfficiency");
                eq.setJavaInterface(SoVTechnicalEfficiencyEquation.class);
                eq.setLanguage(Language.JAVA);
                setTechnicalEfficiencyEquation(eq);
            }

            eq.setName(getName());

            // Fire
            String _oldValue = eq.getContent();
            fireOnPreWrite("content", _oldValue, content);

            eq.setContent(content);
            eq.update();

            fireOnPostWrite("content", _oldValue, content);

        } catch (TopiaException eee) {
            throw new IsisFishRuntimeException(t("isisfish.change.equation"),
                    eee);
        }
    }

    @Override
    public String toString() {
        String result = getName();
        return result;
    }

} //SetOfVesselsImpl
