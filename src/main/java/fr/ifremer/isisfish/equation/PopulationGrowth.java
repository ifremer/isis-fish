/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2010 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.equation;

import fr.ifremer.isisfish.entities.PopulationGroup;
import fr.ifremer.isisfish.result.NecessaryResult;
import fr.ifremer.isisfish.simulator.SimulationContext;
import fr.ifremer.isisfish.annotations.Args;

/**
 * Population's growth equation.
 *
 * Created: 4 juil. 2006 13:03:25
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public interface PopulationGrowth extends NecessaryResult {

    String DEFAULT_CONTENT = "return 0.0;";

    /**
     * Compute growth equation.
     * 
     * @param context simulation context
     * @param age l'age du groupe en mois
     * @param group le groupe dont on souhaite avoir la longueur en fonction de l'age
     * @return le resultat de l'equation (scalaire)
     * @throws Exception 
     */
    @Args({"context", "age", "group"})
    double compute(SimulationContext context, double age, PopulationGroup group) throws Exception;

}
