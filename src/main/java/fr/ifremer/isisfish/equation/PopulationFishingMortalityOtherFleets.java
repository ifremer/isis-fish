/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.equation;

import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.entities.PopulationGroup;
import fr.ifremer.isisfish.entities.Zone;
import fr.ifremer.isisfish.result.NecessaryResult;
import fr.ifremer.isisfish.simulator.SimulationContext;
import fr.ifremer.isisfish.annotations.Args;

/**
 * Equation de mortalité par pêche Other.
 *
 * @author echatellier
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public interface PopulationFishingMortalityOtherFleets  extends NecessaryResult {

    String DEFAULT_CONTENT = "return 0.0;";

    /**
     * Compute fishing mortality other fleets.
     * 
     * @param context simulation context
     * @param pop la population pour lequel il faut calculer la mortalité
     * @param group le groupe pour lequel il faut calculer la mortalité
     * null, si on souhaite la mortalité naturelle du group des naissances
     * @param zone la zone du groupe pour lequel on souhaite la mortalité
     * seulement utilisé pour la mortalité naturelle du group des naissances,
     * sinon null
     * @return le resultat de l'equation (scalaire)
     * @throws Exception 
     */
    @Args({"context", "pop", "group", "zone"})
    double compute(SimulationContext context,
            Population pop, PopulationGroup group, Zone zone) throws Exception;

}
