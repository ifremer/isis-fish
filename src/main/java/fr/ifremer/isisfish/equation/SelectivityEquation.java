/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2011 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.equation;

import fr.ifremer.isisfish.entities.Metier;
import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.entities.PopulationGroup;
import fr.ifremer.isisfish.result.NecessaryResult;
import fr.ifremer.isisfish.simulator.SimulationContext;
import fr.ifremer.isisfish.annotations.Args;

/**
 * Selectivity equation.
 *
 * Created: 23 août 2006 11:39:38
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public interface SelectivityEquation extends NecessaryResult {

    String DEFAULT_CONTENT = "return 0.0;";

    /**
     * Compute selectivity.
     * 
     * @param context simulation context
     * @param pop la population pour lequel on souhaite la selectivite de l'engin
     * @param group le group pour lequel on souhaite la selectivite de l'engin
     * @param metier le metier pour lequel on souhaite la selectivite de l'engin
     * @return le resultat de l'equation (scalaire)
     * @throws Exception 
     */
    @Args({"context", "pop", "group", "metier"})
    double compute(SimulationContext context,
            Population pop, PopulationGroup group, Metier metier) throws Exception;

}
