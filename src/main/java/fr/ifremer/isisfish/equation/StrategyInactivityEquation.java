/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2011 Ifremer, Code Lutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.equation;

import fr.ifremer.isisfish.entities.StrategyMonthInfo;
import fr.ifremer.isisfish.result.NecessaryResult;
import fr.ifremer.isisfish.simulator.SimulationContext;
import fr.ifremer.isisfish.types.Month;
import fr.ifremer.isisfish.annotations.Args;

/**
 * Strategy's inactivity equation.
 * 
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public interface StrategyInactivityEquation extends NecessaryResult {

    String DEFAULT_CONTENT = "return 0.0;";

    /**
     * Compute inactivity.
     * 
     * @param context simulation context
     * @param month le mois courant de simulation
     * @param info l'objet associe a la strategie pour le mois courant
     * @return le resultat de l'equation (scalaire)
     * @throws Exception 
     */
    @Args({"context", "month", "info"})
    double compute(SimulationContext context, Month month, StrategyMonthInfo info) throws Exception;

}
