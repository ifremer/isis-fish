/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2012 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.equation;

import fr.ifremer.isisfish.result.NecessaryResult;
import org.nuiton.topia.persistence.TopiaEntity;

import fr.ifremer.isisfish.simulator.SimulationContext;
import fr.ifremer.isisfish.types.TimeStep;
import fr.ifremer.isisfish.annotations.Args;

/**
 * Generic equation signature used in variable values.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public interface VariableEquation extends NecessaryResult {

    String DEFAULT_CONTENT = "return 1.0;";

    /**
     * Compute target factor.
     * 
     * @param context simulation context
     * @param entity l'entity sur laquelle la variable a été ajoutée
     * @param step le pas de temps courant
     * @return le resultat de l'equation (scalaire)
     * @throws Exception 
     */
    @Args({"context", "entity", "step"})
    double compute(SimulationContext context, TopiaEntity entity, TimeStep step) throws Exception;

}
