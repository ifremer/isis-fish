/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.equation;

import fr.ifremer.isisfish.result.NecessaryResult;
import org.nuiton.math.matrix.MatrixND;

import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.simulator.SimulationContext;
import fr.ifremer.isisfish.types.RecruitmentInputMap;
import fr.ifremer.isisfish.types.TimeStep;
import fr.ifremer.isisfish.annotations.Args;

/**
 * Population recruitment equation.
 *
 * @author echatellier
 * @since 4.4.0.0
 */
public interface PopulationRecruitmentEquation extends NecessaryResult {

    String DEFAULT_CONTENT = "return 0.0;";

    /**
     * Compute recruitment equation.
     * 
     * @param context simulation context
     * @param step current time step
     * @param pop population pour lequel on souhaite la matrice de recrutement
     * @param recruitmentInputs les abundances, biomasses et reproduction correspondant au recrutement courant
     * @param result la matrice resultat que l'equation doit remplir<br>
     *        <strong>Sémantiques : </strong> {@link fr.ifremer.isisfish.entities.PopulationGroup}, <br />
     *        {@link fr.ifremer.isisfish.entities.Zone} (all zones and not only recruitment zones)
     * @return la valeur retournée n'est pas utilisée, par exemple 'return 0;' convient.
     * @throws Exception 
     */
    @Args({"context", "step", "pop", "recruitmentInputs", "result"})
    double compute(SimulationContext context, TimeStep step,
            Population pop, RecruitmentInputMap recruitmentInputs, MatrixND result) throws Exception;

}
