/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2011 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.equation;

import java.util.List;

import fr.ifremer.isisfish.result.NecessaryResult;
import org.nuiton.math.matrix.MatrixND;

import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.entities.PopulationGroup;
import fr.ifremer.isisfish.entities.Zone;
import fr.ifremer.isisfish.simulator.SimulationContext;
import fr.ifremer.isisfish.types.Month;
import fr.ifremer.isisfish.annotations.Args;

/**
 * Population reproduction equation.
 *
 * Created: 23 août 2006 11:11:40
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public interface PopulationReproductionEquation extends NecessaryResult {

    String DEFAULT_CONTENT = "return 0.0;";

    /**
     * Equation de reproduction.
     * 
     * @param context Context de simulation
     * @param N Effectif de la population<br>
     *          <strong>Sémantiques : </strong> {@link fr.ifremer.isisfish.entities.PopulationGroup}, {@link fr.ifremer.isisfish.entities.Zone}
     * @param pop Population pour lequel on souhaite la matrice de reproduction
     * @param month Le mois pour lequel on souhaite la matrice de reproduction
     * @param prepro Le coefficient de reproduction de la population pour ce mois
     * @param zoneRepro La liste des zones de reproduction (dimension 0 de result)
     * @param groups La liste des groups de la population (dimension 0 de N)
     * @param zones La liste des zones de la population (dimension 1 de N)
     * @param result La matrice resultat que l'equation doit remplir<br>
     *               <strong>Sémantiques : </strong> {@link fr.ifremer.isisfish.entities.Zone} (only reproduction zones)
     * @return la valeur retournée n'est pas utilisée, par exemple 'return 0;'
     * convient.
     * @throws Exception 
     */
    @Args({"context", "N", "pop", "month", "prepro", "zoneRepro", "groups", "zones", "result"})
    double compute(SimulationContext context,
            MatrixND N, Population pop, Month month, double prepro,
            List<Zone> zoneRepro, List<PopulationGroup> groups, List<Zone> zones,
            MatrixND result) throws Exception;

}
