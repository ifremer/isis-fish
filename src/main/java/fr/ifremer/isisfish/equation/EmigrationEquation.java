/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2011 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.equation;

import fr.ifremer.isisfish.result.NecessaryResult;
import org.nuiton.math.matrix.MatrixND;

import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.entities.PopulationGroup;
import fr.ifremer.isisfish.entities.Zone;
import fr.ifremer.isisfish.simulator.SimulationContext;
import fr.ifremer.isisfish.annotations.Args;

/**
 * Emigration equation.
 *
 * Created: 23 août 2006 12:06:02
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public interface EmigrationEquation extends NecessaryResult {

    String DEFAULT_CONTENT = "return 0.0;";

    /**
     * Compute emigration equation.
     * 
     * @param context simulation context
     * @param N effectif de la population<br>
     *        <strong>Sémantiques : </strong> {@link fr.ifremer.isisfish.entities.PopulationGroup}, {@link fr.ifremer.isisfish.entities.Zone}
     * @param pop la population pour lequel on souhaite le coefficient d'emigration
     * @param group le group pour lequel on souhaite le coefficient d'emigration
     * @param departureZone la zone de depart
     * @return la proportion d'individu a emigrer de la zone de depart
     * pour le groupe passé en argument
     * @throws Exception 
     */
    @Args({"context", "N", "pop", "group", "departureZone"})
    double compute(SimulationContext context,
            MatrixND N, Population pop, PopulationGroup group,
            Zone departureZone) throws Exception;

}
