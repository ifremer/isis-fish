/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2011 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.equation;

import fr.ifremer.isisfish.annotations.Args;
import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.entities.PopulationGroup;
import fr.ifremer.isisfish.entities.PopulationSeasonInfo;
import fr.ifremer.isisfish.result.NecessaryResult;
import fr.ifremer.isisfish.simulator.SimulationContext;

/**
 * Population capturability equation.
 *
 * Created: 23 août 2006 11:11:40
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public interface PopulationCapturabilityEquation extends NecessaryResult {

    String DEFAULT_CONTENT = "return 0.0;";

    /**
     * Compute capturability equation.
     * 
     * @param context simulation context
     * @param pop population pour lequel on souhaite la matrice de reproduction
     * @param group le group de la population
     * @param season la saison de la population
     * 
     * @return la valeur retournée n'est pas utilisée, par exemple 'return 0;'
     * convient.
     * @throws Exception 
     */
    @Args({"context", "pop", "group", "season"})
    double compute(SimulationContext context, Population pop,
            PopulationGroup group, PopulationSeasonInfo season) throws Exception;

}
