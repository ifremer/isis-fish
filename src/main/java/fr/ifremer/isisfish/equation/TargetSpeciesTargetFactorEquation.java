/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2011 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.equation;

import fr.ifremer.isisfish.entities.MetierSeasonInfo;
import fr.ifremer.isisfish.entities.PopulationGroup;
import fr.ifremer.isisfish.entities.Species;
import fr.ifremer.isisfish.result.NecessaryResult;
import fr.ifremer.isisfish.simulator.SimulationContext;
import fr.ifremer.isisfish.annotations.Args;

/**
 * Target species's target factor equation.
 *
 * Created: 23 août 2006 16:44:40
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public interface TargetSpeciesTargetFactorEquation extends NecessaryResult {

    // Par defaut on retourne 1 si dans les donnes rien n'est
    // precisé (20041108: Stef + dom)
    String DEFAULT_CONTENT = "return 1.0;";

    /**
     * Compute target factor.
     * 
     * @param context simulation context
     * @param group le group pour lequel on recherche le facteur de ciblage
     * @param species l'espece pour lequel on recherche le facteur de ciblage 
     * @param infoMetier
     * @param primaryCatch
     * @return le resultat de l'equation (scalaire)
     * @throws Exception 
     */
    @Args({"context", "group", "species", "infoMetier", "primaryCatch"})
    double compute(SimulationContext context,
            PopulationGroup group, Species species,
            MetierSeasonInfo infoMetier, boolean primaryCatch) throws Exception;

}
