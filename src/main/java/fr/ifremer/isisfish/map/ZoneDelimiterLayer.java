/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2017 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.map;

import com.bbn.openmap.Layer;
import com.bbn.openmap.event.ProjectionEvent;
import com.bbn.openmap.omGraphics.OMGraphic;
import com.bbn.openmap.omGraphics.OMGraphicList;
import com.bbn.openmap.omGraphics.OMRect;
import com.bbn.openmap.proj.Projection;
import fr.ifremer.isisfish.entities.Cell;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Collection;

/**
 * ZoneDelimiterLayer.
 *
 * Created: Wed Apr 17 15:59:09 2002
 *
 * @author <a href="mailto:seb.regnier@free.fr"></a>
 * @version $Revision$
 */
public class ZoneDelimiterLayer extends Layer implements MapDataListener { //ZoneDelimiterLayer

    /** serialVersionUID. */
    private static final long serialVersionUID = 1127201121371123690L;

    /** to use log facility, just put in your code: log.info(\"...\"); */
    private static Log log = LogFactory.getLog(ZoneDelimiterLayer.class);

    protected Projection proj = null;

    /** Contient tous les carres possibles. */
    protected OMGraphicList graphics = new OMGraphicList();

    /** Caracteristiques du cadrillage. */
    protected float pasMailleLatitude;
    protected float pasMailleLongitude;

    protected MapDataProvider mapDataProvider;
    protected Collection<Cell> cells;

    public ZoneDelimiterLayer(MapDataProvider mapDataProvider) throws TopiaException {
        this.mapDataProvider = mapDataProvider;

        this.pasMailleLatitude = mapDataProvider.getCellLengthLatitude();
        this.pasMailleLongitude = mapDataProvider.getCellLengthLongitude();

        cells = mapDataProvider.getCell();
        mapDataProvider.addProviderChangeListener(this);

        this.setVisible(true);
        generateGraphics();
    }

    @Override
    protected void finalize() {
        mapDataProvider.removeProviderChangeListener(this);
    }

    /**
     * Set the projection.
     * @param proj Projection
     */
    public void setProjection(Projection proj) {
        this.proj = proj;
    }

    /**
     * Get the projection.
     * @return Projection proj
     */
    public Projection getProjection() {
        return proj;
    }

    /**
     * Implementing the ProjectionPainter interface.
     */
    public void renderDataForProjection(Projection proj, java.awt.Graphics g) {
        if (proj == null) {
            return;
        } else {
            // generateGraphics();
            setProjection(proj.makeClone());
        }
        paint(g);
    }

    /**
     * Invoked when the projection has changed or this Layer has been
     * added to the MapBean.
     * @param e ProjectionEvent
     */
    public void projectionChanged(ProjectionEvent e) {
        setProjection(e.getProjection());
        repaint();
    }

    /**
     * Create and project the graphics.Creer l ensemble des carres possible et affiche le
     * cadrillage.
     */
    protected void generateGraphics() throws TopiaException {
        graphics.clear();

        OMRect omrect;
        for (Cell cell : cells) {
            omrect = new OMRect(
                    (float) (cell.getLatitude() + pasMailleLatitude),
                    (float) cell.getLongitude(), (float) cell.getLatitude(),
                    (float) (cell.getLongitude() + pasMailleLongitude),
                    OMGraphic.LINETYPE_STRAIGHT);
            if (cell.isLand()) {
                omrect.setLinePaint(new Color(Color.RED.getRed(), Color.RED.getGreen(), Color.RED.getBlue(), 0)); //completement transparent
            } else {
                omrect.setLinePaint(Color.RED);
            }
            omrect.setVisible(true);
            graphics.add(omrect);
        }
        graphics.generate(getProjection());
    }

    protected void refresh() {
        try {
            cells = mapDataProvider.getCell();
            generateGraphics();
            repaint();
        } catch (TopiaException eee) {
            if (log.isWarnEnabled()) {
                log.warn("Can't refresh view", eee);
            }
        }
    }

    /**
    * Paints the layer.
    * @param g the Graphics context for painting
    */
    public void paint(Graphics g) {
        //generateGraphics();
        graphics.generate(getProjection());
        graphics.render(g);
    }

    @Override
    public void regionChanged() {
        // on rafraichi que si on commit au moins un Cell
        refresh();
    }
    
} // ZoneDelimiterLayer
