/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2010 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.map;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import org.nuiton.topia.TopiaException;

import com.bbn.openmap.event.MapMouseListener;
import com.bbn.openmap.event.NavMouseMode;
import com.bbn.openmap.event.NullMouseMode;
import com.bbn.openmap.event.SelectMouseMode;
import com.bbn.openmap.omGraphics.OMGraphic;
import com.bbn.openmap.omGraphics.OMRect;
import com.bbn.openmap.proj.coords.LatLonPoint;

/**
 * Ce layer permet de dessinner un cadrillage avec une latitude/longitude max et min,
 * ainsi qu'un pas de maille. Dans ce cadrillage, il est ensuite possible de selectionner
 * des carres. Cette selection peut se faire avec la souris. Il y a diiferents mode de
 * de selections qui sont {@link #NO_SELECTION}, {@link #SINGLE_SELECTION} et
 * {@link #MULT_SELECTION}.
 *
 * Created: Mon Jan 21 13:42:55 2002
 *
 * @author <a href="mailto:seb.regnier@free.fr"></a>
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class CellSelectionLayer extends ZoneDelimiterLayer implements MapMouseListener {

    /** serialVersionUID */
    private static final long serialVersionUID = 6952137313443302883L;

    /** Current layer selection mode. */
    protected int selectionMode;
    
    /** No selection enabled. */
    public static final int NO_SELECTION = 0;
    /** Only single celle selection enabled. */
    public static final int SINGLE_SELECTION = 1;
    /** Multi selection enabled. */
    public static final int MULT_SELECTION = 2;

    public CellSelectionLayer(MapDataProvider mapDataProvider, int selectionMode)
            throws TopiaException {
        super(mapDataProvider);
        this.selectionMode = selectionMode;
    }

    /**
     * Get the value of selectionMode.
     * @return value of selectionMode.
     */
    public int getSelectionMode() {
        return selectionMode;
    }

    /**
     * Set the value of selectionMode.
     * @param v  Value to assign to selectionMode.
     */
    public void setSelectionMode(int v) {
        this.selectionMode = v;
    }

    /**
     * Permet de retrouver le carre incluant le point donne.
     * @param pt le point pour lequel on cherche le carre correspondant
     * @return le carre correspondant au point donne sinon renvoie null.
     */
    public OMRect findXY(LatLonPoint pt) {
        // Recupere le vecteur de tous les carres possibles.
        List<OMGraphic> allRect = graphics.getTargets();

        // Parcours le vecteur, et cherche le carre, et le renvoie si il existe
        for (OMGraphic rectg : allRect) {
            OMRect rect = (OMRect)rectg;
            if (pt.getLatitude() >= rect.getSouthLat()
                    && pt.getLatitude() < rect.getNorthLat()
                    && pt.getLongitude() >= rect.getWestLon()
                    && pt.getLongitude() < rect.getEastLon()) {
                return rect;
            }
        }
        // Renvoie null si pas trouve
        return null;
    }

    /**
     * Renvoie tous les carres selectionnes.
     * @return le vecteur contenant des LatLonPoint des carres selectionnes.
     */
    public List<LatLonPoint> getSelected() {
        // Recupere le vecteur de tous les carres possibles.
        List<OMGraphic> allRect = graphics.getTargets();

        List<LatLonPoint> result = new ArrayList<>();

        // Parcours le vecteur de tous les carres possibles.
        for (OMGraphic rectg : allRect) {
            OMRect rect = (OMRect)rectg;
            // Si le carre est selectionne, on rajoute ses coordonnees au vector results
            if (rect.getFillPaint() != null && !rect.isClear(rect.getFillPaint())) {
                result.add(new LatLonPoint.Double(rect.getSouthLat(), rect.getWestLon()));
            }
        }
        return result;
    }

    /**
     * Deselectionne tous les carres.
     */
    public void unSelectAll() {
        // Recupere le vecteur de tous les carres possibles.
        List<OMGraphic> allRect = graphics.getTargets();

        // Parcours le vecteur et met a null la couleur de fond de chaque carre
        for (OMGraphic rectg : allRect) {
            OMRect rect = (OMRect)rectg;
            rect.setFillPaint(null);
        }
        // Rafraichit le layer
        repaint();
    }

    /**
     * Permet de selectionner le carre incluant le point donne.
     * @param pt le point pour lequel on cherche le carre correspondant
     * @return true si reussi sinon false.
     */
    public boolean select(LatLonPoint pt) {
        // Recherche le carre correspondant au point donne
        OMRect select = findXY(pt);

        // si pas trouve on sort.
        if (select == null)
            return false;

        // si trouve et pas deja selectionne
        if (select.getFillPaint() == null
                || select.isClear(select.getFillPaint())) {
            // Si on est en mode SINGLE_SELECTION, on deselectionne tous les carres avant.
            if (getSelectionMode() == SINGLE_SELECTION) {
                unSelectAll();
            }
            // on selectionne le carre en lui mettant une couleur de fond
            select.setFillPaint(Color.green);
        }
        // Raffraichit le layer
        repaint();
        return true;
    }

    /**
     * Permet de selectionner le carre incluant le point donne en latitude et longitude.
     * @param latitude la latitude du point
     * @param longitude la longitude du point
     * @return true si reussi sinon false.
     */
    public boolean select(float latitude, float longitude) {
        return select(new LatLonPoint.Float(latitude, longitude));
    }

    /**
     * Permet de deselectionner le carre incluant le point donne.
     * @param pt le point pour lequel on cherche le carre correspondant
     * @return true si reussi sinon false.
     */
    public boolean unSelect(LatLonPoint pt) {
        OMRect select = findXY(pt);
        if (select == null) {
            return false;
        }
        select.setFillPaint(null);
        repaint();
        return true;
    }

    /**
     * Permet de deselectionner le carre incluant le point donne en latitude et longitude.
     * @param latitude la latitude du point
     * @param longitude la longitude du point
     * @return true si reussi sinon false.
     */
    public boolean unSelect(float latitude, float longitude) {
        return unSelect(new LatLonPoint.Float(latitude, longitude));
    }

    /**
     * Return a list of the modes that are interesting to the
     * MapMouseListener.  The source MouseEvents will only get sent to
     * the MapMouseListener if the mode is set to one that the
     * listener is interested in.
     * Layers interested in receiving events should register for
     * receiving events in "select" mode.
     * <pre>
     *  return new String[1] {
     *      SelectMouseMode.modeID
     * };
     * </pre>
     * @see NavMouseMode#modeID
     * @see SelectMouseMode#modeID
     * @see NullMouseMode#modeID
     */
    public String[] getMouseModeServiceList() {
        return new String[] { SelectMouseMode.modeID };
    }

    /**
     * Note: A layer interested in receiving amouse events should
     * implement this function .  Otherwise, return the default, which
     * is null.
     */
    public synchronized MapMouseListener getMapMouseListener() {
        return this;
    }

    /**
     * Invoked when a mouse button has been pressed on a component.
     * @param e MouseEvent
     * @return true if the listener was able to process the event.
     */
    public boolean mousePressed(MouseEvent e) {
        return false;
    }

    /**
     * Invoked when a mouse button has been released on a component.
     * @param e MouseEvent
     * @return true if the listener was able to process the event.
     */
    public boolean mouseReleased(MouseEvent e) {
        return false;
    }

    /**
     * Invoked when the mouse has been clicked on a component.
     * The listener will receive this event if it successfully
     * processed <code>mousePressed()</code>, or if no other listener
     * processes the event.  If the listener successfully processes
     * mouseClicked(), then it will receive the next mouseClicked()
     * notifications that have a click count greater than one.
     * @param e MouseListener MouseEvent to handle.
     * @return true if the listener was able to process the event.
     */
    public boolean mouseClicked(MouseEvent e) {
        // si on est dans le mode NO_SELECTION, on fait rien!;
        if (getSelectionMode() == NO_SELECTION) {
            return false;
        }

        // on recherche le carre clique
        OMRect select = findXY(getProjection().inverse(e.getX(), e.getY()));

        // si on le trouve pas,on renvoie false
        if (select == null) {
            return false;
        }

        // si le carre n est pas selectionne, on le selectionne
        if (select.getFillPaint() == null
                || select.isClear(select.getFillPaint())) {
            // si on est en mode SINGLE_SELECTION, on deselectionne tous avant.
            if (getSelectionMode() == SINGLE_SELECTION) {
                unSelectAll();
            }
            // on selectionne le carre
            select.setFillPaint(Color.green);
        }
        // si le carre est deja selectionne
        else {
            // si on est en mode MULT_SELECTION, on le deselectionne
            if (getSelectionMode() == MULT_SELECTION) {
                select.setFillPaint(null);
            }
        }
        // Raffraichit le layer
        repaint();
        return true;
    }

    /**
     * Invoked when the mouse enters a component.
     * @param e MouseListener MouseEvent to handle.
     */
    public void mouseEntered(MouseEvent e) {
    }

    /**
     * Invoked when the mouse exits a component.
     * @param e MouseListener MouseEvent to handle.
     */
    public void mouseExited(MouseEvent e) {
    }

    /**
     * Invoked when a mouse button is pressed on a component and then
     * dragged.  The listener will receive these events if it
     * successfully processes mousePressed(), or if no other listener
     * processes the event.
     * @param e MouseMotionListener MouseEvent to handle.
     * @return true if the listener was able to process the event.
     */
    public boolean mouseDragged(MouseEvent e) {
        return false;
    }

    /**
     * Invoked when the mouse button has been moved on a component
     * (with no buttons no down).
     * @param e MouseListener MouseEvent to handle.
     * @return true if the listener was able to process the event.
     */
    public boolean mouseMoved(MouseEvent e) {
        return false;
    }

    /**
     * Handle a mouse cursor moving without the button being pressed.
     * This event is intended to tell the listener that there was a
     * mouse movement, but that the event was consumed by another
     * layer.  This will allow a mouse listener to clean up actions
     * that might have happened because of another motion event
     * response.
     */
    public void mouseMoved() {
    }

}// CellSelectionLayer
