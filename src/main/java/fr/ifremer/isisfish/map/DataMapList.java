/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2010 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.map;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.bbn.openmap.omGraphics.OMGraphicList;
import com.bbn.openmap.proj.Projection;

/**
 * DataMapList.java
 *
 * Created: Mon Apr 22 16:27:28 2002
 * 
 * @author <a href="mailto:seb.regnier@free.fr"></a>
 */
public class DataMapList implements Serializable {

    /** serialVersionUID. */
    private static final long serialVersionUID = 7147651962434190318L;

    /** Class logger. */
    private static Log log = LogFactory.getLog(DataMapList.class);
    
    protected Motif motif;

    protected DataMapInfo info;

    protected Vector<DataMap> dataMap = new Vector<>();

    protected Scale scale;

    public DataMapList() {
        this(new Motif(), new DataMapInfo());
    }

    public DataMapList(Motif motif) {
        this(motif, new DataMapInfo());
    }

    public DataMapList(Motif motif, DataMapInfo info) {
        this.motif = motif;
        this.info = info;
    }

    /**
     * Add a datamap to this list
     * 
     * @param data DataMap
     */
    public void addDataMap(DataMap data) {
        dataMap.add(data);
    }

    /**
     * Get all the data map of this list
     * 
     * @return Enumeration
     */
    public Enumeration<DataMap> getDataMaps() {
        return dataMap.elements();
    }

    /**
     *
     * @param c ?
     * @return Get all the data map of this list
     */
    public List<DataMap> getDataMaps(Coordinate c) {
        List<DataMap> v = new ArrayList<>();
        DataMap dm;
        for (Object aDataMap : dataMap) {
            dm = (DataMap) aDataMap;
            if (contains(dm, c)) {
                v.add(dm);
            }
        }
        return v;
    }

    public boolean contains(DataMap dm, Coordinate c) {
        Coordinate[] coord = dm.getCoordinates();
        for (Coordinate aCoord : coord) {
            if (aCoord.y <= c.y && c.y < aCoord.y + motif.getWidth()
                    && aCoord.x <= c.x
                    && c.x < aCoord.x + motif.getHeight()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get the value of motif.
     * 
     * @return Value of motif.
     */
    public Motif getMotif() {
        return motif;
    }

    /**
     * Set the value of motif.
     * 
     * @param v Value to assign to motif.
     */
    public void setMotif(Motif v) {
        this.motif = v;
    }

    /**
     * Get the value of info.
     * 
     * @return Value of info.
     */
    public DataMapInfo getInfo() {
        return info;
    }

    /**
     * Set the value of info.
     * 
     * @param v Value to assign to info.
     */
    public void setInfo(DataMapInfo v) {
        this.info = v;
    }

    /**
     * Get the value of scale.
     * 
     * @return Value of scale.
     */
    public Scale getScale() {
        return scale;
    }

    /**
     * Set the value of scale.
     * 
     * @param v Value to assign to scale.
     */
    public void setScale(Scale v) {
        this.scale = v;
    }

    /**
     * @return Get the max value of the DataMap
     */
    public double getMaxDataMapValue() {
        double max = 0;
        for (Enumeration<DataMap> enu = dataMap.elements(); enu.hasMoreElements();) {
            max = Math.max(max, enu.nextElement().getValue());
        }
        return max;
    }

    /**
     * @return Get the min value of the DataMap
     */
    public double getMinDataMapValue() {
        double min = 0;
        for (Enumeration<DataMap> enu = dataMap.elements(); enu.hasMoreElements();) {
            min = Math.min(min, enu.nextElement().getValue());
        }
        return min;
    }

    /**
     * @return Get the strictly positive min value of the DataMap
     */
    public double getPositiveMinDataMapValue() {
        double min = 0;
        double tmp;
        for (Enumeration<DataMap> enu = dataMap.elements(); enu.hasMoreElements();) {
            tmp = Math.min(min, enu.nextElement().getValue());
            if (tmp > 0) {
                min = tmp;
            }
            else {
                if (log.isErrorEnabled()) {
                    log.error("getPositiveMinDataMapValue:"
                        + " error negative value encountered");
                }
            }
        }
        return min;
    }

    /**
     * @param proj
     * @return Create a graphical object corresponding to this DataMapList according to
     * its motif.
     */
    public OMGraphicList createOMGraphicList(Projection proj) {
        OMGraphicList omlist = new OMGraphicList();
        if (getMaxDataMapValue() > 0) {
            Motif motif = getMotif();
            Scale scale = getScale();
            if (scale == null) {
                scale = new DefaultScale(getMaxDataMapValue());
            }
            DataMap datamap;
            for (Enumeration<DataMap> enu = getDataMaps(); enu.hasMoreElements();) {
                datamap = enu.nextElement();
                motif.createOMDataMap(proj, omlist, scale, datamap);
            }
        }
        return omlist;
    }
} // DataMapList
