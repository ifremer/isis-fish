/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2010 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.map;

import java.awt.geom.Point2D;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * Coordinate extends Float point to add serialisation implementation.
 *
 * Created: Mon Apr 22 16:49:52 2002
 *
 * @author <a href="mailto:seb.regnier@free.fr"></a>
 * @version $Revision$
 */
public class Coordinate extends Point2D.Float implements Serializable {

    /** serialVersionUID. */
    private static final long serialVersionUID = 1L;

    public Coordinate() {
        super();
    }

    public Coordinate(float latitude, float longitude) {
        super(latitude, longitude);
    }

    protected void writeObject(ObjectOutputStream stream)
            throws java.io.IOException {
        stream.writeFloat(x);
        stream.writeFloat(y);
    }

    protected void readObject(ObjectInputStream stream)
            throws java.io.IOException {
        x = stream.readFloat();
        y = stream.readFloat();
    }
} // Coordinate
