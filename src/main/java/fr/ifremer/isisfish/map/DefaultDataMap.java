/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2010 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.map;

/**
 * DefaultDataMap.java
 * 
 * A default DataMap implementation. Created: Mon Apr 22 16:39:08 2002
 * 
 * @author <a href="mailto:seb.regnier@free.fr"></a>
 * @version $Revision$
 */
public class DefaultDataMap implements DataMap {

    /** serialVersionUID. */
    private static final long serialVersionUID = 3277576798583867731L;

    protected double value;

    protected DataMapInfo info;

    protected Coordinate[] coordinates;

    /**
     * Construct the DefaultDataMap.
     */
    public DefaultDataMap() {
    }

    /**
     * Construct the DefaultDataMap.
     * 
     * @param coordinates Coordinates of the DataMap cells
     * @param value Value
     * @param info DataMapInfo
     */
    public DefaultDataMap(Coordinate[] coordinates, double value,
            DataMapInfo info) {
        this.coordinates = coordinates;
        this.value = value;
        this.info = info;
    }

    public DefaultDataMap(Coordinate[] coordinates, double value) {
        this(coordinates, value, null);
    }

    public DefaultDataMap(double value) {
        this.value = value;
    }

    public DefaultDataMap(String latitudes, String longitudes, double value) {
        this(value);
        setCoordinates(latitudes, longitudes);
    }

    public DefaultDataMap(float[] latitudes, float[] longitudes, double value) {
        this(value);
        setCoordinates(latitudes, longitudes);
    }

    /**
     * Set the coordinates of the Cells position of the DataMap on the map.
     * 
     * @param latitudes Latitudes
     * @param longitudes Longitudes
     */
    public void setCoordinates(String latitudes, String longitudes) {
        String[] lat = latitudes.trim().split(" ");
        String[] lon = longitudes.trim().split(" ");
        coordinates = new Coordinate[lat.length];
        for (int i = 0; i < lat.length; i++) {
            coordinates[i] = new Coordinate(Float.parseFloat(lat[i]), Float
                    .parseFloat(lon[i]));
        }
    }

    /**
     * Set the coordinates of the Cells position of the DataMap on the map.
     * 
     * @param latitudes Latitudes
     * @param longitudes Longitudes
     */
    public void setCoordinates(float[] latitudes, float[] longitudes) {
        coordinates = new Coordinate[latitudes.length];
        for (int i = 0; i < latitudes.length; i++) {
            coordinates[i] = new Coordinate(latitudes[i], longitudes[i]);
        }
    }

    /**
     * Set the coordinates of the Cells position of the DataMap on the map.
     * 
     * @param coordinates Coordinates
     */
    public void setCoordinates(Coordinate[] coordinates) {
        this.coordinates = coordinates;
    }

    /**
     * Get the coordinates of the Cells position of the DataMap on the map.
     * 
     * @return All the coordinates of the underlying cells of this DataMap.
     */
    public Coordinate[] getCoordinates() {
        return coordinates;
    }

    /**
     * Get the value of value.
     * 
     * @return Value of value.
     */
    public double getValue() {
        return value;
    }

    /**
     * Set the value of value.
     * 
     * @param v Value to assign to value.
     */
    public void setValue(double v) {
        this.value = v;
    }

    /**
     * Get the value of info.
     * 
     * @return Value of info.
     */
    public DataMapInfo getInfo() {
        return info;
    }

    /**
     * Set the value of info.
     * 
     * @param v Value to assign to info.
     */
    public void setInfo(DataMapInfo v) {
        this.info = v;
    }

    public String toString() {
        String str = "[";
        for (Coordinate coordinate : coordinates) {
            str += " " + coordinate;
        }
        str += " value:" + value + "]";
        return str;
    }

}// DefaultDataMap
