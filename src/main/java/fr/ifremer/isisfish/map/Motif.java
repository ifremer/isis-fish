/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2010 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.map;

import java.awt.Color;
import java.io.Serializable;

import com.bbn.openmap.omGraphics.OMGraphic;
import com.bbn.openmap.omGraphics.OMGraphicList;
import com.bbn.openmap.omGraphics.OMRect;
import com.bbn.openmap.proj.Projection;

/**
 * A Motif that represents how to draw a DataMap on the map. A factor
 * is applied to the graphic according to the DataMap value and the maximum
 * DataMap value.
 * 
 * Created: Mon Apr 22 16:31:39 2002
 * 
 * @author <a href="mailto:seb.regnier@free.fr"></a>
 * @version $Revision$
 */
public class Motif implements Serializable {

    /** serialVersionUID. */
    private static final long serialVersionUID = -2814583859271654759L;

    public static final Color defaultLineColor = new Color(0, true);

    public static final Color defaultFillcolor = new Color(0x00FF00);

    public static final float defaultAlpha = 0.5f;

    /** The default line color. (black) */
    public final static String defaultLineColorString = "00000000"; // none

    /** The default fill color. (none) */
    public final static String defaultFillColorString = "00ff00"; // green

    protected Color lineColor;

    protected Color fillColor;

    protected float alpha = defaultAlpha;

    protected float width = 1;

    protected float height = 0.5f;

    public Motif(Color fill, Color line, float alpha) {
        this.fillColor = fill;
        this.lineColor = line;
        this.alpha = alpha;
    }

    public Motif(Color fill, float alpha) {
        this(fill, defaultLineColor, alpha);
    }

    public Motif(Color fill) {
        this(fill, defaultLineColor, defaultAlpha);
    }

    public Motif() {
        this(defaultFillcolor, defaultLineColor, defaultAlpha);
    }

    /**
     * Get the value of lineColor.
     * 
     * @return Value of lineColor.
     */
    public Color getLineColor() {
        return lineColor;
    }

    /**
     * Set the value of lineColor.
     * 
     * @param v Value to assign to lineColor.
     */
    public void setLineColor(Color v) {
        this.lineColor = v;
    }

    /**
     * Get the value of fillColor.
     * 
     * @return Value of fillColor.
     */
    public Color getFillColor() {
        return fillColor;
    }

    /**
     * Set the value of fillColor.
     * 
     * @param v Value to assign to fillColor.
     */
    public void setFillColor(Color v) {
        this.fillColor = v;
    }

    /**
     * Get the value of alpha.
     * 
     * @return Value of alpha.
     */
    public float getAlpha() {
        return alpha;
    }

    /**
     * Set the value of alpha.
     * 
     * @param v Value to assign to alpha.
     */
    public void setAlpha(float v) {
        this.alpha = v;
    }

    /**
     * Get the value of width.
     * 
     * @return Value of width.
     */
    public float getWidth() {
        return width;
    }

    /**
     * Set the value of width.
     * 
     * @param v Value to assign to width.
     */
    public void setWidth(float v) {
        this.width = v;
    }

    /**
     * Get the value of height.
     * 
     * @return Value of height.
     */
    public float getHeight() {
        return height;
    }

    /**
     * Set the value of height.
     * 
     * @param v Value to assign to height.
     */
    public void setHeight(float v) {
        this.height = v;
    }

    /**
     * Set the width and the height of a cell for this Motif May be more
     * accurate in the DataMap ???
     * 
     * @param width Cell width in radian
     * @param height Cell height in radian
     */
    public void setCellDimension(float width, float height) {
        this.width = width;
        this.height = height;
    }

    /**
     * Create a graphical object corresponding to this DataMap.
     */
    protected void createOMDataMap(Projection proj, OMGraphicList omlist,
            Scale sc, DataMap datamap) {
        if (datamap.getValue() != 0) {
            int rgb = sc.getValueAsRGB(datamap.getValue());
            
            // see http://forge.codelutin.com/issues/5646
            // cela peut se produire en cas de résultat incohérents
            if (rgb < 0) {
                rgb = 0;
            }

            Color lColor = new Color(255 - rgb, 255 - rgb, 255 - rgb, 255);
            Color fColor = lColor;

            OMRect omrect;
            Coordinate[] coordinates = datamap.getCoordinates();
            for (Coordinate coordinate : coordinates) {
                // create OMRect for each coordinates
                omrect = new OMRect(coordinate.x + getHeight(), coordinate.y, coordinate.x,
                        coordinate.y + getWidth(), OMGraphic.LINETYPE_RHUMB);
                omrect.setLinePaint(lColor);
                omrect.setFillPaint(fColor);
                omlist.add(omrect);
            }
        }
    }
} // Motif
