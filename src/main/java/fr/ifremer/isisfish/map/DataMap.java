/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2010 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.map;

import java.io.Serializable;

/**
 * DataMap.
 *
 * A DataMap is some data we have to put on the map.
 * To do this, we use a motif that tells the layer how to render the data.
 *
 * Created: Mon Apr 22 16:45:38 2002
 *
 * @author <a href="mailto:seb.regnier@free.fr"></a>
 * @version $Revision$
 */
public interface DataMap extends Serializable {

    /**
     * Get the coordinates of the Cells position of the DataMap on the map.
     * @return All the coordinates of the underlying cells of this DataMap.
     */
    Coordinate[] getCoordinates();

    /**
     * Get the value of value.
     * @return Value of value.
     */
    double getValue();

    /**
     * Get the value of info.
     * @return Value of info.
     */
    DataMapInfo getInfo();

} // DataMap
