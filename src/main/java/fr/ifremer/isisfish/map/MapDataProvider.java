package fr.ifremer.isisfish.map;

/*
 * #%L
 * ISIS-Fish
 * %%
 * Copyright (C) 2017 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.isisfish.entities.Cell;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public abstract class MapDataProvider {

    protected Set<MapDataListener> mapDataListeners = new HashSet<>();

    public abstract float getMinLongitude();

    public abstract float getMaxLongitude();

    public abstract float getMinLatitude();

    public abstract float getMaxLatitude();

    public abstract float getCellLengthLongitude();

    public abstract float getCellLengthLatitude();

    public abstract List<String> getMapFilePath();

    public abstract List<Cell> findAllByCoordinates(float latitude, float longitude);

    public abstract List<Cell> getCell();

    void addProviderChangeListener(MapDataListener listener) {
        mapDataListeners.add(listener);
    }

    void removeProviderChangeListener(MapDataListener listener) {
        mapDataListeners.remove(listener);
    }

    public void regionChanged() {
        List<MapDataListener> listeners = new ArrayList<>(mapDataListeners);
        for (MapDataListener listener : listeners) {
            listener.regionChanged();
        }
    }
}
