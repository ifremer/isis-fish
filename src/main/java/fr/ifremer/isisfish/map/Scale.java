/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2010 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.map;

import java.io.Serializable;

/**
 * This interface allows the user to define a graduation between 0.0 and 1.0
 * whose movement are proportionnal to a bounded value.
 *
 * Created: Mon Apr 22 16:36:05 2002
 *
 * @author <a href="mailto:seb.regnier@free.fr"></a>
 * @version $Revision$
 */
public interface Scale extends Serializable{

    /**
     * Get the corresponding value between 0.0 and 1.0.
     * 
     * @param value value to get scale
     * @return scale value
     */
    double getValueAsScale(double value);

    int getValueAsRGB(double value);

} // Scale
