/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2017 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.map;

import static org.nuiton.i18n.I18n.t;

import java.awt.Color;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;

import com.bbn.openmap.Layer;
import com.bbn.openmap.LayerHandler;
import com.bbn.openmap.MapBean;
import com.bbn.openmap.MouseDelegator;
import com.bbn.openmap.PropertyHandler;
import com.bbn.openmap.event.CoordMouseMode;
import com.bbn.openmap.event.MapMouseListener;
import com.bbn.openmap.event.MapMouseMode;
import com.bbn.openmap.event.NavMouseMode;
import com.bbn.openmap.gui.OverlayMapPanel;
import com.bbn.openmap.layer.GraticuleLayer;
import com.bbn.openmap.layer.dted.DTEDLayer;
import com.bbn.openmap.layer.e00.E00Layer;
import com.bbn.openmap.layer.mif.MIFLayer;
import com.bbn.openmap.layer.rpf.RpfLayer;
import com.bbn.openmap.layer.shape.ShapeLayer;
import com.bbn.openmap.layer.vpf.VPFLayer;
import com.bbn.openmap.omGraphics.DrawingAttributes;
import com.bbn.openmap.proj.coords.LatLonPoint;

import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.entities.Cell;
import fr.ifremer.isisfish.entities.FisheryRegion;

/**
 * IsisMapBean.
 */
public class IsisMapBean extends OverlayMapPanel implements MapDataListener {

    /** serialVersionUID. */
    private static final long serialVersionUID = -4162103384132928473L;

    /** to use log facility, just put in your code: log.info("..."); */
    private static Log log = LogFactory.getLog(IsisMapBean.class);

    protected MapDataProvider mapDataProvider;
    protected float pasMailleLatitude;
    protected float pasMailleLongitude;
    protected float minLatitude;
    protected float maxLatitude;
    protected float minLongitude;
    protected float maxLongitude;

    protected List<MapMouseListener> orderedListener = new ArrayList<>();
    protected LayerHandler layerHandler = new LayerHandler();
    protected MapMouseMode currentMouseMode;
    protected MouseDelegator md;

    protected CellSelectionLayer activeSelectionLayer = null;
    protected int selectionMode = CellSelectionLayer.MULT_SELECTION;

    protected static final float SCALE = 9500000f;

    public IsisMapBean() {
        super(new PropertyHandler(new Properties()), true);
        create();
        init();
    }

    public void init() {
        MapBean mapBean = getMapBean();
        mapBean.setScale(SCALE);
        // Let the LayerHandler know who is interested in Layers.
        //MapBean is just one component
        //layerHandler.addLayerListener(this);

        md = new MouseDelegator(mapBean);
        // md.addMouseMode(selectMouseMode);
        // Tell the delegator to use the default modes: Navigation
        // and Selection
        md.setDefaultMouseModes();
        addMapComponent(md);
        addMapComponent(layerHandler);
        
        getMapBean().setBackgroundColor(new Color(0x99b3cc));
        
        NavMouseMode mouseMode = new NavMouseMode();
        setActiveMouseMode(mouseMode);
        
        // add copy to clipboard support
        addMapMouseListener(new CopyMapToClipboardListener(mapBean));
    }

    public void setActiveMouseMode(CoordMouseMode mode) {
        md.setActiveMouseMode(mode);
        currentMouseMode = mode;
        // il faut remettre les listeners
        for (MapMouseListener l : orderedListener) {
            currentMouseMode.addMapMouseListener(l);
        }
    }

    public void addMapMouseListener(MapMouseListener mml) {
        if (mml instanceof CellSelectionLayer || mml instanceof ResultatLayer) {
            // faut que certain listener soit toujours en premier
            orderedListener.add(0, mml);
            currentMouseMode.removeAllMapMouseListeners();
            for (MapMouseListener l : orderedListener) {
                currentMouseMode.addMapMouseListener(l);
            }
        } else {
            orderedListener.add(mml);
            currentMouseMode.addMapMouseListener(mml);
        }
    }

    public void removeMapMouseListener(MapMouseListener mml) {
        currentMouseMode.removeMapMouseListener(mml);
    }

    /**
     * @deprecated since 4.4.1 use now {@link MapDataProvider}
     */
    @Deprecated
    public void setFisheryRegion(FisheryRegion fisheryRegion) {
        setMapDataProvider(fisheryRegion != null ? new DatabaseDataProvider(this, fisheryRegion) : null);
    }

    public void setMapDataProvider(MapDataProvider mapDataProvider) {
        if (log.isDebugEnabled()) {
            log.debug("Current provider is now: " + mapDataProvider + " old was: " + this.mapDataProvider);
        }

        try {
            MapDataProvider oldDataProvider = this.mapDataProvider;
            this.mapDataProvider = mapDataProvider;
            if (mapDataProvider == null) {
                layerHandler.removeAll();
                if (oldDataProvider != null) {
                    oldDataProvider.removeProviderChangeListener(this);
                }
            } else {
                if (!mapDataProvider.equals(oldDataProvider)) {
                    mapDataProvider.addProviderChangeListener(this);
                    refreshMap();
                }
            }
        } catch (Exception eee) {
            if (log.isErrorEnabled()) {
                log.error("Impossible d'initialiser la Region", eee);
            }
        }
    }

    @Override
    public void regionChanged() {
        refreshMap();
    }

    protected void refreshMap() {
        pasMailleLatitude = mapDataProvider.getCellLengthLatitude();
        pasMailleLongitude = mapDataProvider.getCellLengthLongitude();
        minLatitude = mapDataProvider.getMinLatitude();
        minLongitude = mapDataProvider.getMinLongitude();
        maxLatitude = mapDataProvider.getMaxLatitude();
        maxLongitude = mapDataProvider.getMaxLongitude();
        initMap();
    }

    public MapDataProvider getMapDataProvider() {
        return mapDataProvider;
    }

    /**
     * Retourne la liste des mailles selectionnées.
     * 
     * @return selected map list
     */
    public List<Cell> getSelectedCells() {
        try {
            List<Cell> result = new ArrayList<>();
            List<LatLonPoint> pts = activeSelectionLayer.getSelected();
            for (LatLonPoint llp : pts) {
                List<Cell> cells = null;
                try {
                    cells = mapDataProvider.findAllByCoordinates(llp.getLatitude(), llp.getLongitude());
                } catch (TopiaException eee) {
                    log.warn("Can't find cell for this point: " + llp, eee);
                }
                if (cells != null && cells.size() > 0) {
                    result.add(cells.get(0));
                }
            }
            return result;
        } catch (RuntimeException eee) {
            if (log.isErrorEnabled()) {
                log.error("Can't find selected cells", eee);
            }
            throw eee;
        }
    }

    public void setSelectedCells(List<Cell> cells) {
        if (cells != null) {
            setSelectedCells(cells.toArray(new Cell[0]));
        } else {
            setSelectedCells();
        }
    }

    public void setSelectedCells(Cell... cells) {
        if (log.isDebugEnabled()) {
            log.debug("Select cells: " + Arrays.toString(cells));
        }
        activeSelectionLayer.unSelectAll();
        addSelectedCells(cells);
    }

    public void addSelectedCells(Cell... cells) {
        if (log.isDebugEnabled()) {
            log.debug("add select cells: " + Arrays.toString(cells));
        }
        for (Cell cell : cells) {
            if (cell != null) {
                activeSelectionLayer.select((cell.getLatitude()
                        + cell.getLatitude() + pasMailleLatitude) / 2f, (cell
                        .getLongitude()
                        + cell.getLongitude() + pasMailleLongitude) / 2f);
            }
        }
    }

    public void removeSelectedCells(Cell... cells) {
        if (log.isDebugEnabled()) {
            log.debug("remove select cells: " + Arrays.toString(cells));
        }
        for (Cell cell : cells) {
            activeSelectionLayer.unSelect((cell.getLatitude()
                    + cell.getLatitude() + pasMailleLatitude) / 2f, (cell
                    .getLongitude()
                    + cell.getLongitude() + pasMailleLongitude) / 2f);
        }
    }

    /**
     * Get the value of layerHandler.
     * @return value of layerHandler.
     */
    public LayerHandler getLayerHandler() {
        return layerHandler;
    }

    public void addResultatLayer(String id, ResultatLayer layer) {
        layer.setName(id);
        addMapMouseListener(layer);
        addMapComponent(layer);
    }

    public void removeAllResultatLayer() {
        Layer[] layers = layerHandler.getLayers();
        for (Layer layer : layers) {
            if (layer instanceof ResultatLayer) {
                removeMapMouseListener((MapMouseListener) layer);
                layerHandler.removeLayer(layer);
            }
        }
    }

    /**
     * Add a new layer to the map depending on mapFile extension.
     * 
     * @param layerId layer id
     * @param mapFile absolute map fail path
     * @param lineColor line color
     * @param fillColor fill color
     */
    protected void addLayer(String layerId, String mapFile, String lineColor,
            String fillColor) {

        Layer layer = null;
        // get layer depending on type
        switch (FilenameUtils.getExtension(mapFile)) {
        case "shp":
            layer = getShapeLayer(layerId, mapFile, lineColor, fillColor);
            break;
        case "e00":
            layer = getE00Layer(layerId, mapFile, lineColor, fillColor);
            break;
        case "mif":
            layer = getMIFLayer(layerId, mapFile, lineColor, fillColor);
            break;
        case "rpf":
        case "cadrg":
        case "cib":
            layer = getRPFLayer(layerId, mapFile, lineColor, fillColor);
            break;
        case "vmap":
        case "dcw":
        case "vpf":
            layer = getVPFLayer(layerId, mapFile, lineColor, fillColor);
            break;
        default:
            if (log.isErrorEnabled()) {
                log.error("Can't find layer for " + mapFile + " (unknown type)");
            }
        }

        // display layer
        if (layer != null) {
            if (log.isDebugEnabled()) {
                log.debug("Add layer " + layer);
            }
            addMapComponent(layer);
        }
    }

    protected void addDrawingAttributes(Properties p, String layerId, String lineColor, String fillColor) {
        if (lineColor != null) {
            p.setProperty(layerId + "." + DrawingAttributes.linePaintProperty, lineColor);
        }
        if (fillColor != null) {
            p.setProperty(layerId + "." + DrawingAttributes.fillPaintProperty, fillColor);
        }
    }

    /**
     * Manage shp layer display.
     * 
     * @param layerId layer id
     * @param mapFile absolute map fail path
     * @param lineColor line color
     * @param fillColor fill color
     * 
     * @return shape layer
     * @see ShapeLayer
     */
    protected Layer getShapeLayer(String layerId, String mapFile, String lineColor,
            String fillColor) {

        if (log.isDebugEnabled()) {
            log.debug("Get ShapeLayer with filename : " + mapFile);
        }

        ShapeLayer shapeLayer = new ShapeLayer();
        Properties p = new Properties();
        p.setProperty(layerId + "." + ShapeLayer.shapeFileProperty, mapFile);
        addDrawingAttributes(p, layerId, lineColor, fillColor);
        shapeLayer.setProperties(layerId, p);
        shapeLayer.setName(layerId);
        
        return shapeLayer;
    }

    /**
     * Manage E00 layer display.
     * 
     * @param layerId layer id
     * @param mapFile absolute map fail path
     * @param lineColor line color
     * @param fillColor fill color
     * 
     * @return e00 layer
     * @see E00Layer
     */
    protected Layer getE00Layer(String layerId, String mapFile, String lineColor,
            String fillColor) {
        if (log.isDebugEnabled()) {
            log.debug("Get E00Layer with filename : " + mapFile);
        }
        E00Layer e00Layer = new E00Layer();
        Properties p = new Properties();
        p.setProperty(layerId + ".FileName", mapFile);
        addDrawingAttributes(p, layerId, lineColor, fillColor);
        e00Layer.setProperties(layerId, p);
        e00Layer.setName(layerId);
        
        return e00Layer;
    }

    /**
     * Manage DTED layer display.
     * 
     * @param layerId layer id
     * @param mapFile absolute map fail path
     * @param lineColor line color
     * @param fillColor fill color
     * 
     * @return dted layer
     * @see DTEDLayer
     */
    protected Layer getDTEDLayer(String layerId, String mapFile, String lineColor,
            String fillColor) {
        if (log.isDebugEnabled()) {
            log.debug("Get DTEDLayer with filename : " + mapFile);
        }

        // get current map parent file
        File mapFileFile = new File(mapFile);
        String parentFolder = mapFileFile.getParent();

        DTEDLayer dtedLayer = new DTEDLayer();
        Properties p = new Properties();
        p.setProperty(layerId + ".dted.paths", parentFolder);
        addDrawingAttributes(p, layerId, lineColor, fillColor);
        
        dtedLayer.setProperties(layerId, p);
        dtedLayer.setName(layerId);
        
        return dtedLayer;
    }
    
    /**
     * Manage MIF layer display.
     * 
     * @param layerId layer id
     * @param mapFile absolute map fail path
     * @param lineColor line color
     * @param fillColor fill color
     * 
     * @return mif layer
     * @see MIFLayer
     */
    protected Layer getMIFLayer(String layerId, String mapFile, String lineColor,
            String fillColor) {
        
        if (log.isDebugEnabled()) {
            log.debug("Get MIFLayer with filename : " + mapFile);
        }

        MIFLayer mifLayer = new MIFLayer();
        Properties p = new Properties();
        p.setProperty(layerId + "." + MIFLayer.MIF_FileProperty, mapFile);
        p.setProperty(layerId + "." + MIFLayer.pointVisibleProperty, "true");
        addDrawingAttributes(p, layerId, lineColor, fillColor);
        
        mifLayer.setProperties(layerId, p);
        mifLayer.setName(layerId);
        
        return mifLayer;
        
    }
    
    /**
     * Manage RPF layer display.
     * 
     * Seams to handle cadrg and cib files too.
     * 
     * @param layerId layer id
     * @param mapFile absolute map fail path
     * @param lineColor line color
     * @param fillColor fill color
     * 
     * @return rpf layer
     * @see RpfLayer
     */
    protected Layer getRPFLayer(String layerId, String mapFile, String lineColor,
            String fillColor) {
        
        if (log.isDebugEnabled()) {
            log.debug("Get RpfLayer with filename : " + mapFile);
        }

        // get current map parent file
        File mapFileFile = new File(mapFile);
        String parentFolder = mapFileFile.getParent();
        
        RpfLayer rpfLayer = new RpfLayer();
        Properties p = new Properties();
        // This property should reflect the paths to the RPF directories
        p.setProperty(layerId + ".rpf.paths", parentFolder);
        p.setProperty(layerId + "." + MIFLayer.pointVisibleProperty, "true");
        addDrawingAttributes(p, layerId, lineColor, fillColor);
        
        rpfLayer.setProperties(layerId, p);
        rpfLayer.setName(layerId);
        
        return rpfLayer;
        
    }
    
    /**
     * Manage VPF layer display.
     * 
     * Can manage .vmap, .dcw, .vpf files ?
     * 
     * @param layerId layer id
     * @param mapFile absolute map fail path
     * @param lineColor line color
     * @param fillColor fill color
     * 
     * @return vpf layer
     * @see VPFLayer
     */
    protected Layer getVPFLayer(String layerId, String mapFile, String lineColor,
            String fillColor) {
        
        // on presume que, dans le cas d'IsisFish, les cartes VMap ne
        // sont pas politiques.

        if (log.isDebugEnabled()) {
            log.debug("Get vpfLayer with filename : " + mapFile);
        }

        VPFLayer vpfLayer = new VPFLayer();
        Properties p = new Properties();
        p.setProperty(layerId + "." + VPFLayer.pathProperty, mapFile);
        p.setProperty(layerId + "." + VPFLayer.defaultLayerProperty, "vmapCoastline");
        addDrawingAttributes(p, layerId, lineColor, fillColor);
        
        vpfLayer.setProperties(layerId, p);
        vpfLayer.setName(layerId);
        
        return vpfLayer;
        
    }

    protected void addGraticuleLayer() {
        GraticuleLayer layer = new GraticuleLayer();
        Properties p = new Properties();
        // Show lat / lon spacing labels
        p.setProperty("." + GraticuleLayer.ShowRulerProperty, "true");
        p.setProperty("." + GraticuleLayer.ShowOneAndFiveProperty, "true");
        // Controls when the five degree lines and one degree lines kick in
        // - when there is less than the threshold of ten degree lat or lon
        // lines, five degree lines are drawn.  The same relationship is there
        // for one to five degree lines.
        p.setProperty("." + GraticuleLayer.ThresholdProperty, "5");
        // the color of 10 degree spacing lines (Hex ARGB)
        p.setProperty("." + GraticuleLayer.TenDegreeColorProperty, "FF000000");
        // the color of 5 degree spacing lines (Hex ARGB)
        p.setProperty("." + GraticuleLayer.FiveDegreeColorProperty, "C7009900");
        // the color of 1 degree spacing lines (ARGB)
        p.setProperty("." + GraticuleLayer.OneDegreeColorProperty, "FF003300");
        // the color of the equator (ARGB)
        p.setProperty("." + GraticuleLayer.EquatorColorProperty, "FFFF0000");
        // the color of the international dateline (ARGB)
        p.setProperty("." + GraticuleLayer.DateLineColorProperty, "FF000099");
        // the color of the special lines (ARGB) (Tropic of Cancer, Capricorn)
        p.setProperty("." + GraticuleLayer.SpecialLineColorProperty, "FF000000");
        // the color of the labels (ARGB)
        p.setProperty("." + GraticuleLayer.TextColorProperty, "FF000000");

        layer.setProperties("", p);
        addMapComponent(layer);
    }

    protected void addZoneDelimiterLayer() throws TopiaException {
        ZoneDelimiterLayer layer = new ZoneDelimiterLayer(mapDataProvider);
        addMapComponent(layer);
    }

    protected void addSpecificLayer() {
        try {
            addSelectionLayer();
            addZoneDelimiterLayer();
        } catch (TopiaException eee) {
            if (log.isWarnEnabled()) {
                log.warn("Can't add specific layer", eee);
            }
        }
    }

    /**
     * init the mServer property value.
     */
    protected void initMap() {
        try {
            float centerLat = (maxLatitude + minLatitude) / 2f;
            float centerLong = (maxLongitude + minLongitude) / 2f;
            layerHandler.removeAll();
            //setProjection( new Mercator(new LatLonPoint(centerLat, centerLong), SCALE, 480, 540));

            //FIXME requiered for DTED datas
            //setProjection(new LLXY(new LatLonPoint(centerLat, centerLong), SCALE, 480, 540));
            // OR this one
            //setProjection(new CADRG(new LatLonPoint(centerLat, centerLong), SCALE, 480, 540));

            getMapBean().setCenter(new LatLonPoint.Float(centerLat, centerLong));
            //setScale(SCALE);
            addGraticuleLayer();

            String lineColor = "ff000000";
            String fillColor = "ffbdde83";

            // ajout des shapes
            boolean shapeLoaded = false;
            for (String filename : mapDataProvider.getMapFilePath()) {
                if (!StringUtils.isEmpty(filename)) {
                    if (log.isDebugEnabled()) {
                        log.debug(t("isisfish.message.load.map", filename));
                    }
                    addLayer(filename, filename, lineColor, null);
                    shapeLoaded = true;
                }
            }

            if (!shapeLoaded) {
                if (log.isDebugEnabled()) {
                    log.debug("Can't load custom map, load default one");
                }
                // createNewCells pas reussi createNewCells charger les fichiers demandés, on charge la
                // carte du monde
                String filename = IsisFish.config.getDefaultMapFilename();
                addLayer(filename, filename, lineColor, fillColor);
            }
            
            addSpecificLayer();

        } catch (Exception eee) {
            if (log.isErrorEnabled()) {
                log.error(t("isisfish.error.init.map"), eee);
            }
        }
    }

    /**
     * Get the value of selectionMode.
     * @return value of selectionMode.
     */
    public int getSelectionMode() {
        return selectionMode;
    }

    /**
     * Set the value of selectionMode.
     * @param v value to assign to selectionMode.
     */
    public void setSelectionMode(int v) {
        this.selectionMode = v;
        
        if (activeSelectionLayer != null) {
            activeSelectionLayer.setSelectionMode(v);
        }
    }

    public void addSelectionLayer() {
        try {
            activeSelectionLayer = new CellSelectionLayer(mapDataProvider, getSelectionMode());
            addMapMouseListener(activeSelectionLayer);
            addMapComponent(activeSelectionLayer);
        } catch (TopiaException eee) {
            if (log.isWarnEnabled()) {
                log.warn("Can't add selection layer", eee);
            }
        }
    }

} // IsisMapBean
