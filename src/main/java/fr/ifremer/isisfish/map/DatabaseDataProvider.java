package fr.ifremer.isisfish.map;

/*
 * #%L
 * ISIS-Fish
 * %%
 * Copyright (C) 1999 - 2017 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.isisfish.IsisFishDAOHelper;
import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.entities.Cell;
import fr.ifremer.isisfish.entities.CellDAO;
import fr.ifremer.isisfish.entities.FisheryRegion;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.event.TopiaTransactionEvent;
import org.nuiton.topia.event.TopiaTransactionListener;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class DatabaseDataProvider extends MapDataProvider implements TopiaTransactionListener {

    protected IsisMapBean isisMapBean;
    protected FisheryRegion fisheryRegion;

    public DatabaseDataProvider(IsisMapBean isisMapBean, FisheryRegion fisheryRegion) {
        this.isisMapBean = isisMapBean;
        this.fisheryRegion = fisheryRegion;
        fisheryRegion.getTopiaContext().addTopiaTransactionListener(this);
    }

    @Override
    public float getMinLongitude() {
        return fisheryRegion.getMinLongitude();
    }

    @Override
    public float getMaxLongitude() {
        return fisheryRegion.getMaxLongitude();
    }

    @Override
    public float getMinLatitude() {
        return fisheryRegion.getMinLatitude();
    }

    @Override
    public float getMaxLatitude() {
        return fisheryRegion.getMaxLatitude();
    }

    @Override
    public float getCellLengthLongitude() {
        return fisheryRegion.getCellLengthLongitude();
    }

    @Override
    public float getCellLengthLatitude() {
        return fisheryRegion.getCellLengthLatitude();
    }

    @Override
    public List<Cell> getCell() {
        return fisheryRegion.getCell();
    }

    @Override
    public List<String> getMapFilePath() {
        return fisheryRegion.getMapFilePath();
    }

    @Override
    public List<Cell> findAllByCoordinates(float latitude, float longitude) {
        return getCellDAO().findAllByProperties("latitude",
                latitude, "longitude", longitude);
    }

    protected TopiaContext getTopiaContext() {
        TopiaContext result = fisheryRegion.getTopiaContext();
        if (result == null) {
            throw new IsisFishRuntimeException(
                    "Can't get topiaContext from FisheryRegion");
        }
        return result;
    }

    protected CellDAO getCellDAO() throws TopiaException {
        return IsisFishDAOHelper.getCellDAO(getTopiaContext());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DatabaseDataProvider that = (DatabaseDataProvider) o;
        return Objects.equals(isisMapBean, that.isisMapBean) &&
                Objects.equals(fisheryRegion, that.fisheryRegion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(isisMapBean, fisheryRegion);
    }

    public void commit(TopiaTransactionEvent event) {
        for (TopiaEntity e : event.getEntities()) {
            if (event.isModification(e) && Cell.class.isAssignableFrom(e.getClass())
                    || FisheryRegion.class.isAssignableFrom(e.getClass())) {
                // on rafraichi que si on commit au moins un Cell
                fireEvent();
                break;
            }
        }
    }

    private void fireEvent() {
        List<MapDataListener> listeners = new ArrayList<>(mapDataListeners);
        for (MapDataListener listener : listeners) {
            listener.regionChanged();
        }
    }


    @Override
    public void rollback(TopiaTransactionEvent event) {
        // nothing to do
    }
}
