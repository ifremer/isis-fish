/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2022 - 2024 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.map;

import fr.ifremer.isisfish.entities.Cell;
import fr.ifremer.isisfish.entities.FisheryRegion;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.geotools.api.data.DataStore;
import org.geotools.api.data.DataStoreFinder;
import org.geotools.api.data.FeatureSource;
import org.geotools.api.data.SimpleFeatureSource;
import org.geotools.api.data.SimpleFeatureStore;
import org.geotools.api.data.Transaction;
import org.geotools.api.feature.simple.SimpleFeature;
import org.geotools.api.feature.simple.SimpleFeatureType;
import org.geotools.api.filter.Filter;
import org.geotools.data.DataUtilities;
import org.geotools.data.DefaultTransaction;
import org.geotools.data.collection.ListFeatureCollection;
import org.geotools.data.shapefile.ShapefileDataStore;
import org.geotools.data.shapefile.ShapefileDataStoreFactory;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.FeatureIterator;
import org.geotools.feature.SchemaException;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Polygon;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/**
 * Utility class related to geotools library.
 */
public class GeoTools {

    /** to use log facility, just put in your code: log.info("..."); */
    private static Log log = LogFactory.getLog(GeoTools.class);

    /**
     * Récupère la liste des cellules d'une région qui ont une intersection de 95% avec la shape du shapefile.
     */
    public static Collection<Cell> getCellFromShapefile(FisheryRegion fisheryRegion, Collection<Cell> cells, File mapFile) {
        return getCellFromShapefile(fisheryRegion, cells, mapFile, 0.95);
    }

    /**
     * Récupère la liste des cellules d'une région qui ont une intersection d'une valeur donnée avec la shape du shapefile.
     */
    public static Collection<Cell> getCellFromShapefile(FisheryRegion fisheryRegion, Collection<Cell> cells, File mapFile, double intersectionPourcentage) {
        Collection<Cell> includedCells = new HashSet<>();

        DataStore dataStore = null;
        try {
            Map<String, Object> map = new HashMap<>();
            map.put("url", mapFile.toURI().toURL());

            dataStore = DataStoreFinder.getDataStore(map);
            String typeName = dataStore.getTypeNames()[0];

            FeatureSource<SimpleFeatureType, SimpleFeature> source = dataStore.getFeatureSource(typeName);
            Filter filter = Filter.INCLUDE; // ECQL.toFilter("BBOX(THE_GEOM, 10,20,30,40)")

            /*SimpleFeatureType schema = source.getSchema();
            String geomType = schema.getGeometryDescriptor().getType().getBinding().getName();
            System.out.println(geomType);*/

            FeatureCollection<SimpleFeatureType, SimpleFeature> collection = source.getFeatures(filter);
            try (FeatureIterator<SimpleFeature> features = collection.features()) {
                while (features.hasNext()) {
                    SimpleFeature feature = features.next();
                    Geometry geometry = (Geometry) feature.getDefaultGeometry();
                    GeometryFactory geometryFactory = new GeometryFactory();

                    // test each cell intersection
                    for (Cell cell : cells) {
                        Coordinate[] coords  = new Coordinate[] {new Coordinate(cell.getLongitude(), cell.getLatitude()),
                                new Coordinate(cell.getLongitude() + fisheryRegion.getCellLengthLongitude(), cell.getLatitude()),
                                new Coordinate(cell.getLongitude() + fisheryRegion.getCellLengthLongitude(), cell.getLatitude() + fisheryRegion.getCellLengthLatitude()),
                                new Coordinate(cell.getLongitude(), cell.getLatitude() + fisheryRegion.getCellLengthLatitude()),
                                new Coordinate(cell.getLongitude(), cell.getLatitude())};
                        Polygon polygon = geometryFactory.createPolygon(coords);
                        double intersectionArea = geometry.intersection(polygon).getArea();
                        double percent = intersectionArea / polygon.getArea();

                        // ici, c'est une hypothese de matcher à 95%
                        // mais il pourrait très bien y avoir d'autres hypotheses à 50% ou 10%
                        if (percent > intersectionPourcentage) {
                            includedCells.add(cell);
                        }
                    }
                }
            }
        } catch (IOException ex) {
            log.error("Can't parce shp file");
        } finally {
            if (dataStore != null) {
                dataStore.dispose();
            }
        }

        return includedCells;
    }

    public static void setCellToShapefile(FisheryRegion fisheryRegion, List<Cell> cells, String name, File file) {
        try {
            SimpleFeatureType TYPE = DataUtilities.createType(name, "the_geom:MultiPolygon:srid=4326");

            /*
             * A list to collect features as we create them.
             */
            List<SimpleFeature> features = new ArrayList<>();

            /*
             * GeometryFactory will be used to create the geometry attribute of each feature,
             * using a Point object for the location.
             */
            GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory();

            SimpleFeatureBuilder featureBuilder = new SimpleFeatureBuilder(TYPE);

            for (Cell cell : cells) {
                Polygon polygon = geometryFactory.createPolygon(new Coordinate[]{
                        new Coordinate(cell.getLongitude(), cell.getLatitude()),
                        new Coordinate(cell.getLongitude(), cell.getLatitude() + fisheryRegion.getCellLengthLatitude()),
                        new Coordinate(cell.getLongitude() + fisheryRegion.getCellLengthLongitude(), cell.getLatitude() + fisheryRegion.getCellLengthLatitude()),
                        new Coordinate(cell.getLongitude() + fisheryRegion.getCellLengthLongitude(), cell.getLatitude()),
                        new Coordinate(cell.getLongitude(), cell.getLatitude())
                });

                featureBuilder.add(polygon);
                SimpleFeature feature = featureBuilder.buildFeature(null);
                features.add(feature);
            }

            ShapefileDataStoreFactory dataStoreFactory = new ShapefileDataStoreFactory();

            Map<String, Serializable> params = new HashMap<>();
            params.put("url", file.toURI().toURL());
            params.put("create spatial index", Boolean.TRUE);

            ShapefileDataStore newDataStore = (ShapefileDataStore) dataStoreFactory.createNewDataStore(params);

            /*
             * TYPE is used as a template to describe the file contents
             */
            newDataStore.createSchema(TYPE);

            /*
             * Write the features to the shapefile
             */
            Transaction transaction = new DefaultTransaction("create");

            String typeName = newDataStore.getTypeNames()[0];
            SimpleFeatureSource featureSource = newDataStore.getFeatureSource(typeName);
            SimpleFeatureType SHAPE_TYPE = featureSource.getSchema();
            /*
             * The Shapefile format has a couple limitations:
             * - "the_geom" is always first, and used for the geometry attribute name
             * - "the_geom" must be of type Point, MultiPoint, MuiltiLineString, MultiPolygon
             * - Attribute names are limited in length
             * - Not all data types are supported (example Timestamp represented as Date)
             *
             * Each data store has different limitations so check the resulting SimpleFeatureType.
             */
            //System.out.println("SHAPE:" + SHAPE_TYPE);

            if (featureSource instanceof SimpleFeatureStore) {
                SimpleFeatureStore featureStore = (SimpleFeatureStore) featureSource;
                /*
                 * SimpleFeatureStore has a method to add features from a
                 * SimpleFeatureCollection object, so we use the ListFeatureCollection
                 * class to wrap our list of features.
                 */
                SimpleFeatureCollection collection = new ListFeatureCollection(TYPE, features);
                featureStore.setTransaction(transaction);
                try {
                    featureStore.addFeatures(collection);
                    transaction.commit();
                } catch (Exception problem) {
                    problem.printStackTrace();
                    transaction.rollback();
                } finally {
                    transaction.close();
                }
            } else {
                System.out.println(typeName + " does not support read/write access");
            }

            newDataStore.dispose();
        } catch (SchemaException | IOException e) {
            e.printStackTrace();
        }
    }
}
