/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2010 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.map;

import static org.nuiton.i18n.I18n.t;

import java.awt.Graphics;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.bbn.openmap.MapBean;
import com.bbn.openmap.event.MapMouseListener;

/**
 * Listener for openmap map bean to display popop menu on right click.
 * 
 * Currently, this listener can :
 *  - copy current map to system clip board
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class CopyMapToClipboardListener implements ActionListener,
        MapMouseListener {

    /** Class logger. */
    private static Log log = LogFactory
            .getLog(CopyMapToClipboardListener.class);

    /** Copy to clic board action command. */
    protected static final String COPY_TO_CLICBOARD_COMMAND = "copytoclipboard";

    /** Popop menu. */
    public JPopupMenu menu;

    /** Open map bean. */
    protected MapBean mapBean;

    /**
     * Constructor with map.
     * 
     * @param mapBean open map bean
     */
    public CopyMapToClipboardListener(MapBean mapBean) {
        this.mapBean = mapBean;

        menu = new JPopupMenu();
        JMenuItem copyClicboardItem = new JMenuItem();
        copyClicboardItem.setText(t("isisfish.input.map.copytoclicboard"));
        copyClicboardItem.setActionCommand(COPY_TO_CLICBOARD_COMMAND);
        copyClicboardItem.addActionListener(this);
        menu.add(copyClicboardItem);
    }

    public MapBean getMapBean() {
        return mapBean;
    }

    protected void copyToClipBoard() {

        // Get image from mapBean
        int width = getMapBean().getWidth();
        int height = getMapBean().getHeight();
        BufferedImage image = new BufferedImage(width, height,
                BufferedImage.TYPE_INT_RGB);
        GraphicsEnvironment ge = GraphicsEnvironment
                .getLocalGraphicsEnvironment();
        Graphics g = ge.createGraphics(image);
        g.setClip(0, 0, width, height);
        getMapBean().paintAll(g);
        ImageSelection imgSel = new ImageSelection(image);

        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(imgSel,
                null);

        if (log.isInfoEnabled()) {
            log.info("Copy current image to system clipboard");
        }
    }

    // This class is used to hold an image while on the clipboard.
    public static class ImageSelection implements Transferable {
        private Image image;

        public ImageSelection(Image image) {
            this.image = image;
        }

        // Returns supported flavors
        public DataFlavor[] getTransferDataFlavors() {
            return new DataFlavor[] { DataFlavor.imageFlavor };
        }

        // Returns true if flavor is supported
        public boolean isDataFlavorSupported(DataFlavor flavor) {
            return DataFlavor.imageFlavor.equals(flavor);
        }

        // Returns image
        public Object getTransferData(DataFlavor flavor)
                throws UnsupportedFlavorException, IOException {
            if (!DataFlavor.imageFlavor.equals(flavor)) {
                throw new UnsupportedFlavorException(flavor);
            }
            return image;
        }
    }

    @Override
    public String[] getMouseModeServiceList() {
        return null;
    }

    @Override
    public boolean mouseClicked(MouseEvent e) {

        // seulement en menu contextuel
        if (e.getButton() == MouseEvent.BUTTON3) {
            menu.show(mapBean, e.getX(), e.getY());
        }
        return false;
    }

    @Override
    public boolean mouseDragged(MouseEvent arg0) {
        return false;
    }

    @Override
    public void mouseEntered(MouseEvent arg0) {

    }

    @Override
    public void mouseExited(MouseEvent arg0) {

    }

    @Override
    public void mouseMoved() {

    }

    @Override
    public boolean mouseMoved(MouseEvent arg0) {
        return false;
    }

    @Override
    public boolean mousePressed(MouseEvent arg0) {
        return false;
    }

    @Override
    public boolean mouseReleased(MouseEvent arg0) {
        return false;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getActionCommand().equals(COPY_TO_CLICBOARD_COMMAND)) {
            copyToClipBoard();
        }

    }
}
