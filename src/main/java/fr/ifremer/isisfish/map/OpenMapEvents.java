/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2005 - 2010 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.map;

import java.awt.event.MouseEvent;

import com.bbn.openmap.Layer;
import com.bbn.openmap.event.CoordMouseMode;
import com.bbn.openmap.event.MapMouseListener;
import com.bbn.openmap.event.MapMouseMode;
import com.bbn.openmap.event.ProjectionEvent;
import com.bbn.openmap.event.SelectMouseMode;

/**
 * OpenMapEvents.java
 *
 * Created: 5 septembre 2005 03:21:41 CEST
 *
 * @author Benjamin POUSSIN &lt;poussin@codelutin.com&gt;
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public abstract class OpenMapEvents extends Layer implements MapMouseListener { // OpenMapEvents

    /** serialVersionUID. */
    private static final long serialVersionUID = -8365120112075269288L;

    /**
     * SelectMouseMode property: Null, Distance, Nav, Select
     */
    protected MapMouseMode mouseMode;
    protected IsisMapBean map;
    
    protected int selectMode = CellSelectionLayer.SINGLE_SELECTION;

    /**
     * Constructor.
     * 
     * Register himself to {@code map} mapMouseListener.
     * Also set mouseMode and selectMode on map.
     * 
     * @param map map bean
     * @param mouseMode mouse mode
     * @param selectMode select mode
     */
    public OpenMapEvents(IsisMapBean map, CoordMouseMode mouseMode, int selectMode) {
        super();
        this.mouseMode = mouseMode;
        this.map = map;
        this.selectMode = selectMode;
        map.setSelectionMode(selectMode);
        map.setActiveMouseMode(mouseMode);
        map.addMapMouseListener(this);

    }

    @Override
    public void projectionChanged(ProjectionEvent e) {
        // do nothing, it's not reel layer
    }

    @Override
    public String[] getMouseModeServiceList() {
        return new String[] { SelectMouseMode.modeID };
    }

    @Override
    public boolean mouseClicked(MouseEvent e) {
        return true;
    }
    
    @Override
    public boolean mouseDragged(MouseEvent e) {
        return false;
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mouseMoved() {
    }

    @Override
    public boolean mouseMoved(MouseEvent e) {
        return false;
    }

    @Override
    public boolean mousePressed(MouseEvent e) {
        return false;
    }

    @Override
    public boolean mouseReleased(MouseEvent e) {
        return false;
    }

} // OpenMapEvents
