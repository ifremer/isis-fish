/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2010 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.map;

import static org.nuiton.i18n.I18n.t;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.Random;
import java.util.Vector;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixND;

import com.bbn.openmap.event.MapMouseListener;
import com.bbn.openmap.event.ProjectionEvent;
import com.bbn.openmap.event.SelectMouseMode;
import com.bbn.openmap.layer.OMGraphicHandlerLayer;
import com.bbn.openmap.omGraphics.OMGraphicList;
import com.bbn.openmap.proj.Projection;
import com.bbn.openmap.proj.coords.LatLonPoint;

import fr.ifremer.isisfish.entities.Cell;
import fr.ifremer.isisfish.entities.Zone;

/**
 * ResultatLayer.java
 * 
 * Created: Mon Apr 15 15:14:40 2002
 * 
 * @author <a href="mailto:seb.regnier@free.fr"></a>
 */
public class ResultatLayer extends OMGraphicHandlerLayer implements
        MapMouseListener {

    /** serialVersionUID. */
    private static final long serialVersionUID = 387094105186421164L;
    
    /** Class logger. */
    private static Log log = LogFactory.getLog(ResultatLayer.class);

    protected DataMapList dataMapList = new DataMapList();

    protected JPanel palette = null;

    protected Projection proj = null;

    /* Contient tous les carres possibles */
    protected OMGraphicList graphics = new OMGraphicList();

    public ResultatLayer() {
    }

    /**
     * Add a DataMap to this layer.
     * 
     * @param data DataMap to add.
     */
    public void addDataMap(DataMap data) {
        dataMapList.addDataMap(data);
        generateGraphics();
    }

    /**
     * Set the dataMapList.
     * 
     * @param list DataMapList
     */
    public void setDataMapList(DataMapList list) {
        this.dataMapList = list;
        generateGraphics();
    }

    /**
     * Get the DataMapList in this layer.
     * 
     * @return DataMapList
     */
    public DataMapList getDataMapList() {
        return dataMapList;
    }

    public void setMatriceInfo(MatrixND matInfo) {
        DataMapList dml = new DataMapList();
        List vzone = null;
        int position = -1;

        // rechercher des zones dans la matrice
        for (int i = 0; i < matInfo.getDimCount(); i++) {
            List sem = matInfo.getSemantic(i);
            if (sem.size() > 0 && sem.get(0) instanceof Zone) {
                position = i;
                vzone = sem;
                break;
            }
        }

        if (position == -1 || vzone == null || vzone.size() == 0) {
            setDataMapList(dml);
            return;
        }

        Vector<float[]> latitude = new Vector<>();
        Vector<float[]> longitude = new Vector<>();
        float pasMailleLatitude = 0f;
        float pasMailleLongitude = 0f;

        try {
            for (Object aVzone : vzone) {
                Zone sect = (Zone) aVzone;
                List<Cell> cells = sect.getCell();
                float[] lat = new float[cells.size()];
                float[] lon = new float[cells.size()];
                for (int j = 0; j < cells.size(); j++) {
                    Cell cell = cells.get(j);
                    lat[j] = cell.getLatitude();
                    lon[j] = cell.getLongitude();
                    if (pasMailleLatitude == 0f && pasMailleLongitude == 0f) {
                        pasMailleLatitude = cell.getFisheryRegion()
                                .getCellLengthLatitude();
                        pasMailleLongitude = cell.getFisheryRegion()
                                .getCellLengthLongitude();
                    }
                }
                latitude.add(lat);
                longitude.add(lon);
            }
        } catch (Exception eee) {
            if (log.isErrorEnabled()) {
                log.error("Erreur dans la creation du datamaplist", eee);
            }
            setDataMapList(dml);
            return;
        }

        for (int i = 0; i < matInfo.getDim(0); i++) {
            for (int j = 0; j < matInfo.getDim(1); j++) {
                double elem = matInfo.getValue(i, j);
                DefaultDataMap data = new DefaultDataMap();
                data.setValue(elem);
                String itemx = matInfo.getSemantic(0).get(i).toString();
                String itemy = matInfo.getSemantic(1).get(j).toString();
                if (position == 0) {
                    data.setCoordinates(latitude.get(i),
                            longitude.get(i));
                    data.setInfo(new DataMapInfo(itemx + " - " + itemy));
                } else {
                    data.setCoordinates(latitude.get(j),
                            longitude.get(j));
                    data.setInfo(new DataMapInfo(itemy + " - " + itemx));
                }
                dml.addDataMap(data);
            }
        }

        Random ran = new Random(System.currentTimeMillis());
        Color color = new Color(ran.nextInt() & 0xFFFFFF);
        Motif motif = new Motif(color);
        motif.setHeight(pasMailleLatitude);
        motif.setWidth(pasMailleLongitude);
        dml.setMotif(motif);

        setDataMapList(dml);
    }

    /**
     * Set the projection.
     * 
     * @param proj Projection
     */
    public void setProjection(Projection proj) {
        this.proj = proj;
    }

    /**
     * Get the projection.
     * 
     * @return Projection proj
     */
    public Projection getProjection() {
        return proj;
    }

    /**
     * Implementing the ProjectionPainter interface.
     */
    public void renderDataForProjection(Projection proj, java.awt.Graphics g) {
        if (proj == null) {
            return;
        } else {
            generateGraphics();
            setProjection(proj.makeClone());
        }
        paint(g);
    }

    /**
     * Invoked when the projection has changed or this Layer has been added to
     * the MapBean.
     * 
     * @param e ProjectionEvent
     */
    public void projectionChanged(ProjectionEvent e) {
        setProjection(e.getProjection());
        repaint();
    }

    /**
     * Create and project the graphics.Creer l ensemble des carres possible et
     * affiche le cadrillage.
     */
    protected void generateGraphics() {
        graphics = dataMapList.createOMGraphicList(getProjection());
        graphics.generate(getProjection());
    }

    /**
     * Palette associated to the layer
     * 
     * @return The gui to modify the layer
     */
    public Component getGUI() {
        // FIXME to be continue
        // if (palette == null) {
        // palette = new ResultatMapPalette(this);
        // }
        return palette;
    }

    /**
     * Paints the layer.
     * 
     * @param g the Graphics context for painting
     */
    public void paint(Graphics g) {
        generateGraphics();
        graphics.generate(getProjection());
        graphics.render(g);
    }

    /**
     * Make the palette visible.
     */
    public void showPalette() {
        // FIXME to be continue
        // if (resultatMapBean != null){
        // resultatMapBean.getPalettePanel().removeAll();
        // resultatMapBean.getPalettePanel().add(this.getGUI());
        // resultatMapBean.getPalettePanel().revalidate();
        // resultatMapBean.getPalettePanel().repaint();
        // }
    }

    /**
     * Hide the layer's palette.
     */
    public void hidePalette() {
        // FIXME to be continue
        // if (resultatMapBean != null){
        // resultatMapBean.getPalettePanel().removeAll();
        // resultatMapBean.getPalettePanel().revalidate();
        // resultatMapBean.getPalettePanel().repaint();
        // }
    }

    /**
     * Return a list of the modes that are interesting to the MapMouseListener.
     * The source MouseEvents will only get sent to the MapMouseListener if the
     * mode is set to one that the listener is interested in. Layers interested
     * in receiving events should register for receiving events in "select"
     * mode.
     * <pre>
     * return new String[1] { SelectMouseMode.modeID };
     * </pre>
     * @see SelectMouseMode#modeID
     */
    public String[] getMouseModeServiceList() {
        return new String[] { SelectMouseMode.modeID };
    }

    /**
     * Note: A layer interested in receiving amouse events should implement this
     * function . Otherwise, return the default, which is null.
     */
    public synchronized MapMouseListener getMapMouseListener() {
        return this;
    }

    /**
     * Invoked when a mouse button has been pressed on a component.
     * 
     * @param e MouseEvent
     * @return true if the listener was able to process the event.
     */
    public boolean mousePressed(MouseEvent e) {
        return false;
    }

    /**
     * Invoked when a mouse button has been released on a component.
     * 
     * @param e MouseEvent
     * @return true if the listener was able to process the event.
     */
    public boolean mouseReleased(MouseEvent e) {
        return false;
    }

    /**
     * Invoked when the mouse has been clicked on a component. The listener will
     * receive this event if it successfully processed
     * {@code mousePressed()}, or if no other listener processes the
     * event. If the listener successfully processes {@code mouseClicked()}, then it
     * will receive the next {@code mouseClicked()} notifications that have a click
     * count greater than one.
     * 
     * @param e MouseEvent to handle.
     * @return true if the listener was able to process the event.
     */
    public boolean mouseClicked(MouseEvent e) {

        // on recherche le datamap clique
        LatLonPoint llp = getProjection().inverse(e.getX(), e.getY());
        List<DataMap> datamaps = getDataMapList().getDataMaps(
                new Coordinate(llp.getLatitude(), llp.getLongitude()));
        if (datamaps != null && datamaps.size() != 0) {
            String info = "";
            for (Object datamap : datamaps) {
                DefaultDataMap dataMap = (DefaultDataMap) datamap;
                if (dataMap.getInfo() != null) {
                    info += t("Info") + ": " + dataMap.getInfo().getLabels()
                            + " " + t("isisfish.common.value") + ":" + dataMap.getValue() + "\n";
                }
                else {
                    info += " " + t("isisfish.common.value") + ":" + dataMap.getValue() + "\n";
                }
            }

            // InfoDisplayEvent infoevt = new InfoDisplayEvent(this,info);
            // fireRequestMessage(infoevt);
            JOptionPane.showMessageDialog(this, info, this.getName(),
                    JOptionPane.INFORMATION_MESSAGE);

            // e.consume();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Invoked when the mouse enters a component.
     * 
     * @param e MouseEvent to handle.
     */
    public void mouseEntered(MouseEvent e) {
    }

    /**
     * Invoked when the mouse exits a component.
     * 
     * @param e MouseEvent to handle.
     */
    public void mouseExited(MouseEvent e) {
    }

    /**
     * Invoked when a mouse button is pressed on a component and then dragged.
     * The listener will receive these events if it successfully processes
     * mousePressed(), or if no other listener processes the event.
     * 
     * @param e MouseEvent to handle.
     * @return true if the listener was able to process the event.
     */
    public boolean mouseDragged(MouseEvent e) {
        return false;
    }

    /**
     * Invoked when the mouse button has been moved on a component (with no
     * buttons no down).
     * 
     * @param e MouseEvent to handle.
     * @return true if the listener was able to process the event.
     */
    public boolean mouseMoved(MouseEvent e) {
        // System.out.println("ResultatLayer
        // mouseClicked-------------------------");
        // // on recherche le datamap clique
        // LatLonPoint llp = getProjection().inverse(e.getX(),e.getY());
        // Vector datamaps = getDataMapList().getDataMaps(new
        // Coordinate(llp.getLatitude(),llp.getLongitude()));
        // String info = "";
        // for(int i=0; i < datamaps.size();i++){
        // DefaultDataMap dataMap = (DefaultDataMap)datamaps.get(i);
        // info +="Info: "+dataMap.getInfo().getLabels()+"
        // Value:"+dataMap.getValue()+"\n";
        // }
        // System.out.println(info);
        // fireRequestToolTip(e,info);
        // System.out.println("ResultatLayer
        // mouseClicked++++++++++++++++++++++++");
        // return true;
        return false;
    }

    /**
     * Handle a mouse cursor moving without the button being pressed. This event
     * is intended to tell the listener that there was a mouse movement, but
     * that the event was consumed by another layer. This will allow a mouse
     * listener to clean up actions that might have happened because of another
     * motion event response.
     */
    public void mouseMoved() {
    }

} // ResultatLayer
