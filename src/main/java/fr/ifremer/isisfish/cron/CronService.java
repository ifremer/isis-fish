/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2010 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.cron;

import java.util.Collection;
import java.util.LinkedList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Main cron service job.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class CronService extends Thread {

    /** Class logger. */
    private static Log log = LogFactory.getLog(CronService.class);

    /** Registered services. */
    protected Collection<Runnable> services;

    /**
     * Init cron service with default available services.
     */
    public CronService() {
        services = new LinkedList<>();
        registerDefaultServices();
    }

    /**
     * Register default services (always executed if service is launched).
     */
    private void registerDefaultServices() {
        addService(new RemoveOldFileTask());
    }

    /**
     * Add a new task for execution.
     * 
     * @param task task to add
     */
    protected void addService(Runnable task) {
        services.add(task);
    }

    /**
     * Run all registered services once and stop.
     * 
     * This behavior may change later to run as a real cron service.
     */
    public void run() {

        if (log.isInfoEnabled()) {
            log.info("Starting " + services.size() + " registered services");
        }

        for (Runnable task : services) {
            Thread taskThread = new Thread(task);
            try {
                taskThread.start();
                taskThread.join(); // run sequential
            } catch (Exception ex) {
                if (log.isDebugEnabled()) {
                    log.debug("Task " + task.getClass().getSimpleName() + " failed to run", ex);
                }
            }
        }

        // free memory
        services.clear();
    }
}
