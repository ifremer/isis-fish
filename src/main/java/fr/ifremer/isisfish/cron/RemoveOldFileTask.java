/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2011 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.cron;

import java.io.File;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.ifremer.isisfish.IsisFish;

/**
 * This cron task is used to remove old isis files.
 * 
 * Currently remove :
 * <ul>
 *  <li>monitored simulations files older than 1 year</li>
 * </ul>
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class RemoveOldFileTask implements Runnable {

    /** Class logger. */
    private static Log log = LogFactory.getLog(RemoveOldFileTask.class);

    @Override
    public void run() {
        if (log.isDebugEnabled()) {
            log.debug("Remove old simulation files task called");
        }
        removeMonitoredSimulations();
    }

    /**
     * Remove old *.zip files located in monitoring directory.
     */
    protected void removeMonitoredSimulations() {
        if (log.isDebugEnabled()) {
            log.debug("Removing old monitored simulations files");
        }

        File directory = IsisFish.config.getMonitoringDirectory();
        File[] files = directory.listFiles();

        // supprime les fichier au premier niveau (pour le moment)
        // qui sont plus vieux qu'un an
        for (File file : files) {
            if (file.isFile()) {
                if (file.lastModified() + DateUtils.MILLIS_PER_DAY * 365 < System.currentTimeMillis()) {
                    if (log.isDebugEnabled()) {
                        log.debug("Removing file " + file);
                    }
                    file.delete();
                }
            }
        }
    }
}
