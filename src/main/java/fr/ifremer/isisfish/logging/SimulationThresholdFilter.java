/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C)  2014 - 2020 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.logging;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.ThreadContext;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.Logger;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.core.filter.AbstractFilter;
import org.apache.logging.log4j.message.Message;

import fr.ifremer.isisfish.datastore.ExportStorage;
import fr.ifremer.isisfish.datastore.FormuleStorage;
import fr.ifremer.isisfish.datastore.ObjectiveStorage;
import fr.ifremer.isisfish.datastore.OptimizationStorage;
import fr.ifremer.isisfish.datastore.RuleStorage;
import fr.ifremer.isisfish.datastore.ScriptStorage;
import fr.ifremer.isisfish.datastore.SensitivityAnalysisStorage;
import fr.ifremer.isisfish.datastore.SensitivityExportStorage;
import fr.ifremer.isisfish.datastore.SimulationPlanStorage;
import fr.ifremer.isisfish.datastore.SimulatorStorage;

import java.util.Map;

/**
 * Filter always available in log4j configuration that filter simulation log depending of simulation
 * log level configuration.
 */
@Plugin(name = "SimulationThresholdFilter", category = "Core", elementType = "filter", printObject = true)
public final class SimulationThresholdFilter extends AbstractFilter {
    private Level defaultThreshold;

    /**
     * Simulation id. Present in MDC context only if a simulation is running.
     */
    public static final String SIMULATION_ID = "simulationId";
    /**
     * Simulation log file for lof4j file appender (only if simulationId is defined)
     */
    public static final String SIMULATION_LOG_FILE = "simulationLogFile";
    /**
     * {@code fr.ifremer.isisfish} package log level.
     */
    public static final String SIMULATION_APPLICATION_LEVEL = "simulationApplicationLevel";
    /**
     * {@code org.nuiton} package log level.
     */
    public static final String SIMULATION_LIB_LEVEL = "simulationLibLevel";
    /**
     * Script log level.
     */
    public static final String SIMULATION_SCRIPT_LEVEL = "simulationScriptLevel";

    private SimulationThresholdFilter(final Level defaultLevel, final Result onMatch, final Result onMismatch) {
        super(onMatch, onMismatch);
        this.defaultThreshold = defaultLevel;
    }

    @Override
    public Result filter(final Logger logger, final Level level, final Marker marker, final String msg,
                         final Object... params) {
        return filter(logger.getName(), level, ThreadContext.getContext());
    }

    @Override
    public Result filter(final Logger logger, final Level level, final Marker marker, final Object msg,
                         final Throwable t) {
        return filter(logger.getName(), level, ThreadContext.getContext());
    }

    @Override
    public Result filter(final Logger logger, final Level level, final Marker marker, final Message msg,
                         final Throwable t) {
        return filter(logger.getName(), level, ThreadContext.getContext());
    }

    @Override
    public Result filter(final LogEvent event) {
        return filter(event.getLoggerFqcn(), event.getLevel(), event.getContextData().toMap());
    }

    private Result filter(final String loggerFqcn, final Level level, final Map<String, String> contextMap) {

        String simulationId = contextMap.get("simulationId");

        Result result;

        // so simulation, no specific filtering (neutral)
        if (simulationId == null) {
            result = Result.NEUTRAL;
        } else {
            Level ctxLevel = defaultThreshold;

            if (loggerFqcn.startsWith("fr.ifremer.isisfish")) {
                ctxLevel = Level.getLevel(contextMap.get(SIMULATION_APPLICATION_LEVEL));
            } else if (loggerFqcn.startsWith("org.nuiton")) {
                ctxLevel = Level.getLevel(contextMap.get(SIMULATION_LIB_LEVEL));
            } else if (StringUtils.startsWithAny(loggerFqcn,
                    ExportStorage.EXPORT_PATH,
                    FormuleStorage.FORMULE_PATH,
                    ObjectiveStorage.OBJECTIVE_PATH,
                    OptimizationStorage.OPTIMIZATION_PATH,
                    RuleStorage.RULE_PATH,
                    ScriptStorage.SCRIPT_PATH,
                    SensitivityAnalysisStorage.SENSITIVITY_ANALYSIS_PATH,
                    SensitivityExportStorage.SENSITIVITY_EXPORT_PATH,
                    SimulationPlanStorage.SIMULATION_PLAN_PATH,
                    SimulatorStorage.SIMULATOR_PATH)) {
                ctxLevel = Level.getLevel(contextMap.get(SIMULATION_SCRIPT_LEVEL));
            }

            result = level.isMoreSpecificThan(ctxLevel) ? onMatch : onMismatch;
        }

        return result;
    }

    /**
     * Create the DynamicThresholdFilter.
     *
     * @param defaultThreshold The default Level.
     * @param onMatch          The action to perform if a match occurs.
     * @param onMismatch       The action to perform if no match occurs.
     * @return The DynamicThresholdFilter.
     */
    @PluginFactory
    public static SimulationThresholdFilter createFilter(
            @PluginAttribute("defaultThreshold") final Level defaultThreshold,
            @PluginAttribute("onMatch") final Result onMatch,
            @PluginAttribute("onMismatch") final Result onMismatch) {
        final Level level = defaultThreshold == null ? Level.ERROR : defaultThreshold;
        return new SimulationThresholdFilter(level, onMatch, onMismatch);
    }
}
