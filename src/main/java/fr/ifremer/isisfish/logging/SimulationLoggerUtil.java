/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2014 Ifremer, Code Lutin, Benjamin Poussin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.logging;

import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.ui.logging.LogFrame;
import fr.ifremer.isisfish.util.UIUtil;
import org.apache.logging.log4j.ThreadContext;

import java.awt.Component;
import java.io.File;
import java.io.IOException;

import static org.nuiton.i18n.I18n.t;

/**
 * FIXME poussin, a priori cette classe ne supporte pas plusieurs simulation
 * en meme temps, ce qui est le cas si on a plusieurs processeurs de dispo :(
 * 
 * 
 * Usefull class for dealing with hot configuration of log4J. this is temporary
 * we must find a way to abstract this layer.
 * 
 * {@code #removeAppender(String, String)}  to remove a appender of a simulation
 * 
 * @author chemit
 */
public class SimulationLoggerUtil {

    public static final String PLAN_CONTEXT_ID = "planContextId";

    public static final String PLAN_LOG_FILE = "planLogFile";

    /**
     * Open a new log console for the given simulation
     *
     * @param simulationName name of the simulation to use
     */
    public static void showSimulationLogConsole(Component parent, String simulationName) throws IOException {
        SimulationStorage storage = SimulationStorage.getSimulation(simulationName);
        File logFile = new File(storage.getSimulationLogFile());
        String title = t("isisfish.simulation.log.console.title", simulationName);

        LogFrame logFrame = new LogFrame();
        logFrame.setName("logFrame");
        UIUtil.setIconImage(logFrame);
        logFrame.setTitle(title);
        logFrame.open(logFile);
        logFrame.setSize(1024, 768);
        logFrame.setLocationRelativeTo(parent);
        logFrame.setVisible(true);
    }

    public static void setPlanLoggingContext(String id) {
        ThreadContext.put(PLAN_CONTEXT_ID, id);
        File logFile = new File(IsisFish.config.getLogDirectory(), id + ".log");
        ThreadContext.put(PLAN_LOG_FILE, logFile.getAbsolutePath());
    }

    public static void clearPlanLoggingContext() {
        ThreadContext.clearAll();
    }
}
