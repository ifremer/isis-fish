/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 - 2019 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.logging;

import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.Property;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;

/**
 * This Appender allows the logging event to be manipulated before it is processed by other Appenders.
 */
@Plugin(name = "Null", category = "Core", elementType = "appender", printObject = true)
public final class NullAppender extends AbstractAppender {

    private NullAppender(final String name) {
        super(name, null, null, true, Property.EMPTY_ARRAY);
    }

    /**
     * Modify the event and pass to the subordinate Appenders.
     * @param event The LogEvent.
     */
    @Override
    public void append(LogEvent event) {
        
    }

    /**
     * Create a RewriteAppender.
     * @param name The name of the Appender.
     * @return The created RewriteAppender.
     */
    @PluginFactory
    public static NullAppender createAppender(@PluginAttribute("name") final String name) {
        return new NullAppender(name);
    }
}

