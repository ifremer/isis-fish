/*
 * #%L
 * IsisFish
 *
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2016 - 2018 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.logging;

import com.opencsv.CSVWriter;
import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.datastore.RegionStorage;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.hibernate.SessionFactory;
import org.hibernate.metadata.ClassMetadata;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.event.TopiaEntityEvent;
import org.nuiton.topia.event.TopiaEntityListener;
import org.nuiton.topia.framework.TopiaContextImpl;
import org.nuiton.topia.persistence.TopiaEntity;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;

/**
 * Listener topia qui ecrit un fichier de log avec toutes les modifications de la base de données
 * dans un fichier changes.csv dans le répertoire de la région.
 */
public class RegionChangeLogger implements TopiaEntityListener {

    /** CSV separator char. */
    protected static final char CSV_SEPARATOR = ';';
    
    /** CSV file name. */
    protected static final String CHANGES_FILE_NAME = "changes.csv";
    
    /** Region storage. */
    protected RegionStorage regionStorage;
    
    /** Context. */
    protected TopiaContext context;

    /** Output log file. */
    protected File logFile;

    public RegionChangeLogger(RegionStorage regionStorage, TopiaContext context) {
        this.regionStorage = regionStorage;
        this.context = context;
        logFile = new File(regionStorage.getDirectory(), CHANGES_FILE_NAME);
    }

    @Override
    public void create(TopiaEntityEvent event) {
        addLogLine("CREATE", event.getEntity().getClass(), event.getEntity());
    }

    @Override
    public void load(TopiaEntityEvent event) {

    }

    @Override
    public void update(TopiaEntityEvent event) {
        if (ArrayUtils.isNotEmpty(event.getDirtyProperties())) {
            Class<? extends TopiaEntity> clazz = event.getEntity().getClass();
            SessionFactory sesionFactory = ((TopiaContextImpl)context).getHibernateFactory();
            ClassMetadata classMetadata = sesionFactory.getClassMetadata(clazz);
            String[] propertyNames = classMetadata.getPropertyNames();

            for (int dirtyProperty : event.getDirtyProperties()) {
                String fieldName = propertyNames[dirtyProperty];
    
                Object oldValue = event.getOldState()[dirtyProperty];
                String oldValueString = oldValue != null ? oldValue.toString() : "";
                Object newValue = event.getState()[dirtyProperty];
                String newValueString = newValue != null ? newValue.toString() : "";

                // on ne log que s'il y a vraiment un changement
                if (!StringUtils.equals(oldValueString, newValueString)) {
                    addLogLine("UPDATE", clazz, event.getEntity(), fieldName, oldValueString, newValueString);
                }
            }
        }
    }

    @Override
    public void delete(TopiaEntityEvent event) {
        addLogLine("DELETE", event.getEntity().getClass(), event.getEntity());
    }

    protected void addLogLine(String changeType, Class<? extends TopiaEntity> clazz, Object entity) {
        addLogLine(changeType, clazz, entity, "", "", "");
    }

    protected void addLogLine(String changeType, Class<? extends TopiaEntity> clazz,
            Object entity, String field, String oldValue, String newValue) {
        String date = DateFormatUtils.format(new Date(), "yyyy/MM/dd HH:mm:ss");
        String entityDisplayInfo = getEntityDisplayInfo(entity);
        addLine(date, changeType, clazz.getSimpleName().replace("Impl", ""), entityDisplayInfo, field, oldValue, newValue);
    }


    /**
     * Add line into file.
     * 
     * File is opened and closed each time (otherwize, content is not dumped to disk :( )
     * 
     * @param data data to add
     */
    protected void addLine(String... data) {
        boolean fileExists = logFile.length() > 0;

        try (CSVWriter out = new CSVWriter(new BufferedWriter(new FileWriter(logFile, true)), CSV_SEPARATOR,
                CSVWriter.DEFAULT_QUOTE_CHARACTER, CSVWriter.DEFAULT_ESCAPE_CHARACTER, CSVWriter.DEFAULT_LINE_END)) {
            // add header if necessary
            if (!fileExists) {
                out.writeNext(new String[] {"date","type","entity","name","field","oldvalue","newvalue"});
            }

            out.writeNext(data);
        } catch (IOException ex) {
            throw new IsisFishRuntimeException("Can't write log file", ex);
        }
    }
    
    /**
     * Most of entity have a 'name' attribute. For those that don't, use toString().
     * 
     * @param entity entity
     * @return display info
     */
    protected String getEntityDisplayInfo(Object entity) {
        String result;
        try {
            result = BeanUtils.getProperty(entity, "name");
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            result = entity.toString();
        }
        return result;
    }
}
