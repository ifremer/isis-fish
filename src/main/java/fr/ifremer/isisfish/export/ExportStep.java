/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2016 Ifremer, Code Lutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.export;

import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.types.TimeStep;
import java.io.Writer;


/**
 * Interface que doivent implanter les classes d'export de resultats qui veulent
 * exporter au fur et a mesure de la simulation.
 * 
 * Created: 18 dec. 2014 15:03
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 *
 * @since 4.4.0.0
 */
public interface ExportStep extends ExportInfo {

    /**
     * Appelée au début de la simulation.
     *
     * @param simulation la simulation dont on souhaite exporter les resultats
     * @param out la sortie sur lequel il faut ecrire l'export
     * @throws Exception if export fail
     */
    void exportBegin(SimulationStorage simulation, Writer out)
            throws Exception;

    /**
     * Exporte les resultats, cette methode est appellee a la fin de chaque pas
     * de temps. Elle peut ecrire dans le fichier via le parametre out,
     * ou collecter les informations et les ecrires a la fin de la simulation
     * lorsque la methode {@link #exportEnd(fr.ifremer.isisfish.datastore.SimulationStorage, java.io.Writer)}
     * est appelee.
     * 
     * @param simulation la simulation dont on souhaite exporter les resultats
     * @param step le pas de temps courant de la simulation
     * @param out la sortie sur lequel il faut ecrire l'export
     * @throws Exception if export fail
     */
    void export(SimulationStorage simulation, TimeStep step, Writer out)
            throws Exception;

    /**
     * Appeler a la fin de la simulation.
     *
     * @param simulation la simulation dont on souhaite exporter les resultats
     * @param out la sortie sur lequel il faut ecrire l'export
     * @throws Exception if export fail
     */
    void exportEnd(SimulationStorage simulation, Writer out)
            throws Exception;
}
