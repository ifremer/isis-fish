/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2015 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.export;

import fr.ifremer.isisfish.result.NecessaryResult;

/**
 * Interface que doivent implanter les classes d'export de resultats.
 * 
 * Le nom du fichier d'export qui sera utilisé sera le basé sur le nom de la
 * classe d'export
 * 
 * Created: 22 janv. 2006 21:12:58
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public interface ExportInfo extends NecessaryResult {

    /**
     * Return ExportInfo description.
     * 
     * @return string displayable to the end user
     */
    String getDescription();

    /**
     * Return filename used to contains export data.
     * 
     * @return filename by example "myexport"
     */
    String getExportFilename();

    /**
     * Return extension used as filename name extension.
     * 
     * @return extension by example ".csv"
     */
    String getExtensionFilename();

}
