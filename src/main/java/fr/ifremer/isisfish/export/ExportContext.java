package fr.ifremer.isisfish.export;

/*-
 * #%L
 * ISIS-Fish
 * %%
 * Copyright (C) 2020 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.isisfish.datastore.SimulationStorage;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;

/**
 * In case of manual export, on result UI, there is not opened transaction for export to run properly.
 *
 * For example, {@code fr.ifremer.isisfish.datastore.ResultStorageAbstract#decorate(MatrixND, TopiaContext)} need a
 * transaction to convert semantics.
 */
public class ExportContext {

    private static ThreadLocal<ExportContext> exportContext = new ThreadLocal<ExportContext>() {
        protected synchronized ExportContext initialValue() {
            return new ExportContext();
        }
    };

    public static ExportContext get() {
        return exportContext.get();
    }

    public static void remove() {
        ExportContext current = get();
        if (current.db != null) {
            current.db.closeContext();
        }
        exportContext.remove();
    }

    protected SimulationStorage simulationStorage;

    /** TopiaContext must be used by rule action to modify data */
    protected TopiaContext db = null;

    public void setSimulationStorage(SimulationStorage simulationStorage) {
        this.simulationStorage = simulationStorage;
    }

    public SimulationStorage getSimulationStorage() {
        return simulationStorage;
    }

    public TopiaContext getDB() throws TopiaException {
        if (db == null && getSimulationStorage() != null) {
            db = getSimulationStorage().getMemStorage().beginTransaction();
        }
        return db;
    }
}
