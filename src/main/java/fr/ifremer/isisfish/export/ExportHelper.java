/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2020 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.export;

import fr.ifremer.isisfish.config.IsisConfig;
import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.datastore.ExportStorage;
import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.datastore.StorageHelper;
import fr.ifremer.isisfish.simulator.SimulationParameterPropertiesHelper;
import fr.ifremer.isisfish.types.TimeStep;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.CountingOutputStream;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaContext;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Properties;
import java.util.zip.GZIPOutputStream;

/**
 * Helper for exports manipulation.
 * 
 * Created: 20 janv. 2006 01:52:04
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class ExportHelper extends StorageHelper {

    /** Class logger. */
    private static final Log log = LogFactory.getLog(ExportHelper.class);

    /**
     * Permet de faire les exports pour une simulation.
     *
     * @param simulation La simulation pour lequel il faut faire les exports
     * @param destdir le repertoire dans lequel il faut ecrire les exports
     * @param exports les instances des exports à faire
     * @param compileDir le nom du répertoire ou les classes d'export sont compilées
     * 
     * @deprecated since 4.4.0.0, used only in one script, will not be replaced
     */
    @Deprecated
    public static void doExport(SimulationStorage simulation, File destdir,
            List<Export> exports, File compileDir) {

        // on ne compte plus ici les temps d'instanciations
        // deplacer dans SimulationExportResultWrapper#afterSimulation(SimulationContext)

        long writtenAll = 0;
        long timeStart = System.currentTimeMillis();
        for (Export export : exports) {
            String exportName = ExportStorage.getName(export);
            long written = 0;
            long time = System.currentTimeMillis();
            try {
                written = exportToFile(simulation, destdir, export);
                writtenAll += written;
            } catch (Exception eee) {
                if (log.isWarnEnabled()) {
                    log.warn("Can't export object: " + exportName, eee);
                }
            }
            simulation.getInformation().addExportSize(exportName, written);
            simulation.getInformation().addExportTime(exportName,System.currentTimeMillis() - time);
        }
        simulation.getInformation().addAllExportSize(writtenAll);
        simulation.getInformation().addAllExportTime(
                System.currentTimeMillis() - timeStart);
    }

    /**
     * Do single export.
     *
     * @param simulation la simulation pour lequel il faut faire les exports
     * @param destdir le repertoire dans lequel il faut ecrire les exports
     * @param export le nom des exports a faire
     *
     * @return number of byte written on disk
     *
     * @throws Exception si une erreur survient
     * 
     * @deprecated since 4.4.0.0, used only in one script, will not be replaced
     */
    @Deprecated
    protected static long exportToFile(SimulationStorage simulation, File destdir, Export export) throws Exception {
        long result = 0;

        String filename = export.getExportFilename();
        String extension = export.getExtensionFilename();

        if (!StringUtils.endsWithIgnoreCase(extension, IsisConfig.COMPRESSION_EXTENSION)
                && IsisFish.config.getExportForceCompression()) {
            extension += IsisConfig.COMPRESSION_EXTENSION;
        }

        File file = new File(destdir, filename + extension);
        // prevent two export with same name
        // name MyExport.csv become MyExport_1.csv
        int val = 0;
        while (file.exists()) {
            val++;
            file = new File(destdir, filename + extension + "_" + val);
        }

        Writer out = null;
        CountingOutputStream counter = null;
        try {

            OutputStream os = new FileOutputStream(file);
            os = counter = new CountingOutputStream(os);

            // if compression is needed by extension, add compression writer
            if (StringUtils.endsWithIgnoreCase(extension, IsisConfig.COMPRESSION_EXTENSION)) {
                os = new GZIPOutputStream(os);
            }

            out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(os, StandardCharsets.UTF_8)));

            export.export(simulation, out);
        } finally {
            IOUtils.closeQuietly(out);
            if (counter != null) {
                result = counter.getByteCount();
            }
        }
        return result;
    }

    /**
     * Do single export.
     *
     * @param simulationStorage la simulation pour lequel il faut faire les exports
     * @param file le fichier de destination
     * @param exportName le nom de l'exports a faire
     *
     * @throws Exception si une erreur survient
     */
    public static void exportToFile(SimulationStorage simulationStorage, String exportName, File file) throws Exception {
        ExportStorage exportStorage = ExportStorage.getExport(exportName);
        ExportInfo exportInfo = exportStorage.getNewInstance();
        exportToFile(simulationStorage, exportInfo, file);
    }

    /**
     * Do single export.
     *
     * @param simulationStorage la simulation pour lequel il faut faire les exports
     * @param file le fichier de destination
     * @param exportInfo l'exports a faire
     *
     * @throws Exception si une erreur survient
     */
    public static void exportToFile(SimulationStorage simulationStorage, ExportInfo exportInfo, File file) throws Exception {
        Writer out = null;

        try {
            ExportContext exportContext = ExportContext.get();
            exportContext.setSimulationStorage(simulationStorage);

            if (StringUtils.endsWithIgnoreCase(file.getName(), IsisConfig.COMPRESSION_EXTENSION)) {
                out = new BufferedWriter(new OutputStreamWriter(
                        new GZIPOutputStream(new FileOutputStream(file)), StandardCharsets.UTF_8));
            } else {
                out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8));
            }

            if (exportInfo instanceof ExportStep) {
                ExportStep exportStep = (ExportStep)exportInfo;
                exportStep.exportBegin(simulationStorage, out);
                TimeStep lastStep = simulationStorage.getResultStorage().getLastStep();
                for (TimeStep step = new TimeStep(0); !step.after(lastStep); step = step.next()) {
                    exportStep.export(simulationStorage, step, out);
                }
                exportStep.exportEnd(simulationStorage, out);

            } else {
                Export export = (Export)exportInfo;
                export.export(simulationStorage, out);
            }
        } finally {
            ExportContext.remove();

            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    // ignore
                }
            }
        }
    }

    /**
     * Recupere dans prop les valeurs des champs specifique au export et met a
     * jour les champs de l'export.
     * 
     * @param exportIndex l'index de l'export
     * @param export l'export a mettre à jour
     * @param context le topia context dont on a besoin
     * @param props les proprietes contenant les parametre de l'export
     */
    public static void populateSensitivityExport(int exportIndex, TopiaContext context,
            SensitivityExport export, Properties props) {
        populateStorageParams(exportIndex, context, export, props, SimulationParameterPropertiesHelper.SENSITIVITY_EXPORT_KEY);
    }
    
    /**
     * Permet de mettre les parametres de l'export sous une forme String pour
     * pouvoir les relire ensuite.
     *
     * @param sensitivityExportIndex l'index de l'export
     * @param context le topia context dont on a besoin
     * @param sensitivityExport l'export dont on souhaite mettre les parametres dans l'objet
     *        Properties retourné
     * @return L'objet Properties contenant les valeurs des parametres de l'export
     */
    public static Properties getSensitivityExportAsProperties(int sensitivityExportIndex, TopiaContext context, SensitivityExport sensitivityExport) {
        return getParamsAsProperties(sensitivityExportIndex, context, sensitivityExport,
                SimulationParameterPropertiesHelper.SENSITIVITY_EXPORT_KEY);
    }
}
