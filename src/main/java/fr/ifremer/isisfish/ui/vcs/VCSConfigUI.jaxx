<!--
  #%L
  IsisFish
  
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2009 - 2015 Ifremer, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->
<JDialog>
    <import>
    static org.nuiton.i18n.I18n.t
    javax.swing.text.html.HTMLEditorKit;
    javax.swing.event.HyperlinkListener;
    javax.swing.event.HyperlinkEvent;
    java.net.URL;
    javax.swing.JComponent
    java.awt.Color
    java.awt.Desktop
    </import>

    <script><![CDATA[
    public boolean cancel;
    protected boolean invalidFirstName;
    protected boolean invalidLastName;
    protected boolean invalidKey;
    protected boolean invalidEmail;
    protected boolean invalidLogin;

    private String oldEmail;
    private String oldServerLogin;
    private boolean oldUseSsh;
    private boolean oldNoPassPhrase;
    private String oldLastname;
    private String oldFirstname;
    private String oldServerPrivateKeyFile;

    init();

    helpEditor.addHyperlinkListener(new HyperlinkListener() {
        public void hyperlinkUpdate(HyperlinkEvent he) {
            if (he.getEventType() == HyperlinkEvent.EventType.ACTIVATED && Desktop.isDesktopSupported()) {

                try {
                    URL u = he.getURL();
                    Desktop.getDesktop().browse(u.toURI());
                } catch (Exception e) {
                    if (log.isWarnEnabled()) {
                        log.warn("Cannot open link (maybe defaut browser in not configured ?)");
                    }
                    if (log.isDebugEnabled()) {
                        log.debug("Error while opening link", e);
                    }
                }
            }
        }
    });
        
    protected void init() {
        String REGISTER_KEY_URL = "https://labs.libre-entreprise.org/account/editsshkeys.php";
        String DOC_URL = "http://isis-fish.labs.libre-entreprise.org/isis-fish/v3/user/addSshKey.html";

        StringBuilder builder = new StringBuilder();
        builder.append("<a href='").append(DOC_URL).append("'>");
        builder.append(t("isisfish.vcs.howto.save.key")).append("</a><br/>");
        builder.append("<a href='").append(REGISTER_KEY_URL).append("'>");
        builder.append(t("isisfish.vcs.save.key")).append("</a>");

        helpEditor.setEditorKit(new HTMLEditorKit());
        helpEditor.setText(builder.toString());
    }

    protected void saveOld() {
        this.oldFirstname=firstname.getText();
        this.oldLastname=lasstname.getText();
        this.oldEmail =email.getText();
        this.oldServerLogin=serverLogin.getText();
        this.oldServerPrivateKeyFile=serverPrivateKeyFile.getText();
        this.oldUseSsh=serverAuthenticationMethodSsh.isSelected();
        this.oldNoPassPhrase =serverNoPassPhrase.isSelected();
        doCheck();
    }

    protected void restoreOld() {
        firstname.setText(oldFirstname);
        lasstname.setText(oldLastname);
        email.setText(oldEmail);
        serverLogin.setText(oldServerLogin);
        serverPrivateKeyFile.setText(oldServerPrivateKeyFile);
        serverAuthenticationMethodSsh.setSelected(oldUseSsh);
        serverNoPassPhrase.setSelected(oldNoPassPhrase);
        doCheck();
    }
    public boolean isConfigValid() {
        return check(serverAuthenticationMethodSsh.isSelected());
    }

    protected boolean check(boolean ssh) {
        invalidFirstName=firstname.isVisible() && firstname.getText().isEmpty();
        invalidLastName=lasstname.isVisible() && lasstname.getText().isEmpty();
        invalidEmail=email.isVisible() && email.getText().isEmpty();
        if (ssh) {
            String file = serverPrivateKeyFile.getText();
            invalidKey=file.isEmpty() || !new java.io.File(file).exists();
            invalidLogin=serverLogin.getText().isEmpty();
        } else {
            invalidLogin=false;
            invalidKey=false;
        }
        return !(invalidEmail||invalidFirstName||invalidLastName||invalidLogin||invalidKey);
    }

    protected void doCheck() {
        boolean ssh = serverAuthenticationMethodSsh.isSelected();
        ok.setEnabled(check(ssh));
        if (ssh) {
            serverPrivateKeyGenerate.setEnabled(!invalidLogin);
        } else {
            serverPrivateKeyGenerate.setEnabled(false);
        }        
        setColor(invalidFirstName, firstnameLabel);
        setColor(invalidLastName, lasstnameLabel);
        setColor(invalidEmail, emailLabel);
        setColor(invalidLogin, serverLoginLabel);
        setColor(invalidKey, serverPrivateKeyFileLabel);
    }

    protected void setColor(boolean invalid, JComponent component) {
        component.setForeground(invalid ? Color.red: Color.black);
    }
    ]]>
    </script>
    <Table>
         <row fill='horizontal'>
            <cell columns='2' weighty="1" fill='both'>
                <JScrollPane height="60">
                    <JEditorPane id="helpEditor" editable="false" />
                </JScrollPane>
            </cell>
        </row>        
        <row fill='horizontal'>
            <cell>
                <JLabel id='firstnameLabel' text='isisfish.launch.firstname'/>
            </cell>
            <cell>
                <JTextField id='firstname' onKeyReleased="doCheck()"/>
            </cell>
        </row>
        <row fill='horizontal'>
            <cell>
                <JLabel id='lasstnameLabel' text='isisfish.launch.lasstname'/>
            </cell>
            <cell>
                <JTextField id='lasstname' onKeyReleased="doCheck()"/>
            </cell>
        </row>
        <row fill='horizontal'>
            <cell>
                <JLabel id='emailLabel' text='isisfish.launch.email'/>
            </cell>
            <cell>
                <JTextField id='email' onKeyReleased="doCheck()"/>
            </cell>
        </row>
        <row fill='horizontal'>
            <cell>
                <JLabel id='serverAuthenticationMethodLabel' text='isisfish.launch.server.authenticationMethod'/>
            </cell>
            <cell>
                <JPanel layout='{new GridLayout(0, 2, 2, 2)}'>
                    <JRadioButton id='serverAuthenticationMethodAnonymous' text='isisfish.launch.anonymous' buttonGroup='serverMethod' value='anonymous' onActionPerformed="doCheck()"/>
                    <JRadioButton id='serverAuthenticationMethodSsh' text='isisfish.launch.ssh' buttonGroup='serverMethod' value='ssh' onActionPerformed="doCheck()"/>
                </JPanel>
            </cell>
        </row>
        <row fill='horizontal'>
            <cell>
                <JLabel id='serverLoginLabel' text='isisfish.launch.server.login' enabled='{serverAuthenticationMethodSsh.isSelected()}'/>
            </cell>
            <cell>
                <JTextField id='serverLogin' onKeyReleased="doCheck()" enabled='{serverAuthenticationMethodSsh.isSelected()}'/>
            </cell>
        </row>
        <row fill='horizontal'>
            <cell>
                <JLabel id='serverLoginNoPassPhraseLabel' text='isisfish.launch.server.ssh.no.passphrase' enabled='{serverAuthenticationMethodSsh.isSelected()}'/>
            </cell>
            <cell>
                <JCheckBox id='serverNoPassPhrase' onKeyReleased="doCheck()" enabled='{serverAuthenticationMethodSsh.isSelected()}' selected="true"/>
            </cell>
        </row>         
        <row fill='horizontal'>
            <cell>
                <JLabel id='serverPrivateKeyFileLabel' text='isisfish.launch.server.ssh.privateKeyFile' enabled='{serverAuthenticationMethodSsh.isSelected()}'/>
            </cell>
            <cell>
                <JTextField id='serverPrivateKeyFile' font-size='12' columns='28' height='25' onKeyReleased="doCheck()" enabled='{serverAuthenticationMethodSsh.isSelected()}'/>
            </cell>
        </row>
        <row fill='horizontal'>
            <cell columns="2">
                <JPanel layout='{new GridLayout(0, 2, 2, 2)}'>
                    <JButton id='serverPrivateKeyChangeFile' text='isisfish.launch.server.ssh.key.change'  enabled='{serverAuthenticationMethodSsh.isSelected()}'/>
                    <JButton id='serverPrivateKeyGenerate' text='isisfish.launch.server.ssh.key.generate' />
                </JPanel>
            </cell>
        </row>
        <row fill='horizontal'>
            <cell columns="2">
                <JPanel layout='{new GridLayout(0, 3, 2, 2)}'>
                    <JButton id='ok' text='isisfish.common.apply' enabled='false'/>
                    <JButton id='reset' text='isisfish.common.reset' onActionPerformed="restoreOld();"/>
                    <JButton id='cancelAction' text='isisfish.common.cancel' onActionPerformed="cancel = true;dispose()"/>
                </JPanel>
            </cell>
        </row>
    </Table>
</JDialog>
