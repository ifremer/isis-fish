/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.population;

import static org.nuiton.i18n.I18n.t;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.swing.JOptionPane;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixFactory;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.math.matrix.gui.MatrixPanelEvent;

import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.ui.input.InputContentHandler;
import fr.ifremer.isisfish.util.ErrorHelper;

/**
 * Population handler.
 */
public class PopulationRecruitmentHandler extends InputContentHandler<PopulationRecruitmentUI> {

    /** Class logger. */
    private static final Log log = LogFactory.getLog(PopulationRecruitmentHandler.class);

    protected PopulationRecruitmentHandler(PopulationRecruitmentUI inputContentUI) {
        super(inputContentUI);
    }

    protected void afterInit() {
        inputContentUI.addPropertyChangeListener(PopulationRecruitmentUI.PROPERTY_BEAN, evt -> {
            if (evt.getNewValue() == null) {
                inputContentUI.fieldPopulationMonthGapBetweenReproRecrutement.setText("");
                inputContentUI.fieldPopulationRecruitmentComment.setText("");
            }
            setPopulationRecruitmentDistributionMatrix();
        });
    }

    protected void setPopulationRecruitmentDistributionMatrix() {
        MatrixND recruitmentDistribution = Optional.ofNullable(inputContentUI.getBean()) //
                .map(Population::getRecruitmentDistribution) //
                .map(MatrixND::copy) //
                .orElse(null);
        inputContentUI.getFieldPopulationRecruitmentDistribution().setMatrix(recruitmentDistribution);
    }

    protected void populationRecruitmentDistributionMatrixChanged(MatrixPanelEvent event) {
        if (inputContentUI.getBean() != null) {
            if (inputContentUI.fieldPopulationRecruitmentDistribution.getMatrix() != null) {
                inputContentUI.getBean().setRecruitmentDistribution(inputContentUI.fieldPopulationRecruitmentDistribution.getMatrix().copy());
            }
        }
    }
    
    public void createRecruitmentDistribution(Population pop) {
        if (log.isTraceEnabled()) {
            log.trace("createRecruitmentDistributionon called: " + pop);
        }
        try {
            String val = JOptionPane
                    .showInputDialog(t("isisfish.message.recruitment.number.month"));
            int num = -1;
            if (val != null && !"".equals(val)) {
                try {
                    num = Integer.parseInt(val);
                } catch (RuntimeException eee) {
                    if (log.isWarnEnabled()) {
                        log.warn("Can't parse val: " + val, eee);
                    }
                }
            }

            if (num > 0) {
                List<String> sem = new ArrayList<>(num);
                for (int i = 0; i < num; i++) {
                    sem.add(t("isisfish.common.month", i));
                }
                MatrixND newMat = MatrixFactory.getInstance().create(
                        new List[] { sem });

                MatrixND mat = pop.getRecruitmentDistribution();
                if (mat != null) {
                    newMat.paste(mat);
                }
                pop.setRecruitmentDistribution(newMat);
                pop.update();
            }
        } catch (Exception eee) {
            if (log.isErrorEnabled()) {
                log.error("Can't remove RecruitmentDistribution", eee);
            }
            ErrorHelper.showErrorDialog(t("isisfish.error.input.removeentity",
                    "RecruitmentDistribution"), eee);
        }
    }
}
