/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.setofvessels;

import fr.ifremer.isisfish.entities.Port;
import fr.ifremer.isisfish.entities.VesselType;
import fr.ifremer.isisfish.ui.input.InputContentHandler;
import fr.ifremer.isisfish.ui.models.common.GenericComboModel;

/**
 * set of vessels handler.
 */
public class SetOfVesselsBasicsHandler extends InputContentHandler<SetOfVesselsBasicsUI> {

    protected boolean init;

    protected SetOfVesselsBasicsHandler(SetOfVesselsBasicsUI inputContentUI) {
        super(inputContentUI);
    }

    protected void afterInit() {

        inputContentUI.addPropertyChangeListener(SetOfVesselsBasicsUI.PROPERTY_BEAN, evt -> {
            if (evt.getNewValue() == null) {

            }
            if (evt.getNewValue() != null) {
                init = true;

                GenericComboModel<Port> modelPort = new GenericComboModel<>(inputContentUI.getFisheryRegion().getPort());
                inputContentUI.fieldSetOfVesselsPort.setModel(modelPort);
                inputContentUI.fieldSetOfVesselsPort.setSelectedItem(inputContentUI.getBean().getPort());

                GenericComboModel<VesselType> modelVessel = new GenericComboModel<>(inputContentUI.getFisheryRegion().getVesselType());
                inputContentUI.fieldSetOfVesselsVesselType.setModel(modelVessel);
                inputContentUI.fieldSetOfVesselsVesselType.setSelectedItem(inputContentUI.getBean().getVesselType());

                init=false;
            }
        });
    }
    
    protected void portChanged() {
        if (!init) {
            inputContentUI.getBean().setPort((Port)inputContentUI.fieldSetOfVesselsPort.getSelectedItem());
        }
    }
    protected void vesselTypeChanged() {
        if (!init) {
            inputContentUI.getBean().setVesselType((VesselType)inputContentUI.fieldSetOfVesselsVesselType.getSelectedItem());
        }
    }
}
