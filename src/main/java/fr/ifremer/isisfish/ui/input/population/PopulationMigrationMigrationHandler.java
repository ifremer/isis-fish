/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.population;

import static org.nuiton.i18n.I18n.t;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.math.matrix.gui.MatrixPanelEvent;

import fr.ifremer.isisfish.entities.PopulationGroup;
import fr.ifremer.isisfish.entities.PopulationSeasonInfo;
import fr.ifremer.isisfish.entities.Zone;
import fr.ifremer.isisfish.ui.input.InputContentHandler;
import fr.ifremer.isisfish.ui.models.common.GenericComboModel;
import fr.ifremer.isisfish.util.ErrorHelper;

/**
 * Population handler.
 */
public class PopulationMigrationMigrationHandler extends InputContentHandler<PopulationMigrationMigrationUI> {

    /** Class logger. */
    private static final Log log = LogFactory.getLog(PopulationMigrationMigrationHandler.class);

    protected PopulationMigrationMigrationHandler(PopulationMigrationMigrationUI inputContentUI) {
        super(inputContentUI);
    }

    protected void afterInit() {

        inputContentUI.addPropertyChangeListener(PopulationMigrationMigrationUI.PROPERTY_BEAN, evt -> {
            if (evt.getNewValue() == null) {
                inputContentUI.fieldPopulationMigrationMigrationCoefficient.setText("");
                inputContentUI.populationMigrationMigrationTable.setMatrix(null);
            }
            if (evt.getNewValue() != null) {

            }
            setFieldPopulationMigrationMigrationGroupChooserModel();
            setFieldPopulationMigrationMigrationDepartureZoneChooserModel();
            setFieldPopulationMigrationMigrationArrivalZoneChooserModel();
            setAddButton();
        });
    }

    protected void populationMigrationMigrationMatrixChanged(MatrixPanelEvent event) {
        if (inputContentUI.getPopInfo() != null) {
            inputContentUI.getPopInfo().setMigrationMatrix(inputContentUI.populationMigrationMigrationTable.getMatrix().clone());
        }
    }

    public void init(PopulationSeasonInfo pi) {
        // add null before, for second to be considered as a changed event
        // otherwize, setBean has no effect
        //setPopInfo(null);
        //setPopInfo(pi);

        inputContentUI.populationMigrationMigrationTable.setMatrix(inputContentUI.getPopInfo().getMigrationMatrix().copy());
    }

    protected void setFieldPopulationMigrationMigrationGroupChooserModel() {
        GenericComboModel<PopulationGroup> groups = new GenericComboModel<>();
        if (inputContentUI.getBean() != null && inputContentUI.getBean().getPopulationGroup() != null) {
            groups.setElementList(inputContentUI.getBean().getPopulationGroup());
        }
        inputContentUI.fieldPopulationMigrationMigrationGroupChooser.setModel(groups);
    }

    protected void setFieldPopulationMigrationMigrationDepartureZoneChooserModel() {
        GenericComboModel<Zone> zones = new GenericComboModel<>();
        if (inputContentUI.getBean() != null && inputContentUI.getBean().getPopulationZone() != null) {
            //jaxx.runtime.SwingUtil.fillComboBox(fieldPopulationMigrationMigrationDepartureZoneChooser,getBean().getPopulationZone(), null, true);
            zones.setElementList(inputContentUI.getBean().getPopulationZone());
        }
        inputContentUI.fieldPopulationMigrationMigrationDepartureZoneChooser.setModel(zones);
    }

    protected void setFieldPopulationMigrationMigrationArrivalZoneChooserModel() {
        GenericComboModel<Zone> zones = new GenericComboModel<>();
        if (inputContentUI.getBean() != null && inputContentUI.getBean().getPopulationZone() != null) {
            //jaxx.runtime.SwingUtil.fillComboBox(fieldPopulationMigrationMigrationArrivalZoneChooser,getBean().getPopulationZone(), null, true);
            zones.setElementList(inputContentUI.getBean().getPopulationZone());
        }
        inputContentUI.fieldPopulationMigrationMigrationArrivalZoneChooser.setModel(zones);
    }

    protected void add() {
        addMigration(inputContentUI.getPopInfo(), (PopulationGroup)inputContentUI.fieldPopulationMigrationMigrationGroupChooser.getSelectedItem(),
                (Zone)inputContentUI.fieldPopulationMigrationMigrationDepartureZoneChooser.getSelectedItem(),
                (Zone)inputContentUI.fieldPopulationMigrationMigrationArrivalZoneChooser.getSelectedItem(),
                Double.parseDouble(inputContentUI.fieldPopulationMigrationMigrationCoefficient.getText()));
        inputContentUI.populationMigrationMigrationTable.setMatrix(inputContentUI.getPopInfo().getMigrationMatrix().clone());
    }
    
    public Object addMigration(PopulationSeasonInfo info,
            PopulationGroup group, Zone departure, Zone arrival, double coeff) {
        if (log.isTraceEnabled()) {
            log.trace("addMigration called");
        }
        try {
            MatrixND mat = info.getMigrationMatrix().copy();
            mat.setValue(group, departure, arrival, coeff);
            info.setMigrationMatrix(mat);

        } catch (Exception eee) {
            if (log.isErrorEnabled()) {
                log.error("Can't add migration", eee);
            }
            ErrorHelper.showErrorDialog(t("isisfish.error.input.addentity",
                    "Migration"), eee);
        }
        return null;
    }

    protected void remove() {
        int row = inputContentUI.populationMigrationMigrationTable.getTable().getSelectedRow();
        if (row != -1) {
            Object group = inputContentUI.populationMigrationMigrationTable.getTable().getValueAt(row, 0);
            Object departure = inputContentUI.populationMigrationMigrationTable.getTable().getValueAt(row, 1);
            Object arrival = inputContentUI.populationMigrationMigrationTable.getTable().getValueAt(row, 2);

            MatrixND mat = inputContentUI.getPopInfo().getMigrationMatrix().clone();
            mat.setValue(group, departure, arrival, 0);
            inputContentUI.getPopInfo().setMigrationMatrix(mat);
            inputContentUI.populationMigrationMigrationTable.setMatrix(inputContentUI.getPopInfo().getMigrationMatrix().copy());
        }
    }

    protected void setAddButton() {
        inputContentUI.add.setEnabled(inputContentUI.isActive() && inputContentUI.fieldPopulationMigrationMigrationGroupChooser.getSelectedItem() != null
                && !inputContentUI.fieldPopulationMigrationMigrationCoefficient.getText().equals("")
                && inputContentUI.fieldPopulationMigrationMigrationDepartureZoneChooser.getSelectedItem() != null
                && inputContentUI.fieldPopulationMigrationMigrationArrivalZoneChooser.getSelectedItem() != null);
    }
}
