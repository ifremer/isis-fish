/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.population;

import static org.nuiton.i18n.I18n.t;

import javax.swing.JOptionPane;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.math.matrix.gui.MatrixPanelEditor;
import org.nuiton.math.matrix.gui.MatrixPanelEvent;

import fr.ifremer.isisfish.entities.PopulationSeasonInfo;
import fr.ifremer.isisfish.ui.input.InputContentHandler;
import fr.ifremer.isisfish.util.ErrorHelper;

/**
 * Population handler.
 */
public class PopulationSeasonSpacializedHandler extends InputContentHandler<PopulationSeasonSpacializedUI> {

    /** Class logger. */
    private static final Log log = LogFactory.getLog(PopulationSeasonsHandler.class);

    protected PopulationSeasonSpacializedHandler(PopulationSeasonSpacializedUI inputContentUI) {
        super(inputContentUI);
        // TODO Auto-generated constructor stub
    }

    protected void afterInit() {

    }
    
    protected void populationSeasonLengthMatrixChanged(MatrixPanelEvent event) {
        if (inputContentUI.getPopulationSeasonInfo() != null && inputContentUI.matrixPanelPopulationSeasonLengthChange.getMatrix() != null) {
            // must be a copy for fire event
            MatrixND lengthChangeMatrix = inputContentUI.matrixPanelPopulationSeasonLengthChange.getMatrix().copy();
            inputContentUI.getPopulationSeasonInfo().setLengthChangeMatrix(lengthChangeMatrix);
        }
    }

    /**
     * Called on spacialized radio button change.
     */
    protected void spacializedActionPerformed() {
        PopulationSeasonInfo popInfo = inputContentUI.getPopulationSeasonInfo();
        boolean spacializedSelection = inputContentUI.radioPopulationSeasonGroupChangeLengthNoSpacialized.isSelected();
        popInfo.setSimpleLengthChangeMatrix(spacializedSelection);
        try {
            MatrixND lengthChangeMatrix = popInfo.getLengthChangeMatrix();
            if (lengthChangeMatrix != null) {
                if (popInfo.isSimpleLengthChangeMatrix()) {
                    lengthChangeMatrix = popInfo.unspacializeLengthChangeMatrix(lengthChangeMatrix);
                } else {
                    lengthChangeMatrix = popInfo.spacializeLengthChangeMatrix(lengthChangeMatrix);
                }
                popInfo.setLengthChangeMatrix(lengthChangeMatrix);
                inputContentUI.matrixPanelPopulationSeasonLengthChange.setMatrix(lengthChangeMatrix);
            }
        } catch(Exception eee) {
            if (log.isErrorEnabled()) {
                log.error("Can't (un)spacialize Matrix Change Of Group", eee);
            }
            ErrorHelper.showErrorDialog(t("isisfish.error.input.spacializematrix"), eee);
        }
    }

    protected void computeMatrixChangeOfGroup() {
        PopulationSeasonInfo popInfo = inputContentUI.getPopulationSeasonInfo();
        MatrixND lengthChangeMatrix = popInfo.computeLengthChangeMatrix();
        if (!popInfo.isSimpleLengthChangeMatrix()){
            lengthChangeMatrix = popInfo.spacializeLengthChangeMatrix(lengthChangeMatrix);
        }
        popInfo.setLengthChangeMatrix(lengthChangeMatrix);
    }

    protected void showSpacializedMatrixChangeOfGroup() {
        PopulationSeasonInfo popInfo = inputContentUI.getPopulationSeasonInfo();
        MatrixND lengthChangeMatrix = popInfo.getLengthChangeMatrix();
        if (popInfo.isSimpleLengthChangeMatrix()) {
            lengthChangeMatrix = popInfo.spacializeLengthChangeMatrix(lengthChangeMatrix);
        }
        MatrixPanelEditor panel = new MatrixPanelEditor(false, 800, 300);
        panel.setMatrix(lengthChangeMatrix);
        JOptionPane.showMessageDialog(inputContentUI, panel, t("isisfish.populationSeasons.spacialized.visualisation"), JOptionPane.INFORMATION_MESSAGE);
    }
}
