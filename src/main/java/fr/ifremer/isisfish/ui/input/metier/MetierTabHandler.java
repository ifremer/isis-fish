/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.metier;

import fr.ifremer.isisfish.entities.Gear;
import fr.ifremer.isisfish.ui.input.InputContentHandler;
import fr.ifremer.isisfish.ui.models.common.GenericComboModel;

/**
 * Metier tab handler.
 */
public class MetierTabHandler extends InputContentHandler<MetierTabUI> {

    protected boolean init;

    protected MetierTabHandler(MetierTabUI inputContentUI) {
        super(inputContentUI);
    }

    protected void afterInit() {
        inputContentUI.addPropertyChangeListener(MetierTabUI.PROPERTY_BEAN, evt -> {
            GenericComboModel<Gear> model = new GenericComboModel<>(inputContentUI.getFisheryRegion().getGear());
            if (evt.getNewValue() == null) {

            }
            if (evt.getNewValue() != null) {
                init = true;
                model.setSelectedItem(inputContentUI.bean.getGear());
                init = false;
            }
            inputContentUI.fieldMetierGear.setModel(model);
        });
    }

    protected void gearChanged() {
        if (!init) {
            inputContentUI.getBean().setGear((Gear)inputContentUI.fieldMetierGear.getSelectedItem());
        }
    }
}
