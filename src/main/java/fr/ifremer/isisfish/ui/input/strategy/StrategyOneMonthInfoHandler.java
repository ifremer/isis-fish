/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 - 2018 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.strategy;

import fr.ifremer.isisfish.entities.TripType;
import fr.ifremer.isisfish.ui.input.InputContentHandler;
import fr.ifremer.isisfish.ui.models.common.GenericComboModel;

/**
 * Strategy one month info handler.
 */
public class StrategyOneMonthInfoHandler extends InputContentHandler<StrategyOneMonthInfoUI> {

    protected boolean init;

    protected StrategyOneMonthInfoHandler(StrategyOneMonthInfoUI inputContentUI) {
        super(inputContentUI);
    }

    protected void afterInit() {
        inputContentUI.addPropertyChangeListener(StrategyOneMonthInfoUI.PROPERTY_STRATEGY_MONTH_INFO, evt -> {
            if (evt.getNewValue() == null) {

            }
            if (evt.getNewValue() != null) {
                init = true;
                refresh();
                init = false;
            }
        });
    }
    
    protected void refresh() {
        if (inputContentUI.getStrategyMonthInfo() != null) {
            inputContentUI.numberOfTrips.putClientProperty("sensitivityBeanID", inputContentUI.getStrategyMonthInfo().getTopiaId());
            inputContentUI.fieldStrategyMonthInfoMinInactivityDays.putClientProperty("sensitivityBeanID", inputContentUI.getStrategyMonthInfo().getTopiaId());

            GenericComboModel<TripType> model = new GenericComboModel<>(inputContentUI.getFisheryRegion().getTripType());
            inputContentUI.fieldStrategyMonthInfoTripType.setModel(model);
            inputContentUI.fieldStrategyMonthInfoTripType.setSelectedItem(inputContentUI.getStrategyMonthInfo().getTripType());
        }
        else {
            // don't put in addPropertyChangeListener
            // if called after, remove content :(
            inputContentUI.numberOfTrips.setText("");
            inputContentUI.fieldStrategyMonthInfoMinInactivityDays.setText("");
        }
    }
}
