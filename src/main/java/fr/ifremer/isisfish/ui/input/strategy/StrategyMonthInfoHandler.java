/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.strategy;

import org.nuiton.math.matrix.MatrixND;
import org.nuiton.math.matrix.gui.MatrixPanelEvent;

import fr.ifremer.isisfish.ui.input.InputContentHandler;

/**
 * Strategy handler.
 */
public class StrategyMonthInfoHandler extends InputContentHandler<StrategyMonthInfoUI> {

    protected StrategyMonthInfoHandler(StrategyMonthInfoUI inputContentUI) {
        super(inputContentUI);
    }

    protected void afterInit() {

        inputContentUI.addPropertyChangeListener(StrategyMonthInfoUI.PROPERTY_BEAN, evt -> {
            if (evt.getNewValue() == null) {
                inputContentUI.setStrategyMonthInfo0(null);
                inputContentUI.setStrategyMonthInfo1(null);
                inputContentUI.setStrategyMonthInfo2(null);
                inputContentUI.setStrategyMonthInfo3(null);
                inputContentUI.setStrategyMonthInfo4(null);
                inputContentUI.setStrategyMonthInfo5(null);
                inputContentUI.setStrategyMonthInfo6(null);
                inputContentUI.setStrategyMonthInfo7(null);
                inputContentUI.setStrategyMonthInfo8(null);
                inputContentUI.setStrategyMonthInfo9(null);
                inputContentUI.setStrategyMonthInfo10(null);
                inputContentUI.setStrategyMonthInfo11(null);
                inputContentUI.fieldStrategyProportion.setMatrix(null);
            }
            if (evt.getNewValue() != null) {
                inputContentUI.setStrategyMonthInfo0(inputContentUI.getBean().getStrategyMonthInfo().get(0));
                inputContentUI.setStrategyMonthInfo1(inputContentUI.getBean().getStrategyMonthInfo().get(1));
                inputContentUI.setStrategyMonthInfo2(inputContentUI.getBean().getStrategyMonthInfo().get(2));
                inputContentUI.setStrategyMonthInfo3(inputContentUI.getBean().getStrategyMonthInfo().get(3));
                inputContentUI.setStrategyMonthInfo4(inputContentUI.getBean().getStrategyMonthInfo().get(4));
                inputContentUI.setStrategyMonthInfo5(inputContentUI.getBean().getStrategyMonthInfo().get(5));
                inputContentUI.setStrategyMonthInfo6(inputContentUI.getBean().getStrategyMonthInfo().get(6));
                inputContentUI.setStrategyMonthInfo7(inputContentUI.getBean().getStrategyMonthInfo().get(7));
                inputContentUI.setStrategyMonthInfo8(inputContentUI.getBean().getStrategyMonthInfo().get(8));
                inputContentUI.setStrategyMonthInfo9(inputContentUI.getBean().getStrategyMonthInfo().get(9));
                inputContentUI.setStrategyMonthInfo10(inputContentUI.getBean().getStrategyMonthInfo().get(10));
                inputContentUI.setStrategyMonthInfo11(inputContentUI.getBean().getStrategyMonthInfo().get(11));
                setProportionMetierMatrix();
            }
        });
    }
    
    protected void setProportionMetierMatrix() {
        MatrixND prop = inputContentUI.getBean().getProportionMetier();
        if (prop != null) {
            inputContentUI.fieldStrategyProportion.setMatrix(prop.copy());
        } else {
            inputContentUI.fieldStrategyProportion.setMatrix(null);
        }
    }

    protected void strategyProportionMatrixChanged(MatrixPanelEvent event) {
        MatrixND mat = inputContentUI.fieldStrategyProportion.getMatrix();
        if (inputContentUI.getBean() != null && mat != null) {
            inputContentUI.getBean().setProportionMetier(mat.copy());
        }
    }
}
