/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.tree;

import javax.swing.tree.DefaultTreeSelectionModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import fr.ifremer.isisfish.ui.input.InputSaveVerifier;
import fr.ifremer.isisfish.ui.input.InputUI;

/**
 * Surcharge du model de selection de l'arbre de pecherie pour
 * intersepter les changements de noeud dans le cas où l'utilisateur
 * n'aurais pas sauvé une UI et eviter de boucler dans le cas d'une
 * annulation.
 */
public class FisheryTreeSelectionModel extends DefaultTreeSelectionModel {

    /** serialVersionUID. */
    private static final long serialVersionUID = 2087232709918450858L;

    protected InputUI ui;

    public FisheryTreeSelectionModel(InputUI ui) {
        this.ui = ui;
        setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
    }

    @Override
    public void setSelectionPath(TreePath path) {
        if (willContinueSelection()) {
            super.setSelectionPath(path);
        }
    }

    @Override
    public void setSelectionPaths(TreePath[] pPaths) {
        if (willContinueSelection()) {
            super.setSelectionPaths(pPaths);
        }
    }

    @Override
    public void addSelectionPath(TreePath path) {
        if (willContinueSelection()) {
            super.addSelectionPath(path);
        }
    }

    @Override
    public void addSelectionPaths(TreePath[] paths) {
        if (willContinueSelection()) {
            super.addSelectionPaths(paths);
        }
    }

    @Override
    public void removeSelectionPath(TreePath path) {
        if (willContinueSelection()) {
            super.removeSelectionPath(path);
        }
    }

    @Override
    public void removeSelectionPaths(TreePath[] paths) {
        if (willContinueSelection()) {
            super.removeSelectionPaths(paths);
        }
    }

    @Override
    public void clearSelection() {
        if (willContinueSelection()) {
            super.clearSelection();
        }
    }

    protected boolean willContinueSelection() {

        boolean result = true;

        // check for non validated modification
        InputSaveVerifier inputSaveVerifier = ui.getContextValue(InputSaveVerifier.class);
        if (inputSaveVerifier.checkEdit() == InputSaveVerifier.CANCEL_OPTION) {
            result = false;
        }

        return result;
    }
}
