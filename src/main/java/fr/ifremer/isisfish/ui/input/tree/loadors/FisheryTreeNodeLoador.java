/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.tree.loadors;

import jaxx.runtime.swing.nav.tree.NavTreeNodeChildLoador;

import org.nuiton.topia.persistence.TopiaEntity;

import fr.ifremer.isisfish.ui.input.tree.FisheryTreeNode;

/**
 * Common class to all loador.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 * @param <T> 
 */
public abstract class FisheryTreeNodeLoador<T extends TopiaEntity> extends NavTreeNodeChildLoador<T, T, FisheryTreeNode> {

    /** serialVersionUID. */
    private static final long serialVersionUID = 7738607604066405224L;

    public FisheryTreeNodeLoador(Class<T> type) {
        super(type);
    }
}
