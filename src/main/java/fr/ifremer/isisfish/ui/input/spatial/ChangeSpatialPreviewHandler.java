/*
 * #%L
 * ISIS-Fish
 * %%
 * Copyright (C) 2017 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.ui.input.spatial;

import com.bbn.openmap.event.SelectMouseMode;
import fr.ifremer.isisfish.IsisFishDAOHelper;
import fr.ifremer.isisfish.entities.Cell;
import fr.ifremer.isisfish.entities.CellDAO;
import fr.ifremer.isisfish.entities.FisheryRegion;
import fr.ifremer.isisfish.entities.Port;
import fr.ifremer.isisfish.entities.Zone;
import fr.ifremer.isisfish.map.CellSelectionLayer;
import fr.ifremer.isisfish.map.DatabaseDataProvider;
import fr.ifremer.isisfish.map.OpenMapEvents;
import fr.ifremer.isisfish.ui.input.InputSaveVerifier;
import fr.ifremer.isisfish.ui.input.InputUI;
import fr.ifremer.isisfish.ui.models.common.GenericComboModel;
import jaxx.runtime.JAXXUtil;
import org.apache.commons.lang3.StringUtils;

import javax.swing.JOptionPane;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.nuiton.i18n.I18n.t;

/**
 * Handler fro preview spatial change UI.
 */
public class ChangeSpatialPreviewHandler {

    protected ChangeSpatialPreviewUI inputUI;

    protected Map<Zone, List<Cell>> zoneMap;

    protected Map<Port, Cell> portMap;

    protected List<Cell> cells;

    protected FisheryRegion oldFisheryRegion;

    protected FisheryRegion newFisheryRegion;

    public ChangeSpatialPreviewHandler(ChangeSpatialPreviewUI inputUI) {
        this.inputUI = inputUI;
    }

    public void setCells(List<Cell> cells) {
        this.cells = cells;
    }

    public void setZoneMap(Map<Zone, List<Cell>> zoneMap) {
        this.zoneMap = zoneMap;
    }

    public void setPortMap(Map<Port, Cell> portMap) {
        this.portMap = portMap;
    }

    public void setNewFisheryRegion(FisheryRegion newFisheryRegion) {
        this.newFisheryRegion = newFisheryRegion;
    }

    public void setOldFisheryRegion(FisheryRegion oldFisheryRegion) {
        this.oldFisheryRegion = oldFisheryRegion;
    }

    public void completeSetup() {
        initMapClick();
        initMapProviders();
    }

    /**
     * Configure click behaviour on maps.
     */
    protected void initMapClick() {
        new OpenMapEvents(inputUI.currentZoneMap, new SelectMouseMode(true), CellSelectionLayer.NO_SELECTION) {
            @Override
            public boolean mouseClicked(MouseEvent e) {
                return false;
            }
        };
        new OpenMapEvents(inputUI.currentPortMap, new SelectMouseMode(true), CellSelectionLayer.NO_SELECTION) {
            @Override
            public boolean mouseClicked(MouseEvent e) {
                return false;
            }
        };
        new OpenMapEvents(inputUI.newZoneMap, new SelectMouseMode(false), CellSelectionLayer.MULT_SELECTION) {
            @Override
            public boolean mouseClicked(MouseEvent e) {
                Zone selectedValue = (Zone) inputUI.currentZones.getSelectedItem();
                zoneMap.put(selectedValue, inputUI.newZoneMap.getSelectedCells());
                updateZoneCellDetails();
                return false;
            }
        };
        new OpenMapEvents(inputUI.newPortMap, new SelectMouseMode(false), CellSelectionLayer.SINGLE_SELECTION) {
            @Override
            public boolean mouseClicked(MouseEvent e) {
                Port selectedValue = (Port) inputUI.currentPorts.getSelectedItem();
                List<Cell> selectedCells = inputUI.newPortMap.getSelectedCells();
                if (!selectedCells.isEmpty()) {
                    portMap.put(selectedValue, selectedCells.get(0));
                }
                updatePortCellDetails();
                return false;
            }
        };
    }

    /**
     * Configure data provider for maps.
     */
    protected void initMapProviders() {
        List<Zone> zones = oldFisheryRegion.getZone();
        List<Port> ports = oldFisheryRegion.getPort();
        inputUI.currentZones.setModel(new GenericComboModel<>(zones));
        inputUI.currentZoneMap.setMapDataProvider(new DatabaseDataProvider(inputUI.currentZoneMap, oldFisheryRegion));
        inputUI.currentPorts.setModel(new GenericComboModel<>(ports));
        inputUI.currentPortMap.setMapDataProvider(new DatabaseDataProvider(inputUI.currentPortMap, oldFisheryRegion));

        SpatialChangeDataProvider spatialChangeDataProvider = new SpatialChangeDataProvider(newFisheryRegion, cells);
        inputUI.newZoneMap.setMapDataProvider(spatialChangeDataProvider);
        inputUI.newPortMap.setMapDataProvider(spatialChangeDataProvider);
    }

    /**
     * Action when a zone is selected in combo.
     */
    public void currentZoneChanged() {
        Zone selectedValue = (Zone) inputUI.currentZones.getSelectedItem();
        inputUI.currentZoneMap.setSelectedCells(selectedValue.getCell());

        List<Cell> newCells = zoneMap.get(selectedValue);
        inputUI.newZoneMap.setSelectedCells(newCells);
        updateZoneCellDetails();
    }

    protected void updateZoneCellDetails() {
        Zone selectedValue = (Zone) inputUI.currentZones.getSelectedItem();
        if (selectedValue != null) {
            List<Cell> currentCells = selectedValue.getCell();
            inputUI.currentZoneCells.setText(cellToString(currentCells));

            List<Cell> newCells = zoneMap.get(selectedValue);
            inputUI.newZoneCells.setText(cellToString(newCells));
        }
    }

    protected String cellToString(List<Cell> cells) {
        List<String> strings = new ArrayList<>();
        for (Cell cell : cells) {
            strings.add(cell.getName());
        }
        return StringUtils.join(strings, ", ");
    }

    /**
     * Action when a port is selected in combo.
     */
    public void currentPortChanged() {
        Port selectedValue = (Port) inputUI.currentPorts.getSelectedItem();
        inputUI.currentPortMap.setSelectedCells(selectedValue.getCell());

        Cell newPort = portMap.get(selectedValue);
        inputUI.newPortMap.setSelectedCells(newPort);
        updatePortCellDetails();
    }

    protected void updatePortCellDetails() {
        Port selectedValue = (Port) inputUI.currentPorts.getSelectedItem();
        if (selectedValue != null) {
            Cell cell = selectedValue.getCell();
            if (cell != null) {
                inputUI.currentPortCell.setText(cell.getName());
            }

            Cell newPort = portMap.get(selectedValue);
            if (newPort != null) {
                inputUI.newPortCell.setText(newPort.getName());
            }
        }
    }

    /**
     * Ask fro user to perform database change and commit.
     */
    public void valid() {
        int response = JOptionPane.showConfirmDialog(inputUI, t("isisfish.changeSpatial.confirmvalidmessage"),
                t("isisfish.changeSpatial.confirmvalidtitle"), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (response == JOptionPane.YES_OPTION) {
            swapPortAndZoneCells();
            oldFisheryRegion.getTopiaContext().commitTransaction();

            InputSaveVerifier inputSaveVerifier = inputUI.getContextValue(InputSaveVerifier.class);
            inputSaveVerifier.cancel();

            InputUI parentInputUI = inputUI.getContextValue(InputUI.class, JAXXUtil.PARENT);

            parentInputUI.getHandler().loadRegion(oldFisheryRegion.getName());
            close();
        }
    }

    /**
     * Cancel and close UI.
     */
    public void close() {
        inputUI.dispose();
    }

    /**
     * Perform swap in database between current cell and new cell for zone and region.
     */
    protected void swapPortAndZoneCells() {
        CellDAO cellDAO = IsisFishDAOHelper.getCellDAO(oldFisheryRegion.getTopiaContext());

        // update region spatial
        oldFisheryRegion.setCellLengthLatitude(newFisheryRegion.getCellLengthLatitude());
        oldFisheryRegion.setCellLengthLongitude(newFisheryRegion.getCellLengthLongitude());
        oldFisheryRegion.setMaxLatitude(newFisheryRegion.getMaxLatitude());
        oldFisheryRegion.setMaxLongitude(newFisheryRegion.getMaxLongitude());
        oldFisheryRegion.setMinLatitude(newFisheryRegion.getMinLatitude());
        oldFisheryRegion.setMinLongitude(newFisheryRegion.getMinLongitude());
        oldFisheryRegion.update();

        // delete old cells
        List<Cell> currentCells = oldFisheryRegion.getCell();
        cellDAO.deleteAll(currentCells);

        // create new cells
        for (Cell cell : cells) {
            cellDAO.create(cell);
        }

        // update current zones
        for (Map.Entry<Zone, List<Cell>> zoneListEntry : zoneMap.entrySet()) {
            Zone zone = zoneListEntry.getKey();
            List<Cell> newCell = zoneListEntry.getValue();

            zone.clearCell();
            zone.addAllCell(newCell);
        }

        // update current ports
        for (Map.Entry<Port, Cell> portCellEntry : portMap.entrySet()) {
            Port port = portCellEntry.getKey();
            Cell newCell = portCellEntry.getValue();
            port.setCell(newCell);
        }
    }
}
