/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2010 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.check;

import javax.swing.table.DefaultTableCellRenderer;
import java.awt.Component;
import javax.swing.JTable;
import java.awt.Color;

/**
 * CheckResultTableRenderer.java
 *
 * Created: 10 janv. 2004
 *
 * @author Benjamin Poussin &lt;poussin@codelutin.com&gt;
 * @version $Revision$
 *
 * Mise a jour: $Date$
 * par : $Author$
 */
public class CheckResultTableRenderer extends DefaultTableCellRenderer { // CheckResultTableRenderer
    /** serialVersionUID. */
    private static final long serialVersionUID = 8015604773747460458L;

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value,
            boolean isSelected, boolean hasFocus, int row, int column) {
        super.getTableCellRendererComponent(table, value, isSelected, hasFocus,
                row, column);
        if (!isSelected) {
            if ("error".equals(table.getValueAt(row, 2))) {
                setBackground(Color.red);
            } else if ("warning".equals(table.getValueAt(row, 2))) {
                setBackground(Color.orange);
            } else if ("info".equals(table.getValueAt(row, 2))) {
                setBackground(Color.cyan);
            }
        }
        return this;
    }
} // CheckResultTableRenderer
