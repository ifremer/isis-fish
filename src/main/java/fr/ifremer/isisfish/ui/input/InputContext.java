/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.ui.input;

import fr.ifremer.isisfish.datastore.RegionStorage;
import fr.ifremer.isisfish.ui.WelcomeSaveVerifier;
import jaxx.runtime.JAXXContext;
import jaxx.runtime.context.DefaultJAXXContext;
import org.nuiton.topia.TopiaContext;

/**
 * Ce contexte regroupe les élements qui servent à une hierachie d'interfaces Input.
 */
public class InputContext extends DefaultJAXXContext {

    static RegionStorage storage;
    static TopiaContext db;

    /**
     * return RegionStorage currently used in input interface.
     * @return
     */
    public static RegionStorage getStorage() {
        return storage;
    }

    /**
     * return TopiaContext currently used in input interface.
     * need to load matrix with IsisMatrixSemanticMapper
     * @return
     */
    public static TopiaContext getDb() {
        return db;
    }

    public static void setDb(TopiaContext db) {
        InputContext.db = db;
    }

    public static void setStorage(RegionStorage storage) {
        InputContext.storage = storage;
    }

    public InputContext(JAXXContext parent) {
        setParentContext(parent);

        // add save verifier for this hierarchy
        InputSaveVerifier inputSaveVerifier = new InputSaveVerifier();
        setContextValue(inputSaveVerifier);
        
        // this verifier is linked to global verifier
        WelcomeSaveVerifier welcomeSaveVerifier = getContextValue(WelcomeSaveVerifier.class);
        welcomeSaveVerifier.addSaveVerifier(inputSaveVerifier);
    }
}
