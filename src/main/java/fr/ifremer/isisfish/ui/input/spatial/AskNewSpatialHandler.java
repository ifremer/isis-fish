/*
 * #%L
 * ISIS-Fish
 * %%
 * Copyright (C) 2017 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.ui.input.spatial;

import fr.ifremer.isisfish.entities.Cell;
import fr.ifremer.isisfish.entities.CellImpl;
import fr.ifremer.isisfish.entities.FisheryRegion;
import fr.ifremer.isisfish.entities.Port;
import fr.ifremer.isisfish.entities.Zone;
import fr.ifremer.isisfish.util.CellPointcomparator;
import org.nuiton.topia.persistence.TopiaId;

import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Handler for new fishery region resolution dialog.
 */
public class AskNewSpatialHandler {

    protected AskNewSpatialUI ui;

    public AskNewSpatialHandler(AskNewSpatialUI ui) {
        this.ui = ui;
    }

    public void performSpatialChange() {
        FisheryRegion currentFisheryRegion = ui.getContextValue(FisheryRegion.class);

        // compute new cells
        FisheryRegion newFisheryRegion = this.ui.getBean();
        List<Cell> newCells = createNewCells(newFisheryRegion, currentFisheryRegion);

        // compute new zones
        Map<Zone, List<Cell>> zoneMap = createNewZones(newCells, newFisheryRegion, currentFisheryRegion);
        Map<Port, Cell> portMap = createNewPorts(newCells, newFisheryRegion, currentFisheryRegion);

        // display next view
        ChangeSpatialPreviewUI changeSpatialPreviewUI = new ChangeSpatialPreviewUI(this.ui);
        changeSpatialPreviewUI.getHandler().setOldFisheryRegion(currentFisheryRegion);
        changeSpatialPreviewUI.getHandler().setNewFisheryRegion(newFisheryRegion);
        changeSpatialPreviewUI.getHandler().setCells(newCells);
        changeSpatialPreviewUI.getHandler().setZoneMap(zoneMap);
        changeSpatialPreviewUI.getHandler().setPortMap(portMap);
        changeSpatialPreviewUI.getHandler().completeSetup();
        changeSpatialPreviewUI.pack();
        changeSpatialPreviewUI.setLocationRelativeTo(this.ui);
        changeSpatialPreviewUI.setVisible(true);

        this.ui.dispose();
    }

    /**
     * Create new cells for new resolution and use old region {@code isLand()} info.
     *
     * @param newFisheryRegion new fichery region
     * @param currentFisheryRegion old fishery region (for matching)
     * @return new cells
     */
    protected List<Cell> createNewCells(FisheryRegion newFisheryRegion, FisheryRegion currentFisheryRegion) {
        List<Cell> currentCells = currentFisheryRegion.getCell();

        CellPointcomparator cellPointcomparator = new CellPointcomparator();
        currentCells.sort(cellPointcomparator);
        List<Cell> cells = new ArrayList<>();

        Rectangle2D.Float intersect = new Rectangle2D.Float();

        for (float lati = newFisheryRegion.getMinLatitude(); lati < newFisheryRegion.getMaxLatitude();
             lati += newFisheryRegion.getCellLengthLatitude()) {
            lati = Math.round(lati * 1000f);
            lati = lati / 1000.0f;
            for (float longi = newFisheryRegion.getMinLongitude(); longi < newFisheryRegion.getMaxLongitude();
                 longi += newFisheryRegion.getCellLengthLongitude()) {
                longi = Math.round(longi * 1000f) / 1000.0f;

                // n'existe pas on la cree
                Cell cell = new CellImpl();
                cell.setTopiaId(TopiaId.create(Cell.class));
                cell.setName("La" + lati + "Lo" + longi);
                cell.setLatitude(lati);
                cell.setLongitude(longi);
                cell.setLand(false);
                cells.add(cell);

                Rectangle2D.Float cellRect = new Rectangle2D.Float(cell.getLatitude(), cell.getLongitude(),
                        newFisheryRegion.getCellLengthLatitude(), newFisheryRegion.getCellLengthLongitude());
                for (Cell currentCell : currentCells) {
                    Rectangle2D.Float curCellRect = new Rectangle2D.Float(currentCell.getLatitude(),
                            currentCell.getLongitude(), currentFisheryRegion.getCellLengthLatitude(),
                            currentFisheryRegion.getCellLengthLongitude());
                    double overlap = intersectPercentage(cellRect, curCellRect, intersect);

                    if (overlap >= 0.5) {
                        cell.setLand(currentCell.isLand());
                    }
                }
            }
        }
        return cells;
    }

    /**
     * Create new zones filled with cells from new region.
     *
     * @param newCells new cells
     * @param newFisheryRegion new region
     * @param currentFisheryRegion old region
     * @return map between current zone and new cell for this zone
     */
    protected Map<Zone, List<Cell>> createNewZones(List<Cell> newCells, FisheryRegion newFisheryRegion, FisheryRegion currentFisheryRegion) {
        Map<Zone, List<Cell>> zonesAndNewCells = new HashMap<>();
        List<Zone> zones = currentFisheryRegion.getZone();
        Rectangle2D.Float intersect = new Rectangle2D.Float();

        for (Zone zone : zones) {
            Set<Cell> zoneNewCells = new HashSet<>();
            for (Cell newCell : newCells) {
                Rectangle2D.Float newCellRect = new Rectangle2D.Float(newCell.getLatitude(), newCell.getLongitude(),
                        newFisheryRegion.getCellLengthLatitude(), newFisheryRegion.getCellLengthLongitude());
                for (Cell currentCell : zone.getCell()) {
                    Rectangle2D.Float currentCellRect = new Rectangle2D.Float(currentCell.getLatitude(), currentCell.getLongitude(),
                            currentFisheryRegion.getCellLengthLatitude(), currentFisheryRegion.getCellLengthLongitude());
                    double overlap = intersectPercentage(newCellRect, currentCellRect, intersect);
                    if (overlap >= 0.5) {
                        zoneNewCells.add(newCell);
                    }
                }
            }
            zonesAndNewCells.put(zone, new ArrayList<>(zoneNewCells));
        }

        return zonesAndNewCells;
    }

    /**
     * Create new port filled with cell from new region.
     *
     * @param newCells new cells
     * @param newFisheryRegion new region
     * @param currentFisheryRegion old region
     * @return map between current ports and new cell for this zone
     */
    protected Map<Port, Cell> createNewPorts(List<Cell> newCells, FisheryRegion newFisheryRegion, FisheryRegion currentFisheryRegion) {
        Map<Port, Cell> portsAndNewCells = new HashMap<>();
        List<Port> ports = currentFisheryRegion.getPort();
        Rectangle2D.Float intersect = new Rectangle2D.Float();

        for (Port port : ports) {

            Cell currentCell = port.getCell();
            if (currentCell != null) {
                Cell portNewCell = null;
                double bestMatch = Double.MIN_VALUE;

                Rectangle2D.Float currentCellRect = new Rectangle2D.Float(currentCell.getLatitude(), currentCell.getLongitude(),
                        currentFisheryRegion.getCellLengthLatitude(), currentFisheryRegion.getCellLengthLongitude());
                for (Cell newCell : newCells) {
                    Rectangle2D.Float newCellRect = new Rectangle2D.Float(newCell.getLatitude(), newCell.getLongitude(),
                            newFisheryRegion.getCellLengthLatitude(), newFisheryRegion.getCellLengthLongitude());
                    double overlap = intersectPercentage(newCellRect, currentCellRect, intersect);
                    if (overlap >= bestMatch) {
                        portNewCell = newCell;
                        bestMatch = overlap;
                    }
                }

                portsAndNewCells.put(port, portNewCell);
            }

        }

        return portsAndNewCells;
    }

    /**
     * Intersection percentage between two rectangle (comparative to the min area of the two rectangle).
     *
     * @param r1 first rect
     * @param r2 second rect
     * @param intersect computed intersection
     * @return intersection percentage
     */
    protected double intersectPercentage(Rectangle2D.Float r1, Rectangle2D.Float r2, Rectangle2D.Float intersect) {
        Rectangle2D.intersect(r1, r2, intersect);
        double result = 0;
        double fr1 = r1.getWidth() * r1.getHeight(); // area of "r1"
        double fr2 = r2.getWidth() * r2.getHeight(); // area of "r2"
        double fr = Math.min(fr1, fr2);
        if (fr > 0 && intersect.getWidth() > 0 && intersect.getHeight() > 0) {
            double f = intersect.getWidth() * intersect.getHeight();  // area of "r" - overlap
            result = f / fr; // overlap percentage
        }
        return result;
    }
}
