/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.strategy;

import fr.ifremer.isisfish.entities.SetOfVessels;
import fr.ifremer.isisfish.ui.input.InputContentHandler;
import fr.ifremer.isisfish.ui.models.common.GenericComboModel;

/**
 * Strategy tab handler.
 */
public class StrategyTabHandler extends InputContentHandler<StrategyTabUI> {

    protected boolean init;

    protected StrategyTabHandler(StrategyTabUI inputContentUI) {
        super(inputContentUI);
    }

    protected void afterInit() {
        inputContentUI.addPropertyChangeListener(StrategyTabUI.PROPERTY_BEAN, evt -> {
            if (evt.getNewValue() == null) {
                inputContentUI.fieldStrategyName.setText("");
                inputContentUI.fieldStrategyProportionSetOfVessels.setText("");
                inputContentUI.fieldStrategyComment.setText("");
            }
            if (evt.getNewValue() != null) {
                init = true;
                setSetOfVesselsModel();
                init = false;
            }
        });
    }
    
    protected void setSetOfVesselsModel() {
        GenericComboModel<SetOfVessels> modelVessel = new GenericComboModel<>(inputContentUI.getFisheryRegion().getSetOfVessels());
        inputContentUI.fieldStrategySetOfVessels.setModel(modelVessel);
        inputContentUI.fieldStrategySetOfVessels.setSelectedItem(inputContentUI.getBean().getSetOfVessels());
    }

    protected void setOfVesselsChanged() {
        if (!init) {
            inputContentUI.getBean().setSetOfVessels((SetOfVessels)inputContentUI.fieldStrategySetOfVessels.getSelectedItem());
        }
    }
}
