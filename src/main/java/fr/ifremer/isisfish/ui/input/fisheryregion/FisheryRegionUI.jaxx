<!--
  #%L
  IsisFish
  
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2009 - 2020 Ifremer, Code Lutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->
<fr.ifremer.isisfish.ui.input.InputContentUI superGenericType='FisheryRegion'>

    <!-- bean property -->
    <fr.ifremer.isisfish.entities.FisheryRegion id='bean' javaBean='null'/>

    <FisheryRegionHandler id="handler" constructorParams="this" />

    <BeanValidator id='validator'
        bean='{getBean()}' beanClass='fr.ifremer.isisfish.entities.FisheryRegion'
        uiClass="jaxx.runtime.validator.swing.ui.ImageValidationUI">
        <field name="name" component="fieldRegion" />
    </BeanValidator>

    <script><![CDATA[
        protected void $afterCompleteSetup() {
            handler.afterInit();
        }
    ]]></script>

    <JPanel id="body">
        <JSplitPane constraints='BorderLayout.CENTER' oneTouchExpandable="true" dividerLocation="200" orientation="HORIZONTAL">
            <Table>
                <row>
                    <cell columns='3'>
                        <JLabel text="isisfish.fisheryRegion.name"/>
                    </cell>
                </row>
                <row>
                    <cell columns='3' fill='horizontal' weightx='1.0'>
                        <JTextField id="fieldRegion" decorator='boxed'
                         text='{jaxx.runtime.SwingUtil.getStringValue(getBean().getName())}'
                         onKeyReleased='getBean().setName(fieldRegion.getText())'/>
                    </cell>
                </row>
                <row>
                    <cell columns='3'>
                        <JLabel text="isisfish.fisheryRegion.area"/>
                    </cell>
                </row>
                <row>
                    <cell>
                        <JLabel text="isisfish.fisheryRegion.latitude.min"/>
                    </cell>
                    <cell columns='2' fill='horizontal' weightx='1.0'>
                        <!-- NumberEditor id='fieldLatMin' constructorParams='this'
                                      bean='{getBean()}' property='minLatitude'
                                      decorator='boxed' useSign='true'/-->
                        <JTextField id="fieldLatMin" text='{String.valueOf(getBean().getMinLatitude())}' decorator='boxed'
                            onKeyReleased='getBean().setMinLatitude(Float.parseFloat(fieldLatMin.getText()))'/>
                    </cell>
                </row>
                <row>
                    <cell>
                        <JLabel text="isisfish.fisheryRegion.latitude.max"/>
                    </cell>
                    <cell columns='2' fill='horizontal' weightx='1.0'>
                        <!-- NumberEditor id='fieldLatMax' constructorParams='this'
                                      bean='{getBean()}' property='maxLatitude'
                                      decorator='boxed' useSign='true'/-->
                        <JTextField id="fieldLatMax" text='{String.valueOf(getBean().getMaxLatitude())}' decorator='boxed'
                            onKeyReleased='getBean().setMaxLatitude(Float.parseFloat(fieldLatMax.getText()))'/>
                    </cell>
                </row>
                <row>
                    <cell>
                        <JLabel text="isisfish.fisheryRegion.longitude.min"/>
                    </cell>
                    <cell columns='2' fill='horizontal' weightx='1.0'>
                        <!-- NumberEditor id='fieldLongMin' constructorParams='this'
                                      bean='{getBean()}' property='minLongitude'
                                      decorator='boxed' useSign='true'/-->
                        <JTextField id="fieldLongMin" text='{String.valueOf(getBean().getMinLongitude())}' decorator='boxed'
                            onKeyReleased='getBean().setMinLongitude(Float.parseFloat(fieldLongMin.getText()))'/>
                    </cell>
                </row>
                <row>
                    <cell>
                        <JLabel text="isisfish.fisheryRegion.longitude.max"/>
                    </cell>
                    <cell columns='2' fill='horizontal' weightx='1.0'>
                        <!-- NumberEditor id='fieldLongMax' constructorParams='this'
                                      bean='{getBean()}' property='maxLongitude'
                                      decorator='boxed' useSign='true'/-->
                        <JTextField id="fieldLongMax" text='{String.valueOf(getBean().getMaxLongitude())}' decorator='boxed'
                            onKeyReleased='getBean().setMaxLongitude(Float.parseFloat(fieldLongMax.getText()))'/>
                    </cell>
                </row>
                <row>
                    <cell columns='3'>
                         <JLabel text="isisfish.fisheryRegion.spatial"/>
                    </cell>
                </row>
                <row>
                    <cell>
                        <JLabel text="isisfish.fisheryRegion.latitude"/>
                    </cell>
                    <cell columns='2' fill='horizontal' weightx='1.0'>
                        <!-- NumberEditor id='fieldCellLengthLatitude' constructorParams='this'
                                      bean='{getBean()}' property='cellLengthLatitude'
                                      decorator='boxed' useSign='true'/-->
                        <JTextField id="fieldCellLengthLatitude" text='{String.valueOf(getBean().getCellLengthLatitude())}' decorator='boxed'
                            onKeyReleased='getBean().setCellLengthLatitude(Float.parseFloat(fieldCellLengthLatitude.getText()))'/>
                    </cell>
                </row>
                <row>
                    <cell>
                        <JLabel text="isisfish.fisheryRegion.longitude"/>
                    </cell>
                    <cell columns='2' fill='horizontal' weightx='1.0'>
                        <!-- NumberEditor id='fieldCellLengthLongitude' constructorParams='this'
                                      bean='{getBean()}' property='cellLengthLongitude'
                                      decorator='boxed' useSign='true'/-->
                        <JTextField id="fieldCellLengthLongitude" text='{String.valueOf(getBean().getCellLengthLongitude())}' decorator='boxed'
                            onKeyReleased='getBean().setCellLengthLongitude(Float.parseFloat(fieldCellLengthLongitude.getText()))'/>
                    </cell>
                </row>
                <row>
                    <cell columns='3' fill='both' weightx='1.0' weighty='0.6'>
                        <JScrollPane>
                            <JList id="fieldMapfiles" genericType="String"
                                decorator='boxed'/>
                        </JScrollPane>
                    </cell>
                </row>
                <row>
                    <cell columns='3' fill='horizontal' weightx='1.0'>
                        <Table>
                            <row>
                                <cell fill='horizontal' weightx='0.5'>
                                    <JButton id="buttonAddMap" icon="fatcow/map_add.png" text="isisfish.common.add"
                                        toolTipText="isisfish.fisheryRegion.addMap" onActionPerformed='handler.addMap()' decorator='boxed'/>
                                </cell>
                                <cell fill='horizontal' weightx='0.5'>
                                     <JButton id="buttonRemoveMap" icon="fatcow/map_delete.png" text="isisfish.common.remove"
                                        toolTipText="isisfish.fisheryRegion.delMap" onActionPerformed='handler.delMap()' decorator='boxed'/>
                                </cell>
                            </row>
                        </Table>
                    </cell>
                </row>
                <row>
                    <cell columns='3'>
                        <JLabel text="isisfish.fisheryRegion.comments"/>
                    </cell>
                </row>
                <row>
                    <cell columns='3' fill='both' weightx='1.0' weighty='0.4'>
                        <JScrollPane>
                            <JTextArea id="fieldComment" text='{jaxx.runtime.SwingUtil.getStringValue(getBean().getComment())}'  decorator='boxed'
                            onKeyReleased='getBean().setComment(fieldComment.getText())'/>
                        </JScrollPane>
                    </cell>
                </row>
                <row>
                    <cell columns='3'>
                        <JLabel text="isisfish.fisheryRegion.selectFile"/>
                    </cell>
                </row>
                <row>
                    <cell fill='horizontal' weightx='0.3'>
                        <JButton id='save' decorator='boxed'
                            text="isisfish.common.save" icon="fatcow/diskette.png"
                            enabled="{validator.isValid() &amp;&amp; validator.isChanged()}"
                            onActionPerformed="handler.save();validator.setChanged(false)"/>
                    </cell>
                    <cell fill='horizontal' weightx='0.3'>
                        <JButton id='cancel' decorator='boxed'
                            text="isisfish.common.cancel" icon="fatcow/cancel.png"
                            enabled="{validator.isChanged()}"
                            onActionPerformed="getSaveVerifier().cancel()"/>
                    </cell>
                    <cell fill='horizontal' weightx='0.3'>
                        <JButton id='check' text="isisfish.common.check" onActionPerformed='handler.check()' decorator='boxed'/>
                    </cell>
                </row>
            </Table>
            <JPanel id='map' layout='{new BorderLayout()}'>
                <fr.ifremer.isisfish.map.IsisMapBean id='cellMap'
                    selectionMode="{fr.ifremer.isisfish.map.CellSelectionLayer.NO_SELECTION}"
                    javaBean='new fr.ifremer.isisfish.map.IsisMapBean()'
                    fisheryRegion='{getBean()}' constraints='BorderLayout.CENTER' decorator='boxed'/>
                <com.bbn.openmap.InformationDelegator id="cellMapInfo" constraints='BorderLayout.SOUTH' />
            </JPanel>
        </JSplitPane>
    </JPanel>
</fr.ifremer.isisfish.ui.input.InputContentUI>
