/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 - 2019 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.setofvessels;

import static org.nuiton.i18n.I18n.t;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.ifremer.isisfish.IsisFishDAOHelper;
import fr.ifremer.isisfish.entities.EffortDescription;
import fr.ifremer.isisfish.entities.EffortDescriptionDAO;
import fr.ifremer.isisfish.entities.Metier;
import fr.ifremer.isisfish.entities.SetOfVessels;
import fr.ifremer.isisfish.ui.input.InputContentHandler;
import fr.ifremer.isisfish.util.ErrorHelper;

/**
 * set of vessels handler.
 */
public class EffortDescriptionHandler extends InputContentHandler<EffortDescriptionUI> {

    /** Class logger. */
    private static final Log log = LogFactory.getLog(EffortDescriptionHandler.class);

    protected EffortDescriptionHandler(EffortDescriptionUI inputContentUI) {
        super(inputContentUI);
    }

    protected void afterInit() {
        inputContentUI.addPropertyChangeListener(EffortDescriptionUI.PROPERTY_BEAN, evt -> {
            if (evt.getNewValue() == null) {
                inputContentUI.fieldEffortDescriptionMetierModel.setElementList(null);
                inputContentUI.fieldEffortDescriptionEffortDescriptionModel.setElementList(null);
            }
            if (evt.getNewValue() != null) {
                setEffortDescriptionEffortDescriptionModel();
            }
        });
    }

    protected void onFieldEffortDescriptionMetierListValueChanged() {
        // active le bouton seulement si l'interface est active
        // dans le cas de sensitivity par exemple
        if (inputContentUI.isActive()) {
            inputContentUI.buttonEffortDescriptionAdd.setEnabled(inputContentUI.fieldEffortDescriptionMetierList.getSelectedIndex() != -1);
        }
    }

    protected void onFieldEffortDescriptionEffortDescriptionListValueChanged() {
        // active le bouton seulement si l'interface est active
        // dans le cas de sensitivity par exemple
        if (inputContentUI.isActive()) {
            inputContentUI.removeEffortDescriptionButton.setEnabled(inputContentUI.fieldEffortDescriptionEffortDescriptionList.getSelectedIndex() != -1);
        }
    }

    protected void setEffortDescriptionEffortDescriptionModel() {
        if (inputContentUI.getBean() != null && inputContentUI.getBean().getPossibleMetiers() != null) {
            List<Metier> availableMetiers = inputContentUI.getFisheryRegion().getMetier();

            List<EffortDescription> effortDescriptions = new ArrayList<>(inputContentUI.getBean().getPossibleMetiers());
            effortDescriptions.stream().map(EffortDescription::getPossibleMetiers).forEach(availableMetiers::remove);
            inputContentUI.fieldEffortDescriptionEffortDescriptionModel.setElementList(effortDescriptions);

            inputContentUI.fieldEffortDescriptionMetierModel.setElementList(availableMetiers);
        }
    }

    protected void addEffortDescriptions(MouseEvent event) {
        // action performed ou 'double click'
        if (event != null && event.getClickCount() != 2) {
            return;
        }

        List<Metier> selectedValues = inputContentUI.fieldEffortDescriptionMetierList.getSelectedValuesList();
        for (Metier selectedMetier : selectedValues) {
            addEffortDescription(inputContentUI.getBean(), selectedMetier);
        }
        setEffortDescriptionEffortDescriptionModel();
    }

    protected void removeEffortDescriptions(MouseEvent event) {
        // action performed ou 'double click'
        if (event != null && event.getClickCount() != 2) {
            return;
        }

        List<EffortDescription> selectedValues = inputContentUI.fieldEffortDescriptionEffortDescriptionList.getSelectedValuesList();
        for (EffortDescription selectedEffortDescription : selectedValues) {
            removeEffortDescription(inputContentUI.getBean(), selectedEffortDescription);
        }
        setEffortDescriptionEffortDescriptionModel();
    }

    protected void addEffortDescription(SetOfVessels setOfVessels, Metier metier) {
        if (log.isDebugEnabled()) {
            log.debug("addEffortDescription called: " + setOfVessels + " metier: " + metier);
        }
        try {
            EffortDescriptionDAO effortDescriptionPS = IsisFishDAOHelper.getEffortDescriptionDAO(metier.getTopiaContext());
            EffortDescription effortDescription = effortDescriptionPS.create();
            // EC20090715 : c'est la classe d'association
            // qui en étant sauvee, sauve les relations en base
            // il faut bien faire les set des deux cotes
            effortDescription.setSetOfVessels(setOfVessels);
            effortDescription.setPossibleMetiers(metier);
            effortDescription.update();
            setOfVessels.addPossibleMetiers(effortDescription);
            setOfVessels.update();
        } catch (Exception eee) {
            if (log.isErrorEnabled()) {
                log.error("Can't create EffortDescription", eee);
            }
            ErrorHelper.showErrorDialog(t("isisfish.error.input.addentity", "EffortDescription"), eee);
        }
    }

    protected void removeEffortDescription(SetOfVessels sov, EffortDescription effort) {
        if (log.isTraceEnabled()) {
            log.trace("removeEffortDescription called");
        }
        try {
            sov.removePossibleMetiers(effort);
            // EC-20091112 : commit() twice cause hibernate error:
            // Found two representations of same collection:
            //sov.getTopiaContext().commitTransaction();
        } catch (Exception eee) {
            if (log.isErrorEnabled()) {
                log.error("Can't remove entity: " + effort, eee);
            }
            ErrorHelper.showErrorDialog(t("isisfish.error.input.removeentity", "EffortDescription"), eee);
        }
    }
}
