/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.port;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

import java.awt.event.MouseEvent;

import com.bbn.openmap.event.SelectMouseMode;

import fr.ifremer.isisfish.entities.Cell;
import fr.ifremer.isisfish.entities.FisheryRegion;
import fr.ifremer.isisfish.map.CellSelectionLayer;
import fr.ifremer.isisfish.map.OpenMapEvents;
import fr.ifremer.isisfish.ui.input.InputContentHandler;
import fr.ifremer.isisfish.ui.models.common.GenericListModel;

/**
 * Port handler.
 */
public class PortHandler extends InputContentHandler<PortUI> {

    protected boolean init = false;

    protected PortHandler(PortUI inputContentUI) {
        super(inputContentUI);
    }

    protected void afterInit() {
        setupGotoNextPath(t("isisfish.input.continueSpecies"), n("isisfish.input.tree.species"));

        new OpenMapEvents(inputContentUI.portMap, new SelectMouseMode(false), CellSelectionLayer.SINGLE_SELECTION) {
            @Override
            public boolean mouseClicked(MouseEvent e) {
                if (inputContentUI.getBean() != null) { // impossible de desactiver la carte :(
                    for (Cell c : inputContentUI.portMap.getSelectedCells()) {
                        if (inputContentUI.getBean().getCell() != null) {
                            if (!inputContentUI.getBean().getCell().getTopiaId().equals(c.getTopiaId())){
                                inputContentUI.portCell.setSelectedValue(c, true);
                                return true;
                            }
                        } else {
                            inputContentUI.portCell.setSelectedValue(c, true);
                            return true;
                        }
                    }
                }
                return true;
            }
        };

        inputContentUI.addPropertyChangeListener(PortUI.PROPERTY_BEAN, evt -> {
            if (evt.getNewValue() == null) {
                inputContentUI.fieldPortName.setText("");
                inputContentUI.fieldPortComment.setText("");
            }
            if (evt.getNewValue() != null) {

            }
            fillCellList();
            // hack because fishery region is not binded
            inputContentUI.getPortMap().setFisheryRegion(inputContentUI.getContextValue(FisheryRegion.class));
        });
    }
    
    protected void fillCellList() {
        GenericListModel<Cell> cellModel = new GenericListModel<>();
        if (inputContentUI.getBean() != null) {
            init = true;
            cellModel.setElementList(inputContentUI.getFisheryRegion().getCell());
            inputContentUI.portCell.setModel(cellModel);
            inputContentUI.portCell.setSelectedValue(inputContentUI.getBean().getCell(), true);
            init = false;
        } else {
            inputContentUI.portCell.setModel(cellModel);
        }
    }

    protected void portChanged() {
        if (inputContentUI.getBean() != null && !init) {
            inputContentUI.getBean().setCell(inputContentUI.portCell.getSelectedValue());
        }
    }
}
