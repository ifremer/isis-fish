/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.variable;

import static org.nuiton.i18n.I18n.t;

import java.awt.CardLayout;
import java.util.List;

import javax.swing.JList;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.persistence.TopiaEntity;

import fr.ifremer.isisfish.IsisFishDAOHelper;
import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.entities.Variable;
import fr.ifremer.isisfish.entities.VariableDAO;
import fr.ifremer.isisfish.entities.VariableType;
import fr.ifremer.isisfish.ui.input.InputContentHandler;
import fr.ifremer.isisfish.ui.models.common.GenericListModel;

/**
 * Handler for generic variable ui.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class EntityVariableHandler extends InputContentHandler<EntityVariableUI> {

    /** Class logger. */
    private static final Log log = LogFactory.getLog(EntityVariableHandler.class);

    protected EntityVariableHandler(EntityVariableUI inputContentUI) {
        super(inputContentUI);
    }

    protected void afterInit() {
        inputContentUI.addPropertyChangeListener(EntityVariableUI.PROPERTY_BEAN, evt -> reloadVariables());

        inputContentUI.addPropertyChangeListener(EntityVariableUI.PROPERTY_VARIABLE, evt -> {
            if (evt.getNewValue() == null) {
                inputContentUI.matrixPanel.setMatrix(null);
            }
        });
    }

    /**
     * Init view with currently entity variables.
     */
    protected void reloadVariables() {

        // clear previous
        inputContentUI.getVariablesList().clearSelection();

        // fill current list
        List<Variable> variables = null;
        GenericListModel<Variable> model = (GenericListModel<Variable>)inputContentUI.getVariablesList().getModel();
        TopiaEntity bean = inputContentUI.getBean();
        if (bean != null) {
            try {
                TopiaContext context = inputContentUI.getTopiaContext();
                VariableDAO dao = IsisFishDAOHelper.getVariableDAO(context);
                variables = dao.findAllByEntityId(bean.getTopiaId());
            } catch (TopiaException ex) {
                throw new IsisFishRuntimeException("Can't init view", ex);
            }
            
            // try to set entity name in label
            try {
                String name = BeanUtils.getProperty(inputContentUI.getBean(), "name");
                inputContentUI.getVariableEntityName().setText(name);
            } catch (Exception ex) {
                if (log.isWarnEnabled()) {
                    log.warn("Can't get entity name", ex);
                }
            }
        } else {
            inputContentUI.getVariableEntityName().setText("");
        }
        
        model.setElementList(variables);
    }

    /**
     * Add new variable into list.
     * 
     * @param view view
     */
    public void addNewVariable(EntityVariableUI view) {
        try {
            TopiaContext context = view.getTopiaContext();
            VariableDAO dao = IsisFishDAOHelper.getVariableDAO(context);

            Variable variable = dao.create(
                    Variable.PROPERTY_ENTITY_ID, view.getBean().getTopiaId(),
                    Variable.PROPERTY_NAME, t("isisfish.variables.defaultname"));

            GenericListModel<Variable> model = (GenericListModel<Variable>)view.getVariablesList().getModel();
            List<Variable> variables = model.getElementList();
            variables.add(variable);
            model.setElementList(variables);

            // auto select
            view.getVariablesList().setSelectedValue(variable, true);
        } catch (TopiaException ex) {
            throw new IsisFishRuntimeException("Can't add variable", ex);
        }
    }

    /**
     * Delete selected variable.
     * 
     * @param view view
     */
    public void deleteVariable(EntityVariableUI view) {
        JList<Variable> variableList = view.getVariablesList();
        Variable variable = variableList.getSelectedValue();

        try {
            // delete in db
            TopiaContext context = view.getTopiaContext();
            VariableDAO dao = IsisFishDAOHelper.getVariableDAO(context);
            dao.delete(variable);
            context.commitTransaction();
            
            // refresh ui
            view.getVariablesList().clearSelection(); // fix event bug
            GenericListModel<Variable> model = (GenericListModel<Variable>)view.getVariablesList().getModel();
            List<Variable> variables = model.getElementList();
            variables.remove(variable);
            model.setElementList(variables);
        } catch (TopiaException ex) {
            throw new IsisFishRuntimeException("Can't delete variable", ex);
        }
    }

    /**
     * Display selected variable for edition.
     * 
     * @param view view
     */
    public void showSelectedVariable(EntityVariableUI view) {

        JList<Variable> variableList = view.getVariablesList();
        Variable variable = variableList.getSelectedValue();
        view.setVariable(variable);
        view.getSaveVerifier().addCurrentEntity(variable);

    }

    /**
     * Display card layout part associated with selected type.
     * 
     * @param view view
     */
    public void showSelectedType(EntityVariableUI view) {
        CardLayout cardLayout = view.getVariableTypeLayout();
        VariableType type = (VariableType)view.getVariableTypeCombo().getSelectedItem();
        view.getVariable().setType(type);
        switch (type) {
        case DOUBLE:
            cardLayout.show(view.getVariableTypePanel(), "doubletype");
            break;
        case EQUATION:
            cardLayout.show(view.getVariableTypePanel(), "equationtype");
            break;
        case MATRIX:
            cardLayout.show(view.getVariableTypePanel(), "matrixtype");
            break;
        }
    }

    /**
     * Save current edited variable.
     * 
     * @param view view
     */
    public void saveVariable(EntityVariableUI view) {
        JList<Variable> variableList = view.getVariablesList();
        Variable variable = variableList.getSelectedValue();
        variable.setName(view.getVariableNameField().getText().trim());

        VariableType type = (VariableType)view.getVariableTypeCombo().getSelectedItem();

        switch (type) {
        case DOUBLE:
            try {
                double v = Double.parseDouble(view.getVariableDoubleValue().getText().trim());
                variable.setDoubleValue(v);
            } catch (NumberFormatException ex) {
                if (log.isWarnEnabled()) {
                    log.warn("Can't parse double value as double", ex);
                }
            }
            break;
        case MATRIX:
            variable.setMatrixValue(view.getMatrixPanel().getMatrix());
            break;
        } // equation already set by ui component

        view.getSaveVerifier().save();

        // refresh ui (name change)
        GenericListModel<Variable> model = (GenericListModel<Variable>)view.getVariablesList().getModel();
        List<Variable> variables = model.getElementList();
        model.setElementList(variables);
    }
}
