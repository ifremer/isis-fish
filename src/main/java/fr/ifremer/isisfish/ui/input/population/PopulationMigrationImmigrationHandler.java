/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.population;

import static org.nuiton.i18n.I18n.t;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.math.matrix.gui.MatrixPanelEvent;

import fr.ifremer.isisfish.entities.PopulationGroup;
import fr.ifremer.isisfish.entities.PopulationSeasonInfo;
import fr.ifremer.isisfish.entities.Zone;
import fr.ifremer.isisfish.ui.input.InputContentHandler;
import fr.ifremer.isisfish.ui.models.common.GenericComboModel;
import fr.ifremer.isisfish.util.ErrorHelper;

/**
 * Population handler.
 */
public class PopulationMigrationImmigrationHandler extends InputContentHandler<PopulationMigrationImmigrationUI> {

    /** Class logger. */
    private static final Log log = LogFactory.getLog(PopulationMigrationImmigrationHandler.class);

    protected PopulationMigrationImmigrationHandler(PopulationMigrationImmigrationUI inputContentUI) {
        super(inputContentUI);
    }

    protected void afterInit() {

        inputContentUI.addPropertyChangeListener(PopulationMigrationImmigrationUI.PROPERTY_BEAN, evt -> {
            if (evt.getNewValue() == null) {
                inputContentUI.fieldPopulationMigrationImmigrationCoefficient.setText("");
                inputContentUI.populationMigrationImmigrationTable.setMatrix(null);
            }
            if (evt.getNewValue() != null) {

            }
            setFieldPopulationMigrationMigrationGroupChooserModel();
            setFieldPopulationMigrationMigrationArrivalZoneChooserModel();
            setAddButton();
        });
    }

    protected void populationMigrationImmigrationMatrixChanged(MatrixPanelEvent event) {
        if (inputContentUI.getPopInfo() != null) {
            inputContentUI.getPopInfo().setImmigrationMatrix(inputContentUI.populationMigrationImmigrationTable.getMatrix().clone());
        }
    }

    public void init(PopulationSeasonInfo populationSeasonInfo) {
        // add null before, for second to be considered as a changed event
        // otherwize, setBean has no effect
        //setPopInfo(null);
        //setPopInfo(populationSeasonInfo);

        inputContentUI.populationMigrationImmigrationTable.setMatrix(inputContentUI.getPopInfo().getImmigrationMatrix().copy());
    }

    protected void setFieldPopulationMigrationMigrationGroupChooserModel() {
        GenericComboModel<PopulationGroup> groups = new GenericComboModel<>();
        if (inputContentUI.getBean() != null && inputContentUI.getBean().getPopulationGroup() != null) {
            groups.setElementList(inputContentUI.getBean().getPopulationGroup());
        }
        inputContentUI.fieldPopulationMigrationImmigrationGroupChooser.setModel(groups);
    }

    protected void setFieldPopulationMigrationMigrationArrivalZoneChooserModel() {
        GenericComboModel<Zone> zones = new GenericComboModel<>();
        if (inputContentUI.getBean() != null && inputContentUI.getBean().getPopulationZone() != null) {
            //jaxx.runtime.SwingUtil.fillComboBox(fieldPopulationMigrationMigrationArrivalZoneChooser,getBean().getPopulationZone(), null, true);
            zones.setElementList(inputContentUI.getBean().getPopulationZone());
        }
        inputContentUI.fieldPopulationMigrationImmigrationArrivalZoneChooser.setModel(zones);
    }

    protected void add() {
        addImmigration(inputContentUI.getPopInfo(),
                (PopulationGroup)inputContentUI.fieldPopulationMigrationImmigrationGroupChooser.getSelectedItem(),
                (Zone)inputContentUI.fieldPopulationMigrationImmigrationArrivalZoneChooser.getSelectedItem(),
                Double.parseDouble(inputContentUI.fieldPopulationMigrationImmigrationCoefficient.getText()));
        inputContentUI.populationMigrationImmigrationTable.setMatrix(inputContentUI.getPopInfo().getImmigrationMatrix().clone());
    }
    
    public Object addImmigration(PopulationSeasonInfo info,
            PopulationGroup group, Zone arrival, double coeff) {
        if (log.isTraceEnabled()) {
            log.trace("addImmigration called");
        }
        try {
            MatrixND mat = info.getImmigrationMatrix().copy();
            mat.setValue(group, arrival, coeff);
            info.setImmigrationMatrix(mat);

        } catch (Exception eee) {
            if (log.isErrorEnabled()) {
                log.error("Can't add immigration", eee);
            }
            ErrorHelper.showErrorDialog(t("isisfish.error.input.addentity",
                    "Immigration"), eee);
        }
        return null;
    }

    protected void remove() {
        int row = inputContentUI.populationMigrationImmigrationTable.getTable().getSelectedRow();
        if (row != -1) {
            Object group = inputContentUI.populationMigrationImmigrationTable.getTable().getValueAt(row, 0);
            Object departure = inputContentUI.populationMigrationImmigrationTable.getTable().getValueAt(row, 1);

            MatrixND mat = inputContentUI.getPopInfo().getImmigrationMatrix().clone();
            mat.setValue(group, departure, 0);
            inputContentUI.getPopInfo().setImmigrationMatrix(mat);
            inputContentUI.populationMigrationImmigrationTable.setMatrix(inputContentUI.getPopInfo().getImmigrationMatrix().copy());
        }
    }

    protected void setAddButton() {
        inputContentUI.add.setEnabled(inputContentUI.isActive() && inputContentUI.fieldPopulationMigrationImmigrationGroupChooser.getSelectedItem() != null
                && !inputContentUI.fieldPopulationMigrationImmigrationCoefficient.getText().equals("")
                && inputContentUI.fieldPopulationMigrationImmigrationArrivalZoneChooser.getSelectedItem() != null);
    }
}
