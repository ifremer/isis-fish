<!--
  #%L
  IsisFish
  
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2009 - 2020 Ifremer, Code Lutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->
<fr.ifremer.isisfish.ui.input.InputContentUI superGenericType='fr.ifremer.isisfish.entities.Population'>

    <!-- bean property -->
    <fr.ifremer.isisfish.entities.Population id='bean' javaBean='null'/>

    <PopulationHandler id="handler" constructorParams="this" />

    <script><![CDATA[
    protected void $afterCompleteSetup() {
        handler.afterInit();
    }

    @Override
    public void setLayer(boolean active) {
        super.setLayer(active);
        populationBasicsUI.setLayer(active);
        populationZoneUI.setLayer(active);
        populationSeasonsUI.setLayer(active);
        populationEquationUI.setLayer(active);
        populationRecruitementUI.setLayer(active);
        populationGroupUI.setLayer(active);
        populationCapturabilityUI.setLayer(active);
        populationMigrationUI.setLayer(active);
        populationPriceUI.setLayer(active);
        populationStockUI.setLayer(active);
        variablesUI.setLayer(active);
    }

    @Override
    public void resetChangeModel() {
        populationBasicsUI.resetChangeModel();
        populationZoneUI.resetChangeModel();
        populationSeasonsUI.resetChangeModel();
        populationEquationUI.resetChangeModel();
        populationRecruitementUI.resetChangeModel();
        populationGroupUI.resetChangeModel();
        populationCapturabilityUI.resetChangeModel();
        populationMigrationUI.resetChangeModel();
        populationPriceUI.resetChangeModel();
        populationStockUI.resetChangeModel();
        variablesUI.resetChangeModel();
    }
    ]]></script>

    <JPanel id='body'>
        <JTabbedPane id="populationTab">
            <!-- Saisie des populations -->
            <tab title='isisfish.populationBasics.title'>
                <PopulationBasicsUI id='populationBasicsUI' bean="{getBean()}" active="{isActive()}" sensitivity="{isSensitivity()}" constructorParams='this'/>
            </tab>
            <!-- Zones -->
            <tab title='isisfish.populationZones.title'>
                <PopulationZonesUI id='populationZoneUI' bean="{getBean()}" active="{isActive()}" sensitivity="{isSensitivity()}" constructorParams='this'/>
            </tab>
            <!-- Saisons -->
            <tab title='isisfish.populationSeasons.title'>
                <PopulationSeasonsUI id='populationSeasonsUI' bean="{getBean()}" active="{isActive()}" sensitivity="{isSensitivity()}" constructorParams='this'/>
            </tab>
            <!-- Saisie des équations -->
            <tab title='isisfish.populationEquation.title'>
                <PopulationEquationUI id='populationEquationUI' bean="{getBean()}" active="{isActive()}" sensitivity="{isSensitivity()}" constructorParams='this'/>
            </tab>
            <!-- Saisie des reproductions -->
            <tab title='isisfish.populationRecruitment.title'>
                <PopulationRecruitmentUI id='populationRecruitementUI' bean="{getBean()}" active="{isActive()}" sensitivity="{isSensitivity()}" constructorParams='this'/>
            </tab>
            <!-- Saisie des groupes de population -->
            <tab title='isisfish.populationGroup.title'>
                <PopulationGroupUI id='populationGroupUI' bean="{getBean()}" active="{isActive()}" sensitivity="{isSensitivity()}" constructorParams='this'/>
            </tab>
            <!--Capturabilité -->
            <tab title='isisfish.populationCapturability.title'>
                <PopulationCapturabilityUI id='populationCapturabilityUI' bean="{getBean()}" active="{isActive()}" sensitivity="{isSensitivity()}" constructorParams='this'/>
            </tab>
            <!-- Migration -->
            <tab title='isisfish.populationMigration.title'>
                <PopulationMigrationUI id='populationMigrationUI' bean="{getBean()}" active="{isActive()}" sensitivity="{isSensitivity()}" constructorParams='this'/>
            </tab>
            <!-- Price -->
            <tab title='isisfish.populationPrice.title'>
                <PopulationPriceUI id='populationPriceUI' bean="{getBean()}" active="{isActive()}" sensitivity="{isSensitivity()}" constructorParams='this'/>
            </tab>
            <!-- Stcck -->
            <tab title='isisfish.populationStock.title'>
                <PopulationStockUI id='populationStockUI' bean="{getBean()}" active="{isActive()}" sensitivity="{isSensitivity()}" constructorParams='this'/>
            </tab>
            <tab title='isisfish.variables.tabtitle'>
                <fr.ifremer.isisfish.ui.input.variable.EntityVariableUI id="variablesUI"
                    bean="{getBean()}" active="{isActive()}"
                    sensitivity="{isSensitivity()}" constructorParams='this'/>
            </tab>
        </JTabbedPane>
    </JPanel>
</fr.ifremer.isisfish.ui.input.InputContentUI>
