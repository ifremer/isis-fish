/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 * Cette classe est utilisé pour conservé un etat de changement
 * dans une UI, dans les cas ou les beans changent sur les validateurs et
 * que l'etat change du validateur est reset.
 * 
 * Elle permet aussi de se bind directement dessus (PCS);
 * 
 * Cela permet en gros d'agregger plusieurs validateurs.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class ChangeModel {

    public static final String PROPERTY_CHANGED = "changed";
    
    public static final String PROPERTY_VALID = "valid";

    protected boolean changed;
    
    protected boolean valid = true;

    /**
     * If {@code true} true, {@link #isChanged()} will always return true.
     */
    protected boolean stayChanged = false;

    protected PropertyChangeSupport support;
    
    public ChangeModel() {
        support = new PropertyChangeSupport(this);
    }

    public boolean isChanged() {
        return changed || stayChanged;
    }
    
    public void setChanged(boolean changed) {
        boolean oldValue = this.changed;
        this.changed = changed;
        support.firePropertyChange(PROPERTY_CHANGED, oldValue, this.changed);
    }
    
    public boolean isValid() {
        return valid;
    }
    
    public void setValid(boolean valid) {
        boolean oldValue = this.valid;
        this.valid = valid;
        support.firePropertyChange(PROPERTY_VALID, oldValue, this.valid);
    }

    public void setStayChanged(boolean stayChanged) {
        boolean oldValue = isChanged();
        this.stayChanged = stayChanged;
        support.firePropertyChange(PROPERTY_CHANGED, oldValue, isChanged());
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        support.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        support.removePropertyChangeListener(listener);
    }

    public void addPropertyChangeListener(String propertyName,
            PropertyChangeListener listener) {
        support.addPropertyChangeListener(propertyName, listener);
    }

    public void removePropertyChangeListener(String propertyName,
            PropertyChangeListener listener) {
        support.removePropertyChangeListener(propertyName, listener);
    }
}
