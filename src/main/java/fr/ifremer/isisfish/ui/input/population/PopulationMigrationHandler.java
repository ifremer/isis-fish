/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.population;

import java.awt.CardLayout;

import fr.ifremer.isisfish.entities.PopulationSeasonInfo;
import fr.ifremer.isisfish.ui.input.InputContentHandler;
import fr.ifremer.isisfish.ui.models.common.GenericComboModel;

/**
 * Population handler.
 */
public class PopulationMigrationHandler extends InputContentHandler<PopulationMigrationUI> {

    protected PopulationMigrationHandler(PopulationMigrationUI inputContentUI) {
        super(inputContentUI);
    }

    protected void afterInit() {

        inputContentUI.addPropertyChangeListener(PopulationMigrationUI.PROPERTY_BEAN, evt -> {
            if (evt.getNewValue() == null) {
                inputContentUI.fieldPopulationMigrationComment.setText("");
                inputContentUI.fieldUseEquationMigration.setSelected(false);
            }
            if (evt.getNewValue() != null) {

            }
            refresh();
        });
    }
    
    public void refresh() {
        //Population population = inputContentUI.getSaveVerifier().getEntity(Population.class);
        
        // add null before, for second to be considered as a changed event
        // otherwize, setBean has no effect
        //setBean(null);
        //setBean(population);

        // refresh psi list in combo box
        GenericComboModel<PopulationSeasonInfo> model = (GenericComboModel<PopulationSeasonInfo>)inputContentUI.fieldPopulationMigrationSeasonChooser.getModel();
        if (inputContentUI.getBean() != null) {
            PopulationSeasonInfo previousSelected = (PopulationSeasonInfo)model.getSelectedItem();
            model.setElementList(inputContentUI.getBean().getPopulationSeasonInfo());
            
            // do this to keep selected after cancel/refresh
            if (previousSelected != null) {
                for (PopulationSeasonInfo psi : inputContentUI.getBean().getPopulationSeasonInfo()) {
                    if (psi.getTopiaId().equals(previousSelected.getTopiaId())) {
                        model.setSelectedItem(psi);
                    }
                }
            }

            seasonChanged();
        } else {
            model.setElementList(null);
        }
    }

    protected void seasonChanged() {
        GenericComboModel<PopulationSeasonInfo> model = (GenericComboModel<PopulationSeasonInfo>)inputContentUI.fieldPopulationMigrationSeasonChooser.getModel();
        PopulationSeasonInfo selectedPSI = (PopulationSeasonInfo)model.getSelectedItem();
        inputContentUI.setPopInfo(selectedPSI);
        if (inputContentUI.getPopInfo() != null) {
            inputContentUI.getSaveVerifier().addCurrentEntity(inputContentUI.getPopInfo());
            inputContentUI.populationMigrationEquationUI.getHandler().init(inputContentUI.getPopInfo());
            inputContentUI.populationMigrationMigrationUI.getHandler().init(inputContentUI.getPopInfo());
            inputContentUI.populationMigrationImmigrationUI.getHandler().init(inputContentUI.getPopInfo());
            inputContentUI.populationMigrationEmigrationUI.getHandler().init(inputContentUI.getPopInfo());
        }
        refreshHidablePanel();
    }

    protected void useEquationChanged() {
        inputContentUI.getPopInfo().setUseEquationMigration(inputContentUI.fieldUseEquationMigration.isSelected());
        refreshHidablePanel();
    }

    protected void refreshHidablePanel() {
        if (inputContentUI.getPopInfo() != null) {
            if (inputContentUI.getPopInfo().isUseEquationMigration()) {
                inputContentUI.fieldUseEquationMigration.setSelected(true);
                ((CardLayout)inputContentUI.hidablePanel.getLayout()).show(inputContentUI.hidablePanel, "fieldUseEquation");
            } else {
                inputContentUI.fieldUseEquationMigration.setSelected(false);
                ((CardLayout)inputContentUI.hidablePanel.getLayout()).show(inputContentUI.hidablePanel, "fieldUseMatrix");
            }
        }
    }
}
