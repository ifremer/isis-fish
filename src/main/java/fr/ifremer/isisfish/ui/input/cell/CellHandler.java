/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.cell;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

import java.awt.event.ItemEvent;
import java.awt.event.MouseEvent;

import com.bbn.openmap.event.SelectMouseMode;

import fr.ifremer.isisfish.entities.Cell;
import fr.ifremer.isisfish.entities.FisheryRegion;
import fr.ifremer.isisfish.map.CellSelectionLayer;
import fr.ifremer.isisfish.map.OpenMapEvents;
import fr.ifremer.isisfish.ui.input.InputContentHandler;
import fr.ifremer.isisfish.ui.input.InputUI;
import fr.ifremer.isisfish.ui.models.common.GenericComboModel;
import fr.ifremer.isisfish.ui.sensitivity.SensitivityInputUI;

/**
 * Cell handler.
 */
public class CellHandler extends InputContentHandler<CellUI> {

    protected boolean init;

    protected CellHandler(CellUI inputContentUI) {
        super(inputContentUI);
    }

    protected void afterInit() {

        setupGotoNextPath(t("isisfish.input.continueZones"), n("isisfish.input.tree.zones"));

        //cellMap.init(cellMapInfo);
        new OpenMapEvents(inputContentUI.getCellMap(), new SelectMouseMode(false), CellSelectionLayer.SINGLE_SELECTION) {
            @Override
            public boolean mouseClicked(MouseEvent e) {
                boolean result = false;
                // TODO a fixer, le clic droit du menu contextuel
                // passe aussi par ici et change la selection
                //if (e.getButton() == MouseEvent.BUTTON1) {
                    if (inputContentUI.getBean() != null) { // impossible de desactiver la carte :(
                        for (Cell c : inputContentUI.getCellMap().getSelectedCells()) {
                            if (!c.getTopiaId().equals(inputContentUI.getBean().getTopiaId())) {
                                inputContentUI.getFieldCell().setSelectedItem(c);
                                result = true;
                            }
                        }
                    }
                //}
                return result;
            }
        };

        inputContentUI.addPropertyChangeListener(CellUI.PROPERTY_BEAN, evt -> {
            init = true;
            GenericComboModel<Cell> cellModel = new GenericComboModel<>();
            if (evt.getNewValue() == null) {
                inputContentUI.fieldCellName.setText("");
                inputContentUI.fieldCellLatitude.setText("");
                inputContentUI.fieldCellLongitude.setText("");
                inputContentUI.fieldCellComment.setText("");
                inputContentUI.fieldCellLand.setSelected(false);
            }
            if (evt.getNewValue() != null) {
                //jaxx.runtime.SwingUtil.fillComboBox(cellUI.fieldCell, cellUI.getFisheryRegion().getCell(), cellUI.getBean());
                cellModel.setElementList(inputContentUI.getFisheryRegion().getCell());
                cellModel.setSelectedItem(inputContentUI.getBean());
            }
            inputContentUI.fieldCell.setModel(cellModel);
            init = false;
            // hack because fishery region is not binded
            inputContentUI.getCellMap().setFisheryRegion(inputContentUI.getContextValue(FisheryRegion.class));
        });
    }

    protected void fieldCellChanged(ItemEvent event) {
        if (!init && event.getStateChange() == ItemEvent.SELECTED) {
            Cell c = (Cell)inputContentUI.fieldCell.getSelectedItem();
            if (c==null) {
                return;
            }
            Cell oldC = inputContentUI.getBean();
            if (oldC != null && c.getTopiaId().equals(oldC.getTopiaId())) {
                // avoid reentrant code
                return;
            }
            
            // FIXME il ne faut pas appeler le parent
            // on ne sais jamais de quel type est le parent
            InputUI inputUI = inputContentUI.getParentContainer(InputUI.class);
            if (inputUI != null) {
                inputUI.getHandler().setTreeSelection(this.inputContentUI, c.getTopiaId());
            } else {
                SensitivityInputUI sensitivityInputUI = inputContentUI.getParentContainer(SensitivityInputUI.class);
                sensitivityInputUI.getHandler().setTreeSelection(this.inputContentUI, c.getTopiaId());
            }
        }
    }
}
