/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.tree;

import jaxx.runtime.swing.nav.tree.NavTreeNode;
import jaxx.runtime.swing.nav.tree.NavTreeNodeChildLoador;

/**
 * Do nothing, just to type strange template.
 * 
 * @author chatellier
 * @version $Revision$
 * @since 3.4.0.0
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class FisheryTreeNode extends NavTreeNode<FisheryTreeNode> {
    
    /** serialVersionUID. */
    private static final long serialVersionUID = -3813351863284748776L;

    /**
     * Constructor.
     * 
     * @param internalClass
     * @param id
     * @param context
     * @param childLoador
     */
    public FisheryTreeNode(Class<?> internalClass, String id, String context,
            NavTreeNodeChildLoador<?, ?, FisheryTreeNode> childLoador) {
        super(internalClass, id, context, childLoador);
    }
}
