/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.population;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixND;

import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.entities.PopulationGroup;
import fr.ifremer.isisfish.ui.input.InputContentHandler;
import fr.ifremer.isisfish.ui.models.common.GenericComboModel;

/**
 * Population handler.
 */
public class PopulationGroupHandler extends InputContentHandler<PopulationGroupUI> {

    /** Class logger. */
    private static final Log log = LogFactory.getLog(PopulationGroupHandler.class);

    protected PopulationGroupHandler(PopulationGroupUI inputContentUI) {
        super(inputContentUI);
    }

    protected void afterInit() {
        inputContentUI.addPropertyChangeListener(PopulationGroupUI.PROPERTY_BEAN, evt -> {

            GenericComboModel<PopulationGroup> groupModel = new GenericComboModel<>();

            if (evt.getNewValue() == null) {
                inputContentUI.setPopulationGroup(null);
            }
            if (evt.getNewValue() != null) {
                groupModel.setElementList(inputContentUI.getBean().getPopulationGroup());
            }

            inputContentUI.populationGroupPopulationGroupComboBox.setModel(groupModel);
        });

        inputContentUI.addPropertyChangeListener(PopulationGroupUI.PROPERTY_POPULATION_GROUP, evt -> {
            if (evt.getNewValue() == null) {
                inputContentUI.fieldPopulationGroupMeanWeight.setText("");
                inputContentUI.fieldPopulationGroupPrice.setText("");
                inputContentUI.fieldPopulationGroupReproductionRate.setText("");
                inputContentUI.fieldPopulationGroupMaturityOgive.setText("");
                inputContentUI.fieldPopulationGroupAge.setText("");
                inputContentUI.fieldPopulationGroupMinLength.setText("");
                inputContentUI.fieldPopulationGroupMaxLength.setText("");
                inputContentUI.fieldPopulationGroupComment.setText("");
                inputContentUI.fieldPopulationGroupNaturalDeathRate.setMatrix(null);
            }
            if (evt.getNewValue() != null) {

            }
        });
    }
    
    protected void setNaturalDeathRateMatrix() {
        try {
            Population population = inputContentUI.getBean();
            MatrixND naturalDeathRateMatrix = population.getNaturalDeathRateMatrix();

            // extract only line for this population group
            MatrixND naturalDeathRateMatrix2 = naturalDeathRateMatrix.getSubMatrix(0, inputContentUI.getPopulationGroup());
            inputContentUI.fieldPopulationGroupNaturalDeathRate.setMatrix(naturalDeathRateMatrix2.copy());
        } catch (Exception e) {
            // can happen if population has no zone yet
            // TODO maybe display a message
            if (log.isWarnEnabled()) {
                log.warn("No zone defined for this population group", e);
            }
        }
    }

    /**
     * Called on PopulationGroup combo box selection.
     */
    protected void populationGroupChanged() {
        PopulationGroup selectedPopulationGroup = (PopulationGroup)inputContentUI.populationGroupPopulationGroupComboBox.getSelectedItem();
        inputContentUI.setPopulationGroup(selectedPopulationGroup);
        if (selectedPopulationGroup != null) {
            inputContentUI.getSaveVerifier().addCurrentEntity(selectedPopulationGroup);
            setNaturalDeathRateMatrix();
        }
    }
}
