/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.population;

import static org.nuiton.i18n.I18n.t;

import fr.ifremer.isisfish.ui.input.metier.MetierSeasonInfoZoneUI;
import fr.ifremer.isisfish.util.IsisUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.math.matrix.gui.MatrixPanelEvent;

import fr.ifremer.isisfish.IsisFishDAOHelper;
import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.entities.PopulationSeasonInfo;
import fr.ifremer.isisfish.entities.PopulationSeasonInfoDAO;
import fr.ifremer.isisfish.types.Month;
import fr.ifremer.isisfish.ui.input.InputContentHandler;
import fr.ifremer.isisfish.ui.models.common.GenericComboModel;
import fr.ifremer.isisfish.util.ErrorHelper;
import fr.ifremer.isisfish.ui.widget.interval.Interval;

import java.beans.PropertyChangeEvent;

/**
 * Population handler.
 */
public class PopulationSeasonsHandler extends InputContentHandler<PopulationSeasonsUI> {

    /** Class logger. */
    private static final Log log = LogFactory.getLog(PopulationSeasonsHandler.class);

    protected Interval seasonInterval;

    protected boolean init = false;

    protected PopulationSeasonsHandler(PopulationSeasonsUI inputContentUI) {
        super(inputContentUI);
    }

    protected void afterInit() {
        
        inputContentUI.addPropertyChangeListener(PopulationSeasonsUI.PROPERTY_BEAN, evt -> {
            if (evt.getNewValue() == null) {
                inputContentUI.fieldPopulationSeasonComment.setText("");
                inputContentUI.fieldPopulationSeasonReproductionDistribution.setMatrix(null);
            }
            if (evt.getNewValue() != null) {
                refresh();
            }
        });
        
        /*
         * Don't add both in same listener.
         * When first is set, last value from getPopulationSeasonInfo()
         * is erased by interval.getLast() default value.
         */
        inputContentUI.seasonIntervalPanel.addPropertyChangeListener("first", evt -> {
            if (inputContentUI.getPopulationSeasonInfo() != null) {
                inputContentUI.getPopulationSeasonInfo().setFirstMonth(new Month(seasonInterval.getFirst()));
                setReproductionDistributionMatrix();
            }
            updateSeasonOverlapIcon(evt);
        });
        inputContentUI.seasonIntervalPanel.addPropertyChangeListener("last", evt -> {
            if (inputContentUI.getPopulationSeasonInfo() != null) {
                inputContentUI.getPopulationSeasonInfo().setLastMonth(new Month(seasonInterval.getLast()));
                setReproductionDistributionMatrix();
            }
            updateSeasonOverlapIcon(evt);
        });

        inputContentUI.addPropertyChangeListener(MetierSeasonInfoZoneUI.PROPERTY_BEAN, this::updateSeasonOverlapIcon);
    }

    protected void updateSeasonOverlapIcon(PropertyChangeEvent evt) {
        boolean visible = false;
        if (inputContentUI.getBean() != null) {
            visible = IsisUtil.isSeasonOverlap(inputContentUI.getBean().getPopulationSeasonInfo());
        }
        inputContentUI.overlapSeasonLabel.setVisible(visible);
    }
    
    protected void create() {
        PopulationSeasonInfo seasonNew = createPopulationSeasonInfo(inputContentUI.getBean());
        inputContentUI.setPopulationSeasonInfo(seasonNew);
        setPopulationSeasonInfoCombo();
    }

    protected void delete() {
        removePopulationSeasonInfo(inputContentUI.getBean(), inputContentUI.getPopulationSeasonInfo());
        inputContentUI.setPopulationSeasonInfo(null);
        setPopulationSeasonInfoCombo();
    }
    
    protected PopulationSeasonInfo createPopulationSeasonInfo(Population pop) {
        if (log.isDebugEnabled()) {
            log.debug("createSeasonInfo called");
        }
        PopulationSeasonInfo seasonInfo = null;
        try {
            PopulationSeasonInfoDAO dao = IsisFishDAOHelper.getPopulationSeasonInfoDAO(pop.getTopiaContext());
            seasonInfo = dao.create();
            seasonInfo.setFirstMonth(Month.MONTH[0]);
            seasonInfo.setLastMonth(Month.MONTH[3]);
            pop.addPopulationSeasonInfo(seasonInfo);
            seasonInfo.setPopulation(pop);

            // EC-20090710 ajout du code d'initailisation
            // d'un populationseasoninfo
            // il a du se perdre au changement d'interface
            // swiat>jaxx
            // moved to entity
            //MatrixND matrix = ((PopulationSeasonInfoImpl) seasonInfo).createNoSpacializedChangeGroupMatrix();
            //seasonInfo.setLengthChangeMatrix(matrix);

            seasonInfo.update();
            pop.update();
            // isisContext.commitTransaction();
        } catch (Exception eee) {
            if (log.isErrorEnabled()) {
                log.error("Can't create PopulationSeasonInfo", eee);
            }
            ErrorHelper.showErrorDialog(t("isisfish.error.input.createentity", "PopulationSeasonInfo"), eee);
        }
        return seasonInfo;
    }

    protected void removePopulationSeasonInfo(Population pop,
            PopulationSeasonInfo populationSeasonInfo) {
        if (log.isDebugEnabled()) {
            log.debug("removePopulationSeasonInfo called");
        }
        try {
            pop.removePopulationSeasonInfo(populationSeasonInfo);
            pop.update();
            //pop.getTopiaContext().commitTransaction();
        } catch (Exception eee) {
            if (log.isErrorEnabled()) {
                log.error("Can't remove PopulationSeasonInfo", eee);
            }
            ErrorHelper.showErrorDialog(t("isisfish.error.input.removeentity",
                    "PopulationSeasonInfo"), eee);
        }
    }

    protected void save() {
        inputContentUI.getSaveVerifier().save();
        setPopulationSeasonInfoCombo();
    }

    protected void populationSeasonReproductionDistributionMatrixChanged(MatrixPanelEvent event) {
        if (inputContentUI.getPopulationSeasonInfo() != null && inputContentUI.fieldPopulationSeasonReproductionDistribution.getMatrix() != null) {
            MatrixND reproductionDistribution = inputContentUI.fieldPopulationSeasonReproductionDistribution.getMatrix().copy();
            if (log.isDebugEnabled()) {
                log.debug("Matrix ReproductionDistribution modified : " + reproductionDistribution);
            }
            inputContentUI.getPopulationSeasonInfo().setReproductionDistribution(reproductionDistribution);
        }
    }

    public void refresh() {
        //Population population = inputContentUI.getSaveVerifier().getEntity(Population.class);
        
        // add null before, for second to be considered as a changed event
        // otherwize, setBean has no effect
        //setBean(null);
        //setBean(population);

        inputContentUI.setPopulationSeasonInfo(null);

        // Model instanciation
        seasonInterval = new Interval();
        seasonInterval.setMin(0);
        seasonInterval.setMax(11);
        seasonInterval.setFirst(0);
        seasonInterval.setLast(2);

        setPopulationSeasonInfoCombo();
        setSeasonInterval();

        inputContentUI.seasonIntervalPanel.setLabelRenderer(Month.MONTH);
        inputContentUI.seasonIntervalPanel.setModel(seasonInterval);

        //fieldPopulationSeasonReproductionDistribution.addMatrixListener(matrixPanelListener);

        /*if(getPopulationSeasonInfo() != null) {
            PopulationSeasonInfo popInfo = getPopulationSeasonInfo();
            setPopulationSeasonInfo(null);
            setPopulationSeasonInfo(popInfo);
            getSaveVerifier().addCurrentEntity(getPopulationSeasonInfo());
        }*/
        //getSaveVerifier().addCurrentPanel(populationSeasonSpecializedUI);
    }

    protected void setSeasonInterval() {
        if (inputContentUI.getPopulationSeasonInfo() != null) {
            try {
                if (log.isDebugEnabled()) {
                    log.debug("Updating interval : ");
                }
                Month firstMonth = inputContentUI.getPopulationSeasonInfo().getFirstMonth();

                if (firstMonth != null) {
                    seasonInterval.setFirst(firstMonth.getMonthNumber());
                    
                    if (log.isDebugEnabled()) {
                        log.debug(" first : " + seasonInterval.getFirst());
                    }
                } else {
                    seasonInterval.setFirst(0);
                }

                Month lastMonth = inputContentUI.getPopulationSeasonInfo().getLastMonth();
                if (lastMonth != null) {
                    seasonInterval.setLast(lastMonth.getMonthNumber());
                    if (log.isDebugEnabled()) {
                        log.debug(" last : " + seasonInterval.getLast());
                    }
                } else {
                    seasonInterval.setLast(3);
                }
            } catch (Exception e) {
                if (log.isErrorEnabled()) {
                    log.error("Can't display interval", e);
                }
            }
        }
    }

    protected void setPopulationSeasonInfoCombo() {
        if (inputContentUI.getBean() != null) {
            GenericComboModel<PopulationSeasonInfo> populationSeasonInfoModel = new GenericComboModel<>(inputContentUI.getBean().getPopulationSeasonInfo());
            inputContentUI.fieldPopulationSeasonInfoChooser.setModel(populationSeasonInfoModel);
            populationSeasonInfoModel.setSelectedItem(inputContentUI.getPopulationSeasonInfo());
        }
    }

    protected void seasonGroupChanged() {
        if (inputContentUI.getPopulationSeasonInfo() != null) {
            inputContentUI.getPopulationSeasonInfo().setGroupChange(inputContentUI.fieldPopulationSeasonGroupChange.isSelected());
        }
    }

    protected void seasonChanged() {
        init = true;
        PopulationSeasonInfo seasonInfoSelected = (PopulationSeasonInfo)inputContentUI.fieldPopulationSeasonInfoChooser.getSelectedItem();
        if (log.isDebugEnabled()) {
            log.debug("Season changed : " + seasonInfoSelected);
        }
        inputContentUI.setPopulationSeasonInfo(seasonInfoSelected);
        if (seasonInfoSelected != null) {
            inputContentUI.getSaveVerifier().addCurrentEntity(seasonInfoSelected);
        }
        setSeasonInterval();
        setReproductionDistributionMatrix();
        init = false;
    }

    protected void setReproductionDistributionMatrix() {
        if (inputContentUI.getPopulationSeasonInfo() != null){
            MatrixND reproductionDistribution = inputContentUI.getPopulationSeasonInfo().getReproductionDistribution();
            // must be a copy (otherwise, modify current entity matrix)
            if (reproductionDistribution != null){
                inputContentUI.fieldPopulationSeasonReproductionDistribution.setMatrix(reproductionDistribution.copy());
            }
        }
    }

    // TODO une methode isXXX ne prend pas de parametre
    // et ne fait rien
    protected boolean isAgeGroupType(boolean result) {
        inputContentUI.populationSeasonSpecializedUI.setVisible(result);
        return result;
    }
}
