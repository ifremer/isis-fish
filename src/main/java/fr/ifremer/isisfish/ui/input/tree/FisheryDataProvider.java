/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.tree;

import jaxx.runtime.swing.nav.NavDataProvider;
import fr.ifremer.isisfish.entities.FisheryRegion;

/**
 * Fishery region data provider.
 * 
 * @author chatellier
 * @version $Revision$
 * @since 3.4.0.0
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class FisheryDataProvider implements NavDataProvider {

    protected FisheryRegion fisheryRegion;

    public FisheryDataProvider(FisheryRegion fisheryRegion) {
        this.fisheryRegion = fisheryRegion;
    }

    /**
     * Get provider fishery region.
     * 
     * @return provide fishery region
     */
    public FisheryRegion getFisheryRegion() {
        return fisheryRegion;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
