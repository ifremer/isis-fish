/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2011 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input;

import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntityContextable;

import fr.ifremer.isisfish.ui.SaveVerifier;

/**
 * Listener used to control InputUI tab panel switching.
 *
 * @author letellier
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class InputTabbedPaneListener implements ChangeListener {

    /** Class logger. */
    private static Log log = LogFactory.getLog(InputTabbedPaneListener.class);

    protected int cacheSelectedIndex = -1;

    @Override
    public void stateChanged(ChangeEvent e) {

        JTabbedPane pane = (JTabbedPane) e.getSource();
        
        // to not fire additional event during tab index management
        pane.removeChangeListener(this);

        // reselect previous tab (and ask user to change)
        int selectedIndex = pane.getSelectedIndex();
        if (cacheSelectedIndex != -1) {
            pane.setSelectedIndex(cacheSelectedIndex);
        }
        InputContentUI ui = (InputContentUI) pane.getSelectedComponent();
        InputSaveVerifier saveVerifier = (InputSaveVerifier)ui.getContextValue(InputSaveVerifier.class);
        if (canChangeTab(saveVerifier)) {
            pane.setSelectedIndex(selectedIndex);
        }
        ui = (InputContentUI) pane.getSelectedComponent();

        // refresh can be broken
        // especially during new region creation
        // don't break refresh cycle
        try {
            TopiaEntityContextable entity = ui.getBean();
            // refresh ui
            ui.setBean(null);
            ui.setBean(entity);
        }
        catch (Exception eee) {
            if (log.isErrorEnabled()) {
                log.error("Error during UI refresh", eee);
            }
        }

        // method only called for tabbed UI
        // otherwise save.cancel buttons are active in last
        // refreshed ui
        if (log.isDebugEnabled()) {
            log.debug("Activating action buttons on " + ui);
        }

        cacheSelectedIndex = pane.getSelectedIndex();
        pane.addChangeListener(this);
        
        // bug suis les UI autre que nimbus
        // la selection des onglet ne se rafraichit pas tres bien
        pane.repaint();
    }

    /**
     * Ask you to save if modification are made.
     * 
     * @param saveVerifier save verifier
     * @return true to change tab
     */
    protected boolean canChangeTab(SaveVerifier saveVerifier) {
        // by default, we says that component was successfully closed
        boolean exit = true;

        int response = saveVerifier.checkEdit();

        if (response == SaveVerifier.CANCEL_OPTION) {
            exit = false;
        }
        return exit;
    }
}
