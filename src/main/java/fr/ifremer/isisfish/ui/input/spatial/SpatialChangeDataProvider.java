/*
 * #%L
 * ISIS-Fish
 * %%
 * Copyright (C) 2017 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.ui.input.spatial;

import fr.ifremer.isisfish.entities.Cell;
import fr.ifremer.isisfish.entities.FisheryRegion;
import fr.ifremer.isisfish.map.MapDataProvider;

import java.util.ArrayList;
import java.util.List;

/**
 * Provider used to provide static data from {@code List} of {@code Cell} instead of database.
 */
public class SpatialChangeDataProvider extends MapDataProvider {

    protected FisheryRegion fisheryRegion;

    protected List<Cell> cells;

    public SpatialChangeDataProvider(FisheryRegion fisheryRegion, List<Cell> cells) {
        this.fisheryRegion = fisheryRegion;
        this.cells = cells;
    }

    @Override
    public float getMinLongitude() {
        return fisheryRegion.getMinLongitude();
    }

    @Override
    public float getMaxLongitude() {
        return fisheryRegion.getMaxLongitude();
    }

    @Override
    public float getMinLatitude() {
        return fisheryRegion.getMinLatitude();
    }

    @Override
    public float getMaxLatitude() {
        return fisheryRegion.getMaxLatitude();
    }

    @Override
    public float getCellLengthLongitude() {
        return fisheryRegion.getCellLengthLongitude();
    }

    @Override
    public float getCellLengthLatitude() {
        return fisheryRegion.getCellLengthLatitude();
    }

    @Override
    public List<Cell> getCell() {
        return cells;
    }

    @Override
    public List<String> getMapFilePath() {
        return fisheryRegion.getMapFilePath();
    }

    @Override
    public List<Cell> findAllByCoordinates(float latitude, float longitude) {
        List<Cell> filteredCells = new ArrayList<>();
        for (Cell cell : cells) {
            if (cell.getLatitude() == latitude && cell.getLongitude() == longitude) {
                filteredCells.add(cell);
            }
        }
        return filteredCells;
    }
}
