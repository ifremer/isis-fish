/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.metier;

import static org.nuiton.i18n.I18n.t;

import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.event.ListSelectionListener;

import fr.ifremer.isisfish.util.IsisUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaException;

import fr.ifremer.isisfish.IsisFishDAOHelper;
import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.entities.Metier;
import fr.ifremer.isisfish.entities.MetierSeasonInfo;
import fr.ifremer.isisfish.entities.MetierSeasonInfoDAO;
import fr.ifremer.isisfish.entities.Zone;
import fr.ifremer.isisfish.types.Month;
import fr.ifremer.isisfish.ui.input.InputContentHandler;
import fr.ifremer.isisfish.ui.models.common.GenericComboModel;
import fr.ifremer.isisfish.ui.models.common.GenericListModel;
import fr.ifremer.isisfish.util.ErrorHelper;
import fr.ifremer.isisfish.ui.widget.interval.Interval;

/**
 * Metier tab handler.
 */
public class MetierSeasonInfoZoneHandler extends InputContentHandler<MetierSeasonInfoZoneUI> {

    /** Class logger. */
    private static final Log log = LogFactory.getLog(MetierSeasonInfoZoneHandler.class);

    protected Interval interval = null;
    protected boolean init = false;

    protected MetierSeasonInfoZoneHandler(MetierSeasonInfoZoneUI inputContentUI) {
        super(inputContentUI);
    }

    protected void afterInit() {
        /*
         * Don't add both in same listener.
         * When first is set, last value from getPopulationSeasonInfo()
         * is erased by interval.getLast() default value.
         */
        inputContentUI.ip.addPropertyChangeListener("first", evt -> {
            if (inputContentUI.getMetierSeasonInfo() != null) {
                inputContentUI.getMetierSeasonInfo().setFirstMonth(new Month(interval.getFirst()));
            }
            updateSeasonOverlapIcon(evt);
        });
        inputContentUI.ip.addPropertyChangeListener("last", evt -> {
            if (inputContentUI.getMetierSeasonInfo() != null) {
                inputContentUI.getMetierSeasonInfo().setLastMonth(new Month(interval.getLast()));
            }
            updateSeasonOverlapIcon(evt);
        });
        
        inputContentUI.addPropertyChangeListener(MetierSeasonInfoZoneUI.PROPERTY_BEAN, evt -> {
            if (evt.getNewValue() != null) {
                refresh();
            }
        });

        inputContentUI.addPropertyChangeListener(MetierSeasonInfoZoneUI.PROPERTY_BEAN, this::updateSeasonOverlapIcon);
    }

    protected void updateSeasonOverlapIcon(PropertyChangeEvent evt) {
        boolean visible = false;
        if (inputContentUI.getBean() != null) {
            visible = IsisUtil.isSeasonOverlap(inputContentUI.getBean().getMetierSeasonInfo());
        }
        inputContentUI.overlapSeasonLabel.setVisible(visible);
    }
    
    protected void save() {
        inputContentUI.getSaveVerifier().save();
        setMetierSeasonInfoCombo();
    }

    protected void create() {
        MetierSeasonInfo newMSI = createMetierSeasonInfo(inputContentUI.getBean());
        inputContentUI.setMetierSeasonInfo(newMSI);
        setMetierSeasonInfoCombo();
    }

    protected void delete() {
        removeMetierSeasonInfo(inputContentUI.getBean(), inputContentUI.getMetierSeasonInfo());
        inputContentUI.setMetierSeasonInfo(null);
        setMetierSeasonInfoCombo();
    }

    public MetierSeasonInfo createMetierSeasonInfo(Metier metier) {
        MetierSeasonInfo metierSeasonInfo;
        if (log.isDebugEnabled()) {
            log.debug("createMetierSeasonInfo called");
        }
        try {
            MetierSeasonInfoDAO metierSeasonInfoPS = IsisFishDAOHelper
                    .getMetierSeasonInfoDAO(metier.getTopiaContext());
            metierSeasonInfo = metierSeasonInfoPS.create();
            metierSeasonInfo.setFirstMonth(Month.MONTH[0]);
            metierSeasonInfo.setLastMonth(Month.MONTH[3]);
            metier.addMetierSeasonInfo(metierSeasonInfo);
            metierSeasonInfo.update();
            metier.update();

        } catch (TopiaException eee) {
            throw new IsisFishRuntimeException("Can't create MetierSeasonInfo", eee);
        }
        return metierSeasonInfo;
    }

    public void removeMetierSeasonInfo(Metier metier, MetierSeasonInfo info) {
        if (log.isDebugEnabled()) {
            log.debug("removeMetierSeasonInfo called");
        }
        try {
            metier.removeMetierSeasonInfo(info);
            metier.update();
            
            // EC-20091112 : commit() twice cause hibernate error:
            // Found two representations of same collection:
            //metier.getTopiaContext().commitTransaction();

        } catch (Exception eee) {
            if (log.isErrorEnabled()) {
                log.error("Can't remove MetierSeasonInfo", eee);
            }
            ErrorHelper.showErrorDialog(t("isisfish.error.input.addentity",
                    "MetierSeasonInfo"), eee);
        }
    }

    public void refresh() {

        inputContentUI.setMetierSeasonInfo(null);

        if (inputContentUI.getBean() != null) {
            // Model instanciation
            interval = new Interval();
            interval.setMin(0);
            interval.setMax(11);
            interval.setFirst(0);
            interval.setLast(2);

            setMetierSeasonInfoCombo();
            setSeason();
            setMetierZone();
            
            inputContentUI.ip.setLabelRenderer(Month.MONTH);
            inputContentUI.ip.setModel(interval);
        }
    }

    protected void setSeason() {
        if (inputContentUI.getMetierSeasonInfo() != null) {
        
            // register selected item in save verifier
            inputContentUI.getSaveVerifier().addCurrentEntity(inputContentUI.getMetierSeasonInfo());

            try {
                if (log.isDebugEnabled()) {
                    log.debug("Refresh interval : ");
                }
                Month firstMonth = inputContentUI.getMetierSeasonInfo().getFirstMonth();
                if (firstMonth != null) {
                    interval.setFirst(firstMonth.getMonthNumber());
                    if (log.isDebugEnabled()) {
                        log.debug(" first : " + interval.getFirst());
                    }
                } else {
                    interval.setFirst(0);
                }

                Month lastMonth = inputContentUI.getMetierSeasonInfo().getLastMonth();
                if (lastMonth != null) {
                    interval.setLast(lastMonth.getMonthNumber());
                    if (log.isDebugEnabled()) {
                        log.debug(" last : " + interval.getLast());
                    }
                } else {
                    interval.setLast(3);
                }
            } catch (Exception e) {
                if (log.isErrorEnabled()) {
                    log.error("Can't display season", e);
                }
            }
        }
    }
    protected void setMetierZone() {
        if (inputContentUI.getMetierSeasonInfo() != null) {
            ListSelectionListener[] listeners = inputContentUI.metierZones.getListSelectionListeners();
            for (ListSelectionListener listener : listeners) {
                inputContentUI.metierZones.removeListSelectionListener(listener);
            }

            List<Zone> allZones = inputContentUI.getFisheryRegion().getZone();
            GenericListModel<Zone> model = new GenericListModel<>(allZones);
            inputContentUI.metierZones.setModel(model);
            // restore selection
            if (inputContentUI.metierSeasonInfo.getZone() != null) {
                for (Zone zone : inputContentUI.metierSeasonInfo.getZone()) {
                    int index = allZones.indexOf(zone);
                    inputContentUI.metierZones.getSelectionModel().addSelectionInterval(index, index);
                }
            }

            for (ListSelectionListener listener : listeners) {
                inputContentUI.metierZones.addListSelectionListener(listener);
            }
        }
    }
   
    protected void setMetierSeasonInfoCombo() {
        List<MetierSeasonInfo> metierSeasonInfoList = inputContentUI.getBean().getMetierSeasonInfo();
        GenericComboModel<MetierSeasonInfo> metierSeasonInfoModel = new GenericComboModel<>(metierSeasonInfoList);
        inputContentUI.metierSeasonInfoCombo.setModel(metierSeasonInfoModel);
        metierSeasonInfoModel.setSelectedItem(inputContentUI.getMetierSeasonInfo());
    }

    protected void metierZonesChanged() {
        List<Zone> selected = inputContentUI.metierZones.getSelectedValuesList();
        List<Zone> zones = new ArrayList<>(selected);
        inputContentUI.getMetierSeasonInfo().setZone(zones);
    }

    protected void seasonChanged() {
        init = true;
        inputContentUI.setMetierSeasonInfo((MetierSeasonInfo)inputContentUI.metierSeasonInfoCombo.getSelectedItem());
        setSeason();
        setMetierZone();
        init = false;
    }
}
