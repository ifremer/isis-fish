/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.vesseltype;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

import java.util.ArrayList;
import java.util.List;

import fr.ifremer.isisfish.entities.TripType;
import fr.ifremer.isisfish.ui.input.InputContentHandler;
import fr.ifremer.isisfish.ui.models.common.GenericListModel;

/**
 * Trip type handler.
 */
public class VesselTypeHandler extends InputContentHandler<VesselTypeUI> {

    protected boolean init;

    protected VesselTypeHandler(VesselTypeUI inputContentUI) {
        super(inputContentUI);
    }

    protected void afterInit() {
        setupGotoNextPath(t("isisfish.input.continueSetOfVessels"), n("isisfish.input.tree.setofvessels"));

        inputContentUI.addPropertyChangeListener(VesselTypeUI.PROPERTY_BEAN, evt -> {
            if (evt.getNewValue() == null) {
                inputContentUI.fieldVesselTypeName.setText("");
                inputContentUI.fieldVesselTypeLength.setText("");
                inputContentUI.fieldVesselTypeSpeed.setText("");
                inputContentUI.fieldVesselTypeMaxTripDuration.setText("");
                inputContentUI.fieldVesselTypeActivityRange.setText("");
                inputContentUI.fieldVesselTypeMinCrewSize.setText("");
                inputContentUI.fieldVesselTypeSpeed.setText("");
                inputContentUI.fieldVesselTypeUnitFuelCostOfTravel.setText("");
                inputContentUI.fieldVesselTypeComment.setText("");
            }
            if (evt.getNewValue() != null) {
                setTripTypeListModel();
            }
        });
    }
    
    protected void setTripTypeListModel() {
        init = true;
        List<TripType> tripTypes = inputContentUI.getFisheryRegion().getTripType();
        GenericListModel<TripType> tripTypeModel = new GenericListModel<>(tripTypes);
        inputContentUI.vesselTypeTripType.setModel(tripTypeModel);
        
        if (inputContentUI.getBean() != null && inputContentUI.getBean().getTripType() != null) {
            for (TripType tripType : inputContentUI.getBean().getTripType()) {
                int index = tripTypes.indexOf(tripType);
                inputContentUI.vesselTypeTripType.addSelectionInterval(index, index);
            }
        }
        init = false;
    }
    protected void tripTypeChanged() {
        if (!init) {
            List<TripType> tripTypes = new ArrayList<>(inputContentUI.vesselTypeTripType.getSelectedValuesList());
            inputContentUI.getBean().setTripType(tripTypes);
        }
    }
}
