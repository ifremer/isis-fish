/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.ui.input.tree.loadors;

import static org.nuiton.i18n.I18n.n;

import java.util.List;

import jaxx.runtime.swing.nav.NavDataProvider;
import fr.ifremer.isisfish.entities.FisheryRegion;
import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.entities.Species;
import fr.ifremer.isisfish.ui.input.tree.FisheryDataProvider;
import fr.ifremer.isisfish.ui.input.tree.FisheryTreeNode;

/**
 * Species tree path node loader.
 * 
 * @author chatellier
 * @since 3.4.0.0
 */
public class SpeciesNodeLoador extends FisheryTreeNodeLoador<Species> {

    /** serialVersionUID. */
    private static final long serialVersionUID = 6540304326033236054L;

    public SpeciesNodeLoador() {
        super(Species.class);
    }

    @Override
    public List<Species> getData(Class<?> parentClass, String parentId, NavDataProvider dataProvider) throws Exception {
        FisheryDataProvider ficheryRegionDataProvider = (FisheryDataProvider)dataProvider;
        FisheryRegion fisheryRegion = ficheryRegionDataProvider.getFisheryRegion();
        List<Species> species = fisheryRegion.getSpecies();
        return species;
    }

    @Override
    public FisheryTreeNode createNode(Species species, NavDataProvider dataProvider) {

        // Create clients static nodes
        FisheryTreeNode speciesNode = new FisheryTreeNode(
                Species.class, species.getTopiaId(),
                null, null);

        // Create clients static nodes
        FisheryTreeNode speciesPopulationStaticNode = new FisheryTreeNode(
                Population.class, n("isisfish.input.tree.populations"),
                null, new PopulationsNodeLoador(species));

        speciesNode.add(speciesPopulationStaticNode);

        return speciesNode;
    }
}
