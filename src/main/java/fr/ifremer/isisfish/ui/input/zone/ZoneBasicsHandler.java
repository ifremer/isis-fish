/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 - 2022 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.zone;

import java.awt.Cursor;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.event.ListSelectionEvent;

import fr.ifremer.isisfish.entities.FisheryRegion;
import fr.ifremer.isisfish.entities.Zone;
import fr.ifremer.isisfish.map.GeoTools;
import fr.ifremer.isisfish.util.IsisFileUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.bbn.openmap.event.SelectMouseMode;

import fr.ifremer.isisfish.entities.Cell;
import fr.ifremer.isisfish.map.CellSelectionLayer;
import fr.ifremer.isisfish.map.OpenMapEvents;
import fr.ifremer.isisfish.ui.input.InputContentHandler;
import fr.ifremer.isisfish.ui.models.common.GenericListModel;

import static org.nuiton.i18n.I18n.t;

/**
 * Zone handler.
 */
public class ZoneBasicsHandler extends InputContentHandler<ZoneBasicsUI> {

    /** Class logger. */
    private static final Log log = LogFactory.getLog(ZoneBasicsHandler.class);

    public ZoneBasicsHandler(ZoneBasicsUI inputContentUI) {
        super(inputContentUI);
    }

    protected void afterInit() {
        new OpenMapEvents(inputContentUI.zoneMap, new SelectMouseMode(false), CellSelectionLayer.MULT_SELECTION) {
            @Override
            public boolean mouseClicked(MouseEvent e) {
                if (inputContentUI.getBean() != null) { // impossible de desactiver la carte :(
                    inputContentUI.getBean().setCell(inputContentUI.zoneMap.getSelectedCells());
                    setZoneCells();
                }
                return false;
            }
        };

        inputContentUI.addPropertyChangeListener(ZoneBasicsUI.PROPERTY_BEAN, evt -> {
            if (evt.getNewValue() == null) {
                inputContentUI.fieldZoneName.setText("");
                inputContentUI.fieldZoneComment.setText("");
                inputContentUI.zoneMap.setSelectedCells();
            }
            if (evt.getNewValue() != null) {

            }
            setZoneCells();
            // hack because fishery region is not binded
            inputContentUI.getZoneMap().setFisheryRegion(inputContentUI.getContextValue(FisheryRegion.class));
        });
    }

    protected void setZoneCells() {
        GenericListModel<Cell> zoneModel = new GenericListModel<>();
        if (inputContentUI.getBean() != null) {
            
            // build model
            List<Cell> cells = inputContentUI.getFisheryRegion().getCell();
            zoneModel.setElementList(cells);
            inputContentUI.zoneCells.setModel(zoneModel);

            // restore selection
            int firstIndex = -1;
            if (inputContentUI.getBean().getCell() != null) {
                for (Cell selectedCell : inputContentUI.getBean().getCell()) {
                    int index = cells.indexOf(selectedCell);
                    inputContentUI.zoneCells.addSelectionInterval(index, index);
                    
                    if (firstIndex == -1) {
                        firstIndex = index;
                    }
                }
            }
            
            // scroll to visible
            if (firstIndex != -1) {
                inputContentUI.zoneCells.ensureIndexIsVisible(firstIndex);
            }
        } else {
            inputContentUI.zoneCells.setModel(zoneModel);
        }
    }

    public void zoneCellsChange(ListSelectionEvent event) {
        // sans ca, ca boucle (modification depuis la carte)
        if (event.getValueIsAdjusting()) {
            // pas a faire dans le cas d'une AS
            if (inputContentUI.isActive()) {
                List<Cell> cells = new ArrayList<>(inputContentUI.zoneCells.getSelectedValuesList());
                inputContentUI.getBean().setCell(cells);
            }
        }
    }

    public void fromMap() {
        File file = IsisFileUtil.getFile(".*\\.shp", "ESRI Shapefiles (.shp)");
        if (file != null) {
            setStatusMessage(inputContentUI, t("isisfish.zone.fromMap.running"), true);
            inputContentUI.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

            new Thread(() -> {
                try {
                    FisheryRegion fisheryRegion = inputContentUI.getFisheryRegion();
                    List<Cell> allCells = fisheryRegion.getCell();
                    List<Cell> selectedCells = new ArrayList<>(GeoTools.getCellFromShapefile(fisheryRegion, allCells, file));
                    inputContentUI.getBean().setCell(selectedCells);
                    setZoneCells();
                } finally {
                    setStatusMessage(inputContentUI, t("isisfish.zone.fromMap.done"));
                    inputContentUI.setCursor(Cursor.getDefaultCursor());
                }
            }).start();
        }
    }

    public void toMap() {
        File file = IsisFileUtil.getFile(".*\\.shp", "ESRI Shapefiles (.shp)");
        if (file != null) {
            file = IsisFileUtil.addExtensionIfNeeded(file, "shp");

            FisheryRegion fisheryRegion = inputContentUI.getFisheryRegion();
            Zone zone = inputContentUI.getBean();
            List<Cell> cells = zone.getCell();
            GeoTools.setCellToShapefile(fisheryRegion, cells, zone.getName(), file);
            setStatusMessage(inputContentUI, t("isisfish.zone.toMap.done"));
        }
    }
}
