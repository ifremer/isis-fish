/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.population;

import java.awt.CardLayout;

import org.nuiton.math.matrix.gui.MatrixPanelEvent;

import fr.ifremer.isisfish.ui.input.InputContentHandler;

/**
 * Population handler.
 */
public class PopulationCapturabilityHandler extends InputContentHandler<PopulationCapturabilityUI> {

    protected PopulationCapturabilityHandler(PopulationCapturabilityUI inputContentUI) {
        super(inputContentUI);
    }

    protected void afterInit() {
        inputContentUI.addPropertyChangeListener(PopulationCapturabilityUI.PROPERTY_BEAN, evt -> {
            if (evt.getNewValue() == null) {
                inputContentUI.fieldPopulationCapturabilityComment.setText("");
                inputContentUI.fieldPopulationCapturability.setMatrix(null);
            }
            if (evt.getNewValue() != null) {
                if (inputContentUI.getBean().getCapturability() != null) {
                    inputContentUI.fieldPopulationCapturability.setMatrix(inputContentUI.getBean().getCapturability().copy());
                }
                refreshHidablePanel();
            }
        });
    }
    
    protected void populationCapturabilityMatrixChanged(MatrixPanelEvent event) {
        if (inputContentUI.getBean() != null && inputContentUI.fieldPopulationCapturability.getMatrix() != null) {
            inputContentUI.getBean().setCapturability(inputContentUI.fieldPopulationCapturability.getMatrix().copy());
        }
    }

    protected void useEquationChanged() {
        inputContentUI.getBean().setCapturabilityEquationUsed(inputContentUI.fieldUseCapturabilityEquation.isSelected());

        // compute matrix again to not diplay values computed by equation
        if (!inputContentUI.fieldUseCapturabilityEquation.isSelected()) {
            if (inputContentUI.getBean().getCapturability() != null) {
                inputContentUI.fieldPopulationCapturability.setMatrix(inputContentUI.getBean().getCapturability().copy());
            }
        }
        refreshHidablePanel();
    }

    protected void refreshHidablePanel() {
        if (inputContentUI.getBean() != null) {
            if (inputContentUI.getBean().isCapturabilityEquationUsed()) {
                inputContentUI.fieldUseCapturabilityEquation.setSelected(true);
                ((CardLayout)inputContentUI.hidablePanel.getLayout()).show(inputContentUI.hidablePanel, "fieldUseEquation");
            } else {
                inputContentUI.fieldUseCapturabilityEquation.setSelected(false);
                ((CardLayout)inputContentUI.hidablePanel.getLayout()).show(inputContentUI.hidablePanel, "fieldUseMatrix");
            }
        }
    }
}
