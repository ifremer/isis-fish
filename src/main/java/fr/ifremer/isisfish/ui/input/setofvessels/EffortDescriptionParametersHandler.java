/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.setofvessels;

import java.util.ArrayList;
import java.util.List;

import fr.ifremer.isisfish.entities.EffortDescription;
import fr.ifremer.isisfish.ui.input.InputContentHandler;
import fr.ifremer.isisfish.ui.models.common.GenericListModel;

/**
 * set of vessels handler.
 */
public class EffortDescriptionParametersHandler extends InputContentHandler<EffortDescriptionParametersUI> {

    protected EffortDescriptionParametersHandler(EffortDescriptionParametersUI inputContentUI) {
        super(inputContentUI);
    }

    protected void afterInit() {

        inputContentUI.addPropertyChangeListener(EffortDescriptionParametersUI.PROPERTY_BEAN, evt -> {
            if (evt.getNewValue() == null) {
                inputContentUI.setEffortDescription(null);
            }
            if (evt.getNewValue() != null) {
                GenericListModel<EffortDescription> model = new GenericListModel<>();
                // getBean().getPossibleMetiers() can be null at region creation
                if (inputContentUI.getBean() != null && inputContentUI.getBean().getPossibleMetiers() != null) {
                    List<EffortDescription> effortDescriptions = new ArrayList<>(inputContentUI.getBean().getPossibleMetiers());
                    model.setElementList(effortDescriptions);
                }
                inputContentUI.fieldEffortDescriptionEffortDescriptionList.setModel(model);
            }
        });
        inputContentUI.addPropertyChangeListener(EffortDescriptionParametersUI.PROPERTY_EFFORT_DESCRIPTION, evt -> {
            if (evt.getNewValue() == null) {
                inputContentUI.fieldEffortDescriptionFishingOperation.setText("");
                inputContentUI.fieldEffortDescriptionFishingOperationDuration.setText("");
                inputContentUI.fieldEffortDescriptionGearsNumberPerOperation.setText("");
                inputContentUI.fieldEffortDescriptionCrewSize.setText("");
                inputContentUI.fieldEffortDescriptionUnitCostOfFishing.setText("");
                inputContentUI.fieldEffortDescriptionFixedCrewSalary.setText("");
                inputContentUI.fieldEffortDescriptionCrewFoodCost.setText("");
                inputContentUI.fieldEffortDescriptionCrewShareRate.setText("");
                inputContentUI.fieldEffortDescriptionRepairAndMaintenanceGearCost.setText("");
                inputContentUI.fieldEffortDescriptionLandingCosts.setText("");
                inputContentUI.fieldEffortDescriptionOtherRunningCost.setText("");
            } else if (inputContentUI.getEffortDescription().getFishingOperationDuration() == null) {
                // FIX non working binding in jaxx 2.4.1
                inputContentUI.fieldEffortDescriptionFishingOperationDuration.setText("");
            }
        });
    }
    
    protected void effortDescriptionSelectionChanged() {
        EffortDescription selectedEffort = inputContentUI.fieldEffortDescriptionEffortDescriptionList.getSelectedValue();
        inputContentUI.setEffortDescription(selectedEffort);

        if (inputContentUI.getEffortDescription() != null) {
            inputContentUI.getSaveVerifier().addCurrentEntity(inputContentUI.getEffortDescription());
            selectedEffort.addPropertyChangeListener(evt -> inputContentUI.changeModel.setStayChanged(true));
        }
    }
}
