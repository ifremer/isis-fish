/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.population;

import static org.nuiton.i18n.I18n.t;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.math.matrix.gui.MatrixPanelEvent;

import fr.ifremer.isisfish.entities.PopulationGroup;
import fr.ifremer.isisfish.entities.PopulationSeasonInfo;
import fr.ifremer.isisfish.entities.Zone;
import fr.ifremer.isisfish.ui.input.InputContentHandler;
import fr.ifremer.isisfish.ui.models.common.GenericComboModel;
import fr.ifremer.isisfish.util.ErrorHelper;

/**
 * Population handler.
 */
public class PopulationMigrationEmigrationHandler extends InputContentHandler<PopulationMigrationEmigrationUI> {

    /** Class logger. */
    private static final Log log = LogFactory.getLog(PopulationMigrationEmigrationHandler.class);

    protected PopulationMigrationEmigrationHandler(PopulationMigrationEmigrationUI inputContentUI) {
        super(inputContentUI);
    }

    protected void afterInit() {

        inputContentUI.addPropertyChangeListener(PopulationMigrationEmigrationUI.PROPERTY_BEAN, evt -> {
            if (evt.getNewValue() == null) {
                inputContentUI.fieldPopulationMigrationEmigrationCoefficient.setText("");
                inputContentUI.populationMigrationEmigrationTable.setMatrix(null);
            }
            if (evt.getNewValue() != null) {

            }
            setFieldPopulationMigrationMigrationGroupChooserModel();
            setFieldPopulationMigrationMigrationDepartureZoneChooserModel();
            setAddButton();
        });
    }

    public void init(PopulationSeasonInfo pi) {
        // add null before, for second to be considered as a changed event
        // otherwize, setBean has no effect
        //setPopInfo(null);
        //setPopInfo(pi);
        inputContentUI.populationMigrationEmigrationTable.setMatrix(inputContentUI.getPopInfo().getEmigrationMatrix().copy());
    }

    protected void populationMigrationEmigrationMatrixChanged(MatrixPanelEvent event) {
        inputContentUI.remove.setEnabled(inputContentUI.populationMigrationEmigrationTable.getTable().getSelectedRow() != -1);
        if (inputContentUI.popInfo != null) {
            inputContentUI.popInfo.setMigrationMatrix(inputContentUI.populationMigrationEmigrationTable.getMatrix().clone());
        }
    }

    protected void setFieldPopulationMigrationMigrationGroupChooserModel() {
        GenericComboModel<PopulationGroup> groups = new GenericComboModel<>();
        if (inputContentUI.getBean() != null && inputContentUI.getBean().getPopulationGroup() != null) {
            //jaxx.runtime.SwingUtil.fillComboBox(fieldPopulationMigrationEmigrationGroupChooser,getBean().getPopulationGroup(), null, true);
            groups.setElementList(inputContentUI.getBean().getPopulationGroup());
        }
        inputContentUI.fieldPopulationMigrationEmigrationGroupChooser.setModel(groups);
    }

    protected void setFieldPopulationMigrationMigrationDepartureZoneChooserModel() {
        GenericComboModel<Zone> zones = new GenericComboModel<>();
        if (inputContentUI.getBean() != null && inputContentUI.getBean().getPopulationZone() != null) {
            //jaxx.runtime.SwingUtil.fillComboBox(fieldPopulationMigrationEmigrationDepartureZoneChooser,getBean().getPopulationZone(), null, true);
            zones.setElementList(inputContentUI.getBean().getPopulationZone());
        }
        inputContentUI.fieldPopulationMigrationEmigrationDepartureZoneChooser.setModel(zones);
    }

    protected void add() {
        addEmigration(inputContentUI.getPopInfo(),
                (PopulationGroup)inputContentUI.fieldPopulationMigrationEmigrationGroupChooser.getSelectedItem(),
                (Zone)inputContentUI.fieldPopulationMigrationEmigrationDepartureZoneChooser.getSelectedItem(),
                Double.parseDouble(inputContentUI.fieldPopulationMigrationEmigrationCoefficient.getText()));
        inputContentUI.populationMigrationEmigrationTable.setMatrix(inputContentUI.getPopInfo().getEmigrationMatrix().clone());
    }
    
    public Object addEmigration(PopulationSeasonInfo info,
            PopulationGroup group, Zone departure, double coeff) {
        if (log.isTraceEnabled()) {
            log.trace("addEmigration called");
        }
        try {
            MatrixND mat = info.getEmigrationMatrix().copy();
            mat.setValue(group, departure, coeff);
            info.setEmigrationMatrix(mat);

        } catch (Exception eee) {
            if (log.isErrorEnabled()) {
                log.error("Can't add emigration", eee);
            }
            ErrorHelper.showErrorDialog(t("isisfish.error.input.addentity",
                    "Emigration"), eee);
        }
        return null;
    }

    protected void remove() {
        int row = inputContentUI.populationMigrationEmigrationTable.getTable().getSelectedRow();
        if (row != -1) {
            Object group = inputContentUI.populationMigrationEmigrationTable.getTable().getValueAt(row, 0);
            Object arrival = inputContentUI.populationMigrationEmigrationTable.getTable().getValueAt(row, 1);

            MatrixND mat = inputContentUI.popInfo.getEmigrationMatrix().clone();
            mat.setValue(group, arrival, 0);
            inputContentUI.popInfo.setEmigrationMatrix(mat);
            inputContentUI.populationMigrationEmigrationTable.setMatrix(inputContentUI.getPopInfo().getEmigrationMatrix().copy());
        }
    }

    protected void setAddButton() {
        inputContentUI.add.setEnabled(inputContentUI.isActive() && inputContentUI.fieldPopulationMigrationEmigrationGroupChooser.getSelectedItem() != null
                && inputContentUI.fieldPopulationMigrationEmigrationDepartureZoneChooser.getSelectedItem() != null
                && !inputContentUI.fieldPopulationMigrationEmigrationCoefficient.getText().equals(""));
    }
}
