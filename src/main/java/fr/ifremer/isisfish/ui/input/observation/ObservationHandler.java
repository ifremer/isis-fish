/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 - 2020 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.observation;

import static org.nuiton.i18n.I18n.t;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixFactory;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.math.matrix.gui.MatrixPanelEvent;

import fr.ifremer.isisfish.entities.Observation;
import fr.ifremer.isisfish.ui.input.InputContentHandler;
import fr.ifremer.isisfish.util.ErrorHelper;
import java.io.File;
import org.nuiton.jaxx.runtime.JaxxFileChooser;

/**
 * Observation handler.
 */
public class ObservationHandler extends InputContentHandler<ObservationUI> {

    /** Class logger. */
    private static final Log log = LogFactory.getLog(ObservationHandler.class);

    protected ObservationHandler(ObservationUI inputContentUI) {
        super(inputContentUI);
    }

    protected void afterInit() {
        inputContentUI.addPropertyChangeListener(ObservationUI.PROPERTY_BEAN, evt -> {
            if (evt.getNewValue() == null) {
                inputContentUI.fieldObservationValue.setMatrix(null);
            }
            if (evt.getNewValue() != null) {
                setObservationValueMatrix();
            }
        });
    }
    
    protected void setObservationValueMatrix() {
        MatrixND prop = inputContentUI.getBean().getValue();
        if (prop != null) {
            inputContentUI.fieldObservationValue.setMatrix(prop.copy());
        } else {
            inputContentUI.fieldObservationValue.setMatrix(null);
        }
    }

    protected void createObservationValueMatrix() {
        createObservationValueMatrix(inputContentUI.getBean());
        setObservationValueMatrix();
    }
    
    public void loadObservationValueMatrix() {
        try {
            Observation observation = inputContentUI.getBean();
            if (observation != null) {
                File file = JaxxFileChooser.forLoadingFile()
                        .setApprovalText(t("isisfish.common.ok"))
                        .setPatternOrDescriptionFilters(".*\\.csv", "MatrixND csv file")
                        .setUseAcceptAllFileFilter(true).choose();
                if (file != null) {
                    MatrixND newMat = MatrixFactory.getInstance().create(file);
                    observation.setValue(newMat);
                    observation.update();
                    setObservationValueMatrix();
                }
            }
        } catch (Exception eee) {
            if (log.isErrorEnabled()) {
                log.error("Can't load matrix observation", eee);
            }
            ErrorHelper.showErrorDialog(t("isisfish.error.input.matrix.load"), eee);
        }
    }

    public void createObservationValueMatrix(Observation observation) {
        try {
            String val = JOptionPane.showInputDialog(inputContentUI, t("isisfish.message.observation.value.dimensions"),
                t("isisfish.common.newMatrix"), JOptionPane.QUESTION_MESSAGE);

            if (StringUtils.isNotBlank(val)) {
                String[] values = val.trim().split("(,|;|\\*|x|\\s)");
                
                List<Integer> dims = new ArrayList<>();
                for (String value : values) {
                    if (!value.isEmpty() && StringUtils.isNumeric(value)) {
                        dims.add(Integer.valueOf(value));
                    }
                }

                int[] dim = new int[dims.size()];
                for (int i = 0; i < dims.size(); i++) {
                    dim[i] = dims.get(i);
                }
                MatrixND newMat = MatrixFactory.getInstance().create(dim);

                MatrixND mat = observation.getValue();
                if (mat != null) {
                    newMat.paste(mat);
                }
                observation.setValue(newMat);
                observation.update();
            }
        } catch (Exception eee) {
            if (log.isErrorEnabled()) {
                log.error("Can't create matrix", eee);
            }
            ErrorHelper.showErrorDialog(t("isisfish.error.input.matrix.create"), eee);
        }
    }

    protected void observationValueMatrixChanged(MatrixPanelEvent event) {
        MatrixND mat = inputContentUI.fieldObservationValue.getMatrix();
        if (inputContentUI.getBean() != null && mat != null) {
            inputContentUI.getBean().setValue(mat.copy());
        }
    }
}
