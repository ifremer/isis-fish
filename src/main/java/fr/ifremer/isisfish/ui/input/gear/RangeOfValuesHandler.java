/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.gear;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.ifremer.isisfish.types.RangeOfValues;
import fr.ifremer.isisfish.ui.input.InputContentHandler;
import fr.ifremer.isisfish.ui.models.common.GenericComboModel;

/**
 * RangeOfValues handler.
 */
public class RangeOfValuesHandler extends InputContentHandler<RangeOfValuesUI> {

    /** Class logger. */
    private static final Log log = LogFactory.getLog(RangeOfValuesHandler.class);

    protected boolean init;

    protected RangeOfValuesHandler(RangeOfValuesUI inputContentUI) {
        super(inputContentUI);
    }

    protected void afterInit() {

        inputContentUI.addPropertyChangeListener(RangeOfValuesUI.PROPERTY_BEAN, evt -> {

            GenericComboModel<String> gearParamTypeModel = new GenericComboModel<>();
            if (evt.getNewValue() == null) {
                inputContentUI.fieldGearParamPossibleValue.setText("");
                inputContentUI.fieldGearParamType.setSelectedItem(null);
                inputContentUI.fieldGearParamType.setModel(gearParamTypeModel);
            }
            if (evt.getNewValue() != null) {
                init = true;

                List<String> values = new ArrayList<>();
                values.addAll(Arrays.asList(RangeOfValues.getPossibleTypes()));
                gearParamTypeModel.setElementList(values);

                inputContentUI.fieldGearParamType.setModel(gearParamTypeModel);
                if (inputContentUI.getBean().getPossibleValue() != null) {
                    inputContentUI.fieldGearParamType.setSelectedItem(inputContentUI.getBean().getPossibleValue().getType());
                }

                init = false;
            }
        });
    }

    protected void gearParamChanged() {
        if (inputContentUI.fieldGearParamType.getSelectedItem() != null && !init) {
            inputContentUI.getBean().setPossibleValue(new RangeOfValues(inputContentUI.fieldGearParamType.getSelectedItem().toString()
                    .concat("[" + inputContentUI.fieldGearParamPossibleValue.getText() + "]")));
        }
    }
}
