/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.setofvessels;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

import fr.ifremer.isisfish.ui.input.InputContentHandler;

/**
 * set of vessels handler.
 */
public class SetOfVesselsHandler extends InputContentHandler<SetOfVesselsUI> {

    protected SetOfVesselsHandler(SetOfVesselsUI inputContentUI) {
        super(inputContentUI);
    }

    protected void afterInit() {
        setupGotoNextPath(t("isisfish.input.continueStrategies"), n("isisfish.input.tree.strategies"));

        // install change listener
        // (depends on sensitivity can't be done on constructor)
        inputContentUI.installChangeListener(inputContentUI.setOfVesselsTab);
    }
}
