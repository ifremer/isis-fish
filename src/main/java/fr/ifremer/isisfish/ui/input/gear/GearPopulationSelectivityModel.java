/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2010 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.gear;

import static org.nuiton.i18n.I18n.t;

import java.awt.Component;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellRenderer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.ifremer.isisfish.entities.Equation;
import fr.ifremer.isisfish.entities.Gear;
import fr.ifremer.isisfish.entities.Selectivity;
import fr.ifremer.isisfish.ui.sensitivity.SensitivityTableModel;

/**
 * Table model for {@link Gear}#{@link Selectivity}.
 * 
 * Columns :
 * <ul>
 * <li>selectivity population name</li>
 * <li>selectivity equation</li>
 * </ul>
 *
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class GearPopulationSelectivityModel extends AbstractTableModel implements TableCellRenderer, SensitivityTableModel {

    /** Log. */
    private static Log log = LogFactory.getLog(GearPopulationSelectivityModel.class);
    
    /** serialVersionUID. */
    private static final long serialVersionUID = 3169786638868209920L;

    /** Columns names. */
    public final static String[] COLUMN_NAMES = {
            t("isisfish.common.population"),
            t("isisfish.common.equation")
    };

    protected List<Selectivity> selectivities;

    /**
     * Empty constructor.
     */
    public GearPopulationSelectivityModel() {
        this(null);
    }

    /**
     * Constructor with data.
     *  
     * @param selectivities initial selectivities
     */
    public GearPopulationSelectivityModel(
            List<Selectivity> selectivities) {
        this.selectivities = selectivities;
    }

    /**
     * Set target species list.
     * 
     * @param selectivities the selectivities to set
     */
    public void setSelectivities(List<Selectivity> selectivities) {
        this.selectivities = selectivities;
    }

    /**
     * Get selectivity list.
     * 
     * @return selectivity list
     */
    public List<Selectivity> getSelectivities() {
        return this.selectivities;
    }

    @Override
    public int getColumnCount() {
        return COLUMN_NAMES.length;
    }

    @Override
    public int getRowCount() {
        int rows = 0;
        if (selectivities != null) {
            rows = selectivities.size();
        }
        return rows;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        Object result;

        Selectivity selectivity = selectivities.get(rowIndex);
        switch (columnIndex) {
        case 0:
            result = selectivity.getPopulation().getName();
            break;
        case 1:
            result = selectivity.getEquation();
            break;
        default:
            throw new IndexOutOfBoundsException("No such column " + columnIndex);
        }

        return result;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {

        Class<?> result;
        
        switch (columnIndex) {
        case 0:
            result = String.class;
            break;
        case 1:
            result = Equation.class;
            break;
        default:
            throw new IndexOutOfBoundsException("No such column " + columnIndex);
        }
        
        return result;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return COLUMN_NAMES[columnIndex];
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return columnIndex > 0;
    }

    @Override
    public void setValueAt(Object value, int rowIndex, int columnIndex) {
        
        if (log.isDebugEnabled()) {
            log.debug("Cell edition (column " + columnIndex + ") = " + value);
        }
        
        Selectivity selectivity = selectivities.get(rowIndex);
        switch (columnIndex) {
        case 1:
            Equation eq = (Equation)value;
            // two events for event to be fired
            selectivity.setEquation(null);
            selectivity.setEquation(eq);
            break;
        default:
            throw new IndexOutOfBoundsException("Can't edit column " + columnIndex);
        }

    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value,
            boolean isSelected, boolean hasFocus, int row, int column) {

        Component c;
        switch (column) {
        case 0:
            c = new JLabel(value.toString());
            break;
        case 1:
            Equation equation = (Equation)value;
            c = new JButton(equation.getName() + "(" + equation.getCategory() + ")");
            break;
        default:
            throw new IndexOutOfBoundsException("No such column " + column);
        }
        return c;
    }

    @Override
    public String getPropertyAtColumn(int column) {
        String result = null;
        if (column == 1) {
            result = "equation";
        }
        return result;
    }

    @Override
    public Object getBeanAtRow(int rowIndex) {
        Object result;
        result = selectivities.get(rowIndex);
        return result;
    }
}
