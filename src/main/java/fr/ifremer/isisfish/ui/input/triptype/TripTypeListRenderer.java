/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2010 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.triptype;

import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;

import fr.ifremer.isisfish.entities.TripType;

/**
 * Renderer pour liste des trip types.
 *
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class TripTypeListRenderer extends DefaultListCellRenderer {

    /** serialVersionUID. */
    private static final long serialVersionUID = 1113492654780402245L;

    /** Append trip type details. */
    protected boolean appendDetails;

    public TripTypeListRenderer() {
        this(false);
    }
    
    public TripTypeListRenderer(boolean appendDetails) {
        this.appendDetails = appendDetails;
    }

    @Override
    public Component getListCellRendererComponent(JList<?> list, Object value,
            int index, boolean isSelected, boolean cellHasFocus) {

        // this must be used to have alternated highlight rows and default
        // selection color
        JLabel c = (JLabel) super.getListCellRendererComponent(list, value,
                index, isSelected, cellHasFocus);

        TripType tripType = (TripType) value;
        if (tripType != null) {
            if (appendDetails) {
                c.setText(tripType.getName() + "(" + tripType.getTripDuration() + ":"
                        + tripType.getMinTimeBetweenTrip() + ")");
            } else {
                c.setText(tripType.getName());
            }
        }

        return c;
    }
}
