/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.population;

import static org.nuiton.i18n.I18n.t;

import java.awt.BorderLayout;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import jaxx.runtime.JAXXUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;

import fr.ifremer.isisfish.IsisFishDAOHelper;
import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.entities.PopulationDAO;
import fr.ifremer.isisfish.entities.PopulationGroup;
import fr.ifremer.isisfish.entities.Species;
import fr.ifremer.isisfish.ui.input.InputContentHandler;
import fr.ifremer.isisfish.ui.input.InputUI;

/**
 * Population handler.
 */
public class PopulationBasicsHandler extends InputContentHandler<PopulationBasicsUI> {

    /** Class logger. */
    private static final Log log = LogFactory.getLog(PopulationBasicsHandler.class);

    protected boolean init;

    protected PopulationBasicsHandler(PopulationBasicsUI inputContentUI) {
        super(inputContentUI);
    }

    protected void afterInit() {
        inputContentUI.addPropertyChangeListener(PopulationBasicsUI.PROPERTY_BEAN, evt -> {
            init = true;
            if (evt.getNewValue() == null) {
                inputContentUI.fieldPopulationBasicsName.setText("");
                inputContentUI.fieldPopulationBasicsGeographicID.setText("");
                inputContentUI.fieldPopulationBasicsNbClasses.setText("");
                inputContentUI.fieldPopulationBasicsComment.setText("");
                inputContentUI.tableAgeLength.setModel(new DefaultTableModel());
            }
            if (evt.getNewValue() != null) {
                setTableAgeLengthModel();
            }
            init = false;
        });
    }
    
    public void refresh() {
        Population population = inputContentUI.getSaveVerifier().getEntity(Population.class);
        
        // add null before, for second to be considered as a changed event
        // otherwize, setBean has no effect
        inputContentUI.setBean(null);
        inputContentUI.setBean(population);
        
        if (inputContentUI.getBean() != null) {
            setTableAgeLengthModel();
        }
        //getSaveVerifier().addCurrentPanel(growthEquation, growthReverseEquation);
    }

    /**
     * Open creation classe wizard after confirmation.
     */
    protected void createGroups() {

        int response = JOptionPane.showConfirmDialog(inputContentUI, t("isisfish.populationBasics.confirmCreateGroups"),
            t("isisfish.common.confirm"), JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
        
        if (response == JOptionPane.YES_OPTION) {
            JFrame wizardFrame = new JFrame();
            wizardFrame.setLayout(new BorderLayout());
            WizardGroupCreationUI wizard = new WizardGroupCreationUI(inputContentUI);
            wizard.getHandler().initParent(inputContentUI);
            wizardFrame.add(wizard, BorderLayout.CENTER);
            wizardFrame.setTitle(t("isisfish.wizardGroupCreation.title"));
            Species species = inputContentUI.getBean().getSpecies();
            wizard.getHandler().setAgeType(species.isAgeGroupType());
            if (wizard.getHandler().isAgeType()) {
                wizard.getHandler().setCard("singleGroupAge");
            } else {
                wizard.getHandler().setCard("beginGroupLength");
            }
            wizardFrame.pack();
            wizardFrame.setLocationRelativeTo(inputContentUI);
            wizardFrame.setVisible(true);
        }
       
    }

    protected void setTableAgeLengthModel() {
        List<PopulationGroup> popGroup = inputContentUI.getBean().getPopulationGroup();
        if (popGroup != null){
            DefaultTableModel model = new DefaultTableModel(2, popGroup.size() + 1);
            model.setValueAt("Age", 0, 0);
            model.setValueAt("Lengths", 1, 0);
            int cnt = 1;
            for (PopulationGroup pg : popGroup){
                model.setValueAt(pg.getAge(), 0, cnt);
                model.setValueAt(pg.getLength(), 1, cnt);
                cnt++;
            }
            inputContentUI.tableAgeLength.setModel(model);
        }
    }

    protected void create() {
        // find species node
        InputUI inputUI = inputContentUI.getContextValue(InputUI.class, JAXXUtil.PARENT);
        Species species = inputUI.getHandler().findSpecies();
        // create node and select it
        Population population = createPopulation(inputContentUI.getTopiaContext(), species);
        inputUI.getHandler().insertTreeNode(Population.class, population);
        inputContentUI.setInfoText(t("isisfish.message.creation.finished"));
    }
    
    protected Population createPopulation(TopiaContext context, Species species) {
        Population pop;
        if (log.isTraceEnabled()) {
            log.trace("createPopulation called");
        }
        try {
            String name = "Population_new";

            PopulationDAO dao = IsisFishDAOHelper.getPopulationDAO(context);
            pop = dao.create();
            pop.setName(name);

            species.addPopulation(pop);
            pop.setSpecies(species);
            species.update();
            pop.update();
        } catch (TopiaException eee) {
            throw new IsisFishRuntimeException(t("isisfish.error.input.addentity", "Population"), eee);
        }
        return pop;
    }
}
