/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2015 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.check;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Classe permettant de créer le résultat d'une vérification de la bonne saisie des objets.
 *
 * Created: 9 janv. 2004
 *
 * @author Benjamin Poussin &lt;poussin@codelutin.com&gt;
 * @version $Revision$
 *
 * Mise a jour: $Date$
 * par : $Author$
 */
public class CheckResult { // CheckResult

    protected Set<Object> order = new LinkedHashSet<>();

    protected Map<Object, List<String>> info = new HashMap<>();
    protected Map<Object, List<String>> warning = new HashMap<>();
    protected Map<Object, List<String>> error = new HashMap<>();

    public StringBuffer getInfo() {
        StringBuffer result = new StringBuffer();
        for (Object o : order) {
            StringBuffer tmp = getInfo(o);
            if (tmp.length() != 0) {
                String title = o.toString();
                tmp.insert(0, "\n");
                for (int e = 0; e < title.length(); e++) {
                    tmp.insert(0, "=");
                }
                tmp.insert(0, title + "\n");
                tmp.append("\n");
                result.append(tmp.toString());
            }
        }
        return result;
    }

    public StringBuffer getWarning() {
        StringBuffer result = new StringBuffer();
        for (Object o : order) {
            StringBuffer tmp = getWarning(o);
            if (tmp.length() != 0) {
                String title = o.toString();
                tmp.insert(0, "\n");
                for (int e = 0; e < title.length(); e++) {
                    tmp.insert(0, "=");
                }
                tmp.insert(0, title + "\n");
                tmp.append("\n");
                result.append(tmp.toString());
            }
        }
        return result;
    }

    public StringBuffer getError() {
        StringBuffer result = new StringBuffer();
        for (Object o : order) {
            StringBuffer tmp = getError(o);
            if (tmp.length() != 0) {
                String title = o.toString();
                tmp.insert(0, "\n");
                for (int e = 0; e < title.length(); e++) {
                    tmp.insert(0, "=");
                }
                tmp.insert(0, title + "\n");
                tmp.append("\n");
                result.append(tmp.toString());
            }
        }
        return result;
    }

    public StringBuffer getInfo(Object o) {
        StringBuffer result = new StringBuffer();
        List<String> list = info.get(o);
        if (list != null && list.size() != 0) {
            result.append("Info\n");
            result.append("----\n");
            for (String aList : list) {
                result.append(aList);
                result.append("\n");
            }
        }
        return result;
    }

    public StringBuffer getWarning(Object o) {
        StringBuffer result = new StringBuffer();
        List<String> list = warning.get(o);
        if (list != null && list.size() != 0) {
            result.append("Problème\n");
            result.append("--------\n");
            for (String aList : list) {
                result.append(aList);
                result.append("\n");
            }
        }
        return result;
    }

    public StringBuffer getError(Object o) {
        StringBuffer result = new StringBuffer();
        List<String> list = error.get(o);
        if (list != null && list.size() != 0) {
            result.append("Erreur\n");
            result.append("------\n");
            for (String aList : list) {
                result.append(aList);
                result.append("\n");
            }
        }
        return result;
    }

    public StringBuffer getAll(Object o) {
        StringBuffer result = new StringBuffer();
        result.append(getError(o).toString());
        result.append(getWarning(o).toString());
        result.append(getInfo(o).toString());

        if (result.length() != 0) {
            String title = o.toString();
            result.insert(0, "\n");
            for (int i = 0; i < title.length(); i++) {
                result.insert(0, "=");
            }
            result.insert(0, title + "\n");
        }
        result.append("\n");
        return result;
    }

    public StringBuffer getAll() {
        StringBuffer result = new StringBuffer();
        for (Object anOrder : order) {
            result.append(getAll(anOrder));
        }
        return result;
    }

    /**
    * Permet d'ajouter une info sur un objet.
    * 
    * @param o l'objet sur lequel le message porte
    * @param message le message à ajouter
    */
    public void addInfo(Object o, String message) {
        order.add(o);
        List<String> list = info.computeIfAbsent(o, k -> new LinkedList<>());
        list.add(message);
    }

    /**
    * Permet d'ajouter un avertissement sur un objet.
    * 
    * @param o l'objet sur lequel le message porte
    * @param message le message à ajouter
    */
    public void addWarning(Object o, String message) {
        order.add(o);
        List<String> list = warning.computeIfAbsent(o, k -> new LinkedList<>());
        list.add(message);
    }

    /**
    * Permet d'ajouter une error sur un objet.
    * 
    * @param o l'objet sur lequel le message porte
    * @param message le message à ajouter
    */
    public void addError(Object o, String message) {
        order.add(o);
        List<String> list = error.computeIfAbsent(o, k -> new LinkedList<>());
        list.add(message);
    }

} // CheckResult
