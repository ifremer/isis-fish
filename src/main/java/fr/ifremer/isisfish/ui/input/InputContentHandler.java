/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JButton;
import javax.swing.JComponent;

import fr.ifremer.isisfish.util.UIUtil;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.swing.BlockingLayerUI;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntityContextable;

import fr.ifremer.isisfish.ui.CommonHandler;
import fr.ifremer.isisfish.ui.sensitivity.wizard.FactorWizardHandler;
import fr.ifremer.isisfish.ui.sensitivity.wizard.FactorWizardUI;

/**
 * Common handler for all input ui.
 */
public abstract class InputContentHandler<T extends InputContentUI<? extends TopiaEntityContextable>> extends CommonHandler implements PropertyChangeListener {

    /** Class logger. */
    private static final Log log = LogFactory.getLog(InputContentHandler.class);
 
    protected T inputContentUI;

    /** Go to next button instance (when available). */
    protected JButton nextButton;

    protected InputContentHandler(T inputContentUI) {
        this.inputContentUI = inputContentUI;
        this.inputContentUI.addPropertyChangeListener(this);
    }

    /**
     * Action appelée lors du clic sur un layer (sensitivity).
     * 
     * @param inputContentUI inputContentUI
     * @param e l'event initial intercepté par le layer
     */
    public void accept(InputContentUI<?> inputContentUI, ActionEvent e) {

        // get clicked component info
        JComponent source = (JComponent) e.getSource();
        Class<? extends TopiaEntityContextable> sensitivityBeanClass = (Class<? extends TopiaEntityContextable>)source.getClientProperty("sensitivityBean");
        String sensitivityBeanID = (String)source.getClientProperty("sensitivityBeanID");
        String sensitivityMethod = (String)source.getClientProperty("sensitivityMethod");

        if (log.isDebugEnabled()) {
            log.debug("Event intercepted " + source);
            log.debug(" client property (bean) : " + sensitivityBeanClass);
            log.debug(" client property (beanID) : " + sensitivityBeanID);
            log.debug(" client property (method) : " + sensitivityMethod);
        }

        displayFactorWizard(inputContentUI, sensitivityBeanClass, sensitivityBeanID, sensitivityMethod);
    }
    
    public void displayFactorWizard(InputContentUI<?> inputContentUI, Class<? extends TopiaEntityContextable> sensitivityBeanClass,
            String sensitivityBeanID, String sensitivityMethod) {

        // get bean for component class info
        TopiaEntityContextable bean;
        if (sensitivityBeanID == null) {
            bean = inputContentUI.getSaveVerifier().getEntity(sensitivityBeanClass);
        } else {
            bean = inputContentUI.getSaveVerifier().getEntity(sensitivityBeanClass, sensitivityBeanID);
        }

        if (bean != null) {
            FactorWizardUI factorWizardUI = new FactorWizardUI(inputContentUI);
            FactorWizardHandler handler = factorWizardUI.getHandler();
            handler.initNewFactor(factorWizardUI, bean, sensitivityMethod);
            factorWizardUI.pack();
            factorWizardUI.setLocationRelativeTo(inputContentUI);
            UIUtil.setIconImage(factorWizardUI);
            factorWizardUI.setVisible(true);
        } else {
            if (log.isErrorEnabled()) {
                log.error("Can't find bean in current verifier (sensitivityBeanClass = " + sensitivityBeanClass + ", sensitivityBeanID = " + sensitivityBeanID + ")");
            }
        }
    }

    /**
     * Ajout un bouton "Aller vers..." en bas de l'UI.
     *
     * @param nextTitle titre du boutton
     * @param nextPath path dans l'arbre
     */
    protected void setupGotoNextPath(String nextTitle, String nextPath) {
        nextButton = new JButton(nextTitle);
        nextButton.addActionListener(e -> goTo(nextPath));
        nextButton.setIcon(SwingUtil.createImageIcon("fatcow/page_white_go.png"));
        this.inputContentUI.add(SwingUtil.boxComponentWithJxLayer(nextButton), BorderLayout.SOUTH);
    }

    protected void goTo(String nextPath) {
        // il ne faut pas appeler le parent
        // on ne sais jamais de quel type est le parent
        InputUI inputUI = this.inputContentUI.getParentContainer(InputUI.class);
        if (inputUI != null) {
            inputUI.getHandler().setTreeSelection(this.inputContentUI, nextPath);
        }
    }

    /**
     * Fix layer for next button in "blocking state".
     * Sensitivity value in unknown when setupGotoNextPath is called.
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (nextButton != null && evt.getPropertyName().equals(InputContentUI.PROPERTY_SENSITIVITY) && (Boolean)evt.getNewValue()) {
            BlockingLayerUI clone = this.inputContentUI.getLayerUI().clone();
            clone.setBlock(true);
            SwingUtil.getLayer(nextButton).setUI(clone);
        }
    }
}
