/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.tree;

import static org.nuiton.i18n.I18n.t;

import java.awt.Component;

import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.persistence.TopiaEntity;

import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.entities.Cell;
import fr.ifremer.isisfish.entities.FisheryRegion;
import fr.ifremer.isisfish.entities.Gear;
import fr.ifremer.isisfish.entities.Metier;
import fr.ifremer.isisfish.entities.Observation;
import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.entities.Port;
import fr.ifremer.isisfish.entities.SetOfVessels;
import fr.ifremer.isisfish.entities.Species;
import fr.ifremer.isisfish.entities.Strategy;
import fr.ifremer.isisfish.entities.TripType;
import fr.ifremer.isisfish.entities.VesselType;
import fr.ifremer.isisfish.entities.Zone;

/**
 * Renderer pour les noeud {@link FisheryTreeNode} de l'arbre.
 * 
 * @author chatellier
 * @version $Revision$
 * @since 3.4.0.0
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class FisheryTreeRenderer extends DefaultTreeCellRenderer {

    /** serialVersionUID. */
    private static final long serialVersionUID = -9045080128695551208L;

    protected FisheryDataProvider provider;

    public FisheryTreeRenderer(FisheryDataProvider provider) {
        this.provider = provider;
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value,
            boolean sel, boolean expanded, boolean leaf, int row,
            boolean hasFocus) {

        String stringValue = null;
        if (value instanceof FisheryTreeNode) {
            FisheryTreeNode node = (FisheryTreeNode)value;
            
            if (!node.isStaticNode() || node.isStringNode()) {
                stringValue = t(node.getId());
            } else {
                FisheryRegion fisheryRegion = provider.getFisheryRegion();
                TopiaContext topiaContext = fisheryRegion.getTopiaContext();
                try {
                    TopiaEntity entity = topiaContext.findByTopiaId(node.getId());

                    // il arrive que ca bug
                    // lorsqu'un renderer essaye de rendre quelque chose qui n'existe plus
                    // cela arrive notement sur le changement de region
                    // lorsqu'une est deja chargée
                    if (entity != null) {
                        if (FisheryRegion.class.isAssignableFrom(entity.getClass())) {
                            stringValue = ((FisheryRegion)entity).getName();
                        } else if (Cell.class.isAssignableFrom(entity.getClass())) {
                            stringValue = ((Cell)entity).getName();
                        } else if (Gear.class.isAssignableFrom(entity.getClass())) {
                            stringValue = ((Gear)entity).getName();
                        } else if (Metier.class.isAssignableFrom(entity.getClass())) {
                            stringValue = ((Metier)entity).getName();
                        } else if (Population.class.isAssignableFrom(entity.getClass())) {
                            stringValue = ((Population)entity).getName();
                        } else if (Port.class.isAssignableFrom(entity.getClass())) {
                            stringValue = ((Port)entity).getName();
                        } else if (SetOfVessels.class.isAssignableFrom(entity.getClass())) {
                            stringValue = ((SetOfVessels)entity).getName();
                        } else if (Species.class.isAssignableFrom(entity.getClass())) {
                            stringValue = ((Species)entity).getName();
                        } else if (Strategy.class.isAssignableFrom(entity.getClass())) {
                            stringValue = ((Strategy)entity).getName();
                        } else if (TripType.class.isAssignableFrom(entity.getClass())) {
                            stringValue = ((TripType)entity).getName();
                        } else if (VesselType.class.isAssignableFrom(entity.getClass())) {
                            stringValue = ((VesselType)entity).getName();
                        } else if (Zone.class.isAssignableFrom(entity.getClass())) {
                            stringValue = ((Zone)entity).getName();
                        } else if (Observation.class.isAssignableFrom(entity.getClass())) {
                            stringValue = ((Observation)entity).getName();
                        }
                    }
                } catch (TopiaException eee) {
                    throw new IsisFishRuntimeException("Can't get entity for id " + node.getId(), eee);
                }
            }
        }
        return super.getTreeCellRendererComponent(tree, stringValue, sel, expanded, leaf,
                row, hasFocus);
    }
}
