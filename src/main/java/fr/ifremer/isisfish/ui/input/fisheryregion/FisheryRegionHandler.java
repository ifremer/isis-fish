/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 - 2017 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.fisheryregion;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

import java.awt.geom.Point2D;
import java.io.File;
import java.util.Collections;
import java.util.List;

import javax.swing.DefaultListModel;

import fr.ifremer.isisfish.ui.input.InputContext;
import fr.ifremer.isisfish.util.IsisFileUtil;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaContext;

import fr.ifremer.isisfish.IsisFishDAOHelper;
import fr.ifremer.isisfish.datastore.RegionStorage;
import fr.ifremer.isisfish.entities.Cell;
import fr.ifremer.isisfish.entities.CellDAO;
import fr.ifremer.isisfish.entities.FisheryRegion;
import fr.ifremer.isisfish.ui.input.InputContentHandler;
import fr.ifremer.isisfish.ui.input.InputUI;
import fr.ifremer.isisfish.ui.input.check.CheckRegion;
import fr.ifremer.isisfish.ui.input.check.CheckResult;
import fr.ifremer.isisfish.ui.input.check.CheckResultFrame;
import fr.ifremer.isisfish.util.ErrorHelper;
import fr.ifremer.isisfish.util.CellPointcomparator;

/**
 * FisheryRegion handler.
 */
public class FisheryRegionHandler extends InputContentHandler<FisheryRegionUI> {

    /** Class logger. */
    private static final Log log = LogFactory.getLog(FisheryRegionHandler.class);

    protected FisheryRegionHandler(FisheryRegionUI inputContentUI) {
        super(inputContentUI);
    }

    protected void afterInit() {
        setupGotoNextPath(t("isisfish.input.continueCells"), n("isisfish.input.tree.cells"));
        
        inputContentUI.addPropertyChangeListener(FisheryRegionUI.PROPERTY_BEAN, evt -> {
            if (evt.getNewValue() == null) {
                // FIXME poussin 20180109 ne faudrait-il pas vider les cartes existantes ?
            }
            if (evt.getNewValue() != null) {
                setFieldMapfilesModel(inputContentUI.getBean());
            }
        });
    }

    public void refresh() {
        FisheryRegion region = inputContentUI.getSaveVerifier().getEntity(FisheryRegion.class);

        // add null before, for second to be considered as a changed event
        // otherwize, setBean has no effect
        inputContentUI.setBean(null);
        inputContentUI.setBean(region);
    }

    protected void setFieldMapfilesModel(FisheryRegion region) {
        DefaultListModel<String> model = new DefaultListModel<>();
        List<String> mapList = region.getMapFileList();
        if (mapList != null) {
            int cnt = 0;
            for (String map : mapList) {
                model.add(cnt, map);
                cnt++;
            }
        }
        inputContentUI.fieldMapfiles.setModel(model);
    }

    protected void addMap() {
        addMap(inputContentUI.getBean());
        setFieldMapfilesModel(inputContentUI.getBean());
    }

    protected void delMap() {
        removeMap(inputContentUI.getBean(), inputContentUI.fieldMapfiles.getSelectedValuesList());
        setFieldMapfilesModel(inputContentUI.getBean());
    }
    
    /**
     * Add new map in region.
     * 
     * Since isis-fsih 3.3.0.0, this method supports a multiple file format :
     *  - http://openmap.bbn.com/cgi-bin/faqw.py?req=all#9.1
     * 
     * @param fisheryRegion fishery region
     */
    public void addMap(FisheryRegion fisheryRegion) {
        if (log.isTraceEnabled()) {
            log.trace("AddMap called");
        }

        // Openmap suported fileformat
        String[] fileFormats = {
                // ESRI (http://www.esri.com) Shapefiles
                ".*\\.shp", "ESRI Shapefiles (.shp)",

                // NIMA (http://www.nima.mil)
                ".*\\.dcw", "Digital Chart of the World (.dcw)",
                ".*\\.vpf", "Vector Product Format (.vpf)",
                ".*\\.vmap", "Vector Map (.vmap)",
                ".*\\.cadrg", "Compressed ARC Digitized Raster Graphics (.cadrg)",
                ".*\\.cib", "Controlled Image Base (.cib)",
                ".*\\.rpf", "Raster Product Format (.rpf)",
                // seams to be a special format with multiples files
                //".*\\.dt[0-2]+", "Digital Terrain Elevation Data (levels 0, 1, 2) (.dt0, .dt0, .dt2)",

                // MapInfo (http://www.mapinfo.com) files (.mif)
                ".*\\.mif", "MapInfo (.mif)",

                // ArcInfo (.e00) files.
                ".*\\.e00", "ArcInfo (.e00)"
        };

        File inputMap = IsisFileUtil.getFile(fileFormats);
        try {
            if (inputMap != null) {

                // copy inputMapFormat to region map directory
                // since isis-fish-3.3.0.0, we store maps with extension
                String filename = inputMap.getName();

                // not working with different name
                // RegionStorage regionStorage = RegionStorage.getRegion(fisheryRegion.getName());
                RegionStorage regionStorage = InputContext.getStorage();
                File destDir = regionStorage.getMapRepository();

                // copy map file
                File destMap = new File(destDir, filename);
                FileUtils.copyFile(inputMap, destMap);

                List<String> maps = fisheryRegion.getMapFileList();
                maps.add(filename);
                fisheryRegion.setMapFileList(maps);

                inputContentUI.getCellMap().getMapDataProvider().regionChanged();
            }
        } catch (Exception eee) {
            if (log.isErrorEnabled()) {
                log.error("Can't copy map file for : " + inputMap, eee);
            }
            ErrorHelper.showErrorDialog(t("isisfish.error.region.addmap"), eee);
        }
    }

    /**
     * Remove map in a region.
     * 
     * @param fisheryRegion fishery region
     * @param selectedMaps la liste de map a retirer
     */
    public void removeMap(FisheryRegion fisheryRegion, List<String> selectedMaps) {
        if (log.isTraceEnabled()) {
            log.trace("removeMap called");
        }

        try {
            List<String> maps = fisheryRegion.getMapFileList();

            // not working with different name
            // RegionStorage regionStorage = RegionStorage.getRegion(fisheryRegion.getName());
            RegionStorage regionStorage = InputContext.getStorage();
            File mapDir = regionStorage.getMapRepository();

            for (String mapName : selectedMaps) {
                maps.remove(mapName);
                // remove on disk too, if possible
                File mapFile = new File(mapDir, mapName);
                mapFile.delete();
                if (log.isDebugEnabled()) {
                    log.debug("Removing map file : " + mapFile);
                }

                // special case, for some format, an index is created
                if (mapName.endsWith(".shp")) {
                    String indexName = mapName.replaceAll("\\.shp$", ".ssx");
                    File indexFile = new File(mapDir, indexName);
                    indexFile.delete();
                    if (log.isDebugEnabled()) {
                        log.debug("Removing index file : " + indexFile);
                    }
                }
            }

            fisheryRegion.setMapFileList(maps);

            inputContentUI.getCellMap().getMapDataProvider().regionChanged();

        } catch (Exception eee) {
            if (log.isErrorEnabled()) {
                log.error("Can't remove map", eee);
            }
            ErrorHelper.showErrorDialog(t("isisfish.error.region.removemap"),
                    eee);
        }
    }

    protected void check() {
        checkFisheryRegion(inputContentUI.getBean());
        inputContentUI.setInfoText(t("isisfish.message.check.finished"));
    }
    
    public void checkFisheryRegion(FisheryRegion fisheryRegion) {
        try {
            if (log.isTraceEnabled()) {
                log.trace("check called: ");
            }
            CheckResult result = new CheckResult();
            CheckRegion.check(fisheryRegion, result);
            CheckResultFrame dialog = new CheckResultFrame();
            dialog.setCheckResult(result);
            dialog.setLocationByPlatform(true);
            dialog.setVisible(true);
        } catch (Exception eee) {
            if (log.isErrorEnabled()) {
                log.error(t("isisfish.error.region.check"), eee);
            }
            ErrorHelper.showErrorDialog(t("isisfish.error.region.check"), eee);
        }
    }

    protected void save() {
        inputContentUI.setInfoText(t("isisfish.message.checking.cell"));

        // this make save done by verifier instead of saveFisheryRegion
        // and refresh tree is not working
        inputContentUI.getSaveVerifier().reset();

        // save generating cells
        saveFisheryRegion(inputContentUI.getBean());

        // reload tree
        InputUI inputUI = inputContentUI.getParentContainer(InputUI.class);
        inputUI.getHandler().reloadFisheryTree();

        inputContentUI.setInfoText(t("isisfish.message.save.finished"));
    }
    
    public void saveFisheryRegion(FisheryRegion fisheryRegion) {
        if (log.isTraceEnabled()) {
            log.trace("save called");
        }
        try {
            TopiaContext isisContext = fisheryRegion.getTopiaContext();

            // il faut peut-etre creer ou supprimer des mailles
            CellPointcomparator cellPointcomparator = new CellPointcomparator();
            CellDAO cellPS = IsisFishDAOHelper.getCellDAO(isisContext);
            List<Cell> cells = cellPS.findAll();
            cells.sort(cellPointcomparator);
            Point2D.Float point = new Point2D.Float();

            for (float lati = fisheryRegion.getMinLatitude(); lati < fisheryRegion.getMaxLatitude();
                 lati += fisheryRegion.getCellLengthLatitude()) {
                lati = Math.round(lati * 1000f);
                lati = lati / 1000.0f;
                for (float longi = fisheryRegion.getMinLongitude(); longi < fisheryRegion.getMaxLongitude();
                     longi += fisheryRegion.getCellLengthLongitude()) {
                    longi = Math.round(longi * 1000f) / 1000.0f;
                    point.setLocation(lati, longi);
                    int position = Collections.binarySearch(cells, point, cellPointcomparator);
                    if (position >= 0) {
                        // deja existant on l'enleve de la liste, et on ne cree rien
                        cells.remove(position);
                    } else {
                        // n'existe pas on la cree
                        Cell cell = cellPS.create();
                        cell.setName("La" + lati + "Lo" + longi);
                        cell.setLatitude(lati);
                        cell.setLongitude(longi);
                        cell.setLand(false);
                        cell.update();
                    }
                }
            }

            // on est pas en autoUpdate donc il faut faire le update avant le commit
            fisheryRegion.update();

            // toutes les mailles restantes dans la liste sont des mailles en trop
            // on les supprimes

            for (Cell cell : cells) {
                // FIXME il faudrait aussi rechercher les objets dependants
                // des mailles que l'on va supprimer et demander confirmation
                // a l'utilisateur qu'il souhaite reellement supprimer tous
                // ces objets. Si ce n'est pas le cas, on sort tout de suite
                // avant le commit
                cellPS.delete(cell);
            }
            isisContext.commitTransaction();
        } catch (Exception eee) {
            if (log.isErrorEnabled()) {
                log.error(t("isisfish.error.region.save"), eee);
            }
            ErrorHelper.showErrorDialog(t("isisfish.error.region.save"), eee);
        }
    }
}
