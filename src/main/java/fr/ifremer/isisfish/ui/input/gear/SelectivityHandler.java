/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.gear;

import static org.nuiton.i18n.I18n.t;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.ifremer.isisfish.IsisFishDAOHelper;
import fr.ifremer.isisfish.entities.Equation;
import fr.ifremer.isisfish.entities.Gear;
import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.entities.Selectivity;
import fr.ifremer.isisfish.entities.SelectivityDAO;
import fr.ifremer.isisfish.entities.Species;
import fr.ifremer.isisfish.ui.input.InputContentHandler;
import fr.ifremer.isisfish.ui.models.common.GenericComboModel;
import fr.ifremer.isisfish.util.ErrorHelper;
import fr.ifremer.isisfish.ui.widget.editor.EquationTableEditor;

/**
 * Selectivity handler.
 */
public class SelectivityHandler extends InputContentHandler<SelectivityUI> {

    /** Class logger. */
    private static final Log log = LogFactory.getLog(SelectivityHandler.class);

    protected SelectivityHandler(SelectivityUI inputContentUI) {
        super(inputContentUI);
    }

    protected void afterInit() {
        inputContentUI.addPropertyChangeListener(SelectivityUI.PROPERTY_BEAN, evt -> {
            if (evt.getNewValue() == null) {
                inputContentUI.setPopulation(null);
                inputContentUI.selectivityTable.setModel(new GearPopulationSelectivityModel());
            }
            if (evt.getNewValue() != null) {
                refresh();
            }
        });
    }
    
    public void refresh() {

        //Gear gear = (Gear)inputContentUI.getSaveVerifier().getEntity(Gear.class);

        // add null before, for second to be considered as a changed event
        // otherwize, setBean has no effect
        //setBean(null);
        //setBean(gear);

        if (inputContentUI.getBean() != null) {
            setSelectivityTableModel();
            inputContentUI.fieldSelectivityPopulation.setModel(getSelectivityPopulationModel());
        }
    }

    protected void setSelectivityTableModel() {
        
        List<Selectivity> selectivitiesList = new ArrayList<>();
        
        // set model even if no selectivity
        // to clear data
        if (inputContentUI.getBean().getPopulationSelectivity() != null) {
            // move collection to list
            // and add all entity to verifier
            for (Selectivity oneSelectivity : inputContentUI.getBean().getPopulationSelectivity()) {
                inputContentUI.getSaveVerifier().addCurrentEntity(oneSelectivity);
                selectivitiesList.add(oneSelectivity);

                // hack to enable save button :(
                oneSelectivity.addPropertyChangeListener(evt -> inputContentUI.changeModel.setStayChanged(true));
            }
        }

        // set table model
        GearPopulationSelectivityModel model = new GearPopulationSelectivityModel(selectivitiesList);
        inputContentUI.selectivityTable.setModel(model);
        inputContentUI.selectivityTable.setDefaultRenderer(Equation.class, model);
        inputContentUI.selectivityTable.setDefaultEditor(Equation.class, new EquationTableEditor());
    }

    protected void addSelectivity() {
        addSelectivity(inputContentUI.getPopulation(),
                inputContentUI.selectivityEquation.getEditor().getText(), inputContentUI.getBean());
        setSelectivityTableModel();
    }

    protected void removeSelectivity() {
        GearPopulationSelectivityModel model = (GearPopulationSelectivityModel)inputContentUI.selectivityTable.getModel();
        Selectivity selectedSelectivity = model.getSelectivities().get(inputContentUI.selectivityTable.getSelectedRow());
        removeSelectivity(inputContentUI.getBean(), selectedSelectivity);
        inputContentUI.getSaveVerifier().removeCurrentEntity(selectedSelectivity.getTopiaId());
        setSelectivityTableModel();
        inputContentUI.removeSelectivityButton.setEnabled(false);
    }
    
    protected void addSelectivity(Population pop, String equation, Gear gear) {
        try {

            SelectivityDAO dao = IsisFishDAOHelper.getSelectivityDAO(pop
                    .getTopiaContext());
            Selectivity selectivity = dao.create();

            selectivity.setGear(gear);
            selectivity.setPopulation(pop);
            selectivity.getEquation().setContent(equation);
            selectivity.update();

            gear.addPopulationSelectivity(selectivity);
            gear.update();

        } catch (Exception eee) {
            if (log.isErrorEnabled()) {
                log.error("Can't add selectivity", eee);
            }
            ErrorHelper.showErrorDialog(t("isisfish.error.input.addentity",
                    "Selectivity"), eee);
        }
    }

    protected void removeSelectivity(Gear gear, Selectivity selectivity) {
        if (log.isTraceEnabled()) {
            log.trace("removeSelectivity called: " + selectivity);
        }
        try {
            if (gear != null && selectivity != null) {
                gear.removePopulationSelectivity(selectivity);
            }
        } catch (Exception eee) {
            if (log.isErrorEnabled()) {
                log.error("Can't remove TargetSpecies", eee);
            }
            ErrorHelper.showErrorDialog(t("isisfish.error.input.removeentity",
                    "Selectivity"), eee);
        }
    }

    protected GenericComboModel<Population> getSelectivityPopulationModel() {
        List<Species> species = inputContentUI.getFisheryRegion().getSpecies();
        List<Population> populations = new ArrayList<>();
        if (species != null) {
            for (Species s : species) {
                if (s.getPopulation() != null) {
                    populations.addAll(s.getPopulation());
                }
            }
        }
        GenericComboModel<Population> selectivityPopulationModel = new GenericComboModel<>(populations);
        return selectivityPopulationModel;
    }

    protected void selectivityChanged() {
        inputContentUI.setPopulation((Population)inputContentUI.fieldSelectivityPopulation.getSelectedItem());
    }
}
