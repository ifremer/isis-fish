/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2010 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.metier;

import static org.nuiton.i18n.I18n.t;

import java.awt.Component;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellRenderer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.ifremer.isisfish.entities.Equation;
import fr.ifremer.isisfish.entities.MetierSeasonInfo;
import fr.ifremer.isisfish.entities.TargetSpecies;
import fr.ifremer.isisfish.ui.sensitivity.SensitivityTableModel;

/**
 * Table model for {@link MetierSeasonInfo}#{@link TargetSpecies}.
 * 
 * Columns :
 * <ul>
 * <li>target species name</li>
 * <li>target species equation</li>
 * <li>target species primaryCatch</li>
 * </ul>
 *
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class MetierSeasonInfoTargetSpeciesTableModel extends AbstractTableModel implements TableCellRenderer, SensitivityTableModel {

    /** Log. */
    private static Log log = LogFactory.getLog(MetierSeasonInfoTargetSpeciesTableModel.class);
    
    /** serialVersionUID. */
    private static final long serialVersionUID = 3169786638868209920L;

    /** Columns names. */
    public final static String[] COLUMN_NAMES = {
            t("isisfish.metierSeasonInfoSpecies.species"),
            t("isisfish.metierSeasonInfoSpecies.targetFactor"),
            t("isisfish.metierSeasonInfoSpecies.mainSpecies") };

    protected List<TargetSpecies> targetSpeciesList;

    /**
     * Empty constructor.
     */
    public MetierSeasonInfoTargetSpeciesTableModel() {
        this(null);
    }

    /**
     * Constructor with data.
     *  
     * @param targetSpeciesList initial target species
     */
    public MetierSeasonInfoTargetSpeciesTableModel(
            List<TargetSpecies> targetSpeciesList) {
        this.targetSpeciesList = targetSpeciesList;
    }

    /**
     * Set target species list.
     * 
     * @param targetSpeciesList the targetSpecies to set
     */
    public void setTargetSpecies(List<TargetSpecies> targetSpeciesList) {
        this.targetSpeciesList = targetSpeciesList;
    }

    @Override
    public int getColumnCount() {
        return COLUMN_NAMES.length;
    }

    @Override
    public int getRowCount() {
        int rows = 0;
        if (targetSpeciesList != null) {
            rows = targetSpeciesList.size();
        }
        return rows;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        Object result;

        TargetSpecies targetSpecies = targetSpeciesList.get(rowIndex);
        switch (columnIndex) {
        case 0:
            result = targetSpecies.getSpecies().getName();
            break;
        case 1:
            result = targetSpecies.getTargetFactorEquation();
            break;
        case 2:
            result = targetSpecies.isPrimaryCatch();
            break;
        default:
            throw new IndexOutOfBoundsException("No such column " + columnIndex);
        }

        return result;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {

        Class<?> result;
        
        switch (columnIndex) {
        case 0:
            result = String.class;
            break;
        case 1:
            result = Equation.class;
            break;
        case 2:
            result = Boolean.class;
            break;
        default:
            throw new IndexOutOfBoundsException("No such column " + columnIndex);
        }
        
        return result;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return COLUMN_NAMES[columnIndex];
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return columnIndex > 0;
    }

    @Override
    public void setValueAt(Object value, int rowIndex, int columnIndex) {
        
        if (log.isDebugEnabled()) {
            log.debug("Cell edition (column " + columnIndex + ") = " + value);
        }
        
        TargetSpecies targetSpecies = targetSpeciesList.get(rowIndex);
        switch (columnIndex) {
        case 1:
            Equation eq = (Equation)value;
            // two events for event to be fired
            targetSpecies.setTargetFactorEquation(null);
            targetSpecies.setTargetFactorEquation(eq);
            break;
        case 2:
            Boolean bValue = (Boolean)value;
            targetSpecies.setPrimaryCatch(bValue);
            break;
        default:
            throw new IndexOutOfBoundsException("Can't edit column " + columnIndex);
        }

    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value,
            boolean isSelected, boolean hasFocus, int row, int column) {

        Component c;
        switch (column) {
        case 0:
            c = new JLabel(value.toString());
            break;
        case 1:
            Equation equation = (Equation)value;
            c = new JButton(equation.getName() + "(" + equation.getCategory() + ")");
            break;
        case 2:
            Boolean bValue = (Boolean)value;
            c = new JCheckBox();
            ((JCheckBox)c).setSelected(bValue);
            break;
        default:
            throw new IndexOutOfBoundsException("No such column " + column);
        }
        return c;
    }

    @Override
    public String getPropertyAtColumn(int column) {
        String property = null;
        if (column == 1) {
            property = "targetFactorEquation";
        }
        return property;
    }

    @Override
    public Object getBeanAtRow(int rowIndex) {
        Object value;
        value = targetSpeciesList.get(rowIndex);
        return value;
    }
}
