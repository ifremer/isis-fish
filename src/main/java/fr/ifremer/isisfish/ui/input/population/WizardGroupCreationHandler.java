/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.population;

import java.awt.CardLayout;

import javax.swing.JFrame;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaContext;

import fr.ifremer.isisfish.IsisFishDAOHelper;
import fr.ifremer.isisfish.entities.Equation;
import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.entities.PopulationGroup;
import fr.ifremer.isisfish.entities.PopulationGroupDAO;

/**
 * wizard group creation handler.
 */
public class WizardGroupCreationHandler {

    /** Class logger. */
    private static final Log log = LogFactory.getLog(WizardGroupCreationHandler.class);

    protected WizardGroupCreationUI wizardUI;

    protected String current = null;
    protected boolean ageType = false;
    protected boolean inputType = false;
    protected boolean sameSizeType = false;
    protected boolean growthCurveType = false;

    protected double first = 0;
    protected double last = 0;

    protected String maxLength = "";
    protected int numberOfGroup = 0;
    protected double groupSize = 0;

    protected double step = 1;
    protected PopulationBasicsUI popBasic;

    public WizardGroupCreationHandler(WizardGroupCreationUI wizardUI) {
        this.wizardUI = wizardUI;
    }

    protected void afterInit() {
        
    }

    public void initParent(PopulationBasicsUI popBasic) {
        this.popBasic = popBasic;
    }

    /**
     * @return Returns the ageType.
     */
    public boolean isAgeType() {
        return this.ageType;
    }

    /**
     * @param ageType The ageType to set.
     */
    public void setAgeType(boolean ageType) {
        this.ageType = ageType;
    }

    /**
     * @return Returns the inputType.
     */
    public boolean isInputType() {
        return this.inputType;
    }

    /**
     * @param inputType The inputType to set.
     */
    public void setInputType(boolean inputType) {
        this.inputType = inputType;
    }

    /**
     * @return Returns the sameSizeType.
     */
    public boolean isSameSizeType() {
        return this.sameSizeType;
    }

    /**
     * @param sameSizeType The sameSizeType to set.
     */
    public void setSameSizeType(boolean sameSizeType) {
        this.sameSizeType = sameSizeType;
    }

    /**
     * @return Returns the growthCurveType.
     */
    public boolean isGrowthCurveType() {
        return this.growthCurveType;
    }

    /**
     * @param growthCurveType The growthCurveType to set.
     */
    public void setGrowthCurveType(boolean growthCurveType) {
        this.growthCurveType = growthCurveType;
    }

    /**
     * @return Returns the first.
     */
    public double getFirst() {
        return this.first;
    }

    /**
     * @param first The first to set.
     */
    public void setFirst(double first) {
        this.first = first;
    }

    /**
     * @return Returns the last.
     */
    public double getLast() {
        return this.last;
    }

    /**
     * @param last The last to set.
     */
    public void setLast(double last) {
        this.last = last;
    }

    /**
     * @return Returns the maxLength.
     */
    public String getMaxLength() {
        return this.maxLength;
    }

    /**
     * @param maxLength The maxLength to set.
     */
    public void setMaxLength(String maxLength) {
        this.maxLength = maxLength;
    }

    /**
     * @return Returns the numberOfGroup.
     */
    public int getNumberOfGroup() {
        return this.numberOfGroup;
    }

    /**
     * @param numberOfGroup The numberOfGroup to set.
     */
    public void setNumberOfGroup(int numberOfGroup) {
        this.numberOfGroup = numberOfGroup;
    }

    /**
     * @return Returns the groupSize.
     */
    public double getGroupSize() {
        return this.groupSize;
    }

    /**
     * @param groupSize The groupSize to set.
     */
    public void setGroupSize(double groupSize) {
        this.groupSize = groupSize;
    }

    /**
     * @return Returns the step.
     */
    public double getStep() {
        return this.step;
    }

    /**
     * @param step The step to set.
     */
    public void setStep(double step) {
        this.step = step;
    }

    public void setCard(String name) {
        current = name;
        ((CardLayout) wizardUI.wizardPanels.getLayout()).show(wizardUI.wizardPanels, name);
    }

    protected void prev() {
        if (isAgeType()) {
            // do nothing only one panel
        } else {
            setCard("beginGroupLength");
        }
        wizardUI.prev.setEnabled(false);
        wizardUI.next.setEnabled(true);
        wizardUI.finish.setEnabled(false);
    }

    protected void next() {
        if (isAgeType()) {
            // do nothing only one panel
        } else if (isInputType()) {
            setCard("endInputGroupLength");
        } else if (isSameSizeType()) {
            setCard("endSameSizeGroupLength");
        } else if (isGrowthCurveType()) {
            setCard("endGrowthCurveGroupLength");
        }
        wizardUI.prev.setEnabled(true);
        wizardUI.next.setEnabled(false);
        wizardUI.finish.setEnabled(true);
    }

    protected void finish() {
        if (log.isDebugEnabled()) {
            log.debug("wizardGroupFinish called");
        }

        try {
            Population pop = popBasic.getBean();
            // remove all old group
            pop.clearPopulationGroup();

            TopiaContext isisContext = pop.getTopiaContext();
            PopulationGroupDAO populationGroupDAO = IsisFishDAOHelper.getPopulationGroupDAO(isisContext);

            if (isAgeType()) {
                double ageFirst = getFirst();
                double ageLast = getLast();
                for (int id = 0; id + ageFirst <= ageLast; id++) {
                    PopulationGroup group = populationGroupDAO.create();
                    group.setId(id);
                    group.setPopulation(pop);
                    group.setAge(ageFirst + id);
                    pop.addPopulationGroup(group);
                    populationGroupDAO.update(group);
                }
            } else if (isInputType()) {
                double minLength = getFirst();
                String[] values = getMaxLength().split(";");
                for (int i = 0; i < values.length; i++) {
                    if (!"".equals(values[i])) {
                        double length = Double.parseDouble(values[i]);
                        PopulationGroup group = populationGroupDAO.create();
                        group.setId(i);
                        group.setPopulation(pop);
                        group.setMinLength(minLength);
                        group.setMaxLength(length);
                        pop.addPopulationGroup(group);
                        populationGroupDAO.update(group);
                        minLength = length;
                    }
                }

            } else if (isSameSizeType()) {
                double minLength = getFirst();
                int numberOfGroup = getNumberOfGroup();
                double step = getGroupSize();
                if (numberOfGroup < 0) {
                    //                        return new OutputView("Error.xml", "error", t("isisfish.error.number.classes.upper.zero"));
                }
                if (step == 0) {
                    //                        return new OutputView("Error.xml", "error", t"isisfish.error.no.null.time.step"));
                }

                double maxLength = minLength;
                for (int i = 0; i < numberOfGroup; i++) {
                    maxLength = minLength + step;
                    PopulationGroup group = populationGroupDAO.create();
                    group.setId(i);
                    group.setPopulation(pop);
                    group.setMinLength(minLength);
                    group.setMaxLength(maxLength);
                    pop.addPopulationGroup(group);
                    populationGroupDAO.update(group);
                    minLength = maxLength;
                }
            } else if (isGrowthCurveType()) {
                double minLength = getFirst();
                int numberOfGroup = getNumberOfGroup();
                int step = (int) getStep();

                Equation equation = pop.getGrowth();
                if (equation == null) {
                    //                        return new OutputView("Error.xml", "error", t("isisfish.error.growth.equation.before.create.group.population"));
                }
                double deltat = -1;
                double Lmin = minLength;
                for (int i = 0; i < numberOfGroup; i++) {
                    // on creer la classe avec une valeur Lmax fausses ...
                    PopulationGroup group = populationGroupDAO.create();
                    group.setId(i);
                    group.setPopulation(pop);
                    group.setMinLength(Lmin);
                    group.setMaxLength(Lmin);
                    pop.addPopulationGroup(group);

                    if (deltat < 0) {
                        // premier passage, recuperation de l'age minimum
                        deltat = pop.getAge(minLength, group);
                    }
                    // incrementation pour calculer la longueur max de la classe
                    deltat += step;

                    // ... pour pouvoir avoir la classe pour l'equation
                    double Lmax = pop.getLength(deltat, group);
                    group.setMaxLength(Lmax);
                    Lmin = Lmax;
                }
            }
            popBasic.getHandler().refresh();
            cancel();
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Can't create PopulationGroup", e);
            }
        }

    }

    protected void cancel() {
        wizardUI.getParentContainer(JFrame.class).dispose();
    }

    protected void refreshChoice() {
        setInputType(wizardUI.beginGroupLengthTypeInput.isSelected());
        setSameSizeType(wizardUI.beginGroupLengthTypeSameSize.isSelected());
        setGrowthCurveType(wizardUI.beginGroupLengthTypeGrowthCurve.isSelected());
    }

    protected void stepChanged() {
        if (!wizardUI.fieldStep.getText().equals("")) {
            setStep(Double.parseDouble(wizardUI.fieldStep.getText()));
        }
    }

    protected void firstAgeChanged() {
        if (!wizardUI.firstAge.getText().equals("")) {
            setFirst(Double.parseDouble(wizardUI.firstAge.getText()));
        }
    }

    protected void lastAgeChanged() {
        if (!wizardUI.lastAge.getText().equals("")) {
            setLast(Double.parseDouble(wizardUI.lastAge.getText()));
        }
    }

    protected void firstInputLengthChanged() {
        if (!wizardUI.firstInputLength.getText().equals("")) {
            setFirst(Double.parseDouble(wizardUI.firstInputLength.getText()));
        }
    }

    protected void maximalGroupsLengthChanged() {
        if (!wizardUI.maximalGroupsLength.getText().equals("")) {
            setMaxLength(wizardUI.maximalGroupsLength.getText());
        }
    }

    protected void firstSizeLengthChanged() {
        if (!wizardUI.firstSizeLength.getText().equals("")) {
            setFirst(Double.parseDouble(wizardUI.firstSizeLength.getText()));
        }
    }

    protected void sameSizeNumberOfGroupChanged() {
        if (!wizardUI.sameSizeNumberOfGroup.getText().equals("")) {
            setNumberOfGroup(Integer.parseInt(wizardUI.sameSizeNumberOfGroup.getText()));
        }
    }

    protected void groupWidthChanged() {
        if (!wizardUI.groupWidth.getText().equals("")) {
            setGroupSize(Double.parseDouble(wizardUI.groupWidth.getText()));
        }
    }

    protected void growthCurveFirstGroupChanged() {
        if (!wizardUI.growthCurveFirstGroup.getText().equals("")) {
            setFirst(Double.parseDouble(wizardUI.growthCurveFirstGroup.getText()));
        }
    }

    protected void fieldNumberOfGroupChanged() {
        if (!wizardUI.fieldNumberOfGroup.getText().equals("")) {
            setNumberOfGroup(Integer.parseInt(wizardUI.fieldNumberOfGroup.getText()));
        }
    }
}
