/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 - 2020 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.population;

import fr.ifremer.isisfish.entities.Zone;
import fr.ifremer.isisfish.ui.input.InputContentHandler;
import fr.ifremer.isisfish.ui.models.common.GenericListModel;
import org.nuiton.math.matrix.gui.MatrixPanelEvent;

import javax.swing.JList;
import java.util.ArrayList;
import java.util.List;

/**
 * Population handler.
 */
public class PopulationZonesHandler extends InputContentHandler<PopulationZonesUI> {

    protected boolean init = false;

    protected PopulationZonesHandler(PopulationZonesUI inputContentUI) {
        super(inputContentUI);
    }

    protected void afterInit() {

        inputContentUI.addPropertyChangeListener(PopulationZonesUI.PROPERTY_BEAN, evt -> {
            if (evt.getNewValue() == null) {
                setPopulationZonesPresenceModel();
                setFieldPopulationZonesReproductionModel(getSelectedValues(inputContentUI.populationZonesPresence));
                setFieldPopulationZonesRecruitmentModel(getSelectedValues(inputContentUI.populationZonesPresence));
                setFieldPopulationMappingZoneReproZoneRecru();
            }
            if (evt.getNewValue() != null) {
                init = true;
                setPopulationZonesPresenceModel();
                setFieldPopulationZonesReproductionModel(getSelectedValues(inputContentUI.populationZonesPresence));
                setFieldPopulationZonesRecruitmentModel(getSelectedValues(inputContentUI.populationZonesPresence));
                setFieldPopulationMappingZoneReproZoneRecru();
                init = false;
            }
        });
    }

    protected void populationMappingZoneReproZoneRecruMatrixChanged(MatrixPanelEvent event) {
        inputContentUI.getBean().setMappingZoneReproZoneRecru(inputContentUI.fieldPopulationMappingZoneReproZoneRecru.getMatrix().clone());
    }

    protected void setFieldPopulationMappingZoneReproZoneRecru() {
        if (inputContentUI.getBean() != null) {
            if (inputContentUI.getBean().getMappingZoneReproZoneRecru() != null) {
                inputContentUI.fieldPopulationMappingZoneReproZoneRecru.setMatrix(inputContentUI.getBean().getMappingZoneReproZoneRecru().copy());
            }
        }
    }
    protected void setPopulationZonesPresenceModel() {
        if (inputContentUI.getBean() != null) {
            List<Zone> zones = inputContentUI.getFisheryRegion().getZone();
            setModel(zones, inputContentUI.getBean().getPopulationZone(), inputContentUI.populationZonesPresence);
        }
    }
    protected void setFieldPopulationZonesReproductionModel(List<Zone> zones) {
        if (inputContentUI.getBean() != null) {
            setModel(zones, inputContentUI.getBean().getReproductionZone(), inputContentUI.fieldPopulationZonesReproduction);
        }
    }
    protected void setFieldPopulationZonesRecruitmentModel(List<Zone> zones) {
        if (inputContentUI.getBean() != null) {
            setModel(zones, inputContentUI.getBean().getRecruitmentZone(), inputContentUI.fieldPopulationZonesRecruitment);
        }
    }

    /**
     * Change model of {@code associatedList} with all available zones, but keep
     * selection with {@code selectedZones}.
     */
    protected void setModel(List<Zone> availableZones, List<Zone> selectedZones, JList<Zone> associatedList) {
        GenericListModel<Zone> zoneModel = new GenericListModel<>(availableZones);
        associatedList.setModel(zoneModel);

        // can be null at population init
        if (selectedZones != null) {
            for (Zone selectedZone : selectedZones) {
                int index = availableZones.indexOf(selectedZone);
                associatedList.addSelectionInterval(index, index);
            }
        }
    }

    protected void presenceChanged() {
        if (!init) {
            inputContentUI.getBean().setPopulationZone(getSelectedValues(inputContentUI.populationZonesPresence));
            setFieldPopulationZonesReproductionModel(getSelectedValues(inputContentUI.populationZonesPresence));
            setFieldPopulationZonesRecruitmentModel(getSelectedValues(inputContentUI.populationZonesPresence));
            setFieldPopulationMappingZoneReproZoneRecru();
        }
    }

    protected void reproductionChanged() {
        if (!init) {
            inputContentUI.getBean().setReproductionZone(getSelectedValues(inputContentUI.fieldPopulationZonesReproduction));
            setFieldPopulationMappingZoneReproZoneRecru();
        }
    }

    protected void recruitementChanged() {
        if (!init) {
            inputContentUI.getBean().setRecruitmentZone(getSelectedValues(inputContentUI.fieldPopulationZonesRecruitment));
            setFieldPopulationMappingZoneReproZoneRecru();
        }
    }

    /**
     * Get selected values for components as list.
     */
    protected List<Zone> getSelectedValues(JList<Zone> component) {
        List<Zone> selectedValues = component.getSelectedValuesList();
        List<Zone> selectedZone = new ArrayList<>(selectedValues);
        return selectedZone;
    }
}
