/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2011 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.check;

import static org.nuiton.i18n.I18n.t;

import javax.swing.JFrame;
import javax.swing.WindowConstants;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 * CheckResultFrame.
 *
 * Created: 10 janv. 2004
 *
 * @author Benjamin Poussin &lt;poussin@codelutin.com&gt;
 * @version $Revision$
 *
 * Mise a jour: $Date$
 * par : $Author$
 */
public class CheckResultFrame extends JFrame { // CheckResultFrame
    
    /** serialVersionUID. */
    private static final long serialVersionUID = -3425876562425941700L;
    
    protected CheckResultTableModel model;

    public CheckResultFrame(CheckResult checkResult) {
        this();
        setCheckResult(checkResult);
    }

    public CheckResultFrame() {
        super(t("isisfish.message.result.verif.region"));
        model = new CheckResultTableModel(new CheckResult());
        JTable table = new JTable(model);
        model.addMouseListenerToHeaderInTable(table);
        table.setDefaultRenderer(Object.class, new CheckResultTableRenderer());
        table.getColumnModel().getColumn(2).setPreferredWidth(30);

        this.setSize(600, 400);
        this.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        this.getContentPane().add(new JScrollPane(table));
    }

    public void setCheckResult(CheckResult checkResult) {
        model.setCheckResult(checkResult);
    }

} // CheckResultFrame
