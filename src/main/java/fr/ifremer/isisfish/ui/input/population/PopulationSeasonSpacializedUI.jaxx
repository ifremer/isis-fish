<!--
  #%L
  IsisFish
  
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2009 - 2020 Ifremer, Code Lutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->
<fr.ifremer.isisfish.ui.input.InputContentUI superGenericType='fr.ifremer.isisfish.entities.Population'>

    <!-- bean property -->
    <fr.ifremer.isisfish.entities.Population id='bean' javaBean='null'/>

    <fr.ifremer.isisfish.entities.PopulationSeasonInfo id='populationSeasonInfo' javaBean='null'/>

    <Boolean id='ageGroupType' javaBean='false'/>

    <PopulationSeasonSpacializedHandler id="handler" constructorParams="this" />

    <script><![CDATA[
    protected void $afterCompleteSetup() {
        handler.afterInit();
    }
    ]]></script>

    <JPanel id='body'>
        <Table>
            <row>
                <cell>
                    <Panel/>
                </cell>
                <cell fill='horizontal' weightx='0.5'>
                    <JRadioButton id="radioPopulationSeasonGroupChangeLengthNoSpacialized"
                        buttonGroup="radioPopulationSeasonGroupChangeLengthSpacializedGroup"
                        selected='{getPopulationSeasonInfo().isSimpleLengthChangeMatrix()}'
                        enabled='{getPopulationSeasonInfo() != null}'
                        text="isisfish.populationSeasons.noSpacialized" onActionPerformed='handler.spacializedActionPerformed()'
                        visible='{isAgeGroupType()}' decorator='boxed' />
                </cell>
                <cell fill='horizontal' weightx='0.5'>
                    <JRadioButton id="radioPopulationSeasonGroupChangeLengthSpacialized"
                        buttonGroup="radioPopulationSeasonGroupChangeLengthSpacializedGroup"
                        selected='{!getPopulationSeasonInfo().isSimpleLengthChangeMatrix()}'
                        enabled='{getPopulationSeasonInfo() != null}'
                        text="isisfish.populationSeasons.spacialized" onActionPerformed='handler.spacializedActionPerformed()'
                        visible='{isAgeGroupType()}' decorator='boxed' />
                </cell>
            </row>
            <row>
                <cell>
                    <JLabel text="isisfish.populationSeasons.changeGroup" visible='{isAgeGroupType()}'
                        enabled='{getPopulationSeasonInfo() != null}'/>
                </cell>
                <cell fill='horizontal' weightx='0.5'>
                    <JButton text="isisfish.populationSeasons.computeCoefficient" decorator='boxed' visible='{isAgeGroupType()}'
                        enabled='{getPopulationSeasonInfo() != null}' onActionPerformed='handler.computeMatrixChangeOfGroup()'/>
                </cell>
                <cell fill='horizontal' weightx='0.5'>
                    <JButton id="buttonPopulationSeasonGroupChangeLengthButtonShow" text="isisfish.populationSeasons.showSpacialized"
                        enabled='{radioPopulationSeasonGroupChangeLengthNoSpacialized.isSelected() &amp;&amp; getPopulationSeasonInfo() != null}'
                        onActionPerformed='handler.showSpacializedMatrixChangeOfGroup()'
                        visible='{isAgeGroupType()}' decorator='boxed'/>
                </cell>
            </row>
            <row>
                <cell>
                    <Panel/>
                </cell>
                <cell columns='2' fill='both' weightx='1.0' weighty='1.0'>
                    <org.nuiton.math.matrix.gui.MatrixPanelEditor id='matrixPanelPopulationSeasonLengthChange'
                        enabled='{getPopulationSeasonInfo() != null}'
                        _sensitivityBean='{PopulationSeasonInfo.class}' _sensitivityMethod='"LengthChangeMatrix"'
                        visible='{isAgeGroupType()}' decorator='boxed'
                        matrix='{getPopulationSeasonInfo() == null ? null : getPopulationSeasonInfo().getLengthChangeMatrix().copy()}'
                        onMatrixChanged="handler.populationSeasonLengthMatrixChanged(event)" />
                </cell>
            </row>
        </Table>
    </JPanel>
</fr.ifremer.isisfish.ui.input.InputContentUI>
