/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2020 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input;

import static org.nuiton.i18n.I18n.t;

import java.awt.Component;
import java.awt.Dimension;
import java.beans.PropertyChangeListener;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import fr.ifremer.isisfish.types.hibernate.MatrixDatabaseHelper;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.lang3.reflect.MethodUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.persistence.TopiaDAO;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityContextable;

import fr.ifremer.isisfish.IsisFishDAOHelper;
import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.ui.SaveVerifier;
import jaxx.runtime.JAXXUtil;

/**
 * InputSaveVerifier.
 *
 * @author letellier
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class InputSaveVerifier implements SaveVerifier {

    /** Class logger. */
    private static Log log = LogFactory.getLog(InputSaveVerifier.class);

    /** Current entity modification state (modifier by PCL). */
    protected boolean changed = false;

    /** L'ui en cours d'edition (au plus bas niveau des tabPane). */
    protected InputContentUI<?> inputContentUI;

    /** Map entity key to {@link TopiaEntity}. */
    protected HashMap<String, TopiaEntityContextable> currentEntities = new HashMap<>();

    /** Intance unique de PCL pour pouvoir être ajouter et supprimé au bon moment. */
    protected PropertyChangeListener entityPCL = evt -> {
        if (log.isDebugEnabled()) {
            log.debug("PropertyChanged : "
                    + evt.getPropertyName()
                    + " New Value : " + evt.getNewValue());
        }
        topiaChanged();
    };

    public InputContentUI<?> getInputContentUI() {
        return inputContentUI;
    }

    public void setInputContentUI(InputContentUI<?> inputContentUI) {
        this.inputContentUI = inputContentUI;
    }

    /**
     * Tell verifier to check for unsaved modification.
     * 
     * Then return user choice ({@link javax.swing.JOptionPane} type).
     * 
     * @return user JOptionPane response
     * @see javax.swing.JOptionPane#YES_OPTION
     * @see javax.swing.JOptionPane#NO_OPTION
     * @see javax.swing.JOptionPane#CANCEL_OPTION
     */
    @Override
    public int checkEdit() {
        int response = JOptionPane.NO_OPTION;
        if (changed) {
            // ask user to close edition
            // still in edit mode, must warn user
            response = JOptionPane.showConfirmDialog(inputContentUI, t("isisfish.message.page.modified"),
                    t("isisfish.input.menu.commit"), JOptionPane.YES_NO_CANCEL_OPTION,
                    JOptionPane.WARNING_MESSAGE);
            if (response == JOptionPane.NO_OPTION) {
                cancel();
            } else if (response == JOptionPane.YES_OPTION) {
                save();
            }
        }
        return response;
    }

    /* Called by entity property change listener on data change.*/
    protected void topiaChanged() {
        changed = true;
    }

    /**
     * Called by this class when entity modification state has been reset.
     * (cancel, save...)
     */
    protected void noModif() {
        changed = false;
    }

    /**
     * Delete one entity and commit the change, try to selected intelligently
     * other node in tree.
     * <p>
     * Refresh all ui component where name match "input&lt;entity type without
     * package &gt;.*"
     */
    public void delete() {
        try {
            boolean doDelete;
            TopiaEntityContextable topiaEntity = inputContentUI.getBean();

            // check in matrix
            List<TopiaEntity> referencedEntitites = MatrixDatabaseHelper.checkUsedObjects(topiaEntity.getTopiaContext(), topiaEntity);
            if (referencedEntitites.isEmpty()) {

                // check composite
                List<TopiaEntity> allWillBeRemoved = topiaEntity.getComposite();
                if (!allWillBeRemoved.isEmpty()) {
                    String label = t("isisfish.message.delete.object", toString(topiaEntity));
                    String text = allWillBeRemoved.stream() //
                            .map(e -> ClassUtils.getShortClassName(e.getClass()) + " - " + toString(e)) //
                            .collect(Collectors.joining("\n"));
                    int resp = showTextAreaConfirmationMessage(null, label, text,
                            t("isisfish.message.delete.entities"),
                            JOptionPane.YES_NO_OPTION);
                    doDelete = resp == JOptionPane.YES_OPTION;
                } else {
                    String text = t("isisfish.message.confirm.delete.object", toString(topiaEntity));
                    int resp = JOptionPane.showConfirmDialog(null, text,
                            t("isisfish.message.delete.entity"),
                            JOptionPane.YES_NO_OPTION);
                    doDelete = resp == JOptionPane.YES_OPTION;
                }

                if (doDelete) {
                    topiaEntity.delete();
                    topiaEntity.getTopiaContext().commitTransaction();

                    noModif();
                    InputUI inputUI = inputContentUI.getParentContainer(InputUI.class);
                    inputUI.getHandler().deleteTreeNode(topiaEntity.getTopiaId());
                }
            } else {
                String label = t("isisfish.message.delete.referenced", toString(topiaEntity));
                String text = referencedEntitites.stream() //
                        .map(e -> ClassUtils.getShortClassName(e.getClass()) + " - " + toString(e)) //
                        .collect(Collectors.joining("\n"));
                showTextAreaConfirmationMessage(null, label, text,
                        t("isisfish.message.delete.entities"),
                        JOptionPane.OK_OPTION);
            }

        } catch (TopiaException eee) {
            throw new IsisFishRuntimeException("Can't remove entity: " + currentEntities.get(0), eee);
        }
    }

    /**
     * Try to get toString value from entity using entity name (or toString if name is not defined).
     * 
     * @param e entity
     * @return
     */
    protected String toString(TopiaEntity e) {
        String result;

        try {
            result = BeanUtils.getProperty(e, "name");
        } catch (Exception ex) {
            result = e.toString();
        }

        return result;
    }

    /**
     * Create new {@code type} entity.
     * 
     * @param type
     */
    public void create(Class<? extends TopiaEntityContextable> type) {

        if (log.isDebugEnabled()) {
            log.debug("Create called for " + type);
        }

        try {
            String typeSimpleName = type.getSimpleName();
            String name = typeSimpleName + "_new";

            // FIXME 20161110 poussin don't use introspection but
            // IsisFishDAOHelper.getDAO(context, type);

            // use introspection to call IsisFishDAOHelper.getXXXDAO()
            TopiaContext topiaContext = inputContentUI.getTopiaContext();
            Method method = MethodUtils.getAccessibleMethod(IsisFishDAOHelper.class,
                    "get" + typeSimpleName + "DAO", TopiaContext.class);
            TopiaDAO<TopiaEntity> dao = (TopiaDAO<TopiaEntity>) method.invoke(null, topiaContext);

            // create new entity
            TopiaEntityContextable topiaEntity = (TopiaEntityContextable)dao.create("name", name);
            // entity.update(); FIXME mandatory ???
            topiaContext.commitTransaction();

            InputUI inputUI = inputContentUI.getContextValue(InputUI.class, JAXXUtil.PARENT);
            inputUI.getHandler().insertTreeNode(type, topiaEntity);

            //rootUI.setStatusMessage(t("isisfish.message.creation.finished"));
        } catch (Exception eee) {
            throw new IsisFishRuntimeException(t("isisfish.error.input.createentity"), eee);
        }
    }

    /**
     * Save all non saved entities.
     * 
     * Change registered button states.
     * Commit opened topia context.
     */
    public void save() {
        TopiaEntityContextable topiaEntity = inputContentUI.getBean();
        TopiaContext topiaContext = inputContentUI.getTopiaContext();

        try {
            for (TopiaEntityContextable entity : currentEntities.values()) {
                entity.update();
                if (log.isDebugEnabled()) {
                    log.debug("Updating verifier entity : " + entity);
                }
            }
            topiaContext.commitTransaction();

            InputUI inputUI = inputContentUI.getParentContainer(InputUI.class);
            inputUI.getHandler().updateTreeNode(topiaEntity.getTopiaId());

            // FIXME echatellier 20111201 small fix because save
            // button has to reset internal ui change model state
            inputContentUI.resetChangeModel();
            
            noModif();
            
            //rootUI.setStatusMessage(t("isisfish.message.save.finished"));
        } catch (TopiaException eee) {
            // EC-20100401 : ajouté pour avoir un context
            // correct apres un erreur, sinon, on ne peut plus rien faire
            // rien sauver d'autre...
            try {
                topiaContext.rollbackTransaction();
                throw new IsisFishRuntimeException(t("isisfish.error.input.saveentity"), eee);
            } catch (TopiaException eee2) {
                throw new IsisFishRuntimeException(t("isisfish.error.input.saveentity"), eee2);
            }
        }
    }

    /**
     * Cancel all modification on entity (rollback), and force reload it.
     */
    public void cancel() {
        TopiaContext topiaContext = inputContentUI.getTopiaContext();
        TopiaEntityContextable topiaEntity = inputContentUI.getBean();

        // cas possible si pas appelé par les boutons d'action, mais par les imports par exemple
        if (topiaEntity == null) {
            return;
        }

        try {
            topiaContext.rollbackTransaction();
            noModif();
            
            // sans cette boucle
            // une annulation/sauvegarde provoque l'erreur
            // org.hibernate.NonUniqueObjectException: a different object with
            //  the same identifier value was already associated with the session
            Map<String, TopiaEntityContextable> canceledEntity = (Map<String, TopiaEntityContextable>)currentEntities.clone();
            currentEntities.clear();
            for (Entry<String, TopiaEntityContextable> currentEntity : canceledEntity.entrySet()) {
                TopiaEntityContextable t = (TopiaEntityContextable)topiaContext.findByTopiaId(currentEntity.getValue().getTopiaId());
                String key = currentEntity.getKey();
                addCurrentEntity(t, key);
            }

            InputUI inputUI = inputContentUI.getParentContainer(InputUI.class);
            inputUI.getHandler().updateTreeNode(topiaEntity.getTopiaId());

            // FIXME echatellier 20111201 small fix because save
            // button has to reset internal ui change model state
            inputContentUI.resetChangeModel();

            // refresh all registered panel
            // to discard modification in UI
            //refreshAll();
            topiaEntity = (TopiaEntityContextable)topiaContext.findByTopiaId(topiaEntity.getTopiaId());
            InputContentUI inputContentUI2 = inputContentUI;
            inputContentUI2.setBean(null);
            inputContentUI2.setBean(topiaEntity);
            //rootUI.setStatusMessage(t("isisfish.message.cancel.finished"));
        } catch (TopiaException eee) {
            throw new IsisFishRuntimeException(t("isisfish.error.input.cancelentity"), eee);
        }
    }

    /**
     * Display a JOptionPane with a JTextArea as main component.
     * 
     * @param parent parent
     * @param labelMessage label message
     * @param textMessage text message into area
     * @param title
     * @param option
     * @return user response
     */
    protected int showTextAreaConfirmationMessage(Component parent,
            String labelMessage, String textMessage, String title, int option) {
        JLabel labelForMessage = new JLabel(labelMessage);
        JTextArea areaForMessage = new JTextArea(textMessage);
        areaForMessage.setEditable(false);
        areaForMessage.setAutoscrolls(true);
        JScrollPane spMessage = new JScrollPane(areaForMessage);
        spMessage.setPreferredSize(new Dimension(500, 100)); // don't remove popup is huge

        int response = JOptionPane.showConfirmDialog(parent, new Object[] {
                labelForMessage, spMessage }, title, option);
        return response;
    }

    

    /**
     * Add entity to check for modification.
     * 
     * The verifier register to entity using {@link TopiaEntity#addPropertyChangeListener(PropertyChangeListener)}.
     * So modification have to be done on current entity.
     * 
     * To check for embedded entity, add it too.
     * 
     * Remove all entity with key 
     * 
     * @param currentEntity entity to check
     * @param key specific key (default to topiaId)
     */
    public void addCurrentEntity(TopiaEntityContextable currentEntity, String key) {
        if (currentEntity != null) {
            currentEntity.addPropertyChangeListener(entityPCL);
            this.currentEntities.put(key, currentEntity);
        }
    }

    /**
     * Add entity to check for modification.
     * 
     * The verifier register to entity using {@link TopiaEntity#addPropertyChangeListener(PropertyChangeListener)}.
     * So modification have to be done on current entity.
     * 
     * To check for embedded entity, add it too.
     * 
     * Remove all entity with key 
     * 
     * @param currentEntity entity to check
     */
    public void addCurrentEntity(TopiaEntityContextable currentEntity) {
        if (currentEntity != null) {
            addCurrentEntity(currentEntity, currentEntity.getTopiaId());
        }
    }

    /**
     * Remove monitored entities.
     * 
     * @param key entity key to remove
     */
    public void removeCurrentEntity(String key) {
        currentEntities.remove(key);
    }

    /**
     * Reset state.
     */
    public void reset() {
        // il faut absolument supprimer les property change listener
        // sinon, lors de la selection d'une espece, à la creation
        // d'une population, le PCL est declanché, et l'abre
        // croit qu'il y a eu une modification et il boucle
        for (TopiaEntity entity : currentEntities.values()) {
            entity.removePropertyChangeListener(entityPCL);
        }
        currentEntities.clear();
        noModif();
    }

    public <E extends TopiaEntityContextable> E getEntity(Class<E> clazz, String key) {
        return (E) currentEntities.get(key);
    }

    public <E extends TopiaEntityContextable> E getEntity(Class<E> clazz) {
        for (TopiaEntity te : currentEntities.values()) {
            if (clazz.isInstance(te)) {
                return (E) te;
            }
        }
        return null;
    }
}
