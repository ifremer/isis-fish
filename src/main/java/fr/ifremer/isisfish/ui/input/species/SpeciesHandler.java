/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.species;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

import fr.ifremer.isisfish.ui.input.InputContentHandler;
import fr.ifremer.isisfish.ui.input.InputUI;

/**
 * Species handler.
 */
public class SpeciesHandler extends InputContentHandler<SpeciesUI> {

    protected SpeciesHandler(SpeciesUI inputContentUI) {
        super(inputContentUI);
    }

    protected void afterInit() {
        setupGotoNextPath(t("isisfish.input.continuePopulations"), null);
        
        inputContentUI.addPropertyChangeListener(SpeciesUI.PROPERTY_BEAN, evt -> {
            if (evt.getNewValue() == null) {
                inputContentUI.fieldSpeciesName.setText("");
                inputContentUI.fieldSpeciesScientificName.setText("");
                inputContentUI.fieldSpeciesCodeRubbin.setText("");
                inputContentUI.fieldSpeciesCEE.setText("");
                inputContentUI.fieldSpeciesComment.setText("");
            }
            if (evt.getNewValue() != null) {

            }
        });
    }

    @Override
    protected void goTo(String nextPath) {
        // FIXME il ne faut pas appeler le parent
        // on ne sais jamais de quel type est le parent
        InputUI inputUI = inputContentUI.getParentContainer(InputUI.class);
        if (inputUI != null) {
            if (inputContentUI.getBean() == null) {
                inputUI.getHandler().setTreeSelection(inputContentUI, n("isisfish.input.tree.species"), n("isisfish.input.tree.populations"));
            }
            else {
                inputUI.getHandler().setTreeSelection(inputContentUI, inputContentUI.getBean().getTopiaId(), n("isisfish.input.tree.populations"));
            }
        }
    }
}
