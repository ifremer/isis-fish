/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.ui.input.tree;

import static org.nuiton.i18n.I18n.n;

import java.util.HashMap;
import java.util.Map;

import javax.swing.tree.TreeModel;

import jaxx.runtime.swing.nav.tree.NavTreeHelper;

import org.nuiton.topia.persistence.TopiaEntity;

import fr.ifremer.isisfish.entities.Cell;
import fr.ifremer.isisfish.entities.FisheryRegion;
import fr.ifremer.isisfish.entities.Gear;
import fr.ifremer.isisfish.entities.Metier;
import fr.ifremer.isisfish.entities.Observation;
import fr.ifremer.isisfish.entities.Port;
import fr.ifremer.isisfish.entities.SetOfVessels;
import fr.ifremer.isisfish.entities.Species;
import fr.ifremer.isisfish.entities.Strategy;
import fr.ifremer.isisfish.entities.TripType;
import fr.ifremer.isisfish.entities.VesselType;
import fr.ifremer.isisfish.entities.Zone;
import fr.ifremer.isisfish.ui.input.tree.loadors.CellsNodeLoador;
import fr.ifremer.isisfish.ui.input.tree.loadors.FisheryTreeNodeLoador;
import fr.ifremer.isisfish.ui.input.tree.loadors.GearsNodeLoador;
import fr.ifremer.isisfish.ui.input.tree.loadors.MetiersNodeLoador;
import fr.ifremer.isisfish.ui.input.tree.loadors.ObservationsNodeLoador;
import fr.ifremer.isisfish.ui.input.tree.loadors.PortsNodeLoador;
import fr.ifremer.isisfish.ui.input.tree.loadors.SetOfVesselsNodeLoador;
import fr.ifremer.isisfish.ui.input.tree.loadors.SpeciesNodeLoador;
import fr.ifremer.isisfish.ui.input.tree.loadors.StrategiesNodeLoador;
import fr.ifremer.isisfish.ui.input.tree.loadors.TripTypesNodeLoador;
import fr.ifremer.isisfish.ui.input.tree.loadors.VesselTypesNodeLoador;
import fr.ifremer.isisfish.ui.input.tree.loadors.ZonesNodeLoador;

/**
 * Fishery region tree helpers.
 *
 * @author chatellier
 * @since 3.4.0.0
 */
public class FisheryTreeHelper extends NavTreeHelper<FisheryTreeNode> {

    public Map<Class, FisheryTreeNodeLoador<? extends TopiaEntity>> loadorCache = new HashMap<>();

    public <T extends TopiaEntity> FisheryTreeNodeLoador<? extends TopiaEntity> getLoadorFor(Class<T> type) {
        return loadorCache.get(type);
    }

    public TreeModel createTreeModel(FisheryRegion fisheryRegion) {

        FisheryDataProvider provider = new FisheryDataProvider(fisheryRegion);
        setDataProvider(provider);

        // Create root static node
        FisheryTreeNode root = new FisheryTreeNode(FisheryRegion.class,
                fisheryRegion.getTopiaId(), null, null);

        // declare loador
        loadorCache.put(Cell.class, new CellsNodeLoador());
        loadorCache.put(Zone.class, new ZonesNodeLoador());
        loadorCache.put(Port.class, new PortsNodeLoador());
        loadorCache.put(Species.class, new SpeciesNodeLoador());
        loadorCache.put(Gear.class, new GearsNodeLoador());
        loadorCache.put(Metier.class, new MetiersNodeLoador());
        loadorCache.put(TripType.class, new TripTypesNodeLoador());
        loadorCache.put(VesselType.class, new VesselTypesNodeLoador());
        loadorCache.put(SetOfVessels.class, new SetOfVesselsNodeLoador());
        loadorCache.put(Strategy.class, new StrategiesNodeLoador());
        loadorCache.put(Observation.class, new ObservationsNodeLoador());

        // first level nodes
        CellsNodeLoador cellsNodeLoador = new CellsNodeLoador();
        FisheryTreeNode cellsChild = new FisheryTreeNode(Cell.class,
                n("isisfish.input.tree.cells"), null, cellsNodeLoador);
        loadorCache.put(Cell.class, cellsNodeLoador);

        ZonesNodeLoador zonesNodeLoador = new ZonesNodeLoador();
        FisheryTreeNode zonesChild = new FisheryTreeNode(Zone.class,
                n("isisfish.input.tree.zones"), null, zonesNodeLoador);
        loadorCache.put(Zone.class, zonesNodeLoador);
        
        PortsNodeLoador portsNodeLoador = new PortsNodeLoador();
        FisheryTreeNode portsChild = new FisheryTreeNode(Port.class,
                n("isisfish.input.tree.ports"), null, portsNodeLoador);
        loadorCache.put(Port.class, portsNodeLoador);
        
        SpeciesNodeLoador speciesNodeLoador = new SpeciesNodeLoador();
        FisheryTreeNode speciesChild = new FisheryTreeNode(Species.class,
                n("isisfish.input.tree.species"), null, speciesNodeLoador);
        loadorCache.put(Species.class, speciesNodeLoador);
        
        GearsNodeLoador gearsNodeLoador = new GearsNodeLoador();
        FisheryTreeNode gearsChild = new FisheryTreeNode(Gear.class,
                n("isisfish.input.tree.gears"), null, gearsNodeLoador);
        loadorCache.put(Gear.class, gearsNodeLoador);
        
        MetiersNodeLoador metiersNodeLoador = new MetiersNodeLoador();
        FisheryTreeNode metiersChild = new FisheryTreeNode(Metier.class,
                n("isisfish.input.tree.metiers"), null, metiersNodeLoador);
        loadorCache.put(Metier.class, metiersNodeLoador);
        
        TripTypesNodeLoador tripTypesNodeLoador = new TripTypesNodeLoador();
        FisheryTreeNode tripTypesChild = new FisheryTreeNode(TripType.class,
                n("isisfish.input.tree.triptypes"), null, tripTypesNodeLoador);
        loadorCache.put(TripType.class, tripTypesNodeLoador);
        
        VesselTypesNodeLoador vesselTypesNodeLoador = new VesselTypesNodeLoador();
        FisheryTreeNode vesselTypesChild = new FisheryTreeNode(VesselType.class,
                n("isisfish.input.tree.vesseltypes"), null, vesselTypesNodeLoador);
        loadorCache.put(VesselType.class, vesselTypesNodeLoador);
        
        SetOfVesselsNodeLoador setOfVesselsNodeLoador = new SetOfVesselsNodeLoador();
        FisheryTreeNode setOfVesselsChild = new FisheryTreeNode(SetOfVessels.class,
                n("isisfish.input.tree.setofvessels"), null, setOfVesselsNodeLoador);
        loadorCache.put(SetOfVessels.class, setOfVesselsNodeLoador);
        
        StrategiesNodeLoador strategiesNodeLoador = new StrategiesNodeLoador();
        FisheryTreeNode strategiesChild = new FisheryTreeNode(Strategy.class,
                n("isisfish.input.tree.strategies"), null, strategiesNodeLoador);
        loadorCache.put(Strategy.class, strategiesNodeLoador);

        ObservationsNodeLoador observationsNodeLoador = new ObservationsNodeLoador();
        FisheryTreeNode observationsChild = new FisheryTreeNode(Observation.class,
                n("isisfish.input.tree.observations"), null, observationsNodeLoador);
        loadorCache.put(Observation.class, observationsNodeLoador);

        root.add(cellsChild);
        root.add(zonesChild);
        root.add(portsChild);
        root.add(speciesChild);
        root.add(gearsChild);
        root.add(metiersChild);
        root.add(tripTypesChild);
        root.add(vesselTypesChild);
        root.add(setOfVesselsChild);
        root.add(strategiesChild);
        root.add(observationsChild);

        // Create model
        TreeModel model = createModel(root);

        return model;
    }
}
