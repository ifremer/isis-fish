/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2011 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.check;

import static org.nuiton.i18n.I18n.t;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.math.matrix.MatrixIterator;
import org.nuiton.math.matrix.MatrixND;

import fr.ifremer.isisfish.entities.Cell;
import fr.ifremer.isisfish.entities.EffortDescription;
import fr.ifremer.isisfish.entities.Equation;
import fr.ifremer.isisfish.entities.FisheryRegion;
import fr.ifremer.isisfish.entities.Gear;
import fr.ifremer.isisfish.entities.Metier;
import fr.ifremer.isisfish.entities.MetierSeasonInfo;
import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.entities.PopulationGroup;
import fr.ifremer.isisfish.entities.PopulationSeasonInfo;
import fr.ifremer.isisfish.entities.Port;
import fr.ifremer.isisfish.entities.Selectivity;
import fr.ifremer.isisfish.entities.SetOfVessels;
import fr.ifremer.isisfish.entities.Species;
import fr.ifremer.isisfish.entities.Strategy;
import fr.ifremer.isisfish.entities.StrategyMonthInfo;
import fr.ifremer.isisfish.entities.TargetSpecies;
import fr.ifremer.isisfish.entities.TripType;
import fr.ifremer.isisfish.entities.VesselType;
import fr.ifremer.isisfish.entities.Zone;
import fr.ifremer.isisfish.types.Month;
import fr.ifremer.isisfish.util.EvaluatorHelper;

/**
 * CheckRegion.java
 *
 * Created: 9 janv. 2004
 *
 * @author Benjamin Poussin &lt;poussin@codelutin.com&gt;
 * @version $Revision$
 *
 * Mise a jour: $Date$
 * par : $Author$
 */
public class CheckRegion { // CheckSaisie
/*
        // check equation compilation
        for (Species species : fisheryRegion.getSpecies()) {
            for (Population pop : species.getPopulation()) {
                pop.getGrowth();
                pop.getGrowthReverse();
                pop.getMeanWeight();
                pop.getNaturalDeathRate();
                pop.getPrice();
                pop.getReproductionEquation();
                for (PopulationSeasonInfo info : pop.getPopulationSeasonInfo()) {
                    info.getMigrationEquation();
                    info.getEmigrationEquation();
                    info.getImmigrationEquation();
                }
            }
        }

 */
    
    static public void check(Object parent, String name, Equation eq, CheckResult result) {
        if (eq == null) {
            result.addError(parent, t("isisfish.error.invalid.nullequation", name));
        }
        else if (0 != EvaluatorHelper.check(eq.getJavaInterface(), eq.getContent(), null)) {
            result.addError(eq, t("isisfish.error.invalid.equation"));
        }
    }
    
    static public void check(TargetSpecies capturabilite, CheckResult result){
        check(capturabilite, "TargetFactorEquation", capturabilite.getTargetFactorEquation(), result);
    }

    static public void check(PopulationGroup classPop, CheckResult result){
        // error
        if (classPop.getMeanWeight() < 0) {
            result.addError(classPop, t("isisfish.error.invalid.mean.weight"));
        }

        // warning
        if (classPop.getMeanWeight() == 0) {
            result.addWarning(classPop, t("isisfish.error.not.null.mean.weight"));
        }
    }

    static public void check(Gear engin, CheckResult result) {
        if (StringUtils.isBlank(engin.getName())) {
            result.addWarning(engin, t("isisfish.error.empty.name"));
        }
        if (StringUtils.isBlank(engin.getEffortUnit())) {
            result.addWarning(engin, t("isisfish.error.empty.unit"));
        }
        
        Collection<Selectivity> selectivites = engin.getPopulationSelectivity();
        if (CollectionUtils.isEmpty(selectivites)) {
            result.addWarning(engin, t("isisfish.error.empty.selectivity"));
        } else {
            for (Selectivity selectivity : selectivites) {
                check(selectivity, result);
            }
        }
        if (engin.getPossibleValue() == null) {
            result.addWarning(engin, t("isisfish.error.invalid.range"));
        }

        if (StringUtils.isBlank(engin.getParameterName())) {
            result.addInfo(engin, t("isisfish.error.empty.params.name"));
        }
    }

    static public void check(MetierSeasonInfo infoSaisonMetier, CheckResult result){
        if (CollectionUtils.isEmpty(infoSaisonMetier.getZone())) {
            result.addWarning(infoSaisonMetier, t("isisfish.error.no.sector"));
        }
        Collection<TargetSpecies> especeCaptus = infoSaisonMetier.getSpeciesTargetSpecies();
        if (CollectionUtils.isEmpty(especeCaptus)) {
            result.addInfo(infoSaisonMetier, t("isisfish.error.empty.species"));
        } else {
            for (TargetSpecies i : especeCaptus) {
                check(i, result);
            }
        }
    }

    static public boolean isEmptyMatrix(Object entity, MatrixND mat) {
        boolean result = true;
        for (MatrixIterator i = mat.iterator(); result && i.next();) {
            result = false;
        }
        return result;
    }

    static public boolean isNullMatrix(MatrixND mat) {
        boolean result = true;
        if (mat != null) {
            for (MatrixIterator i = mat.iteratorNotZero(); result && i.next();) {
                result = false;
            }
        }
        return result;
    }
    
    static public boolean isNegativeMatrix(MatrixND mat) {
        boolean result = true;
        for (MatrixIterator i = mat.iterator(); result && i.next();) {
            result = i.getValue() < 0;
        }
        return result;
    }

    static public double sumMatrix(MatrixND mat) {
        double result = mat.sumAll();
        return result;
    }
    
    static public void check(PopulationSeasonInfo infoSaisonPop, CheckResult result){
        if (infoSaisonPop.isUseEquationMigration()) {
            check(infoSaisonPop, "MigrationEquation", infoSaisonPop.getMigrationEquation(), result);
            check(infoSaisonPop, "EmigrationEquation", infoSaisonPop.getEmigrationEquation(), result);
            check(infoSaisonPop, "ImmigrationEquation", infoSaisonPop.getImmigrationEquation(), result);
        } else {

            try {
                if (isEmptyMatrix(infoSaisonPop, infoSaisonPop.getMigrationMatrix())) {
                    result.addInfo(infoSaisonPop, t("isisfish.error.empty.migration"));
                } else if(isNullMatrix(infoSaisonPop.getMigrationMatrix())) {
                    result.addInfo(infoSaisonPop, t("isisfish.error.empty.migration"));
                } else if(isNegativeMatrix(infoSaisonPop.getMigrationMatrix())) {
                    result.addError(infoSaisonPop, t("isisfish.error.migration.negative"));
                }
            }
            catch (NullPointerException e) {
                result.addError(infoSaisonPop, t("isisfish.error.null.semantics", "MigrationEquation"));
            }
            
            try {
                if (isEmptyMatrix(infoSaisonPop, infoSaisonPop.getEmigrationMatrix())) {
                    result.addInfo(infoSaisonPop, t("isisfish.error.empty.emigration"));
                } else if(isNullMatrix(infoSaisonPop.getEmigrationMatrix())) {
                    result.addInfo(infoSaisonPop, t("isisfish.error.empty.emigration"));
                } else if(isNegativeMatrix(infoSaisonPop.getEmigrationMatrix())) {
                    result.addError(infoSaisonPop, t("isisfish.error.emigration.negative"));
                }
            }
            catch (NullPointerException e) {
                result.addError(infoSaisonPop, t("isisfish.error.null.semantics", "EmigrationEquation"));
            }
            
            try {
                if (isEmptyMatrix(infoSaisonPop, infoSaisonPop.getImmigrationMatrix())) {
                    result.addInfo(infoSaisonPop, t("isisfish.error.empty.immigration"));
                } else if(isNullMatrix(infoSaisonPop.getImmigrationMatrix())) {
                    result.addInfo(infoSaisonPop, t("isisfish.error.empty.immigration"));
                } else if(isNegativeMatrix(infoSaisonPop.getImmigrationMatrix())) {
                    result.addError(infoSaisonPop, t("isisfish.error.immigration.negative"));
                }
            }
            catch (NullPointerException e) {
                result.addError(infoSaisonPop, t("isisfish.error.null.semantics", "ImmigrationEquation"));
            }
        }
    }

    static public void check(Cell maille, CheckResult result) {
        if (StringUtils.isBlank(maille.getName())) {
            result.addWarning(maille, t("isisfish.error.empty.name"));
        }
    }

    static public void check(Species metaPop, CheckResult result){
        Collection<Population> pops = metaPop.getPopulation();
        if (CollectionUtils.isEmpty(pops)) {
            result.addWarning(metaPop, t("isisfish.error.empty.population"));
        } else {
            for (Population pop : pops) {
                check(pop, result);
            }
        }
        if (StringUtils.isBlank(metaPop.getName())) {
            result.addInfo(metaPop, t("isisfish.error.empty.species.name"));
        }
        if (StringUtils.isBlank(metaPop.getScientificName())) {
            result.addInfo(metaPop, t("isisfish.error.empty.scientific.name"));
        }
        if (StringUtils.isBlank(metaPop.getCodeRubbin())) {
            result.addInfo(metaPop, t("isisfish.error.empty.code.rubbin"));
        }
    }

    static public void check(Metier metier, CheckResult result){
        if (StringUtils.isBlank(metier.getName())) {
            result.addWarning(metier, t("isisfish.error.empty.name"));
        }
        List<MetierSeasonInfo> infoSaisons = metier.getMetierSeasonInfo();
        if (CollectionUtils.isEmpty(infoSaisons)) {
            result.addWarning(metier, t("isisfish.error.empty.season"));
        } else {
            Set<Month> months = new HashSet<>();
            for (MetierSeasonInfo info : infoSaisons) {
                if (!Collections.disjoint(months, info.getMonths())) {
                    result.addError(info, t("isisfish.error.overlap.season"));
                }
                months.addAll(info.getMonths());
                check(info, result);
            }
            if (months.size() != Month.NUMBER_OF_MONTH) {
                result.addError(metier, t("isisfish.error.notallyear.season"));
            }
        }
        if (metier.getGear() == null) {
            result.addError(metier, t("isisfish.error.undefined.gear"));
        } else {
            check(metier.getGear(), result);
            
            try {
                if (!metier.getGear().getPossibleValue().contains(metier.getGearParameterValue())) {
                    result.addWarning(metier, t("isisfish.error.invalid.values.params"));
                }
            } catch(Exception e) {
                // can't happen StringIndexOutOfBoundsException
                // if gear.PossibleValue if not valid
                result.addWarning(metier, t("isisfish.error.invalid.values.params"));
            }
        }
    }

    static public void check(Population pop, CheckResult result){
        if (StringUtils.isBlank(pop.getName())) {
            result.addWarning(pop, t("isisfish.error.empty.name"));
        }
        if (StringUtils.isBlank(pop.getGeographicId())) {
            result.addInfo(pop, t("isisfish.error.empty.id.geographic"));
        }
        check(pop, "MaturityOgiveEquation", pop.getMaturityOgiveEquation(), result);
        check(pop, "ReproductionRateEquation", pop.getReproductionRateEquation(), result);
        check(pop, "Growth", pop.getGrowth(), result);
        check(pop, "GrowthReverse", pop.getGrowthReverse(), result);
        List<PopulationGroup> classes = pop.getPopulationGroup();
        if (CollectionUtils.isEmpty(classes)) {
            result.addWarning(pop, t("isisfish.error.undefined.classes"));
        } else {
            for (PopulationGroup group : classes) {
                check(group, result);
            }
        }
        int gap = pop.getMonthGapBetweenReproRecrutement();
        List<PopulationGroup> groups = pop.getPopulationGroup();
        if (gap < 0 || (pop.getSpecies().isAgeGroupType() && !groups.isEmpty()
                && gap <= groups.get(0).getAge() * Month.NUMBER_OF_MONTH )) {
            result.addWarning(pop, t("isisfish.error.invalid.interval.reproduction.recruitment"));
        }
        if (isNullMatrix(pop.getMappingZoneReproZoneRecru())) {
            result.addWarning(pop, t("isisfish.error.undefined.correspondence.zones.reproduction.recruitment"));
        }
        if (CollectionUtils.isEmpty(pop.getPopulationZone())) {
            result.addWarning(pop, t("isisfish.error.undefined.zone.population"));
        }
        if (CollectionUtils.isEmpty(pop.getRecruitmentZone())) {
            result.addWarning(pop, t("isisfish.error.undefined.zone.recruitment"));
        }
        if (CollectionUtils.isEmpty(pop.getReproductionZone())) {
            result.addWarning(pop, t("isisfish.error.undefined.zone.reproduction"));
        }
        List<PopulationSeasonInfo> infoSaisons = pop.getPopulationSeasonInfo();
        if (CollectionUtils.isEmpty(infoSaisons)) {
            result.addWarning(pop, t("isisfish.error.empty.season"));
        } else {
            Set<Month> months = new HashSet<>();
            for (PopulationSeasonInfo info : infoSaisons) {
                if (!Collections.disjoint(months, info.getMonths())) {
                    result.addError(info, t("isisfish.error.overlap.season"));
                }
                months.addAll(info.getMonths());
                check(info, result);
            }
            if (months.size() != Month.NUMBER_OF_MONTH) {
                result.addError(pop, t("isisfish.error.notallyear.season"));
            }
        }
        check(pop, "Price", pop.getPrice(), result);
        check(pop, "MeanWeight", pop.getMeanWeight(), result);
        check(pop, "ReproductionEquation", pop.getReproductionEquation(), result);
        check(pop, "NaturalDeathRate", pop.getNaturalDeathRate(), result);

        if (pop.getRecruitmentDistribution() == null || sumMatrix(pop.getRecruitmentDistribution()) != 1) {
            result.addWarning(pop, t("isisfish.error.invalid.distribution.recruitment"));
        }
    }

    static public void check(FisheryRegion region, CheckResult result){
        if (StringUtils.isBlank(region.getName())) {
            result.addWarning(region, t("isisfish.error.empty.name"));
        }
        
        List<Zone> secteurs = region.getZone();
        if (CollectionUtils.isEmpty(secteurs)) {
            result.addWarning(region, t("isisfish.error.undefined.sector"));
        } else {
            for(Zone zone : secteurs) {
                check(zone, result);
            }
        }
        List<Cell> mailles = region.getCell();
        if (CollectionUtils.isEmpty(mailles)) {
            result.addWarning(region, t("isisfish.error.undefined.mesh"));
        } else {
            for (Cell cell : mailles) {
                check(cell, result);
            }
        }
        List<Species> metaPops = region.getSpecies();
        if (CollectionUtils.isEmpty(metaPops)) {
            result.addWarning(region, t("isisfish.error.undefined.meta.population"));
        } else {
            for (Species species : metaPops) {
                check(species, result);
            }
        }
        List<Metier> metiers = region.getMetier();
        if (CollectionUtils.isEmpty(metiers)) {
            result.addWarning(region, t("isisfish.error.undefined.meta.population"));
        } else {
            for (Metier metier : metiers) {
                check(metier, result);
            }
        }
        List<Strategy> strategies = region.getStrategy();
        if (CollectionUtils.isEmpty(strategies)){
            result.addWarning(region, t("isisfish.error.undefined.stategy"));
        } else {
            for (Strategy strategy : strategies) {
                check(strategy, result);
            }
        }
    }

    static public void check(Zone secteur, CheckResult result){
        if (StringUtils.isBlank(secteur.getName())) {
            result.addWarning(secteur, t("isisfish.error.empty.name"));
        }
        if (CollectionUtils.isEmpty(secteur.getCell())) {
            result.addWarning(secteur, t("isisfish.error.undefined.mesh.sector"));
        }
    }

    static public void check(Selectivity selectivite, CheckResult result){

    }


    static public void check(Strategy strategy, CheckResult result) {
        if (StringUtils.isBlank(strategy.getName())) {
            result.addWarning(strategy, t("isisfish.error.empty.name"));
        }
        check(strategy.getSetOfVessels(), result);
        
        List<StrategyMonthInfo> smi = strategy.getStrategyMonthInfo();
        if (smi == null || smi.size() != 12) {
            result.addWarning(strategy, t("isisfish.error.undefined.stategy.months"));
        } else {
            for (StrategyMonthInfo info : smi) {
                check(info, result);
            }
        }

    }

    static public void check(SetOfVessels sov, CheckResult result){
        check(sov.getPort(), result);
        if (sov.getNumberOfVessels() < 0) {
            result.addWarning(sov, t("isisfish.error.invalid.number"));
        }
        if (sov.getFixedCosts() < 0) {
            result.addWarning(sov, t("isisfish.error.invalid.costs.fix"));
        }
        Collection<EffortDescription> efforts = sov.getPossibleMetiers();
        if (CollectionUtils.isEmpty(efforts)) {
            result.addWarning(sov, t("isisfish.error.not.possible.metier"));
        } else {
            for (EffortDescription effort : efforts) {
                check(effort, result);
            }
        }
    }

    // TODO check for VesselType, TripType, Port, StrategyMonthInfo
    static public void check(EffortDescription effort, CheckResult result){

    }
    static public void check(StrategyMonthInfo smi, CheckResult result){

    }
    static public void check(VesselType vesselType, CheckResult result){

    }
    static public void check(TripType tripType, CheckResult result){

    }
    static public void check(Port port, CheckResult result){
        if (StringUtils.isBlank(port.getName())) {
            result.addWarning(port, t("isisfish.error.empty.name"));
        }
        if (port.getCell() == null) {
            result.addWarning(port, t("isisfish.error.undefined.mesh.sector"));
        }
    }

} // CheckSaisie
