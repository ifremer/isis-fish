/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2019 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input;

import cern.colt.Arrays;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.event.ItemEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.datastore.StorageChangeEvent;
import fr.ifremer.isisfish.datastore.StorageChangeListener;
import fr.ifremer.isisfish.datastore.RegionAlreadyExistException;
import fr.ifremer.isisfish.entities.FisheryRegionImpl;
import fr.ifremer.isisfish.logging.RegionChangeLogger;
import fr.ifremer.isisfish.ui.input.spatial.AskNewSpatialUI;
import fr.ifremer.isisfish.ui.widget.filterable.FilterableComboBox;
import fr.ifremer.isisfish.util.IsisFileUtil;
import freemarker.cache.ClassTemplateLoader;
import freemarker.ext.beans.BeansWrapper;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.prompt.PromptSupport;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.persistence.TopiaEntityContextable;
import org.nuiton.util.DesktopUtil;

import fr.ifremer.isisfish.IsisFishDAOHelper;
import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.datastore.RegionStorage;
import fr.ifremer.isisfish.datastore.StorageException;
import fr.ifremer.isisfish.entities.FisheryRegion;
import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.entities.RegionExportJson;
import fr.ifremer.isisfish.entities.RegionImportJson;
import fr.ifremer.isisfish.entities.Species;
import fr.ifremer.isisfish.entities.SpeciesDAO;
import fr.ifremer.isisfish.mexico.export.RegionExplorer;
import fr.ifremer.isisfish.mexico.export.RegionExport;
import fr.ifremer.isisfish.mexico.export.RegionExportFactorXML;
import fr.ifremer.isisfish.ui.NavigationHandler;
import fr.ifremer.isisfish.ui.input.tree.FisheryDataProvider;
import fr.ifremer.isisfish.ui.input.tree.FisheryTreeHelper;
import fr.ifremer.isisfish.ui.input.tree.FisheryTreeNode;
import fr.ifremer.isisfish.ui.input.tree.FisheryTreeRenderer;
import fr.ifremer.isisfish.ui.input.tree.FisheryTreeSelectionModel;
import fr.ifremer.isisfish.ui.input.tree.loadors.PopulationsNodeLoador;
import fr.ifremer.isisfish.ui.models.common.GenericComboModel;
import fr.ifremer.isisfish.vcs.VCSException;
import java.awt.Component;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import org.nuiton.topia.persistence.TopiaEntity;

/**
 * Main handler for fishery edition action.
 * Next, each ui as his own handler.
 *
 * In context : 
 * <ul>
 *  <li>FisheryRegion
 *  <li>RegionStorage
 * </ul>
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$
 * By : $Author$
 */
public class InputHandler extends NavigationHandler {

    /** Class logger. */
    private static final Log log = LogFactory.getLog(InputHandler.class);

    protected InputUI inputUI;

    protected StorageChangeListener regionListStorageListener;

    public InputHandler(InputUI inputUI) {
        this.inputUI = inputUI;
    }

    protected void initRegionListModel(StorageChangeEvent evt) {
        SwingUtilities.invokeLater(() -> {
            inputUI.fieldCurrentRegionModel.setElementList(RegionStorage.getRegionNames());
        });
    }

    /**
     * Post init ui.
     */
    public void afterInit() {
        // region list
        regionListStorageListener = this::initRegionListModel;
        RegionStorage.addStorageListener(regionListStorageListener);
        this.initRegionListModel(null);

        // new region
        PromptSupport.setPrompt(t("isisfish.input.newRegion"), inputUI.getFieldNewRegion());
    }

    /**
     * Load region by region name, set it into jaxx context and refresh ui.
     *
     * Before loading region, try to close old one.
     *
     * @param name region name to load
     */
    public void loadRegion(String name) {
        if (log.isDebugEnabled()) {
            log.debug("Load region " + name);
        }

        if (name == null) {
            // show empty region ui
            InputContext.setStorage(null);
            InputContext.setDb(null);
            inputUI.getCardlayoutPrincipal().show(inputUI.getInputPanePrincipal(), "none");
            TreeModel model = new DefaultTreeModel(null);
            inputUI.getFisheryRegionTree().setModel(model);
        } else {
            FisheryRegion fisheryRegion;
            RegionStorage regionStorage;
            TopiaContext topiaContext;
            RegionChangeLogger regionChangeLogger;

            // load region
            try {
                regionStorage = RegionStorage.getRegion(name);
                topiaContext = regionStorage.getStorage().beginTransaction();
                regionChangeLogger = new RegionChangeLogger(regionStorage, topiaContext);
                // warning : this is a weak reference
                topiaContext.addTopiaEntityListener(regionChangeLogger);
                fisheryRegion = RegionStorage.getFisheryRegion(topiaContext);

                InputContext.setStorage(regionStorage);
                InputContext.setDb(topiaContext);
            } catch (TopiaException | StorageException ex) {
                throw new IsisFishRuntimeException("Can't load region", ex);
            }

            // TODO echatellier 20110323 voir pour remplacer le binding
            inputUI.setRegionLoaded(fisheryRegion != null);

            // init tree model loader with fishery region
            FisheryTreeHelper fisheryTreeHelper = new FisheryTreeHelper();
            FisheryDataProvider dataProvider = new FisheryDataProvider(fisheryRegion);
            fisheryTreeHelper.setDataProvider(dataProvider);
            TreeModel fisheryTreeModel = fisheryTreeHelper.createTreeModel(fisheryRegion);
            inputUI.getFisheryRegionTree().setCellRenderer(null); // fix region opening after import
            inputUI.getFisheryRegionTree().setModel(fisheryTreeModel);
            inputUI.getFisheryRegionTree().setCellRenderer(new FisheryTreeRenderer(dataProvider));
            inputUI.getFisheryRegionTree().setSelectionModel(new FisheryTreeSelectionModel(inputUI));
            fisheryTreeHelper.setUI(inputUI.getFisheryRegionTree(), true, false, null);

            // global context value : fisheryRegion, regionStorage, treeHelper
            inputUI.setContextValue(fisheryRegion);
            inputUI.setContextValue(regionStorage);
            inputUI.setContextValue(fisheryTreeHelper);
            inputUI.setContextValue(fisheryTreeModel);
            inputUI.setContextValue(topiaContext);
            inputUI.setContextValue(regionChangeLogger); // just to keep reference (totally useless)

            inputUI.getCardlayoutPrincipal().show(inputUI.getInputPanePrincipal(),"normale");

            // autoselect root node (fire some event)
            fisheryTreeHelper.selectNode((FisheryTreeNode)fisheryTreeModel.getRoot());
        }
    }

    /**
     * Reload current loaded fishery tree.
     */
    public void reloadFisheryTree() {
        FisheryTreeHelper fisheryTreeHelper = inputUI.getContextValue(FisheryTreeHelper.class);
        TreeModel fisheryTreeModel = inputUI.getContextValue(TreeModel.class);
        fisheryTreeHelper.refreshNode((FisheryTreeNode)fisheryTreeModel.getRoot(), true);
    }

    /**
     * Main ui fishery region selection changed.
     *
     * @param e event
     */
    public void regionChange(ItemEvent e) {
        // event launched twice with itemchange listener
        if (e.getStateChange() == ItemEvent.SELECTED) {
            final String name = (String)inputUI.getFieldCurrentRegion().getSelectedItem();
            if (log.isDebugEnabled()) {
                log.debug("New region selected " + name);
            }

            // long operation, run status bar
            setStatusMessage(inputUI, t("isisfish.message.loading.region", name), true);
            inputUI.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

            SwingUtilities.invokeLater(() -> {
                loadRegion(name);
                setStatusMessage(inputUI, t("isisfish.message.load.finished"));
                inputUI.setCursor(Cursor.getDefaultCursor());
            });
        }
    }

    /**
     * Create new region (called if region name is not empty).
     */
    public void createNewRegion() {
        String name = inputUI.getFieldNewRegion().getText();
        setStatusMessage(inputUI, t("isisfish.message.creating.region", name), true);

        if (RegionStorage.getRegionNames().contains(name)) {
            JOptionPane.showMessageDialog(inputUI, t("isisfish.error.region.already.exists"));
        } else {
            try {
                RegionStorage.create(name);
            } catch (StorageException ex) {
                throw new IsisFishRuntimeException("Can't create region", ex);
            }
            inputUI.getFieldNewRegion().setText("");
            //refreshRegionList(name);
            inputUI.getFieldCurrentRegion().setSelectedItem(name);
        }

        setStatusMessage(inputUI, t("isisfish.message.creation.finished"));
    }

    /**
     * Exporter la region dans un zip.
     */
    public void importRegion() {
        setStatusMessage(inputUI, t("isisfish.message.import.zip"), true);

        File file = IsisFileUtil.getFile(".*.zip$",
                t("isisfish.message.import.region.zipped"));

        if (file != null) {
            try {
                RegionStorage.importZip(file);
            } catch (RegionAlreadyExistException eee) {
                String regionName = eee.getRegionName();
                //Erreur à l'import probablement parce qu'une autre région existe déjà
                String[] options = {t("isisfish.message.import.region.delete"), t("isisfish.message.import.region.rename"), t("isisfish.message.import.region.cancel")};
                int select = JOptionPane.showOptionDialog(inputUI,
                        t("isisfish.message.import.region.exists", regionName),
                        t("isisfish.message.import.region.select.option"),
                        JOptionPane.DEFAULT_OPTION,
                        JOptionPane.INFORMATION_MESSAGE,
                        null,
                        options,
                        options[2]);

                if (select == 1) {
                    // ask new region name
                    String newRegionName = JOptionPane.showInputDialog(
                        t("isisfish.message.import.region.name"), regionName);

                    if (StringUtils.isNotBlank(newRegionName)) {
                        if (RegionStorage.getRegionNames().contains(newRegionName)) {
                            JOptionPane.showMessageDialog(inputUI, t("isisfish.error.region.already.exists"));
                        }
                        else {
                            try {
                                RegionStorage.importAndRenameZip(file, newRegionName);
                            } catch (Exception exception) {
                                throw new IsisFishRuntimeException(t("isisfish.error.region.import"), exception);
                            }
                        }
                    }
                } else if (select == 0) {
                    try {
                        //delete previous
                        RegionStorage previousRegion = RegionStorage.getRegion(regionName);
                        if (previousRegion != null) {
                            previousRegion.delete(false);
                        }
                        //import new one
                        RegionStorage.importZip(file);
                    } catch (Exception exception) {
                        throw new IsisFishRuntimeException(t("isisfish.error.region.import"), exception);
                    }
                }
                //Si on ne souhaite ni renommer, ni supprimer, on ne fait rien (annulation)
            } catch (Exception eee) {
                throw new IsisFishRuntimeException(t("isisfish.error.region.import"), eee);
            }
        }
        setStatusMessage(inputUI, t("isisfish.message.import.finished"));
    }

    /**
     * Exporter la region dans un zip.
     */
    public void importRegionAndRename() {
        setStatusMessage(inputUI, t("isisfish.message.import.zip"), true);

        try {
            File file = IsisFileUtil.getFile(".*.zip$",
                    t("isisfish.message.import.region.zipped"));
            if (file != null) {

                String newName = JOptionPane
                        .showInputDialog(t("isisfish.message.name.imported.region"));
                RegionStorage.importAndRenameZip(file, newName);
            }
        } catch (Exception eee) {
            throw new IsisFishRuntimeException(t("isisfish.error.region.import"), eee);
        }

        setStatusMessage(inputUI, t("isisfish.message.import.finished"));
    }

    /**
     * Extract from a simulation the region, and rename it with name given
     * by user.
     */
    public void importRegionFromSimulation() {

        setStatusMessage(inputUI, t("isisfish.message.import"), true);

        // first step select a simulation and new region name
        try {
            List<String> simulationNames = SimulationStorage.getSimulationNames();

            // build component for JOptionPane
            JPanel panel = new JPanel();
            panel.setLayout(new BorderLayout());
            JLabel label = new JLabel(t("isisfish.message.import.selectSimulation"));
            panel.add(label, BorderLayout.NORTH);
            FilterableComboBox<String> comboBox = new FilterableComboBox<>();
            comboBox.setModel(new GenericComboModel<>(simulationNames));
            panel.add(comboBox, BorderLayout.SOUTH);

            int response = JOptionPane.showConfirmDialog(inputUI, panel, t("isisfish.message.import"),
                    JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
            String simulationName = (String)comboBox.getSelectedItem();
            if (response == JOptionPane.OK_OPTION && simulationName != null) {

                if (log.isInfoEnabled()) {
                    log.info("simulation used " + simulationName);
                }

                // ask new region name
                String regionName = (String)JOptionPane.showInputDialog(inputUI,
                        t("isisfish.message.import.region.name"), t("isisfish.message.import"),
                        JOptionPane.QUESTION_MESSAGE, null, null, "region from " + simulationName);

                if (StringUtils.isNotBlank(regionName)) {
                    if (RegionStorage.getRegionNames().contains(regionName)) {
                        JOptionPane.showMessageDialog(inputUI, t("isisfish.error.region.already.exists"));
                    } else {
                        SimulationStorage.getSimulation(simulationName).extractRegion(regionName);
                    }
                }
            }

        } catch (Exception eee) {
            throw new IsisFishRuntimeException("Can't import region from simulation", eee);
        }

        setStatusMessage(inputUI, t("isisfish.message.export.done"));
    }

    /**
     * Exporter la region dans un zip
     *
     * Call only if region is loaded.
     */
    public void exportRegion() {
        try {
            File file = IsisFileUtil.getFile(".*.zip$",
                    t("isisfish.message.import.region.zipped"));

            if (file != null) {
                // add .zip extension is not set
                file = IsisFileUtil.addExtensionIfNeeded(file, "zip");

                int resp = JOptionPane.YES_OPTION;
                if (file.exists()) {
                    resp = JOptionPane.showConfirmDialog(inputUI,
                            t("isisfish.message.file.overwrite"));
                }
                if (resp == JOptionPane.YES_OPTION) {
                    RegionStorage regionStorage = inputUI.getContextValue(RegionStorage.class);
                    regionStorage.createZip(file);
                }
            }
        } catch (IOException eee) {
            throw new IsisFishRuntimeException("Can't export region", eee);
        }
    }

     /**
     * Exporter une entite json gzip
     */
    protected void exportJson(TopiaEntity e) {
        try {
            File file = IsisFileUtil.getFile(t("isisfish.input.menu.exportJson"), t("isisfish.common.export"), (Component)null,
                    ".*" + RegionExportJson.FORMAT_EXTENSION + "$", t("isisfish.message.import.json.zipped"));

            if (file != null) {
                file = IsisFileUtil.addExtensionIfNeeded(file, RegionExportJson.FORMAT_EXTENSION);

                int resp = JOptionPane.YES_OPTION;
                if (file.exists()) {
                    resp = JOptionPane.showConfirmDialog(inputUI,
                            t("isisfish.message.file.overwrite"));
                }
                if (resp == JOptionPane.YES_OPTION) {
                    try (OutputStream out = new BufferedOutputStream(
                            new GZIPOutputStream(new FileOutputStream(file)))) {
                        RegionExportJson json = new RegionExportJson(new OutputStreamWriter(out, StandardCharsets.UTF_8));
                        json.export(e);
                    }
                }
            }
        } catch (Exception eee) {
            throw new IsisFishRuntimeException("Can't export region", eee);
        }
    }

    public void exportEntityJson() {
        Component[] cs = inputUI.getInputPane().getComponents();
        if (cs != null && cs.length == 1 && cs[0] instanceof InputContentUI) {
            InputContentUI inputContentUI = (InputContentUI)cs[0];
            TopiaEntityContextable e = inputContentUI.getBean();
            if (e != null) {
                log.info("try to export json version of: " + e);
                exportJson(e);
            } else {
                log.error("Can't find entity in : " + inputContentUI);
            }
        } else {
            log.error("Can't find InputContentUI in : " + Arrays.toString(cs));
        }
    }

    public void importEntityJson() {
        setStatusMessage(inputUI, t("isisfish.message.import.zip"), true);
        try {
            InputSaveVerifier inputSaveVerifier = inputUI.getContextValue(InputSaveVerifier.class);
            if (inputSaveVerifier.checkEdit() != JOptionPane.CANCEL_OPTION) {
                File file = IsisFileUtil.getFile(t("isisfish.input.menu.importJson"), t("isisfish.common.import"), (Component)null,
                        ".*" + RegionExportJson.FORMAT_EXTENSION + "$", t("isisfish.message.import.json.zipped"));
                if (file != null) {
                    file = IsisFileUtil.addExtensionIfNeeded(file, RegionExportJson.FORMAT_EXTENSION);
                    FisheryRegion fisheryRegion = inputUI.getContextValue(FisheryRegion.class);
                    TopiaContext tx = fisheryRegion.getTopiaContext();
                    RegionImportJson.RegionMergeDatabase merge =
                            new RegionImportJson.RegionMergeDatabase(tx);
                    try (InputStream in = new BufferedInputStream(new GZIPInputStream(new FileInputStream(file)))) {
                        RegionImportJson json = new RegionImportJson(new InputStreamReader(in, StandardCharsets.UTF_8),
                                merge);
                        Collection<TopiaEntity> entities = json.getEntities();
                        log.info("Entities to import: " + entities.size());
                        for (TopiaEntity e : entities) {
                            tx.add(e);
                        }
                        tx.commitTransaction();
                        log.info("Import merged");
                    } catch (Exception eee) {
                        tx.rollbackTransaction();
                        if (!merge.isAbort()) {
                            throw eee;
                        } else {
                            log.info("Import aborted");
                        }
                    }
                    inputSaveVerifier.cancel();
                    loadRegion(fisheryRegion.getName());
                }
            }
        } catch (Exception eee) {
            throw new IsisFishRuntimeException("Can't import region", eee);
        }

        setStatusMessage(inputUI, t("isisfish.message.import.finished"));
    }

    /**
     * Remove region.
     *
     * @param vcsDelete if true delete region in CVS too
     */
    public void removeRegion(boolean vcsDelete) {
        try {
            RegionStorage regionStorage = inputUI.getContextValue(RegionStorage.class);
            int resp = JOptionPane.showConfirmDialog(inputUI,
                    t("isisfish.message.confirm.remove.region", regionStorage.getName()));
            if (resp == JOptionPane.YES_OPTION) {
                loadRegion(null);
                //regionStorage.closeStorage();
                regionStorage.delete(vcsDelete);
            }
        } catch (StorageException eee) {
            throw new IsisFishRuntimeException("Can't delete region", eee);
        }
    }

    /**
     * Copy la region avec un autre nom.
     */
    public void copyRegion() {
        try {
            String newName = JOptionPane
                    .showInputDialog(t("isisfish.message.new.region.name"));

            if (StringUtils.isNotEmpty(newName)) {
                RegionStorage regionStorage = inputUI.getContextValue(RegionStorage.class);
                File zip = regionStorage.createZip();
                RegionStorage.importAndRenameZip(zip, newName);
            }

        } catch (IOException | StorageException | RegionAlreadyExistException eee) {
            throw new IsisFishRuntimeException("Can't copy region", eee);
        }
    }

    /**
     * Commit region.
     */
    public void commitRegionInCVS() {
        try {
            RegionStorage regionStorage = inputUI.getContextValue(RegionStorage.class);
            setStatusMessage(inputUI, t("isisfish.message.commiting.region", regionStorage.getName()), true);
            String msg = regionStorage.getCommentForNextCommit();
            JTextArea text = new JTextArea(msg);
            int resp = JOptionPane.showOptionDialog(null,
                    new JScrollPane(text), t("isisfish.commit.message"),
                    JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,
                    null, // icon
                    null, null);
            if (resp == JOptionPane.OK_OPTION) {
                regionStorage.commit(text.getText());
                regionStorage.clearCommentForNextCommit();
                setStatusMessage(inputUI, t("isisfish.message.region.commited"));
            } else {
                setStatusMessage(inputUI, t("isisfish.message.commit.region.canceled"));
            }
        } catch (VCSException | IOException eee) {
            throw new IsisFishRuntimeException("Can't commit region", eee);
        }
        setStatusMessage(inputUI, t("isisfish.message.export.done"));
    }

    /**
     * Explore region and export any enabled sensitivity factor name with value.
     */
    public void exportRegionSensitivityFactors() {

        try {
            File exportFile = IsisFileUtil.getFile(t("isisfish.input.sensitivity.export.title"),
                    t("isisfish.common.ok"), inputUI, ".*\\.xml", "XML Files");

            if (exportFile != null) {
                // make sur that filename ends with ".xml"
                if (!exportFile.getAbsolutePath().endsWith(".xml")) {
                    exportFile = new File(exportFile.getAbsolutePath() + ".xml");
                }

                FisheryRegion fisheryRegion = inputUI.getContextValue(FisheryRegion.class);
                setStatusMessage(inputUI, t("isisfish.input.sensitivity.export.running"), true);
                RegionExplorer explorer = new RegionExplorer();
                RegionExport exportXML = new RegionExportFactorXML(exportFile);
                explorer.explore(fisheryRegion, exportXML);
                setStatusMessage(inputUI, t("isisfish.input.sensitivity.export.complete"), true);
            }
            else {
                setStatusMessage(inputUI, t("isisfish.input.sensitivity.export.cancel"), true);
            }
        } catch (TopiaException eee) {
            throw new IsisFishRuntimeException("Can't export sensitivity factors", eee);
        }
    }

    /**
     * Changement de selection dans l'arbre de la pecherie.
     *
     * @param event
     */
    public void nodeSelectionChanged(TreeSelectionEvent event) {

        TreePath newTreePath = event.getNewLeadSelectionPath();
        if (newTreePath != null) {
            Object lastTreePath = newTreePath.getLastPathComponent();
            if (lastTreePath instanceof FisheryTreeNode) {
                FisheryTreeNode isisTreeNode = (FisheryTreeNode)lastTreePath;

                Class<?> internalClass = isisTreeNode.getInternalClass();
                TopiaEntityContextable topiaEntity = null;
                String topiaId = isisTreeNode.getId();

                try {
                    // noeud qui n'en charge pas d'autres (= un bean)
                    if (isisTreeNode.isStaticNode()) {
                        FisheryRegion fisheryRegion = inputUI.getContextValue(FisheryRegion.class);
                        TopiaContext topiaContext = fisheryRegion.getTopiaContext();
                        topiaEntity = (TopiaEntityContextable)topiaContext.findByTopiaId(topiaId);
                    }

                    InputContentUI inputContentUI = getUIInstanceForBeanClass(internalClass, inputUI);
                    inputContentUI.getSaveVerifier().setInputContentUI(inputContentUI);

                    // mandatory set
                    inputContentUI.getSaveVerifier().reset(); // before set bean !!!
                    if (topiaEntity != null) {
                        inputContentUI.getSaveVerifier().addCurrentEntity(topiaEntity);
                    }

                    inputContentUI.setBean(null); // hack to refresh ui and force next line to trigger change event
                    inputContentUI.setBean(topiaEntity);
                    inputContentUI.setActive(topiaEntity != null);

                    // add initialized ui to panel
                    inputUI.getCardlayoutPrincipal().show(inputUI.getInputPanePrincipal(), "normale");
                    inputUI.getInputPane().removeAll();
                    inputUI.getInputPane().add(inputContentUI, BorderLayout.CENTER);
                    inputUI.getInputPane().repaint();
                    inputUI.getInputPane().validate();
                } catch (Exception ex) {
                    throw new IsisFishRuntimeException("Can't display bean " + topiaId, ex);
                }
            }
        }
    }

    /**
     * Delete tree node referenced by parent, and auto select parent node.
     *
     * @param topiaId node id to delete
     */
    public void deleteTreeNode(String topiaId) {
        FisheryTreeHelper fisheryTreeHelper = inputUI.getContextValue(FisheryTreeHelper.class);
        TreeModel fisheryTreeModel = inputUI.getContextValue(TreeModel.class);
        FisheryTreeNode newSelectNode = fisheryTreeHelper.findNode((FisheryTreeNode)fisheryTreeModel.getRoot(), topiaId);
        FisheryTreeNode parentNode = newSelectNode.getParent();
        fisheryTreeHelper.selectNode(parentNode);
        fisheryTreeHelper.removeNode(newSelectNode);
    }

    /**
     * Insert new tree node and select it.
     *
     * @param nodeClass node type to create
     * @param topiaEntity node to insert
     */
    public void insertTreeNode(Class nodeClass, TopiaEntityContextable topiaEntity) {
        FisheryTreeHelper fisheryTreeHelper = inputUI.getContextValue(FisheryTreeHelper.class);

        // on part du principe que pour ne pas compliquer les ui est les lier
        // à l'arbre, on ajoute un nouveau noeud en fils de celui selectionné
        // (ou son parent si la creation à lieu à partir d'un noeud bean)
        FisheryTreeNode selectedNode = fisheryTreeHelper.getSelectedNode();
        if (selectedNode.isStaticNode()) {
            selectedNode = selectedNode.getParent();
        }

        // must use loador to properly load species node
        /*FisheryTreeNodeLoador typeNodeLoador = fisheryTreeHelper.getLoadorFor(nodeClass);
        FisheryTreeNode newNode = null;
        if (newNode != null) {
            newNode = (FisheryTreeNode)typeNodeLoador.createNode(topiaEntity, null);
        } else {
            // FIXME echatellier 20110418 cas non résolu du node "population"
            // pour lequel on n'a pas le loador
            newNode = new FisheryTreeNode(nodeClass, topiaEntity.getTopiaId(), null, null);
        }*/

        // FIXME echatellier 20110429 Hack en dur pour population
        FisheryTreeNode newNode = new FisheryTreeNode(nodeClass, topiaEntity.getTopiaId(), null, null);
        fisheryTreeHelper.insertNode(selectedNode, newNode);
        if (nodeClass.equals(Species.class)) {
            FisheryTreeNode newPopNode = new FisheryTreeNode(
                    Population.class, n("isisfish.input.tree.populations"),
                    null, new PopulationsNodeLoador((Species)topiaEntity));
            fisheryTreeHelper.insertNode(newNode, newPopNode);
        }
        fisheryTreeHelper.selectNode(newNode);
    }

    /**
     * Update tree node for topiaId.
     *
     * @param topiaId node id to update
     */
    public void updateTreeNode(String topiaId) {
        FisheryTreeHelper fisheryTreeHelper = inputUI.getContextValue(FisheryTreeHelper.class);
        TreeModel fisheryTreeModel = inputUI.getContextValue(TreeModel.class);
        FisheryTreeNode newSelectNode = fisheryTreeHelper.findNode((FisheryTreeNode)fisheryTreeModel.getRoot(), topiaId);
        fisheryTreeHelper.refreshNode(newSelectNode, false);
    }

    /**
     * Dans le cas d'une creation de population, on doit la creer dans
     * une espèce. On doit rechercher celle qui est sélectionnée dans l'arbre.
     *
     * @return selected species
     */
    public Species findSpecies() {
        // get selected node
        FisheryTreeHelper treeHelper = inputUI.getContextValue(FisheryTreeHelper.class);
        FisheryTreeNode selectedNode = treeHelper.getSelectedNode();
        if (selectedNode == null) {
            throw new IsisFishRuntimeException("Not selected tree node");
        }

        // look for parent node (Species type)
        String speciesId;
        FisheryTreeNode parentNode = selectedNode.getParent();
        if (parentNode.getInternalClass().equals(Species.class)) {
            // cas selection du noeud type "Population"
            speciesId = parentNode.getId();
        } else if (parentNode.getParent().getInternalClass().equals(Species.class)) {
            // cas où on est deja sur une population
            speciesId = parentNode.getParent().getId();
        } else {
            throw new IsisFishRuntimeException("Not selected tree node");
        }

        // look for entities
        TopiaContext topiaContext = inputUI.getContextValue(TopiaContext.class);
        Species result;
        try {
            SpeciesDAO dao = IsisFishDAOHelper.getSpeciesDAO(topiaContext);
            result = dao.findByTopiaId(speciesId);
        } catch (TopiaException e) {
            throw new IsisFishRuntimeException("Can't find ");
        }

        return result;
    }

    /**
     * Changement de la resolution spatiale.
     */
    public void changeSpatialResolution() {
        InputSaveVerifier inputSaveVerifier = inputUI.getContextValue(InputSaveVerifier.class);
        if (inputSaveVerifier.checkEdit() != JOptionPane.CANCEL_OPTION) {
            AskNewSpatialUI askNewSpatialUI = new AskNewSpatialUI(inputUI);

            FisheryRegion fisheryRegion = inputUI.getContextValue(FisheryRegion.class);
            FisheryRegion newFisheryRegion = new FisheryRegionImpl();
            newFisheryRegion.setName(fisheryRegion.getName());
            newFisheryRegion.setCellLengthLatitude(fisheryRegion.getCellLengthLatitude());
            newFisheryRegion.setCellLengthLongitude(fisheryRegion.getCellLengthLongitude());
            newFisheryRegion.setMaxLatitude(fisheryRegion.getMaxLatitude());
            newFisheryRegion.setMaxLongitude(fisheryRegion.getMaxLongitude());
            newFisheryRegion.setMinLatitude(fisheryRegion.getMinLatitude());
            newFisheryRegion.setMinLongitude(fisheryRegion.getMinLongitude());
            newFisheryRegion.setMapFileList(fisheryRegion.getMapFileList());
            askNewSpatialUI.setContextValue(newFisheryRegion, "newFisheryRegion");
            askNewSpatialUI.setBean(newFisheryRegion);

            askNewSpatialUI.pack();
            askNewSpatialUI.setLocationRelativeTo(inputUI);
            askNewSpatialUI.setVisible(true);
        }
    }

    /**
     * Realise un export JSON et affiche la page de visualisation dans un navigateur.
     */
    public void exportWeb() {
        try {
            StringWriter outJson = new StringWriter();
            RegionExportJson json = new RegionExportJson(outJson);

            FisheryRegion fisheryRegion = inputUI.getContextValue(FisheryRegion.class);
            json.export(fisheryRegion);

            Configuration freemarkerConfiguration = new Configuration(Configuration.VERSION_2_3_25);
            freemarkerConfiguration.setDefaultEncoding("utf-8");
            ClassTemplateLoader templateLoader = new ClassTemplateLoader(InputHandler.class, "/");
            freemarkerConfiguration.setTemplateLoader(templateLoader);
            freemarkerConfiguration.setObjectWrapper(new BeansWrapper(Configuration.VERSION_2_3_25));

            // get template
            Template template = freemarkerConfiguration.getTemplate("templates/web/region-web.html");
            Map<String, Object> root = new HashMap<>();
            root.put("fisheryRegion", fisheryRegion);
            root.put("jsonExport", "\nvar jsonExport = " + outJson.toString() + ";");

            // render template
            File outFile = IsisFileUtil.createTempFile("isis-export-", ".html");
            outFile.deleteOnExit();
            Writer outWriter = new OutputStreamWriter(new BufferedOutputStream(new FileOutputStream(outFile)), StandardCharsets.UTF_8);
            template.process(root, outWriter);
            outWriter.flush();

            // open file
            DesktopUtil.browse(outFile.toURI());

        } catch (IOException|TemplateException ex) {
            throw new IsisFishRuntimeException("Can't export web", ex);
        }

    }
}
