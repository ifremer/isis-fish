/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.equation;

import java.awt.Color;
import java.awt.Desktop;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.swing.event.HyperlinkEvent;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.HTMLFrameHyperlinkEvent;
import javax.swing.text.html.StyleSheet;

import fr.ifremer.isisfish.util.IsisFileUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.ifremer.isisfish.util.CompileHelper;
import fr.ifremer.isisfish.util.EvaluatorHelper;

/**
 * Equation editor handler.
 */
public class EquationEditorPaneHandler {

    /** Class logger. */
    private static final Log log = LogFactory.getLog(EquationEditorPaneHandler.class);

    protected EquationEditorPaneUI equationUI;

    protected Class javaInterface = null;

    public EquationEditorPaneHandler(EquationEditorPaneUI equationUI) {
        this.equationUI = equationUI;
    }

    protected void afterInit() {
        equationUI.split.setDividerLocation(0.8);
    }
    
    public void setEquation(String category, String name, Class javaInterface, String content) throws Exception {
        this.javaInterface = javaInterface;
        equationUI.getDoc().setText(CompileHelper.extractDoc(category, name, javaInterface));
        equationUI.getEditor().open(IsisFileUtil.getTempFile(content, ".java"));
    }

    public HTMLEditorKit configureEditorKit() {
        HTMLEditorKit htmlEditorKit = new HTMLEditorKit();
        StyleSheet styleSheet = htmlEditorKit.getStyleSheet();
        styleSheet.addRule("""  
                p {
                    margin: 0;
                }
                
                .equation-name {
                    font-size: 10px;
                    font-weight: bold;
                    color: #333;
                    margin: 0;
                }
                
                .heading {
                    font-size: 9px;
                    font-weight: bold;
                    color: #333;
                    margin: 5px 0 0;
                }
                
                .params-table {
                    width: 100%;
                    border: 1px solid #ddd;
                    margin: 0;
                }
                
                .params-table th,
                .params-table td {
                    text-align: left;
                    padding: 3px;
                    background-color: #f2f2f2;
                    margin: 0;
                }
                
                .default-content {
                    background-color: #f2f2f2;
                    border-left: 4px solid #ddd;
                    padding: 3px;
                    margin: 0;
                }
                """);
        return htmlEditorKit;
    }

    public void docEditorHyperlinkUpdate(HyperlinkEvent e) {
        if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
            if (e instanceof HTMLFrameHyperlinkEvent) {
                ((HTMLDocument)equationUI.doc.getDocument()).processHTMLFrameHyperlinkEvent(
                    (HTMLFrameHyperlinkEvent)e);
            } else {
                try {
                    if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
                        Desktop.getDesktop().browse(e.getURL().toURI());
                    } else {
                        equationUI.doc.setPage(e.getURL());
                    }
                } catch (Exception ex) {
                    if (log.isErrorEnabled()) {
                        log.error("Error for : " + e.getURL(), ex);
                    }
                }
            }
        }
    }

    public void check() {
        try {
            String content = equationUI.editor.getText();
            
            StringWriter result = new StringWriter();
            PrintWriter out = new PrintWriter(result);
            int compileResult;
            try {
                compileResult = EvaluatorHelper.check(javaInterface, content, out);
            } catch (Exception eee) {
                compileResult = -1;
            }
            
            out.flush();
            if (compileResult != 0) {
                equationUI.checkWindow.setText(result.toString());
                equationUI.checkWindow.setBackground(Color.RED);
            } else {
                equationUI.checkWindow.setText("Compilation Ok");
                equationUI.checkWindow.setBackground(Color.WHITE);
            }
                        
        } catch(Exception e){
            log.error("Can't check script", e);
        }
    }
}
