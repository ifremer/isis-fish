/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2010 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.check;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumnModel;

import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaId;

/**
 * CheckResultTableModel.
 *
 * Created: 10 janv. 2004
 *
 * @author Benjamin Poussin &lt;poussin@codelutin.com&gt;
 * @version $Revision$
 *
 * Mise a jour: $Date$
 * par : $Author$
 */
public class CheckResultTableModel extends AbstractTableModel implements
        Comparator<Object[]> { // CheckResultTableModel

    /** serialVersionUID. */
    private static final long serialVersionUID = 328398134761266970L;

    protected CheckResult checkResult;
    protected List<Object[]> data;
    protected int sortedBy = -1;
    protected boolean ascending = true;

    public CheckResultTableModel(CheckResult checkResult) {
        setCheckResult(checkResult);
    }

    public void setCheckResult(CheckResult checkResult) {
        this.checkResult = checkResult;
        computeData();
    }

    protected void computeData() {
        data = new ArrayList<>();
        for (Object anOrder : checkResult.order) {
            TopiaEntity o = (TopiaEntity) anOrder;
            String type = TopiaId.getClassNameAsString(o.getTopiaId());

            addOneData(o, type, "error", checkResult.error.get(o), data);
            addOneData(o, type, "warning", checkResult.warning.get(o),
                    data);
            addOneData(o, type, "info", checkResult.info.get(o), data);
        }
        fireTableDataChanged();
    }

    protected void addOneData(Object o, String type, String level, List<String> list,
            Collection<Object[]> result) {
        if (list != null && list.size() != 0) {
            for (String message : list) {
                result.add(new Object[]{type, o, level, message});
            }
        }
    }

    String[] titles = new String[] { "Class", "Object", "Level", "Message" };

    public String getColumnName(int column) {
        return titles[column];
    }

    public int getRowCount() {
        return data.size();
    }

    public int getColumnCount() {
        return 4;
    }

    public Object getValueAt(int row, int column) {
        Object[] rowData = data.get(row);
        return rowData[column];
    }

    public void setSortedBy(int column) {
        if (column == sortedBy) {
            ascending = !ascending;
        } else {
            sortedBy = column;
            ascending = true;
        }
        data.sort(this);
        fireTableDataChanged();
    }

    public int compare(Object[] data1, Object[] data2) {
        int result = data1[sortedBy].toString().compareTo(
                data2[sortedBy].toString());
        return ascending ? result : -result;
    }

    public void addMouseListenerToHeaderInTable(final JTable table) {
        table.setColumnSelectionAllowed(false);
        MouseAdapter listMouseListener = new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                TableColumnModel columnModel = table.getColumnModel();
                int viewColumn = columnModel.getColumnIndexAtX(e.getX());
                int column = table.convertColumnIndexToModel(viewColumn);
                if (e.getClickCount() == 1 && column != -1) {
                    setSortedBy(column);
                }
            }
        };
        JTableHeader th = table.getTableHeader();
        th.addMouseListener(listMouseListener);
    }

} // CheckResultTableModel
