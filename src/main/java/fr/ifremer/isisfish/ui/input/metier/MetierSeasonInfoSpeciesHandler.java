/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.metier;

import static org.nuiton.i18n.I18n.t;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.ifremer.isisfish.IsisFishDAOHelper;
import fr.ifremer.isisfish.entities.Equation;
import fr.ifremer.isisfish.entities.Metier;
import fr.ifremer.isisfish.entities.MetierSeasonInfo;
import fr.ifremer.isisfish.entities.Species;
import fr.ifremer.isisfish.entities.TargetSpecies;
import fr.ifremer.isisfish.entities.TargetSpeciesDAO;
import fr.ifremer.isisfish.ui.input.InputContentHandler;
import fr.ifremer.isisfish.ui.models.common.GenericComboModel;
import fr.ifremer.isisfish.util.ErrorHelper;
import fr.ifremer.isisfish.ui.widget.editor.EquationTableEditor;

/**
 * Metier tab handler.
 */
public class MetierSeasonInfoSpeciesHandler extends InputContentHandler<MetierSeasonInfoSpeciesUI> {

    /** Class logger. */
    private static final Log log = LogFactory.getLog(MetierSeasonInfoSpeciesHandler.class);

    protected MetierSeasonInfoSpeciesHandler(MetierSeasonInfoSpeciesUI inputContentUI) {
        super(inputContentUI);
    }

    protected void afterInit() {

        inputContentUI.addPropertyChangeListener(MetierSeasonInfoSpeciesUI.PROPERTY_BEAN, evt -> {
            setSeasonModel();
            inputContentUI.setSpecies(null);
            inputContentUI.setMetierSeasonInfo(null);
            setTargetSpeciesModel();
            setTableTargetSpeciesModel();
        });
    }
    
    protected void setSeasonModel() {
        List<MetierSeasonInfo> metierSeasonInfo = null;
        
        if (inputContentUI.getBean() != null) {
            metierSeasonInfo = inputContentUI.getBean().getMetierSeasonInfo();
        }
        GenericComboModel<MetierSeasonInfo> seasonModel = new GenericComboModel<>(metierSeasonInfo);
        inputContentUI.fieldMetierSeasonInfo.setModel(seasonModel);
    }

    protected void metierSeasonInfoChanged() {
        MetierSeasonInfo selectedMSI = (MetierSeasonInfo)inputContentUI.fieldMetierSeasonInfo.getSelectedItem();
        inputContentUI.setMetierSeasonInfo(selectedMSI);
        if (selectedMSI != null) {
            inputContentUI.getSaveVerifier().addCurrentEntity(inputContentUI.getMetierSeasonInfo());
            setTableTargetSpeciesModel();
        }
    }

    protected void setTargetSpeciesModel() {
        List<Species> species = inputContentUI.getFisheryRegion().getSpecies();
        GenericComboModel<Species> fieldTargetSpeciesModel = new GenericComboModel<>(species);
        inputContentUI.fieldTargetSpecies.setModel(fieldTargetSpeciesModel);
    }

    protected void speciesChanged() {
        Species species = (Species)inputContentUI.fieldTargetSpecies.getSelectedItem();
        inputContentUI.setSpecies(species);
    }

    protected void setTableTargetSpeciesModel() {
        List<TargetSpecies> targetSpecies = new ArrayList<>();

        if (inputContentUI.getBean() != null && inputContentUI.getMetierSeasonInfo() != null) {
            // SpeciesTargetSpecies can be null durring region creation
            if (inputContentUI.getMetierSeasonInfo().getSpeciesTargetSpecies() != null) {
                // move collection to list
                // and add all entity to verifier
                for (TargetSpecies oneTargetSpecies : inputContentUI.getMetierSeasonInfo().getSpeciesTargetSpecies()) {
                    targetSpecies.add(oneTargetSpecies);
                    inputContentUI.getSaveVerifier().addCurrentEntity(oneTargetSpecies);
                    oneTargetSpecies.addPropertyChangeListener(evt -> inputContentUI.changeModel.setStayChanged(true));
                }
            }
        }
        
        // set table model
        MetierSeasonInfoTargetSpeciesTableModel model = new MetierSeasonInfoTargetSpeciesTableModel(targetSpecies);
        inputContentUI.tableTargetSpecies.setModel(model);
        inputContentUI.tableTargetSpecies.setDefaultRenderer(Equation.class, model);
        inputContentUI.tableTargetSpecies.setDefaultEditor(Equation.class, new EquationTableEditor());
    }

    protected void add() {
        Species selectedSpecies = (Species)inputContentUI.fieldTargetSpecies.getSelectedItem();
        if (selectedSpecies != null) {
            // il n'y en a pas a la creation de la base
            //Formule selectedFormule = (Formule)targetFactor.getFormuleComboBox().getSelectedItem();
            addTargetSpecies(
                    inputContentUI.getBean(),
                    inputContentUI.getMetierSeasonInfo(),
                    selectedSpecies,
                    inputContentUI.targetFactor.getEditor().getText(),
                    inputContentUI.fieldPrimaryCatch.isSelected());
            setTableTargetSpeciesModel();
        }
    }

    protected void remove() {
        // TODO change delete selected truc from model
        Object[] targetSpecies = inputContentUI.getMetierSeasonInfo().getSpeciesTargetSpecies().toArray();

        Object o = targetSpecies[inputContentUI.tableTargetSpecies.getSelectedRow()];
        if (o != null) {
            TargetSpecies ts = (TargetSpecies)o;
            removeTargetSpecies(inputContentUI.getMetierSeasonInfo(), ts);
            inputContentUI.getSaveVerifier().removeCurrentEntity(ts.getTopiaId());
            setTableTargetSpeciesModel();
        }
    }
    
    public void addTargetSpecies(Metier metier, MetierSeasonInfo m,
            Species species, String targetFactorEquationContent,
            boolean primaryCatch) {
        if (log.isDebugEnabled()) {
            log.debug("addTargetSpecies called: " + metier + " " + species
                    + " " + primaryCatch + " " + targetFactorEquationContent);
        }
        try {
            // build targetFactorEquation name
            String targetFactorEquationName = metier.getName() + "-"
                    + species.getName() + "(" + m.getFirstMonth() + "-"
                    + m.getLastMonth() + ")";

            TargetSpeciesDAO dao = IsisFishDAOHelper.getTargetSpeciesDAO(metier
                    .getTopiaContext());
            TargetSpecies targetSpecies = dao.create();

            targetSpecies.setSpecies(species);
            targetSpecies.getTargetFactorEquation().setName(
                    targetFactorEquationName);
            targetSpecies.getTargetFactorEquation().setContent(
                    targetFactorEquationContent);
            targetSpecies.setPrimaryCatch(primaryCatch);
            m.addSpeciesTargetSpecies(targetSpecies);
        } catch (Exception eee) {
            if (log.isErrorEnabled()) {
                log.error("Can't add TargetSpecies", eee);
            }
            ErrorHelper.showErrorDialog(t("isisfish.error.input.addentity",
                    "TargetSpecies"), eee);
        }
    }

    public void removeTargetSpecies(MetierSeasonInfo m,
            TargetSpecies targetSpecies) {
        if (log.isDebugEnabled()) {
            log.debug("removeTargetSpecies called: " + targetSpecies);
        }
        try {
            if (targetSpecies != null) {
                m.removeSpeciesTargetSpecies(targetSpecies);
            }
        } catch (Exception eee) {
            if (log.isErrorEnabled()) {
                log.error("Can't remove TargetSpecies", eee);
            }
            ErrorHelper.showErrorDialog(t("isisfish.error.input.removeentity",
                    "TargetSpecies"), eee);
        }
    }
}
