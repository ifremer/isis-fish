/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.population;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.types.Month;
import fr.ifremer.isisfish.ui.input.InputContentHandler;

/**
 * Population stock handler.
 */
public class PopulationStockHandler extends InputContentHandler<PopulationStockUI> {

    /** Class logger. */
    private static final Log log = LogFactory.getLog(PopulationStockHandler.class);

    protected boolean init;

    protected PopulationStockHandler(PopulationStockUI inputContentUI) {
        super(inputContentUI);
    }

    protected void afterInit() {
        inputContentUI.addPropertyChangeListener(PopulationBasicsUI.PROPERTY_BEAN, evt -> {
            init = true;
            if (evt.getNewValue() == null) {
                inputContentUI.fieldPopulationAbundanceReferenceMonth.setSelectedItem(null);
            }
            if (evt.getNewValue() != null) {
                Population bean = (Population)evt.getNewValue();
                inputContentUI.fieldPopulationAbundanceReferenceMonth.setSelectedItem(bean.getAbundanceReferenceMonth());
            }
            init = false;
        });
    }
    
    public void refresh() {
        
    }
    
    public void onRepresentativeAbundanceMonthChanged() {
        if (!init) {
            Month month = (Month)inputContentUI.fieldPopulationAbundanceReferenceMonth.getSelectedItem();
            inputContentUI.getBean().setAbundanceReferenceMonth(month);
        }
    }
}
