/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.input.equation;

import static org.nuiton.i18n.I18n.t;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import fr.ifremer.isisfish.util.IsisFileUtil;
import fr.ifremer.isisfish.util.UIUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaContext;

import fr.ifremer.isisfish.datastore.FormuleStorage;
import fr.ifremer.isisfish.entities.Equation;
import fr.ifremer.isisfish.entities.Formule;
import fr.ifremer.isisfish.ui.input.InputContentHandler;
import fr.ifremer.isisfish.ui.models.common.GenericComboModel;
import fr.ifremer.isisfish.util.ErrorHelper;
import fr.ifremer.isisfish.ui.widget.text.SyntaxEditor;

/**
 * Equation handler.
 */
public class InputOneEquationHandler extends InputContentHandler<InputOneEquationUI> {

    /** Class logger. */
    private static final Log log = LogFactory.getLog(InputOneEquationHandler.class);

    protected DocumentListener listener = null;

    protected InputOneEquationHandler(InputOneEquationUI inputContentUI) {
        super(inputContentUI);
        // TODO Auto-generated constructor stub
    }

    protected void afterInit() {
        inputContentUI.addPropertyChangeListener(InputOneEquationUI.PROPERTY_BEAN, evt -> {
            if (evt.getNewValue() == null) {
                inputContentUI.formuleComboBox.setModel(new DefaultComboBoxModel<>());
                try {
                    File nullFile = IsisFileUtil.getTempFile("", ".java");
                    inputContentUI.editor.open(nullFile);
                    inputContentUI.editor.setEnabled(false); // editor is replaced at each launch
                } catch (IOException e) {
                    if (log.isErrorEnabled()) {
                        log.error("Error on property change", e);
                    }
                }
            }
            if (evt.getNewValue() != null) {
                // chatellier 20090526 : force refresh in bean change ?
                refresh();
            }
        });

        if (listener == null) {
            listener = new DocumentListener() {
                @Override
                public void insertUpdate(DocumentEvent e) {
                    setEquation();
                }
                @Override
                public void removeUpdate(DocumentEvent e) {
                    setEquation();
                }
                @Override
                public void changedUpdate(DocumentEvent e) {
                }
            };
        }
        setComboModel();
    }
    
    /**
     * Get equation on entity.
     */
    protected Equation getEquation() {
        Equation result = null;
        try {
            // can be null in some uis
            // content is managed by caller, can do anythings here
            if (inputContentUI.bean != null && inputContentUI.beanProperty != null) {
                String localBeanProperty = StringUtils.capitalize(inputContentUI.beanProperty);
                Method m = inputContentUI.bean.getClass().getMethod("get" + localBeanProperty);
                result = (Equation)m.invoke(inputContentUI.bean);
            }
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Can't get equation", e);
            }
        }
        return result;
    }

    /**
     * Call setXXXContent on topia entity (convention).
     */
    protected void setEquation() {
        try {
            // can be null in some uis
            // content is managed by caller, can do anythings here
            if (inputContentUI.bean != null && inputContentUI.beanProperty != null) {
                if (log.isDebugEnabled()) {
                    log.debug("Saving equation content (" + inputContentUI.bean.getClass().getSimpleName() +
                        "#" + inputContentUI.beanProperty + ")");
                }
                String localBeanProperty = StringUtils.capitalize(inputContentUI.beanProperty);
                Method m = inputContentUI.bean.getClass().getMethod("set" + localBeanProperty + "Content", String.class);
                m.invoke(inputContentUI.bean, inputContentUI.editor.getText());
            }
            else {
                if (log.isDebugEnabled()) {
                    log.debug("No bean property defined, skipping content saving");
                }
            }
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Can't set equation content", e);
            }
        }
    }

    public void refresh() {

        // TODO binding don't work
        if (inputContentUI.getText() == null) {
            inputContentUI.setText(t("isisfish.common.equation"));    
        }

        inputContentUI.editor.removeDocumentListener(listener);
        try {
            File equationContentFile = IsisFileUtil.getTempFile("", ".java");
            inputContentUI.editor.open(equationContentFile);
            inputContentUI.editor.setEnabled(inputContentUI.isActive());
        } catch (IOException ex) {
            if (log.isErrorEnabled()) {
                log.error("Can't refresh editor", ex);
            }
        }
        if (inputContentUI.formuleCategory != null) {
            // chatellier 20090526 (change to get combo selected on this equation)
            inputContentUI.selectedEquation = getEquation();
            setComboModel();
            inputContentUI.setFormule((Formule)inputContentUI.formuleComboBox.getSelectedItem());
            setEditorText();
        }

        // utilisee lors de l'edition des equations dans l'interface de sensibilité
        // le contenu de l'equation ne doit pas automatiquement
        // etre sauvé dans l'entité (pas de sens pour un facteur discret de type equation)
        if (inputContentUI.autoSaveModification) {
            inputContentUI.editor.addDocumentListener(listener);
        }
    }

    protected void setComboModel() {
        if (inputContentUI.getBean() !=  null){
            TopiaContext isisContext = inputContentUI.getBean().getTopiaContext();
            List<Formule> formules;
            if (inputContentUI.formuleCategory != null) {
                formules = FormuleStorage.getFormules(isisContext, inputContentUI.formuleCategory);
                GenericComboModel<Formule> formulesModel = new GenericComboModel<>(formules);
                inputContentUI.formuleComboBox.setModel(formulesModel);
                // fix default selection
                inputContentUI.formuleComboBox.setSelectedItem(inputContentUI.selectedEquation);
            }
        }
    }

    protected void saveModel() {
        saveAsModel(inputContentUI.formuleCategory, "java", inputContentUI.editor.getText());
        setComboModel();
        inputContentUI.setInfoText(t("isisfish.message.saveModel.finished"));
    }
    
    /**
     * Save an Equation as model, to reuse it for other equation
     *
     * @param category category for this equation
     * @param language equation to put in models
     * @param content  content ?
     */
    protected void saveAsModel(String category, String language, String content) {
        try {
            String name = JOptionPane.showInputDialog(inputContentUI, t("isisfish.message.saveModel.dialog"),
                    t("isisfish.common.saveModel"), JOptionPane.QUESTION_MESSAGE);
            if (name != null) {
                FormuleStorage storage = FormuleStorage.createFormule(category, name, language);
                storage.setContent(content);
            }

        } catch (Exception eee) {
            if (log.isErrorEnabled()) {
                log.error("Can't save equation as model", eee);
            }
            ErrorHelper.showErrorDialog(t("isisfish.error.equation.savemodel"), eee);
        }
    }

    /**
     * Open external editor, with equation documentation.
     */
    protected void openEditor() {
        Formule e = (Formule)inputContentUI.formuleComboBox.getSelectedItem();
        if (e != null) {
            openEditor(inputContentUI, e.getCategory(),
                    e.getName(), inputContentUI.clazz, e.getContent(), inputContentUI.editor);
        } else {
            openEditor(inputContentUI, inputContentUI.formuleCategory,
                    "new", inputContentUI.clazz, inputContentUI.editor.getText(), inputContentUI.editor);
        }
    }
    
    public void openEditor(InputOneEquationUI ui, String category, String name, Class<?> javaInterface,
            String content, SyntaxEditor editor) {
        if (log.isTraceEnabled()) {
            log.trace("openEditor");
        }
        try {
            EquationEditorPaneUI pane = new EquationEditorPaneUI(ui);
            pane.getHandler().setEquation(category, name, javaInterface, content);
            pane.pack();
            pane.setLocationRelativeTo(ui);
            UIUtil.setIconImage(pane);
            pane.setVisible(true); // blocking call
            if (pane.isResultOk() && editor != null) {
                editor.setText(pane.getEditor().getText());
            }
            pane.dispose();
        } catch (Exception eee) {
            if (log.isErrorEnabled()) {
                log.error("Can't open editor", eee);
            }
            ErrorHelper.showErrorDialog(t("isisfish.error.equation.openeditor"), eee);
        }
    }

    protected void setEditorText() {
        if (inputContentUI.getFormule() != null) {
            inputContentUI.editor.setText(inputContentUI.getFormule().getContent());
        } else {
            try {
                File nullFile = IsisFileUtil.getTempFile("", ".java");
                inputContentUI.editor.open(nullFile);
            } catch (IOException e) {
                if (log.isErrorEnabled()) {
                  log.error("Open editor", e);
                }
            }
        }
    }

    /**
     * New formule selected, replace content.
     */
    protected void formuleChanged() {
        Formule selectedFormule = (Formule)inputContentUI.formuleComboBox.getSelectedItem();
        inputContentUI.setFormule(selectedFormule);
        setEditorText();
    }
}
