/*
 * #%L
 * jTimer
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.script.model;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.TransferHandler;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.ifremer.isisfish.ui.script.ScriptUI;

/**
 * Transfer handler used to transfer tasks in table.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$ By : $Author$
 */
public class ScriptTransferHandler extends TransferHandler {

    /** serialVersionUID. */
    private static final long serialVersionUID = 5433321973795969278L;

    /** log. */
    private static Log log = LogFactory.getLog(ScriptTransferHandler.class);

    protected ScriptUI scriptUI;

    /**
     * Constructor.
     * 
     * @param scriptUI scriptUI
     */
    public ScriptTransferHandler(ScriptUI scriptUI) {
        this.scriptUI = scriptUI;
    }

    @Override
    public boolean canImport(JComponent cp, DataFlavor[] df) {

        for (DataFlavor dataFlavor : df) {
            if (dataFlavor.equals(ScriptTranferable.myData)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean importData(JComponent component, Transferable transferable) {

        boolean confirmImport = false;

        /*if (transferable.isDataFlavorSupported(FactorTranferable.myData)) {
            try {

                FactorTree factorTree = (FactorTree) component;
                FactorGroup selectedFactorGroup = factorTree.getSelectedFactorGroup();

                if (selectedFactorGroup != null) {
                    Object myObject = transferable.getTransferData(FactorTranferable.myData);
                    List<Factor> movedFactors = (List<Factor>) myObject;
                }
            } catch (IOException e) {
                if (log.isErrorEnabled()) {
                    log.error("Exception while transfering task", e);
                }
            } catch (UnsupportedFlavorException e) {
                if (log.isErrorEnabled()) {
                    log.error("Exception while transfering task", e);
                }
            }
        }*/

        confirmImport = true;

        return confirmImport;

    }

    /**
     * Get selected task in tree, and build Transferable object for it.
     * 
     * @param cp component
     * @return transferable instance for selected task
     */
    @Override
    protected Transferable createTransferable(JComponent cp) {

        Transferable transferable = null;

        ScriptTree scriptTree = (ScriptTree) cp;
        // only task can be moved !
        List<File> selectedFiles = scriptTree.getSelectedFiles();
        if (CollectionUtils.isNotEmpty(selectedFiles)) {
            List<File> files = new LinkedList<>(selectedFiles);
            transferable = new ScriptTranferable(files);
        }

        return transferable;
    }

    @Override
    protected void exportDone(JComponent cp, Transferable transferable, int type) {
        if (log.isDebugEnabled()) {
            log.debug("Transfert done");
        }

        if (type == TransferHandler.MOVE) {

            try {
                ScriptTree factorTree = (ScriptTree) cp;
                // elements here, task can be move to directory only
                File directory = factorTree.getSelectedDirectory();

                if (directory != null) {
                    Object myObject = transferable.getTransferData(ScriptTranferable.myData);
                    List<File> movedFiles = (List<File>) myObject;
                    scriptUI.getHandler().moveFiles(directory, movedFiles);
                }
            } catch (IOException | UnsupportedFlavorException e) {
                if (log.isErrorEnabled()) {
                    log.error("Exception while transfering task", e);
                }
            }
        }
    }

    @Override
    public int getSourceActions(JComponent component) {
        return MOVE;
    }
}
