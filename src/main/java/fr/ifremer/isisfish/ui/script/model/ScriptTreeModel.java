/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2011 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.script.model;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.tree.TreeModelSupport;

import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.datastore.ExportStorage;
import fr.ifremer.isisfish.datastore.FormuleStorage;
import fr.ifremer.isisfish.datastore.ObjectiveStorage;
import fr.ifremer.isisfish.datastore.OptimizationStorage;
import fr.ifremer.isisfish.datastore.ResultInfoStorage;
import fr.ifremer.isisfish.datastore.RuleStorage;
import fr.ifremer.isisfish.datastore.ScriptStorage;
import fr.ifremer.isisfish.datastore.SensitivityAnalysisStorage;
import fr.ifremer.isisfish.datastore.SensitivityExportStorage;
import fr.ifremer.isisfish.datastore.SimulationPlanStorage;
import fr.ifremer.isisfish.datastore.SimulatorStorage;
import fr.ifremer.isisfish.vcs.VCS;

/**
 * Tree model for scripts edition.
 * 
 * Tree model is structured as this :
 * <pre>
 * - root
 *   - vcs
 *     - categories
 *       - files
 * </pre>
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class ScriptTreeModel implements TreeModel {

    /** Class logger. */
    private static Log log = LogFactory.getLog(ScriptTreeModel.class);

    protected TreeModelSupport modelSupport;

    /** VCS list. */
    protected List<VCS> vcsList = new ArrayList<>();

    /** Categories files for VCS. */
    protected Map<VCS, List<File>> rootFiles;

    /** Add node operation */
    public static final int OPERATION_ADD = 0;
    /** Modify node operation */
    public static final int OPERATION_MODIFY = 1;
    /** Delete Node operation */
    public static final int OPERATION_DELETE = 2;

    public ScriptTreeModel() {
        modelSupport = new TreeModelSupport(this);

        rootFiles = new HashMap<>();
        List<File> offFiles = new ArrayList<>();
        offFiles.add(ExportStorage.getExportDirectory());
        offFiles.add(FormuleStorage.getFormuleDirectory());
        offFiles.add(ObjectiveStorage.getObjectiveDirectory());
        offFiles.add(OptimizationStorage.getOptimizationDirectory());
        offFiles.add(ResultInfoStorage.getResultInfoDirectory());
        offFiles.add(RuleStorage.getRuleDirectory());
        offFiles.add(ScriptStorage.getScriptDirectory());
        offFiles.add(SensitivityAnalysisStorage.getSensitivityAnalysisDirectory());
        offFiles.add(SensitivityExportStorage.getSensitivityExportDirectory());
        offFiles.add(SimulationPlanStorage.getSimulationPlanDirectory());
        offFiles.add(SimulatorStorage.getSimulatorDirectory());

        List<File> comFiles = new ArrayList<>();
        comFiles.add(ExportStorage.getCommunityExportDirectory());
        comFiles.add(FormuleStorage.getCommunityFormuleDirectory());
        comFiles.add(ObjectiveStorage.getCommunityObjectiveDirectory());
        comFiles.add(OptimizationStorage.getCommunityOptimizationDirectory());
        comFiles.add(ResultInfoStorage.getCommunityResultInfoDirectory());
        comFiles.add(RuleStorage.getCommunityRuleDirectory());
        comFiles.add(ScriptStorage.getCommunityScriptDirectory());
        comFiles.add(SensitivityAnalysisStorage.getCommunitySensitivityAnalysisDirectory());
        comFiles.add(SensitivityExportStorage.getCommunitySensitivityExportDirectory());
        comFiles.add(SimulationPlanStorage.getCommunitySimulationPlanDirectory());
        comFiles.add(SimulatorStorage.getCommunitySimulatorDirectory());

        rootFiles.put(IsisFish.vcs, offFiles);
        rootFiles.put(IsisFish.communityVcs, comFiles);

        vcsList.add(IsisFish.vcs);
        vcsList.add(IsisFish.communityVcs);
    }

    @Override
    public void addTreeModelListener(TreeModelListener l) {
        modelSupport.addTreeModelListener(l);
    }

    @Override
    public Object getChild(Object parent, int index) {

        Object result;

        if (parent == vcsList) {
            result = vcsList.get(index);
        } else if (parent instanceof VCS) {
            result = rootFiles.get(parent).get(index);
        } else {
            File parentFile = (File)parent;
            File[] filesArray = parentFile.listFiles();
            List<File> files = getVersionnableSortedFiles(filesArray);
            result = files.get(index);
        }

        return result;
    }

    @Override
    public int getChildCount(Object parent) {

        int count = 0;
        if (parent == vcsList) {
            count = vcsList.size();
        } else if (parent instanceof VCS) {
            count = rootFiles.get(parent).size();
        } else {
            File parentFile = (File)parent;
            File[] filesArray = parentFile.listFiles();
            if ( filesArray != null) {
                List<File> files = getVersionnableSortedFiles(filesArray);
                count = files.size();
            }
        }
        return count;
    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        int index;
        if (parent == vcsList) {
            index = vcsList.indexOf(child);
        } else if (parent instanceof VCS) {
            index = rootFiles.get(parent).indexOf(child);
        } else {
            File parentFile = (File)parent;
            File[] filesArray = parentFile.listFiles();
            List<File> files = getVersionnableSortedFiles(filesArray);
            index = files.indexOf(child);
        }
        return index;
    }

    @Override
    public Object getRoot() {
        return vcsList;
    }

    @Override
    public boolean isLeaf(Object node) {
        boolean result = false;
        
        if (node instanceof File) {
            result = ((File)node).isFile();
        }
        
        return result;
    }

    @Override
    public void removeTreeModelListener(TreeModelListener l) {
        modelSupport.removeTreeModelListener(l);
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {

    }

    /**
     * Filter input file array and return only sorted collection with only directories.
     * 
     * @param filesArray file array
     * @return sorted list
     */
    protected List<File> getVersionnableSortedFiles(File[] filesArray) {
        List<File> files = new ArrayList<>();
        for (File fileArray : filesArray) {
            if (IsisFish.vcs.isVersionnableFile(fileArray)) {
                files.add(fileArray);
            } else if (IsisFish.communityVcs.isVersionnableFile(fileArray)) {
                files.add(fileArray);
            }
        }
        Collections.sort(files);
        return files;
    }
    
    /**
     * Notify for file addition.
     * 
     * @param file added file
     */
    public void fileAdded(File file) {
        TreePath path = new TreePath(getRoot());
        updateChildren(file, path, OPERATION_ADD);
    }
    
    /**
     * Notify for file modification.
     * 
     * @param file modified file
     */
    public void fileModified(File file) {
        TreePath path = new TreePath(getRoot());
        updateChildren(file, path, OPERATION_MODIFY);
    }
    
    /**
     * Notify for file deletion.
     * 
     * TODO must be called "before" effective deletion
     * 
     * @param file deleted file
     */
    public void fileDeleted(File file) {
        TreePath path = new TreePath(getRoot());
        updateChildren(file, path, OPERATION_DELETE);
    }

    /**
     * Notify for tree operation.
     * 
     * @param file
     * @param path path to update
     * @param operation operation {@link #OPERATION_ADD #OPERATION_DELETE #OPERATION_MODIFY}
     * @return {@code true} if path has been updated
     */
    protected boolean updateChildren(File file, TreePath path,
            int operation) {
        Object pathLastComponent = path.getLastPathComponent();

        // pour ses enfants
        boolean updated = false;
        int childCount = getChildCount(pathLastComponent);
        for (int childIndex = 0; !updated && childIndex < childCount; ++childIndex) {
            Object child = getChild(pathLastComponent, childIndex);

            TreePath childTreePath = path.pathByAddingChild(child);

            if (file.equals(child)) {

                // this update only node, not all path...
                switch (operation) {

                case OPERATION_ADD:
                    modelSupport.fireChildAdded(path, childIndex, child);
                    // expand path
                    //projectsAndTaskTable.expandPath(path);
                    break;

                case OPERATION_DELETE:
                    modelSupport.fireChildRemoved(path, childIndex, child);
                    break;

                case OPERATION_MODIFY:
                    modelSupport.fireChildChanged(path, childIndex, child);
                    break;

                default:
                    if (log.isErrorEnabled()) {
                        log.error("Unknow operation : " + operation);
                    }
                }

                if (log.isTraceEnabled()) {
                    log.trace(" updated : " + childTreePath);
                }
                updated = true;
            } else {
                updated = updateChildren(file, childTreePath, operation);

                if (updated) {
                    // ...and by recursion update all path
                    modelSupport.firePathChanged(path);
                }
            }
        }

        return updated;
    }
    
    /**
     * Find tree path for a file
     * 
     * @param file to search
     * @return tree path
     */
    public TreePath getTreePathFor(File file) {
        TreePath path = new TreePath(vcsList);
        path = getRecursiveTreePath(file, path);
        return path;
    }

    /**
     * Find file in tree, and return tree path.
     * 
     * @param file
     * @param path path to update
     * @return tree path
     */
    protected TreePath getRecursiveTreePath(File file, TreePath path) {
        Object pathLastComponent = path.getLastPathComponent();
        TreePath resultTreePath = null;
        
        int childCount = getChildCount(pathLastComponent);
        for (int childIndex = 0; resultTreePath == null && childIndex < childCount; ++childIndex) {
            Object child = getChild(pathLastComponent, childIndex);

            TreePath childTreePath = path.pathByAddingChild(child);

            if (file.equals(child)) {
                resultTreePath = childTreePath;
            } else {
                resultTreePath = getRecursiveTreePath(file, childTreePath);
            }
        }

        return resultTreePath;
    }
}
