/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2016 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.script;

import static org.nuiton.i18n.I18n.t;

import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Method;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.FileUtil;
import org.nuiton.util.ZipUtil;

import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.datastore.CodeSourceStorage;
import fr.ifremer.isisfish.datastore.CodeSourceStorage.Location;
import fr.ifremer.isisfish.datastore.ExportStorage;
import fr.ifremer.isisfish.datastore.FormuleStorage;
import fr.ifremer.isisfish.datastore.JavaSourceStorage;
import fr.ifremer.isisfish.datastore.ObjectiveStorage;
import fr.ifremer.isisfish.datastore.OptimizationStorage;
import fr.ifremer.isisfish.datastore.ResultInfoStorage;
import fr.ifremer.isisfish.datastore.RuleStorage;
import fr.ifremer.isisfish.datastore.ScriptStorage;
import fr.ifremer.isisfish.datastore.SensitivityAnalysisStorage;
import fr.ifremer.isisfish.datastore.SensitivityExportStorage;
import fr.ifremer.isisfish.datastore.SimulationPlanStorage;
import fr.ifremer.isisfish.datastore.SimulatorStorage;
import fr.ifremer.isisfish.ui.WelcomePanelUI;
import fr.ifremer.isisfish.ui.script.model.ScriptTree;
import fr.ifremer.isisfish.ui.script.model.ScriptTreeModel;
import fr.ifremer.isisfish.util.ErrorHelper;
import fr.ifremer.isisfish.util.CompileHelper;
import fr.ifremer.isisfish.util.JavadocHelper;
import fr.ifremer.isisfish.vcs.VCSException;
import freemarker.cache.ClassTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;

/**
 * ScriptAction.
 * 
 * Template are now loaded with freemarker.
 * 
 * @author letellier
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class ScriptHandler implements TreeSelectionListener {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    private static Log log = LogFactory.getLog(ScriptHandler.class);

    /** Couleur de succes (vert leger). */
    public static final Color COLOR_SUCCESS = new Color(210, 255, 210);

    /** Couleur d'echec (rouge leger). */
    public static final Color COLOR_FAILURE = new Color(255, 210, 210);

    /** Storage for currently edited file. */
    protected CodeSourceStorage codeStorage;

    /** Freemarke configuration used to create new script (based on templates). */
    protected Configuration freemarkerConfiguration;

    /** UI managed by this action class. */
    protected ScriptUI scriptUI;
    
    /**
     * Constructeur.
     * 
     * Init freemarker.
     * 
     * @param scriptUI managed script UI
     */
    public ScriptHandler(ScriptUI scriptUI) {

        this.scriptUI = scriptUI;

        freemarkerConfiguration = new Configuration(Configuration.VERSION_2_3_0);
        // needed to overwrite "Defaults to default system encoding."
        // fix encoding issue on some systems
        freemarkerConfiguration.setDefaultEncoding("utf-8");
        // specific template loader to get template from jars (classpath)
        ClassTemplateLoader templateLoader = new ClassTemplateLoader(ScriptHandler.class, "/");
        freemarkerConfiguration.setTemplateLoader(templateLoader);
    }

    /**
     * Post init, must be called after ui building.
     */
    public void afterInit() {

        ScriptTree scriptTree = this.scriptUI.getScriptTree();

        // add listeners
        scriptTree.addTreeSelectionListener(this);

        // expand official VCS tree node
        scriptTree.expandRow(0);
        
        // bind actions
        scriptUI.getMiSearch().setAction(scriptUI.getEditor().getFindAction());
        scriptUI.getMiReplace().setAction(scriptUI.getEditor().getReplaceAction());
        scriptUI.getMiGoto().setAction(scriptUI.getEditor().getGotoAction());
        scriptUI.getMiSave().setAction(scriptUI.getEditor().getSaveAction());
        scriptUI.getMiCopy().setAction(scriptUI.getEditor().getCopyAction());
        scriptUI.getMiCut().setAction(scriptUI.getEditor().getCutAction());
        scriptUI.getMiPaste().setAction(scriptUI.getEditor().getPasteAction());
        scriptUI.buttonScriptCut.setAction(scriptUI.getEditor().getCutAction());
        scriptUI.buttonScriptCopy.setAction(scriptUI.getEditor().getCopyAction());
        scriptUI.buttonScriptPaste.setAction(scriptUI.getEditor().getPasteAction());
        scriptUI.buttonScriptSave.setAction(scriptUI.getEditor().getSaveAction());
        
        // hide check log
        scriptUI.getActionLogSplitPane().getBottomComponent().setMinimumSize(new Dimension());
        scriptUI.getActionLogSplitPane().setDividerLocation(1.0d);
    }

    /**
     * Update script UI component actions buttons.
     */
    protected void setButton() {
    
        File selectedFile = scriptUI.getScriptTree().getSelectedFile();

        if (selectedFile != null) {
            scriptUI.setSingleFileSelected(selectedFile.isFile());
            boolean isFormule = selectedFile.getAbsolutePath().contains(File.separator + FormuleStorage.FORMULE_PATH + File.separator);
            scriptUI.setJavaFileSelected(selectedFile.getName().endsWith(".java") && !isFormule);
        } else {
            scriptUI.setSingleFileSelected(false);
            scriptUI.setJavaFileSelected(false);
        }
    }
    
    protected void setStatusMessage(String msg) {
        // FIXME remove all parent container reference
        WelcomePanelUI parentUI = scriptUI.getParentContainer(WelcomePanelUI.class);
        parentUI.setStatusMessage(msg);
    }

    protected void setStatusMessage(String msg, boolean running) {
        // FIXME remove all parent container reference
        WelcomePanelUI parentUI = scriptUI.getParentContainer(WelcomePanelUI.class);
        parentUI.setStatusMessage(msg, running);
    }

    /**
     * Make new script, and select it on tree.
     */
    public void newScript(MouseEvent event) {
        JPopupMenu menu = new JPopupMenu();

        // make a basic copy of already existing menu
        JMenu oldMenu = scriptUI.getScriptNewMenu();
        for (int i = 0; i < oldMenu.getItemCount(); i++) {
            JMenuItem oldMenuItem = (JMenuItem)oldMenu.getMenuComponent(i);
            JMenuItem miCopy = new JMenuItem();
            miCopy.setText(oldMenuItem.getText());
            miCopy.setIcon(oldMenuItem.getIcon());
            for (ActionListener l : oldMenuItem.getActionListeners()) {
                miCopy.addActionListener(l);
            }
            menu.add(miCopy);
        }

        menu.show((Component)event.getSource(), event.getX(), event.getY());
    }

    /**
     * Make new script, and select it on tree.
     *
     * @param scriptType script to make
     */
    public void newScript(ScriptMapping scriptType) {

        String equationModelType = "";
        String equationModelTypePath = "";

        // specific case for equation model
        if (scriptType.equals(ScriptMapping.Formule)) {
            List<String> values = FormuleStorage.getCategories();
            equationModelType = (String) JOptionPane.showInputDialog(scriptUI,
                    t("isisfish.message.new.formule.category"),
                    t("isisfish.message.new.formule.title"),
                    JOptionPane.PLAIN_MESSAGE, null, values.toArray(), values.get(0));
            equationModelTypePath = equationModelType + File.separator;
        }

        // if user has not choose "cancel"
        if (equationModelType != null) {
            String fileName = JOptionPane.showInputDialog(t("isisfish.message.new.filename"));
            // user cancel
            if (!StringUtils.isEmpty(fileName)) {
                File scriptFile = newScript(equationModelTypePath + fileName, scriptType);
                // creation successful
                if (scriptFile != null) {
                    ScriptTreeModel model = scriptUI.getScriptTreeModel();
                    model.fileAdded(scriptFile);
                    TreePath treePath = model.getTreePathFor(scriptFile);
                    scriptUI.getScriptTree().setSelectionPath(treePath);
                }
            }
        }
    }

    /**
     * Creer un nouveau script, ici un script peut-etre un Script, un Simulator,
     * un Export.
     *
     * @param fileName full filename
     * @param scriptType le type que l'on souhaite Script, Simulator, ou Export.
     * @return created file or {@code null} if any error happen
     */
    protected File newScript(String fileName, ScriptMapping scriptType) {

        if (log.isDebugEnabled()) {
            log.debug("newScript called [" + scriptType + "]");
        }

        File scriptFile = null;

        try {
            // Vérifie qu'il n'y pas de caractères spéciaux. Seul les caractre
            // de a à z (majuscule ou minuscule) ainsi que les nombres sont
            // autorisés. + pour signifier qu'il doit y avoir
            // au moins 1 caractère.
            String realFilename;
            String category;

            int pos = fileName.lastIndexOf('/');
            if (pos != -1) {
                if (scriptType != ScriptMapping.Formule) {
                    // interdit pour le moment ?
                    String message = t("isisfish.error.invalid.file.name",
                            fileName);
                    Exception e = new RuntimeException(message);
                    returnError(t("isisfish.error.script.create", fileName, e.getMessage()), e);
                    return null;
                }
                // il y a un sous type à traiter
                if (pos == fileName.length() - 1) {
                    String message = t("isisfish.error.invalid.file.name",
                            fileName);
                    Exception e = new RuntimeException(message);
                    returnError(t("isisfish.error.script.create", fileName, e.getMessage()), e);
                    return null;
                }
                realFilename = fileName
                        .substring(fileName.lastIndexOf('/') + 1);
                category = fileName.substring(0, fileName.lastIndexOf('/'));
            } else {
                realFilename = fileName;
                category = "";
            }
            if (!realFilename.matches("[A-Z0-9_][a-zA-Z0-9_]*")) {
                String message = t("isisfish.error.invalid.file.name", fileName);
                Exception e = new RuntimeException(message);
                returnError(t("isisfish.error.script.create", fileName, e.getMessage()), e);
                return null;
            }
            //TODO do test on category
            CodeSourceStorage script = null;
            switch (scriptType) {
            case CommunitySimulationPlan:
                script = SimulationPlanStorage.createSimulationPlan(fileName, Location.COMMUNITY);
                break;
            case CommunityExport:
                script = ExportStorage.createExport(fileName, Location.COMMUNITY);
                break;
            case CommunityObjective:
                script = ObjectiveStorage.createObjective(fileName, Location.COMMUNITY);
                break;
            case CommunityResultInfo:
                script = ResultInfoStorage.createResultInfo(fileName, Location.COMMUNITY);
                break;
            case CommunityOptimization:
                script = OptimizationStorage.createOptimization(fileName, Location.COMMUNITY);
                break;
            case CommunityRule:
                script = RuleStorage.createRule(fileName, Location.COMMUNITY);
                break;
            case CommunityScript:
                script = ScriptStorage.createScript(fileName, Location.COMMUNITY);
                break;
            case CommunitySimulator:
                script = SimulatorStorage.createSimulator(fileName, Location.COMMUNITY);
                break;
            case CommunitySensitivity:
                script = SensitivityAnalysisStorage.createSensitivityAnalysis(fileName, Location.COMMUNITY);
                break;
            case CommunitySensitivityExport:
                script = SensitivityExportStorage
                        .createSensitivityExport(fileName, Location.COMMUNITY);
                break;
            default:
                if (log.isErrorEnabled()) {
                    log.fatal("ScriptType unknown: " + scriptType);
                }
            }

            if (script != null && script.exists()) {
                // Message d'erreur si le fichier existe en local.
                String message = t("isisfish.error.file.already.exists", fileName);
                Exception e = new RuntimeException(message);
                returnError(t("isisfish.error.script.create", fileName, e
                        .getMessage()), e);
            }

            String scriptTemplatePath = scriptType.getTemplatePath();

            if (scriptTemplatePath != null) {

                if (log.isDebugEnabled()) {
                    log.debug("Parsing freemarker template located in "
                            + scriptTemplatePath);
                }

                // get template
                Template template = freemarkerConfiguration
                        .getTemplate(scriptTemplatePath);

                // context values
                Map<String, Object> root = new HashMap<>();
                root.put("name", realFilename);
                root.put("date", new Date());
                root.put("author", IsisFish.config.getUserName());
                root.put("email", IsisFish.config.getUserMail());

                // process template
                Writer out = new StringWriter();
                template.process(root, out);
                out.flush();
                script.setContent(out.toString());
            } else {
                throw new IsisFishRuntimeException("There is no templatePath");
            }

            codeStorage = script;
            scriptFile = script.getFile();
        } catch (Exception eee) {
            returnError(t("isisfish.error.script.create", fileName, eee
                    .getMessage()), eee);
        }

        return scriptFile;
    }

    /**
     * Write error in log and display exception to user.
     * 
     * @param s message
     * @param eee cause
     */
    protected void returnError(String s, Exception eee) {
        if (log.isErrorEnabled()) {
            log.error(s, eee);
        }
        ErrorHelper.showErrorDialog(s, eee);
    }

    @Override
    public void valueChanged(TreeSelectionEvent e) {
        if (e.getNewLeadSelectionPath() != null) {
            
            Object selectedNode = e.getNewLeadSelectionPath().getLastPathComponent();
            if (selectedNode instanceof File && ((File)selectedNode).isFile()) {
                File selectedFile = (File)selectedNode;
                // load file into current action codeStorage
                loadScript(selectedFile);
                scriptUI.getEditor().open(selectedFile);
                setButton();
            } else {
                scriptUI.getEditor().close();
                setButton();
            }
        }
    }
    
    /**
     * Load specified script in current action.
     * 
     * TODO can we change this ?
     * 
     * @param file file to load
     */
    public void loadScript(File file) {
        ScriptMapping mapping = ScriptMapping.getMappingFor(file);
        CodeSourceStorage script = null;

        try {
            switch (mapping) {
            case Formule:
            case CommunityFormule:
                String fullPath = file.getAbsolutePath();
                int lastIndexOf = fullPath.lastIndexOf('/');
                // in string .../aaa/bbb/ccc/ddd.java
                // get ccc
                String category = fullPath.substring(fullPath.lastIndexOf('/',
                        lastIndexOf - 1) + 1, lastIndexOf);
                if (mapping.equals(ScriptMapping.Formule)) {
                    script = FormuleStorage.getFormule(category, file.getName());
                } else {
                    script = FormuleStorage.getCommunityFormule(category, file.getName());
                }
                break;
            case Rule:
                script = RuleStorage.getRule(file.getName(), Location.OFFICIAL);
                break;
            case ResultInfo:
                script = ResultInfoStorage.getResultInfo(file.getName(), Location.OFFICIAL);
                break;
            case Objective:
                script = ObjectiveStorage.getObjective(file.getName(), Location.OFFICIAL);
                break;
            case Optimization:
                script = OptimizationStorage.getOptimization(file.getName(), Location.OFFICIAL);
                break;
            case SimulationPlan:
                script = SimulationPlanStorage.getSimulationPlan(file.getName(), Location.OFFICIAL);
                break;
            case Export:
                script = ExportStorage.getExport(file.getName(), Location.OFFICIAL);
                break;
            case Script:
                script = ScriptStorage.getScript(file.getName(), Location.OFFICIAL);
                break;
            case Simulator:
                script = SimulatorStorage.getSimulator(file.getName(), Location.OFFICIAL);
                break;
            case Sensitivity:
                script = SensitivityAnalysisStorage.getSensitivityAnalysis(file.getName(), Location.OFFICIAL);
                break;
            case SensitivityExport:
                script = SensitivityExportStorage.getSensitivityExport(file.getName(), Location.OFFICIAL);
                break;
            case CommunityObjective:
                script = ObjectiveStorage.getObjective(file.getName(), Location.COMMUNITY);
                break;
            case CommunityOptimization:
                script = OptimizationStorage.getOptimization(file.getName(), Location.COMMUNITY);
                break;
            case CommunityRule:
                script = RuleStorage.getRule(file.getName(), Location.COMMUNITY);
                break;
            case CommunityResultInfo:
                script = ResultInfoStorage.getResultInfo(file.getName(), Location.COMMUNITY);
                break;
            case CommunitySimulationPlan:
                script = SimulationPlanStorage.getSimulationPlan(file.getName(), Location.COMMUNITY);
                break;
            case CommunityExport:
                script = ExportStorage.getExport(file.getName(), Location.COMMUNITY);
                break;
            case CommunityScript:
                script = ScriptStorage.getScript(file.getName(), Location.COMMUNITY);
                break;
            case CommunitySimulator:
                script = SimulatorStorage.getSimulator(file.getName(), Location.COMMUNITY);
                break;
            case CommunitySensitivity:
                script = SensitivityAnalysisStorage.getSensitivityAnalysis(file.getName(), Location.COMMUNITY);
                break;
            case CommunitySensitivityExport:
                script = SensitivityExportStorage.getSensitivityExport(file.getName(), Location.COMMUNITY);
                break;
            default:
                log.fatal("ScriptType unknown: " + file.getName());
            }

            //frame.setInfoText(t("isisfish.message.load.finished"));
        } catch (Exception eee) {
            returnError(t("isisfish.error.script.load", file.getAbsolutePath(),
                    eee.getMessage()), eee);

        } finally {
            codeStorage = script;
        }
    }

    public boolean fileLoaded() {
        return codeStorage != null;
    }

    public boolean isJavaScript() {
        return codeStorage instanceof JavaSourceStorage;
    }

    /**
     * Save current editor test in current loaded codeStorage.
     */
    public void saveScript() {
        if (log.isDebugEnabled()) {
            log.debug("saveScript called on " + codeStorage.getName());
        }

        try {
            // setContent() or scriptUI.getEditor().save()
            // if setContent() only editor ask for t saving
            scriptUI.getEditor().save();
            String content = scriptUI.getEditor().getText();
            codeStorage.setContent(content, false);

            // notify tree to refresh
            ScriptTreeModel model = scriptUI.getScriptTreeModel();
            model.fileModified(codeStorage.getFile());
        } catch (Exception eee) {
            returnError(t("isisfish.error.script.save", codeStorage.getFile(),
                    eee.getMessage()), eee);
        }
        setStatusMessage(t("isisfish.message.save.finished"));
    }

    /**
     * Save script, and display commit UI.
     */
    public void commitScript() {

        if (log.isDebugEnabled()) {
            log.debug("commitScript called for " + codeStorage.getName());
        }

        try {
            // save script before commit
            saveScript();

            String msg = JOptionPane.showInputDialog(t("isisfish.message.script.commit", codeStorage.getName()));
            if (msg == null) {
                setStatusMessage(t("isisfish.message.commit.cancelled"));
            } else {
                codeStorage.commit(msg);
                codeStorage.reload();
                setStatusMessage(t("isisfish.message.commit.finished"));
            }
        } catch (Exception ex) {
            if (log.isErrorEnabled()) {
                log.error("Error on script commit", ex);
            }
            
            // if vcs can't write
            ErrorHelper.showErrorDialog(ex.getMessage(), ex);
        }
    }

    /**
     * Exporte le(s) script(s) sélectionnés dans l'arbre.
     * <br>L'arbre doit avoir au moins un script de selectionnés
     */
    public void exportScript() {
        
        TreePath[] selectedFilesPath = scriptUI.getScriptTree().getSelectionPaths();

        // first step : acquire list of files required 
        int prefixLength = IsisFish.config.getDatabaseDirectory()
                .getAbsolutePath().length() + 1;
        List<String> listFiles = extractFiles(prefixLength, selectedFilesPath);
        
    }

    protected static List<String> extractFiles(int prefixLength,
            TreePath[] selectedPaths) {
        List<String> result = new ArrayList<>();
        List<File> dirFound = new ArrayList<>();
        List<File> dirWithFileFound = new ArrayList<>();

        for (TreePath selectedPath : selectedPaths) {

            DefaultMutableTreeNode node = (DefaultMutableTreeNode) selectedPath
                    .getPathComponent(1);
            String moduleDisplayName = String.valueOf(node.getUserObject());
            File file = ScriptMapping.valueOf(moduleDisplayName).getModule();
            int nbPaths = selectedPath.getPathCount();
            if (nbPaths > 2)
                for (int i = 2; i < nbPaths; i++) {
                    node = (DefaultMutableTreeNode) selectedPath
                            .getPathComponent(i);
                    String pathName = String.valueOf(node.getUserObject());
                    file = new File(file, pathName);
                }
            if (file.isFile()) {
                File parentFile = file.getParentFile();
                if (!dirFound.contains(parentFile)) {
                    dirFound.add(parentFile);
                }
                dirWithFileFound.add(parentFile);
                result.add(file.getAbsolutePath().substring(prefixLength));
            } else {
                // mark the file
                dirFound.add(file);
            }
        }

        // keep only user selected directories
        dirFound.removeAll(dirWithFileFound);
        dirWithFileFound.clear();

        if (!dirFound.isEmpty()) {
            List<File> listF = new ArrayList<>();
            // there is some directories selected by user
            for (File dir : dirFound) {
                FileFilter filter = new FileFilter() {
                    FileFilter excludeFilter = getScriptFileFilter();

                    public boolean accept(File pathname) {
                        return !excludeFilter.accept(pathname);
                    }
                };
                listF.addAll(FileUtil.getFilteredElements(dir, filter, true));
            }
            for (File file : listF)
                result.add(file.getAbsolutePath().substring(prefixLength));
            listF.clear();
        }
        dirFound.clear();
        return result;
    }

    protected static FileFilter scriptFileFilter;

    public static FileFilter getScriptFileFilter() {
        if (scriptFileFilter == null) {
            scriptFileFilter = new ScriptFileFilter(IsisFish.vcs);
        }
        return scriptFileFilter;
    }

    /**
     * Check script content.
     * 
     * @return compilation success flag
     */
    public boolean checkScript() {

        boolean check = false;

        resetCheckLogArea();

        // can't compile formule
        if (codeStorage instanceof FormuleStorage) {
            return false;
        }

        if (log.isDebugEnabled()) {
            log.debug("checkScript called");
        }
        // save script before compile
        saveScript();

        JavaSourceStorage javaCode = (JavaSourceStorage) codeStorage;
        StringWriter result = new StringWriter();
        PrintWriter out = new PrintWriter(result);
        try {
            int compileResult = javaCode.compile(false, out);

            if (compileResult == 0) {
                check = true;
            }
        } catch (Exception eee) {
            eee.printStackTrace(out);
        }
        out.flush();

        if (check) {
            scriptUI.getActionLogArea().setText(
                    t("isisfish.script.compilation.ok", result.toString()));
            scriptUI.getActionLogArea().setBackground(COLOR_SUCCESS);
        } else {
            scriptUI.getActionLogArea().setText(
                    t("isisfish.script.compilation.failed", result
                            .toString()));
            scriptUI.getActionLogArea().setBackground(COLOR_FAILURE);
        }

        setStatusMessage(t("isisfish.message.check.finished"));
        return check;
    }
    
    /**
     * Check all scripts.
     */
    public void checkAllScripts() {
        new SwingWorker<Void, Void>() {
            public Void doInBackground() {
                checkAllScriptsAsync();
                return null;
            }
        }.execute();
    }

    protected void resetCheckLogArea() {
        // hidden by default
        int maxLocation = scriptUI.getActionLogSplitPane().getMaximumDividerLocation();
        if (scriptUI.getActionLogSplitPane().getBottomComponent().getHeight() == 0) {
            scriptUI.getActionLogSplitPane().setDividerLocation((int)(maxLocation * 0.75));
        }
        scriptUI.getActionLogArea().setBackground(null);
        scriptUI.getActionLogArea().setText("");
    }

    protected void checkAllScriptsAsync() {

        setStatusMessage(t("isisfish.message.check.inprogress"), true);

        boolean allSuccess = true;

        // reset previous state
        resetCheckLogArea();
        
        for (ScriptMapping scriptMapping : ScriptMapping.values()) {
            List<File> javaFiles = FileUtil.find(scriptMapping.getModule(), ".+\\.java$", false);

            //enleve les formules
            //les fichiers java ne sont pas compilables sans la formule associée
            javaFiles.removeIf(file -> file.getAbsolutePath().contains(File.separator + FormuleStorage.FORMULE_PATH + File.separator));

            for (File javaFile : javaFiles) {
                scriptUI.getActionLogArea().append(t("isisfish.script.compilingfile", javaFile));

                StringWriter result = new StringWriter();
                PrintWriter out = new PrintWriter(result);
                int compileResult = CompileHelper.compile(IsisFish.config.getDatabaseDirectory(), javaFile, IsisFish.config.getCompileDirectory(), out);
                out.flush();

                if (compileResult == 0) {
                    scriptUI.getActionLogArea().append(t("isisfish.common.ok") + "\n");
                }
                else {
                    scriptUI.getActionLogArea().append(t("isisfish.common.error") + ":\n");
                    scriptUI.getActionLogArea().append(result.toString() + "\n");
                    scriptUI.getActionLogArea().setBackground(COLOR_FAILURE);
                    allSuccess = false;
                }

                // scroll down
                scriptUI.getActionLogArea().setCaretPosition(scriptUI.getActionLogArea().getText().length());
            }
        }

        // set final color
        if (allSuccess) {
            scriptUI.getActionLogArea().setBackground(COLOR_SUCCESS);
        }

        setStatusMessage(t("isisfish.message.check.finished"));
    }

    /**
     * Call main method in current cod storage code.
     * Check script before call.
     */
    public void evaluateScript() {

        if (log.isDebugEnabled()) {
            log.debug("evaluateScript called");
        }

        try {
            if (checkScript()) {
                // reset area color
                scriptUI.getActionLogArea().setBackground(null);

                JavaSourceStorage javaCode = (JavaSourceStorage) codeStorage;
                ByteArrayOutputStream result = new ByteArrayOutputStream();
                PrintStream out = new PrintStream(result);
                PrintStream err = new PrintStream(result);
                PrintStream oldOut = System.out;
                PrintStream oldErr = System.err;
                System.setOut(out);
                System.setErr(err);
                Class<?> clazz = javaCode.getCodeClass();
                Method main = clazz.getMethod("main", String[].class);
                //noinspection RedundantArrayCreation
                main.invoke(null, new Object[] { new String[] {} });
                System.setOut(oldOut);
                System.setErr(oldErr);
                scriptUI.getActionLogArea().setText(result.toString());
            }
        } catch (Exception ex) {
            if (log.isDebugEnabled()) {
                log.debug("Error on script evaluation", ex);
            }
            ByteArrayOutputStream result = new ByteArrayOutputStream();
            PrintStream out = new PrintStream(result);
            ex.printStackTrace(out);
            scriptUI.getActionLogArea().setText(result.toString());
        }
        setStatusMessage(t("isisfish.message.evaluation.finished"));
    }

    /**
     * Copy a single file to destination directory.
     * 
     * @param src file to copy
     * @param path path of file to copy
     * @param suffix file suffix
     * @throws IOException if copy fail
     */
    protected void backup(File src, String path, String suffix)
            throws IOException {

    }

    /**
     * Scan archive for script, and return an array of new files and conflict files.
     * 
     * @param source archive file
     * @param root database directory
     * @return an array [newFiles, conflictFiles]
     */
    protected static List<String>[] scanZip(File source, File root) {

        List<String> overwrittenFiles = new ArrayList<>();
        List<String> newFiles = new ArrayList<>();

        // ontain list of relative paths (to add or overwrite)
        try {
            ZipUtil.scan(source, root, newFiles, overwrittenFiles,
                    getScriptFileFilter(), null, null);
        } catch (IOException e) {
            log.error("Can't scan zip (" + source + ")", e);
            throw new RuntimeException(e);
        }

        return new List[] { newFiles, overwrittenFiles };
    }

    /**
     * Delete a script
     * 
     * @param deleteRemote {@code true} to remove in vcs too
     */
    public void deleteScript(boolean deleteRemote) {

        if (log.isDebugEnabled()) {
            log.debug("DeleteScript called");
        }

        String name = codeStorage.getName();
        int resp = JOptionPane.showConfirmDialog(scriptUI, t("isisfish.message.confirm.remove.script", name), 
                null, JOptionPane.YES_NO_OPTION);
        if (resp == JOptionPane.YES_OPTION) {
            // stay in UI even if deleted
            scriptUI.getEditor().close();

            try {
                // TODO change this, need to be called before
                // effective deletion
                scriptUI.getScriptTreeModel().fileDeleted(codeStorage.getFile());

                //TODO desactive editor
                //TODO Review this because after delete fi file saw previously
                //TODO modified, it ask if we want to save, and then we have
                //TODO again the file in panel but not in tree panel ?
                codeStorage.delete(deleteRemote);
                if (codeStorage.getFile().exists()) {
                    ErrorHelper.showErrorDialog(t("isisfish.error.script.delete", codeStorage
                                    .getFile()));
                }
            } catch (Exception eee) {
                returnError(t("isisfish.error.script.delete",
                        codeStorage == null ? null : codeStorage.getFile(), eee
                                .getMessage()), eee);
            }
            setStatusMessage(t("isisfish.message.delete.finished"));
        } else {
            setStatusMessage(t("isisfish.message.delete.canceled"));
        }
    }

    /**
     * Show diff between selected files and files server version.
     */
    public void diffScript() {

        if (log.isDebugEnabled()) {
            log.debug("Method diffScript called on " + codeStorage.getFile());
        }

        try {
            String result;
            if (IsisFish.vcs.isOnRemote(codeStorage.getFile())) {
                result = IsisFish.vcs.getDiff(codeStorage.getFile());
            } else {
                result = "File not on remote";
            }
            scriptUI.getActionLogArea().setText(result);
        } catch (VCSException e) {
            if (log.isErrorEnabled()) {
                log.error("Can't get diff", e);
            }
            ErrorHelper.showErrorDialog(t("isisfish.vcs.vcssvn.diff.error"), e);
        }
    }

    /** enum to encapsulate a script module */
    protected enum ScriptMapping {

        Export(
                ExportStorage.getExportDirectory(),
                ExportStorage.EXPORT_TEMPLATE, true),
        Rule(
                RuleStorage.getRuleDirectory(),
                RuleStorage.RULE_TEMPLATE, true),
        ResultInfo(
                ResultInfoStorage.getResultInfoDirectory(),
                ResultInfoStorage.RESULT_INFO_TEMPLATE, true),
        Objective(
                ObjectiveStorage.getObjectiveDirectory(),
                ObjectiveStorage.OBJECTIVE_TEMPLATE, true),
        Optimization(
                OptimizationStorage.getOptimizationDirectory(),
                OptimizationStorage.OPTIMIZATION_TEMPLATE, true),
        Script(
                ScriptStorage.getScriptDirectory(),
                ScriptStorage.SCRIPT_TEMPLATE, true),
        Sensitivity(
                SensitivityAnalysisStorage.getSensitivityAnalysisDirectory(), 
                SensitivityAnalysisStorage.SENSITIVITY_ANALYSIS_TEMPLATE, true),
        SensitivityExport(
                SensitivityExportStorage.getSensitivityExportDirectory(),
                SensitivityExportStorage.SENSITIVITY_EXPORT_TEMPLATE, true),
        SimulationPlan(
                SimulationPlanStorage.getSimulationPlanDirectory(),
                SimulationPlanStorage.SIMULATION_PLAN_TEMPLATE, true),
        Simulator(
                SimulatorStorage.getSimulatorDirectory(),
                SimulatorStorage.SIMULATOR_TEMPLATE, true),
        Formule(
                FormuleStorage.getFormuleDirectory(),
                FormuleStorage.FORMULE_TEMPLATE, true),

        CommunityExport(
                ExportStorage.getCommunityExportDirectory(),
                ExportStorage.EXPORT_TEMPLATE),
        CommunityObjective(
                ObjectiveStorage.getCommunityObjectiveDirectory(),
                ObjectiveStorage.OBJECTIVE_TEMPLATE),
        CommunityOptimization(
                OptimizationStorage.getCommunityOptimizationDirectory(),
                OptimizationStorage.OPTIMIZATION_TEMPLATE),
        CommunityRule(
                RuleStorage.getCommunityRuleDirectory(), 
                RuleStorage.RULE_TEMPLATE),
        CommunityResultInfo(
                ResultInfoStorage.getCommunityResultInfoDirectory(),
                ResultInfoStorage.RESULT_INFO_TEMPLATE),
        CommunityScript(
                ScriptStorage.getCommunityScriptDirectory(), 
                ScriptStorage.SCRIPT_TEMPLATE),
        CommunitySensitivity(
                SensitivityAnalysisStorage.getCommunitySensitivityAnalysisDirectory(), 
                SensitivityAnalysisStorage.SENSITIVITY_ANALYSIS_TEMPLATE),
        CommunitySensitivityExport(
                SensitivityExportStorage.getCommunitySensitivityExportDirectory(), 
                SensitivityExportStorage.SENSITIVITY_EXPORT_TEMPLATE),
        CommunitySimulationPlan(
                SimulationPlanStorage.getCommunitySimulationPlanDirectory(), 
                SimulationPlanStorage.SIMULATION_PLAN_TEMPLATE),
        CommunitySimulator(
                SimulatorStorage.getCommunitySimulatorDirectory(), 
                SimulatorStorage.SIMULATOR_TEMPLATE),
        CommunityFormule(
                FormuleStorage.getCommunityFormuleDirectory(), 
                FormuleStorage.FORMULE_TEMPLATE);

        protected File module;
        protected String templatePath;
        protected boolean officialVCS;

        ScriptMapping(File module, String templatePath) {
            this(module, templatePath, false);
        }
        
        ScriptMapping(File module, String templatePath, boolean officialVCS) {
            this.module = module;
            this.templatePath = templatePath;
            this.officialVCS = officialVCS;
        }

        /**
         * Get script type for script path.
         * 
         * @param file file to get type
         * @return ScriptMapping type
         */
        public static ScriptMapping getMappingFor(File file) {

            ScriptMapping result = null;

            // test if path starts with type begin path
            
            // don't forget last / for distinction begin
            // "sensitivity" and "sensitivityexport"
            for (ScriptMapping mapping : ScriptMapping.values()) {
                if (file.getAbsolutePath().startsWith(mapping.getModule().getAbsolutePath() + File.separator)) {
                    result = mapping;
                }
            }

            return result;
        }

        public File getModule() {
            return module;
        }

        public String getTemplatePath() {
            return templatePath;
        }
        
        public boolean isOfficialVCS() {
            return officialVCS;
        }
    }

    /**
     * Generate javadoc and display output in UI.
     */
    public void generateScriptJavadoc() {
        setStatusMessage(t("isisfish.script.menu.javadocgenerating",
                IsisFish.config.getJavadocDirectory()), true);

        SwingUtilities.invokeLater(() -> {
            File rootDatabase = IsisFish.config.getDatabaseDirectory();
            File javadocDirectory = IsisFish.config.getJavadocDirectory();

            StringWriter output = new StringWriter();
            try (PrintWriter out = new PrintWriter(output)) {
                int ok = JavadocHelper.generateJavadoc(rootDatabase, javadocDirectory, out);

                if (ok == 0) {
                    scriptUI.getActionLogArea().setText(t("isisfish.script.javadoc.ok", output.toString()));
                    // vert leger
                    scriptUI.getActionLogArea().setBackground(COLOR_SUCCESS);
                } else {
                    scriptUI.getActionLogArea().setText(t("isisfish.script.compilation.failed", output.toString()));
                    // rouge leger
                    scriptUI.getActionLogArea().setBackground(COLOR_FAILURE);
                }
            }

            setStatusMessage(t("isisfish.script.menu.javadocgenerated", IsisFish.config.getJavadocDirectory()));
        });
    }

    /**
     * Open a browser displaying javadoc.
     */
    public void showScriptJavadoc() {
        try {
            // in faut ouvrir l'index, sinon, ca ouvre
            // un explorateur de fichier
            File indexFile = new File(IsisFish.config.getJavadocDirectory(),
                    "index.html");

            URI uri = indexFile.toURI();
            Desktop.getDesktop().browse(uri);
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Can't show script javadocs", e);
            }
        }
    }

    /**
     * Move files.
     * (called by drag and drop handler).
     * 
     * @param directory directory to moves files to
     * @param filesToMove files to move to directory
     */
    public void moveFiles(File directory, Collection<File> filesToMove) {
        
        ScriptTreeModel model = scriptUI.getScriptTreeModel();
        for (File fileToMove : filesToMove) {
            model.fileDeleted(fileToMove);
            File destFile = new File(directory, fileToMove.getName());
            try {
                FileUtils.moveFile(fileToMove, destFile);
            } catch (IOException ex) {
                throw new IsisFishRuntimeException("Can't move file", ex);
            }
            model.fileAdded(destFile);
        }
    }
}
