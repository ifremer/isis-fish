/*
 * #%L
 * Isis-Fish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.script.model;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Represent transfered data.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$ By : $Author$
 */
public class ScriptTranferable implements Transferable {

    /** log */
    private static Log log = LogFactory.getLog(ScriptTranferable.class);

    /** Data transfer mime type */
    protected static final String MIMETYPE = DataFlavor.javaJVMLocalObjectMimeType
            + ";class=" + File.class.getName();

    /** Data instance */
    protected static DataFlavor myData;

    /** Files to transfer. Chosen implementation must be serializable. */
    protected List<File> filesToTransfer;

    /**
     * Constructor.
     *
     * @param filesToTransfer files to transfer
     */
    public ScriptTranferable(List<File> filesToTransfer) {

        // save task
        this.filesToTransfer = filesToTransfer;

        // build new DataFlavor
        try {
            myData = new DataFlavor(MIMETYPE);
        } catch (ClassNotFoundException e) {
            if (log.isErrorEnabled()) {
                log.error("Class not found", e);
            }
        }
    }

    @Override
    public Object getTransferData(DataFlavor flavor)
            throws UnsupportedFlavorException, IOException {

        List<File> filesToTransfer;

        if (flavor == null) {
            throw new IOException("flavor is null");
        }

        if (flavor.equals(myData)) {
            filesToTransfer = this.filesToTransfer;
        } else {
            throw new UnsupportedFlavorException(flavor);
        }

        return filesToTransfer;
    }

    @Override
    public DataFlavor[] getTransferDataFlavors() {
        return new DataFlavor[] { myData };
    }

    @Override
    public boolean isDataFlavorSupported(DataFlavor flavor) {
        return flavor.equals(myData);
    }
}
