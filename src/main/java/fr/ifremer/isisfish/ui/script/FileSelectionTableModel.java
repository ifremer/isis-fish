/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2010 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.script;

import static org.nuiton.i18n.I18n.t;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

/**
 * File selection table model displayed in import/export script
 * for files selection (first column is checkbox).
 *
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class FileSelectionTableModel extends AbstractTableModel {

    /** serialVersionUID. */
    private static final long serialVersionUID = 6327223042377741126L;

    /** Column names. */
    public final static String[] COLUMN_NAMES = { t("isisfish.common.ellipsis"),
            t("isisfish.common.module"), t("isisfish.common.file") };

    /** All files displayed in table. */
    protected List<String> availableFiles;
    
    /** Only selected files. */
    protected List<String> selectedFiles;

    /**
     * Constructor.
     * 
     * @param availableFiles available file list
     */
    public FileSelectionTableModel(List<String> availableFiles) {
        this.availableFiles = availableFiles;
        this.selectedFiles = new ArrayList<>(availableFiles);
    }

    @Override
    public int getColumnCount() {
        return COLUMN_NAMES.length;
    }

    @Override
    public String getColumnName(int column) {
        return COLUMN_NAMES[column];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        Class<?> result;

        switch (columnIndex) {
        case 0:
            result = Boolean.class;
            break;
        default:
            result = String.class;
            break;
        }

        return result;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        // seulement les cases a cocher sont editables
        return columnIndex == 0;
    }

    @Override
    public int getRowCount() {
        return availableFiles.size();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        Object result = null;

        String rowFileName = availableFiles.get(rowIndex);
        switch (columnIndex) {
        case 0:
            result = selectedFiles.contains(rowFileName);
            break;
        case 1:
            if (rowFileName.contains(java.io.File.separator)) {
                result = rowFileName.substring(0, rowFileName
                        .lastIndexOf(java.io.File.separator));
            } else {
                result = rowFileName;
            }
            break;
        case 2:
            if (rowFileName.contains(java.io.File.separator)) {
                result = rowFileName.substring(rowFileName
                        .lastIndexOf(java.io.File.separator) + 1);
            } else {
                result = "";
            }
            break;
        }
        return result;
    }

    @Override
    public void setValueAt(Object value, int rowIndex, int columnIndex) {

        String rowFileName = availableFiles.get(rowIndex);
        switch (columnIndex) {
        case 0:
            Boolean booleanValue = (Boolean) value;
            if (booleanValue) {
                selectedFiles.add(rowFileName);
            } else {
                selectedFiles.remove(rowFileName);
            }
            break;
        default:
            throw new RuntimeException("Can't edit that column");
        }
    }

    /**
     * Set all files selected.
     * 
     * @param selection {@code true} to select all files, {@code false} to unselect all files
     */
    public void setAllChecked(boolean selection) {
        if (selection) {
            selectedFiles.addAll(availableFiles);
        } else {
            selectedFiles.clear();
        }
        fireTableDataChanged();
    }
    
    /**
     * Get current files selection.
     * 
     * @return list of selected file path
     */
    public List<String> getSelectedFiles() {
        return selectedFiles;
    }
}
