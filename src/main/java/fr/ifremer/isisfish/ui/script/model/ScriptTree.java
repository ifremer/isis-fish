/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.script.model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTree;
import javax.swing.tree.TreePath;

import fr.ifremer.isisfish.ui.script.ScriptUI;

/**
 * Factor tree.
 * 
 * Add drag and drop support.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class ScriptTree extends JTree {

    /** serialVersionUID. */
    private static final long serialVersionUID = 1999481128072081972L;

    /**
     * Constructor (add drag and drop).
     * 
     * @param scriptUI script ui
     */
    public ScriptTree(ScriptUI scriptUI) {
        // enable drag n drop
        setDragEnabled(true);
        setTransferHandler(new ScriptTransferHandler(scriptUI));
    }

    /**
     * Return selected files (only files not directories).
     * 
     * @return selected files
     */
    public List<File> getSelectedFiles() {

        List<File> selectedFiles = new ArrayList<>();

        TreePath[] selectedPaths = getSelectionModel().getSelectionPaths();
        if (selectedPaths != null) {
            for (TreePath selectedPath : selectedPaths) {
                if (selectedPath != null) {
                    Object[] pathWay = selectedPath.getPath();
                    
                    Object selectedFile = pathWay[pathWay.length - 1];
                    if (selectedFile instanceof File && ((File)selectedFile).isFile()) {
                        selectedFiles.add((File)selectedFile);
                    }
                }
            }
        }
        
        return selectedFiles;
    }

    /**
     * Get selected file.
     * 
     * @return selected file
     */
    public File getSelectedFile() {
        File result = null;
        Object selectedObject = getLastSelectedPathComponent();
        if (selectedObject instanceof File) {
            result = (File)selectedObject;
        }
        
        return result;
    }

    /**
     * Return selected directory.
     * 
     * @return selected directory
     */
    public File getSelectedDirectory() {

        File selectedDirectory = null;

        TreePath[] selectedPaths = getSelectionModel().getSelectionPaths();
        if (selectedPaths != null) {
            for (TreePath selectedPath : selectedPaths) {
                if (selectedPath != null) {
                    Object[] pathWay = selectedPath.getPath();
                    
                    Object selectedFile = pathWay[pathWay.length - 1];
                    if (selectedFile instanceof File && ((File)selectedFile).isDirectory()) {
                        selectedDirectory = (File)selectedFile;
                        break;
                    }
                }
            }
        }
        
        return selectedDirectory;
    }
}
