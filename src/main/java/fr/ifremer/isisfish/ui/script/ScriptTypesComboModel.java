/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2011 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.script;

import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;

import fr.ifremer.isisfish.ui.script.ScriptHandler.ScriptMapping;

/**
 * Model pour la liste des scripts disponibles.
 *
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class ScriptTypesComboModel extends DefaultComboBoxModel<ScriptMapping> {

    /** serialVersionUID. */
    private static final long serialVersionUID = -4070846632975105788L;

    /** Mapping list. */
    protected List<ScriptMapping> mappings;
    
    /**
     * Empty constructor.
     */
    public ScriptTypesComboModel() {
        ScriptMapping[] values = ScriptMapping.values();
        mappings = new ArrayList<>(values.length);
        for (ScriptMapping mapping : values) {
            if (!mapping.isOfficialVCS()) {
                mappings.add(mapping);
            }
        }
        
        // default first selected
        if (!mappings.isEmpty()) {
            setSelectedItem(mappings.get(0));
        }
    }

    @Override
    public ScriptMapping getElementAt(int index) {
        return mappings.get(index);
    }

    @Override
    public int getSize() {
        return mappings.size();
    }
}
