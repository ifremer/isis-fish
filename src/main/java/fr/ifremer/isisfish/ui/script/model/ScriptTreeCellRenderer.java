/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2020 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.script.model;

import static org.nuiton.i18n.I18n.t;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.io.File;

import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.Resource;

import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.vcs.VCS.Status;
import fr.ifremer.isisfish.vcs.VCSException;

/**
 * Renderer for script tree.
 * 
 * Can display VCS local status on files.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class ScriptTreeCellRenderer extends DefaultTreeCellRenderer {

    /** serialVersionUID. */
    private static final long serialVersionUID = 807570061868454777L;

    /** Class logger. */
    private static Log log = LogFactory.getLog(ScriptTreeCellRenderer.class);

    protected static final Icon VCS_ICON_OFF = Resource.getIcon("/icons/fatcow/database_green.png");
    protected static final Icon VCS_ICON_COMM = Resource.getIcon("/icons/fatcow/database_yellow.png");

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value,
            boolean selected, boolean expanded, boolean leaf, int row,
            boolean hasFocus) {

        JLabel c = (JLabel)super.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);

        if (value == IsisFish.vcs) {
            c.setText(t("isisfish.ui.script.officialvcs"));
            c.setIcon(VCS_ICON_OFF);
            c.setFont(c.getFont().deriveFont(Font.BOLD));
        } else if (value == IsisFish.communityVcs) {
            c.setText(t("isisfish.ui.script.communityvcs"));
            c.setIcon(VCS_ICON_COMM);
            c.setFont(c.getFont().deriveFont(Font.BOLD));
        } else if (value instanceof File) {
            File file = (File)value;

            try {
                if (file.isFile()) {
                    if (IsisFish.vcs.getLocalStatus(file) == Status.STATUS_MODIFIED) {
                        if (selected) {
                            c.setForeground(new Color(188, 188, 255));
                        }
                        else {
                            c.setForeground(Color.BLUE);
                        }
                    }
                    else if (IsisFish.vcs.getLocalStatus(file) == Status.STATUS_ADDED) {
                        if (selected) {
                            c.setForeground(Color.GREEN.brighter());
                        }
                        else {
                            c.setForeground(Color.GREEN.darker());
                        }
                    }
                }
            } catch (VCSException e) {
                if (log.isErrorEnabled()) {
                    log.error("Can't get file status", e);
                }
            }

            c.setText(file.getName());
            if (leaf) {
                c.setIcon(leafIcon);
            } else if (expanded) {
                c.setIcon(openIcon);
            } else {
                c.setIcon(closedIcon);
            }
            c.setFont(c.getFont().deriveFont(Font.PLAIN));
        }

        return c;
    }
}
