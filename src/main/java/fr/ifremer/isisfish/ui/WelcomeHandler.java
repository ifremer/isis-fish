/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2020 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui;

import fr.ifremer.isisfish.config.IsisConfig;
import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.ui.config.RConfigUI;
import fr.ifremer.isisfish.ui.config.SSHLauncherConfigUI;
import fr.ifremer.isisfish.ui.input.InputContext;
import fr.ifremer.isisfish.ui.input.InputUI;
import fr.ifremer.isisfish.ui.queue.QueueUI;
import fr.ifremer.isisfish.ui.result.ResultUI;
import fr.ifremer.isisfish.ui.script.ScriptUI;
import fr.ifremer.isisfish.ui.sensitivity.SensitivityContext;
import fr.ifremer.isisfish.ui.sensitivity.SensitivityUI;
import fr.ifremer.isisfish.ui.simulator.SimulUI;
import fr.ifremer.isisfish.ui.simulator.SimulatorContext;
import fr.ifremer.isisfish.util.UIUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JFrame;
import javax.swing.ToolTipManager;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import static org.nuiton.i18n.I18n.t;

/**
 * Welcome related ui handler.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class WelcomeHandler extends CommonHandler {

    /** Class logger. */
    private static Log log = LogFactory.getLog(WelcomeHandler.class);

    protected WelcomeUI welcomeUI;

    protected Map<JFrame, WelcomePanelUI> allFrameOpened;

    // URL alias
    protected static final Map<String, String> URLSALIAS = new HashMap<>();

    static {
        URLSALIAS.put("ISISFISH", IsisFish.config.getIsisFishURL());
        URLSALIAS.put("JAVA_API", IsisFish.config.getJavadocJavaURL());
        URLSALIAS.put("ISIS_API", IsisFish.config.getJavadocIsisURL());
        URLSALIAS.put("MATRIX_API", IsisFish.config.getJavadocMatrixURL());
        URLSALIAS.put("TOPIA_API", IsisFish.config.getJavadocTopiaURL());
    }

    public WelcomeHandler(WelcomeUI welcomeUI) {
        this.welcomeUI = welcomeUI;
    }

    public void afterInit() {
        allFrameOpened = new HashMap<>();
        WelcomePanelUI welcomePanelUI = welcomeUI.getWelcomePanelUI();
        welcomePanelUI.setContent(new WelcomeTabUI(welcomePanelUI));
    
        // increase tooltip display time
        ToolTipManager toolTipManager = ToolTipManager.sharedInstance();
        toolTipManager.setInitialDelay(0);
        toolTipManager.setDismissDelay(60000);
    }

    protected void openFrame(WelcomeUI welcomeUI, Component c, String title) {

        WelcomePanelUI welcome = new WelcomePanelUI(welcomeUI);
        welcome.setContent(c);
        
        JFrame frame = new JFrame(title);
        frame.setLayout(new BorderLayout());
        frame.add(welcome, BorderLayout.CENTER);
        frame.setSize(new Dimension(800, 600));
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

        allFrameOpened.put(frame, welcome);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                JFrame f = (JFrame) e.getSource();
                allFrameOpened.get(f).close(f);
            }
        });
        UIUtil.setIconImage(frame);
        frame.setLocationRelativeTo(welcomeUI);
        frame.setVisible(true);
    }

    public void newSimulationFrame(WelcomeUI welcomeUI) {
        SimulatorContext simulatorContext = new SimulatorContext(welcomeUI);
        SimulUI simulUI = new SimulUI(simulatorContext);
        openFrame(welcomeUI, simulUI, t("isisfish.simulation.title"));
    }

    public void newResultFrame(WelcomeUI welcomeUI) {
        openFrame(welcomeUI, new ResultUI(), t("isisfish.result.title"));
    }

    public void newInputFrame(WelcomeUI welcomeUI) {
        InputContext inputContext = new InputContext(welcomeUI);
        InputUI inputUI = new InputUI(inputContext);
        openFrame(welcomeUI, inputUI, t("isisfish.input.title"));
    }

    public void newScriptFrame(WelcomeUI welcomeUI) {
        openFrame(welcomeUI, new ScriptUI(), t("isisfish.script.title"));
    }

    public void newSensitivityFrame(WelcomeUI welcomeUI) {
        SensitivityContext sensitivityContext = new SensitivityContext(welcomeUI);
        SensitivityUI sensitivityUI = new SensitivityUI(sensitivityContext);
        openFrame(welcomeUI, sensitivityUI, t("isisfish.sensitivity.title"));
    }

    public void newQueueFrame(WelcomeUI welcomeUI) {
        openFrame(welcomeUI, new QueueUI(), t("isisfish.queue.title"));
    }

    public void close(WelcomeUI welcomeUI) {
        WelcomeSaveVerifier verifier = welcomeUI.getContextValue(WelcomeSaveVerifier.class);
        if (verifier.allIsSaved()) {
            welcomeUI.dispose();
        }
    }

    /**
     * Show config frame.
     *
     * @param welcomeUI parent ui
     */
    public void config(WelcomeUI welcomeUI) {
        fr.ifremer.isisfish.ui.config.ConfigUI configUI = new fr.ifremer.isisfish.ui.config.ConfigUI(welcomeUI, welcomeUI, true);
        configUI.pack();
        configUI.setLocationRelativeTo(welcomeUI);
        configUI.setVisible(true);
    }

    /*
     * Display VCS config ui.
     * 
     * @param welcomeUI parent ui
     *
    public void configVCS(WelcomeUI welcomeUI) {
        VCSConfigUI vcsConfig = new VCSConfigUI(welcomeUI, welcomeUI);
        vcsConfig.pack();
        vcsConfig.setLocationRelativeTo(welcomeUI);
        vcsConfig.setVisible(true);
    }*/

    /**
     * Display ssh launcher config ui.
     * 
     * @param welcomeUI parent ui
     */
    public void configurationSSHLauncher(WelcomeUI welcomeUI) {
        SSHLauncherConfigUI configUI = new SSHLauncherConfigUI(welcomeUI, welcomeUI, true);
        configUI.pack();
        configUI.setSize(600, configUI.getHeight());
        configUI.setLocationRelativeTo(welcomeUI);
        configUI.setVisible(true);
    }

    /**
     * Display R config tester UI.
     * 
     * @param welcomeUI parent ui
     */
    public void configurationR(WelcomeUI welcomeUI) {
        RConfigUI rconfigUI = new RConfigUI(welcomeUI, welcomeUI, true);
        rconfigUI.pack();
        rconfigUI.setLocationRelativeTo(welcomeUI);
        rconfigUI.setVisible(true);
    }

    /**
     * Open url using default system browser.
     *
     * @param welcomeUI parent ui
     * @param urlOrAlias url alias, or full url
     */
    public void help(WelcomeUI welcomeUI, String urlOrAlias) {
        try {
            String url = URLSALIAS.get(urlOrAlias);

            if (url == null) {
                url = urlOrAlias;
            }

            Desktop.getDesktop().browse(new URL(url).toURI());
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Can't show help", e);
            }
        }
    }

    /**
     * Show about dialog.
     * 
     * @param welcomeUI parent ui
     */
    public void about(WelcomeUI welcomeUI) {
       AboutDialog about = new AboutDialog(welcomeUI);
       about.setTitle(t("isisfish.about.title"));
       about.setIconPath("images/isislogo.png");
       about.setAboutHtmlText(t("isisfish.about.abouthtmltext", IsisConfig.getVersion()));
       about.setLicenseText(t("isisfish.about.licensetext"));
       about.setBackgroundColor(Color.WHITE);
       about.setSize(640, 520);
       about.setLocationRelativeTo(welcomeUI);
       about.setVisible(true);
    }
}
