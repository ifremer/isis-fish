/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2010 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.queue;

import static org.nuiton.i18n.I18n.t;

import javax.swing.JProgressBar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.ifremer.isisfish.logging.SimulationLoggerUtil;
import fr.ifremer.isisfish.simulator.launcher.SimulationJob;
import fr.ifremer.isisfish.simulator.launcher.SimulationService;
import fr.ifremer.isisfish.util.ErrorHelper;

/**
 * Common action for all queue ui.
 *
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class QueueHandler {

    /** Log. */
    private static Log log = LogFactory.getLog(QueueHandler.class);
    
    /** queue ui to manage. */
    protected QueueUI queueUI;

    /**
     * 
     * @param queueUI queue ui to manage
     */
    public QueueHandler(QueueUI queueUI) {
        this.queueUI = queueUI;
    }

    public void afterInit() {
        queueUI.queueTable.setDefaultRenderer(JProgressBar.class, new ComponentTableCellRenderer());
        queueUI.queueTableDone.setDefaultRenderer(JProgressBar.class, new ComponentTableCellRenderer());
    }

    /**
     * Update ui buttons.
     */
    public void updateActions() {
        queueUI.setCanStop(!queueUI.getSelectionModelQueueTable().isSelectionEmpty());
        queueUI.setCanShowLog(!queueUI.getSelectionModelQueueTableDone().isSelectionEmpty());
        queueUI.setCanClear(queueUI.getQueueTableDone().getModel().getRowCount()>0);
        queueUI.setCanRestart(!queueUI.getSelectionModelQueueTableDone().isSelectionEmpty());
    }
    
    /**
     * Stop simulation associated with table selected rows.
     */
    protected void stopSimulation() {
        int[] selectedRows = queueUI.getQueueTable().getSelectedRows();
        SimulationJob[] jobsToStop = new SimulationJob[selectedRows.length];
        int index = 0;
        
        // to do in two pass, because each stopped simulation
        // change selected rows
        for (int selectedRow : selectedRows) {
            jobsToStop[index++] = queueUI.getNewSimulationModel().getJob(selectedRow);
        }
        for (SimulationJob jobToStop : jobsToStop) {
            jobToStop.stop();
            if (log.isInfoEnabled()) {
                log.info(t("User stop simulation %s", jobToStop.getItem().getControl()
                    .getId()));
            }
        }
    }

    /**
     * Restart simulation.
     */
    protected void restartSimulation() {
        int[] selectedRows = queueUI.getQueueTableDone().getSelectedRows();
        SimulationJob[] jobsToRestart = new SimulationJob[selectedRows.length];
        int index = 0;
        
        // to do in two pass, because each simulation
        // change selected rows
        for (int selectedRow : selectedRows) {
            jobsToRestart[index++] = queueUI.getDoneSimulationModel().getJob(selectedRow);
        }
        for (SimulationJob jobToRestart : jobsToRestart) {
            
            jobToRestart.restart();
            jobToRestart.getItem().getControl().setText(t("isisfish.simulation.restarting"));
            if (log.isInfoEnabled()) {
                log.info(t("User restart simulation %s", jobToRestart.getItem().getControl()
                    .getId()));
            }
        }
    }
    
    /**
     * View log of selected done jobs.
     */
    protected void viewLog() {
        if (queueUI.getQueueTableDone().getSelectedRow() >= 0) {
            SimulationJob selectedJob = queueUI.getDoneSimulationModel().getJob(queueUI.getQueueTableDone().getSelectedRow());
            
            String id = selectedJob.getItem().getControl().getId();
            try {
                SimulationLoggerUtil.showSimulationLogConsole(queueUI, id);
            } catch (Exception eee) {
                if (log.isErrorEnabled()) {
                    log.error(t("Can't open log for %s", id), eee);
                }
                ErrorHelper.showErrorDialog(t("Can't open log for %s", id), eee);
            }
        }
    }

    /**
     * Remove all done jobs.
     */
    protected void clearDoneJobs() {
        SimulationService ss = SimulationService.getService();
        ss.clearJobDone();
    }
}
