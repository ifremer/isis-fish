<!--
  #%L
  IsisFish
  
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2009 - 2015 Ifremer, Code Lutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->
<Table>
    <QueueHandler id="handler" constructorParams="this" />

    <import>
    fr.ifremer.isisfish.simulator.launcher.SimulationService
    fr.ifremer.isisfish.simulator.launcher.SimulationServiceTableModel
    </import>

    <script><![CDATA[
    protected void $afterCompleteSetup() {
        handler.afterInit();
    }
    ]]></script>

    <Boolean id='canStop' javaBean='false'/>
    <Boolean id='canShowLog' javaBean='false'/>
    <Boolean id='canClear' javaBean='false'/>
    <Boolean id='canRestart' javaBean='false'/>
    
    
    <SimulationServiceTableModel id="newSimulationModel" javaBean='new SimulationServiceTableModel(SimulationService.getService(), true)'/>
    <SimulationServiceTableModel id="doneSimulationModel" javaBean='new SimulationServiceTableModel(SimulationService.getService(), false)'
        onTableChanged='handler.updateActions()' />
    <PauseButtonModel id="autoLaunchButtonModel" javaBean='new PauseButtonModel(SimulationService.getService())'/>
    
    <DefaultListSelectionModel id="selectionModelQueueTable" onValueChanged='handler.updateActions()'/>
    <DefaultListSelectionModel id="selectionModelQueueTableDone" onValueChanged='handler.updateActions()'/>

    <row>
        <cell columns="5" fill="both" weightx="1.0" weighty="0.5">
            <JSplitPane oneTouchExpandable="true" orientation="VERTICAL" resizeWeight="0.5">
                <JScrollPane>
                    <JTable id="queueTable" model='{newSimulationModel}'
                        selectionMode="{javax.swing.ListSelectionModel.MULTIPLE_INTERVAL_SELECTION}"
                        selectionModel="{selectionModelQueueTable}"/>
                </JScrollPane>
                <JScrollPane>
                    <JTable id="queueTableDone" model='{doneSimulationModel}'
                        selectionMode="{javax.swing.ListSelectionModel.SINGLE_SELECTION}"
                        selectionModel="{selectionModelQueueTableDone}" />
                </JScrollPane>
            </JSplitPane>
        </cell>
    </row>
    <row>
        <cell fill="horizontal" weightx="0.3">
            <JToggleButton id="autoLaunchButton" text="isisfish.queue.simulationLaunch" model="{autoLaunchButtonModel}"/>
        </cell>
        <cell fill="horizontal" weightx="0.3">
            <JButton id="stopSimuButton" text="isisfish.queue.stopSimulation" onActionPerformed='handler.stopSimulation()' enabled='{isCanStop()}' />
        </cell>
        <cell fill="horizontal" weightx="0.3">
            <JButton id="restartSimulationButton" text="isisfish.queue.restartSimulation" onActionPerformed='handler.restartSimulation()' enabled='{isCanRestart()}' />
        </cell>
        <cell fill="horizontal" weightx="0.3">
            <JButton id="showLogButton" text="isisfish.queue.showLog" onActionPerformed='handler.viewLog()' enabled='{isCanShowLog()}' />
        </cell>
        <cell fill="horizontal" weightx="0.3">
            <JButton id="clearDoneJobsButton" text="isisfish.queue.clearDone" onActionPerformed='handler.clearDoneJobs()' enabled='{isCanClear()}' />
        </cell>
    </row>
</Table>
