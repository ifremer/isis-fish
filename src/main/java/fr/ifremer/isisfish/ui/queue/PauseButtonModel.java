package fr.ifremer.isisfish.ui.queue;

/*
 * #%L
 * IsisFish
 * %%
 * Copyright (C) 2014 - 2015 Ifremer, Codelutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.isisfish.simulator.launcher.SimulationExecutor;
import fr.ifremer.isisfish.simulator.launcher.SimulationService;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JToggleButton.ToggleButtonModel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class PauseButtonModel extends ToggleButtonModel implements PropertyChangeListener, ItemListener {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(PauseButtonModel.class);
    private static final long serialVersionUID = 1L;

    protected SimulationService service;
    final static private String servicePropertyName = "autoLaunch";
    final static private String executorPropertyName = "pause";

    public PauseButtonModel(SimulationService service) {
        this.service = service;

        boolean b = service.isAutoLaunch();
        service.addPropertyChangeListener(servicePropertyName, this);

        for (SimulationExecutor e : service.getSimulationExecutors()) {
            b = b && !e.isPause();
            e.addPropertyChangeListener(executorPropertyName, this);
        }

        setSelected(b);

        addItemListener(this);
    }

    // event from service or executor, change button
    @Override
    public void propertyChange(PropertyChangeEvent event) {
        boolean b = (Boolean)event.getNewValue();
        setSelected(b);
    }

    // event from button, change model
    @Override
    public void itemStateChanged(ItemEvent event) {
        boolean b = event.getStateChange() == ItemEvent.SELECTED;
        service.setAutoLaunch(b);
        // if pause asked, (b == false) only service can be put in autoLaunch = false
        // executor see this autoLaunch value.
        // if resume asked (b == true), we must ensure that all executer are resume too
        if (b) {
            for (SimulationExecutor e : service.getSimulationExecutors()) {
                e.resume();
            }
        }
    }

}
