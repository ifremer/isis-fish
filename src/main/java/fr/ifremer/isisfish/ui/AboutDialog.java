/*
 * #%L
 * IsisFish
 *
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2019 - 2020 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui;

import static org.nuiton.i18n.I18n.t;

import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.Resource;

/**
 * About windows.
 *
 * Can't set :
 *  - top image
 *  - about tab
 *  - license tab (optionnal)
 *
 * @author chatellier
 * @version $Revision: 300 $
 *
 * Last update : $Date: 2010-11-17 22:35:44 +0100 (mer., 17 nov. 2010) $ By : $Author: sletellier $
 */
public class AboutDialog extends JDialog implements ActionListener, HyperlinkListener {

    /** log */
    private static final Log log = LogFactory.getLog(AboutDialog.class);
    /** Top image path (classpath) */
    protected String iconPath;
    /** Background color */
    protected Color backgroundColor;
    /** Html text displayed in about tab */
    protected String aboutHtmlText = "";
    /** License text displayed in license tab */
    protected String licenseText;

    /**
     * Constructor.
     *
     * Build UI.
     */
    public AboutDialog(Frame owner) {
        super(owner, true);

        // fix resizing
        this.setResizable(false);
    }

    /**
     * Set about text.
     *
     * @param aboutHtmlText the aboutHtmlText to set
     */
    public void setAboutHtmlText(String aboutHtmlText) {
        this.aboutHtmlText = aboutHtmlText;
    }

    /**
     * Set licence text.
     *
     * @param licenseText the licenseText to set
     */
    public void setLicenseText(String licenseText) {
        this.licenseText = licenseText;
    }

    /**
     * Set icon path.
     *
     * @param iconPath the iconPath to set
     */
    public void setIconPath(String iconPath) {
        this.iconPath = iconPath;
    }

    /**
     * Set background color.
     *
     * @param backgroundColor the backgroundColor to set
     */
    public void setBackgroundColor(Color backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    /**
     * Build ui.
     */
    protected void buildUI() {

        setLayout(new GridBagLayout());

        // top panel
        Component top = getTopPanel();
        add(top, new GridBagConstraints(0, 0, 1, 1, 1, 0,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(1, 1, 1, 1), 0, 0));

        JTabbedPane tabPanel = new JTabbedPane();

        // first panel
        Component firstTab = getAboutTab();
        tabPanel.add(t("isisfish.aboutdialog.about"), firstTab);

        // second panel
        if (licenseText != null) {
            Component secondTab = getLicenseTab();
            tabPanel.add(t("isisfish.aboutdialog.license"), secondTab);
        }

        add(tabPanel, new GridBagConstraints(0, 1, 1, 1, 1, 1,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 5, 5), 0, 0));

        // ok button
        JButton okButton = new JButton(t("isisfish.aboutdialog.ok"));
        okButton.setActionCommand("ok");
        okButton.addActionListener(this);
        add(okButton, new GridBagConstraints(0, 2, 1, 1, 1, 0,
                GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 5, 5, 5), 20, 0));

        // change back color
        if (backgroundColor != null) {
            getContentPane().setBackground(backgroundColor);
        }
    }

    /**
     * Build the top component
     *
     * @return top component
     */
    protected Component getTopPanel() {
        // image
        JLabel labelIcon;
        if (iconPath != null) {
            Icon logoIcon = Resource.getIcon(iconPath);
            labelIcon = new JLabel(logoIcon);
        } else {
            labelIcon = new JLabel();
        }
        return labelIcon;
    }

    /**
     * Build license tab.
     *
     * @return license tab component
     */
    protected Component getLicenseTab() {

        JTextArea licenseArea = new JTextArea();
        licenseArea.setLineWrap(true);
        licenseArea.setWrapStyleWord(true);
        licenseArea.setText(licenseText);
        licenseArea.setEditable(false);
        return licenseArea;
    }

    /**
     * Build about tab.
     *
     * @return about tab component
     */
    protected Component getAboutTab() {

        JEditorPane htmlAbout = new JEditorPane("text/html", aboutHtmlText);
        htmlAbout.addHyperlinkListener(this);
        htmlAbout.setEditable(false);

        return htmlAbout;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        String command = e.getActionCommand();

        if ("ok".equals(command)) {
            setVisible(false);
        }
    }

    @Override
    public void hyperlinkUpdate(HyperlinkEvent he) {
        if (he.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {

            if (Desktop.isDesktopSupported()) {
                try {
                    URL u = he.getURL();
                    if (u.getProtocol().equalsIgnoreCase("mailto")
                            || u.getProtocol().equalsIgnoreCase("http")
                            || u.getProtocol().equalsIgnoreCase("ftp")) {
                        Desktop.getDesktop().browse(u.toURI());
                    }
                } catch (IOException | URISyntaxException e) {
                    if (log.isErrorEnabled()) {
                        log.error("Error while opening link", e);
                    }
                }
            }
        }
    }

    @Override
    public void setVisible(boolean b) {
        if (b) {
            // build UI
            buildUI();
        }

        super.setVisible(b);
    }
}
