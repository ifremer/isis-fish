/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2012 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.sensitivity;

import java.awt.Cursor;
import java.awt.event.MouseEvent;

import javax.swing.JComponent;
import javax.swing.JScrollBar;
import javax.swing.JTable;
import javax.swing.table.TableModel;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.jxlayer.JXLayer;
import org.jdesktop.jxlayer.plaf.AbstractLayerUI;
import org.jdesktop.jxlayer.plaf.LayerUI;
import org.nuiton.topia.persistence.TopiaEntityContextable;

import fr.ifremer.isisfish.simulator.sensitivity.SensitivityUtils;
import fr.ifremer.isisfish.ui.input.InputContentUI;

/**
 * A layer for factorizable entities properties in {@link JTable} component.
 * 
 * This is a {@link LayerUI} that check if :
 * <ul>
 *  <li>component is {@link JScrollBar} : clic allowed</li>
 *  <li>component is {@link JTable} and table model is {@link SensitivityTableModel} : do sensitivity stuff</li>
 * </ul>
 * 
 * Tree {@link MouseEvent}s are managed:
 * <ul>
 *  <li>{@link MouseEvent#MOUSE_CLICKED} : display factor interface depending on sensitivity enabled table column</li>
 *  <li>{@link MouseEvent#MOUSE_MOVED} : change cursor depending on sensitivity enabled table column</li>
 *  <li>{@link MouseEvent#MOUSE_EXITED} : back to default cursor</li>
 * </ul>
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class TableBlockingLayerUI extends AbstractLayerUI<JComponent> {

    /** serialVersionUID. */
    private static final long serialVersionUID = 1877010363682882308L;

    /** Class logger. */
    private static Log log = LogFactory.getLog(TableBlockingLayerUI.class);

    /** Parent UI. */
    protected InputContentUI<? extends TopiaEntityContextable> parent;

    /**
     * Init layer with parent.
     * 
     * @param parent parent
     */
    public TableBlockingLayerUI(InputContentUI<? extends TopiaEntityContextable> parent) {
        this.parent = parent;
    }

    @Override
    protected void processMouseEvent(MouseEvent event, JXLayer<? extends JComponent> layer) {

        // scrollbar can be moved
        if (!(event.getSource() instanceof JScrollBar)) {
            event.consume();
        }

        if (event.getSource() instanceof JTable) {

            JTable tableSource = (JTable) event.getSource();
            TableModel model = tableSource.getModel();

            if (model instanceof SensitivityTableModel) {
                SensitivityTableModel sensitivityTableModel = (SensitivityTableModel) model;

                // mouse exit table
                if (event.getID() == MouseEvent.MOUSE_EXITED) {
                    Cursor cursor = new Cursor(Cursor.DEFAULT_CURSOR);
                    tableSource.setCursor(cursor);
                } else {
                    int rowIndex = tableSource.rowAtPoint(event.getPoint());
                    int columnIndex = tableSource.columnAtPoint(event.getPoint());

                    if (rowIndex >= 0 && columnIndex >= 0) {

                        // test if sensitivity property is enabled
                        SensitivityTableModel sensitivityModel = (SensitivityTableModel) model;
                        TopiaEntityContextable value = (TopiaEntityContextable) sensitivityTableModel.getBeanAtRow(rowIndex);
                        Class<? extends TopiaEntityContextable> beanClass = value.getClass();
                        String property = sensitivityModel.getPropertyAtColumn(columnIndex);
                        String sensitivityName = beanClass.getSimpleName().replaceFirst("Impl", "") + "." + property;

                        if (SensitivityUtils.isSensitivityFactorEnabled(sensitivityName)) {

                            // clic sur la table
                            if (event.getID() == MouseEvent.MOUSE_CLICKED) {
                                if (log.isDebugEnabled()) {
                                    log.debug("Clic done on an enabled factor : " + sensitivityName);
                                }

                                parent.getHandler().displayFactorWizard(parent, beanClass, value.getTopiaId(), property);
                            }

                            // mouse moved over table
                            else if (event.getID() == MouseEvent.MOUSE_MOVED) {
                                Cursor cursor = new Cursor(Cursor.CROSSHAIR_CURSOR);
                                tableSource.setCursor(cursor);
                            }
                        } else {
                            Cursor cursor = new Cursor(Cursor.DEFAULT_CURSOR);
                            tableSource.setCursor(cursor);
                        }
                    }
                }
            }
        }
    }

    @Override
    protected void processMouseMotionEvent(MouseEvent e, JXLayer<? extends JComponent> l) {
        processMouseEvent(e, l);
    }
}
