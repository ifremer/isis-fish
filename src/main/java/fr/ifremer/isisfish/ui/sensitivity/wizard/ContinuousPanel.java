/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.sensitivity.wizard;

import javax.swing.JPanel;

import fr.ifremer.isisfish.simulator.sensitivity.Factor;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Common code for all continuous panels:
 * <ul>
 * <li>{@link EquationContinuousPanelUI}</li>
 * <li>{@link ContinuousDistributionPanel}</li>
 * </ul>
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public abstract class ContinuousPanel extends JPanel {

    /** serialVersionUID. */
    private static final long serialVersionUID = -7759972555434206618L;

    protected Collection<ContinuousPanelListener> listeners = new ArrayList<>();

    protected boolean continuePossible;

    /**
     * Return true only if UI component are all filled and value are correct
     * to define new factor.
     * 
     * @return {@code true} if factor is valid
     */
    public abstract boolean isFactorValid();

    public boolean isContinuePossible() {
        return continuePossible;
    }

    public void setContinuePossible(boolean continuePossible) {
        boolean oldValue = this.continuePossible;
        this.continuePossible = continuePossible;
        firePropertyChange("continuePossible", oldValue, continuePossible);
    }

    /**
     * Init current panel with existing factor.
     * 
     * @param factor
     */
    public abstract void initWithFactor(Factor factor);

    public void addContinuousPanelListener(ContinuousPanelListener continuousPanelListener) {
        listeners.add(continuousPanelListener);
    }

    protected void fireContinousPanelEvent() {
        for (ContinuousPanelListener listener : listeners) {
            listener.panelChanged();
        }
    }
}
