/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.ui.sensitivity.wizard;

import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;

import fr.ifremer.isisfish.simulator.sensitivity.Distribution;

/**
 * Renderer qui affiche la description de la distribution.
 */
public class DistributionListRenderer extends DefaultListCellRenderer {

    /** serialVersionUID. */
    private static final long serialVersionUID = -8210763862382993163L;

    @Override
    public Component getListCellRendererComponent(JList<?> list, Object value,
            int index, boolean isSelected, boolean cellHasFocus) {

        Distribution distribution = (Distribution)value;
        String stringValue = null;
        if (distribution != null) {
            stringValue = distribution.getDescription();
        }
        return super.getListCellRendererComponent(list, stringValue, index, isSelected, cellHasFocus);
    }
}
