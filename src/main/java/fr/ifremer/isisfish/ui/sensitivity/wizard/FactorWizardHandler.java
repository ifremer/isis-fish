/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.sensitivity.wizard;

import static org.nuiton.i18n.I18n.t;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.text.JTextComponent;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.math.matrix.gui.MatrixPanelEditor;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityContextable;

import fr.ifremer.isisfish.IsisFishDAOHelper;
import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.datastore.RegionStorage;
import fr.ifremer.isisfish.datastore.RuleStorage;
import fr.ifremer.isisfish.entities.Equation;
import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.rule.Rule;
import fr.ifremer.isisfish.simulator.SimulationParameter;
import fr.ifremer.isisfish.simulator.sensitivity.Domain;
import fr.ifremer.isisfish.simulator.sensitivity.Factor;
import fr.ifremer.isisfish.simulator.sensitivity.FactorGroup;
import fr.ifremer.isisfish.simulator.sensitivity.SensitivityUtils;
import fr.ifremer.isisfish.simulator.sensitivity.domain.ContinuousDomain;
import fr.ifremer.isisfish.simulator.sensitivity.domain.DiscreteDomain;
import fr.ifremer.isisfish.simulator.sensitivity.domain.EquationDiscreteDomain;
import fr.ifremer.isisfish.simulator.sensitivity.domain.RuleDiscreteDomain;
import fr.ifremer.isisfish.types.Month;
import fr.ifremer.isisfish.types.RangeOfValues;
import fr.ifremer.isisfish.types.TimeStep;
import fr.ifremer.isisfish.types.TimeUnit;
import fr.ifremer.isisfish.ui.SimulationUI;
import fr.ifremer.isisfish.ui.input.equation.InputOneEquationUI;
import fr.ifremer.isisfish.ui.simulator.RuleChooser;
import fr.ifremer.isisfish.ui.widget.editor.MonthComponent;
import fr.ifremer.isisfish.ui.widget.editor.StepComponent;

/**
 * Handler for all class in wizard packages.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class FactorWizardHandler implements ContinuousPanelListener {

    /** Class logger. */
    private static final Log log = LogFactory.getLog(FactorWizardHandler.class);

    protected FactorWizardUI factorWizardUI;

    public FactorWizardHandler(FactorWizardUI factorWizardUI) {
        this.factorWizardUI = factorWizardUI;
    }

    protected void afterInit() {
        factorWizardUI.addPropertyChangeListener(FactorWizardUI.PROPERTY_CONTINUE_SELECTED, evt -> {
            if ((Boolean)evt.getNewValue()) {
                ((CardLayout)factorWizardUI.hidablePanel.getLayout()).show(factorWizardUI.hidablePanel, "continuousPanelContainer");
            } else {
                ((CardLayout)factorWizardUI.hidablePanel.getLayout()).show(factorWizardUI.hidablePanel, "discretePanelContainer");
            }
        });
    }

    /**
     * Return opened topia context that need to be closed.
     * 
     * There is several things in ugly jaxx context:
     * <ul>
     * <li>opened TopiaContext from SensitivityTabUI that is null in ParamUI
     * <li>RegionStorage on SimulAction
     * </ul>
     * 
     * Use region storage here.
     * 
     * @return opened topia context
     * @throws TopiaException 
     */
    protected TopiaContext getTopiaContext(FactorWizardUI factorWizardUI) throws TopiaException {
        // RegionStorage object is common to ParamsUI
        // and SensitivityTabUI and may be valued
        RegionStorage regionStorage = factorWizardUI.getContextValue(RegionStorage.class);
        TopiaContext context = regionStorage.getStorage().beginTransaction();
        return context;
    }

    /**
     * Initialize un nouveau wizard avec lorsque l'utilisateur clic sur
     * un layer sont le sous composant accepte la mise en facteur.
     * 
     * On recupere des info sur le type correspondant à la proprieté a mettre
     * en facteur pour savoir le composant d'edition et s'il peut etre continue
     * ou pas.
     * 
     * @param factorWizardUI factorWizardUI
     * @param bean bean in current ui
     * @param property bean property to edit
     */
    public void initNewFactor(FactorWizardUI factorWizardUI, TopiaEntityContextable bean, String property) {

        // path is topiaId#property
        // ex : fwn#fsd#0.3425345#name
        // for JAXX : cOrigine start with upper case
        // for commons beanutils : must be lower case
        String beanProperty = StringUtils.uncapitalize(property);
        String path = bean.getTopiaId() + "#" + beanProperty;
        factorWizardUI.setFactorPath(path);
        factorWizardUI.getBean().setName(bean.toString() + "." + beanProperty);

        // get value for pointed path
        //TopiaContext topiaContext = factorWizardUI.getContextValue(TopiaContext.class);
        //Class<?> classForPath = getPropertyClass(path, topiaContext);
        // peut etre pas une bonne idée que ce soit basé sur les valeurs
        // au lieu des types (mais pour RangeOfValues, pas evident)
        try {
            Object valueForPath = PropertyUtils.getProperty(bean, beanProperty);
            boolean continuePossible = SensitivityUtils.canBeContinue(valueForPath);
            boolean continueSelected = SensitivityUtils.isContinue(valueForPath);
    
            // init panel
            if (continuePossible) {
                ContinuousPanel comp = getContinuousPanel(valueForPath, bean, property);
                comp.addContinuousPanelListener(this);
                factorWizardUI.getContinuousPanelContainer().add(comp, BorderLayout.CENTER);
            }
    
            // after, for binding on continuePossible, continueSelected to work
            factorWizardUI.setContinuePossible(continuePossible);
            factorWizardUI.setContinueSelected(continueSelected);
            factorWizardUI.getContinueRadio().setSelected(continueSelected);
        } catch (Exception ex) {
            throw new IsisFishRuntimeException("Can't init wizard", ex);
        }
    }

    /**
     * Appelé suite a un double clic sur l'arbre des facteurs pour modifier un
     * facteur.
     * 
     * @param factorWizardUI view to init
     * @param factor factor factor to edit
     */
    public void initExistingFactor(FactorWizardUI factorWizardUI, Factor factor) {

        factorWizardUI.setEditingState(true);
        String factorPath = factor.getPath();
        Domain domain = factor.getDomain();
        String factorName = factor.getName();
        factorWizardUI.setFactorPath(factorPath);
        factorWizardUI.getComment().setText(factor.getComment());

        if (domain instanceof ContinuousDomain) {

            ContinuousPanel comp = getNewContinuousComponent(factorWizardUI, factor.getPath());
            comp.initWithFactor(factor);
            factorWizardUI.getContinuousPanelContainer().add(comp, BorderLayout.CENTER);

            factorWizardUI.getContinueRadio().setSelected(true);
            factorWizardUI.setContinueSelected(true);
            factorWizardUI.setContinuePossible(true);
        } else {

            // un facteur pour être edité sans domain
            // lors de la creation
            if (domain != null) {
                // restaure discrete domain
                DiscreteDomain dDomain = (DiscreteDomain)domain;
    
                int nb = dDomain.getValues().size();
                factorWizardUI.getDiscretNumber().setText(String.valueOf(nb));
                factorWizardUI.getTabPane().removeAll();
    
                SortedMap<Object, Object> values = dDomain.getValues();
                int i = 0;
                for (Object o : values.values()) {
                    i++;
                    JComponent c = null;
                    if (o != null) {
                        c = getEditorWithValue(factorWizardUI, factor, dDomain, o);
                    } else if (log.isWarnEnabled()) {
                        log.warn("Null value in factor");
                    }
                    JScrollPane js = new JScrollPane(c);
                    String tabName = t("isisfish.sensitivity.discretevaluelabel", i);
                    factorWizardUI.getTabPane().addTab(tabName, js);
                }
            }

            // init non selected continous panel
            JComponent comp = getNewContinuousComponent(factorWizardUI, factor.getPath());
            if (comp != null) {
                factorWizardUI.setContinuePossible(true);
                factorWizardUI.getContinuousPanelContainer().add(comp, BorderLayout.CENTER);
            }
        }

        factorWizardUI.getBean().setName(factorName);
    }

    /**
     * Renvoie un componsant gaphique initialisé avec la valeur
     * qui est enregistré dans le domain.
     * Pour réédition d'un facteur existant (facteur discret).
     * 
     * @param factorWizardUI context for context value (RegionStorage)
     * @param value type to get editor
     * @param factor factor for path value
     * @param domain domain for domain type
     * @return component ui component with value
     */
    protected JComponent getEditorWithValue(FactorWizardUI factorWizardUI, Factor factor, Domain domain, Object value) {

        JComponent result = null;
        if (Double.class.isAssignableFrom(value.getClass())) {
            result = new JTextField();
            ((JTextField)result).setText(String.valueOf(value));
        }
        else if (double.class.isAssignableFrom(value.getClass())) {
            result = new JTextField();
            ((JTextField)result).setText(String.valueOf(value));
        }
        else if (MatrixND.class.isAssignableFrom(value.getClass())) {
            result = new MatrixPanelEditor();
            ((MatrixPanelEditor)result).setMatrix((MatrixND)value);
        }
        else if (domain instanceof RuleDiscreteDomain) {
            result = new RuleChooser(factorWizardUI);
            ((RuleChooser)result).setRulesList((List<Rule>)value);
        }
        else if (domain instanceof EquationDiscreteDomain) {
            String factorPath = factor.getPath();
            if (factor.getPath().indexOf('#') != -1) {
                String topiaId = factorPath.substring(0, factorPath.lastIndexOf('#'));
                String property = factorPath.substring(factorPath.lastIndexOf('#') + 1);

                // get bean in database
                try {
                    TopiaContext topiaContext = getTopiaContext(factorWizardUI);
                    TopiaEntityContextable entity = (TopiaEntityContextable)topiaContext.findByTopiaId(topiaId);
                    Equation equation = (Equation)PropertyUtils.getProperty(entity, property);
                    //topiaContext.closeContext();

                    // fill component
                    InputOneEquationUI ui = new InputOneEquationUI(factorWizardUI);
                    ui.setAutoSaveModification(false);
                    ui.setFormuleCategory(equation.getCategory());
                    ui.setText(t("isisfish.common.equation")); // can't get real name
                    ui.setClazz(value.getClass());
                    ui.setBeanProperty(property);
                    ui.setBean(entity); // set bean fire content modification event
                    ui.getEditor().setText((String)value);
                    ui.setActive(true);

                    result = ui;
                } catch (Exception ex) {
                    throw new IsisFishRuntimeException("Can't restore initial factor database property", ex);
                }
            }
            // TODO path with no # (normalement pas possible pour les equations)
        }
        else if (value instanceof TimeUnit) {
            result = new JTextField();
            ((JTextField)result).setText(String.valueOf(((TimeUnit)value).getTime()));
        } else if (value instanceof TopiaEntity) {
            RegionStorage regionStorage = factorWizardUI.getContextValue(RegionStorage.class);
            TopiaContext context = null;
            try {
                context = regionStorage.getStorage().beginTransaction();
                List list = context.findAll("from " + value.getClass().getName());
                JComboBox c = new JComboBox<>(list.toArray());
                c.setSelectedItem(value);
                result = c;
            } catch (TopiaException ex) {
                throw new IsisFishRuntimeException("Can't get entity list", ex);
            } finally {
                if (context != null) {
                    try {
                        context.closeContext();
                    } catch (TopiaException ex) {
                        throw new IsisFishRuntimeException("Can't get entity list", ex);
                    }
                }
            }
        } else if (value instanceof TimeStep) {
            TimeStep timeStep = (TimeStep)value;
            result = new StepComponent(timeStep.getMonth().getMonthNumber(), timeStep.getYear());
        } else if (value instanceof Month) {
            Month month = (Month)value;
            result = MonthComponent.createMounthCombo(month.getMonthNumber());
        } else if (value instanceof String) {
            // valeur non typées ???
            result = new JTextField();
            ((JTextField)result).setText(String.valueOf(value));
        }
        
        if (log.isDebugEnabled()) {
            log.debug("Editor for value " + value + " is " + result);
        }

        return result;
    }

    /**
     * Retourne le componant permettant de mettre en facteur continue
     * une valeur.
     * 
     * @param value value (must be continuable)
     * @param bean bean (in case of equation)
     * @param property bean property (in case of equation)
     * @return component initialized
     */
    protected ContinuousPanel getContinuousPanel(Object value, TopiaEntityContextable bean, String property) {
        ContinuousPanel result;

        if (value instanceof RangeOfValues) {
            RangeOfValues rangeOfValues = (RangeOfValues)value;
            //DefaultContinuousPanelUI ui = new DefaultContinuousPanelUI();
            String values = rangeOfValues.getValues();
            String min = "0";
            //String max = "0";
            if (values.matches("^\\ *[0-9]*\\ *\\-\\ *[0-9]*\\ *$")) {
                int first = values.indexOf("-");
                if (first != -1) {
                    min = values.substring(0, first);
                    //max = values.substring(first + 1);
                }
            }
            //ui.init(min, max, min, null);
            //result = ui;
            result = new ContinuousDistributionPanel(min);
        } else if (value instanceof Equation) {
            Equation equation = (Equation)value;
            EquationContinuousPanelUI ui = new EquationContinuousPanelUI();
            ui.setSelectedEquation(equation);
            ui.setText(t("isisfish.common.equation")); // can't get real name
            ui.setFormuleCategory(equation.getCategory());
            ui.setClazz(equation.getJavaInterface());
            ui.setBeanProperty(property);
            ui.setBean(bean);
            result = ui;
        } else if (value instanceof MatrixND) {
            MatrixND matrix = (MatrixND)value;
            result = new ContinuousDistributionPanel(matrix.clone());
        } else if (value instanceof TimeUnit) {
            TimeUnit timeUnit = (TimeUnit)value;
            //DefaultContinuousPanelUI ui = new DefaultContinuousPanelUI();
            //ui.init(String.valueOf(timeUnit.getTime()), String.valueOf(timeUnit.getTime()),
            //        String.valueOf(timeUnit.getTime()), null);
            //result = ui;
            result = new ContinuousDistributionPanel(String.valueOf(timeUnit.getTime()));
        } else {
            //DefaultContinuousPanelUI ui = new DefaultContinuousPanelUI();
            //ui.init(String.valueOf(value), String.valueOf(value), String.valueOf(value), null);
            //result = ui;
            result = new ContinuousDistributionPanel(String.valueOf(value));
        }

        if (log.isDebugEnabled()) {
            log.debug("Component for " + value + " (" + bean + ", " + property + ")");
            log.debug(" > " + result);
        }

        return result;
    }

    /**
     * Rafraichit l'assistant de facteur pour modifier le nombre de valeurs
     * (onglet) d'un composant discret.
     * 
     * @param factorWizardUI factorWizardUI
     */
    public void addTabs(FactorWizardUI factorWizardUI) {
        String discreteNumber = factorWizardUI.getDiscretNumber().getText();
        int nbTab = Integer.parseInt(discreteNumber);
        int currentCount = factorWizardUI.getTabPane().getTabCount();

        // remove useless tab
        for (int tab = currentCount - 1; tab > nbTab -1 ; tab--) {
            factorWizardUI.getTabPane().remove(tab);
        }

        // add new tabs
        for (int tab = currentCount ; tab < nbTab ; tab++) {
            JComponent c = getNewDiscreteComponent(factorWizardUI);
            String tabName = t("isisfish.sensitivity.discretevaluelabel", tab);
            factorWizardUI.getTabPane().addTab(tabName, c);
        }

        factorWizardUI.pack();
    }

    /**
     * Return new discrete component inited with value defined by factor path.
     * 
     * Le composant retourné est inclut dans un jscrollpane (sauf pour les
     * matrices qui contient deja un jscrollpane)
     * 
     * @param factorWizardUI factorWizardUI
     * @return component copy
     */
    protected JComponent getNewDiscreteComponent(FactorWizardUI factorWizardUI) {

        JComponent result = null;
        String factorPath = factorWizardUI.getFactorPath();

        try {
            if (factorPath.indexOf('#') != -1) {
                String topiaId = factorPath.substring(0, factorPath.lastIndexOf('#'));
                String property = factorPath.substring(factorPath.lastIndexOf('#') + 1);

                // get bean in database
                TopiaContext topiaContext = getTopiaContext(factorWizardUI);
                TopiaEntityContextable entity = (TopiaEntityContextable)topiaContext.findByTopiaId(topiaId);
                Object value = PropertyUtils.getProperty(entity, property);

                // init new jcomponent for value
                if (value instanceof Number) {
                    result = new JTextField(String.valueOf(value));
                } else if (value instanceof MatrixND) {
                    result = new MatrixPanelEditor();
                    MatrixND matrix = ((MatrixND)value).copy();
                    ((MatrixPanelEditor)result).setMatrix(matrix);
                } else if (value instanceof RangeOfValues) {
                    RangeOfValues rangeOfValues = (RangeOfValues)value;
                    result = new JTextField(rangeOfValues.getValues());
                } else if (value instanceof TimeUnit) {
                    TimeUnit timeUnit = (TimeUnit)value;
                    result = new JTextField(String.valueOf(timeUnit.getTime()));
                } else if (value instanceof Equation) {
                    Equation equation = (Equation)value;
                    // fill component
                    InputOneEquationUI ui = new InputOneEquationUI(factorWizardUI);
                    ui.setAutoSaveModification(false);
                    ui.setText(equation.getContent());
                    ui.setFormuleCategory(equation.getCategory());
                    ui.setText(t("isisfish.common.equation")); // can't get real name
                    ui.setClazz(value.getClass());
                    ui.setBeanProperty(property);
                    ui.setBean(entity);
                    ui.setActive(true);
                    result = ui;
                }

                topiaContext.closeContext();

            }
            else {
                // dans ce cas c'est des regles, pop de départ ou parametres de regles
                // c'est un peu galere car le code n'a rien a voir avec le reste
                // donc, c'est du cas par cas
                if (factorPath.equals("parameters.rules")) {
                    result = new RuleChooser(factorWizardUI);
                } else if (factorPath.startsWith("parameters.population.")) {
                    // la seule facon d'avoir les parametres ici est d'aller
                    // les chercher dans les parametres de simulation
                    Pattern pattern = Pattern.compile("^parameters\\.population\\.(\\w+)(\\.(.+)?)$");
                    Matcher matcher = pattern.matcher(factorPath);
                    if (matcher.matches()) {
                        String populationName = matcher.group(1);
                        if (log.isDebugEnabled()) {
                            log.debug("Loading population : " + populationName);
                        }
                        TopiaContext topiaContext = getTopiaContext(factorWizardUI);
                        Population pop = IsisFishDAOHelper.getPopulationDAO(topiaContext).findByName(populationName);
                        MatrixND N = getParameters().getNumberOf(pop);
                        result = new MatrixPanelEditor();
                        ((MatrixPanelEditor)result).setMatrix(N.clone());
                        topiaContext.closeContext();
                    }
                } else if (factorPath.startsWith("parameters.rule.")) {
                    Pattern pattern = Pattern.compile("^parameters\\.rule\\.(\\d+)\\.parameter\\.(\\w+)(\\..+)?$");
                    Matcher matcher = pattern.matcher(factorPath);
                    if (matcher.matches()) {
                        int ruleIndex = Integer.parseInt(matcher.group(1));
                        Rule rule = getParameters().getRules().get(ruleIndex);
                        Class valueClazz = RuleStorage.getParameterType(rule, matcher.group(2));
                        Object value = RuleStorage.getParameterValue(rule, matcher.group(2));
                        result = getTypeDiscreteComponent(factorWizardUI, valueClazz, value);
    
                    } else {
                        // double...
                        result = new JTextField("0.0");
                    }
                } else {
                    if (log.isWarnEnabled()) {
                        log.warn("Can't find component for path " + factorPath);
                    }
                }
            }
        } catch (Exception ex) {
            throw new IsisFishRuntimeException("Can't restore intial factor database property", ex);
        }

        if (log.isDebugEnabled()) {
            log.debug("Component for path " + factorPath + " is " + result);
        }

        // hack : si on met 2 fois un jscrollpane, rien ne s'affiche
        if (!(result instanceof MatrixPanelEditor)) {
            result = new JScrollPane(result);
        }

        return result;
    }

    /**
     * Return new continuous component inited with value defined by factor path.
     * 
     * @param factorWizardUI (to use topia context)
     * @param factorPath factor path
     * @return component initialized
     */
    protected ContinuousPanel getNewContinuousComponent(FactorWizardUI factorWizardUI, String factorPath) {
        ContinuousPanel result = null;

        try {
            if (factorPath.indexOf('#') != -1) {
                String topiaId = factorPath.substring(0, factorPath.lastIndexOf('#'));
                String property = factorPath.substring(factorPath.lastIndexOf('#') + 1);

                // get bean in database
                TopiaContext topiaContext = getTopiaContext(factorWizardUI);
                TopiaEntityContextable entity = (TopiaEntityContextable)topiaContext.findByTopiaId(topiaId);
                Object value = PropertyUtils.getProperty(entity, property);
                
                if (value instanceof RangeOfValues) {
                    RangeOfValues rangeOfValues = (RangeOfValues)value;
                    //DefaultContinuousPanelUI ui = new DefaultContinuousPanelUI();
                    String values = rangeOfValues.getValues();
                    String min = "0";
                    //String max = "0";
                    if (values.matches("^\\ *[0-9]*\\ *\\-\\ *[0-9]*\\ *$")) {
                        int first = values.indexOf("-");
                        if (first != -1) {
                            min = values.substring(0, first);
                            //max = values.substring(first + 1);
                        }
                    }
                    result = new ContinuousDistributionPanel(min);
                } else if (value instanceof Equation) {
                    Equation equation = (Equation)value;
                    EquationContinuousPanelUI ui = new EquationContinuousPanelUI();
                    ui.setSelectedEquation(equation);
                    ui.setText(t("isisfish.common.equation")); // can't get real name
                    ui.setFormuleCategory(equation.getCategory());
                    ui.setClazz(equation.getClass());
                    ui.setBeanProperty(property);
                    ui.setBean(entity);
                    result = ui;
                } else if (value instanceof MatrixND) {
                    MatrixND matrix = (MatrixND)value;
                    result = new ContinuousDistributionPanel(matrix.clone());
                } else if (value instanceof TimeUnit) {
                    TimeUnit timeUnit = (TimeUnit)value;
                    //DefaultContinuousPanelUI ui = new DefaultContinuousPanelUI();
                    //ui.init(String.valueOf(timeUnit.getTime()), String.valueOf(timeUnit.getTime()),
                    //        String.valueOf(timeUnit.getTime()), null);
                    //result = ui;
                    result = new ContinuousDistributionPanel(String.valueOf(timeUnit.getTime()));
                } else {
                    //DefaultContinuousPanelUI ui = new DefaultContinuousPanelUI();
                    //ui.init(String.valueOf(value), String.valueOf(value), String.valueOf(value), null);
                    //result = ui;
                    result = new ContinuousDistributionPanel(String.valueOf(value));
                }
                
                topiaContext.closeContext();

            } else {
                if (factorPath.startsWith("parameters.population.")) {
                    // la seule facon d'avoir les parametres ici est d'aller
                    // les chercher dans les parametres de simulation
                    Pattern pattern = Pattern.compile("^parameters\\.population\\.(\\w+)(\\.(.+)?)$");
                    Matcher matcher = pattern.matcher(factorPath);
                    if (matcher.matches()) {
                        String populationName = matcher.group(1);
                        if (log.isDebugEnabled()) {
                            log.debug("Loading population : " + populationName);
                        }
                        TopiaContext topiaContext = getTopiaContext(factorWizardUI);
                        Population pop = IsisFishDAOHelper.getPopulationDAO(topiaContext).findByName(populationName);
                        MatrixND N = getParameters().getNumberOf(pop);
                        result = new ContinuousDistributionPanel(N.clone());
                        topiaContext.closeContext();
                    }
                } else if (factorPath.startsWith("parameters.rule.")) {
                    Pattern pattern = Pattern.compile("^parameters\\.rule\\.(\\d+)\\.parameter\\.(\\w+)(\\..+)?$");
                    Matcher matcher = pattern.matcher(factorPath);
                    if (matcher.matches()) {
                        int ruleIndex = Integer.parseInt(matcher.group(1));
                        Rule rule = getParameters().getRules().get(ruleIndex);
                        Class valueClazz = RuleStorage.getParameterType(rule, matcher.group(2));
                        Object value = RuleStorage.getParameterValue(rule, matcher.group(2));
                        result = getTypeContinousComponent(factorWizardUI, valueClazz, value);

                    } else {
                        // double...
                        result = getContinuousPanel(0.0, null, null);
                    }
                } else {
                    if (log.isWarnEnabled()) {
                        log.warn("Can't find component for path " + factorPath);
                    }
                }
            }
        } catch (Exception ex) {
            throw new IsisFishRuntimeException("Can't init wizard", ex);
        }

        if (result != null) {
            result.addContinuousPanelListener(this);
        }

        return result;
    }

    /**
     * Get new special component for typed parameters.
     * 
     * @param factorWizardUI
     * @param type
     * @return rule discrete component
     */
    protected JComponent getTypeDiscreteComponent(FactorWizardUI factorWizardUI, Class type, Object value) {

        JComponent result;

        if (TopiaEntity.class.isAssignableFrom(type)) {
            try {
                TopiaContext context = getTopiaContext(factorWizardUI);
                List list = context.findAll("from " + type.getName());
                JComboBox c = new JComboBox(list.toArray());
                result = c;
                context.closeContext();
            } catch (TopiaException ex) {
                throw new IsisFishRuntimeException("Can't get entity list", ex);
            }
        } else if (TimeStep.class.isAssignableFrom(type)) {
            result = new StepComponent(0, 0);
        } else if (Month.class.isAssignableFrom(type)) {
            result = MonthComponent.createMounthCombo(0);
        } else {
            if (value != null) {
                result = new JTextField(value.toString());
            } else {
                result = new JTextField();
            }
            
        }

        return result;
    }
    
    /**
     * Get new special component for typed parameters.
     * 
     * @param factorWizardUI
     * @param type type to get component
     * @return rule discrete component
     */
    protected ContinuousPanel getTypeContinousComponent(FactorWizardUI factorWizardUI, Class type, Object value) {

        ContinuousPanel result = null;

        if (Double.class.isAssignableFrom(type) || double.class.isAssignableFrom(type)) {
            if (value != null) {
                result = new ContinuousDistributionPanel(value.toString());
            } else {
                result = new ContinuousDistributionPanel(0.0);
            }
        }

        return result;
    }

    /**
     * Save current factor.
     * 
     * @param factorWizardUI factorWizardUI
     */
    public void save(FactorWizardUI factorWizardUI) {

        // get continuous component if any
        ContinuousPanel continuousPanel = null;
        if (factorWizardUI.getContinueRadio().isSelected() && factorWizardUI.getContinuousPanelContainer().getComponentCount() > 0) {
            continuousPanel = (ContinuousPanel)factorWizardUI.getContinuousPanelContainer().getComponent(0);
        }

        // first check is factor is valid
        boolean factorValid = true;
        if (continuousPanel != null) {
            factorValid = continuousPanel.isFactorValid();
        }
        if (!factorValid) {
            JOptionPane.showMessageDialog(factorWizardUI, t("isisfish.sensitivity.factor.notvalid"),
                    t("isisfish.sensitivity.title"), JOptionPane.ERROR_MESSAGE);
            return;
        }

        // call specific method depending on continuous/discrete
        String name = factorWizardUI.getBean().getName();
        if (factorWizardUI.getContinueRadio().isSelected()) {
            saveContinue(name,
                factorWizardUI.getComment().getText(), factorWizardUI.getFactorPath(), continuousPanel,
                factorWizardUI.isEditingState());
        } else {
            Component[] discreteComponents = factorWizardUI.getTabPane().getComponents();
            saveDiscret(name,
                    factorWizardUI.getComment().getText(), factorWizardUI.getFactorPath(), discreteComponents,
                    factorWizardUI.isEditingState());
        }

        // refresh factor list
        factorWizardUI.getContextValue(SimulationUI.class, "SimulationUI").refreshFactorTree();

        // close window
        factorWizardUI.dispose();
    }

    /**
     * Save a continous factor.
     * 
     * @param name factor name
     * @param comment comment
     * @param path factor path
     * @param panel panel
     * @param exist exist
     */
    protected void saveContinue(String name,
            String comment, String path, ContinuousPanel panel, boolean exist) {
        if (panel instanceof EquationContinuousPanelUI) {
            try {
                EquationContinuousPanelUI equationPanel = (EquationContinuousPanelUI) panel;
                String property = StringUtils.uncapitalize(equationPanel.getBeanProperty()) + "Content";
                TopiaEntityContextable bean = equationPanel.getBean();
                TopiaContext topiaContext = bean.getTopiaContext();
                PropertyUtils.setProperty(bean, property, equationPanel.getEditor().getEditor().getText());

                // Save equation
                bean.update();
                topiaContext.commitTransaction();

                List<Factor> factors = equationPanel.getHandler().getFactors();
                for (Factor factor : factors) {
                    factor.setName(name);
                    factor.setComment(comment);
                    factor.setPath(path);
                    addContinuousEquationFactor(factor, exist);
                }
            } catch (Exception ex) {
                if (log.isErrorEnabled()) {
                    log.error("Can't call method : ", ex);
                }
            }
        } else if (panel instanceof ContinuousDistributionPanel) {
            ContinuousDistributionPanel defaultPanel = (ContinuousDistributionPanel) panel;
            ContinuousDomain domain = defaultPanel.generateDomain();
            addContinuousFactor(name, comment, path, domain, exist);
        }
    }

    /**
     * Save a discret factor.
     * 
     * @param name
     * @param comment
     * @param path
     * @param components
     * @param exist
     */
    protected void saveDiscret(String name,
            String comment, String path, Component[] components, boolean exist) {
        List<Object> values = new ArrayList<>();
        
        boolean ruleFactor = false;
        boolean equationFactor = false;
        for (Component component : components) {

            if (component instanceof JScrollPane) {
                component = ((JScrollPane) component).getViewport().getView();
            }

            // get internat component value
            Object result = null;
            if (component instanceof JTextComponent) {
                result = ((JTextComponent) component).getText();
            } else if (component instanceof MatrixPanelEditor) {
                result = ((MatrixPanelEditor) component).getMatrix();
            } else if (component instanceof InputOneEquationUI) {
                result = ((InputOneEquationUI) component).getEditor().getText();
                equationFactor = true;
            } else if (component instanceof RuleChooser) {
                result = ((RuleChooser)component).getRulesList();
                ruleFactor = true;
            } else if (component instanceof StepComponent) {
                result = new TimeStep(((StepComponent)component).getSelectedValue());
            } else if (component instanceof MonthComponent) {
                result = new Month(((MonthComponent)component).getSelectedValue());
            } else if (component instanceof JComboBox) {
                // on suppose qu'il y a dedans des TopiaEntity
                result = ((JComboBox)component).getSelectedItem();
            }

            values.add(result);
        }
        
        if (ruleFactor) {
            addDiscreteRuleFactor(name, comment, path, values, exist);
        } else if (equationFactor) {
            addDiscreteEquationFactor(name, comment, path, values, exist);
        } else {
            addDiscreteFactor(name, comment, path, values, exist);
        }
    }

    /**
     * Remove current factor.
     * 
     * @param factorWizardUI factorWizardUI
     */
    public void remove(FactorWizardUI factorWizardUI) {
        removeFactor(factorWizardUI.getFactorPath());
        factorWizardUI.getContextValue(SimulationUI.class, "SimulationUI").refreshFactorTree();
        factorWizardUI.dispose();
    }
    
    protected void addFactor(Factor f) {
        if (log.isDebugEnabled()) {
            log.debug("Add factor (" + f.getName() + ") : " +f.getPath());
        }
        getFactorGroup().addFactor(f);
    }

    /**
     * Ajout d'un facteur continue de type (min/max).
     * 
     * @param name
     * @param comment
     * @param path
     * @param domain
     * @param exist
     */
    public void addContinuousFactor(String name, String comment, String path,
            ContinuousDomain domain, boolean exist) {
        Factor f = new Factor(name);
        f.setDomain(domain);
        f.setComment(comment);
        f.setPath(path);
        if (exist) {
            removeFactor(path);
        }
        addFactor(f);
    }

    public void addDiscreteFactor(String name, String comment, String path,
            List<Object> values, boolean exist) {
        addDiscreteFactor(new DiscreteDomain(), name, comment, path, values, exist);
    }
    
    public void addDiscreteRuleFactor(String name, String comment, String path,
            List<Object> values, boolean exist) {
        addDiscreteFactor(new RuleDiscreteDomain(), name, comment, path, values, exist);
    }
    
    public void addDiscreteEquationFactor(String name, String comment, String path,
            List<Object> values, boolean exist) {
        addDiscreteFactor(new EquationDiscreteDomain(), name, comment, path, values, exist);
    }
    
    protected void addDiscreteFactor(DiscreteDomain domain, String name, String comment, String path,
            List<Object> values, boolean exist) {
        Factor f = new Factor(name);
        SortedMap<Object, Object> domainValues = new TreeMap<>();
        int label = 0;
        for (Object value : values) {
            // FIXME test when integer
            // Don't work with String ;(
            domainValues.put(label, value);
            // and start at 0
            label++;
        }
        domain.setValues(domainValues);
        f.setDomain(domain);
        f.setComment(comment);
        f.setPath(path);
        if (exist) {
            removeFactor(path);
        }
        addFactor(f);
    }

    public void addContinuousEquationFactor(Factor f, boolean exist) {
        // factor name need to be composed
        //Factor f = new Factor(name + "." + domain.getVariableName());
        //f.setDomain(domain);
        //f.setComment(comment);
        //f.setPath(path);
        if (exist) {
            removeFactor(f.getPath());
        }
        addFactor(f);
    }
    
    /**
     * Remove factor in factor group tree by path.
     * 
     * @param factorPath factor path to remove
     */
    public void removeFactor(String factorPath) {
        removeFactor(getFactorGroup(), factorPath);
    }

    /**
     * Recursive remove for factor in factor group by path.
     * 
     * @param factorGroup factor group to search to
     * @param factorPath factor path to remove
     */
    protected void removeFactor(FactorGroup factorGroup, String factorPath) {
        Collection<Factor> factorCopy = new ArrayList<>(factorGroup.getFactors());
        for (Factor factor : factorCopy) {
            if (factor instanceof FactorGroup) {
                removeFactor((FactorGroup)factor, factorPath);
            }
            if (factorPath.equals(factor.getPath())) {
                factorGroup.remove(factor);
            }
        }
    }
    
    protected FactorGroup getFactorGroup() {
        return factorWizardUI.getContextValue(FactorGroup.class);
    }
    
    protected SimulationParameter getParameters() {
        return factorWizardUI.getContextValue(SimulationParameter.class);
    }

    @Override
    public void panelChanged() {
        factorWizardUI.pack();
    }
}
