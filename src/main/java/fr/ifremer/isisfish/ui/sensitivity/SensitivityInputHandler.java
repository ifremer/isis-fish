/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.sensitivity;

import static org.nuiton.i18n.I18n.t;

import java.awt.BorderLayout;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import fr.ifremer.isisfish.ui.simulator.SimulatorContext;
import fr.ifremer.isisfish.util.ErrorHelper;
import fr.ifremer.isisfish.util.IsisFileUtil;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.persistence.TopiaEntityContextable;

import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.datastore.RegionStorage;
import fr.ifremer.isisfish.entities.FisheryRegion;
import fr.ifremer.isisfish.mexico.MexicoHelper;
import fr.ifremer.isisfish.simulator.sensitivity.DesignPlan;
import fr.ifremer.isisfish.simulator.sensitivity.Factor;
import fr.ifremer.isisfish.simulator.sensitivity.FactorGroup;
import fr.ifremer.isisfish.ui.NavigationHandler;
import fr.ifremer.isisfish.ui.WelcomePanelUI;
import fr.ifremer.isisfish.ui.input.InputContentUI;
import fr.ifremer.isisfish.ui.input.InputSaveVerifier;
import fr.ifremer.isisfish.ui.input.tree.FisheryDataProvider;
import fr.ifremer.isisfish.ui.input.tree.FisheryTreeHelper;
import fr.ifremer.isisfish.ui.input.tree.FisheryTreeNode;
import fr.ifremer.isisfish.ui.input.tree.FisheryTreeRenderer;
import fr.ifremer.isisfish.ui.sensitivity.model.FactorTreeModel;
import fr.ifremer.isisfish.ui.sensitivity.wizard.FactorWizardHandler;
import fr.ifremer.isisfish.ui.sensitivity.wizard.FactorWizardUI;

/**
 * Handler for sensitivity tab ui (fishery region factors).
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class SensitivityInputHandler extends NavigationHandler {

    /** Class logger. */
    private static Log log = LogFactory.getLog(SensitivityInputHandler.class);

    protected SensitivityInputUI sensitivityInputUI;

    public SensitivityInputHandler(SensitivityInputUI sensitivityInputUI) {
        this.sensitivityInputUI = sensitivityInputUI;
    }

    protected void afterInit() {
        
    }

    protected InputSaveVerifier getVerifier() {
        return sensitivityInputUI.getContextValue(InputSaveVerifier.class);
    }

    protected RegionStorage getRegionStorage() {
        return sensitivityInputUI.getContextValue(RegionStorage.class);
    }
    
    protected FactorGroup getFactorGroup() {
        return sensitivityInputUI.getContextValue(FactorGroup.class);
    }

    public void setTreeModel() {
        if (sensitivityInputUI.getFisheryRegion() != null) {
            sensitivityInputUI.getCardlayoutPrincipal().show(sensitivityInputUI.inputPanePrincipal,"normale");
            sensitivityInputUI.setContextValue(sensitivityInputUI.getFisheryRegion());
            loadFisheryRegionTree();
        }
    }

    protected void setInfoText(String s) {
        WelcomePanelUI root = sensitivityInputUI.getParentContainer(WelcomePanelUI.class);
        root.setStatusMessage(s);
    }

    protected void regionNull() {
        sensitivityInputUI.getCardlayoutPrincipal().show(sensitivityInputUI.inputPanePrincipal,"none");
        DefaultTreeModel model = new DefaultTreeModel(null);
        sensitivityInputUI.fisheryRegionTree.setModel(model);
    }

    public void setFactorModel() {
        FactorGroup factorGroup = getFactorGroup();
        FactorTreeModel model = new FactorTreeModel(factorGroup);
        sensitivityInputUI.factorsTree.setModel(model);
        sensitivityInputUI.getParentContainer(SensitivityUI.class).getSensitivityChooserUI().getHandler().setFactorCardinalityTableModel();
    }

    /**
     * 
     */
    public void loadFisheryRegionTree() {
        FisheryRegion fisheryRegion = sensitivityInputUI.getFisheryRegion();

        if (fisheryRegion == null) {
            // show empty region ui
            sensitivityInputUI.getCardlayoutPrincipal().show(sensitivityInputUI.getInputPanePrincipal(),"none");
            TreeModel model = new DefaultTreeModel(null);
            sensitivityInputUI.getFisheryRegionTree().setModel(model);
        }
        else {
            // init tree model loader with fishery region
            FisheryTreeHelper treeHelper = new FisheryTreeHelper();
            FisheryDataProvider dataProvider = new FisheryDataProvider(fisheryRegion);
            treeHelper.setDataProvider(dataProvider);
            TreeModel model = treeHelper.createTreeModel(fisheryRegion);
            sensitivityInputUI.getFisheryRegionTree().setModel(model);
            sensitivityInputUI.getFisheryRegionTree().setCellRenderer(new FisheryTreeRenderer(dataProvider));
            treeHelper.setUI(sensitivityInputUI.getFisheryRegionTree(), true, false, null);

            // global context value : fisheryRegion, regionStorage, treeHelper
            sensitivityInputUI.setContextValue(fisheryRegion);
            sensitivityInputUI.setContextValue(treeHelper);
            sensitivityInputUI.setContextValue(model);
            sensitivityInputUI.setContextValue(fisheryRegion.getTopiaContext());

            sensitivityInputUI.getCardlayoutPrincipal().show(sensitivityInputUI.getInputPanePrincipal(),"normale");
        }
    }

    /**
     * Changement de selection dans l'arbre de la pecherie.
     * 
     * @param event
     */
    public void nodeSelectionChanged(TreeSelectionEvent event) {

        TreePath newTreePath = event.getNewLeadSelectionPath();

        if (newTreePath != null) {
            Object lastTreePath = newTreePath.getLastPathComponent();
            if (lastTreePath instanceof FisheryTreeNode) {
                FisheryTreeNode isisTreeNode = (FisheryTreeNode)lastTreePath;

                Class<?> internalClass = isisTreeNode.getInternalClass();

                // noeud qui n'en charge pas d'autres (= un bean)
                TopiaEntityContextable topiaEntity = null;
                String topiaId = isisTreeNode.getId();

                try {
                    if (isisTreeNode.isStaticNode()) {
                        FisheryRegion fisheryRegion = sensitivityInputUI.getContextValue(FisheryRegion.class);
                        TopiaContext topiaContext = fisheryRegion.getTopiaContext();
                        topiaEntity = (TopiaEntityContextable)topiaContext.findByTopiaId(topiaId);
                    }

                    InputContentUI inputContentUI = getUIInstanceForBeanClass(internalClass, sensitivityInputUI);

                    // mandatory set
                    inputContentUI.getSaveVerifier().reset(); // before set bean !!!
                    if (topiaEntity != null) {
                        inputContentUI.getSaveVerifier().addCurrentEntity(topiaEntity);
                        inputContentUI.getSaveVerifier().setInputContentUI(inputContentUI);
                    }

                    inputContentUI.setBean(topiaEntity);
                    inputContentUI.setActive(topiaEntity != null);
                    inputContentUI.setLayer(true);
                    inputContentUI.setSensitivity(true);

                    // add initialized ui to panel
                    sensitivityInputUI.getCardlayoutPrincipal().show(sensitivityInputUI.getInputPanePrincipal(), "normale");
                    sensitivityInputUI.getInputPane().removeAll();
                    sensitivityInputUI.getInputPane().add(inputContentUI, BorderLayout.CENTER);
                    sensitivityInputUI.getInputPane().repaint();
                    sensitivityInputUI.getInputPane().validate();
                } catch (Exception ex) {
                    throw new IsisFishRuntimeException("Can't display bean " + topiaId, ex);
                }
            }
        }
    }

    /**
     * Add new continuous factor group in factor tree.
     * 
     * @param continuous continuous
     */
    public void addNewFactorGroup(boolean continuous) {
        String factorName = JOptionPane.showInputDialog(sensitivityInputUI, t("isisfish.sensitivity.newfactorname"),
                t("isisfish.sensitivity.title"), JOptionPane.QUESTION_MESSAGE);
        
        if (StringUtils.isNotBlank(factorName)) {
            FactorGroup rootFactorGroup = getFactorGroup();
            FactorGroup factorGroup = new FactorGroup(factorName, continuous);
            rootFactorGroup.addFactor(factorGroup);
            setFactorModel();
        }
    }

    /**
     * Move factors to another factorgroup.
     * 
     * @param selectedFactorGroup
     * @param movedFactors
     */
    public void moveFactor(FactorGroup selectedFactorGroup, List<Factor> movedFactors) {
        try {
            // add all factors, to do first, throw
            // exception if can't be done
            selectedFactorGroup.addAllFactors(movedFactors);

            // remove duplicated from factor group
            FactorGroup rootFactorGroup = getFactorGroup();
            if (!rootFactorGroup.equals(selectedFactorGroup)) {
                rootFactorGroup.removeAll(movedFactors);
            }
            for (int index = 0 ; index < rootFactorGroup.size(); ++index) {
                Factor factor = rootFactorGroup.get(index);
                if (factor instanceof FactorGroup) {
                    FactorGroup factorGroup = (FactorGroup)factor;
                    if (!factorGroup.equals(selectedFactorGroup)) {
                        factorGroup.removeAll(movedFactors);
                    }
                }
            }
            setFactorModel();
        } catch (IllegalArgumentException ex) {
            JOptionPane.showMessageDialog(sensitivityInputUI, t("isisfish.sensitivity.moveillegal"),
                    t("isisfish.sensitivity.title"), JOptionPane.ERROR_MESSAGE);
        }
    }
    
    /**
     * Mouse click on factors tree.
     * 
     * <ul>
     *  <li>normal click : factor edit</li>
     *  <li>right click : popup menu</li>
     * </ul>
     * 
     * @param e mouse event
     */
    public void factorsTreeMouseClicked(MouseEvent e) {
        // clic droit
        if (e.getButton() == MouseEvent.BUTTON3) {
            JPopupMenu menu = new JPopupMenu();
            JMenuItem menuItemDelete = new JMenuItem(t("isisfish.common.delete"));
            menuItemDelete.addActionListener(e1 -> deleteSelectedFactors());
            menu.add(menuItemDelete);
            menu.show(e.getComponent(), e.getX(), e.getY());
        }
        else if (e.getButton() == MouseEvent.BUTTON1 && e.getClickCount() == 2) {
            // autre double clic
            factorSelected();
        }
    }

    /**
     * Factor selection, display modification wizard.
     */
    protected void factorSelected() {
        // get selected factor
        TreePath selectedPath = sensitivityInputUI.getFactorsTree().getSelectionPath();
        
        // method appelee au clic, donc pas forcement de selection
        if (selectedPath != null) {
            Object[] pathWay = selectedPath.getPath();
            Object selectedObject = pathWay[pathWay.length - 1];
            if (selectedObject != null) {
                if (!(selectedObject instanceof FactorGroup)) {
                    Factor selectedFactor = (Factor)selectedObject;
                    FactorWizardUI factorWizardUI = new FactorWizardUI(sensitivityInputUI);
                    FactorWizardHandler handler = factorWizardUI.getHandler();
                    handler.initExistingFactor(factorWizardUI, selectedFactor);
                    factorWizardUI.pack();
                    factorWizardUI.setLocationRelativeTo(sensitivityInputUI);
                    factorWizardUI.setVisible(true);
                }
            }
        }
    }

    /**
     * Delete selection factors.
     */
    protected void deleteSelectedFactors() {
        // get selected factor
        TreePath[] selectedPaths = sensitivityInputUI.getFactorsTree().getSelectionPaths();
        if (!ArrayUtils.isEmpty(selectedPaths)) { // can happen
            for (TreePath selectedPath : selectedPaths) {
                Object[] pathWay = selectedPath.getPath();
                // > 2 : can't delete root
                if (pathWay.length >= 2) {
                    Object selectedObject = pathWay[pathWay.length - 1];
                    if (selectedObject != null) {
                        if (selectedObject instanceof Factor) {
                            Factor selectedFactor = (Factor)selectedObject;
                            FactorGroup selectedFactorGroup = (FactorGroup)pathWay[pathWay.length - 2];
                            if (log.isDebugEnabled()) {
                                log.debug("Deleting factor " + selectedFactor.getName());
                            }
                            selectedFactorGroup.remove(selectedFactor);
                            setFactorModel();
                        }
                    }
                }
            }
        }
    }

    /**
     * Export factors list to xml mexico file.
     * 
     * @since 4.1.1.2
     */
    public void exportFactorsToMexico() {
        File xmlFile = IsisFileUtil.getFile(t("isisfish.sensitivity.mexico.exporttoxml.title"),
                t("isisfish.sensitivity.mexico.exporttoxml.approve"), sensitivityInputUI,
                ".*\\.xml",
                t("isisfish.sensitivity.mexico.exportxml.filter"));

        if (xmlFile != null) {
            
            // try to add ".xml" extension if no present
            if (!FilenameUtils.isExtension(xmlFile.getAbsolutePath(), "xml")) {
                xmlFile = new File(xmlFile.getAbsolutePath() + ".xml");
            }

            DesignPlan designPlan = new DesignPlan();
            designPlan.setFactorGroup(getFactorGroup());
            //String xml = MexicoHelper.getDesignPlanAsXML(designPlan);
            MexicoHelper.writeDesignPlanToFile(xmlFile, designPlan);
        }
    }

    /**
     * Import factors list from xml mexico file.
     *
     * @since 4.5.0.0
     */
    public void importFactorsFromMexico() {
        File xmlFile = IsisFileUtil.getFile(t("isisfish.sensitivity.mexico.importfromxml.title"),
                t("isisfish.sensitivity.mexico.importfromxml.approve"), sensitivityInputUI,
                ".*\\.xml",
                t("isisfish.sensitivity.mexico.exportxml.filter"));

        if (xmlFile != null) {

            try {
                SimulatorContext context = sensitivityInputUI.getContextValue(SimulatorContext.class, "SimulatorContext");
                TopiaContext topiaContext = sensitivityInputUI.getContextValue(TopiaContext.class);
                DesignPlan designPlan = MexicoHelper.getDesignPlanFromXML(xmlFile, topiaContext);
                for (Factor factor : designPlan.getFactors()) {
                    if (log.isDebugEnabled()) {
                        log.debug("Find factor : " + factor.getName());
                    }
                    FactorGroup factorGroup = designPlan.getFactorGroup();
                    context.setFactorGroup(factorGroup);
                    sensitivityInputUI.factorsTreeModel.setRootFactor(factorGroup);
                }
            } catch (IOException e) {
                ErrorHelper.showErrorDialog(sensitivityInputUI, "Can't import mexico factor file", e);
            }
        }
    }
}
