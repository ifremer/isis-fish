/*
 * #%L
 * jTimer
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2008 - 2011 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.sensitivity.model;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.TransferHandler;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.ifremer.isisfish.simulator.sensitivity.Factor;
import fr.ifremer.isisfish.simulator.sensitivity.FactorGroup;
import fr.ifremer.isisfish.ui.sensitivity.SensitivityInputUI;

/**
 * Transfer handler used to transfer tasks in table.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$ By : $Author$
 */
public class FactorTransferHandler extends TransferHandler {

    /** serialVersionUID. */
    private static final long serialVersionUID = 5433321973795969278L;

    /** log. */
    private static Log log = LogFactory.getLog(FactorTransferHandler.class);

    /** Associated ui */
    protected SensitivityInputUI sensitivityInputUI;

    /**
     * Constructor.
     * 
     * @param sensitivityInputUI associated ui
     */
    public FactorTransferHandler(SensitivityInputUI sensitivityInputUI) {
        this.sensitivityInputUI = sensitivityInputUI;
    }

    @Override
    public boolean canImport(JComponent cp, DataFlavor[] df) {

        for (DataFlavor dataFlavor : df) {
            if (dataFlavor.equals(FactorTranferable.myData)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean importData(JComponent component, Transferable transferable) {

        boolean confirmImport = false;

        /*if (transferable.isDataFlavorSupported(FactorTranferable.myData)) {
            try {

                FactorTree factorTree = (FactorTree) component;
                FactorGroup selectedFactorGroup = factorTree.getSelectedFactorGroup();

                if (selectedFactorGroup != null) {
                    Object myObject = transferable.getTransferData(FactorTranferable.myData);
                    List<Factor> movedFactors = (List<Factor>) myObject;
                }
            } catch (IOException e) {
                if (log.isErrorEnabled()) {
                    log.error("Exception while transfering task", e);
                }
            } catch (UnsupportedFlavorException e) {
                if (log.isErrorEnabled()) {
                    log.error("Exception while transfering task", e);
                }
            }
        }*/

        confirmImport = true;

        return confirmImport;

    }

    /**
     * Get selected task in tree, and build Transferable object for it.
     * 
     * @param cp component
     * @return transferable instance for selected task
     */
    @Override
    protected Transferable createTransferable(JComponent cp) {

        Transferable transferable = null;

        FactorTree factorTree = (FactorTree) cp;
        // only task can be moved !
        List<Factor> selectedFactors = factorTree.getSelectedFactors();
        if (selectedFactors != null && !selectedFactors.isEmpty()) {
            List<Factor> factors = new LinkedList<>(selectedFactors);
            transferable = new FactorTranferable(factors);
        }

        return transferable;
    }

    @Override
    protected void exportDone(JComponent cp, Transferable transferable, int type) {
        if (log.isDebugEnabled()) {
            log.debug("Transfert done");
        }

        if (type == TransferHandler.MOVE) {

            try {
                FactorTree factorTree = (FactorTree) cp;
                // elements here, task can be move to group only
                FactorGroup selectedFactorGroup = factorTree.getSelectedFactorGroup();

                if (selectedFactorGroup != null) {
                    Object myObject = transferable.getTransferData(FactorTranferable.myData);
                    List<Factor> movedFactors = (List<Factor>) myObject;
                    sensitivityInputUI.getHandler().moveFactor(selectedFactorGroup, movedFactors);
                }
            } catch (IOException | UnsupportedFlavorException e) {
                if (log.isErrorEnabled()) {
                    log.error("Exception while transfering task", e);
                }
            }
        }
    }

    @Override
    public int getSourceActions(JComponent component) {
        return MOVE;
    }
}
