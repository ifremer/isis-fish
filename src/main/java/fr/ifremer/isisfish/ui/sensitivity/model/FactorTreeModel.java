/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2012 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.sensitivity.model;

import java.util.ArrayList;
import java.util.Collection;

import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.simulator.sensitivity.Factor;
import fr.ifremer.isisfish.simulator.sensitivity.FactorGroup;

/**
 * Cette classe gere l'affichage d'une liste de facteurs dans l'arbre.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date: 2008-06-13 11:05:57 +0200 (ven., 13 juin 2008)
 * $ By : $Author$
 */
public class FactorTreeModel implements TreeModel {

    /** Tree model root. */
    protected Factor rootFactor;
    
    /** Tree model listeners. */
    protected Collection<TreeModelListener> modelListeners;

    /**
     * Constructor.
     * 
     * @param rootFactor root factor (typically, a {@link FactorGroup})
     */
    public FactorTreeModel(Factor rootFactor) {
        this.rootFactor = rootFactor;
        modelListeners = new ArrayList<>();
    }

    public void setRootFactor(Factor rootFactor) {
        this.rootFactor = rootFactor;

        TreeModelEvent e = new TreeModelEvent(this, new TreePath(rootFactor));
        for (TreeModelListener modelListener : modelListeners) {
            modelListener.treeStructureChanged(e);
        }
    }

    @Override
    public Object getChild(Object parent, int index) {

        Object value = null;

        if (parent instanceof FactorGroup) {
            FactorGroup factorGroup = (FactorGroup)parent;
            value = factorGroup.get(index);
        }

        return value;
    }

    @Override
    public int getChildCount(Object parent) {

        int childCount = 0;

        if (parent instanceof FactorGroup) {
            FactorGroup factorGroup = (FactorGroup)parent;
            childCount = factorGroup.size();
        }

        return childCount;
    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {

        int childIndex = -1;

        if (parent instanceof FactorGroup) {
            FactorGroup factorGroup = (FactorGroup)parent;
            childIndex = factorGroup.indexOf(child);
        }

        return childIndex;
    }

    @Override
    public boolean isLeaf(Object node) {
        return getChildCount(node) == 0;
    }

    @Override
    public void addTreeModelListener(TreeModelListener l) {
        modelListeners.add(l);
    }

    @Override
    public Object getRoot() {
        return rootFactor;
    }

    @Override
    public void removeTreeModelListener(TreeModelListener l) {
        modelListeners.remove(l);
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {
        throw new IsisFishRuntimeException("Not implemented");
    }

}
