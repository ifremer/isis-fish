/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.ui.sensitivity;

import fr.ifremer.isisfish.ui.WelcomeSaveVerifier;
import fr.ifremer.isisfish.ui.simulator.SimulatorContext;
import jaxx.runtime.JAXXContext;

/**
 * Ce contexte regroupe les élements qui servent à une hierachie d'interfaces Sensitivity.
 * 
 * C'est globalement un context de simulation + input (pour la selection des fateurs).
 */
public class SensitivityContext extends SimulatorContext {

    public SensitivityContext(JAXXContext parent) {
        super(parent);
        
        // add save verifier for this hierarchy (input)
        SensitivitySaveVerifier sensitivitySaveVerifier = new SensitivitySaveVerifier();
        setContextValue(sensitivitySaveVerifier);
        
        // this verifier is linked to global verifier (input)
        WelcomeSaveVerifier welcomeSaveVerifier = getContextValue(WelcomeSaveVerifier.class);
        welcomeSaveVerifier.addSaveVerifier(sensitivitySaveVerifier);
    }
}
