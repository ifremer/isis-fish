package fr.ifremer.isisfish.ui.sensitivity.wizard;

/*-
 * #%L
 * ISIS-Fish
 * %%
 * Copyright (C) 1999 - 2020 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.isisfish.simulator.sensitivity.Factor;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EquationContinuousPanelHandler implements ContinuousPanelListener {

    protected EquationContinuousPanelUI equationContinuousPanelUI;

    protected List<Factor> factors = new ArrayList<>();

    public EquationContinuousPanelHandler(EquationContinuousPanelUI equationContinuousPanelUI) {
        this.equationContinuousPanelUI = equationContinuousPanelUI;
    }

    public void afterInit() {
        equationContinuousPanelUI.distributionPanel.addContinuousPanelListener(this);
    }

    public List<Factor> getFactors() {
        return factors;
    }

    public void initWithFactor(Factor factor) {
        addFactor(factor);
    }

    protected void addFactor(Factor factor) {
        factors.add(factor);
        // fire data change
        equationContinuousPanelUI.factorsListModel.setFactors(factors);
    }

    /**
     * Valid selected factor.
     */
    protected void validSelectedFactor() {
        // fill selected
        Factor selectedFactor = equationContinuousPanelUI.factorsList.getSelectedValue();
        selectedFactor.setDomain(equationContinuousPanelUI.distributionPanel.generateDomain());

        // parse equation content to replace
        // double xxx = 4.0;
        // by
        // double xxx = context.getValueAndCompute("myfactorname.xxx", 4.0)
        // if xxx is the variable name to replace
        FactorWizardUI wizard = equationContinuousPanelUI.getParentContainer(FactorWizardUI.class);
        String factorName = wizard.getFactorNameField().getText().trim();
        String variableName = selectedFactor.getEquationVariableName();
        String[] lines = equationContinuousPanelUI.editor.getEditor().getText().split("\n");
        String result = "";
        for (String line : lines) {
            Pattern p = Pattern.compile("(^.*\\s+" + variableName + "\\s*\\=\\s*)([\\d\\.]+).*\\;$");
            Matcher matcher = p.matcher(line);
            if (matcher.find()) {
                String fullVariableName = factorName + "." + variableName;
                line = matcher.group(1) +  "context.getValueAndCompute(\"" + fullVariableName + "\", " + matcher.group(2) + ");";
            }
            result += line + "\n";
        }
        equationContinuousPanelUI.editor.getEditor().setText(result);

        // fire data change
        equationContinuousPanelUI.factorsListModel.setFactors(factors);
    }

    protected void addNewVariable() {
        Factor factor = new Factor("");
        factor.setEquationVariableName("X");
        addFactor(factor);

        // auto select
        equationContinuousPanelUI.factorsList.setSelectedValue(factor, true);
    }

    protected void displaySelectedFactor() {
        Factor selectedFactor = equationContinuousPanelUI.factorsList.getSelectedValue();
        equationContinuousPanelUI.setSelectedFactor(selectedFactor);
        equationContinuousPanelUI.distributionPanel.initWithFactor(selectedFactor);
    }

    protected void removeSelectedVariable() {
        int selectedIndex = equationContinuousPanelUI.factorsList.getSelectedIndex();
        factors.remove(selectedIndex);

        // fire data change
        equationContinuousPanelUI.factorsList.clearSelection();
        equationContinuousPanelUI.factorsListModel.setFactors(factors);
    }

    public boolean isFactorValid() {
        return equationContinuousPanelUI.getValidator().isValid() && equationContinuousPanelUI.distributionPanel.isFactorValid();
    }

    @Override
    public void panelChanged() {
        equationContinuousPanelUI.fireContinousPanelEvent();
    }
}
