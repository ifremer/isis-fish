/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.sensitivity;

import fr.ifremer.isisfish.ui.input.InputSaveVerifier;

/**
 * Save verifier pour l'interface de sensibilité.
 * 
 * Surchargé pour ne rien faire. Apparement il est difficile de se passer
 * d'une instance dans les interface.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class SensitivitySaveVerifier extends InputSaveVerifier {

    @Override
    public int checkEdit() {
        // si jamais il y a eu des modifications dues a des setters
        // qui sont passés ou des modifications d'equation
        // on ne doit pas demander à l'utilisateur de sauver
        // ou meme carrement sauver les modification
        return YES_OPTION;
    }

}
