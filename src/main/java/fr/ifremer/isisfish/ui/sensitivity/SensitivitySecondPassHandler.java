package fr.ifremer.isisfish.ui.sensitivity;

/*
 * #%L
 * ISIS-Fish
 * %%
 * Copyright (C) 1999 - 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.simulator.SimulationParameter;
import fr.ifremer.isisfish.simulator.launcher.SimulationJob;
import fr.ifremer.isisfish.simulator.launcher.SimulationService;
import fr.ifremer.isisfish.simulator.launcher.SimulationServiceListener;
import fr.ifremer.isisfish.simulator.sensitivity.SensitivityAnalysis;
import fr.ifremer.isisfish.simulator.sensitivity.SensitivityException;
import fr.ifremer.isisfish.ui.models.common.GenericComboModel;

public class SensitivitySecondPassHandler {

    /** Class logger. */
    private static Log log = LogFactory.getLog(SensitivitySecondPassHandler.class);

    protected SensitivitySecondPassUI sensitivitySecondPassUI;

    // declaration sépérée de la variable d'instance (jaxx)
    protected SimulationServiceListener simulationListener;

    public SensitivitySecondPassHandler(SensitivitySecondPassUI sensitivitySecondPassUI) {
        this.sensitivitySecondPassUI = sensitivitySecondPassUI;
    }
    
    protected void afterInit() {
        // permet de rafaichir la liste les simulations
        // des qu'une simulation se termine
        simulationListener = new SimulationServiceListener() {
            @Override
            public void simulationStart(SimulationService simService, SimulationJob job) {
            }
        
            @Override
            public void simulationStop(SimulationService simService, SimulationJob job) {
                if (log.isDebugEnabled()) {
                    log.debug("Refresh second pass UI simulations list");
                }
                GenericComboModel<String> model = (GenericComboModel<String>)sensitivitySecondPassUI.fieldSensitivitySimulationSelect.getModel();
                String simulationName = job.getId();
                if (simulationName.startsWith("as_")) {
                    String masterSensitivityName = simulationName.substring(0, simulationName.lastIndexOf("_"));
                    if (!model.containsElement(masterSensitivityName)) {
                        model.addElement(masterSensitivityName);
                    }
                }
            }
        
            @Override
            public void clearJobDone(SimulationService simService) {
            }
        };
        SimulationService.getService().addSimulationServiceListener(simulationListener);
    }
    
    /**
     * Model de contenu de la liste des nom de simulations
     */
    public GenericComboModel<String> getSensitivitySimulationModel() {
        List<String> asNames = new ArrayList<>();
        for (String simulationName : SimulationStorage.getSimulationNames()) {
            if (simulationName.startsWith("as_")) {
                String masterSensitivityName = simulationName.substring(0, simulationName.lastIndexOf("_"));
                if (!asNames.contains(masterSensitivityName)) {
                    asNames.add(masterSensitivityName);
                }
            }
        }
        
        GenericComboModel<String> model = new GenericComboModel<>(asNames);
        return model;
    }

    protected void launchSecondPass(ActionEvent event) {
        if (sensitivitySecondPassUI.fieldSensitivitySimulationSelect.getSelectedIndex() != -1) {
            runSensitivitySecondPass((String)sensitivitySecondPassUI.fieldSensitivitySimulationSelect.getSelectedItem());
            displaySensitivitySecondPass(event);
        }
    }

    protected void displaySensitivitySecondPass(ActionEvent event) {
        if (sensitivitySecondPassUI.fieldSensitivitySimulationSelect.getSelectedIndex() != -1) {
        
            try {
                String selectedSimulationName = (String)sensitivitySecondPassUI.fieldSensitivitySimulationSelect.getSelectedItem();
                List<File> files = getSensitivitySecondPassResults(selectedSimulationName);
                
                String content = "";
                for(File file : files) {
                    content += "Fichier " + file.getName() + "\n";
                    content += "------------------------------\n";
                    content += FileUtils.readFileToString(file, StandardCharsets.UTF_8) + "\n";
                    content += "\n";
                }
                sensitivitySecondPassUI.textAreaSensitivitySecondpassResult.setText(content);
            } catch(IOException e) {
                if (log.isErrorEnabled()) {
                    log.error("Can't run second pass", e);
                }
            }
        }
    }
    
    /**
     * Lance la second passe d'une analyse de sensibilité.
     * 
     * @param masterSensitivitySimulationName nom du prefix de toutes les simulations (without _)
     */
    public void runSensitivitySecondPass(String masterSensitivitySimulationName) {

        // sensitivity analysis found
        SensitivityAnalysis sensitivityAnalysis = null;
        //List<SimulationStorage> simulationStorageForAnalyze = new ArrayList<SimulationStorage>();
        SortedMap<Integer, SimulationStorage> simulationStorageForAnalyze = new TreeMap<>();
        File simuationDirectory = SimulationStorage.getSimulationDirectory();
        for (File simuation : simuationDirectory.listFiles()) {
            if (simuation.isDirectory()
                    && simuation.getName().startsWith(
                            masterSensitivitySimulationName + "_")) {
                SimulationStorage storage = SimulationStorage
                        .getSimulation(simuation.getName());
                String suffix = simuation.getName().substring(
                        simuation.getName().lastIndexOf("_") + 1);
                simulationStorageForAnalyze.put(Integer.valueOf(suffix),
                        storage);

                if (sensitivityAnalysis == null) {
                    // try to find find calculator name in one storage
                    SimulationParameter params = storage.getParameter();
                    sensitivityAnalysis = params.getSensitivityAnalysis();
                }
            }
        }

        if (sensitivityAnalysis != null) {
            try {
                // build master sensitivity export directory
                File masterExportDirectory = new File(SimulationStorage.getSensitivityResultsDirectory(),
                        masterSensitivitySimulationName);
                if (!masterExportDirectory.isDirectory()) {
                    masterExportDirectory.mkdirs();
                }
                List<SimulationStorage> simulationStorageForAnalyzeList = new ArrayList<>(
                        simulationStorageForAnalyze.values());
                sensitivityAnalysis.analyzeResult(
                        simulationStorageForAnalyzeList, masterExportDirectory);
            } catch (SensitivityException e) {
                if (log.isErrorEnabled()) {
                    log.error("Can't call analyse result", e);
                }
            }
        } else {
            if (log.isWarnEnabled()) {
                log.warn("Can't run sensitivity second pass");
            }
        }
    }

    /**
     * Renvoie les resultats de la seconde pass.
     * 
     * @param masterSensitivitySimulationName nom du prefix de toutes les simulations (without _)
     * 
     * @return les fichiers genere lors de la seconde passe (seulement ceux qui ne commence pas par ".")
     */
    public List<File> getSensitivitySecondPassResults(
            String masterSensitivitySimulationName) {

        List<File> result = new ArrayList<>();

        // build master sensitivity export directory
        File masterExportDirectory = new File(SimulationStorage.getSensitivityResultsDirectory(), masterSensitivitySimulationName);
        if (masterExportDirectory.isDirectory()) {
            for (File exportFile : masterExportDirectory.listFiles()) {
                if (exportFile.isFile()
                        && !exportFile.getName().startsWith(".")) {
                    result.add(exportFile);
                }
            }
        }

        return result;
    }
}
