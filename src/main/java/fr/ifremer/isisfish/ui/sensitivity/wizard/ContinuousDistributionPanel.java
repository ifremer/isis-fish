/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2012 - 2020 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.sensitivity.wizard;

import static org.nuiton.i18n.I18n.t;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.JTextComponent;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.math.matrix.gui.MatrixPanelEditor;

import fr.ifremer.isisfish.simulator.sensitivity.Distribution;
import fr.ifremer.isisfish.simulator.sensitivity.Distribution.DistributionParam;
import fr.ifremer.isisfish.simulator.sensitivity.Factor;
import fr.ifremer.isisfish.simulator.sensitivity.domain.ContinuousDomain;

/**
 * Panel dynamique qui se construit suivant la distribution sélectionnée.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class ContinuousDistributionPanel extends ContinuousPanel implements ItemListener {

    /** Class logger. */
    private static final Log log = LogFactory.getLog(ContinuousDistributionPanel.class);

    /** serialVersionUID. */
    private static final long serialVersionUID = 2289588676194644582L;

    /** Label. */
    protected JLabel distributionLabel;

    /** Distribution combo. */
    protected JComboBox<Distribution> distributionCombo;

    /** Panel that contains dynamics components. */
    protected JPanel dynamicPanel;

    /** Original value used to auto fill some components. */
    protected Object originalValue;

    /** UI components used to fill distribution params value. */
    protected List<JComponent> paramComponents = new ArrayList<>();

    public ContinuousDistributionPanel(Object originalValue) {

        setLayout(new GridBagLayout());

        // label
        distributionLabel = new JLabel(t("isisfish.sensitivity.distribution"));
        add(distributionLabel, new GridBagConstraints(0, 0, 1, 1, 0, 0,
                GridBagConstraints.WEST, GridBagConstraints.NONE,
                new Insets(0, 0, 0, 0), 0, 0));

        // values possibility depending on input type
        Distribution[] values;
        if (originalValue instanceof MatrixND) {
            values = new Distribution[]{Distribution.QUNIFPC};
        } else {
            values = Distribution.values();
        }
        
        // combobox
        distributionCombo = new JComboBox<>(values);
        // important : for setSelectedItem to fire event
        distributionCombo.setSelectedItem(null);
        distributionCombo.addItemListener(this);
        distributionCombo.setRenderer(new DistributionListRenderer());
        add(distributionCombo, new GridBagConstraints(1, 0, 1, 1, 0, 0,
                GridBagConstraints.WEST, GridBagConstraints.NONE,
                new Insets(0, 0, 0, 0), 0, 0));
        
        // sub dynamic panel containing stuff
        dynamicPanel = new JPanel(new GridBagLayout());
        add(dynamicPanel, new GridBagConstraints(0, 1, 2, 1, 1, 1,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(5, 0, 0, 0), 0, 0));
        
        this.originalValue = originalValue;
    }

    @Override
    public void initWithFactor(Factor factor) {

        ContinuousDomain domain = (ContinuousDomain)factor.getDomain();
        // can happen during factor creation
        if (domain == null) {
            return;
        }

        Distribution distrib = domain.getDistribution();
        // this fires components list modification
        distributionCombo.setSelectedItem(distrib);

        DistributionParam[] params = distrib.getDistibutionParams();
        int paramComponentIndex = 0;
        for (DistributionParam param : params) {
            Object value = domain.getDistributionParameters().get(param.getName());
            if (value != null) {
                JComponent comp = paramComponents.get(paramComponentIndex);

                if (comp instanceof MatrixPanelEditor) {
                    ((MatrixPanelEditor)comp).setMatrix((MatrixND)value);
                } else {
                    ((JTextComponent)comp).setText(value.toString()); 
                }
            } else {
                if (log.isWarnEnabled()) {
                    log.warn("Param " + param.getName() + " not found during init");
                }
            }

            paramComponentIndex++;
        }
    }

    @Override
    public void itemStateChanged(ItemEvent e) {

        if (e.getStateChange() != ItemEvent.SELECTED) {
            return;
        }

        Distribution distrib = (Distribution)e.getItem();

        if (log.isDebugEnabled()) {
            log.debug("Refreshing component for distribution : " + distrib);
        }

        dynamicPanel.removeAll();
        paramComponents.clear();

        // location is good (after removeAll)
        if (distrib == null) {
            return;
        }

        int index = 0;
        // add label
        JLabel paramDetailsLabel = new JLabel(t("isisfish.sensitivity.distribution.parameters", distrib.getDescription()));
        dynamicPanel.add(paramDetailsLabel, new GridBagConstraints(0, index, 2, 1, 0, 0,
            GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
            new Insets(0, 0, 0, 0), 0, 0));
        index++;

        // add others params
        DistributionParam[] params = distrib.getDistibutionParams();
        for (DistributionParam param : params) {
            JLabel paramLabel = new JLabel(param.getName() + " :");
            paramLabel.setToolTipText(param.getDescription());

            JComponent paramField = getParamField(param);
            paramField.setToolTipText(param.getDescription());

            // rendu étendu pour les matrix panels
            if (paramField instanceof MatrixPanelEditor) {
                dynamicPanel.add(paramLabel, new GridBagConstraints(0, index, 2, 1, 0, 0,
                    GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
                    new Insets(0, 0, 0, 0), 0, 0));
                index++;
                dynamicPanel.add(paramField, new GridBagConstraints(0, index, 2, 1, 1, 1,
                    GridBagConstraints.NORTH, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
                index++;
            } else {
                // rendu simple
                dynamicPanel.add(paramLabel, new GridBagConstraints(0, index, 1, 1, 0, 0,
                    GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
                    new Insets(0, 0, 0, 0), 0, 0));
    
                dynamicPanel.add(paramField, new GridBagConstraints(1, index, 1, 1, 1, 0,
                    GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
                    new Insets(0, 0, 0, 0), 0, 0));
                index++;
            }

            paramComponents.add(paramField);
        }

        fireContinousPanelEvent();
        repaint();
        validate();
    }

    /**
     * Return new component used to edit parameter.
     * 
     * If {@link DistributionParam#isOriginalValue()} return true, then component
     * is rendered according to original value.
     * 
     * @param param param to render
     * @return component
     */
    protected JComponent getParamField(DistributionParam param) {
        
        JComponent result;

        if (param.isOriginalValue()) {
            if (originalValue instanceof MatrixND) {
                result = new MatrixPanelEditor();
                ((MatrixPanelEditor)result).setMatrix((MatrixND)originalValue);
            } else {
                result = new JTextField(originalValue.toString());
            }
        } else {
            result = new JTextField();
        }

        return result;
    }

    @Override
    public boolean isFactorValid() {
        return getDistribution() != null;
    }

    /**
     * Return distribution selected in combo box.
     *
     * @return selected distribution
     */
    protected Distribution getDistribution() {
        return (Distribution)distributionCombo.getSelectedItem();
    }

    /**
     * Generate domain filled with distribution and distribution parameters.
     * 
     * @return filled domain
     */
    public ContinuousDomain generateDomain() {
        Distribution distrib = getDistribution();
        ContinuousDomain domain = new ContinuousDomain(distrib);

        DistributionParam[] params = distrib.getDistibutionParams();
        int paramComponentIndex = 0;
        for (DistributionParam param : params) {
            JComponent comp = paramComponents.get(paramComponentIndex);

            if (comp instanceof MatrixPanelEditor) {
                domain.addDistributionParam(param.getName(), ((MatrixPanelEditor)comp).getMatrix());
            } else {
                // XXX echatellier 20131201 : always double type ?
                String strValue = ((JTextComponent)comp).getText().trim();
                domain.addDistributionParam(param.getName(), Double.parseDouble(strValue));
            }

            paramComponentIndex++;
        }
        return domain;
    }

    @Override
    public void setEnabled(boolean enabled) {
        distributionLabel.setEnabled(enabled);
        if (!enabled) {
            distributionCombo.setSelectedItem(null);
        }
        distributionCombo.setEnabled(enabled);
    }
}
