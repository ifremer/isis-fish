/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2011 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.sensitivity.model;

import static org.nuiton.i18n.I18n.t;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.ifremer.isisfish.simulator.sensitivity.Factor;

/**
 * Model de la table de definition des cadinalité de facteurs.
 * 
 * Columns :
 * <ul>
 * <li>Factor name</li>
 * <li>Cardinality</li>
 * </ul>
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class FactorCardinalityTableModel extends AbstractTableModel {

    /** serialVersionUID. */
    private static final long serialVersionUID = 7739699364084480516L;

    /** Log. */
    private static Log log = LogFactory
            .getLog(FactorCardinalityTableModel.class);

    /** Columns names. */
    public final static String[] COLUMN_NAMES = {
            t("isisfish.sensitivity.factor"),
            t("isisfish.sensitivity.increment") };

    protected List<Factor> factors;

    /**
     * Constructor with null data.
     */
    public FactorCardinalityTableModel() {
        this(null);
    }

    /**
     * Constructor with data.
     *  
     * @param factors factors
     */
    public FactorCardinalityTableModel(List<Factor> factors) {
        super();
        this.factors = factors;
    }

    @Override
    public int getColumnCount() {
        return COLUMN_NAMES.length;
    }

    @Override
    public int getRowCount() {
        int count = 0;

        if (factors != null) {
            count = factors.size();
        }
        return count;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        Object result;

        Factor factor = factors.get(rowIndex);
        switch (columnIndex) {
        case 0:
            result = factor.getName();
            break;
        case 1:
            result = factor.getCardinality();
            break;
        default:
            throw new IndexOutOfBoundsException("No such column " + columnIndex);
        }

        return result;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {

        Class<?> result;

        switch (columnIndex) {
        case 0:
            result = String.class;
            break;
        case 1:
            result = Integer.class;
            break;
        default:
            throw new IndexOutOfBoundsException("No such column " + columnIndex);
        }

        return result;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return COLUMN_NAMES[columnIndex];
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return columnIndex > 0;
    }

    @Override
    public void setValueAt(Object value, int rowIndex, int columnIndex) {

        if (log.isDebugEnabled()) {
            log.debug("Cell edition (column " + columnIndex + ") = " + value);
        }

        Factor factor = factors.get(rowIndex);
        switch (columnIndex) {
        case 1:
            Integer iValue = (Integer) value;
            factor.setCardinality(iValue);
            break;
        default:
            throw new IndexOutOfBoundsException("Can't edit column "
                    + columnIndex);
        }
    }
}
