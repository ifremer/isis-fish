/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2012 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.sensitivity.model;

import java.util.List;

import javax.swing.DefaultListModel;

import fr.ifremer.isisfish.simulator.sensitivity.Factor;

/**
 * Model pour la liste des {@link Factor}.
 * 
 * Pas de selection par defaut.
 *
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class FactorListModel extends DefaultListModel<Factor> {

    /** serialVersionUID. */
    private static final long serialVersionUID = 2281927104735245489L;

    /** EquationContinuousDomain list */
    protected List<Factor> factors;

    /**
     * Constructor with factors list.
     * 
     * @param factors factors list
     */
    public FactorListModel(List<Factor> factors) {
        this.factors = factors;
    }

    /**
     * Change data list and fire data change event.
     * 
     * @param factors new data list
     */
    public void setFactors(List<Factor> factors) {
        this.factors = factors;
        fireContentsChanged(this, 0, factors.size());
    }

    @Override
    public Factor getElementAt(int index) {
        return factors.get(index);
    }

    @Override
    public int getSize() {
        int size = 0;

        if (factors != null) {
            size = factors.size();
        }
        return size;
    }
}
