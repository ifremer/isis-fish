/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2020 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.sensitivity.model;

import static org.nuiton.i18n.I18n.t;

import java.awt.Component;
import java.util.HashMap;
import java.util.Map;

import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

import org.apache.commons.lang3.StringUtils;
import org.nuiton.util.Resource;

import fr.ifremer.isisfish.simulator.sensitivity.Domain;
import fr.ifremer.isisfish.simulator.sensitivity.Factor;
import fr.ifremer.isisfish.simulator.sensitivity.FactorGroup;
import fr.ifremer.isisfish.simulator.sensitivity.domain.ContinuousDomain;

/**
 * Factor tree cell renderer.
 *
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class FactorTreeCellRenderer extends DefaultTreeCellRenderer {

    /** serialVersionUID. */
    private static final long serialVersionUID = 1686424876690392268L;

    /** Icon cache. */
    protected Map<String, Icon> iconCache;

    public FactorTreeCellRenderer() {
        iconCache = new HashMap<>();
        iconCache.put("c", Resource.getIcon("/icons/factors/brick_c.png")); // continuous
        iconCache.put("d", Resource.getIcon("/icons/factors/brick_d.png")); // discrete
        iconCache.put("dg", Resource.getIcon("/icons/factors/bricks_d.png")); // group discrete
        iconCache.put("cg", Resource.getIcon("/icons/factors/bricks_c.png")); // group continuous
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value,
            boolean selected, boolean expanded, boolean leaf, int row,
            boolean hasFocus) {

        JLabel c = (JLabel)super.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);

        String icon = null;
        String text = null;
        if (value instanceof Factor) {
            Factor factor = (Factor)value;
            if (value instanceof FactorGroup) {
                FactorGroup factorGroup = (FactorGroup)value;
                if (factorGroup == tree.getModel().getRoot()) { // root
                    // non du facteur group principal
                    text = t("isisfish.sensitivity.factors");
                } else {
                    if (factorGroup.isDiscrete()) {
                        icon = "d"; 
                    }
                    else if (factorGroup.isContinuous()) {
                        icon = "c";
                    }
                    icon +="g"; // group
                    text = factor.getName();
                }
            }
            else {
                Domain domain = factor.getDomain();

                // get factor type
                if (domain instanceof ContinuousDomain) {
                    icon = "c";
                }
                else {
                    icon = "d";
                }

                text = factor.getName();
            }

            // add equation name if any
            if (StringUtils.isNotBlank(factor.getEquationVariableName())) {
                text += "." + factor.getEquationVariableName();
            }
        }
        c.setIcon(iconCache.get(icon));
        c.setText(text);

        return c;
    }
}
