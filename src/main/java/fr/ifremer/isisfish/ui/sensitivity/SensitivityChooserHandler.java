package fr.ifremer.isisfish.ui.sensitivity;

/*
 * #%L
 * ISIS-Fish
 * %%
 * Copyright (C) 1999 - 2019 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.isisfish.IsisFishException;
import fr.ifremer.isisfish.datastore.JavaSourceStorage;
import fr.ifremer.isisfish.datastore.RegionStorage;
import fr.ifremer.isisfish.datastore.SensitivityAnalysisStorage;
import fr.ifremer.isisfish.datastore.SensitivityExportStorage;
import fr.ifremer.isisfish.datastore.StorageChangeEvent;
import fr.ifremer.isisfish.datastore.StorageChangeListener;
import fr.ifremer.isisfish.export.SensitivityExport;
import fr.ifremer.isisfish.simulator.SimulationParameter;
import fr.ifremer.isisfish.simulator.sensitivity.Domain;
import fr.ifremer.isisfish.simulator.sensitivity.Factor;
import fr.ifremer.isisfish.simulator.sensitivity.FactorGroup;
import fr.ifremer.isisfish.simulator.sensitivity.SensitivityAnalysis;
import fr.ifremer.isisfish.simulator.sensitivity.domain.ContinuousDomain;
import fr.ifremer.isisfish.ui.models.common.GenericListModel;
import fr.ifremer.isisfish.ui.models.common.ScriptParametersParamNameRenderer;
import fr.ifremer.isisfish.ui.models.common.ScriptParametersParamValueEditor;
import fr.ifremer.isisfish.ui.models.common.ScriptParametersParamModel;
import fr.ifremer.isisfish.ui.models.common.ScriptParametersParamValueRenderer;
import fr.ifremer.isisfish.ui.sensitivity.model.FactorCardinalityTableModel;
import fr.ifremer.isisfish.ui.widget.editor.ScriptParameterDialog;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.table.DefaultTableModel;
import java.awt.CardLayout;
import java.util.ArrayList;
import java.util.List;

public class SensitivityChooserHandler {

    /** Class logger. */
    private static final Log log = LogFactory.getLog(SensitivityChooserHandler.class);

    protected SensitivityChooserUI sensitivityChooserUI;

    protected StorageChangeListener sensitivityAnalysisListener;
    protected StorageChangeListener sensitivityExportListener;

    public SensitivityChooserHandler(SensitivityChooserUI sensitivityChooserUI) {
        this.sensitivityChooserUI = sensitivityChooserUI;
    }

    /**
     * Return simulation parameters from context.
     * 
     * @return SimulationParameter
     * @deprecated maybe be inherited
     */
    @Deprecated
    protected SimulationParameter getParameters() {
        return sensitivityChooserUI.getContextValue(SimulationParameter.class);
    }
    
    /**
     * Return factor group from context.
     * 
     * @return FactorGroup
     * @deprecated maybe be inherited
     */
    @Deprecated
    protected FactorGroup getFactorGroup() {
        return sensitivityChooserUI.getContextValue(FactorGroup.class);
    }

    protected void initSensitivityAnalysisModel(StorageChangeEvent evt) {
        sensitivityChooserUI.sensitivityAnalysisModel.setElementList(getSensitivityAnalysisNames());
    }

    protected void initAvailableExportListModel(StorageChangeEvent evt) {
        sensitivityChooserUI.availableExportListModel.setElementList(getSensitivityExportNames());
    }

    protected void afterInit() {
        // listener to listen for modification on sensitivity analysis
        sensitivityAnalysisListener = this::initSensitivityAnalysisModel;
        SensitivityAnalysisStorage.addStorageListener(sensitivityAnalysisListener);
        this.initSensitivityAnalysisModel(null);

        // listener to listen for modification on sensitivity exports
        sensitivityExportListener = this::initAvailableExportListModel;
        SensitivityExportStorage.addStorageListener(sensitivityExportListener);
        this.initAvailableExportListModel(null);
    }

    /**
     * Auto select analysis in JComboBox
     * with analysis selected in SimulAction.
     *
     * Used by "reloadOldSimulation" operation.
     */
    public void refreshSelectedSensitivityAnalysis() {
        SensitivityAnalysis sensitivityAnalysis = getSensitivityAnalysis();
        if (sensitivityAnalysis != null) {
            if (log.isDebugEnabled()) {
                log.debug("Refreshing sensitivity analysis list");
            }
            // TODO it's not getSimpleName() here !
            sensitivityChooserUI.fieldSensitivityAnalysisSelect.setSelectedItem(sensitivityAnalysis.getClass().getSimpleName());
        }
    }
    
    protected void sensitivityAnalysisChanged() {

        String sensitivityName = (String)sensitivityChooserUI.fieldSensitivityAnalysisSelect.getSelectedItem();
        if (sensitivityName == null) {
            return;
        }

        // when analysis is changed by refreshSelectedSensitivityAnalysis
        // this event is thrown by build a new Analysis instance
        // and parameter are lost
        // fix it with a small class name test
        SensitivityAnalysis sensitivityAnalysis = getSensitivityAnalysis();
        if (sensitivityAnalysis != null && sensitivityAnalysis.getClass().getSimpleName().equals(sensitivityName)) {
            sensitivityAnalysis = getSensitivityAnalysis();
        } else {
            sensitivityAnalysis = getSensitivityAnalysisInstance(sensitivityName);
        }
        
        // can be null for example if analysis can't be compiled
        if (sensitivityAnalysis != null) {
            setSensitivityAnalysis(sensitivityAnalysis);
            
            CardLayout factorPanelLayout = (CardLayout)sensitivityChooserUI.factorCardinalityPanel.getLayout();
            if (sensitivityAnalysis.canManageCardinality()) {
                factorPanelLayout.show(sensitivityChooserUI.factorCardinalityPanel, "factorCardinalitySupported");
            } else {
                factorPanelLayout.show(sensitivityChooserUI.factorCardinalityPanel, "factorCardinalityNotSupported");
            }

            // update model
            ScriptParametersParamModel parametersTableModel = new ScriptParametersParamModel(sensitivityAnalysis);
            sensitivityChooserUI.simulSensitivityAnalysisParam.setModel(parametersTableModel);
            ScriptParametersParamNameRenderer nameRenderer = new ScriptParametersParamNameRenderer(sensitivityAnalysis);
            sensitivityChooserUI.simulSensitivityAnalysisParam.getColumnModel().getColumn(0).setCellRenderer(nameRenderer);
            ScriptParametersParamValueRenderer valueRenderer = new ScriptParametersParamValueRenderer();
            sensitivityChooserUI.simulSensitivityAnalysisParam.getColumnModel().getColumn(1).setCellRenderer(valueRenderer);
            sensitivityChooserUI.simulSensitivityAnalysisParam.getColumnModel().getColumn(1).setCellEditor(new ScriptParametersParamValueEditor(sensitivityAnalysis));
        }
    }

    public void setFactorCardinalityTableModel() {
        List<Factor> factors = getFactorGroup().getFactors();
        List<Factor> factorsContinue = new ArrayList<>();
        /* get sublist with only Continous factor */
        for (Factor f : factors) {
            Domain domain = f.getDomain();
            if (domain instanceof ContinuousDomain) {
                //ContinuousDomain continuousDomain = (ContinuousDomain)domain;
                factorsContinue.add(f);
            }
        }
        FactorCardinalityTableModel model = new FactorCardinalityTableModel(factorsContinue);
        sensitivityChooserUI.factorCardinality.setModel(model);
    }

    /**
     * Add multiples export.
     *
     * Export name can be duplicated.
     */
    protected void addExports() {
        List<String> exportNames = sensitivityChooserUI.availableSensitivityExports.getSelectedValuesList();
        for (String exportName : exportNames) {
            addSensitivityExport(sensitivityChooserUI, exportName);
        }
        setSensitivityExportListModel();
    }
    
    /**
     * Remove multiple export.
     */
    protected void removeExports() {
        List<SensitivityExport> exports = sensitivityChooserUI.selectedSensitivityExports.getSelectedValuesList();
        for (SensitivityExport export : exports) {
            removeSensitivityExport(export);
        }
        setSensitivityExportListModel();
    }

    public void duplicateExports() {
        List<SensitivityExport> exports = sensitivityChooserUI.selectedSensitivityExports.getSelectedValuesList();
        for (SensitivityExport export : exports) {
            SensitivityExport e = (SensitivityExport)JavaSourceStorage.clone(export);
            getParameters().getSensitivityExport().add(e);
        }
        setSensitivityExportListModel();
    }
    
    /**
     * Set model (refresh export JList).
     */
    public void setSensitivityExportListModel() {
        List<SensitivityExport> exports = getSensitivityExports();
        GenericListModel<SensitivityExport> model = new GenericListModel<>(exports);
        sensitivityChooserUI.selectedSensitivityExports.setModel(model);
    }

    /**
     * When selection change on available export list.
     */
    protected void availableSensitivityExportSelection() {
        sensitivityChooserUI.addExportButton.setEnabled(sensitivityChooserUI.availableSensitivityExports.getSelectedIndices().length != 0);
    }
    
    /**
     * When selection change on choosen export list.
     */
    protected void selectedSensitivityExportSelection() {
        if (log.isDebugEnabled()) {
            log.debug("Sensitivity export selection change"); 
        }
        setExportParams();
        sensitivityChooserUI.removeExportButton.setEnabled(sensitivityChooserUI.selectedSensitivityExports.getSelectedIndices().length != 0);
        sensitivityChooserUI.duplicateExportButton.setEnabled(sensitivityChooserUI.selectedSensitivityExports.getSelectedIndices().length != 0);
    }
    
    /**
     * Fill export parameters names and values in table.
     */ 
    protected void setExportParams() {
        if (sensitivityChooserUI.selectedSensitivityExports.getSelectedIndices().length == 1) {
            SensitivityExport export = sensitivityChooserUI.selectedSensitivityExports.getSelectedValue();
            ScriptParametersParamModel model = new ScriptParametersParamModel(export);
            sensitivityChooserUI.exportParamsTable.setModel(model);
            ScriptParametersParamNameRenderer nameRenderer = new ScriptParametersParamNameRenderer(export);
            sensitivityChooserUI.exportParamsTable.getColumnModel().getColumn(0).setCellRenderer(nameRenderer);
            ScriptParametersParamValueRenderer valueRenderer = new ScriptParametersParamValueRenderer();
            sensitivityChooserUI.exportParamsTable.getColumnModel().getColumn(1).setCellRenderer(valueRenderer);
            ScriptParametersParamValueEditor cellEditor = new ScriptParametersParamValueEditor(export);
            cellEditor.setRegionStorage(sensitivityChooserUI.getContextValue(RegionStorage.class));
            sensitivityChooserUI.exportParamsTable.getColumnModel().getColumn(1).setCellEditor(cellEditor);
        } else {
            sensitivityChooserUI.exportParamsTable.setModel(new DefaultTableModel());
        }
    }
    
    protected List<String> getSensitivityExportNames() {
        List<String> exportNames = SensitivityExportStorage.getSensitivityExportNames();
        List<String> result = new ArrayList<>(exportNames);
        return result;
    }

    public List<SensitivityExport> getSensitivityExports() {
        List<SensitivityExport> result = getParameters().getSensitivityExport();
        return result;
    }

    public void addSensitivityExport(SensitivityChooserUI sensitivityChooserUI, String name) {
        try {
            SensitivityExportStorage storage = SensitivityExportStorage.getSensitivityExport(name);
            SensitivityExport sensitivityExport = storage.getNewInstance();
            
            // add it after autoconfiguration (if enabled)
            sensitivityExport = (SensitivityExport)ScriptParameterDialog.displayConfigurationFrame(sensitivityChooserUI, sensitivityExport);
            if (sensitivityExport != null) {
                getParameters().getSensitivityExport().add(sensitivityExport);
            }
        } catch (IsisFishException e) {
            if (log.isErrorEnabled()) {
                log.error("Can't add sensitivity export", e);
            }
        }
    }

    /**
     * Remove an export.
     * 
     * @param export export to remove
     */
    public void removeSensitivityExport(SensitivityExport export) {
        getParameters().getSensitivityExport().remove(export);
    }
    
    /**
     * Return sensitivity analysis name without .java extension.
     * 
     * @return sensitivity analysis names list
     */
    protected List<String> getSensitivityAnalysisNames() {
        List<String> result = new ArrayList<>();
        for (String r : SensitivityAnalysisStorage.getSensitivityAnalysisNames()) {
            // there is some non java files in sensitivity directory
            if (r.endsWith(".java")) {
                // Remove .java extention
                // for example SensitivityStorage.getRuleName(String)
                result.add(r.substring(0, r.length() - 5));
            }
        }
        return result;
    }

    /**
     * Get current sensitivity calculator instance.
     * @return sensitivity calculator
     */
    public SensitivityAnalysis getSensitivityAnalysis() {
        return getParameters().getSensitivityAnalysis();
    }

    /**
     * Build a new sensitivity calculator instance by his name.
     * 
     * @param name calculator name
     * @return instance
     */
    public SensitivityAnalysis getSensitivityAnalysisInstance(String name) {
        SensitivityAnalysis sensitivityAnalysis = null;
        try {
            SensitivityAnalysisStorage sensitivityStorage = SensitivityAnalysisStorage.getSensitivityAnalysis(name);
            sensitivityAnalysis = sensitivityStorage.getNewInstance();
        } catch (IsisFishException e) {
            if (log.isErrorEnabled()) {
                log.error("Can't set sensitivity analysis", e);
            }
        }
        return sensitivityAnalysis;
    }

    /**
     * Set calculator instance to use.
     * 
     * @param sensitivityAnalysis new instance
     */
    public void setSensitivityAnalysis(SensitivityAnalysis sensitivityAnalysis) {
        getParameters().setSensitivityAnalysis(sensitivityAnalysis);
    }
}
