/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.sensitivity.model;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JTree;
import javax.swing.tree.TreePath;

import fr.ifremer.isisfish.simulator.sensitivity.Factor;
import fr.ifremer.isisfish.simulator.sensitivity.FactorGroup;
import fr.ifremer.isisfish.ui.sensitivity.SensitivityInputUI;

/**
 * Factor tree.
 * 
 * Add drag and drop support.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class FactorTree extends JTree {

    /** serialVersionUID. */
    private static final long serialVersionUID = 1999481128072081972L;

    /**
     * Constructor (add drag and drop).
     * 
     * @param sensitivityInputUI parent ui
     */
    public FactorTree(SensitivityInputUI sensitivityInputUI) {
        // enable drag n drop
        setDragEnabled(true);
        setTransferHandler(new FactorTransferHandler(sensitivityInputUI));
    }

    /**
     * Return selected factor group.
     * 
     * @return selected factor group
     */
    public FactorGroup getSelectedFactorGroup() {
        
        FactorGroup factorGroup = null;
        
        TreePath[] selectedPaths = getSelectionModel().getSelectionPaths();
        if (selectedPaths != null) {
            for (TreePath selectedPath : selectedPaths) {
                if (selectedPath != null) {
                    Object[] pathWay = selectedPath.getPath();
                    
                    if (pathWay[pathWay.length - 1] instanceof FactorGroup) {
                        if (factorGroup == null) {
                            factorGroup = (FactorGroup)pathWay[pathWay.length - 1];
                        }
                        else {
                            // multiselection de factor group
                            // on retourne rien
                            factorGroup = null;
                            break;
                        }
                    }
                }
            }
        }
        
        return factorGroup;
    }
    
    /**
     * Return selected factors.
     * 
     * @return selected factors
     */
    public List<Factor> getSelectedFactors() {
        
        List<Factor> factors = new ArrayList<>();
        
        TreePath[] selectedPaths = getSelectionModel().getSelectionPaths();
        if (selectedPaths != null) {
            for (TreePath selectedPath : selectedPaths) {
                if (selectedPath != null) {
                    Object[] pathWay = selectedPath.getPath();
                    
                    if (!(pathWay[pathWay.length - 1] instanceof FactorGroup)) {
                        factors.add((Factor)pathWay[pathWay.length - 1]);
                    }
                }
            }
        }
        
        return factors;
    }
}
