/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2015 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui;

import javax.swing.JOptionPane;

/**
 * Isis interface modification verifier.
 * 
 * Check that unsaved datas need to be saved.
 * 
 * @author letellier
 * 
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public interface SaveVerifier {

    int YES_OPTION = JOptionPane.YES_OPTION;
    int NO_OPTION = JOptionPane.NO_OPTION;
    int CANCEL_OPTION = JOptionPane.CANCEL_OPTION;

    /**
     * Tell verifier to check for unsaved modification.
     * 
     * @return user int response
     * @see #YES_OPTION
     * @see #NO_OPTION
     * @see #CANCEL_OPTION
     */
    int checkEdit();
}
