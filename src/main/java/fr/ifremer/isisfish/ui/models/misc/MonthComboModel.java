/*
 * #%L
 * IsisFish
 * 
 * $Id: ScriptParametersTableCellEditor.java 4156 2014-12-09 11:27:18Z echatellier $
 * $HeadURL: https://svn.codelutin.com/isis-fish/trunk/src/main/java/fr/ifremer/isisfish/ui/models/common/ScriptParametersTableCellEditor.java $
 * %%
 * Copyright (C) 2015 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.ui.models.misc;

import javax.swing.DefaultComboBoxModel;

import fr.ifremer.isisfish.types.Month;

/**
 * Month combo box model.
 */
public class MonthComboModel extends DefaultComboBoxModel<Month> {

    /** serialVersionUID. */
    private static final long serialVersionUID = -2458250929660180160L;

    @Override
    public Month getElementAt(int index) {
        Month result = null;
        if (index > 0) {
            result = Month.MONTH[index - 1];
        }
        return result;
    }

    @Override
    public int getSize() {
        return Month.MONTH.length + 1;
    }
}
