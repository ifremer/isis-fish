/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2010 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.models.sensitivityanalysis;

import java.awt.Component;
import java.util.HashMap;
import java.util.Map;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;

import fr.ifremer.isisfish.datastore.SensitivityAnalysisStorage;
import fr.ifremer.isisfish.simulator.sensitivity.SensitivityAnalysis;
import fr.ifremer.isisfish.ui.models.RendererHelper;

/**
 * Renderer pour la combo des calculateurs de sensibilités.
 *
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class SensitivityAnalysisComboRenderer extends DefaultListCellRenderer {

    /** serialVersionUID. */
    private static final long serialVersionUID = -4070846632975105788L;

    /** Sensitivity cache. */
    protected Map<String, SensitivityAnalysis> sensitivityAnalysisCache;

    /**
     * Empty constructor.
     */
    public SensitivityAnalysisComboRenderer() {
        sensitivityAnalysisCache = new HashMap<>();
    }

    @Override
    public Component getListCellRendererComponent(JList<?> list, Object value,
            int index, boolean isSelected, boolean cellHasFocus) {

        // this must be used to have alterned highlight rows and default
        // selection color
        JLabel c = (JLabel) super.getListCellRendererComponent(list, value,
                index, isSelected, cellHasFocus);

        String sensitivityName = (String) value;
        String text;

        // c'est tres couteux d'avoir une instance
        // on les met en cache
        if (sensitivityName != null) {
            try {
                SensitivityAnalysis plan = sensitivityAnalysisCache.get(sensitivityName);
                if (plan == null) {
                    SensitivityAnalysisStorage storage = SensitivityAnalysisStorage
                            .getSensitivityAnalysis(sensitivityName);
                    plan = storage.getNewInstance();
                    sensitivityAnalysisCache.put(sensitivityName, plan);
                }
    
                text = RendererHelper.getNameAndDocComboString(sensitivityName, plan.getDescription());
            } catch (Exception e) {
                text = RendererHelper.getNameAndCompComboString(sensitivityName);
            }
            
            c.setText(text);
        }

        return c;
    }
}
