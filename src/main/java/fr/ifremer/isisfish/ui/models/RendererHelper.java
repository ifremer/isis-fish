/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2010 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.models;

import static org.nuiton.i18n.I18n.t;

import org.apache.commons.lang3.StringUtils;

/**
 * Common format utils methods mostly used in renderers.
 *
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class RendererHelper {

    protected static final String DOCUMENTATION_SUBTITLE = "<html><b>%s</b>%s<font color='gray'>%s</font></html>";
    protected static final String COMPILATION_SUBTITLE = "<html><b color='red'>%s</b>%s<font color='red'>%s</font></html>";

    /**
     * Return HTML formatted tooltip.
     * 
     * Use optional documentation (if any) or use @Doc's class doc.
     * 
     * @param annotationDoc the doc to use
     * @return a string nicely print version
     */
    public static String docToString(String annotationDoc) {
        
        String tooltip = null;

        if (StringUtils.isNotBlank(annotationDoc)) {
            StringBuilder sb = new StringBuilder();
            sb.append("<html>");
            sb.append("<p>" + annotationDoc + "</p>");
            sb.append("</html>");
            tooltip = sb.toString();
            
            // replace all \n by <br/>
            tooltip = tooltip.replace("\n", "<br/>");
        }
        return tooltip;
    }

    public static String getNameAndDocListString(String name, String documentation) {
        String result = String.format(DOCUMENTATION_SUBTITLE, name, "<br />", documentation);
        return result;
    }

    public static String getNameAndCompListString(String name) {
        String result = String.format(COMPILATION_SUBTITLE, name, "<br />", t("isisfish.error.compile.documentationerror"));
        return result;
    }
    
    public static String getNameAndDocComboString(String name, String documentation) {
        String result = String.format(DOCUMENTATION_SUBTITLE, name, " - ", documentation);
        return result;
    }

    public static String getNameAndCompComboString(String name) {
        String result = String.format(COMPILATION_SUBTITLE, name, " - ", t("isisfish.error.compile.documentationerror"));
        return result;
    }
}
