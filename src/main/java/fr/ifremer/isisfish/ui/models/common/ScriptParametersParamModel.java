/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2011 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.models.common;

import static org.nuiton.i18n.I18n.t;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.table.AbstractTableModel;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.ifremer.isisfish.IsisFishException;
import fr.ifremer.isisfish.datastore.JavaSourceStorage;

/**
 * Model de la table de definition de parametre d'un script.
 * 
 * Columns :
 * <ul>
 * <li>Parameter name</li>
 * <li>Parameter value</li>
 * <li>Parameter factor (if enabled)</li>
 * </ul>
 *
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class ScriptParametersParamModel extends AbstractTableModel {

    /** Log. */
    private static final Log log = LogFactory.getLog(ScriptParametersParamModel.class);

    /** serialVersionUID. */
    private static final long serialVersionUID = 3169786638868209920L;

    /** Columns names. */
    public final static String[] COLUMN_NAMES = {
        t("isisfish.common.name"),
        t("isisfish.common.value"),
        t("isisfish.common.ellipsis")};

    protected Object script;

    protected List<String> scriptParametersNames;

    /** If {@code true} display optional. */
    protected boolean showFactorColumn;

    public ScriptParametersParamModel() {

    }

    public ScriptParametersParamModel(Object script) {
        this();
        setScript(script);
    }

    public void setShowFactorColumn(boolean showFactorColumn) {
        this.showFactorColumn = showFactorColumn;
        fireTableStructureChanged();
    }

    /**
     * Set new script, and fire changed event.
     * 
     * @param script script
     */
    public void setScript(Object script) {
        this.script = script;
        scriptParametersNames = new ArrayList<>();
        if (script != null) {
            Map<String, Field> ruleParametersNamesAndTypes = JavaSourceStorage.getParameterNamesAndField(script);
            scriptParametersNames.addAll(ruleParametersNamesAndTypes.keySet());
        }
        fireTableDataChanged();
    }

    @Override
    public int getColumnCount() {
        int result = 2;
        if (showFactorColumn) {
            result = 3;
        }
        return result;
    }

    @Override
    public int getRowCount() {
        int result = 0;
        if (scriptParametersNames != null) {
            result = scriptParametersNames.size();
        }
        return result;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        Object result = null;

        String name = scriptParametersNames.get(rowIndex);
        switch (columnIndex) {
        case 0:
        case 2:
            result = name;
            break;
        case 1:
            try {
                result = JavaSourceStorage.getParameterValue(script, name);
            } catch (IsisFishException e) {
                if (log.isErrorEnabled()) {
                    log.debug("Can't get parameters value", e);
                }
            }
            break;
        default:
            throw new IndexOutOfBoundsException("No such column " + columnIndex);
        }

        return result;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {

        Class<?> result;

        switch (columnIndex) {
        case 0:
        case 2:
            result = String.class;
            break;
        case 1:
            result = Object.class;
            break;
        default:
            throw new IndexOutOfBoundsException("No such column " + columnIndex);
        }

        return result;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return COLUMN_NAMES[columnIndex];
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return columnIndex > 0;
    }

    @Override
    public void setValueAt(Object value, int rowIndex, int columnIndex) {

        if (log.isDebugEnabled()) {
            log.debug("Cell edition (column " + columnIndex + ") = " + value);
        }

        String name = scriptParametersNames.get(rowIndex);
        switch (columnIndex) {
        case 1:
            try {
                JavaSourceStorage.setParameterValue(script, name, value);
            } catch (IsisFishException e) {
                if (log.isErrorEnabled()) {
                    log.error("Can't set parameter value", e);
                }
            }
            break;
        case 2:
            // appelé parce qu'on ne peut pas l'empecher
            // pour la colonne Action, 
            break;
        default:
            throw new IndexOutOfBoundsException("Can't edit column " + columnIndex);
        }

    }
}
