/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2010 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.models.optimization;

import java.awt.Component;
import java.util.HashMap;
import java.util.Map;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;

import fr.ifremer.isisfish.datastore.ObjectiveStorage;
import fr.ifremer.isisfish.simulator.Objective;
import fr.ifremer.isisfish.ui.models.RendererHelper;

/**
 * Renderer pour la combo des script d'optimisation.
 *
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class ObjectiveComboRenderer extends DefaultListCellRenderer {

    /** serialVersionUID. */
    private static final long serialVersionUID = -4070846632975105788L;

    /** Objective cache. */
    protected Map<String, Objective> objectiveCache;

    /**
     * Empty constructor.
     */
    public ObjectiveComboRenderer() {
        objectiveCache = new HashMap<>();
    }

    @Override
    public Component getListCellRendererComponent(JList<?> list, Object value,
            int index, boolean isSelected, boolean cellHasFocus) {

        // this must be used to have alterned highlight rows and default
        // selection color
        JLabel c = (JLabel) super.getListCellRendererComponent(list, value,
                index, isSelected, cellHasFocus);

        String objectiveName = (String) value;
        String text;

        // c'est tres couteux d'avoir une instance
        // on les met en cache
        if (objectiveName != null) {
            try {
                Objective objective = objectiveCache.get(objectiveName);
                if (objective == null) {
                    ObjectiveStorage storage = ObjectiveStorage
                            .getObjective(objectiveName);
                    objective = storage.getNewInstance();
                    objectiveCache.put(objectiveName, objective);
                }
    
                text = RendererHelper.getNameAndDocComboString(objectiveName, objective.getDescription());
            } catch (Exception e) {
                text = RendererHelper.getNameAndCompComboString(objectiveName);
            }
            
            c.setText(text);
        }

        return c;
    }
}
