/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2020 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.models.rule;

import static org.nuiton.i18n.I18n.t;

import java.awt.Component;

import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.Resource;

import fr.ifremer.isisfish.IsisFishException;
import fr.ifremer.isisfish.datastore.RuleStorage;
import fr.ifremer.isisfish.rule.Rule;
import fr.ifremer.isisfish.simulator.sensitivity.SensitivityUtils;
import fr.ifremer.isisfish.ui.simulator.RuleChooser;

/**
 * Specific editor for rule parameters.
 *
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class RuleParametersFactorTableCellRenderer extends DefaultTableCellRenderer {

    /** serialVersionUID. */
    private static final long serialVersionUID = -1655744662816030649L;

    /** Class logger. */
    private static Log log = LogFactory.getLog(RuleParametersFactorTableCellRenderer.class);

    protected RuleChooser ruleChooser;

    protected Rule rule;

    public RuleParametersFactorTableCellRenderer(RuleChooser ruleChooser, Rule rule) {
        this.ruleChooser = ruleChooser;
        this.rule = rule;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value,
            boolean isSelected, boolean hasFocus, int row, int column) {

        Component comp = null;

        String paramName = (String)value;
        try {
            Class paramType = RuleStorage.getParameterType(rule, paramName);
            boolean canBeFactor = SensitivityUtils.canBeFactor(paramType);
            if (canBeFactor) {
                JButton button = new JButton(Resource.getIcon("/icons/fatcow/brick_add.png"));
                button.setToolTipText(t("isisfish.sensitivity.ruleparameterfactortip"));
                comp = button;
            }
        } catch (IsisFishException ex) {
            if (log.isWarnEnabled()) {
                log.warn("Can't get value type", ex);
            }
        }

        if (comp == null) {
            comp = super.getTableCellRendererComponent(table, "", isSelected, hasFocus, row, column);
        }

        return comp;
    }

}
