/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2011 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.models.result;

import java.awt.Component;
import java.util.HashMap;
import java.util.Map;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.ifremer.isisfish.datastore.ResultInfoStorage;
import fr.ifremer.isisfish.result.ResultInfo;
import fr.ifremer.isisfish.ui.models.RendererHelper;

/**
 * Renderer pour les listes des noms de resultats.
 *
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class ResultInfoNameListRenderer extends DefaultListCellRenderer {

    /** Class logger. */
    private static final Log log = LogFactory.getLog(ResultInfoNameListRenderer.class);

    /** serialVersionUID. */
    private static final long serialVersionUID = -4070846632975105788L;

    /** Rule cache. */
    protected Map<String, ResultInfo> resultNameCache;

    /**
     * Empty constructor.
     */
    public ResultInfoNameListRenderer() {
        resultNameCache = new HashMap<>();
    }

    @Override
    public Component getListCellRendererComponent(JList<?> list, Object value,
            int index, boolean isSelected, boolean cellHasFocus) {
        
        // this must be used to have alterned highlight rows and default
        // selection color
        JLabel c = (JLabel)super.getListCellRendererComponent(list,value,index, isSelected, cellHasFocus);
        
        String resultNameName = (String)value;
        String text;
        try {
            ResultInfo resultInfo = resultNameCache.get(resultNameName);
            if (resultInfo == null) {
                ResultInfoStorage storage = ResultInfoStorage.getResultInfo(resultNameName);
                resultInfo = storage.getNewInstance();
                resultNameCache.put(resultNameName, resultInfo);
            }

            text = RendererHelper.getNameAndDocListString(resultNameName, resultInfo.getDescription());
        } catch (Exception ex) {
            text = RendererHelper.getNameAndCompListString(resultNameName);
        }

        c.setText(text);

        return c;
    }
}
