package fr.ifremer.isisfish.ui.models.optimization;

/*
 * #%L
 * IsisFish
 * %%
 * Copyright (C) 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import static org.nuiton.i18n.I18n.t;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.table.AbstractTableModel;

import fr.ifremer.isisfish.entities.Observation;
import fr.ifremer.isisfish.export.ExportInfo;

public class ExportObservationTableModel extends AbstractTableModel {

    /** serialVersionUID. */
    private static final long serialVersionUID = 7555677813473489539L;

    /** Optimizations exports and observations. */
    protected Map<ExportInfo, Observation> optimizationExportsObservations;

    /** Indexed keys list. */
    protected List<ExportInfo> keysCache;

    /** Columns names. */
    public final static String[] COLUMN_NAMES = {
        t("isisfish.optimization.export"),
        t("isisfish.optimization.observation")
    };
    
    public ExportObservationTableModel() {
        
    }

    public ExportObservationTableModel(Map<ExportInfo, Observation> optimizationExportsObservations) {
        this();
        setOptimizationExportsObservations(optimizationExportsObservations);
    }

    public void setOptimizationExportsObservations(Map<ExportInfo, Observation> optimizationExportsObservations) {
        this.optimizationExportsObservations = optimizationExportsObservations;
        keysCache = new ArrayList<>(optimizationExportsObservations.keySet());
        fireTableDataChanged();
    }

    public ExportInfo getExportForRow(int line) {
        return keysCache.get(line);
    }

    public void deleteExport(ExportInfo export) {
        int index = keysCache.indexOf(export);
        keysCache.remove(index);
        fireTableRowsDeleted(index, index);
    }

    @Override
    public int getColumnCount() {
        int result = 2;
        return result;
    }

    @Override
    public int getRowCount() {
        int result = 0;
        if (keysCache != null) {
            result = keysCache.size();
        }
        return result;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return COLUMN_NAMES[columnIndex];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        ExportInfo export = keysCache.get(rowIndex);
        Object result = export;
        if (columnIndex == 1) {
            result = optimizationExportsObservations.get(export);
        }
        return result;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return columnIndex > 0;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        ExportInfo export = getExportForRow(rowIndex);
        this.optimizationExportsObservations.put(export, (Observation)aValue);
    }
}
