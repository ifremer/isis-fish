/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2011 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.models.simulationplan;

import java.awt.Component;
import java.util.HashMap;
import java.util.Map;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;

import fr.ifremer.isisfish.datastore.SimulationPlanStorage;
import fr.ifremer.isisfish.simulator.SimulationPlan;
import fr.ifremer.isisfish.ui.models.RendererHelper;
import org.apache.commons.lang3.StringUtils;

/**
 * Renderer pour la combo des plans de simulation.
 *
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class SimulationPlanComboRenderer extends DefaultListCellRenderer {

    /** serialVersionUID. */
    private static final long serialVersionUID = -4070846632975105788L;

    /** Simulation plan cache. */
    protected Map<String, SimulationPlan> simulationPlanCache;

    /**
     * Empty constructor.
     */
    public SimulationPlanComboRenderer() {
        simulationPlanCache = new HashMap<>();
    }

    @Override
    public Component getListCellRendererComponent(JList<?> list, Object value,
            int index, boolean isSelected, boolean cellHasFocus) {

        // this must be used to have alterned highlight rows and default
        // selection color
        JLabel c = (JLabel) super.getListCellRendererComponent(list, value,
                index, isSelected, cellHasFocus);

        String simulationPlanName = (String) value;
        if (StringUtils.isNotBlank(simulationPlanName)) {
            String text;

            // c'est tres couteux d'avoir une instance
            // on les met en cache
            try {
                SimulationPlan plan = simulationPlanCache.get(simulationPlanName);
                if (plan == null) {
                    SimulationPlanStorage storage = SimulationPlanStorage
                            .getSimulationPlan(simulationPlanName);
                    plan = storage.getNewInstance();
                    simulationPlanCache.put(simulationPlanName, plan);
                }

                text = RendererHelper.getNameAndDocComboString(simulationPlanName, plan.getDescription());
            } catch (Exception e) {
                text = RendererHelper.getNameAndCompComboString(simulationPlanName);
            }

            c.setText(text);
        }

        return c;
    }
}
