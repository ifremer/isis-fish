/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2015 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.models.common;

import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

/**
 * Model pour pour la representation d'une liste de type generic.
 * Utilisation dans les {@link JComboBox}.
 *
 * @param <E> generic type
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class GenericComboModel<E> extends DefaultComboBoxModel<E> {

    /** serialVersionUID. */
    private static final long serialVersionUID = -4070846632975105788L;

    /** E list. */
    protected List<E> elementList;

    /**
     * Empty constructor.
     */
    public GenericComboModel() {
        this(null);
    }

    /**
     * Constructor with export list.
     * 
     * @param elementList E list
     */
    public GenericComboModel(List<E> elementList) {
        setElementList(elementList);
    }

    /**
     * Get E list.
     * 
     * @return E list
     */
    public List<E> getElementList() {
        return elementList;
    }

    /**
     * Set E list.
     * 
     * @param elementList E list to set
     */
    public void setElementList(List<E> elementList) {
        // reset selected
        setSelectedItem(null);

        this.elementList = elementList;

        fireContentsChanged(this, 0, elementList == null ? 0 : elementList.size() - 1);
    }

    public void addElement(E elt) {
        elementList.add(elt);
        fireIntervalAdded(this, elementList.size() - 1, elementList.size());
    }

    public boolean containsElement(E elt) {
        return elementList.contains(elt);
    }

    @Override
    public E getElementAt(int index) {
        return elementList.get(index);
    }

    @Override
    public int getSize() {
        int size = 0;

        if (elementList != null) {
            size = elementList.size();
        }
        return size;
    }
}
