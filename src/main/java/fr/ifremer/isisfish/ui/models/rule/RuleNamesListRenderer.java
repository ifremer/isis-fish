/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2011 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.models.rule;

import java.awt.Component;
import java.util.HashMap;
import java.util.Map;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;

import fr.ifremer.isisfish.datastore.RuleStorage;
import fr.ifremer.isisfish.rule.Rule;
import fr.ifremer.isisfish.ui.models.RendererHelper;

/**
 * Renderer pour les listes des noms de regles.
 *
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class RuleNamesListRenderer extends DefaultListCellRenderer {

    /** serialVersionUID. */
    private static final long serialVersionUID = -4070846632975105788L;

    /** Rule cache. */
    protected Map<String, Rule> ruleCache;

    /**
     * Empty constructor.
     */
    public RuleNamesListRenderer() {
        ruleCache = new HashMap<>();
    }

    @Override
    public Component getListCellRendererComponent(JList<?> list, Object value,
            int index, boolean isSelected, boolean cellHasFocus) {
        
        // this must be used to have alterned highlight rows and default
        // selection color
        JLabel c = (JLabel)super.getListCellRendererComponent(list,value,index, isSelected, cellHasFocus);

        /*String ruleName = (String)value;
        String TEMPLATE = "<html><b>%s</b><br /><font color='gray'>%s</font></html>";
        c.setText(String.format(TEMPLATE, ruleName, "test"));

        // c'est tres couteux d'avoir une instance
        // on les met en cache
        try {
            Rule rule = ruleCache.get(ruleName);
            if (rule == null) {
                RuleStorage storage = RuleStorage.getRule(ruleName);
                rule = storage.getNewInstance();
                ruleCache.put(ruleName, rule);
            }

            c.setToolTipText(rule.getDescription());
            c.setForeground(null);
        } catch (Exception e) {
            String errorTooltip = RendererHelper.getErrorTooltip(t("isisfish.error.compile.fileerror", ruleName));
            c.setToolTipText(errorTooltip);
            c.setForeground(Color.RED);
        }*/
        
        String ruleName = (String)value;
        String text;
        try {
            Rule rule = ruleCache.get(ruleName);
            if (rule == null) {
                RuleStorage storage = RuleStorage.getRule(ruleName);
                rule = storage.getNewInstance();
                ruleCache.put(ruleName, rule);
            }

            text = RendererHelper.getNameAndDocListString(ruleName, rule.getDescription());
        } catch (Exception ex) {
            text = RendererHelper.getNameAndCompListString(ruleName);
        }
        c.setText(text);

        return c;
    }
}
