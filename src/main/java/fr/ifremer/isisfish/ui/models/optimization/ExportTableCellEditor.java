/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2011 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.models.optimization;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaContext;

import fr.ifremer.isisfish.datastore.RegionStorage;
import fr.ifremer.isisfish.entities.Observation;

/**
 * Observation editor.
 *
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class ExportTableCellEditor extends AbstractCellEditor implements TableCellEditor, ActionListener {

    /** Class logger. */
    private static Log log = LogFactory.getLog(ExportTableCellEditor.class);

    /** serialVersionUID. */
    private static final long serialVersionUID = -1655744662816030649L;

    protected RegionStorage regionStorage = null;

    protected JComboBox<Observation> editorComponent;

    public void setRegionStorage(RegionStorage regionStorage) {
        this.regionStorage = regionStorage;
    }

    @Override
    public Object getCellEditorValue() {
        return editorComponent.getSelectedItem();
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {

        try {
            if (regionStorage == null) {
                return null;
            }
            TopiaContext context = regionStorage.getStorage().beginTransaction();
            if (context != null) {
                List<Observation> list = context.findAll("from " + Observation.class.getName());
                editorComponent = new JComboBox<>(list.toArray(new Observation[0]));
                editorComponent.setSelectedItem(value);
                editorComponent.setRenderer(new ObservationComboRenderer());
                editorComponent.addActionListener(this);
                context.closeContext();
            }

        } catch (Exception eee) {
            if (log.isWarnEnabled()) {
                log.warn("Can't get entity object for combobox", eee);
            }
        }
        
        return editorComponent;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        stopCellEditing();
    }
}
