/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2011 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.models.common;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import fr.ifremer.isisfish.datastore.JavaSourceStorage;
import fr.ifremer.isisfish.ui.widget.editor.ParameterTableCellEditor;

/**
 * Specific editor for script parameters.
 *
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class ScriptParametersParamValueEditor extends ParameterTableCellEditor {

    /** serialVersionUID. */
    private static final long serialVersionUID = 8211639776194497615L;

    protected Object script;

    protected List<Field> scriptParametersTypes;

    /**
     * Constructor with script parameter.
     * 
     * @param script script
     */
    public ScriptParametersParamValueEditor(Object script) {
        this.script = script;
        scriptParametersTypes = new ArrayList<>();
        Map<String, Field> ruleParametersNamesAndTypes = JavaSourceStorage.getParameterNamesAndField(script);
        scriptParametersTypes.addAll(ruleParametersNamesAndTypes.values());
    }

    @Override
    protected Field getType(Object value, int row, int column) {
        return scriptParametersTypes.get(row);
    }
}
