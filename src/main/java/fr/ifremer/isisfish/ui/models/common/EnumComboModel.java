/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.ui.models.common;

import org.jdesktop.swingx.combobox.EnumComboBoxModel;

/**
 * Just a delegator class to not use external API in Isis.
 *
 * @param <E> enum type
 */
public class EnumComboModel<E extends Enum<E>> extends EnumComboBoxModel<E>{

    /** serialVersionUID. */
    private static final long serialVersionUID = -912859822412819321L;

    public EnumComboModel(Class<E> en) {
        super(en);
    }

}
