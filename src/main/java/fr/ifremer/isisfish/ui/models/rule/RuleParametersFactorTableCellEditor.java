/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2020 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.models.rule;

import static fr.ifremer.isisfish.simulator.SimulationParameterPropertiesHelper.DOT;
import static fr.ifremer.isisfish.simulator.SimulationParameterPropertiesHelper.PARAMETERS_KEY;
import static fr.ifremer.isisfish.simulator.SimulationParameterPropertiesHelper.PARAMETER_KEY;
import static fr.ifremer.isisfish.simulator.SimulationParameterPropertiesHelper.RULE_KEY;
import static org.nuiton.i18n.I18n.t;

import java.awt.Component;

import javax.swing.AbstractCellEditor;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.Resource;

import fr.ifremer.isisfish.IsisFishException;
import fr.ifremer.isisfish.datastore.RuleStorage;
import fr.ifremer.isisfish.rule.Rule;
import fr.ifremer.isisfish.simulator.sensitivity.Factor;
import fr.ifremer.isisfish.simulator.sensitivity.SensitivityUtils;
import fr.ifremer.isisfish.ui.sensitivity.wizard.FactorWizardHandler;
import fr.ifremer.isisfish.ui.sensitivity.wizard.FactorWizardUI;
import fr.ifremer.isisfish.ui.simulator.RuleChooser;

/**
 * Specific editor for rule parameters.
 *
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class RuleParametersFactorTableCellEditor extends AbstractCellEditor
    implements TableCellEditor {

    /** serialVersionUID. */
    private static final long serialVersionUID = -1655744662816030649L;

    private static Log log = LogFactory.getLog(RuleParametersFactorTableCellEditor.class);

    protected RuleChooser ruleChooser;

    protected Rule rule;

    /**
     * Constructor.
     * 
     * Super dependant de l'ui {@link RuleChooser}.
     * 
     * @param ruleChooser rule chooser ui
     * @param rule rule
     */
    public RuleParametersFactorTableCellEditor(RuleChooser ruleChooser, Rule rule) {
        this.ruleChooser = ruleChooser;
        this.rule = rule;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value,
            boolean isSelected, int row, int column) {

        Component comp = null;

        final String paramName = (String)value;
        try {
            Class paramType = RuleStorage.getParameterType(rule, paramName);
            boolean canBeFactor = SensitivityUtils.canBeFactor(paramType);
            if (canBeFactor) {
                JButton button = new JButton(Resource.getIcon("/icons/fatcow/brick_add.png"));
                button.addActionListener(e -> {
                    cancelCellEditing();
                    addRuleParameterFactor(ruleChooser, rule, paramName);
                });
                button.setToolTipText(t("isisfish.sensitivity.ruleparameterfactortip"));
                comp = button;
            }
        } catch (IsisFishException ex) {
            if (log.isWarnEnabled()) {
                log.warn("Can't get value type", ex);
            }
        }

        if (comp == null) {
            comp = new JLabel();
        }

        return comp;
    }

    @Override
    public Object getCellEditorValue() {
        return null;
    }
    
    /**
     * Ajout d'un nouveau facteur sur un parametres de règles.
     * 
     * Le facteur est directement crée avec un nom et un path correct.
     * 
     * @param ruleChooser rule chooser
     * @param rule rule
     * @param paramName rule parameter name
     */
    public void addRuleParameterFactor(RuleChooser ruleChooser, Rule rule, String paramName) {

        // get index of rule in rule list
        // warning, factor path must always be cohérent
        // with rule list, if a rule is deleted, factor on it must
        // be deleted too, et next factor must be renamed
        int index = ruleChooser.getRulesList().indexOf(rule);
        String factorPath = PARAMETERS_KEY + DOT + RULE_KEY + DOT + index + DOT
                + PARAMETER_KEY + DOT + paramName;
        //try {

            /*// on a besoin de la valeur pour savoir s'il peut être continue
            Object paramValue = RuleStorage.getParameterValue(rule, paramName);

            if (!SensitivityUtils.canBeContinue(paramValue)) {
                // dans le cas ou il ne peut pas être continue, on le gere
                // differement et on utilise sont type comme
                // valeur
                Class paramType = RuleStorage.getParameterType(rule, paramName);
                factorPath += "." + paramType.getName();
                paramValue = paramType;
            }

            // init new factor wizard ui
            FactorWizardUI factorWizardUI = new FactorWizardUI(ruleChooser);
            factorWizardUI.getFactorNameField().setText(t("isisfish.sensitivity.ruleparameterfactorname",
                    rule.getClass().getSimpleName(), paramName));
            factorWizardUI.setFactorPath(factorPath);
            SensitivityWizardHandler handler = factorWizardUI.getHandler();
            handler.initNewFactorWithValue(factorWizardUI, paramValue);
            factorWizardUI.pack();
            factorWizardUI.setLocationRelativeTo(ruleChooser);
            factorWizardUI.setVisible(true);*/
            
            Factor factor = new Factor(t("isisfish.sensitivity.ruleparameterfactorname",
                    rule.getClass().getSimpleName(), paramName));
            factor.setPath(factorPath);
            FactorWizardUI factorWizardUI = new FactorWizardUI(ruleChooser);
            FactorWizardHandler handler = factorWizardUI.getHandler();
            handler.initExistingFactor(factorWizardUI, factor);
            factorWizardUI.pack();
            factorWizardUI.setLocationRelativeTo(ruleChooser);
            factorWizardUI.setVisible(true);
            
        /*} catch (IsisFishException ex) {
            throw new IsisFishRuntimeException("Can't add factor on rule", ex);
        }*/
    }
}
