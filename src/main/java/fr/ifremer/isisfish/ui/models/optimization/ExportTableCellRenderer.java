/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2011 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.models.optimization;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import fr.ifremer.isisfish.entities.Observation;
import fr.ifremer.isisfish.export.ExportInfo;

/**
 * ExportInfo renderer for table.
 *
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class ExportTableCellRenderer extends DefaultTableCellRenderer {

    /** serialVersionUID. */
    private static final long serialVersionUID = -1655744662816030649L;

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value,
            boolean isSelected, boolean hasFocus, int row, int column) {

        String stringValue = null;
        String tooltip = null;

        switch (column) {
            case 0:
                ExportInfo export = (ExportInfo)value;
                stringValue =  value.getClass().getSimpleName();
                tooltip = export.getDescription();
                break;
            default:
                Observation observation = (Observation)value;
                if (observation != null) {
                    stringValue = observation.getName();
                    tooltip = observation.getComment();
                }
                break;
        }

        JLabel c = (JLabel) super.getTableCellRendererComponent(table, stringValue, isSelected, hasFocus, row, column);
        c.setToolTipText(tooltip);
        return c;
    }
}
