/* %%Ignore-License
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 TERAI Atsuhiro
 *
 * @see https://github.com/aterai/java-swing-tips/tree/master/CheckedComboBox
 */
package fr.ifremer.isisfish.ui.widget.checkboxcombo;

public class CheckableItem<E> {
    private final E element;
    private boolean selected;

    protected CheckableItem(E element, boolean selected) {
        this.element = element;
        this.selected = selected;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public E getElement() {
        return element;
    }

    @Override
    public String toString() {
        return element.toString();
    }
}
