/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.ui.widget.text;

import org.fife.ui.rsyntaxtextarea.AbstractTokenMakerFactory;

/**
 * Extends rsyntaxtextarea factory to add Isis fish specific style.
 */
public class SyntaxTokenManagerFactory extends AbstractTokenMakerFactory implements IsisSyntaxConstants {

    @Override
    protected void initTokenMakerMap() {
        // isis support for log
        putMapping(SYNTAX_STYLE_LOG, LogTokenMarker.class.getName());
        
        // default code
        String pkg = "org.fife.ui.rsyntaxtextarea.modes.";

        putMapping(SYNTAX_STYLE_NONE,           pkg + "PlainTextTokenMaker");
        putMapping(SYNTAX_STYLE_ACTIONSCRIPT,   pkg + "ActionScriptTokenMaker");
        putMapping(SYNTAX_STYLE_ASSEMBLER_X86,  pkg + "AssemblerX86TokenMaker");
        putMapping(SYNTAX_STYLE_BBCODE,         pkg + "BBCodeTokenMaker");
        putMapping(SYNTAX_STYLE_C,              pkg + "CTokenMaker");
        putMapping(SYNTAX_STYLE_CLOJURE,        pkg + "ClojureTokenMaker");
        putMapping(SYNTAX_STYLE_CPLUSPLUS,      pkg + "CPlusPlusTokenMaker");
        putMapping(SYNTAX_STYLE_CSHARP,         pkg + "CSharpTokenMaker");
        putMapping(SYNTAX_STYLE_CSS,            pkg + "CSSTokenMaker");
        putMapping(SYNTAX_STYLE_DELPHI,         pkg + "DelphiTokenMaker");
        putMapping(SYNTAX_STYLE_DTD,            pkg + "DtdTokenMaker");
        putMapping(SYNTAX_STYLE_FORTRAN,        pkg + "FortranTokenMaker");
        putMapping(SYNTAX_STYLE_GROOVY,         pkg + "GroovyTokenMaker");
        putMapping(SYNTAX_STYLE_HTACCESS,       pkg + "HtaccessTokenMaker");
        putMapping(SYNTAX_STYLE_HTML,           pkg + "HTMLTokenMaker");
        putMapping(SYNTAX_STYLE_JAVA,           pkg + "JavaTokenMaker");
        putMapping(SYNTAX_STYLE_JAVASCRIPT,     pkg + "JavaScriptTokenMaker");
        putMapping(SYNTAX_STYLE_JSON,           pkg + "JsonTokenMaker");
        putMapping(SYNTAX_STYLE_JSP,            pkg + "JSPTokenMaker");
        putMapping(SYNTAX_STYLE_LATEX,          pkg + "LatexTokenMaker");
        putMapping(SYNTAX_STYLE_LISP,           pkg + "LispTokenMaker");
        putMapping(SYNTAX_STYLE_LUA,            pkg + "LuaTokenMaker");
        putMapping(SYNTAX_STYLE_MAKEFILE,       pkg + "MakefileTokenMaker");
        putMapping(SYNTAX_STYLE_MXML,           pkg + "MxmlTokenMaker");
        putMapping(SYNTAX_STYLE_NSIS,           pkg + "NSISTokenMaker");
        putMapping(SYNTAX_STYLE_PERL,           pkg + "PerlTokenMaker");
        putMapping(SYNTAX_STYLE_PHP,            pkg + "PHPTokenMaker");
        putMapping(SYNTAX_STYLE_PROPERTIES_FILE,pkg + "PropertiesFileTokenMaker");
        putMapping(SYNTAX_STYLE_PYTHON,         pkg + "PythonTokenMaker");
        putMapping(SYNTAX_STYLE_RUBY,           pkg + "RubyTokenMaker");
        putMapping(SYNTAX_STYLE_SAS,            pkg + "SASTokenMaker");
        putMapping(SYNTAX_STYLE_SCALA,          pkg + "ScalaTokenMaker");
        putMapping(SYNTAX_STYLE_SQL,            pkg + "SQLTokenMaker");
        putMapping(SYNTAX_STYLE_TCL,            pkg + "TclTokenMaker");
        putMapping(SYNTAX_STYLE_UNIX_SHELL,     pkg + "UnixShellTokenMaker");
        putMapping(SYNTAX_STYLE_VISUAL_BASIC,   pkg + "VisualBasicTokenMaker");
        putMapping(SYNTAX_STYLE_WINDOWS_BATCH,  pkg + "WindowsBatchTokenMaker");
        putMapping(SYNTAX_STYLE_XML,            pkg + "XMLTokenMaker");
    }

}
