/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2010 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.widget.interval;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JComponent;

/**
 * IntervalGraphic.
 * 
 * Created: Wed Oct  4 2000
 *
 * @author poussin
 * @version $Revision$
 * 
 * Mise a jour: $Date$
 * par : $Author$
 */
public class IntervalGraphic extends JComponent implements Observer { // IntervalGraphic

    /** serialVersionUID. */
    private static final long serialVersionUID = -6795996897731533693L;

    /* some constants. */
    private static final int HANDLE_WITH = 5;
    private static final Cursor DEFAULT_CURSOR = new Cursor(Cursor.DEFAULT_CURSOR);
    private static final Cursor W_RESIZE_CURSOR = new Cursor(Cursor.W_RESIZE_CURSOR);
    private static final Cursor E_RESIZE_CURSOR = new Cursor(Cursor.E_RESIZE_CURSOR);
    private static final Cursor MOVE_CURSOR = new Cursor(Cursor.MOVE_CURSOR);

    /* Colors. */
    private Color backColor = Color.BLUE;
    private Color foreColor = Color.YELLOW;
    private Color lineColor = Color.YELLOW.darker();

    protected Interval model;

    /**
     * Rapport entre le model et la vue.
     */
    protected float coef;

    /**
     * Decalage de la position de la souris lors d'un drag par rapport
     * à la position de l'element 1.
     */
    protected float delta;

    //tous pour changer la selection avec la souris
    private boolean inDrag = false;
    private int cursorType = 1;
    //private Cursor mouseCursor;

    //si vrai le composant est editable
    private boolean enable = true;

    class MouseClick extends MouseAdapter {
        /*public void mouseEntered( MouseEvent e) {
            mouseCursor = getCursor();
        }*/
        public void mousePressed(MouseEvent e) {
            int mouseX = e.getX();

            if (model.getFirst() <= model.getLast()) {
                inDrag = (model.getFirst() * coef - HANDLE_WITH < mouseX)
                        && (mouseX < (model.getLast() + 1) * coef + HANDLE_WITH);
            } else if (model.getFirst() > model.getLast()) {
                inDrag = ((0 <= mouseX) && (mouseX < (model.getLast() + 1)
                        * coef + HANDLE_WITH))
                        || ((model.getFirst() * coef - HANDLE_WITH < mouseX) && (mouseX <= getSize().width));
            }
            if (inDrag) {
                delta = mouseX - (model.getFirst() * coef);
            }
        }

        public void mouseReleased(MouseEvent e) {
            inDrag = false;
        }
    }

    class MouseMove extends MouseMotionAdapter {
        public void mouseMoved(MouseEvent e) {
            if (!inDrag && enable) {
                int x = e.getX();
                //on determine sur quel section on est
                if ((model.getFirst() * coef - HANDLE_WITH < x)
                        && (x < model.getFirst() * coef + HANDLE_WITH)) {
                    cursorType = 0;
                    setCursor(W_RESIZE_CURSOR);
                } else if (((model.getLast() + 1) * coef - HANDLE_WITH < x)
                        && (x < (model.getLast() + 1) * coef + HANDLE_WITH)) {
                    cursorType = 2;
                    setCursor(E_RESIZE_CURSOR);
                    delta = x - (model.getLast() * coef);
                } else {
                    boolean bool = ((model.getFirst() <= model.getLast())
                            && (model.getFirst() * coef + HANDLE_WITH < x) && (x < (model
                            .getLast() + 1)
                            * coef - HANDLE_WITH))
                            || ((model.getFirst() > model.getLast()) && (((0 <= x) && (x < (model
                                    .getLast() + 1)
                                    * coef - HANDLE_WITH)) || ((model
                                    .getFirst()
                                    * coef + HANDLE_WITH < x) && (x <= getSize().width))));

                    if (bool) {
                        cursorType = 1;
                        setCursor(MOVE_CURSOR);
                    } else {
                        cursorType = 4;
                        setCursor(DEFAULT_CURSOR);
                    }
                }
            }
        }

        public void mouseDragged(MouseEvent e) {
            if (inDrag && enable) {
                int x = e.getX();
                switch (cursorType) {
                case 0: //on tire par le debut
                    if (x < 0) {
                        x = 0;
                    }
                    if (x > getSize().width) {
                        x = getSize().width;
                    }
                    model.setFirst(Math.round(x / coef));
                    break;
                case 1: //on deplace
                    int decal = Math.round((x - delta) / coef)
                            - model.getFirst();
                    model.move(decal);
                    break;
                case 2: //on tire par la fin
                    if (x < 0) {
                        x = 0;
                    }
                    if (x > getSize().width) {
                        x = getSize().width;
                    }
                    model.setLast(Math.round((x - coef) / coef));
                    break;
                }
            }
        }
    }

    /**
     * Constructor with interval value.
     * 
     * @param m interval
     */
    public IntervalGraphic(Interval m) {
        setModel(m);
        addMouseListener(new MouseClick());
        addMouseMotionListener(new MouseMove());
    }

    public void setModel(Interval m) {
        if (this.model != null) {
            this.model.deleteObserver(this);
        }
        this.model = m;
        this.model.addObserver(this);
        update(this.model, null);
    }

    public void setEnabled(boolean enable) {
        this.enable = enable;
    }

    /**
     * Dessine sur le graphique l'interval courant.
     */
    protected void redraw(Graphics g) {
        coef = (float) getSize().width / (float) (model.getMax() + 1);

        int width = getSize().width;
        int height = getSize().height;

        Image ImageBuffer = createImage(width, height);
        Graphics tmpg = ImageBuffer.getGraphics();

        //peindre le fond
        tmpg.setColor(backColor);
        tmpg.fillRect(0, 0, width, height);

        //peindre la section
        tmpg.setColor(foreColor);
        Integer first = null, last = null;
        for (Interval.IntervalIterator i = (Interval.IntervalIterator) model.iterator(); i.hasNext();) {
            int val = i.nextInt();
            if (first == null) {
                first = val;
            }
            last = val;

            tmpg.setColor(foreColor);
            tmpg.fillRect(Math.round(val * coef), 0, Math.round(coef), height);
            tmpg.setColor(lineColor);
            tmpg.drawRect(Math.round(val * coef), 0, Math.round(coef), height);
        }

        // start
        if (first != null) {
            tmpg.setColor(lineColor);
            tmpg.fillRect(Math.round(first * coef), 0, 5, height);
            tmpg.fillRect(Math.round((last + 1) * coef) - 5, 0, 5, height);
        }

        g.drawImage(ImageBuffer, 0, 0, getSize().width, getSize().height,
                (img, infoflags, x, y, width1, height1) -> true);
    }

    @Override
    public void paint(Graphics g) {
        redraw(g);
    }

    @Override
    public void update(Graphics g) {
        redraw(g);
    }

    /**
     * Called by observable object.
     */
    @Override
    public void update(Observable o, Object arg) {
        repaint();
    }

} // IntervalGraphic

