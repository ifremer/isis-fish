/*
 * #%L
 * ISIS-Fish
 * %%
 * Copyright (C) 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.widget.text;

import javax.swing.text.Segment;

import org.fife.ui.rsyntaxtextarea.AbstractTokenMaker;
import org.fife.ui.rsyntaxtextarea.RSyntaxUtilities;
import org.fife.ui.rsyntaxtextarea.Token;
import org.fife.ui.rsyntaxtextarea.TokenMap;

/**
 * Token marker for Isis Log files.
 */
public class LogTokenMarker extends AbstractTokenMaker {

    @Override
    public Token getTokenList(Segment text, int initialTokenType, int startOffset) {
        resetTokenList();

        char[] array = text.array;
        int offset = text.offset;
        int count = text.count;
        int end = offset + count;

        // See, when we find a token, its starting position is always of the form:
        // 'startOffset + (currentTokenStart-offset)'; but since startOffset and
        // offset are constant, tokens' starting positions become:
        // 'newStartOffset+currentTokenStart' for one less subtraction operation.
        int newStartOffset = startOffset - offset;

        int currentTokenStart = offset;
        int currentTokenType = initialTokenType;

//beginning:
        for (int i=offset; i<end; i++) {

            char c = array[i];

            switch (currentTokenType) {

                case Token.NULL:

                    currentTokenStart = i;  // Starting a new token here.

                    switch (c) {

                        case ' ':
                        case '\t':
                            currentTokenType = Token.WHITESPACE;
                            break;

                        case '"':
                            currentTokenType = Token.ERROR_STRING_DOUBLE;
                            break;

                        // The "separators".
                        case '|':
                            addToken(text, currentTokenStart,i, Token.SEPARATOR, newStartOffset+ currentTokenStart);
                            currentTokenType = Token.NULL;
                            break;

                        default:

                            // Just to speed things up a tad, as this will usually be the case (if spaces above failed).
                            if (RSyntaxUtilities.isLetterOrDigit(c) || c=='\\') {
                                currentTokenType = Token.IDENTIFIER;
                                break;
                            }

                            
                            currentTokenType = Token.IDENTIFIER;
                            break;

                    } // End of switch (c).

                    break;

                case Token.WHITESPACE:

                    switch (c) {

                        case ' ':
                        case '\t':
                            break;  // Still whitespace.

                        case '"':
                            addToken(text, currentTokenStart,i-1, Token.WHITESPACE, newStartOffset + currentTokenStart);
                            currentTokenStart = i;
                            currentTokenType = Token.ERROR_STRING_DOUBLE;
                            break;

                        // The "separators".
                        case '|':
                            addToken(text, currentTokenStart,i-1, Token.WHITESPACE, newStartOffset + currentTokenStart);
                            addToken(text, i,i, Token.SEPARATOR, newStartOffset+i);
                            currentTokenType = Token.NULL;
                            break;

                        default:    // Add the whitespace token and start anew.

                            addToken(text, currentTokenStart,i-1, Token.WHITESPACE, newStartOffset + currentTokenStart);
                            currentTokenStart = i;

                            // Just to speed things up a tad, as this will usually be the case (if spaces above failed).
                            if (RSyntaxUtilities.isLetterOrDigit(c) || c=='\\') {
                                currentTokenType = Token.IDENTIFIER;
                                break;
                            }

                            
                            currentTokenType = Token.IDENTIFIER;

                    } // End of switch (c).

                    break;

                default: // Should never happen
                case Token.IDENTIFIER:

                    switch (c) {

                        case ' ':
                        case '\t':
                            // Check for REM comments.
                            if (i - currentTokenStart == 3 &&
                                (array[i-3]=='r' || array[i-3]=='R') &&
                                (array[i-2]=='e' || array[i-2]=='E') &&
                                (array[i-1]=='m' || array[i-1]=='M')) {
                                    currentTokenType = Token.COMMENT_EOL;
                                    break;
                            }
                            addToken(text, currentTokenStart,i-1, Token.IDENTIFIER, newStartOffset+ currentTokenStart);
                            currentTokenStart = i;
                            currentTokenType = Token.WHITESPACE;
                            break;

                        case '"':
                            addToken(text, currentTokenStart,i-1, Token.IDENTIFIER, newStartOffset+ currentTokenStart);
                            currentTokenStart = i;
                            currentTokenType = Token.ERROR_STRING_DOUBLE;
                            break;

                        // The "separators".
                        case '|':
                            addToken(text, currentTokenStart,i-1, Token.IDENTIFIER, newStartOffset+ currentTokenStart);
                            addToken(text, i,i, Token.SEPARATOR, newStartOffset+i);
                            currentTokenType = Token.NULL;
                            break;


                        default:

                            // Just to speed things up a tad, as this will usually be the case.
                            if (RSyntaxUtilities.isLetterOrDigit(c) || c=='\\') {
                                break;
                            }

                            // Otherwise, fall through and assume we're still okay as an IDENTIFIER...

                    } // End of switch (c).

                    break;

                case Token.COMMENT_EOL:
                    i = end - 1;
                    addToken(text, currentTokenStart,i, Token.COMMENT_EOL, newStartOffset+ currentTokenStart);
                    // We need to set token type to null so at the bottom we don't add one more token.
                    currentTokenType = Token.NULL;
                    break;

                case Token.PREPROCESSOR: // Used for labels
                    i = end - 1;
                    addToken(text, currentTokenStart,i, Token.PREPROCESSOR, newStartOffset+ currentTokenStart);
                    // We need to set token type to null so at the bottom we don't add one more token.
                    currentTokenType = Token.NULL;
                    break;

                case Token.ERROR_STRING_DOUBLE:

                    if (c=='"') {
                        addToken(text, currentTokenStart,i, Token.LITERAL_STRING_DOUBLE_QUOTE, newStartOffset+ currentTokenStart);
                        currentTokenStart = i + 1;
                        currentTokenType = Token.NULL;
                    }
                    // Otherwise, we're still an unclosed string...

                    break;

            } // End of switch (currentTokenType).

        } // End of for (int i=offset; i<end; i++).

        // Deal with the (possibly there) last token.
        if (currentTokenType != Token.NULL) {

                // Check for REM comments.
                if (end- currentTokenStart ==3 &&
                    (array[end-3]=='r' || array[end-3]=='R') &&
                    (array[end-2]=='e' || array[end-2]=='E') &&
                    (array[end-1]=='m' || array[end-1]=='M')) {
                        currentTokenType = Token.COMMENT_EOL;
                }

                addToken(text, currentTokenStart,end-1, currentTokenType, newStartOffset+ currentTokenStart);
        }

        addNullToken();

        // Return the first token in our linked list.
        return firstToken;

    }

    @Override
    public TokenMap getWordsToHighlight() {
        TokenMap tokenMap = new TokenMap();

        tokenMap.put("FATAL", Token.RESERVED_WORD_2);
        tokenMap.put("ERROR", Token.RESERVED_WORD_2);
        tokenMap.put("INFO", Token.RESERVED_WORD);
        tokenMap.put("DEBUG", Token.RESERVED_WORD);
        tokenMap.put("TRACE", Token.RESERVED_WORD);

        return tokenMap;
    }
    
    @Override
    public void addToken(Segment segment, int start, int end, int tokenType, int startOffset) {

        switch (tokenType) {
            // Since reserved words, functions, and data types are all passed
            // into here as "identifiers," we have to see what the token
            // really is...
            case Token.IDENTIFIER:
                int value = wordsToHighlight.get(segment, start,end);
                if (value!=-1)
                    tokenType = value;
                break;
        }

        super.addToken(segment, start, end, tokenType, startOffset);

    }

}
