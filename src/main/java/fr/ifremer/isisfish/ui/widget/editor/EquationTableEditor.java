/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2010 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.widget.editor;

import java.awt.Component;
import java.awt.Window;
import java.util.EventObject;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.TableCellEditor;

import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.entities.Equation;
import fr.ifremer.isisfish.ui.input.equation.EquationEditorPaneUI;

/**
 * Equation table cell editor.
 *
 * Created: 23 mars 2004
 *
 * @author Benjamin Poussin &lt;poussin@codelutin.com&gt;
 * @version $Revision$
 *
 * Mise a jour: $Date$
 * par : $Author$
 */
public class EquationTableEditor extends JButton implements TableCellEditor { // EquationTableEditor

    /** serialVersionUID. */
    private static final long serialVersionUID = -2483612426979170213L;

    protected Set<CellEditorListener> listeners = new HashSet<>();

    protected EquationEditorPaneUI frame = null;

    protected Equation equation = null;

    public EquationTableEditor() {
        addActionListener(e -> {
            Window editorFrame = getFrame();
            editorFrame.setLocationByPlatform(true);
            editorFrame.setVisible(true);
        });
    }

    protected EquationEditorPaneUI getFrame() {
        if (frame == null) {
            frame = new EquationEditorPaneUI();
            frame.getOkButton().addActionListener(e -> stopCellEditing());
            frame.getCancelButton().addActionListener(e -> cancelCellEditing());
        }
        return frame;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value,
            boolean isSelected, int row, int column) {
        equation = (Equation) value;

        this.setText(equation.getName());
        try {
            getFrame().getHandler().setEquation(equation.getCategory(), equation.getName(),
                    equation.getJavaInterface(), equation.getContent());
        } catch (Exception eee) {
            throw new IsisFishRuntimeException(
                    "Can't create file to edit equation", eee);
        }
        return this;
    }

    @Override
    public void addCellEditorListener(CellEditorListener l) {
        listeners.add(l);
    }

    @Override
    public void removeCellEditorListener(CellEditorListener l) {
        listeners.remove(l);
    }

    @Override
    public Object getCellEditorValue() {
        if (equation != null) {
            String content = getFrame().getEditor().getText();
            equation.setContent(content);
        }
        return equation;
    }

    @Override
    public boolean isCellEditable(EventObject anEvent) {
        return true;
    }

    @Override
    public boolean shouldSelectCell(EventObject anEvent) {
        return true;
    }

    @Override
    public void cancelCellEditing() {
        ChangeEvent e = new ChangeEvent(this);
        for (CellEditorListener l : listeners) {
            l.editingCanceled(e);
        }

        if (frame != null) {
            frame.dispose();
        }
    }

    @Override
    public boolean stopCellEditing() {
        ChangeEvent e = new ChangeEvent(this);
        for (CellEditorListener l : listeners) {
            l.editingStopped(e);
        }

        if (frame != null) {
            frame.dispose();
        }
        return true;
    }

} // EquationTableEditor
