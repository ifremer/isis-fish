/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2012 - 2020 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.widget.filterable;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXTextField;
import org.nuiton.util.Resource;

import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * A filterable combobox using an additional text field and reset button.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class FilterableComboBox<T> extends JPanel implements ListDataListener {

    private static final Log log = LogFactory.getLog(FilterableComboBox.class);

    protected JComboBox<T> dataBox;

    protected JTextField filterField;

    protected JLabel resetButton;

    protected JLabel searchButton;

    protected List<ActionListener> actionListeners;

    protected boolean filtering = false;

    public FilterableComboBox() {
        actionListeners = new ArrayList<>();
        buildLayout();
    }

    public FilterableComboBox(ComboBoxModel<T> model) {
        this();
        setModel(model);
    }

    class FilterComboBoxModel extends AbstractListModel<T> implements ComboBoxModel<T> {

        protected ComboBoxModel<T> model;

        protected List<Integer> realIndexes = new ArrayList<>();

        protected Object selectedItem;

        public FilterComboBoxModel(ComboBoxModel<T> model) {
            this.model = model;
            fireDataChanged();
        }

        public ComboBoxModel<T> getModel() {
            return model;
        }

        @Override
        public int getSize() {
            int size = realIndexes.size();
            return size;
        }

        @Override
        public T getElementAt(int index) {
            int realIndex = realIndexes.get(index);
            T element = model.getElementAt(realIndex);
            return element;
        }

        public void fireDataChanged() {
            filtering = true;

            if (log.isDebugEnabled()) {
                log.debug("Refreshing real indexes list");
            }

            // filter list
            List<Integer> newRealIndexes = new ArrayList<>();
            for (int i = 0; i < model.getSize(); ++i) {
                Object element = model.getElementAt(i);
                if (element.toString().contains(filterField.getText())) {
                    newRealIndexes.add(i);
                }
            }
            realIndexes = newRealIndexes; // prevent thread concurrency

            fireIntervalRemoved(this, 0, model.getSize());
            fireIntervalAdded(this, 0, realIndexes.size());
            filtering = false;
        }

        @Override
        public void setSelectedItem(Object anItem) {
            this.selectedItem = anItem;
        }

        @Override
        public Object getSelectedItem() {
            return selectedItem;
        }
    }

    class FilterDocumentListener implements DocumentListener {
        @Override
        public void insertUpdate(DocumentEvent e) {
            updateFilter();
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            updateFilter();
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
            updateFilter();
        }

        protected void updateFilter() {
            ((FilterComboBoxModel) dataBox.getModel()).fireDataChanged();
            resetButton.setEnabled(!filterField.getText().isEmpty());
        }
    }

    /**
     * Build layout.
     */
    private void buildLayout() {

        dataBox = new JComboBox<>();
        dataBox.setName("filterBox");
        dataBox.addActionListener(e -> {
            if (!filtering) {
                for (ActionListener a : actionListeners) {
                    a.actionPerformed(e);
                }
            }
        });

        JPanel addonPanel = new JPanel();
        CardLayout mgr = new PageViewer();
        addonPanel.setLayout(mgr);
        searchButton = new JLabel(Resource.getIcon("/icons/fatcow/magnifier.png"));
        searchButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                mgr.show(addonPanel, "on");
                filterField.requestFocus();
            }
        });
        addonPanel.add(searchButton, "off");

        JPanel filterPanel = new JPanel(new BorderLayout());
        filterField = new JXTextField(t("isisfish.common.filter"));
        filterField.setName("filterText");
        filterField.getDocument().addDocumentListener(new FilterDocumentListener());
        filterField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                if (StringUtils.isBlank(filterField.getText())) {
                    mgr.show(addonPanel, "off");
                }
            }
        });
        filterField.setPreferredSize(new Dimension(200, 0)); // fix size

        filterPanel.add(filterField, BorderLayout.CENTER);
        resetButton = new JLabel(Resource.getIcon("/icons/fatcow/cancel.png"));
        resetButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                filterField.setText("");
                mgr.show(addonPanel, "off");
            }
        });
        resetButton.setEnabled(false);
        resetButton.setName("filterReset");
        filterPanel.add(resetButton, BorderLayout.EAST);
        addonPanel.add(filterPanel, "on");

        setLayout(new BorderLayout());
        add(dataBox, BorderLayout.CENTER);
        add(addonPanel, BorderLayout.EAST);
    }

    /**
     * Change model.
     * 
     * @param model model to set
     */
    public void setModel(ComboBoxModel<T> model) {
        model.addListDataListener(this);
        dataBox.setModel(new FilterComboBoxModel(model));
    }

    public ComboBoxModel<T> getModel() {
        return ((FilterComboBoxModel)dataBox.getModel()).getModel();
    }

    public Object getSelectedItem() {
        return dataBox.getSelectedItem();
    }

    public void setSelectedItem(Object object) {
        dataBox.setSelectedItem(object);
    }

    public boolean addActionListener(ActionListener e) {
        return actionListeners.add(e);
    }

    public boolean removeActionListener(ActionListener o) {
        return actionListeners.remove(o);
    }

    @Override
    public void intervalAdded(ListDataEvent e) {
        if (!filtering) {
            if (log.isDebugEnabled()) {
                log.debug("intervalAdded : fireDataChanged");
            }
            ((FilterComboBoxModel) dataBox.getModel()).fireDataChanged();
        }
    }

    @Override
    public void intervalRemoved(ListDataEvent e) {
        if (!filtering) {
            if (log.isDebugEnabled()) {
                log.debug("intervalRemoved : fireDataChanged");
            }
            ((FilterComboBoxModel) dataBox.getModel()).fireDataChanged();
        }
    }

    @Override
    public void contentsChanged(ListDataEvent e) {
        if (!filtering) {
            if (log.isDebugEnabled()) {
                log.debug("contentsChanged : fireDataChanged");
            }
            ((FilterComboBoxModel) dataBox.getModel()).fireDataChanged();
        }
    }
}
