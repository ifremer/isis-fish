/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2010 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.widget.interval;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JPanel;

/**
 * Widget permettant d'editer graphiquement un interval.
 * 
 * Created: Wed Oct  4 2000
 *
 * @author poussin
 * @version $Revision$
 * 
 * Mise a jour: $Date$
 * par : $Author$
 */
public class IntervalPanel extends JPanel implements Observer {

    /** serialVersionUID. */
    private static final long serialVersionUID = 4245022583233407638L;

    private final static int DEFAULT_WIDTH = 110;
    private final static int DEFAULT_HEIGHT = 50;

    private Interval model = null;
    private IntervalGraphic graph;
    private IntervalLabel label;

    public IntervalPanel() {
        this(null);
    }

    public IntervalPanel(Interval model) {
        this(model, DEFAULT_WIDTH, DEFAULT_HEIGHT);
    }

    public IntervalPanel(Interval model, int width, int height) {
        if (model == null) {
            model = new Interval(0, 0);
        }
        this.model = model;

        setPreferredSize(new Dimension(width, height));
        this.setLayout(new GridLayout(2, 1));

        graph = new IntervalGraphic(this.model);
        label = new IntervalLabel(this.model);
        label.setVisible(true);
        graph.setVisible(true);
        this.add(label);
        this.add(graph);
    }

    public Interval getModel() {
        return model;
    }

    public void setModel(Interval model) {
        if (this.model != null) {
            this.model.deleteObserver(this);
        }
        this.model = model;
        model.addObserver(this);

        graph.setModel(model);
        label.setModel(model);
    }

    public void setEnabled(boolean enable) {
        graph.setEnabled(enable);
        label.setEnabled(enable);
    }

    public void update(Observable o, Object arg) {
        if (arg instanceof String) {
            String props = (String) arg;
            if (props.contains("min")) {
                firePropertyChange("min", null, getModel().getMin());
            }
            if (props.contains("max")) {
                firePropertyChange("max", null, getModel().getMax());
            }
            if (props.contains("first")) {
                firePropertyChange("first", null, getModel().getFirst());
            }
            if (props.contains("last")) {
                firePropertyChange("last", null, getModel().getLast());
            }
        }
    }

    public void setLabelRenderer(Object[] renderArray) {
        label.setLabelRenderer(renderArray);
    }

}
