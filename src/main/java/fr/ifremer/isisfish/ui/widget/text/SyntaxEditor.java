package fr.ifremer.isisfish.ui.widget.text;

/*
 * #%L
 * ISIS-Fish
 * %%
 * Copyright (C) 1999 - 2020 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import static org.nuiton.i18n.I18n.t;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.nuiton.util.FileUtil;

/**
 * Add custom behavior to RSyntaxEditor UI.
 * 
 * Like :
 * <ul>
 * <li>copy/paste</li>
 * <li>file change detection</li>
 * </ul>
 */
public class SyntaxEditor extends SyntaxEditorUI implements DocumentListener {

    /** serialVersionUID. */
    private static final long serialVersionUID = 8010988112139944408L;

    /** class logger */
    private static final Log log = LogFactory.getLog(SyntaxEditor.class);

    /** Will ask user to perform save on close. */
    protected boolean askIfNotSaved = false;

    /** Currently edited file. */
    protected File file;

    /** Flag is file has been modified. */
    protected transient boolean modified;

    /** Timestamp when saved has been performed from editor. */
    protected transient long lastSaveTimestamp;

    /** Save action instance. */
    protected Action saveAction;

    /** Single watch service instance. */
    protected WatchService watchService;

    /** Watch service thread. */
    protected Thread watchThread;

    /** Ask reload file panel (not null if displayed). */
    protected JPanel askReloadPanel;

    public SyntaxEditor() {

        // save action
        int c = getToolkit().getMenuShortcutKeyMask();
        saveAction = new AbstractAction(t("isisfish.editor.save"), new ImageIcon(getClass().getResource("/icons/fatcow/diskette.png"))) {
            public void actionPerformed(ActionEvent e) {
                save();
            }
        };
        saveAction.setEnabled(false);
        saveAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_S, c));

        // autocomplete support
        setAutocomplete();

        // to listen for user modification (after setAutocomplete)
        textArea.getDocument().addDocumentListener(this);
    }

    /**
     * Autocomplete support is not perfect yet and don't support partial classes (equation).
     */
    protected void setAutocomplete() {

        /*LanguageSupportFactory lsf = LanguageSupportFactory.get();
        LanguageSupport support = lsf.getSupportFor(SyntaxConstants.SYNTAX_STYLE_JAVA);
        JavaLanguageSupport jls = (JavaLanguageSupport)support;
        try {
           jls.getJarManager().addCurrentJreClassFileSource();
        } catch (IOException ioe) {
           ioe.printStackTrace();
        }
        lsf.register(textArea);

        ToolTipManager.sharedInstance().registerComponent(textArea);
        CompletionProvider provider = new DefaultCompletionProvider();
        AutoCompletion ac = new AutoCompletion(provider);
        ac.install(textArea);*/
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            watchService.close();
        } finally {
            super.finalize();
        }
    }

    public boolean isAskIfNotSaved() {
        return askIfNotSaved;
    }

    public void setAskIfNotSaved(boolean askIfNotSaved) {
        this.askIfNotSaved = askIfNotSaved;
    }

    protected void setModified(boolean modified) {
        this.modified = modified;
        saveAction.setEnabled(modified);
    }

    public boolean isModified() {
        return modified;
    }
    
    public void addDocumentListener(DocumentListener listener) {
        textArea.getDocument().addDocumentListener(listener);
    }
    
    public void removeDocumentListener(DocumentListener listener) {
        textArea.getDocument().removeDocumentListener(listener);
    }

    public boolean open(File file) {
        // try to save previous file if necessary
        boolean result = askAndSaveOrCancel();

        if (result) {
            
            clearReloadPanel();

            try (Reader in = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8))) {

                String ext = FileUtil.extension(file);
                if ("java".equalsIgnoreCase(ext)) {
                    textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVA);
                } else if ("xml".equalsIgnoreCase(ext)) {
                    textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_XML);
                } else if ("sql".equalsIgnoreCase(ext)) {
                    textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_SQL);
                } else if ("r".equalsIgnoreCase(ext)) {
                    textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_PERL);
                } else if ("log".equalsIgnoreCase(ext)) {
                    textArea.setSyntaxEditingStyle(IsisSyntaxConstants.SYNTAX_STYLE_LOG);
                    textArea.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
                }
                
                textArea.read(in, null);
                textArea.setCaretPosition(0);

                setFile(file); // after textArea.read
            } catch (Exception eee) {
                if (log.isWarnEnabled()) {
                    log.warn("Can't read file", eee);
                }
            }
        }
        
        return result;
    }

    protected boolean askAndSaveOrCancel() {
        boolean result = true;
        if (isAskIfNotSaved() && isModified()) {
            int val = JOptionPane.showConfirmDialog(this,
                    t("isisfish.editor.saveorcancel.message", file.getName()),
                    t("isisfish.editor.saveorcancel.title"),
                    JOptionPane.YES_NO_CANCEL_OPTION,
                    JOptionPane.QUESTION_MESSAGE);
            switch (val) {
            case JOptionPane.YES_OPTION:
                save();
                result = true;
                break;
            case JOptionPane.NO_OPTION:
                result = true;
                break;
            case JOptionPane.CANCEL_OPTION:
                result = false;
                break;
            }
        }
        return result;
    }

    public boolean close() {
        boolean result = askAndSaveOrCancel();
        setFile(null);
        return result;
    }
    
    protected void setFile(File file) {
        // XXX maybe remove old file watch service ?
        this.file = file;

        setEnabled(file != null);
        setModified(false);

        // detect external changes
        if (file != null) {
            detectExternalChanges(file);
        }
    }

    public void save() {
        clearReloadPanel();

        try (Writer out = new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8)) {
            lastSaveTimestamp = System.currentTimeMillis();

            textArea.write(out);
            setModified(false);
        } catch (IOException eee) {
            if (log.isWarnEnabled()) {
                log.warn("Can't save file", eee);
            }
        }
    }
    
    public String getText() {
        return textArea.getText();
    }
    
    public void copy() {
        textArea.copy();
    }
    
    public void paste() {
        textArea.paste();
    }
    
    public void cut() {
        textArea.cut();
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        setModified(true);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        setModified(true);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        setModified(true);
    }
    
    public Action getSaveAction() {
        return saveAction;
    }
    
    protected void clearReloadPanel() {
        if (askReloadPanel != null) {
            hideBottomComponent();
            askReloadPanel = null;
        }
    }

    protected void detectExternalChanges(File file) {
        if (askIfNotSaved) {
            try {
                if (watchService == null) {
                    watchService = FileSystems.getDefault().newWatchService();
                }
                if (watchThread == null) {
                    watchThread = new WatchThread();
                    watchThread.start();
                }
    
                // register new file to watch service
                Path filePath = file.toPath();
                Path parentPath = filePath.getParent();
                parentPath.register(watchService, StandardWatchEventKinds.ENTRY_MODIFY, StandardWatchEventKinds.ENTRY_CREATE);
            } catch (IOException ex) {
                if (log.isErrorEnabled()) {
                    log.error("Can't listen for file", ex);
                }
            }
        }
    }
    
    protected void notifyFileChanged(final File file) {
        if (askReloadPanel == null && file.equals(this.file)) {

            // check to not display self save
            if (file.lastModified() - lastSaveTimestamp < 1000) {
                return;
            }

            setModified(true); // to allow erasure

            askReloadPanel = new JPanel(new BorderLayout());
            
            askReloadPanel.setBackground(new Color(242, 242, 189)); // dark yellow
            
            askReloadPanel.add(new JLabel(t("isisfish.editor.reloadExternal"),
                    new ImageIcon(getClass().getResource("/icons/fatcow/error.png")), JLabel.LEFT), BorderLayout.CENTER);
            
            JPanel actionsPanel = new JPanel(new FlowLayout());
            actionsPanel.setBackground(new Color(242, 242, 189)); // dark yellow
            actionsPanel.add(new JButton(new AbstractAction(t("isisfish.editor.reload")) {
                @Override
                public void actionPerformed(ActionEvent e) {
                    setModified(false); // to never ask
                    open(file);
                }
            }));
            actionsPanel.add(new JButton(new AbstractAction(t("isisfish.editor.cancel")) {
                @Override
                public void actionPerformed(ActionEvent e) {
                    clearReloadPanel();
                }
            }));
            
            askReloadPanel.add(actionsPanel, BorderLayout.EAST);
            addBottomComponent(askReloadPanel);
            showBottomComponent(askReloadPanel);
        }
    }

    class WatchThread extends Thread {
        public void run() {
            try {
                while (true) {
                    final WatchKey wk = watchService.take();
                    for (WatchEvent<?> event : wk.pollEvents()) {
                        //we only register "ENTRY_MODIFY" so the context is always a Path.
                        Path changed = (Path)event.context();
                        Path watchPath = (Path)wk.watchable();
                        File fullFile = new File(watchPath.toFile(), changed.toString());
                        notifyFileChanged(fullFile);
                    }
                    // reset the key
                    boolean valid = wk.reset();
                    if (!valid) {
                        if (log.isErrorEnabled()) {
                            log.error("Watch service unregistered");
                        }
                    }
                }
            } catch (InterruptedException ex) {
                if (log.isErrorEnabled()) {
                    log.error("Thread interrupted", ex);
                }
            }
        }
    }
}
