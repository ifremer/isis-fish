/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2020 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.widget.editor;

import fr.ifremer.isisfish.datastore.RegionStorage;
import fr.ifremer.isisfish.entities.FisheryRegion;
import fr.ifremer.isisfish.simulator.sensitivity.Factor;
import fr.ifremer.isisfish.types.Month;
import fr.ifremer.isisfish.types.TimeStep;
import fr.ifremer.isisfish.ui.models.common.GenericComboModel;
import fr.ifremer.isisfish.ui.widget.checkboxcombo.CheckableItem;
import fr.ifremer.isisfish.ui.widget.checkboxcombo.CheckedComboBox;
import fr.ifremer.isisfish.ui.widget.checkboxcombo.CheckedComboUtil;
import fr.ifremer.isisfish.ui.widget.editor.FactorEditorListener.FactorCallback;
import fr.ifremer.isisfish.util.converter.ConverterUtil;
import org.apache.commons.beanutils.ConvertUtilsBean;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.persistence.TopiaEntity;

import javax.swing.AbstractCellEditor;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellEditor;
import java.awt.Component;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * Un editeur generic de champs qui se base sur le model de données
 * a editer.
 *
 * Created: 25 sept. 06 12:35:21
 *
 * @author poussin
 * @author chemit
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class ParameterTableCellEditor extends AbstractCellEditor implements TableCellEditor {

    private static final long serialVersionUID = 6860330126841984303L;

    /** Class logger. */
    private static final Log log = LogFactory.getLog(ParameterTableCellEditor.class);

    protected RegionStorage regionStorage = null;

    protected TopiaContext topiaContext = null;

    protected Component editor = null;

    protected Field field = null;

    protected FactorEditorListener factorActionListener;

    private enum TypeClassMapping {
        /** for simple int. */
        INT(int.class, Integer.class),
        /** for a topia entity. */
        TOPIA(TopiaEntity.class),
        /** for a month. */
        MONTH(Month.class),
        /** for a simple date. */
        TIMESTEP(TimeStep.class),
        /** for a simple boolean. */
        DOUBLE(double.class, Double.class),
        /** for a simple boolean. */
        BOOLEAN(boolean.class, Boolean.class),
        /** for a Factor. */
        FACTOR(Factor.class),
        /** for a file location. */
        FILE(java.io.File.class),
        /** for a simple String. */
        STRING(String.class),
        /** topia entities collections. */
        COLLECTION(Collection.class, Set.class, List.class);

        private final Class<?>[] klazz;

        TypeClassMapping(Class<?>... klazz) {
            this.klazz = klazz;
        }

        protected static TypeClassMapping getMapping(Field field) {
            for (TypeClassMapping t : TypeClassMapping.values()) {
                for (Class<?> loopKlazz : t.klazz) {
                    if (loopKlazz.isAssignableFrom(field.getType())) {
                        return t;
                    }
                }
            }
            throw new RuntimeException("could not found a TypeClassMapping for this class " + field);
        }
    }

    /**
     * Set region storage.
     * 
     * @param regionStorage the region storage to set.
     */
    public void setRegionStorage(RegionStorage regionStorage) {
        this.regionStorage = regionStorage;
        if (regionStorage == null && log.isWarnEnabled()) {
            log.warn("setRegionStorage called with null value in parameters table cell editor");
        }
    }

    public void setTopiaContext(TopiaContext topiaContext) {
        this.topiaContext = topiaContext;
        if (topiaContext == null && log.isWarnEnabled()) {
            log.warn("setTopiaContext called with null value in parameters table cell editor");
        }
    }

    public void setFactorActionListener(FactorEditorListener factorActionListener) {
        this.factorActionListener = factorActionListener;
    }

    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {

        // get value type
        field = getType(value, row, column);
        JComboBox c;

        TypeClassMapping mapping = TypeClassMapping.getMapping(field);
        switch (mapping) {
        case TOPIA:
            // on a un bean comme parametre
            try {
                if (topiaContext == null && regionStorage == null) {
                    return null;
                }
                TopiaContext context = topiaContext;
                boolean closeContext = false;
                if (context == null) {
                    context = regionStorage.getStorage().beginTransaction();
                    closeContext = true;
                }
                List list = context.findAll("from " + field.getType().getName());
                c = new JComboBox(list.toArray());
                c.setSelectedItem(value);
                editor = c;
                if (closeContext) {
                    context.closeContext();
                }

            } catch (Exception eee) {
                if (log.isWarnEnabled()) {
                    log.warn("Can't get entity object for combobox", eee);
                }
            }
            break;
        case COLLECTION:
            // on a un bean comme parametre
            try {
                if (topiaContext == null && regionStorage == null) {
                    return null;
                }

                // determination du type generic (seulement les entités Topia du modèle)
                ParameterizedType genericType = (ParameterizedType)field.getGenericType();
                String typeName = genericType.getActualTypeArguments()[0].getTypeName();
                if (typeName == null || !typeName.startsWith(FisheryRegion.class.getPackage().getName())) {
                    return null;
                }

                TopiaContext context = topiaContext;
                boolean closeContext = false;
                if (context == null) {
                    context = regionStorage.getStorage().beginTransaction();
                    closeContext = true;
                }
                List list = context.findAll("from " + typeName);
                List modelItems = CheckedComboUtil.getModelItems(list, (Collection)value);
                c = new CheckedComboBox<>(new GenericComboModel<CheckableItem<TopiaEntity>>(modelItems));
                c.setSelectedItem(value);
                editor = c;
                if (closeContext) {
                    context.closeContext();
                }

            } catch (Exception eee) {
                if (log.isWarnEnabled()) {
                    log.warn("Can't get entity object for combobox", eee);
                }
            }
            break;
            case BOOLEAN:
            List<Boolean> listB = new ArrayList<>();
            listB.add(Boolean.TRUE);
            listB.add(Boolean.FALSE);
            c = new JComboBox(listB.toArray());
            listB.clear();
            // TODO See how to convert
            c.setSelectedItem(value);
            editor = c;
            break;
        case TIMESTEP:
            TimeStep date = (TimeStep) value;
            if (date == null) {
                date = new TimeStep(0);
            }
            int month = date.getMonth().getMonthNumber();
            int yea = date.getYear();
            editor = new StepComponent(month, yea);
            break;
        case MONTH:
            editor = MonthComponent.createMounthCombo(((Month) value).getMonthNumber());
            break;
        case FACTOR:
            editor = new FactorEditor(this, (Factor)value);
            if (factorActionListener == null) {
                throw new RuntimeException("Missing factorActionListener call setFactorActionListener()");
            }
            factorActionListener.setFactorCallback((FactorCallback)editor);
            ((JButton)editor).addActionListener(factorActionListener);
            break;
        case FILE:
            // break;
        default:
            editor = new JTextField(String.valueOf(value));
        }
        if (editor != null) {
            if (editor instanceof CheckedComboBox) {
                editor.addFocusListener(new FocusAdapter() {
                    @Override
                    public void focusLost(FocusEvent e) {
                        stopCellEditing();
                    }
                });
            } else if (editor instanceof JComboBox) {
                ((JComboBox<?>) editor).addActionListener(getStopEditingListener());
            } else if (editor instanceof JTextField) {
                editor.addFocusListener(new FocusAdapter() {
                    @Override
                    public void focusLost(FocusEvent e) {
                        if (log.isDebugEnabled()) {
                            log.debug("Stop cell editing");
                        }
                        stopCellEditing();
                    }
                });
            } else if (editor instanceof StepComponent) {
                StepComponent date = ((StepComponent) editor);
                if (date.getMounthCombo() != null) {
                    date.getMounthCombo().addActionListener(getStopEditingListener());
                }
                if (date.getYearCombo() != null) {
                    date.getYearCombo().addActionListener(getStopEditingListener());
                }
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("getCellEditorValue [" + field + "] [mapping:" + mapping + "=" + editor);
        }

        return editor;
    }

    /**
     * Get type for value.
     * 
     * Must be overridden (value can be null).
     * 
     * @param value
     * @param row
     * @param column
     * @return type for value
     */
    protected Field getType(Object value, int row, int column) {
        // default to null
        return null;
    }

    protected ActionListener getStopEditingListener() {
        return e -> stopCellEditing();
    }

    public Object getCellEditorValue() {

        Object result = null;
        TopiaContext context = null;
        if (editor == null) {
            return "";
        }
        TypeClassMapping mapping = TypeClassMapping.getMapping(field);
        switch (mapping) {
        case TOPIA:
        case BOOLEAN:
            result = ((JComboBox) editor).getSelectedItem();
            break;
        case COLLECTION:
            GenericComboModel<CheckableItem<TopiaEntity>> comboBoxModel = (GenericComboModel<CheckableItem<TopiaEntity>>)((JComboBox)editor).getModel();
            List<CheckableItem<TopiaEntity>> modelItems = comboBoxModel.getElementList();
            List<TopiaEntity> selectedItem = CheckedComboUtil.getSelectedItem(modelItems);
            result = selectedItem;
            break;
        case MONTH:
            result = new Month(((MonthComponent)editor).getSelectedValue());
            break;
        case TIMESTEP:
            result = new TimeStep(((StepComponent) editor).getSelectedValue());
            break;
        case FACTOR:
            result = ((FactorEditor)editor).getFactor();
            break;
        case FILE:
            //TODO
            // result = getTextFieldValue(editor);break;
        default:
            if (editor instanceof JTextField) {
                result = ((JTextField) editor).getText();
            } else if (editor instanceof JComboBox) {
                result = ((JComboBox) editor).getSelectedItem();
            }
        }

        if (result instanceof String && !String.class.equals(field.getType())) {
            ConvertUtilsBean cub = ConverterUtil.getConverter(context);
            result = cub.convert((String) result, field.getType());
        }

        if (log.isDebugEnabled()) {
            log.debug("getCellEditorValue [" + field + "] [mapping:" + mapping + "]= " + result);
        }

        return result;
    }
}
