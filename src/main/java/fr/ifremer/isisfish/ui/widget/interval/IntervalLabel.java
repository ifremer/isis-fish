/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2010 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.widget.interval;

import java.util.Observable;
import java.util.Observer;
import javax.swing.JLabel;

/**
 * La vue en label.
 * 
 * Created: Wed Oct  4 2000
 *
 * @author poussin
 * @version $Revision$
 * 
 * Mise a jour: $Date$
 * par : $Author$
 */
public class IntervalLabel extends JLabel implements Observer {

    /** serialVersionUID. */
    private static final long serialVersionUID = -6269941008930677478L;

    private Interval model = null;
    private Object[] renderArray;

    public IntervalLabel(Interval m) {
        setModel(m);
    }

    public void setModel(Interval m) {
        if (this.model != null) {
            this.model.deleteObserver(this);
        }
        this.model = m;
        m.addObserver(this);
        update(m, null);
    }

    public void setLabelRenderer(Object[] renderArray) {
        this.renderArray = renderArray;
    }

    @Override
    public void update(Observable o, Object arg) {
        setText(toString(model.getFirst()) + " - " + toString(model.getLast()));
    }

    protected String toString(int val) {
        String result;
        if (renderArray != null && renderArray.length > val) {
            result = String.valueOf(renderArray[val]);
        } else {
            result = String.valueOf(val);
        }
        return result;
    }
}
