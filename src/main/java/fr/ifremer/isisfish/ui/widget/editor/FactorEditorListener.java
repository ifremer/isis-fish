package fr.ifremer.isisfish.ui.widget.editor;

/*
 * #%L
 * IsisFish
 * %%
 * Copyright (C) 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.event.ActionListener;

import fr.ifremer.isisfish.simulator.sensitivity.Factor;

/**
 * Action listener that call factor callback when action is complete.
 */
public abstract class FactorEditorListener implements ActionListener {

    public interface FactorCallback {
        void setSelectedFactor(Factor factor);
    }

    public FactorCallback callback;

    public void setFactorCallback(FactorCallback callback) {
        this.callback = callback;
    }
    
    public void setSelectedFactor(Factor factor) {
        callback.setSelectedFactor(factor);
    }
}
