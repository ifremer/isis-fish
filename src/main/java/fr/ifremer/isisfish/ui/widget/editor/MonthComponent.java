/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.widget.editor;

import javax.swing.JComboBox;

import org.nuiton.util.MonthEnum;

/**
 * Subclass JComboBox to get a typed ComboBox.
 * Usefull when editing factors.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class MonthComponent extends JComboBox<MonthEnum> {

    /** serialVersionUID. */
    private static final long serialVersionUID = 2992441627176713132L;

    public MonthComponent(MonthEnum[] values) {
        super(values);
    }

    public static MonthComponent createMounthCombo(int mounth) {
        MonthComponent monthComponent = new MonthComponent(MonthEnum.values());
        monthComponent.setSelectedIndex(mounth);
        return monthComponent;
    }
    
    public int getSelectedValue() {
        return getSelectedIndex();
    }
}
