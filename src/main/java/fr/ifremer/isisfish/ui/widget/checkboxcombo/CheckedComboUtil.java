/*-
 * #%L
 * ISIS-Fish
 * %%
 * Copyright (C) 2020 - 2021 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.ui.widget.checkboxcombo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class CheckedComboUtil {

    public static <E> List<CheckableItem<E>> getModelItems(List<E> allData, Collection<E> selectedData) {
        List<CheckableItem<E>> modelItem = new ArrayList<>(allData.size());
        for (E data : allData) {
            CheckableItem<E> item = new CheckableItem<>(data, selectedData != null && selectedData.contains(data));
            modelItem.add(item);
        }
        return modelItem;
    }

    public static <E> List<E> getSelectedItem(List<CheckableItem<E>> items) {
        return items.stream()
                .filter(CheckableItem::isSelected)
                .map(CheckableItem::getElement)
                .collect(Collectors.toList());
    }
}
