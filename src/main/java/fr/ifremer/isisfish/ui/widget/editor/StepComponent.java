/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.widget.editor;

import java.awt.GridLayout;

import javax.swing.JComboBox;
import javax.swing.JPanel;

import org.nuiton.util.MonthEnum;

/**
 * Component used to edit TimeStep.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class StepComponent extends JPanel {

    private static final long serialVersionUID = -6694461572642939712L;

    protected JComboBox<MonthEnum> mounth;

    protected JComboBox<Integer> year;

    public StepComponent(int mounth, int year) {
        super(new GridLayout(0, 2));

        this.mounth = MonthComponent.createMounthCombo(mounth);
        this.year = createYearCombo(200, year);

        this.add(this.mounth);
        this.add(this.year);
    }

    public JComboBox<Integer> getYearCombo() {
        return year;
    }

    public JComboBox<MonthEnum> getMounthCombo() {
        return mounth;
    }

    public int getSelectedMounth() {
        return mounth.getSelectedIndex();
    }

    public int getSelectedYear() {
        return year.getSelectedIndex();
    }

    public void setSelectedValue(int year, int mounth) {
        this.mounth.setSelectedIndex(mounth);
        this.year.setSelectedIndex(year);
    }

    public int getSelectedValue() {
        int selectedYear = getSelectedYear();
        int selectedMounth = getSelectedMounth();
        return selectedMounth + selectedYear * 12;
    }

    public static JComboBox<Integer> createYearCombo(int nb, int selectedYear) {
        JComboBox<Integer> year = new JComboBox<>();
        for (int i = 0; i < nb; i++) {
            year.addItem(i);
        }
        year.setSelectedIndex(selectedYear);
        return year;
    }
}
