/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2020 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.ui.widget.status;

import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.entities.RegionExportJson;
import fr.ifremer.isisfish.ui.WelcomeUI;
import fr.ifremer.isisfish.util.IsisFileUtil;
import fr.ifremer.isisfish.util.UIUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JSeparator;
import java.awt.AWTException;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import static org.nuiton.i18n.I18n.t;

public class StatusBar extends JPanel {

    /** Class logger. */
    private static final Log log = LogFactory.getLog(StatusBar.class);

    protected JProgressBar progressBar;

    protected JLabel messageLabel;

    /** Attribute used to remember screenshot last directory. */
    protected JFileChooser screenshotFileChooser;

    public StatusBar() {
        setLayout(new GridBagLayout());
        setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, Color.DARK_GRAY));

        progressBar = new JProgressBar();
        add(progressBar, new GridBagConstraints(0, 0, 1, 1, 1, 0,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(1, 3, 1, 3), 0, 0));

        messageLabel = new JLabel();
        add(messageLabel, new GridBagConstraints(1, 0, 1, 1, 5, 0,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(1, 3, 1, 3), 0, 0));

        Box indicatorPanel = Box.createHorizontalBox();
        add(indicatorPanel, new GridBagConstraints(2, 0, 1, 1, 0.1, 0,
                GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(1, 3, 1, 3), 0, 0));

        // add some indicator
        indicatorPanel.add(new JLabel(UIUtil.getIconImage("/icons/fatcow/ddr_memory.png")));
        indicatorPanel.add(Box.createHorizontalStrut(3));
        indicatorPanel.add(new MemoryStatus());
        indicatorPanel.add(Box.createHorizontalStrut(3));
        indicatorPanel.add(new JLabel(UIUtil.getIconImage("/icons/fatcow/disk_space.png")));
        indicatorPanel.add(Box.createHorizontalStrut(3));
        indicatorPanel.add(new DiskStatus(new File(IsisFish.config.getIsisHomeDirectory())));
        indicatorPanel.add(new JSeparator(JSeparator.VERTICAL));
        indicatorPanel.add(Box.createHorizontalStrut(3));
        JLabel screenshotLabel = new JLabel(UIUtil.getIconImage("/icons/fatcow/camera.png"));
        screenshotLabel.setToolTipText(t("isisfish.status.screenshot"));
        screenshotLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                takeScreenshot();
            }
        });
        indicatorPanel.add(screenshotLabel);
    }

    /**
     * Change status message and stop progress bar if running.
     *
     * @param message new message
     */
    public void setStatusMessage(String message) {
        setStatusMessage(message, false);
    }

    /**
     * Change status message and progress bar state.
     *
     * @param message new message
     */
    public void setStatusMessage(String message, boolean state) {
        // use a basic mode, swap indeterminate state
        // can't known real progression here
        progressBar.setIndeterminate(state);
        messageLabel.setText(message);
    }

    protected void takeScreenshot() {
        if (screenshotFileChooser == null) {
            screenshotFileChooser = new JFileChooser();
            screenshotFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            screenshotFileChooser.setMultiSelectionEnabled(false);
        }

        // reset
        File previousFile = screenshotFileChooser.getSelectedFile();
        if (previousFile != null) {
            screenshotFileChooser.setSelectedFile(new File("")); // hack alert
            screenshotFileChooser.setCurrentDirectory(previousFile.getParentFile());
        }

        // find parent
        Component parent = this;
        while (!(parent instanceof WelcomeUI)) {
            parent = parent.getParent();
        }
        final Component realParent = parent;

        try {
            // take screenshot
            // we must take screenshot BEFORE filechooser display
            Rectangle screenRect = realParent.getBounds();
            final BufferedImage capture = new Robot().createScreenCapture(screenRect);

            // ask user for file
            int opt = screenshotFileChooser.showSaveDialog(realParent);
            if (opt == JFileChooser.APPROVE_OPTION) {
                File file = screenshotFileChooser.getSelectedFile();
                file = IsisFileUtil.addExtensionIfNeeded(file, "png");
                final File realFile = file;

                // save screenshot
                ImageIO.write(capture, "png", realFile);
            }
        } catch (AWTException | IOException ex) {
            if (log.isErrorEnabled()) {
                log.error("Can't take screenshot help", ex);
            }
        }
    }
}
