/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2010 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.widget.interval;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Observable;

/**
 * Classe permettant de d'ecrire un interval. Cette interval est un peu
 * particulier car il est circulaire. Par exemple si min=0, max=100, first=75 et
 * bien last peut etre egal à 25 donc inferieur a first. Dans ce cas si l'on
 * demande contains 50 il repond faux et contains 99 retourne vrai.
 * 
 * Created: Wed Oct  4 2000
 *
 * @author POUSSIN Benjamin &lt;bpoussin@free.fr&gt; Copyright COGITEC
 * @version $Revision$
 * 
 * Mise a jour: $Date$
 * par : $Author$
 */
public class Interval extends Observable implements Serializable, Cloneable { // Interval

    /** serialVersionUID. */
    private static final long serialVersionUID = 245693725840885583L;

    private int min = 0;

    private int max = 100;

    private int first = 25;

    private int last = 75;

    public Interval(int first, int last) {
        init(min, max, first, last);
    }

    public Interval() {
        init(min, max, first, last);
    }

    private void init(int min, int max, int first, int last) {
        this.min = min;
        this.max = max;
        this.first = first;
        this.last = last;
    }

    /**
     * Retourne le nombre minimal que peut prendre comme valeur cette interval.
     * 
     * @return interval minimal value
     */
    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        if (this.min != min) {
            this.min = min;
            setChanged();
            notifyObservers("min");
        }
    }

    /**
     * Retourne le nombre maximal que peut prendre comme valeur cette interval.
     * 
     * @return interval maximal value
     */
    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        if (this.max != max) {
            this.max = max;
            setChanged();
            notifyObservers("max");
        }
    }

    /**
     * Get the value of first.
     * 
     * @return Value of first.
     */
    public int getFirst() {
        return first;
    }

    /**
     * Set the value of first.
     * 
     * @param v Value to assign to first.
     */
    public void setFirst(int v) {
        int lv = Math.min(max, v);
        if (first != lv) {
            this.first = lv;
            setChanged();
            notifyObservers("first");
        }
    }

    /**
     * Get the value of last.
     * 
     * @return Value of last.
     */
    public int getLast() {
        return last;
    }

    /**
     * Set the value of last.
     * 
     * @param v Value to assign to last.
     */
    public void setLast(int v) {
        int lv = Math.max(min, v);
        if (last != lv) {
            this.last = lv;
            setChanged();
            notifyObservers("last");
        }
    }

    /**
     * Move.
     * 
     * @param decal decal to move
     */
    public void move(int decal) {
        int first = this.first + decal;
        while (first < getMin()) {
            first = getMax() - getMin() + first + 1;
        }
        if (first > getMax()) {
            first = first % (getMax() + 1);
        }

        int last = this.last + decal;
        while (last < getMin()) {
            last = getMax() - getMin() + last + 1;
        }
        if (last > getMax()) {
            last = last % (getMax() + 1);
        }
        if (this.first != first || this.last != last) {
            this.first = first;
            this.last = last;
            setChanged();
            notifyObservers("first,last");
        }
    }

    /**
     * Retourne vrai si l'entier est dans l'interval.
     * 
     * @param integerToCheck integer to check
     * @return {@code true} if integerToCheck is in interval
     */
    public boolean contains(int integerToCheck) {
        if (getFirst() <= getLast()) {
            return getFirst() <= integerToCheck && integerToCheck <= getLast();
        }

        boolean result = (getMin() <= integerToCheck && integerToCheck <= getLast())
                || (getFirst() <= integerToCheck && integerToCheck <= getMax());
        return result;
    }

    /**
     * @return le nombre d'element de l'interval
     */
    public int getNbElem() {
        if (getFirst() <= getLast()) {
            return getLast() - getFirst() + 1;
        }
        return (getMax() - getFirst()) + (getLast() - getMin()) + 2;
    }

    public String toString() {
        return getFirst() + " - " + getLast();
    }

    /**
     * @return une iteration
     */
    public Iterator<Integer> iterator() {
        return new IntervalIterator(getMin(), getMax(), getFirst(), getLast());
    }

    /**
     * Method equals
     * 
     * @param o
     * @return boolean true if o is equal
     */
    public boolean equals(Object o) {
        if (o instanceof Interval) {
            Interval i = (Interval) o;
            return (i.min == min) && (i.max == max) && (i.first == first)
                    && (i.last == last);
        }
        return false;
    }

    @Override
    public Interval clone() {
        Interval result = new Interval();
        result.setFirst(this.getFirst());
        result.setLast(this.getLast());
        result.setMax(this.getMax());
        result.setMin(this.getMin());
        return result;
    }

    public static class IntervalIterator implements Iterator<Integer> {
        //private int min;

        private int max;

        private int first;

        private int last;

        private int current;

        public IntervalIterator(int min, int max, int first, int last) {
            //this.min = min;
            this.max = max;
            this.first = first;
            this.last = last;

            this.current = this.first;
        }

        public boolean hasNext() {
            return (current % (max + 1) != (last + 1) % (max + 1))
                    || (current == first); // si last + 1 == first
        }

        public int nextInt() {
            return (current++) % (max + 1);
        }

        public Integer next() {
            return nextInt();
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }

    }
} // Interval
