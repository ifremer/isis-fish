package fr.ifremer.isisfish.ui.widget.editor;

/*
 * #%L
 * IsisFish
 * %%
 * Copyright (C) 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.swing.CellEditor;
import javax.swing.JButton;

import fr.ifremer.isisfish.simulator.sensitivity.Factor;
import fr.ifremer.isisfish.ui.widget.editor.FactorEditorListener.FactorCallback;

public class FactorEditor extends JButton implements FactorCallback {

    /** Cell editor. */
    protected CellEditor cellEditor;

    /** Component factor value. */
    protected Factor factor;

    /** serialVersionUID. */
    private static final long serialVersionUID = 4070032587998279914L;

    public FactorEditor(CellEditor cellEditor, Factor factor) {
        this.cellEditor = cellEditor;
        this.factor = factor;
        
        if (factor != null) {
            setText(factor.toString());
        } else {
            setText("Edition du facteur");
        }
    }

    public Factor getFactor() {
        return factor;
    }

    @Override
    public void setSelectedFactor(Factor factor) {
        this.factor = factor;
        cellEditor.stopCellEditing();
    }
}
