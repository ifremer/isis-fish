/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2020 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.ui.widget.status;

import static org.nuiton.i18n.I18n.t;

public class MemoryStatus extends ResourceStatus {

    public MemoryStatus() {
        updateStats();
    }

    @Override
    protected void updateStats() {
        Runtime runtime = Runtime.getRuntime();
        int freeMemory = (int) (runtime.freeMemory() / 1024L);
        this.max = (int) (runtime.totalMemory() / 1024L);
        this.current = this.max - freeMemory;
        this.formatString = t("isisfish.widget.memory.label", this.current / 1024, this.max / 1024);
        this.setToolTipText(t("isisfish.widget.memory.tip", this.current / 1024, this.max / 1024));
    }
}
