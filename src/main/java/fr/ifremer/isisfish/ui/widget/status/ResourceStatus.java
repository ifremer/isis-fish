/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2020 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.ui.widget.status;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.Timer;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.font.FontRenderContext;
import java.awt.font.LineMetrics;
import java.awt.geom.Rectangle2D;

/**
 * Inspired by https://github.com/rapidminer/rapidminer-studio/blob/master/src/main/java/com/rapidminer/gui/tools/SystemMonitor.java
 */
public abstract class ResourceStatus extends JComponent implements ActionListener {

    public static final Color COLOR_MAX_RED = new Color(250, 50, 50);

    public static final Color COLOR_MIN_BLUE = new Color(216, 224, 242);

    private final static String memoryTestStr = "99999/99999Mb";

    protected FontRenderContext frc = new FontRenderContext(null, false, false);

    protected LineMetrics lm = new JLabel().getFont().getLineMetrics(memoryTestStr, frc);

    protected Timer timer;

    protected long max;

    protected long current;

    protected String formatString;

    protected ResourceStatus() {
        Font font = new JLabel().getFont();
        Rectangle2D bounds = font.getStringBounds(memoryTestStr, frc);
        Dimension dim = new Dimension((int) bounds.getWidth(), (int) bounds.getHeight());
        setPreferredSize(dim);
        setMaximumSize(dim);

        timer = new Timer(2000, this);
        timer.start();
    }

    @Override
    protected void finalize() throws Throwable {
        timer.stop();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        updateStats();
        repaint();
    }

    protected abstract void updateStats();

    public void paintComponent(Graphics g) {
        Insets insets = new Insets(0, 0, 0, 0);

        int width = getWidth() - insets.left - insets.right;
        int height = getHeight() - insets.top - insets.bottom - 1;
        float fraction = (float) this.current / (float) this.max;
        Color c = getMemoryColor(fraction);
        g.setColor(c);
        g.fillRect(insets.left, insets.top, (int) ((float) width * fraction), height);

        Rectangle2D bounds = g.getFont().getStringBounds(formatString, frc);
        Graphics g2 = g.create();
        g2.setClip(insets.left, insets.top, (int) ((float) width), height);
        g2.setColor(Color.BLACK);
        g2.drawString(formatString, insets.left  + (int) ((double) width - bounds.getWidth()) / 2,  (int) ((float) insets.top + lm.getAscent()));
        g2.dispose();
    }

    protected Color getMemoryColor(double value) {
        if (Double.isNaN(value)) {
            return COLOR_MIN_BLUE;
        }
        float[] minCol = Color.RGBtoHSB(COLOR_MIN_BLUE.getRed(), COLOR_MIN_BLUE.getGreen(), COLOR_MIN_BLUE.getBlue(), null);
        float[] maxCol = Color.RGBtoHSB(COLOR_MAX_RED.getRed(), COLOR_MAX_RED.getGreen(), COLOR_MAX_RED.getBlue(), null);
        double hColorDiff = maxCol[0] - minCol[0];
        double sColorDiff = maxCol[1] - minCol[1];
        double bColorDiff = maxCol[2] - minCol[2];
        return new Color(Color.HSBtoRGB((float) (minCol[0] + hColorDiff * value), (float) (minCol[1] + value * sColorDiff),
                (float) (minCol[2] + value * bColorDiff)));
    }
}
