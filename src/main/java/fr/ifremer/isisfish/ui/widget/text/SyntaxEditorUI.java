/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 - 2020 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.ui.widget.text;

import static org.nuiton.i18n.I18n.t;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.BadLocationException;

import org.fife.rsta.ui.CollapsibleSectionPanel;
import org.fife.rsta.ui.GoToDialog;
import org.fife.rsta.ui.search.FindDialog;
import org.fife.rsta.ui.search.ReplaceDialog;
import org.fife.rsta.ui.search.SearchEvent;
import org.fife.rsta.ui.search.SearchListener;
import org.fife.ui.rsyntaxtextarea.ErrorStrip;
import org.fife.ui.rsyntaxtextarea.RSyntaxDocument;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxScheme;
import org.fife.ui.rsyntaxtextarea.Token;
import org.fife.ui.rtextarea.RTextScrollPane;
import org.fife.ui.rtextarea.SearchContext;
import org.fife.ui.rtextarea.SearchEngine;
import org.fife.ui.rtextarea.SearchResult;

/**
 * {@link RSyntaxTextArea} component property configured for isis fish.
 * 
 * An intermediate panel is used here to allow displaying user action panel to user.
 */
public class SyntaxEditorUI extends CollapsibleSectionPanel implements SearchListener, CaretListener {

    /** serialVersionUID. */
    private static final long serialVersionUID = 1427883892685276516L;

    protected RTextScrollPane textScrollpane;
    protected RSyntaxTextArea textArea;
    protected JLabel noFileLabel;

    protected FindDialog findDialog;
    protected ReplaceDialog replaceDialog;

    protected Action findAction;
    protected Action replaceAction;
    protected Action gotoAction;
    protected Action cutAction;
    protected Action pasteAction;
    protected Action copyAction;

    public SyntaxEditorUI() {
        super(true);

        textArea = new RSyntaxTextArea();
        textScrollpane = new RTextScrollPane(textArea);
        
        noFileLabel = new JLabel(t("isisfish.editor.noselectedfile"), JLabel.CENTER);
        noFileLabel.setEnabled(false);

        // for error/warning markers
        ErrorStrip errorStrip = new ErrorStrip(textArea);
        add(errorStrip, BorderLayout.LINE_END);

        // highlight marked occurence after small period of time
        textArea.setMarkOccurrences(true);

        // display line number
        textScrollpane.setLineNumbersEnabled(true);
        
        // tab is evil
        textArea.setTabsEmulated(true);
        textArea.setTabSize(4);

        initActions();
        
        setEnabled(false);
    }

    protected void initActions() {
        Frame parent = null;

        findDialog = new FindDialog(parent, this);
        replaceDialog = new ReplaceDialog(parent, this);
        
        findAction = new ShowFindDialogAction();
        replaceAction = new ShowReplaceDialogAction();
        gotoAction = new GoToLineAction(parent);
        
        int c = getToolkit().getMenuShortcutKeyMask();
        // caret update will update setEnabled state
        textArea.addCaretListener(this);
        cutAction = new AbstractAction(t("isisfish.editor.cut"), new ImageIcon(getClass().getResource("/icons/fatcow/cut.png"))) {
            @Override
            public void actionPerformed(ActionEvent e) {
                textArea.cut();
            }
        };
        cutAction.setEnabled(false);
        cutAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_C, c));
        pasteAction = new AbstractAction(t("isisfish.editor.paste"), new ImageIcon(getClass().getResource("/icons/fatcow/paste_plain.png"))) {
            @Override
            public void actionPerformed(ActionEvent e) {
                textArea.paste();
            }
        };
        pasteAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_V, c));
        copyAction = new AbstractAction(t("isisfish.editor.copy"), new ImageIcon(getClass().getResource("/icons/fatcow/page_copy.png"))) {
            @Override
            public void actionPerformed(ActionEvent e) {
                textArea.copy();
            }
        };
        copyAction.setEnabled(false);
        copyAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_C, c));
    }

    /**
     * Set highligth style.
     * 
     * @param style new style
     * @see IsisSyntaxConstants
     */
    public void setStyle(String style) {
        RSyntaxDocument doc = new RSyntaxDocument(new SyntaxTokenManagerFactory(), style);
        textArea.setDocument(doc);
        
        // special theme for log
        if (IsisSyntaxConstants.SYNTAX_STYLE_LOG.equals(style)) {
            SyntaxScheme scheme = textArea.getSyntaxScheme();
            scheme.getStyle(Token.RESERVED_WORD_2).foreground = Color.RED;
        }
    }

    public void setText(String text) {
        textArea.setText(text);
        textArea.invalidate();
    }

    public void setRows(int rows) {
        textArea.setRows(rows);
    }

    public void setColumns(int columns) {
        textArea.setColumns(columns);
    }

    @Override
    public void setEnabled(boolean enabled) {
        if (enabled) {
            remove(noFileLabel);
            add(textScrollpane);
        } else {
            remove(textScrollpane);
            add(noFileLabel);
            
            cutAction.setEnabled(false);
            copyAction.setEnabled(false);
        }

        pasteAction.setEnabled(enabled);
        findAction.setEnabled(enabled);
        replaceAction.setEnabled(enabled);
        gotoAction.setEnabled(enabled);

        // workarround for http://bugs.java.com/bugdatabase/view_bug.do?bug_id=4286743
        //textScrollpane.getHorizontalScrollBar().setEnabled(enabled);
        //textScrollpane.getVerticalScrollBar().setEnabled(enabled);
        //textScrollpane.getViewport().getView().setEnabled(enabled);
        //textScrollpane.setEnabled(enabled);

        revalidate();
        repaint();
    }

    private class GoToLineAction extends AbstractAction {
        protected Frame parent;
        public GoToLineAction(Frame parent) {
            super(t("isisfish.script.gotoline"));
            this.parent = parent;
            int c = getToolkit().getMenuShortcutKeyMask();
            putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_L, c));
        }

        public void actionPerformed(ActionEvent e) {
            if (findDialog.isVisible()) {
                findDialog.setVisible(false);
            }
            if (replaceDialog.isVisible()) {
                replaceDialog.setVisible(false);
            }
            GoToDialog dialog = new GoToDialog(this.parent);
            dialog.setMaxLineNumberAllowed(textArea.getLineCount());
            dialog.setVisible(true);
            int line = dialog.getLineNumber();
            if (line>0) {
                try {
                    textArea.setCaretPosition(textArea.getLineStartOffset(line-1));
                } catch (BadLocationException ble) { // Never happens
                    UIManager.getLookAndFeel().provideErrorFeedback(textArea);
                    ble.printStackTrace();
                }
            }
        }

    }

    private class ShowFindDialogAction extends AbstractAction {
        
        public ShowFindDialogAction() {
            super(t("isisfish.script.find"), new ImageIcon(ShowFindDialogAction.class.getResource("/icons/fatcow/find.png")));
            int c = getToolkit().getMenuShortcutKeyMask();
            putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_F, c));
        }

        public void actionPerformed(ActionEvent e) {
            if (replaceDialog.isVisible()) {
                replaceDialog.setVisible(false);
            }
            findDialog.setVisible(true);
        }

    }

    private class ShowReplaceDialogAction extends AbstractAction {
        
        public ShowReplaceDialogAction() {
            super(t("isisfish.script.replace"), new ImageIcon(ShowReplaceDialogAction.class.getResource("/icons/fatcow/text_replace.png")));
            int c = getToolkit().getMenuShortcutKeyMask();
            putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_H, c));
        }

        public void actionPerformed(ActionEvent e) {
            if (findDialog.isVisible()) {
                findDialog.setVisible(false);
            }
            replaceDialog.setVisible(true);
        }

    }

    public Action getFindAction() {
        return findAction;
    }
    
    public Action getReplaceAction() {
        return replaceAction;
    }
    
    public Action getGotoAction() {
        return gotoAction;
    }
    
    public Action getPasteAction() {
        return pasteAction;
    }
    
    public Action getCopyAction() {
        return copyAction;
    }
    
    public Action getCutAction() {
        return cutAction;
    }
    
    @Override
    public String getSelectedText() {
        return textArea.getSelectedText();
    }

    /**
     * Listens for events from our search dialogs and actually does the dirty
     * work.
     */
    @Override
    public void searchEvent(SearchEvent e) {

        SearchEvent.Type type = e.getType();
        SearchContext context = e.getSearchContext();
        SearchResult result;

        switch (type) {
            default: // Prevent FindBugs warning later
            case MARK_ALL:
                result = SearchEngine.markAll(textArea, context);
                break;
            case FIND:
                result = SearchEngine.find(textArea, context);
                if (!result.wasFound()) {
                    UIManager.getLookAndFeel().provideErrorFeedback(textArea);
                }
                break;
            case REPLACE:
                result = SearchEngine.replace(textArea, context);
                if (!result.wasFound()) {
                    UIManager.getLookAndFeel().provideErrorFeedback(textArea);
                }
                break;
            case REPLACE_ALL:
                result = SearchEngine.replaceAll(textArea, context);
                JOptionPane.showMessageDialog(null, result.getCount() +
                        " occurrences replaced.");
                break;
        }
    }

    @Override
    public void caretUpdate(CaretEvent e) {
        // selection pas vide si dot = mark
        boolean selected = e.getDot() != e.getMark();
        cutAction.setEnabled(selected);
        copyAction.setEnabled(selected);
    }
}
