/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.result;

import static org.nuiton.i18n.I18n.t;

import java.awt.Component;

import javax.swing.Icon;

import org.nuiton.math.matrix.MatrixND;
import org.nuiton.math.matrix.viewer.MatrixRenderer;

import fr.ifremer.isisfish.entities.FisheryRegion;
import fr.ifremer.isisfish.map.ResultatLayer;

/**
 * Result matrix viewer map renderer.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class MatrixMapRenderer implements MatrixRenderer {

    protected MapRendererUI mapRendererUI;

    public MatrixMapRenderer(FisheryRegion fisheryRegion) {
        mapRendererUI = new MapRendererUI();

        // set fishery region
        mapRendererUI.getIsisMapBean().setFisheryRegion(fisheryRegion);
    }

    @Override
    public Component getComponent(MatrixND matrix) {
        
        ResultatLayer layer = new ResultatLayer();
        layer.setMatriceInfo(matrix);
        mapRendererUI.getIsisMapBean().removeAllResultatLayer();
        
        mapRendererUI.getIsisMapBean().addResultatLayer(matrix.getName(), layer);
        mapRendererUI.getLegendPanel().setModel(new LegendModel(0,Math.round(layer.getDataMapList().getMaxDataMapValue())));

        return mapRendererUI;
    }

    @Override
    public Icon getIcon() {
        return null;
    }

    @Override
    public String getName() {
        return t("isisfish.result.map");
    }

}
