/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2015 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.result;

/**
 * LegendModel.
 *
 * Created: Tue Oct  8 11:31:57 2002
 *
 * @author <a href="mailto:seb.regnier@free.fr"></a>
 * @version $Revision$
 */
public class LegendModel {

    protected double min;
    protected double max;

    public LegendModel() {
        this(0, 0);
    }

    public LegendModel(double min, double max) {
        this.min = min;
        this.max = max;
    }

    /**
     * Get the value of min.
     * @return value of min.
     */
    public double getMin() {
        return min;
    }

    /**
     * Set the value of min.
     * @param v  Value to assign to min.
     */
    public void setMin(double v) {
        this.min = v;
    }

    /**
     * Get the value of max.
     * @return value of max.
     */
    public double getMax() {
        return max;
    }

    /**
     * Set the value of max.
     * @param v  Value to assign to max.
     */
    public void setMax(double v) {
        this.max = v;
    }

}// LegendModel
