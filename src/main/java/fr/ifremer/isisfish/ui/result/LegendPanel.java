/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2015 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.result;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * LegendPanel.
 *
 * Created: Tue Oct  8 10:56:14 2002
 *
 * @author <a href="mailto:seb.regnier@free.fr"></a>
 * @version $Revision$
 */
public class LegendPanel extends JPanel {

    /** serialVersionUID */
    private static final long serialVersionUID = -6803899655178581165L;

    private final static int DEFAULT_WIDTH = 110;
    private final static int DEFAULT_HEIGHT = 50;

    protected LegendModel model = null;
    protected LegendGraphic graph;
    protected JLabel labelMax;
    protected JLabel labelMin;

    public LegendPanel() {
        this(null);
    }

    public LegendPanel(LegendModel model) {
        this(model, DEFAULT_WIDTH, DEFAULT_HEIGHT);
    }

    public LegendPanel(LegendModel model, int width, int height) {
        if (model == null) {
            model = new LegendModel(0, 0);
        }
        this.model = model;

        setPreferredSize(new Dimension(width, height));
        this.setLayout(new GridBagLayout());

        graph = new LegendGraphic(this.model);
        labelMin = new JLabel(Double.toString(model.getMin()));
        labelMax = new JLabel(Double.toString(model.getMax()));

        GridBagConstraints c;

        c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 0;
        c.weighty = 1;
        c.gridwidth = 1;
        c.gridheight = 1;
        c.fill = GridBagConstraints.HORIZONTAL;
        this.add(labelMin, c);

        c = new GridBagConstraints();
        c.gridx = 1;
        c.gridy = 0;
        c.weightx = 1;
        c.weighty = 1;
        c.gridwidth = 1;
        c.gridheight = 1;
        c.fill = GridBagConstraints.BOTH;
        this.add(graph, c);

        c = new GridBagConstraints();
        c.gridx = 2;
        c.gridy = 0;
        c.weightx = 0;
        c.weighty = 1;
        c.gridwidth = 1;
        c.gridheight = 1;
        c.fill = GridBagConstraints.HORIZONTAL;
        this.add(labelMax, c);
    }

    public LegendModel getModel() {
        return model;
    }

    public void setModel(LegendModel model) {
        this.model = model;
        graph.setModel(model);
        labelMin.setText(Double.toString(model.getMin()));
        labelMax.setText(Double.toString(model.getMax()));
    }

    public void setEnabled(boolean enable) {
        graph.setEnabled(enable);
        labelMin.setEnabled(enable);
        labelMax.setEnabled(enable);
    }
}// LegendPanel
