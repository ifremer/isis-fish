/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2020 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.result;

import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.datastore.StorageException;
import fr.ifremer.isisfish.logging.SimulationLoggerUtil;
import fr.ifremer.isisfish.simulator.launcher.SimulationJob;
import fr.ifremer.isisfish.simulator.launcher.SimulationService;
import fr.ifremer.isisfish.simulator.launcher.SimulationServiceListener;
import fr.ifremer.isisfish.ui.CommonHandler;
import fr.ifremer.isisfish.ui.models.common.GenericComboModel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.SwingUtilities;
import java.beans.PropertyVetoException;
import java.util.List;

/**
 * Handler for result UIs.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class ResultHandler extends CommonHandler {

    private static Log log = LogFactory.getLog(ResultHandler.class);

    protected SimulationServiceListener listener = null;

    protected ResultUI resultUI;
    
    public ResultHandler(ResultUI resultUI) {
        this.resultUI = resultUI;
    }

    /**
     * Initialise la vue avec les liste des simulations disponible.
     */
    public void afterInit() {
        initSimulationList();

        // reference must stay allocated
        listener = new SimulationServiceListener() {
            @Override
            public void simulationStart(SimulationService simService, SimulationJob job) {
            }
            @Override
            public void simulationStop(SimulationService simService, final SimulationJob job) {
                final GenericComboModel<String> model = (GenericComboModel<String>)resultUI.getSimulationComboBox().getModel();
                SwingUtilities.invokeLater(() -> model.addElement(job.getId()));
            }
            @Override
            public void clearJobDone(SimulationService simService) {
            }
        };
        SimulationService.getService().addSimulationServiceListener(listener);
    }

    /**
     * Reload simulation combo box model.
     */
    protected void initSimulationList() {
        List<String> value = SimulationStorage.getSimulationNames();

        GenericComboModel<String> model = new GenericComboModel<>(value);
        resultUI.getSimulationComboBox().setModel(model);
        resultUI.getSimulationComboBox().setSelectedItem(null);
    }

    /**
     * Open selected simulation item in new internal frame.
     */
    public void openNewSimulation() {
        String selected = (String)resultUI.getSimulationComboBox().getSelectedItem();
        SimulationStorage simulation = SimulationStorage.getSimulation(selected);

        ResultFrameUI resultFrameUI = new ResultFrameUI(resultUI);
        resultFrameUI.setTitle(simulation.getName());
        resultFrameUI.getHandler().afterInit(simulation);
        resultFrameUI.setSize(800, 600);
        resultUI.getSimulationDesktopPane().add(resultFrameUI);
        try {
            resultFrameUI.setSelected(true);
        } catch (PropertyVetoException ex) {
            if (log.isWarnEnabled()) {
                log.warn("Can't auto selected internal frame", ex);
            }
        }
    }

    /**
     * Delete selected simulation.
     */
    public void deleteSimulation() {
        String selected = (String)resultUI.getSimulationComboBox().getSelectedItem();
        SimulationStorage simulation = SimulationStorage.getSimulation(selected);
        try {
            simulation.delete(false);
        } catch (StorageException ex) {
            throw new IsisFishRuntimeException("Can't delete simulation", ex);
        }
        initSimulationList();
    }

    /**
     * Display simulation log.
     */
    public void showLog() {
        String selected = (String)resultUI.getSimulationComboBox().getSelectedItem();
        try {
            SimulationLoggerUtil.showSimulationLogConsole(resultUI, selected);
        } catch (Exception ex) {
            throw new IsisFishRuntimeException("Can't display simulation log", ex);
        }
    }
}
