/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2021 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.result;

import javax.swing.Icon;

import org.nuiton.math.matrix.MatrixND;
import org.nuiton.math.matrix.viewer.MatrixDimensionAction;
import org.nuiton.util.Resource;

import fr.ifremer.isisfish.types.TimeStep;

import java.util.ArrayList;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Action de somme des elements par 12 (année) pour la dimension des pas de temps.
 */
public class YearSumDimensionAction implements MatrixDimensionAction {

    @Override
    public Icon getIcon() {
        return Resource.getIcon("/icons/sum-y-disabled.png");
    }

    @Override
    public Icon getSelectedIcon() {
        return Resource.getIcon("/icons/sum-y.png");
    }

    @Override
    public String getDescription() {
        return t("isisfish.result.matrix.sumByYearDescription");
    }

    @Override
    public boolean canApply(int i, List<?> list) {
        return list.get(0) instanceof TimeStep;
    }

    @Override
    public MatrixND apply(MatrixND matrix, int dim) {

        matrix = matrix.sumOverDim(dim, 12);

        // si c une somme pour les annees, on change l'intitule
        matrix.setDimensionName(dim, t("isisfish.common.year"));

        // #1905 : modifie les semantiques de type Date pour que lorsque
        // c'est par exemple une somme par année
        // les semantique se nomment
        // janvier 0, janvier 1...
        // plutot que
        // janvier 0, fevrier 0...
        //if (dim == 0) {
        //    List<Object> semList = (List<Object>)matrix.getSemantic(dim);
        //    List<Object> newList = new ArrayList<>();
        //    for (Object semObject : semList) {
        //        if (semObject instanceof TimeStep) {
        //            TimeStep semStep = (TimeStep) semObject;
        //            TimeStep newStep = new TimeStep(semStep.getStep() * sumStep);
        //            newList.add(newStep);
        //        } else {
        //            newList.add(semObject);
        //        }
        //    }
        //    matrix.setSemantic(dim, newList);
        //}
        // end semantics modification */

        // modifie les libellés pour afficher "année 1", "année 2"...
        int semCount = matrix.getSemantic(dim).size();
        List<String> newSems = new ArrayList<>(semCount);
        for (int i = 0; i < semCount ; i++) {
            newSems.add(t("isisfish.result.matrix.sumByYearLegend", i));
        }
        matrix.setSemantic(0, newSems);

        return matrix;
    }
}
