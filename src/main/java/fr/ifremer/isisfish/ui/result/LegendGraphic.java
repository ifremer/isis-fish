/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.result;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JComponent;

/**
 * TODO add comment here.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class LegendGraphic extends JComponent {

    /** serialVersionUID. */
    private static final long serialVersionUID = -4493919765232743572L;

    protected LegendModel model;

    /**
     * Constructeur.
     * 
     * @param m model
     */
    public LegendGraphic(LegendModel m) {
        setModel(m);
    }

    public void setModel(LegendModel m) {
        this.model = m;
        repaint();
    }

    /**
     * Dessine sur le graphique la legende courante.
     * 
     * @param g graphics
     */
    protected void redraw(Graphics g) {
        //double coef = (double)getSize().width / (double)(model.getMax()-model.getMin());
        float coef = (float) getSize().width / 255f;

        int width = getSize().width;
        int height = getSize().height;

        Image ImageBuffer = createImage(width, height);
        Graphics tmpg = ImageBuffer.getGraphics();

        int rgb;
        for (int i = 0; i <= 255; i++) {
            rgb = 255 - i;
            tmpg.setColor(new Color(rgb, rgb, rgb));
            //      tmpg.setColor(model.getLegendColor());
            tmpg.fillRect(Math.round(i * coef), 0, Math.round(coef),
                    height);
            tmpg.drawRect(Math.round(i * coef), 0, Math.round(coef),
                    height);
        }

        g.drawImage(ImageBuffer, 0, 0, getSize().width, getSize().height,
                (img, infoflags, x, y, width1, height1) -> true);
    }

    @Override
    public void paint(Graphics g) {
        redraw(g);
    }

}
