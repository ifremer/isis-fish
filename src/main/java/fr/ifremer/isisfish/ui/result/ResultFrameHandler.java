/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2020 - 2022 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.result;

import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.datastore.ExportStorage;
import fr.ifremer.isisfish.datastore.ResultStorage;
import fr.ifremer.isisfish.datastore.SensitivityExportStorage;
import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.datastore.StorageException;
import fr.ifremer.isisfish.entities.FisheryRegion;
import fr.ifremer.isisfish.export.ExportHelper;
import fr.ifremer.isisfish.export.ExportInfo;
import fr.ifremer.isisfish.simulator.launcher.SimulationServiceListener;
import fr.ifremer.isisfish.ui.CommonHandler;
import fr.ifremer.isisfish.ui.models.common.GenericComboModel;
import fr.ifremer.isisfish.ui.widget.editor.ScriptParameterDialog;
import fr.ifremer.isisfish.util.IsisFileUtil;
import fr.ifremer.isisfish.util.MenuScroller;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.math.matrix.viewer.MatrixViewerPanel;
import org.nuiton.math.matrix.viewer.renderer.MatrixChartRenderer;
import org.nuiton.math.matrix.viewer.renderer.MatrixPanelRenderer;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;

import javax.swing.JMenuItem;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.io.File;
import java.io.IOException;

import static org.nuiton.i18n.I18n.t;

/**
 * Handler for result UIs.
 *
 * @author chatellier
 * @version $Revision$
 * <p>
 * Last update : $Date$
 * By : $Author$
 */
public class ResultFrameHandler extends CommonHandler {

    private static Log log = LogFactory.getLog(ResultFrameHandler.class);

    protected SimulationServiceListener listener = null;

    protected ResultFrameUI resultFrameUI;

    protected SimulationStorage simulationStorage;

    protected TopiaContext topiaContext;

    public ResultFrameHandler(ResultFrameUI resultFrameUI) {
        this.resultFrameUI = resultFrameUI;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        if (topiaContext != null) {
            topiaContext.closeContext();
        }
    }

    /**
     * Initialise la vue avec les liste des simulations disponible.
     */
    public void afterInit(SimulationStorage simulation) {
        this.simulationStorage = simulation;

        try {
            for (String exportName : ExportStorage.getExportNames()) {
                JMenuItem item = new JMenuItem(t(exportName));
                item.addActionListener(new ExportActionListener(exportName, false));
                resultFrameUI.getExportMenu().add(item);
            }
            MenuScroller.setScrollerFor(resultFrameUI.getExportMenu());

            // init sensitivity menu
            for (String sensitivityExportName : SensitivityExportStorage.getSensitivityExportNames()) {
                JMenuItem item = new JMenuItem(t(sensitivityExportName));
                item.addActionListener(new ExportActionListener(sensitivityExportName, true));
                resultFrameUI.getSensitivityMenu().add(item);
            }

            topiaContext = simulation.getStorage().beginTransaction();
            final ResultStorage resultStorage = simulation.getResultStorage();
            FisheryRegion fisheryRegion = SimulationStorage.getFisheryRegion(topiaContext);

            // init viewer panel
            final MatrixViewerPanel matrixViewerPanel = new MatrixViewerPanel();
            matrixViewerPanel.addMatrixDimentionAction(new YearSumDimensionAction());
            matrixViewerPanel.addMatrixRenderer(new MatrixSummaryRenderer(simulation, resultStorage, topiaContext), true);
            matrixViewerPanel.addMatrixRenderer(new MatrixChartRenderer());
            matrixViewerPanel.addMatrixRenderer(new MatrixMapRenderer(fisheryRegion));
            matrixViewerPanel.addMatrixRenderer(new MatrixPanelRenderer());

            // init available results list
            GenericComboModel<String> model = new GenericComboModel<>(resultStorage.getResultName());
            resultFrameUI.getResultsComboBox().setModel(model);
            resultFrameUI.getResultsComboBox().addItemListener(e -> {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    String selectedMatrixName = (String) e.getItem();
                    MatrixND matrix = resultStorage.getMatrix(selectedMatrixName, topiaContext);
                    matrixViewerPanel.setMatrix(matrix);
                }
            });

            // force summary display
            matrixViewerPanel.updateSelectedRenderingComponent();
            resultFrameUI.getMatrixViewerContainer().add(matrixViewerPanel, BorderLayout.CENTER);

            if (model.getSize() > 0) {
                // FIXME poussin 20140721 est-ce bien intelligent de forcer l'ouverture
                // du 1er resultat trouver. On prend du temps pour ca, alors que l'utilisateur
                // veut potentiellement regarder autre chose. Demander au utilisateur si on peut
                // ne pas ouvrir de resultat par defaut.
                model.setSelectedItem(model.getElementAt(0));
            }
        } catch (TopiaException | StorageException ex) {
            throw new IsisFishRuntimeException("Can't open simulation", ex);
        }
    }

    protected class ExportActionListener implements ActionListener {

        protected String exportName;

        protected boolean sensitivity;

        public ExportActionListener(String exportName, boolean sensitivity) {
            this.exportName = exportName;
            this.sensitivity = sensitivity;
        }

        public void actionPerformed(ActionEvent e) {
            try {
                File file = IsisFileUtil.getFile(".+\\.csv(\\.gz)?", t("isisfish.result.export.file"));
                if (file != null) {
                    // add csv extension if not set
                    if (!file.getName().matches(".+\\.csv(\\.gz)?")) {
                        file = new File(file.getAbsolutePath() + ".csv.gz");
                    }

                    ExportInfo exportInfo;
                    if (sensitivity) {
                        SensitivityExportStorage exportStorage = SensitivityExportStorage.getSensitivityExport(exportName);
                        exportInfo = exportStorage.getNewInstance();
                    } else {
                        ExportStorage exportStorage = ExportStorage.getExport(exportName);
                        exportInfo = exportStorage.getNewInstance();
                    }
                    exportInfo = (ExportInfo) ScriptParameterDialog.displayConfigurationFrame(resultFrameUI, topiaContext, exportInfo);
                    if (exportInfo != null) {
                        ExportHelper.exportToFile(simulationStorage, exportInfo, file);
                    }
                }
            } catch (Exception eee) {
                if (log.isWarnEnabled()) {
                    log.warn("Erreur lors de l'export ", eee);
                }
            }
        }
    }

    /**
     * Export current simulation to zip file.
     */
    public void exportSimulation() {
        File file = IsisFileUtil.getFile(".+\\.zip", t("isisfish.result.export.simulation.file"));
        if (file != null) {
            try {
                simulationStorage.createZip(file);
            } catch (IOException ex) {
                throw new IsisFishRuntimeException("Can't export simulation", ex);
            }
        }
    }

    /**
     * Export current simulation to zip file.
     */
    public void exportSimulationReference() {
        File file = IsisFileUtil.getFile(".+\\.zip", t("isisfish.result.export.simulation.file"));
        if (file != null) {
            try {
                simulationStorage.createReferenceZip(file);
            } catch (IOException ex) {
                throw new IsisFishRuntimeException("Can't export simulation", ex);
            }
        }
    }
}
