/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.result;

import static org.nuiton.i18n.I18n.t;
import static org.nuiton.i18n.I18n.n;

import java.awt.Component;

import javax.swing.Icon;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import org.nuiton.math.matrix.MatrixND;
import org.nuiton.math.matrix.viewer.MatrixRenderer;
import org.nuiton.topia.TopiaContext;

import fr.ifremer.isisfish.datastore.ResultStorage;
import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.simulator.SimulationParameter;
import fr.ifremer.isisfish.simulator.SimulationParameterPropertiesHelper;
import fr.ifremer.isisfish.types.TimeStep;
import java.util.Properties;

/**
 * Simulation summary renderer.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class MatrixSummaryRenderer implements MatrixRenderer {

    protected static final String ABONDANCE_STRING = n("matrixAbundance");
    protected static final String CAPTUREPARMETIER_STRING = n("matrixCatchPerStrategyMetPerZoneMet");
    protected static final String REJETPARMETIER_STRING = n("matrixDiscardsPerStrMetPerZonePop");

    protected SimulationStorage simulationStorage;

    protected ResultStorage resultStorage;

    protected TopiaContext topiaContext;

    protected Component cachedComponent;

    public MatrixSummaryRenderer(SimulationStorage simulationStorage, ResultStorage resultStorage, TopiaContext topiaContext) {
        this.resultStorage = resultStorage;
        this.simulationStorage = simulationStorage;
        this.topiaContext = topiaContext;
    }

    @Override
    public Component getComponent(MatrixND matrix) {
        if (cachedComponent == null) {

            SimulationParameter sp = simulationStorage.getParameter();
            Properties prop = sp.toProperties();
            String params = SimulationParameterPropertiesHelper.toString(prop);
            StringBuilder resume = new StringBuilder(params);
            resume.append("\n");

            for (String name : resultStorage.getResultName()) {
                try {
                    if (name.startsWith(ABONDANCE_STRING)){
                        MatrixND mat1 = resultStorage.getMatrix(new TimeStep(0), name, topiaContext);
                        mat1 = mat1.sumOverDim(0);
                        mat1 = mat1.sumOverDim(1);
                        mat1 = mat1.reduce();
                        resume.append(t("isisfish.result.begin.simulation")).append(mat1.getName()).append(": ").append(mat1.getValue(0)).append("\n");
    
                        mat1 = resultStorage.getMatrix(resultStorage.getLastStep(), name, topiaContext);
                        mat1 = mat1.sumOverDim(0);
                        mat1 = mat1.sumOverDim(1);
                        mat1 = mat1.reduce();
                        resume.append(t("isisfish.result.end.simulation")).append(mat1.getName()).append(": ").append(mat1.getValue(0)).append("\n");
                    }
                    else if(name.startsWith(REJETPARMETIER_STRING)){
                        MatrixND mat1 = resultStorage.getMatrix(name, topiaContext);
                        mat1 = mat1.sumOverDim(0);
                        mat1 = mat1.sumOverDim(1);
                        mat1 = mat1.sumOverDim(2);
                        mat1 = mat1.sumOverDim(3);
    
                        mat1 = mat1.reduce();
                        resume.append(t("isisfish.common.sum")).append(" ").append(mat1.getName()).append(": ").append(mat1.getValue(0)).append("\n");
                    }
                    else if (!name.startsWith(CAPTUREPARMETIER_STRING)){
                        MatrixND mat1 = resultStorage.getMatrix(name, topiaContext);
                        mat1 = mat1.sumOverDim(0);
                        mat1 = mat1.sumOverDim(1);
                        mat1 = mat1.sumOverDim(2);
    
                        mat1 = mat1.reduce();
                        resume.append(t("isisfish.common.sum")).append(" ").append(mat1.getName()).append(": ").append(mat1.getValue(0)).append("\n");
                    }
                } catch (Exception eee) {
                    resume.append(t("isisfish.error.no.matrix", name)).append("\n");
                }
            }
            
            resume.append("\n\n");
            resume.append(simulationStorage.getInformation().toString());

            cachedComponent = new JScrollPane(new JTextArea(resume.toString()));
        }
        
        return cachedComponent;
    }

    @Override
    public Icon getIcon() {
        return null;
    }

    @Override
    public String getName() {
        return t("isisfish.result.summary");
    }
}
