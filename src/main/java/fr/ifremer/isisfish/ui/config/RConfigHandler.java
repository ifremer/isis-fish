/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2022 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.config;

import static org.nuiton.i18n.I18n.t;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.text.DefaultCaret;

import fr.ifremer.isisfish.simulator.SimulationControl;
import fr.ifremer.isisfish.simulator.launcher.SubProcessSimulationLauncher;
import fr.ifremer.isisfish.ui.script.ScriptHandler;
import fr.ifremer.isisfish.util.RUtil;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.j2r.REngine;
import org.nuiton.j2r.RException;
import org.nuiton.j2r.RProxy;

/**
 * Handler for R Configuration UI.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class RConfigHandler implements Runnable {

    private static final Log log = LogFactory.getLog(RConfigHandler.class);

    protected RConfigUI rconfigUI;

    protected File rDirectory;

    public RConfigHandler(RConfigUI rconfigUI) {
        this.rconfigUI = rconfigUI;
    }

    /**
     * Initialise l'ui.
     */
    public void afterInit() {
        String systemPath = System.getProperty("java.library.path");
        rconfigUI.getLibraryPathField().setText(systemPath);

        // scroll to end of area
        DefaultCaret caret = (DefaultCaret)rconfigUI.getInformationField().getCaret();
        caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
    }

    public void testRConfiguration() {
        this.testREngine();
    }

    /**
     * Test r configuration.
     */
    public void testREngine() {
        rconfigUI.getInformationField().setBackground(null);

        try {
            rconfigUI.getInformationField().setText(t("isisfish.simulator.configuration.r.testing"));

            REngine engine = new RProxy();
            engine.eval("library('lhs')");
            engine.eval("library('sensitivity')");

            rconfigUI.getInformationField().setBackground(ScriptHandler.COLOR_SUCCESS);
            rconfigUI.getInformationField().setText(t("isisfish.simulator.configuration.r.testedok"));
        } catch (RException ex) {
            rconfigUI.getInformationField().setBackground(ScriptHandler.COLOR_FAILURE);
            rconfigUI.getInformationField().setText(t("isisfish.simulator.configuration.r.testedincomplete") + "\n\n");

            JOptionPane.showMessageDialog(rconfigUI, t("isisfish.simulator.configuration.r.cantinit", ex.getMessage()),
                    t("isisfish.simulator.configuration.r.title"), JOptionPane.ERROR_MESSAGE);

            // R auto configuration
            JFileChooser jfc = new JFileChooser();
            jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            int option = jfc.showOpenDialog(rconfigUI);
            if (option == JFileChooser.APPROVE_OPTION) {
                rDirectory = jfc.getSelectedFile();
                new Thread(this).start();
            }
        }
    }

    public void run() {
        rconfigUI.setRunning(false);
        try {
            rconfigUI.getInformationField().append(t("isisfish.simulator.configuration.r.installing") + "\n");
            rconfigUI.getInformationField().setBackground(null);

            File rBin = new File(rDirectory, "bin");
            File rScript = new File(rBin, "Rscript");
            boolean result = installPackages(rScript);
            result &= configureJriPath(rScript);

            if (result) {
                rconfigUI.getInformationField().setBackground(ScriptHandler.COLOR_SUCCESS);
                rconfigUI.getInformationField().append("****************************\n");
                rconfigUI.getInformationField().append(t("isisfish.simulator.configuration.r.installed") + "\n");
                rconfigUI.getInformationField().append(t("isisfish.simulator.configuration.r.restart") + "\n");
                rconfigUI.getInformationField().append("****************************\n");
            } else {
                rconfigUI.getInformationField().setBackground(ScriptHandler.COLOR_FAILURE);
                rconfigUI.getInformationField().append(t("isisfish.simulator.configuration.r.failure") + "\n");
            }
        }  finally {
            rconfigUI.setRunning(false);
        }
    }

    protected boolean installPackages(File rScript) {
        boolean result = false;

        String script = "dir.create(path = Sys.getenv(\"R_LIBS_USER\"), showWarnings = FALSE, recursive = TRUE)\n" +
                "if(!require(rJava)){\n" +
                "    install.packages(\"rJava\", lib = Sys.getenv(\"R_LIBS_USER\"), repos=\"https://cran.irsn.fr/\")\n" +
                "}\n" +
                "if(!require(lhs)){\n" +
                "    install.packages(\"lhs\", lib = Sys.getenv(\"R_LIBS_USER\"), repos=\"https://cran.irsn.fr/\")\n" +
                "}\n" +
                "if(!require(sensitivity)){\n" +
                "    install.packages(\"sensitivity\", lib = Sys.getenv(\"R_LIBS_USER\"), repos=\"https://cran.irsn.fr/\")\n" +
                "}";
        try {
            // use real file (argument not working on windows)
            File scriptFile = File.createTempFile("isis-r-script-", ".R");
            scriptFile.deleteOnExit();
            FileUtils.writeStringToFile(scriptFile, script, StandardCharsets.UTF_8);

            ProcessBuilder pb = new ProcessBuilder(rScript.getAbsolutePath(), scriptFile.getAbsolutePath());
            pb.redirectErrorStream(true);
            Process process = pb.start();

            SubProcessLogThread logThread = new SubProcessLogThread(process, rconfigUI.getInformationField());
            logThread.start();

            int errorCode = process.waitFor();
            if (errorCode == 0) {
                result = true;
            } else {
                log.error("Can't call Rscript : " + errorCode);
            }
        } catch (IOException | InterruptedException ex) {
            rconfigUI.getInformationField().append(ex.getMessage() + "\n");
            log.error("Can't install r libraries", ex);
        }

        // output configuration (in . dir)
        File isisRConfigFile = new File(RUtil.JRI_PATH_FILE);
        try {
            FileUtils.writeStringToFile(isisRConfigFile, rDirectory.getAbsolutePath(), StandardCharsets.UTF_8);
        } catch (IOException ex) {
            log.error("Can't write isis configuration", ex);
        }

        return result;
    }

    protected boolean configureJriPath(File rScript) {
        boolean result = false;

        try {
            String script = "installed.packages()[which(installed.packages()[,1]=='rJava'),]['LibPath']";
            ProcessBuilder pb = new ProcessBuilder(rScript.getAbsolutePath(), "-e", script);
            Process process = pb.start();
            int exitValue = process.waitFor();

            if (exitValue == 0) {
                // normal exit
                String jriDirectory = RUtil.parseRscriptOutput(process.getInputStream());

                if (StringUtils.isNotBlank(jriDirectory)) {
                    // output configuration (in . dir)
                    File isisRConfigFile = new File(RUtil.JRI_PATH_FILE);
                    try {
                        FileUtils.writeStringToFile(isisRConfigFile, jriDirectory, StandardCharsets.UTF_8);

                        result = true;
                    } catch (IOException ex) {
                        log.error("Can't write isis configuration", ex);
                    }
                } else {
                    rconfigUI.getInformationField().setBackground(ScriptHandler.COLOR_FAILURE);
                    rconfigUI.getInformationField().append(t("isisfish.simulator.configuration.r.failurejri") + "\n");
                }
            } else {
                // R trouvé, mais la commande R a échoué
                InputStream err = process.getErrorStream();
                rconfigUI.getInformationField().setBackground(ScriptHandler.COLOR_FAILURE);
                rconfigUI.getInformationField().append(IOUtils.toString(err, StandardCharsets.UTF_8) + "\n");
            }
        } catch (IOException ex) {
            // Le processus n'a pas pu être lancé (R non trouvé)
            if (log.isDebugEnabled()) {
                log.debug("Can't find R", ex);
            }
        } catch (InterruptedException ex) {
            if (log.isErrorEnabled()) {
                log.error("Can't find R", ex);
            }
        }

        return result;
    }
}

class SubProcessLogThread extends Thread { // SubProcessProcessThread

    private static final Log log = LogFactory.getLog(SubProcessLogThread.class);

    // on l'appel plutot out que in, car c le output du process lance
    protected InputStream out = null;
    protected JTextArea textField;

    public SubProcessLogThread(Process process, JTextArea textField) {
        this.out = process.getInputStream();
        this.textField = textField;
    }

    @Override
    public void run() {
        InputStreamReader osr = new InputStreamReader(out);
        BufferedReader br = new BufferedReader(osr);

        // use tip available at
        // https://www.javaworld.com/article/2071275/when-runtime-exec---won-t.html
        // for reading subprocess output
        String line;
        try {
            while ((line = br.readLine()) != null) {
                final String line2 = line;
                SwingUtilities.invokeLater(() -> textField.append(line2 + "\n"));
            }
        } catch (IOException ex) {
            log.error("Can't read thread output", ex);
        }
    }
}
