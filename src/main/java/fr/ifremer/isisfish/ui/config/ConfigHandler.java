/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2019 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.config;

import fr.ifremer.isisfish.config.IsisConfig;
import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.ui.models.common.GenericComboModel;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.math.matrix.SparseVector;
import org.nuiton.math.matrix.Vector;
import org.reflections.Reflections;

import javax.swing.JOptionPane;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * Handler for Configuration UI.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class ConfigHandler {

    private static Log log = LogFactory.getLog(ConfigHandler.class);

    protected ConfigUI configUI;

    protected Reflections reflections = new Reflections(MatrixND.class.getPackage().getName());

    public ConfigHandler(ConfigUI configUI) {
        this.configUI = configUI;
    }

    /**
     * Initialise l'ui.
     */
    public void afterInit() {
        setupConfigFileLocation();
        fillCurrentConfig();
    }

    protected void setupConfigFileLocation() {
        File file = IsisFish.config.getUserConfigFile();
        configUI.isisConfigFileLocationLabel.setText(t("isisfish.simulator.configuration.configFileLocation", file.getAbsolutePath()));
    }


    protected void fillCurrentConfig() {
        IsisConfig config = IsisFish.config;
        // general
        configUI.isisHomeDirectoryField.setText(StringUtils.stripToEmpty(config.getIsisHomeDirectory()));
        configUI.localeCombo.setSelectedItem(config.getLocale().toString());
        // simulator
        configUI.simulationInMaxThreadsField.setText(String.valueOf(config.getSimulatorInMaxThreads()));
        configUI.simulationSubMaxProcessField.setText(String.valueOf(config.getSimulatorSubMaxProcess()));
        configUI.simulationSubMaxMemoryField.setText(StringUtils.stripToEmpty(config.getSimulatorSubMaxMemory()));
        // simulation
        List<Class<? extends Vector>> vectors = new ArrayList<>(reflections.getSubTypesOf(Vector.class));
        List<Class<? extends Vector>> fullVectors = vectors.stream().filter(c -> !SparseVector.class.isAssignableFrom(c)).collect(Collectors.toList());
        List<Class<? extends Vector>> sparseVectors = vectors.stream().filter(c-> !c.isInterface()).filter(SparseVector.class::isAssignableFrom).collect(Collectors.toList());
        configUI.matrixVectorClassField.setModel(new GenericComboModel<>(fullVectors));
        configUI.matrixVectorClassField.setSelectedItem(config.getSimulationMatrixVectorClass());
        configUI.matrixVectorSparseClassField.setModel(new GenericComboModel<>(sparseVectors));
        configUI.matrixVectorSparseClassField.setSelectedItem(config.getSimulationMatrixVectorSparseClass());
        configUI.matrixVectorThresholdField.setText(String.valueOf(config.getSimulationMatrixThresholdUseSparse()));
        configUI.matrixVectorLazyVectorField.setSelected(config.getSimulationMatrixdUseLazyVector());
        configUI.matrixStoreResultOnDiskField.setText(String.valueOf(config.getSimulationStoreResultOnDisk()));
        configUI.matrixStoreResultCacheStepField.setText(String.valueOf(config.getSimulationStoreResultCacheStep()));
        configUI.exportForceCompressionField.setSelected(config.getExportForceCompression());
        // vcs
        configUI.vcsUsernameField.setText(StringUtils.stripToEmpty(config.getVcsUserName()));
        configUI.vcsPasswordField.setText(StringUtils.stripToEmpty(config.getVcsUserPassword()));
        configUI.vcsCommunityUsernameField.setText(StringUtils.stripToEmpty(config.getVcsCommunityUserName()));
        configUI.vcsCommunityPasswordField.setText(StringUtils.stripToEmpty(config.getVcsCommunityUserPassword()));
    }

    protected void saveCurrentConfig() {
        IsisConfig config = IsisFish.config;
        // general
        config.setIsisHomeDirectory(StringUtils.trimToNull(configUI.isisHomeDirectoryField.getText()));
        config.setLocale((String) configUI.localeCombo.getSelectedItem());
        // simulator
        config.setSimulatorInMaxThreads(Integer.parseInt(configUI.simulationInMaxThreadsField.getText()));
        config.setSimulatorSubMaxProcess(Integer.parseInt(configUI.simulationSubMaxProcessField.getText()));
        config.setSimulatorSubMaxMemory(StringUtils.trimToNull(configUI.simulationSubMaxMemoryField.getText()));
        // simulation
        config.setSimulationMatrixVectorClass((Class) configUI.matrixVectorClassField.getSelectedItem());
        config.setSimulationMatrixVectorSparseClass((Class) configUI.matrixVectorSparseClassField.getSelectedItem());
        config.setSimulationMatrixThresholdUseSparse(Integer.parseInt(configUI.matrixVectorThresholdField.getText()));
        config.setSimulationMatrixdUseLazyVector(configUI.matrixVectorLazyVectorField.isSelected());
        config.setSimulationStoreResultOnDisk(Integer.parseInt(configUI.matrixStoreResultOnDiskField.getText()));
        config.setSimulationStoreResultCacheStep(Integer.parseInt(configUI.matrixStoreResultCacheStepField.getText()));
        config.setExportForceCompression(configUI.exportForceCompressionField.isSelected());
        // vcs
        config.setVcsUserName(StringUtils.trimToNull(configUI.vcsUsernameField.getText()));
        config.setVcsUserPassword(StringUtils.trimToNull(configUI.vcsPasswordField.getText()));
        config.setVcsCommunityUserName(StringUtils.trimToNull(configUI.vcsCommunityUsernameField.getText()));
        config.setVcsCommunityUserPassword(StringUtils.trimToNull(configUI.vcsCommunityPasswordField.getText()));

        config.saveForUser();
    }

    public void cancel() {
        configUI.dispose();
    }

    public void save() {
        try {
            saveCurrentConfig();
            configUI.dispose();
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(configUI, t("isisfish.simulator.configuration.cantParseConfig"),
                    t("isisfish.simulator.configuration.error"), JOptionPane.ERROR_MESSAGE);
        }
    }
}
