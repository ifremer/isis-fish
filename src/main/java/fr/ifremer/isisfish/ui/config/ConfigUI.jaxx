<!--
  #%L
  IsisFish
  
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2019 - 2020 Ifremer, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->
<JDialog title="isisfish.simulator.configuration.title" layout="{new BorderLayout()}">
    <ConfigHandler id="handler" constructorParams='this' />

    <import>
    org.nuiton.math.matrix.Vector
    </import>

    <script><![CDATA[
    protected void $afterCompleteSetup() {
        handler.afterInit();
    }
    ]]></script>

    <Table constraints="BorderLayout.CENTER">
        <row>
            <cell columns="2" fill="horizontal">
                <JLabel id="isisConfigFileLocationLabel" />
            </cell>
        </row>
        <row>
            <cell weightx='1.0' weighty='1.0' fill="both" columns="2">
                <JTabbedPane>
                    <tab title='isisfish.simulator.configuration.general.title'>
                        <JPanel layout="{new BorderLayout()}">
                            <Table constraints="BorderLayout.NORTH">
                                <row>
                                    <cell fill="horizontal">
                                        <JLabel text="isisfish.config.main.compileDirectory.description" />
                                    </cell>
                                    <cell weightx='1.0' fill="horizontal">
                                        <JTextField id="isisHomeDirectoryField" />
                                    </cell>
                                </row>
                                <row>
                                    <cell fill="horizontal">
                                        <JLabel text="isisfish.config.main.locale.description" />
                                    </cell>
                                    <cell fill="horizontal">
                                        <JComboBox id="localeCombo" genericType="String" constructorParams='new String[] { "fr_FR", "en_GB" }' />
                                    </cell>
                                </row>
                             </Table>
                         </JPanel>
                    </tab>
                    <tab title='isisfish.simulator.configuration.tabSimulator'>
                        <JPanel layout="{new BorderLayout()}">
                            <Table constraints="BorderLayout.NORTH">
                                <row>
                                    <cell fill="horizontal">
                                        <JLabel text="isisfish.config.main.simulation.in.max.threads.description" />
                                    </cell>
                                    <cell weightx='1.0' fill="horizontal">
                                        <JTextField id="simulationInMaxThreadsField" />
                                    </cell>
                                </row>
                                <row>
                                    <cell fill="horizontal">
                                        <JLabel text="isisfish.config.main.simulation.sub.max.process.description"
                                                toolTipText="isisfish.config.main.simulation.sub.max.process.longDescription" />
                                    </cell>
                                    <cell fill="horizontal">
                                        <JTextField id="simulationSubMaxProcessField" />
                                    </cell>
                                </row>
                                <row>
                                    <cell fill="horizontal">
                                        <JLabel text="isisfish.config.main.simulation.sub.max.memory.description" />
                                    </cell>
                                    <cell fill="horizontal">
                                        <JTextField id="simulationSubMaxMemoryField" />
                                    </cell>
                                </row>
                             </Table>
                        </JPanel>
                    </tab>
                    <tab title='isisfish.simulator.configuration.tabSimulation'>
                        <JPanel layout="{new BorderLayout()}">
                            <Table constraints="BorderLayout.NORTH">
                                <row>
                                    <cell fill="horizontal">
                                        <JLabel text="isisfish.config.simulation.matrix.vector.class.description" />
                                    </cell>
                                    <cell weightx='1.0' fill="horizontal">
                                        <JComboBox id="matrixVectorClassField" genericType="Class&lt;? extends Vector&gt;"
                                            renderer='{new fr.ifremer.isisfish.ui.config.models.ClassListRenderer()}' />
                                    </cell>
                                </row>
                                <row>
                                    <cell fill="horizontal">
                                        <JLabel text="isisfish.config.simulation.matrix.vector.sparse.class.description" />
                                    </cell>
                                    <cell fill="horizontal">
                                        <JComboBox id="matrixVectorSparseClassField" genericType="Class&lt;? extends Vector&gt;"
                                            renderer='{new fr.ifremer.isisfish.ui.config.models.ClassListRenderer()}' />
                                    </cell>
                                </row>
                                <row>
                                    <cell fill="horizontal">
                                        <JLabel text="isisfish.config.simulation.matrix.threshold.use.sparse.class.description"
                                                toolTipText="isisfish.config.simulation.matrix.threshold.use.sparse.class.longDescription" />
                                    </cell>
                                    <cell fill="horizontal">
                                        <JTextField id="matrixVectorThresholdField" />
                                    </cell>
                                </row>
                                <row>
                                    <cell fill="horizontal">
                                        <JLabel text="isisfish.config.simulation.matrix.use.lazy.vector.description"
                                                toolTipText="isisfish.config.simulation.matrix.use.lazy.vector.longDescription" />
                                    </cell>
                                    <cell fill="horizontal">
                                        <JCheckBox id="matrixVectorLazyVectorField" />
                                    </cell>
                                </row>
                                <row>
                                    <cell fill="horizontal">
                                        <JLabel text="isisfish.config.simulation.store.result.ondisk.description"
                                                toolTipText="isisfish.config.simulation.store.result.ondisk.longDescription"/>
                                    </cell>
                                    <cell fill="horizontal">
                                        <JTextField id="matrixStoreResultOnDiskField" />
                                    </cell>
                                </row>
                                <row>
                                    <cell fill="horizontal">
                                        <JLabel text="isisfish.config.simulation.store.result.cachestep.description"
                                                toolTipText="isisfish.config.simulation.store.result.cachestep.longDescription"/>
                                    </cell>
                                    <cell fill="horizontal">
                                        <JTextField id="matrixStoreResultCacheStepField" />
                                    </cell>
                                </row>
                                <row>
                                    <cell fill="horizontal">
                                        <JLabel text="isisfish.config.export.force.compression.description" />
                                    </cell>
                                    <cell fill="horizontal">
                                        <JCheckBox id="exportForceCompressionField" />
                                    </cell>
                                </row>
                             </Table>
                        </JPanel>
                    </tab>
                    <tab title='isisfish.simulator.configuration.tabVCS'>
                        <JPanel layout="{new BorderLayout()}">
                            <Table constraints="BorderLayout.NORTH">
                                <row>
                                    <cell fill="horizontal">
                                        <JLabel text="isisfish.config.vcs.userName.description" />
                                    </cell>
                                    <cell weightx='1.0' fill="horizontal">
                                        <JTextField id="vcsUsernameField" />
                                    </cell>
                                </row>
                                <row>
                                    <cell fill="horizontal">
                                        <JLabel text="isisfish.config.vcs.userPassword.description" />
                                    </cell>
                                    <cell fill="horizontal">
                                        <JTextField id="vcsPasswordField" />
                                    </cell>
                                </row>
                                <row>
                                    <cell fill="horizontal">
                                        <JLabel text="isisfish.config.vcs.community.username.description" />
                                    </cell>
                                    <cell fill="horizontal">
                                        <JTextField id="vcsCommunityUsernameField" />
                                    </cell>
                                </row>
                                <row>
                                    <cell fill="horizontal">
                                        <JLabel text="isisfish.config.vcs.community.password.description" />
                                    </cell>
                                    <cell fill="horizontal">
                                        <JTextField id="vcsCommunityPasswordField" />
                                    </cell>
                                </row>
                             </Table>
                         </JPanel>
                    </tab>
                </JTabbedPane>
            </cell>
        </row>
        <row>
            <cell columns="2" fill="horizontal">
                <JSeparator/>
            </cell>
        </row>
        <row>
            <cell weightx='1.0' anchor='east'>
                <JButton text="isisfish.common.cancel" icon="fatcow/cancel.png" onActionPerformed="handler.cancel()" />
            </cell>
            <cell>
                <JButton text="isisfish.common.save" icon="fatcow/diskette.png" onActionPerformed="handler.save()"/>
            </cell>
        </row>
    </Table>
</JDialog>
