/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2020 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.config;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.KeyPair;
import com.jcraft.jsch.Session;
import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.ui.keystore.PasswordManager;
import fr.ifremer.isisfish.util.ssh.InvalidPassphraseException;
import fr.ifremer.isisfish.util.ssh.SSHAgent;
import fr.ifremer.isisfish.util.ssh.SSHException;
import fr.ifremer.isisfish.util.ssh.SSHUserInfo;
import fr.ifremer.isisfish.util.ssh.SSHUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

import javax.swing.JComponent;
import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.net.SocketException;

import static org.nuiton.i18n.I18n.t;

/**
 * Action for SSHLauncherConfig UI.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class SSHLauncherConfigHandler {

    /** Class logger. */
    private static final Log log = LogFactory.getLog(SSHLauncherConfigHandler.class);

    protected SSHLauncherConfigUI configUI;

    protected String currentSSHserver;
    protected String currentSSHUsername;
    protected File currentSSHKey;
    protected String currentFTPserver;
    protected String currentFTPUsername;
    protected String currentSSHIsisHome;
    protected String currentSSHJavaPath;
    protected String currentSSHPbsQsubOptions;
    protected String currentSSHMaxThreads;
    protected String currentSSHMaxMemory;
    protected String currentSSHControlInterval;

    /**
     * Constructor with UI.
     * 
     * @param configUI config ui
     */
    public SSHLauncherConfigHandler(SSHLauncherConfigUI configUI) {
        this.configUI = configUI;
        configUI.setLayout(new javax.swing.BoxLayout(configUI.getContentPane(), javax.swing.BoxLayout.PAGE_AXIS));
    }
    
    protected void afterInit() {
        // fill default values
        resetSSHConfiguration();
    }

    /**
     * Reset values with default configuration values.
     */
    public void resetSSHConfiguration() {
        // server config (SSH)
        configUI.getSshServerField().setText(IsisFish.config.getSimulatorSshServer());
        configUI.getSshUsernameField().setText(IsisFish.config.getSimulatorSshUsername());
        configUI.getSshKeyField().setText(IsisFish.config.getSSHPrivateKeyFilePath().getAbsolutePath());

        // server config (FTP)
        configUI.getFtpServerField().setText(IsisFish.config.getSimulatorFtpServer());
        configUI.getFtpLoginField().setText(IsisFish.config.getSimulatorFtpLogin());

        // caparmor config
        configUI.getSshIsisHomeField().setText(IsisFish.config.getSimulatorSshIsisHome());
        configUI.getSshJavaPathField().setText(IsisFish.config.getSimulatorSshJavaPath());
        configUI.getSshPbsQsubOptionsField().setText(IsisFish.config.getSimulatorSshPbsQsubOptions());
        configUI.getSshControlIntervalField().setText(String.valueOf(IsisFish.config.getSimulatorSshControlCheckInterval()));
        configUI.getSshMaxThreadsField().setText(String.valueOf(IsisFish.config.getSimulatorSshMaxThreads()));
        configUI.getSshMaxMemoryField().setText(IsisFish.config.getSimulatorSshMaxMemory());

        doCheck();
    }

    /**
     * Reset display.
     */
    protected void configurationChanged() {
        
        configUI.setConnected(false);
        configUI.getMessageSSHLabel().setText("");
        configUI.getMessageFTPLabel().setText("");
        configUI.getMessageMiscLabel().setText("");
    }

    /**
     * Check input format validity.
     */
    public void doCheck() {

        configurationChanged();

        // copy values
        currentSSHserver = configUI.getSshServerField().getText().trim();
        if (currentSSHserver.isEmpty()) {
            setColor(true, configUI.getSshServerField());
        } else {
            setColor(false, configUI.getSshServerField());
        }

        currentSSHUsername = configUI.getSshUsernameField().getText().trim();
        if (!currentSSHUsername.matches("\\w+")) {
            setColor(true, configUI.getSshUsernameField());
        } else {
            setColor(false, configUI.getSshUsernameField());
        }

        // if ssh key is set, must be a valid file
        currentSSHKey = new File(configUI.getSshKeyField().getText().trim());
        if (currentSSHKey.isFile() && currentSSHKey.canRead()) {
            configUI.getSshKeyButton().setEnabled(false);
            configUI.getSshKeyField().setEnabled(false);
            setColor(false, configUI.getSshKeyField());
        } else {
            setColor(true, configUI.getSshKeyField());
        }

        currentFTPserver = configUI.getFtpServerField().getText().trim();
        if (currentFTPserver.isEmpty()) {
            setColor(true, configUI.getFtpServerField());
        } else {
            setColor(false, configUI.getFtpServerField());
        }

        currentFTPUsername = configUI.getFtpLoginField().getText().trim();
        if (!currentFTPUsername.matches("\\w+")) {
            setColor(true, configUI.getFtpLoginField());
        } else {
            setColor(false, configUI.getFtpLoginField());
        }

        currentSSHIsisHome = configUI.getSshIsisHomeField().getText().trim();
        if (currentSSHIsisHome.isEmpty()) {
            setColor(true, configUI.getSshIsisHomeField());
        } else {
            setColor(false, configUI.getSshIsisHomeField());
        }

        currentSSHJavaPath = configUI.getSshJavaPathField().getText().trim();
        if (currentSSHJavaPath.isEmpty()) {
            setColor(true, configUI.getSshJavaPathField());
        } else {
            setColor(false, configUI.getSshJavaPathField());
        }

        // currentSSHPbsQsubOptions
        currentSSHPbsQsubOptions = configUI.getSshPbsQsubOptionsField().getText().trim();

        // currentSSHMaxThreads
        currentSSHMaxThreads = configUI.getSshMaxThreadsField().getText().trim();
        if (!currentSSHMaxThreads.matches("\\d+")) {
            setColor(true, configUI.getSshMaxThreadsField());
        } else {
            setColor(false, configUI.getSshMaxThreadsField());
        }

        // currentSSHMaxMemory
        currentSSHMaxMemory = configUI.getSshMaxMemoryField().getText().trim();
        
        // currentSSHControlInterval
        currentSSHControlInterval = configUI.getSshControlIntervalField().getText().trim();
        if (!currentSSHControlInterval.matches("\\d+")) {
            setColor(true, configUI.getSshControlIntervalField());
        } else {
            setColor(false, configUI.getSshControlIntervalField());
        }
    }

    /**
     * Set values in config and force configuration save.
     */
    public void saveSSHConfiguration() {
        IsisFish.config.setSimulatorSshServer(currentSSHserver);
        IsisFish.config.setSimulatorSshUsername(currentSSHUsername);
        IsisFish.config.setSimulatorFtpServer(currentFTPserver);
        IsisFish.config.setSimulatorFtpLogin(currentFTPUsername);

        // pour eviter de faire un new File("")
        if (StringUtils.isBlank(configUI.getSshKeyField().getText())) {
            IsisFish.config.setSSHPrivateKeyFilePath(null);
        } else {
            IsisFish.config.setSSHPrivateKeyFilePath(currentSSHKey);
        }

        IsisFish.config.setSimulatorSshIsisHome(currentSSHIsisHome);
        IsisFish.config.setSimulatorSshJavaPath(currentSSHJavaPath);
        IsisFish.config.setSimulatorSshPbsQsubOptions(currentSSHPbsQsubOptions);
        IsisFish.config.setSimulatorSshControlCheckInterval(Integer.parseInt(currentSSHControlInterval));
        IsisFish.config.setSimulatorSshMaxThreads(Integer.parseInt(currentSSHMaxThreads));
        IsisFish.config.setSimulatorSshMaxMemory(currentSSHMaxMemory);

        IsisFish.config.saveForUser();
        configUI.dispose();
    }

    /**
     * Close frame.
     */
    public void cancelSSHConfiguration() {
        configUI.dispose();
    }

    /**
     * Realise une connexion SSH et FTP.
     */
    public void testSSHConfiguration() {
        configurationChanged();

        boolean ssh = testSSHOnlyConfiguration();
        boolean ftp = testFTPOnlyConfiguration();
        if (ssh && ftp) {
            configUI.setConnected(true);
        }
    }

    /**
     * Realise une connexion ssh et teste les données.
     */
    protected boolean testSSHOnlyConfiguration() {
        boolean result = false;
        JSch jsch = new JSch();

        String host = currentSSHserver;
        int port = 22; // by default, 22
        String sPort = null;

        configUI.getMessageSSHLabel().setText(t("isisfish.simulator.ssh.configuration.connecting"));

        Session session = null;
        try {
            if (host.indexOf(':') > 0) {
                sPort = host.substring(host.indexOf(':') + 1);
                port = Integer.parseInt(sPort);
                host = host.substring(0, host.indexOf(':'));
            }

            // add ssh key
            boolean sshKeyUsed = false;
            if (currentSSHKey.isFile() && currentSSHKey.canRead()) {
                if (log.isInfoEnabled()) {
                    log.info(String.format("Ssh key found '%s' will be used to connect to %s", currentSSHKey.getAbsoluteFile(), host));
                }
                jsch.addIdentity(currentSSHKey.getAbsolutePath());
                sshKeyUsed = true;
            } else {
                if (log.isInfoEnabled()) {
                    log.info(String.format("Can't read ssh key : %s", currentSSHKey));
                }
            }

            session = jsch.getSession(currentSSHUsername, host, port);

            // username and password will be given via UserInfo interface.
            SSHUserInfo ui = new SSHUserInfo();
            if (sshKeyUsed) {
                char[] passchars = SSHAgent.getAgent().getPassphrase(currentSSHKey);
                if (passchars != null) {
                    String passphrase = String.valueOf(passchars);
                    ui.setPassphrase(passphrase);
                }
                setTestSSHMessage(t("isisfish.simulator.ssh.configuration.connectingpk"), false);
            }
            session.setUserInfo(ui);
            session.connect(10000); // timeout

            setTestSSHMessage(t("isisfish.simulator.ssh.configuration.connectionok"), false);

            result = testMiscConfiguration(session);
        } catch (NumberFormatException e) {
            if (log.isErrorEnabled()) {
                log.error("Can't connect", e);
            }
            setTestSSHMessage(t("isisfish.error.simulation.remote.wrongportvalue", sPort), true);
        } catch (JSchException e) {
            if (log.isErrorEnabled()) {
                log.error("Can't connect", e);
            }
            setTestSSHMessage(t("isisfish.simulator.ssh.configuration.connectionerror", e.getMessage()), true);
        } catch (InvalidPassphraseException e) {
            if (log.isErrorEnabled()) {
                log.error("Can't connect", e);
            }
            setTestSSHMessage(t("isisfish.simulator.ssh.configuration.invalidpassphrase"), true);
        } finally {
            if (session != null) {
                session.disconnect();
            }
        }

        return result;
    }

    /**
     * Test FTP connection.
     */
    protected boolean testFTPOnlyConfiguration() {
        boolean result = false;

        String host = currentFTPserver;
        int port = 21; // by default, 21
        String sPort;

        configUI.getMessageFTPLabel().setText(t("isisfish.simulator.ssh.configuration.ftpconnecting"));

        FTPClient ftp = null;
        try {
            if (host.indexOf(':') > 0) {
                sPort = host.substring(host.indexOf(':') + 1);
                port = Integer.parseInt(sPort);
                host = host.substring(0, host.indexOf(':'));
            }

            ftp = new FTPClient();
            ftp.connect(host, port);
            int reply = ftp.getReplyCode();
            if (FTPReply.isPositiveCompletion(reply)) {
                setTestFTPMessage(t("isisfish.simulator.ssh.configuration.ftpconnectedtrylogin"), false);

                String login = currentFTPUsername;
                boolean success;
                int tryLogin = 0;
                boolean forceNewPassword = false;
                String password;
                do {
                    password = PasswordManager.getInstance().getPassword(String.format("%s@%s", login, host), forceNewPassword);
                    success = ftp.login(login, password);
                    forceNewPassword = true;
                    tryLogin++;
                } while (!success && tryLogin < 3 && password != null);

                if (success) {
                    result = true;
                    setTestFTPMessage(t("isisfish.simulator.ssh.configuration.ftpconnectionok"), false);
                } else {
                    setTestFTPMessage(t("isisfish.simulator.ssh.configuration.ftpconnectedcantlogin"), true);
                }

            } else {
                setTestFTPMessage(t("isisfish.simulator.ssh.configuration.ftpinvalidreplycode", reply), true);
            }

            ftp.disconnect();

        } catch (SocketException e) {
            if (log.isErrorEnabled()) {
                log.error("Can't open socket", e);
            }
            setTestFTPMessage(t("isisfish.simulator.ssh.configuration.ftperror", e.getMessage()), true);
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Can't connect", e);
            }
            setTestFTPMessage(t("isisfish.simulator.ssh.configuration.ftperror", e.getMessage()), true);
        } finally {
            if (ftp != null) {
                try {
                    ftp.disconnect();
                } catch (IOException e) {
                    // ignored
                }
            }
        }

        return result;
    }

    /**
     * Test some others configuration.
     */
    protected boolean testMiscConfiguration(Session session) {
        boolean result = true;

        try {
            if (result) {
                if (SSHUtils.exec(session, "test -d \"" + currentSSHIsisHome + "\" && test -x \"" + currentSSHIsisHome + "\"") != 0) {
                    result = false;
                    setTestMiscMessage(t("isisfish.simulator.ssh.configuration.miscisishomenotvalid"), true);
                }
            }

            if (result) {
                if (SSHUtils.exec(session, "test -f \"" + currentSSHJavaPath + "\" && test -x \"" + currentSSHJavaPath + "\"") != 0) {
                    result = false;
                    setTestMiscMessage(t("isisfish.simulator.ssh.configuration.miscjavapathnotvalid"), true);
                }
            }

            if (result) {
                setTestMiscMessage(t("isisfish.simulator.ssh.configuration.miscconfigurationok"), false);
            }
        } catch (SSHException e) {
            setTestMiscMessage(t("isisfish.simulator.ssh.configuration.miscconfigurationerror", e.getMessage()), true);
        }

        return result;
    }

    /**
     * Generate new SSH key.
     */
    protected void generateSSHKey() {
        if (currentSSHKey.exists()) {
            throw new IllegalArgumentException("Can't overwrite ssh key");
        }

        try {
            // make parent dir
            if (currentSSHKey.getParentFile() != null && !currentSSHKey.getParentFile().exists()) {
                currentSSHKey.getParentFile().mkdirs();
            }
            JSch jsch = new JSch();
            KeyPair kpair = KeyPair.genKeyPair(jsch, KeyPair.RSA, 2048);
            //kpair.setPassphrase(passphrase);
            kpair.writePrivateKey(currentSSHKey.getAbsolutePath());
            kpair.writePublicKey(currentSSHKey.getAbsolutePath() + ".pub",currentSSHUsername + "@forIsisFish");
            if (log.isInfoEnabled()) {
                log.info("Finger print: " + kpair.getFingerPrint());
            }
            kpair.dispose();
        } catch (JSchException | IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Can't make ssh key", e);
            }
        }

        // refresh ckeck
        doCheck();
    }

    /**
     * Set color depending of field validity.
     * 
     * @param invalid valid field
     * @param component component to set color
     */
    protected void setColor(boolean invalid, JComponent component) {
        component.setForeground(invalid ? Color.RED : Color.BLACK);
    }

    /**
     * Set message in message field with status color.
     * 
     * @param message message to display
     * @param error error status
     */
    protected void setTestSSHMessage(String message, boolean error) {
        configUI.getMessageSSHLabel().setForeground(error ? Color.RED : Color.GREEN.darker());
        configUI.getMessageSSHLabel().setText(message);
    }

    /**
     * Set message in message field with status color.
     *
     * @param message message to display
     * @param error error status
     */
    protected void setTestFTPMessage(String message, boolean error) {
        configUI.getMessageFTPLabel().setForeground(error ? Color.RED : Color.GREEN.darker());
        configUI.getMessageFTPLabel().setText(message);
    }

    /**
     * Set message in message field with status color.
     *
     * @param message message to display
     * @param error error status
     */
    protected void setTestMiscMessage(String message, boolean error) {
        configUI.getMessageMiscLabel().setForeground(error ? Color.RED : Color.GREEN.darker());
        configUI.getMessageMiscLabel().setText(message);
    }
}
