/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui;

import jaxx.runtime.JAXXContext;
import jaxx.runtime.JAXXUtil;
import fr.ifremer.isisfish.ui.WelcomePanelUI;

/**
 * Common action for all handler.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class CommonHandler {

    /**
     * Set main application status bar context.
     * 
     * @param context context
     * @param s string to set
     */
    protected void setStatusMessage(JAXXContext context, String s) {
        WelcomePanelUI root = context.getContextValue(WelcomePanelUI.class, JAXXUtil.PARENT);
        if (root != null) {
            root.setStatusMessage(s);
        }
    }
    
    /**
     * Set main application status bar context.
     * 
     * @param context context
     * @param s string to set
     * @param running enable progress bar running state
     */
    protected void setStatusMessage(JAXXContext context, String s, boolean running) {
        WelcomePanelUI root = context.getContextValue(WelcomePanelUI.class, JAXXUtil.PARENT);
        if (root != null) {
            root.setStatusMessage(s, running);
        }
    }
}
