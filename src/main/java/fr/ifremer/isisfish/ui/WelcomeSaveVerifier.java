/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2015 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui;

import java.util.ArrayList;
import java.util.List;

/**
 * WelcomeSaveVerifier.
 * 
 * @author letellier
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class WelcomeSaveVerifier {

    /** Save verifier list. */
    protected List<SaveVerifier> verifiers = new ArrayList<>();

    /**
     * Add new verifier to manage.
     * 
     * @param saveVerifier new verifier
     */
    public void addSaveVerifier(SaveVerifier saveVerifier) {
        verifiers.add(saveVerifier);
    }

    /**
     * Check that all 'input' opened interface has no non-saved modification.
     * 
     * @return {@code true} if interface can be closed
     */
    public boolean allIsSaved() {
        boolean canExit = true;

        for (SaveVerifier s : verifiers) {
            int reponse = s.checkEdit();

            // return false, if at least one verifier
            // return cancel option
            if (reponse == SaveVerifier.CANCEL_OPTION) {
                canExit = false;
            }
        }

        return canExit;
    }

}
