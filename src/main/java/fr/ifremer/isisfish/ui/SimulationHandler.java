/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 - 2020 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui;

import static org.nuiton.i18n.I18n.t;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

import javax.swing.JFileChooser;

import fr.ifremer.isisfish.simulator.launcher.SimulationService;
import fr.ifremer.isisfish.ui.simulator.SimulatorContext;
import fr.ifremer.isisfish.util.IsisFileUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.ifremer.isisfish.simulator.SimulationParameter;
import fr.ifremer.isisfish.util.ErrorHelper;

/**
 * Common action for all handler.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class SimulationHandler {

    private static final Log log = LogFactory.getLog(SimulationHandler.class);

    protected SimulationUI simulationUI;

    public SimulationHandler(SimulationUI simulationUI) {
        this.simulationUI = simulationUI;
    }

    protected SimulationParameter getParameters() {
        return simulationUI.getContextValue(SimulationParameter.class);
    }
    
    public void afterInit() {
        simulationUI.addPropertyChangeListener(SimulationUI.PROPERTY_REGION_STORAGE, evt -> simulationUI.regionStorageChanged());

        // FIXME c'est un hack qu'il faudrait supprimer
        // c'est utilisé à l'arrache par certaines interfaces pour mettre à jour l'arbre de facteur
        simulationUI.setContextValue(simulationUI, "SimulationUI");
    }

    /**
     * Open user dialog to selected a parameter file.
     * Reload it, and call {@link SimulationUI#refresh()}.
     */
    public void importSimulation() {
        JFileChooser fc = new JFileChooser();
        int returnVal = fc.showOpenDialog(simulationUI);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            //This is where a real application would open the file.
            importSimulation(file);
            simulationUI.refresh();
        }
    }

    /**
     * Save current parameter in user selected parameter output file.
     */
    public void saveSimulation() {
        JFileChooser fc = new JFileChooser();
        int returnVal = fc.showSaveDialog(simulationUI);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            //This is where a real application would open the file.
            saveSimulation(file);
        }
    }
    
    /**
     * Load simulation parameter file.
     * 
     * @param f
     * 
     * TODO public just for sensitivity
     */
    protected void importSimulation(File f) {

        try (FileInputStream fos = new FileInputStream(f)) {
            
            Properties proper = new Properties();
            proper.load(fos);

            SimulatorContext context = simulationUI.getContextValue(SimulatorContext.class, "SimulatorContext");
            context.initDefaultParameters();
            getParameters().fromProperties(proper);
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Can't import simulation", e);
            }
            ErrorHelper.showErrorDialog(t("isisfish.error.simulation.importparameter"), e);
        }
    }

    /**
     * Save current simulation to given file.
     * 
     * @param f file to save simulation to
     */
    protected void saveSimulation(File f) {
        try (FileOutputStream fos = new FileOutputStream(f)) {
            getParameters().toProperties().store(fos, null); // XXX: was simulName instead of null
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Can't save simulation", e);
            }
            ErrorHelper.showErrorDialog(t("isisfish.error.simulation.savesimulation"), e);
        }
    }

    /**
     * Open user dialog to selected a ZIP file.
     */
    public void prepareSimulation() {
        JFileChooser fc = new JFileChooser();
        int returnVal = fc.showOpenDialog(simulationUI);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            file = IsisFileUtil.addExtensionIfNeeded(file, "zip");

            SimulationParameter params = getParameters();
            SimulationService.getService().prepareSimulationZipFile(params, file, null, null, null);
        }
    }
}
