/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2020 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.ui.keystore;

import fr.ifremer.isisfish.util.UIUtil;
import jaxx.runtime.SwingUtil;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import static org.nuiton.i18n.I18n.t;

public class PasswordUI extends JDialog implements WindowListener {

    protected JLabel accountLabel;

    protected JPasswordField accountPassword;

    protected JCheckBox rememberPassword;

    protected boolean canceled;

    public PasswordUI(Frame owner) {
        super(owner, true);
        setTitle(t("isisfish.ui.keystore.title"));
        buildUI();
        setResizable(false);
        UIUtil.setIconImage(this);
        addWindowListener(this);
    }

    protected void buildUI() {
        setLayout(new GridBagLayout());
        setMinimumSize(new Dimension(400, 0));

        // key icon
        JLabel iconLabel = new JLabel();
        iconLabel.setIcon(SwingUtil.createIcon("/icons/fatcow/key.png"));
        add(iconLabel, new GridBagConstraints(0, 0, 1, 3, 0, 0, GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(10, 10, 10, 10), 0, 0));

        // account label
        accountLabel = new JLabel();
        add(accountLabel, new GridBagConstraints(1, 0, 2, 1, 1.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(10, 3, 3, 10), 0, 0));

        // accountPassword field
        accountPassword = new JPasswordField();
        add(accountPassword, new GridBagConstraints(1, 1, 2, 1, 1.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(3, 3, 3, 10), 0, 0));

        // remember field
        rememberPassword = new JCheckBox();
        add(rememberPassword, new GridBagConstraints(1, 2, 2, 1, 1.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(3, 3, 10, 10), 0, 0));

        // ok button
        JButton okButton = new JButton();
        okButton.addActionListener(e -> dispose());
        okButton.setText(t("isisfish.common.ok"));
        add(okButton, new GridBagConstraints(1, 3, 1, 1, 1.0, 1.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(3, 3, 3, 3), 0, 0));

        // ok button
        JButton cancelButton = new JButton();
        cancelButton.addActionListener(e -> cancel());
        cancelButton.setText(t("isisfish.common.cancel"));
        add(cancelButton, new GridBagConstraints(2, 3, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(3, 3, 3, 3), 0, 0));
    }

    public String getPassword(String info) {
        canceled = false;

        if (info.contains("@")) {
            accountLabel.setText(t("isisfish.ui.keystore.question", info));
            rememberPassword.setText(t("isisfish.ui.keystore.rememberPassword"));
        } else {
            accountLabel.setText(t("isisfish.ui.keystore.askPassphrase", info));
            rememberPassword.setText(t("isisfish.ui.keystore.rememberPassphrase"));
        }

        pack();
        setLocationRelativeTo(null);
        setVisible(true);

        String password = null;
        if (!canceled) {
            password = new String(accountPassword.getPassword());
        }
        return password;
    }

    public boolean isRememberPassword() {
        return rememberPassword.isSelected();
    }

    protected void cancel() {
        canceled = true;
        dispose();
    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {
        cancel();
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}
