/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2020 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.ui.keystore;

import fr.ifremer.isisfish.IsisFish;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.net.util.Base64;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Password manager store password encoded as base64 using following structure:
 * id1:b64(pwd1);id2:b64(pwd2);
 */
public class PasswordManager {

    protected static final String ACCOUNT_SEPARATOR = ";";

    protected static final String PASSWORD_SEPARATOR = ":";

    protected static PasswordManager instance;

    protected Map<String, String> storedPasswords = new HashMap<>();
    protected Map<String, String> memoryPassword = new HashMap<>();

    public static PasswordManager getInstance() {
        if (instance == null) {
            instance = new PasswordManager();
        }
        return instance;
    }

    protected PasswordManager() {
        parsePasswords();
    }

    protected void parsePasswords() {
        String keystorePasswords = IsisFish.config.getKeystorePasswords();
        storedPasswords.clear();
        if (StringUtils.isNotBlank(keystorePasswords)) {
            String[] accounts = keystorePasswords.split(Pattern.quote(ACCOUNT_SEPARATOR));
            for (String account : accounts) {
                String[] all = account.split(PASSWORD_SEPARATOR, 2);
                String pwd = new String(Base64.decodeBase64(all[1]));
                storedPasswords.put(all[0], pwd);
            }
        }
    }

    protected void addPassword(String identifier, String password, boolean store) {
        memoryPassword.put(identifier, password);
        if (store) {
            storedPasswords.put(identifier, password);
            storePasswords();
        }
    }

    protected void storePasswords() {
        String passwordAsString = storedPasswords.entrySet().stream()
                .map(e -> e.getKey() + PASSWORD_SEPARATOR + Base64.encodeBase64String(e.getValue().getBytes(StandardCharsets.UTF_8)))
                .collect(Collectors.joining(ACCOUNT_SEPARATOR));
        IsisFish.config.setKeystorePasswords(passwordAsString);
        IsisFish.config.saveForUser();
    }

    public String getPassword(String identifier) {
        return getPassword(identifier, false);
    }

    public synchronized String getPassword(String identifier, boolean forceNew) {
        String current = storedPasswords.get(identifier);
        if (current == null) {
            current = memoryPassword.get(identifier);
        }
        if (current == null || forceNew) {
            PasswordUI passwordUI = new PasswordUI(null);
            String password = passwordUI.getPassword(identifier);
            if (password == null) {
                current = null;
                memoryPassword.remove(identifier);
            } else {
                addPassword(identifier, password, passwordUI.isRememberPassword());
                current = password;
            }
        }
        return current;
    };
}
