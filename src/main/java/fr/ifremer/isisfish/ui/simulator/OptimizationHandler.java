/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2020 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.ui.simulator;

import fr.ifremer.isisfish.IsisFishException;
import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.datastore.ExportStorage;
import fr.ifremer.isisfish.datastore.ObjectiveStorage;
import fr.ifremer.isisfish.datastore.OptimizationStorage;
import fr.ifremer.isisfish.datastore.RegionStorage;
import fr.ifremer.isisfish.datastore.StorageChangeEvent;
import fr.ifremer.isisfish.datastore.StorageChangeListener;
import fr.ifremer.isisfish.entities.FisheryRegion;
import fr.ifremer.isisfish.entities.Observation;
import fr.ifremer.isisfish.export.ExportInfo;
import fr.ifremer.isisfish.simulator.Objective;
import fr.ifremer.isisfish.simulator.Optimization;
import fr.ifremer.isisfish.simulator.SimulationParameter;
import fr.ifremer.isisfish.simulator.sensitivity.Factor;
import fr.ifremer.isisfish.simulator.sensitivity.FactorGroup;
import fr.ifremer.isisfish.ui.SimulationUI;
import fr.ifremer.isisfish.ui.models.common.ScriptParametersParamModel;
import fr.ifremer.isisfish.ui.models.common.ScriptParametersParamNameRenderer;
import fr.ifremer.isisfish.ui.models.common.ScriptParametersParamValueEditor;
import fr.ifremer.isisfish.ui.models.common.ScriptParametersParamValueRenderer;
import fr.ifremer.isisfish.ui.models.optimization.ExportObservationTableModel;
import fr.ifremer.isisfish.ui.models.optimization.ExportTableCellEditor;
import fr.ifremer.isisfish.ui.models.optimization.ExportTableCellRenderer;
import fr.ifremer.isisfish.ui.sensitivity.SensitivityInputUI;
import fr.ifremer.isisfish.ui.sensitivity.SensitivitySaveVerifier;
import fr.ifremer.isisfish.ui.widget.editor.FactorEditorListener;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.TopiaContext;

import javax.swing.JDialog;
import javax.swing.JTable;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class OptimizationHandler extends SimulatorTabHandler {

    private static final Log log = LogFactory.getLog(OptimizationHandler.class);

    protected FactorEditorListener factorEditorListener;

    protected OptimizationUI tabUI;

    protected StorageChangeListener objectiveStorageListener;

    protected StorageChangeListener optimisationStorageListener;

    protected StorageChangeListener exportStorageListener;

    public OptimizationHandler(OptimizationUI tabUI) {
        super(tabUI);
        this.tabUI = tabUI;
    }

    protected void enableOptimization() {
        boolean selected = tabUI.fieldSimulUseOptimization.isSelected();
        getParameters().setUseOptimization(selected);
        setTabSelectedIcon(tabUI, selected);

        // to unselect buttons
        tabUI.fieldExportList.clearSelection();
        tabUI.exportObservationTable.clearSelection();
    }

    protected void initFieldObjectiveMethodModel(StorageChangeEvent evt) {
        // there is some non java files in sensitivity directory
        List<String> result = ObjectiveStorage.getObjectiveNames().stream()
                .filter(r -> r.endsWith(".java"))
                .map(r -> r.substring(0, r.length() - 5))
                .collect(Collectors.toList());
        tabUI.fieldObjectiveMethodModel.setElementList(result);
    }

    protected void initFieldOptimizationMethodModel(StorageChangeEvent evt) {
        // there is some non java files in sensitivity directory
        List<String> result = OptimizationStorage.getOptimizationNames().stream()
                .filter(r -> r.endsWith(".java"))
                .map(r -> r.substring(0, r.length() - 5))
                .collect(Collectors.toList());
        tabUI.fieldOptimizationMethodModel.setElementList(result);
    }

    protected void initFieldExportListModel(StorageChangeEvent evt) {
        // there is some non java files in sensitivity directory
        List<String> result = ExportStorage.getExportNames().stream()
                .filter(r -> r.endsWith(".java"))
                .map(r -> r.substring(0, r.length() - 5))
                .collect(Collectors.toList());
        tabUI.fieldExportListModel.setElementList(result);
    }

    public void afterInit() {
        // objective
        objectiveStorageListener = this::initFieldObjectiveMethodModel;
        ObjectiveStorage.addStorageListener(objectiveStorageListener);
        initFieldObjectiveMethodModel(null);

        // optimisation
        optimisationStorageListener = this::initFieldOptimizationMethodModel;
        OptimizationStorage.addStorageListener(optimisationStorageListener);
        initFieldOptimizationMethodModel(null);

        // export
        exportStorageListener = this::initFieldExportListModel;
        ExportStorage.addStorageListener(exportStorageListener);
        initFieldExportListModel(null);

        // model init
        tabUI.getExportObservationTable().setModel(getExportObservationTableModel());
    }

    public void refresh() {
        tabUI.fieldSimulUseOptimization.setSelected(getParameters().getUseOptimization());

        // get info
        SimulationParameter param = getParameters();
        RegionStorage regionStorage = tabUI.getContextValue(RegionStorage.class);

        // refresh after simulation storage set
        if (param.getObjective() != null) {
            tabUI.getFieldObjectiveMethodSelect().setSelectedItem(ObjectiveStorage.getName(param.getObjective()));
        }
        if (param.getOptimization() != null) {
            tabUI.getFieldOptimizationMethodSelect().setSelectedItem(OptimizationStorage.getName(param.getOptimization()));
        }
        // export/observation
        tabUI.getExportObservationTable().setModel(getExportObservationTableModel());

        // order matters
        tabUI.getExportObservationTable().getColumnModel().getColumn(0).setCellRenderer(new ExportTableCellRenderer());
        ExportTableCellEditor editor = new ExportTableCellEditor();
        editor.setRegionStorage(regionStorage);
        tabUI.getExportObservationTable().getColumnModel().getColumn(1).setCellEditor(editor);
        tabUI.getExportObservationTable().getColumnModel().getColumn(1).setCellRenderer(new ExportTableCellRenderer());
    }

    /**
     * Selection d'une méthode d'optimisation.
     */
    public void objectiveChanged() {
        String objectiveName = (String)tabUI.getFieldObjectiveMethodSelect().getSelectedItem();
        if (StringUtils.isBlank(objectiveName)) {
            return;
        }

        SimulationParameter param = getParameters();
        RegionStorage regionStorage = tabUI.getContextValue(RegionStorage.class);

        // creation new instance only when name change to not lose parameters value
        Objective objective = param.getObjective();
        if (objective == null || !objective.getClass().getSimpleName().equals(objectiveName)) {
            try {
                ObjectiveStorage objectiveStorage = ObjectiveStorage.getObjective(objectiveName);
                objective = objectiveStorage.getNewInstance();
            } catch (IsisFishException e) {
                if (log.isErrorEnabled()) {
                    log.error("Can't set optimization", e);
                }
            }
        }

        // can be null for example if analysis can't be compiled
        if (objective != null) {
            param.setObjective(objective);

            // update model
            ScriptParametersParamModel parametersTableModel = new ScriptParametersParamModel(objective);
            tabUI.getSimulObjectiveMethodParam().setModel(parametersTableModel);
            ScriptParametersParamNameRenderer nameRenderer = new ScriptParametersParamNameRenderer(objective);
            tabUI.getSimulObjectiveMethodParam().getColumnModel().getColumn(0).setCellRenderer(nameRenderer);
            ScriptParametersParamValueRenderer valueRenderer = new ScriptParametersParamValueRenderer();
            tabUI.getSimulObjectiveMethodParam().getColumnModel().getColumn(1).setCellRenderer(valueRenderer);
            
            ScriptParametersParamValueEditor editor = new ScriptParametersParamValueEditor(objective);
            editor.setRegionStorage(regionStorage);
            editor.setFactorActionListener(getFactorActionListener());
            tabUI.getSimulObjectiveMethodParam().getColumnModel().getColumn(1).setCellEditor(editor);
        }
    }

    /**
     * Retourne une instance de action listener qui affichera l'interface de sélection d'un facteur.
     * 
     * @return action listener
     */
    protected FactorEditorListener getFactorActionListener() {
        if (factorEditorListener == null) {
            factorEditorListener = new FactorEditorListener() {
                public void actionPerformed(ActionEvent event) {

                    // init new sensitivity tav ui hierarchy
                    final SensitivityInputUI sensitivityTabUI = new SensitivityInputUI(tabUI);
                    sensitivityTabUI.setContextValue(new SensitivitySaveVerifier()); // prevent NPE
                    sensitivityTabUI.setContextValue(tabUI.getParentContainer(SimulationUI.class), "SimulationUI"); // prevent NPE

                    // init region
                    // FIXME this transaction in never closed
                    // and can't be closed because used in
                    try {
                        RegionStorage regionStorage = tabUI.getContextValue(RegionStorage.class);
                        TopiaContext tx = regionStorage.getStorage().beginTransaction();
                        FisheryRegion fisheryRegion = RegionStorage.getFisheryRegion(tx);
                        sensitivityTabUI.setFisheryRegion(fisheryRegion);
                        sensitivityTabUI.getHandler().setTreeModel();
                    } catch (Exception ex) {
                        throw new IsisFishRuntimeException("Can't init dialog tree", ex);
                    }

                    // display dialog
                    JDialog dialog = new JDialog();
                    dialog.setTitle("Sélection d'un facteur");
                    dialog.add(sensitivityTabUI);
                    dialog.setSize(800, 600);
                    dialog.setLocationRelativeTo(tabUI);
                    dialog.addWindowListener(new WindowAdapter() {
                        @Override
                        public void windowClosing(WindowEvent e) {
                            // get first factor
                            FactorGroup factorGroup = getFactorGroup();

                            if (factorGroup.size() > 0) {
                                Factor factor = factorGroup.get(factorGroup.size() - 1);
                                // useless, but clear for next call
                                removeFactor(factor.getPath());
                                setSelectedFactor(factor);
                            }
                        }
                    });
                    dialog.setVisible(true);
                }
            };
        }
        return factorEditorListener;
    }
    
    /**
     * Remove factor in factor group tree by path.
     * 
     * @param factorPath factor path to remove
     * @deprecated this is a hack i think, should not be here and not used at all
     */
    @Deprecated
    public void removeFactor(String factorPath) {
        removeFactor(getFactorGroup(), factorPath);
    }

    /**
     * Recursive remove for factor in factor group by path.
     * 
     * @param factorGroup factor group to search to
     * @param factorPath factor path to remove
     * @deprecated this is a hack i think, should not be here and not used at all
     */
    @Deprecated
    protected void removeFactor(FactorGroup factorGroup, String factorPath) {
        Collection<Factor> factorCopy = new ArrayList<>(factorGroup.getFactors());
        for (Factor factor : factorCopy) {
            if (factor instanceof FactorGroup) {
                removeFactor((FactorGroup)factor, factorPath);
            }
            if (factorPath.equals(factor.getPath())) {
                factorGroup.remove(factor);
            }
        }
    }

    /**
     * Selection d'une méthode d'optimisation.
     */
    public void optimizationChanged() {
        String optimizationName = (String)tabUI.getFieldOptimizationMethodSelect().getSelectedItem();
        if (StringUtils.isBlank(optimizationName)) {
            return;
        }

        SimulationParameter param = getParameters();
        RegionStorage regionStorage = tabUI.getContextValue(RegionStorage.class);

        // creation new instance only when name change to not lose parameters value
        Optimization optimization = param.getOptimization();
        if (optimization == null || !optimization.getClass().getSimpleName().equals(optimizationName)) {
            try {
                OptimizationStorage optimizationStorage = OptimizationStorage.getOptimization(optimizationName);
                optimization = optimizationStorage.getNewInstance();
            } catch (IsisFishException e) {
                if (log.isErrorEnabled()) {
                    log.error("Can't set optimization", e);
                }
            }
        }

        // can be null for example if analysis can't be compiled
        if (optimization != null) {
            param.setOptimization(optimization);

            // update model
            ScriptParametersParamModel parametersTableModel = new ScriptParametersParamModel(optimization);
            tabUI.getSimulOptimizationMethodParam().setModel(parametersTableModel);
            ScriptParametersParamNameRenderer nameRenderer = new ScriptParametersParamNameRenderer(optimization);
            tabUI.getSimulOptimizationMethodParam().getColumnModel().getColumn(0).setCellRenderer(nameRenderer);
            ScriptParametersParamValueRenderer valueRenderer = new ScriptParametersParamValueRenderer();
            tabUI.getSimulOptimizationMethodParam().getColumnModel().getColumn(1).setCellRenderer(valueRenderer);
            
            ScriptParametersParamValueEditor editor = new ScriptParametersParamValueEditor(optimization);
            editor.setRegionStorage(regionStorage);
            editor.setFactorActionListener(getFactorActionListener());
            tabUI.getSimulOptimizationMethodParam().getColumnModel().getColumn(1).setCellEditor(editor);
        }
    }

    /**
     * ExportInfo model for combo box.
     * 
     * @return model
     */
    public ExportObservationTableModel getExportObservationTableModel() {
        SimulationParameter param = getParameters();
        Map<ExportInfo, Observation> optimizationExportsObservations = param.getOptimizationExportsObservations();
        ExportObservationTableModel model = new ExportObservationTableModel(optimizationExportsObservations);
        return model;
    }

    /**
     * Add selected export.
     */
    public void addExports() {
        // get elements
        SimulationParameter param = getParameters();
        ExportObservationTableModel tableModel = (ExportObservationTableModel)tabUI.getExportObservationTable().getModel();
        
        // add all selected exports
        List<String> selectedExports = tabUI.getFieldExportList().getSelectedValuesList();
        for (String selectedExport : selectedExports) {
            try {
                // create export instance
                ExportStorage exportStorage = ExportStorage.getExport(selectedExport);
                ExportInfo export = exportStorage.getNewInstance();

                // add export instance into parameters
                Map<ExportInfo, Observation> optimizationExports = param.getOptimizationExportsObservations();
                optimizationExports.put(export, null); // new entry
                tableModel.setOptimizationExportsObservations(optimizationExports); // FIXME add fire
                param.setOptimizationExportsObservations(optimizationExports);
            } catch (IsisFishException ex) {
                throw new IsisFishRuntimeException("Can't create new export", ex);
            }
        }
    }

    /**
     * Remove selected exports in table.
     */
    public void removeExports() {
        // get element
        SimulationParameter param = getParameters();
        Map<ExportInfo, Observation> optimizationExports = param.getOptimizationExportsObservations();
        ExportObservationTableModel tableModel = (ExportObservationTableModel)tabUI.getExportObservationTable().getModel();

        JTable table = tabUI.getExportObservationTable();
        int[] rows = table.getSelectedRows();
        // reverse order
        for (int i = rows.length - 1; i >= 0; i--) {
            ExportInfo export = tableModel.getExportForRow(rows[i]);
            optimizationExports.remove(export);
            tableModel.deleteExport(export);
        }

        // force save
        param.setOptimizationExportsObservations(optimizationExports);
    }
}
