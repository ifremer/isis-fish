/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2011 - 2020 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.simulator;

import fr.ifremer.isisfish.IsisFishException;
import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.datastore.JavaSourceStorage;
import fr.ifremer.isisfish.datastore.RegionStorage;
import fr.ifremer.isisfish.datastore.RuleStorage;
import fr.ifremer.isisfish.datastore.StorageChangeEvent;
import fr.ifremer.isisfish.datastore.StorageChangeListener;
import fr.ifremer.isisfish.rule.Rule;
import fr.ifremer.isisfish.simulator.sensitivity.Factor;
import fr.ifremer.isisfish.simulator.sensitivity.FactorGroup;
import fr.ifremer.isisfish.ui.SimulationUI;
import fr.ifremer.isisfish.ui.models.common.ScriptParametersParamNameRenderer;
import fr.ifremer.isisfish.ui.models.common.ScriptParametersParamValueEditor;
import fr.ifremer.isisfish.ui.models.common.ScriptParametersParamValueRenderer;
import fr.ifremer.isisfish.ui.models.rule.RuleParametersFactorTableCellEditor;
import fr.ifremer.isisfish.ui.models.rule.RuleParametersFactorTableCellRenderer;
import fr.ifremer.isisfish.ui.widget.editor.ScriptParameterDialog;
import jaxx.runtime.SwingUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.DropMode;
import javax.swing.JList;
import javax.swing.TransferHandler;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragSource;
import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static fr.ifremer.isisfish.simulator.SimulationParameterPropertiesHelper.DOT;
import static fr.ifremer.isisfish.simulator.SimulationParameterPropertiesHelper.PARAMETERS_KEY;
import static fr.ifremer.isisfish.simulator.SimulationParameterPropertiesHelper.RULE_KEY;

/**
 * Handler for {@link RuleChooser class}.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class RuleChooserHandler {

    private static final Log log = LogFactory.getLog(RuleChooserHandler.class);

    protected RuleChooser ruleChooser;

    protected StorageChangeListener availableRuleListener;

    public RuleChooserHandler(RuleChooser ruleChooser) {
        this.ruleChooser = ruleChooser;
    }

    protected void initAvailableRules(StorageChangeEvent evt) {
        ruleChooser.availableRulesListModel.setElementList(RuleStorage.getRuleNames());
    }

    protected void initSelectedRules(PropertyChangeEvent evt) {
        List<Rule> rules;
        if (evt.getNewValue() == null) {
            rules = new ArrayList<>();
        } else {
            rules = (List<Rule>)evt.getNewValue();
        }
        ruleChooser.selectedRulesListModel.setElementList(rules);
    }

    protected void afterInit() {

        // update and init available
        availableRuleListener = this::initAvailableRules;
        RuleStorage.addStorageListener(availableRuleListener);
        initAvailableRules(null);

        // selected
        ruleChooser.addPropertyChangeListener(RuleChooser.PROPERTY_RULES_LIST, this::initSelectedRules);

        // fix colums size for buttons
        if (ruleChooser.isShowFactorColumn()) {
            SwingUtil.fixTableColumnWidth(ruleChooser.selectedRuleParameterTable, 2, 30);
        }
        
        setupSelectedListDnD();
    }

    /**
     * Setup drag and drop support on selected list.
     */
    protected void setupSelectedListDnD() {
        final JList<Rule> selectedRuleList = ruleChooser.getSelectedRulesList();
        
        // params
        selectedRuleList.setDragEnabled(true);
        selectedRuleList.setDropMode(DropMode.INSERT);

        // transfert handler
        selectedRuleList.setTransferHandler(new TransferHandler() {
            private static final long serialVersionUID = 7821083182552085625L;
            @Override
            public boolean canImport(TransferHandler.TransferSupport support) {
                boolean result = false;

                if (support.isDataFlavorSupported(DataFlavor.stringFlavor)) {
                    JList.DropLocation dl = (JList.DropLocation) support.getDropLocation();
                    if (dl.getIndex() != -1) {
                        return true;
                    }
                }

                return result;
            }
            @Override
            public boolean importData(TransferHandler.TransferSupport support) {
                boolean result = false;

                if (canImport(support)) {
                    Transferable transferable = support.getTransferable();
                    try {
                        String indexString = (String) transferable.getTransferData(DataFlavor.stringFlavor);
                        
                        int index = Integer.parseInt(indexString);
                        JList.DropLocation dl = (JList.DropLocation) support.getDropLocation();
                        int dropTargetIndex = dl.getIndex();

                        // must adjust removed index
                        if (index < dropTargetIndex) {
                            dropTargetIndex -= 1;
                        }
        
                        // perform real move
                        List<Rule> rules = ruleChooser.getRulesList();
                        Rule rule = rules.remove(index);
                        rules.add(dropTargetIndex, rule);
                        
                        selectedRuleList.setSelectedIndex(dropTargetIndex);
                        
                        result = true;
                    } catch (Exception e) {
                        if (log.isErrorEnabled()) {
                            log.error("Cant't import data", e);
                        }
                        result = false;
                    }
                }

                return result;
            }
        });

        // drag gesture
        final DragSource ds = new DragSource();
        ds.createDefaultDragGestureRecognizer(selectedRuleList, DnDConstants.ACTION_MOVE, dge -> {
            StringSelection transferable = new StringSelection(Integer.toString(selectedRuleList.getSelectedIndex()));
            // use null because gragsource listener is not used here (could be)
            ds.startDrag(dge, DragSource.DefaultCopyDrop, transferable, null);
        });
    }

    /**
     * Get new instance for selected rules names and add it to {@code rulesList} list.
     */
    public void addSelectedRules() {
        List<String> availableRuleValues = ruleChooser.getAvailableRuleList().getSelectedValuesList();
        for (String availableRuleName : availableRuleValues) {
            try {
                RuleStorage ruleStorage = RuleStorage.getRule(availableRuleName);
                Rule ruleTmp = ruleStorage.getNewInstance();

                // add it after autoconfiguration (if enabled)
                ruleTmp = (Rule)ScriptParameterDialog.displayConfigurationFrame(ruleChooser, ruleTmp);
                if (ruleTmp != null) {
                    ruleChooser.getRulesList().add(ruleTmp);
                }
            } catch (IsisFishException ex) {
                throw new IsisFishRuntimeException("Can't add rule", ex);
            }
        }
        ruleChooser.getSelectedRulesListModel().setElementList(ruleChooser.getRulesList());
    }
    
    /**
     * Remove selected rules for selected rules list.
     */
    public void removeSelectedRules() {
        List<Rule> selectedRuleValues = ruleChooser.getSelectedRulesList().getSelectedValuesList();
        for (Rule selectedRuleValue : selectedRuleValues) {

            // condition pour savoir si on est dans l'instance principal
            // de définition d'une simulation (hack)
            if (ruleChooser.isShowFactorColumn()) {
                int ruleIndex = ruleChooser.getRulesList().indexOf(selectedRuleValue);
                preRemoveRule(ruleIndex);
                ruleChooser.getContextValue(SimulationUI.class, "SimulationUI").refreshFactorTree();
            }

            // real rule remove
            ruleChooser.getRulesList().remove(selectedRuleValue);
        }
        ruleChooser.getSelectedRulesListModel().setElementList(ruleChooser.getRulesList());
        ruleChooser.getSelectedRulesList().clearSelection();
    }

    public void duplicateSelectedRules() {
        List<Rule> selectedRuleValues = ruleChooser.getSelectedRulesList().getSelectedValuesList();
        for (Rule selectedRuleValue : selectedRuleValues) {
            Rule r = (Rule)JavaSourceStorage.clone(selectedRuleValue);
            ruleChooser.getRulesList().add(r);
        }
        ruleChooser.getSelectedRulesListModel().setElementList(ruleChooser.getRulesList());
        ruleChooser.getSelectedRulesList().clearSelection();
    }
    
    /**
     * Display parameters table form single selected list.
     */
    protected void displayRuleParameters() {
        Rule selectedRule = ruleChooser.getSelectedRulesList().getSelectedValue();
        if (selectedRule != null) {
            ruleChooser.getSelectedRuleParameterTableModel().setScript(selectedRule);

            ScriptParametersParamNameRenderer nameRenderer = new ScriptParametersParamNameRenderer(selectedRule);
            ruleChooser.getSelectedRuleParameterTable().getColumnModel().getColumn(0).setCellRenderer(nameRenderer);
            ScriptParametersParamValueRenderer valueRenderer = new ScriptParametersParamValueRenderer();
            ruleChooser.getSelectedRuleParameterTable().getColumnModel().getColumn(1).setCellRenderer(valueRenderer);
            
            ScriptParametersParamValueEditor valueEditor = new ScriptParametersParamValueEditor(selectedRule);
            valueEditor.setRegionStorage(ruleChooser.getContextValue(RegionStorage.class));
            ruleChooser.getSelectedRuleParameterTable().getColumnModel().getColumn(1).setCellEditor(valueEditor);
            
            if (ruleChooser.isShowFactorColumn()) {
                RuleParametersFactorTableCellRenderer sensitivityRenderer = new RuleParametersFactorTableCellRenderer(ruleChooser, selectedRule);
                ruleChooser.getSelectedRuleParameterTable().getColumnModel().getColumn(2).setCellRenderer(sensitivityRenderer);
                RuleParametersFactorTableCellEditor sensitivityEditor = new RuleParametersFactorTableCellEditor(ruleChooser, selectedRule);
                ruleChooser.getSelectedRuleParameterTable().getColumnModel().getColumn(2).setCellEditor(sensitivityEditor);
            }
        } else {
            ruleChooser.getSelectedRuleParameterTableModel().setScript(null);
        }
    }
    
    /**
     * Called by RuleChooser component before rule deletion.
     * Used to remove factor associated to rule to delete.
     * 
     * Factor path reference rule with factor path containing rule index
     * in rule list :
     * for example :
     * <pre>
     * parameters.rule.2.parameter.tacPoids
     * </pre>
     * 
     * Must also rename all next indices.
     * 
     * @param ruleIndex rule index to to delete
     */
    public void preRemoveRule(int ruleIndex) {
        preRemoveRule(ruleChooser.getContextValue(FactorGroup.class), ruleIndex);
    }

    /**
     * Recursive rename and delete rule factor path.
     * 
     * @param factorGroup factorGroup
     * @param ruleIndex rule index to to delete
     */
    protected void preRemoveRule(FactorGroup factorGroup, int ruleIndex) {
        Collection<Factor> factorCopy = new ArrayList<>(factorGroup.getFactors());
        for (Factor factor : factorCopy) {
            if (factor instanceof FactorGroup) {
                preRemoveRule((FactorGroup)factor, ruleIndex);
            }
            else {
                Pattern factorPathPattern = Pattern.compile(
                        "^(" +PARAMETERS_KEY + "\\" + DOT + RULE_KEY + "\\" + DOT + ")(\\d+)(.*)$");
                Matcher factorPathMatcher = factorPathPattern.matcher(factor.getPath());
                if (factorPathMatcher.find()) {
                    Integer index = Integer.parseInt(factorPathMatcher.group(2));
                    if (index == ruleIndex) {
                        // meme index, suppression
                        if (log.isDebugEnabled()) {
                            log.debug("Removing factor for index " + ruleIndex + " : " + factor.getPath());
                        }
                        factorGroup.remove(factor);
                    }
                    else if (index > ruleIndex) {
                        // index supérieur, renommage
                        // avec un index de moins
                        String factorPath = factorPathMatcher.group(1) +
                                (index - 1) + factorPathMatcher.group(3);
                        if (log.isDebugEnabled()) {
                            log.debug("Renammed factor for index " + ruleIndex + " : " + factor.getPath());
                        }
                        factor.setPath(factorPath);
                    }
                }
            }
        }
    }
}
