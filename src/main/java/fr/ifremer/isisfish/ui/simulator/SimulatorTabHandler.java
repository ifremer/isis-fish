package fr.ifremer.isisfish.ui.simulator;

/*
 * #%L
 * ISIS-Fish
 * %%
 * Copyright (C) 1999 - 2020 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.isisfish.simulator.SimulationParameter;
import fr.ifremer.isisfish.simulator.sensitivity.FactorGroup;
import fr.ifremer.isisfish.ui.CommonHandler;
import jaxx.runtime.JAXXObject;
import org.nuiton.util.Resource;

import javax.swing.JTabbedPane;
import java.awt.Component;

/**
 * Handler common à tous les handler de chaque onglet de l'interface de simulation.
 */
public abstract class SimulatorTabHandler extends CommonHandler {

    protected JAXXObject simulationUI;

    protected SimulatorTabHandler(JAXXObject simulationUI) {
        this.simulationUI = simulationUI;
    }

    /**
     * Return simulation parameters from context.
     * 
     * @return SimulationParameter
     */
    protected SimulationParameter getParameters() {
        return simulationUI.getContextValue(SimulationParameter.class);
    }
    
    /**
     * Return FactorGroup from context.
     * 
     * @return FactorGroup
     */
    protected FactorGroup getFactorGroup() {
        return simulationUI.getContextValue(FactorGroup.class);
    }

    protected void setTabSelectedIcon(Component component, boolean selected) {
        JTabbedPane parentContainer = simulationUI.getParentContainer(JTabbedPane.class);
        int index = parentContainer.indexOfComponent(component);
        if (selected) {
            parentContainer.setIconAt(index, Resource.getIcon("/icons/fatcow/tick_button.png"));
            parentContainer.setTitleAt(index, parentContainer.getTitleAt(index) + " ");
        } else {
            parentContainer.setIconAt(index, null);
            parentContainer.setTitleAt(index, parentContainer.getTitleAt(index).trim());
        }
    }
}
