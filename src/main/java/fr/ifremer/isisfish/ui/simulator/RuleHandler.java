/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2020 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.ui.simulator;

import fr.ifremer.isisfish.ui.sensitivity.SensitivityUI;

import javax.swing.JComponent;

/**
 * Rule UI handler.
 */
public class RuleHandler extends SimulatorTabHandler {

    protected RuleUI ruleUi;

    public RuleHandler(RuleUI ruleUi) {
        super(ruleUi);
        this.ruleUi = ruleUi;
    }

    protected void afterInit() {
        // la gestion des regles est maintenant independante de SimulAction
        // il faut faire la lié aux parametres de simulations
        getParameters().setRules(ruleUi.ruleChooser.getRulesList());
    }

    public void refresh() {
        ruleUi.ruleChooser.setRulesList(getParameters().getRules());
    }

    public void addFactorWithComponent(JComponent source) {
        // FIXME ugly workaround
        SensitivityUI parent = ruleUi.getParentContainer(SensitivityUI.class);
        parent.getParamsUI().getHandler().addFactorWithComponent(source);
    }
}
