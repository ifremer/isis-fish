/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2019 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.ui.simulator;

import static org.nuiton.i18n.I18n.t;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.swing.event.ListSelectionEvent;

import fr.ifremer.isisfish.datastore.StorageChangeEvent;
import fr.ifremer.isisfish.datastore.StorageChangeListener;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.datastore.ResultInfoStorage;
import fr.ifremer.isisfish.ui.WelcomePanelUI;

/**
 * Result choice UI handler.
 */
public class ResultChoiceHandler extends SimulatorTabHandler {

    private static final Log log = LogFactory.getLog(ResultChoiceHandler.class);

    protected ResultChoiceUI tabUI;

    protected StorageChangeListener resultInfoStorageListener;

    public ResultChoiceHandler(ResultChoiceUI tabUI) {
        super(tabUI);
        this.tabUI = tabUI;
    }

    protected void initListResultNamesModel(StorageChangeEvent evt) {
        List<String> allResults = ResultInfoStorage.getResultInfoNames();
        tabUI.listResultNamesModel.setElementList(allResults);

        // restore selection
        Collection<String> userResults = getParameters().getResultEnabled();
        tabUI.listResultNames.clearSelection();
        for (String userResult : userResults) {
            int index = allResults.indexOf(userResult);
            tabUI.listResultNames.addSelectionInterval(index, index);
        }
    }

    protected void afterInit() {
        resultInfoStorageListener = this::initListResultNamesModel;
        ResultInfoStorage.addStorageListener(resultInfoStorageListener);
        initListResultNamesModel(null);
    }
    
    public void refresh() {
        // not existing binding (reload old simulation)
        if (tabUI.isSensitivity()) {
            tabUI.sensitivityOnlyKeepFirstResultCheckBox.setSelected(getParameters().isSensitivityAnalysisOnlyKeepFirst());
        } else {
            tabUI.resultDeleteAfterExportCheckBox.setSelected(getParameters().isResultDeleteAfterExport());
        }

        initListResultNamesModel(null);
    }

    protected void saveParametersResultNames(ListSelectionEvent event) {
        List<String> resultNamesString = new ArrayList<>(tabUI.listResultNames.getSelectedValuesList());
        getParameters().setResultEnabled(resultNamesString);
        
        if (log.isDebugEnabled()) {
            log.debug("Set simulation result names : " + resultNamesString);
        }
    }
  
    protected void saveConfigResultNames() {
        List<String> resultNamesString = new ArrayList<>(tabUI.listResultNames.getSelectedValuesList());
        IsisFish.config.setDefaultResultNames(resultNamesString);
        setStatusMessage(t("isisfish.resultChoice.saved"));

        if (log.isDebugEnabled()) {
            log.debug("Set configuration result names : " + resultNamesString);
        }
    }

    protected void setStatusMessage(String txt) {
        // TODO remove getParentContainer use
        WelcomePanelUI root = tabUI.getParentContainer(WelcomePanelUI.class);
        root.setStatusMessage(txt);
    }
}
