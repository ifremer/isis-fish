/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 - 2020 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.ui.simulator;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.datastore.RegionStorage;
import fr.ifremer.isisfish.datastore.ResultInfoStorage;
import fr.ifremer.isisfish.simulator.SimulationParameter;
import fr.ifremer.isisfish.simulator.SimulationParameterImpl;
import fr.ifremer.isisfish.simulator.sensitivity.FactorGroup;
import jaxx.runtime.JAXXContext;
import jaxx.runtime.context.DefaultJAXXContext;
import org.nuiton.topia.TopiaContext;

/**
 * Ce contexte regroupe les élements qui servent à une hierachie d'interfaces Simulator.
 * 
 * Ce context contient:
 * <ul>
 * <li>SimulationParameter.class : les parametres des la simulation</li>
 * <li>FactorGroup.class : le facteur group racine</li>
 * <li>RegionStorage.class : la region sélectionner pour ajouter des facteurs</li>
 * <li>SimulatorContext.class : l'instance de ce context</li>
 * </ul>
 */
public class SimulatorContext extends DefaultJAXXContext {

    private static final Log log = LogFactory.getLog(SimulatorContext.class);

    protected static TopiaContext db;

    public SimulatorContext(JAXXContext parent) {
        setParentContext(parent);
        
        initDefaultParameters();
        initFactorGroup();
        setSimulatorContext(this);
    }

    public static TopiaContext getDb() {
        return db;
    }

    public static void setDb(TopiaContext db) {
        SimulatorContext.db = db;
    }

    public void setSimulationParameter(SimulationParameter param) {
        setContextValue(param);
    }
    
    public void setFactorGroup(FactorGroup factorGroup) {
        setContextValue(factorGroup);
    }

    public void setRegionStorage(RegionStorage regionStorage) {
        setContextValue(regionStorage);
    }
    
    public void setSimulatorContext(SimulatorContext simulatorContext) {
        // add this one named to prevent infinite loop
        setContextValue(simulatorContext, "SimulatorContext");
    }

    /**
     * Build simulation parameters 
     */
    public void initDefaultParameters() {
        
        SimulationParameter param = new SimulationParameterImpl();
        
        // init with default values from configuration
        param.setSimulatorName(IsisFish.config.getSimulatorClassfile());

        // export names
        param.setExportNames(IsisFish.config.getDefaultExportNamesAsList());

        // result names
        List<String> defaultResultNames = IsisFish.config.getDefaultResultNamesAsList();
        if (defaultResultNames != null) {
            param.setResultEnabled(defaultResultNames);
        } else {
            List<String> allResultNames = ResultInfoStorage.getResultInfoNames();
            param.setResultEnabled(allResultNames);
        }

        // tag values
        Map<String, String> tv = new LinkedHashMap<>();
        tv.putAll(IsisFish.config.getDefaultTagValueAsMap());
        tv.putAll(IsisFish.config.getDefaultSimulationConfig());
        param.setTagValue(tv);
        
        // set it in context to be used by all UIs
        setSimulationParameter(param);
    }
    
    /**
     * List de facteur sous forme d'arbre (factor group).
     * {@code null} name for compatibility with 3.3.0.0.
     */
    protected void initFactorGroup() {
        
        FactorGroup factorGroup = new FactorGroup(null);
        setContextValue(factorGroup);
    }
}
