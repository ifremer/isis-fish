/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2019 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.ui.simulator;

import static org.nuiton.i18n.I18n.t;

import java.util.Map;

import javax.swing.table.DefaultTableModel;

import fr.ifremer.isisfish.datastore.StorageChangeEvent;
import fr.ifremer.isisfish.datastore.StorageChangeListener;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.datastore.SimulatorStorage;
import fr.ifremer.isisfish.simulator.SimulationParameter;

/**
 * AdvancedParams UI handler.
 */
public class AdvancedParamsHandler extends SimulatorTabHandler {

    /** Class logger. */
    private static final Log log = LogFactory.getLog(AdvancedParamsHandler.class);

    protected AdvancedParamsUI tabUI;

    protected StorageChangeListener simulatorStorageListener;

    protected AdvancedParamsHandler(AdvancedParamsUI tabUI) {
        super(tabUI);
        this.tabUI = tabUI;
    }

    protected void initSimulAdvParamsSimulatorModel(StorageChangeEvent evt) {
        tabUI.fieldSimulAdvParamsSimulatorModel.setElementList(SimulatorStorage.getSimulatorNames());

        // retore selection
        SimulationParameter params = getParameters();
        tabUI.fieldSimulAdvParamsSimulatorSelect.setSelectedItem(params.getSimulatorName());
    }

    protected void afterInit() {
        simulatorStorageListener = this::initSimulAdvParamsSimulatorModel;
        SimulatorStorage.addStorageListener(simulatorStorageListener);

        /*
         * Listener to enable/disable remove button.
         */
        tabUI.tableTagValues.getSelectionModel().addListSelectionListener(e -> tabUI.setRemove(tabUI.tableTagValues.getSelectedRow() != -1));
    
        refresh();
    }

    public void refresh() {
        initSimulAdvParamsSimulatorModel(null);

        SimulationParameter params = getParameters();

        tabUI.fieldSimulationStatistique.setSelected(params.getUseStatistic());
        tabUI.fieldSimulationCache.setSelected(params.getUseCache());

        tabUI.fieldSimulAdvParamsSimulLoggerError.setSelected(params.isSimulErrorLevel());
        tabUI.fieldSimulAdvParamsSimulLoggerWarn.setSelected(params.isSimulWarnLevel());
        tabUI.fieldSimulAdvParamsSimulLoggerInfo.setSelected(params.isSimulInfoLevel());
        tabUI.fieldSimulAdvParamsSimulLoggerDebug.setSelected(params.isSimulDebugLevel());

        tabUI.fieldSimulAdvParamsScriptLoggerError.setSelected(params.isScriptErrorLevel());
        tabUI.fieldSimulAdvParamsScriptLoggerWarn.setSelected(params.isScriptWarnLevel());
        tabUI.fieldSimulAdvParamsScriptLoggerInfo.setSelected(params.isScriptInfoLevel());
        tabUI.fieldSimulAdvParamsScriptLoggerDebug.setSelected(params.isScriptDebugLevel());

        tabUI.fieldSimulAdvParamsLibLoggerError.setSelected(params.isLibErrorLevel());
        tabUI.fieldSimulAdvParamsLibLoggerWarn.setSelected(params.isLibWarnLevel());
        tabUI.fieldSimulAdvParamsLibLoggerInfo.setSelected(params.isLibInfoLevel());
        tabUI.fieldSimulAdvParamsLibLoggerDebug.setSelected(params.isLibDebugLevel());

        setTableTagValues();
    }

    protected void addTagValue() {
        addTagValue(tabUI.fieldTag.getText(), tabUI.fieldValue.getText());
        setTableTagValues();
        valueChanged(true);
    }

    protected void removeTagValue() {
        removeTagValue(tabUI.tableTagValues.getValueAt(tabUI.tableTagValues.getSelectedRow(),0).toString());
        setTableTagValues();
        valueChanged(true);
    }

    protected void saveTagValues() {
        if (tabUI.fieldSimulAdvParamsSimulatorSelect.getSelectedIndex() != -1) {
            saveTagValue((String)tabUI.fieldSimulAdvParamsSimulatorSelect.getSelectedItem());
        }
        valueChanged(false);
    }

    protected void setSimulatorName() {
        if (tabUI.fieldSimulAdvParamsSimulatorSelect.getSelectedIndex() != -1) {
            getParameters().setSimulatorName((String)tabUI.fieldSimulAdvParamsSimulatorSelect.getSelectedItem());
            valueChanged(true);
        }
    }

    protected void setTableTagValues() {
        Map<String, String> tagValues = getParameters().getTagValue();
        
        // TODO make a table model for it
        String[] columnName = { t("isisfish.common.tag"), t("isisfish.common.value")};
        if (tagValues != null) {
            DefaultTableModel model = new DefaultTableModel(columnName, tagValues.size()) {
                @Override
                public boolean isCellEditable(int row, int column) {
                    return false;
                }
            };
            //Set<Entry<String, String>> set = tagValues.entrySet();
            int row = 0;
            for (String name : tagValues.keySet()) {
                model.setValueAt(name, row, 0);
                model.setValueAt(tagValues.get(name), row, 1);
                row++;
            }
            tabUI.tableTagValues.setModel(model);
        }
    }

    protected void valueChanged(boolean b) {
        tabUI.setChanged(b);
    }

    public void addTagValue(String tag, String value) {
        getParameters().getTagValue().put(tag, value);
    }

    public void removeTagValue(String tag) {
        if (log.isDebugEnabled()) {
            log.debug("removeTagValue: " + tag);
        }
        getParameters().getTagValue().remove(tag);
    }

    public void saveTagValue(String simulatorName) {
        Map<String, String> tagValues = getParameters().getTagValue();
        if (log.isDebugEnabled()) {
            log.debug("call saveTagValue: " + tagValues);
        }
        IsisFish.config.setDefaultTagValues(tagValues);

        IsisFish.config.setSimulatorClassfile(simulatorName);
    }
}
