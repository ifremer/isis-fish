/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2020 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.ui.simulator;

/**
 * Prescript UI handler.
 */
public class PreScriptsHandler extends SimulatorTabHandler {

    protected PreScriptsUI tabUI;

    protected PreScriptsHandler(PreScriptsUI tabUI) {
        super(tabUI);
        this.tabUI = tabUI;
    }

    protected void afterInit() {
        
    }

    public void refresh() {
        tabUI.fieldUseSimulPreScripts.setSelected(getParameters().getUsePreScript());
    }

    protected void enablePreScript() {
        boolean selected = tabUI.fieldUseSimulPreScripts.isSelected();
        getParameters().setUsePreScript(selected);
        setTabSelectedIcon(tabUI, selected);
    }

    protected void save() {
        getParameters().setPreScript(tabUI.fieldSimulPreScript.getText());
    }

}
