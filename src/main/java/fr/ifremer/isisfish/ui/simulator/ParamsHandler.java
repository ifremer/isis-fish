/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2020 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.ui.simulator;

import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.datastore.RegionStorage;
import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.datastore.StorageChangeEvent;
import fr.ifremer.isisfish.datastore.StorageChangeListener;
import fr.ifremer.isisfish.datastore.StorageException;
import fr.ifremer.isisfish.entities.FisheryRegion;
import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.entities.Species;
import fr.ifremer.isisfish.entities.Strategy;
import fr.ifremer.isisfish.mexico.MexicoHelper;
import fr.ifremer.isisfish.simulator.SimulationParameter;
import fr.ifremer.isisfish.simulator.launcher.SimulationJob;
import fr.ifremer.isisfish.simulator.launcher.SimulationService;
import fr.ifremer.isisfish.simulator.launcher.SimulationServiceListener;
import fr.ifremer.isisfish.simulator.launcher.SimulatorLauncher;
import fr.ifremer.isisfish.simulator.sensitivity.DesignPlan;
import fr.ifremer.isisfish.simulator.sensitivity.Factor;
import fr.ifremer.isisfish.simulator.sensitivity.FactorGroup;
import fr.ifremer.isisfish.simulator.sensitivity.SensitivityAnalysis;
import fr.ifremer.isisfish.ui.SimulationUI;
import fr.ifremer.isisfish.ui.WelcomePanelUI;
import fr.ifremer.isisfish.ui.WelcomeTabUI;
import fr.ifremer.isisfish.ui.models.common.GenericComboModel;
import fr.ifremer.isisfish.ui.sensitivity.SensitivityUI;
import fr.ifremer.isisfish.ui.sensitivity.wizard.FactorWizardHandler;
import fr.ifremer.isisfish.ui.sensitivity.wizard.FactorWizardUI;
import fr.ifremer.isisfish.util.ErrorHelper;
import jaxx.runtime.SwingUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.math.matrix.gui.MatrixPanelEditor;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static fr.ifremer.isisfish.simulator.SimulationParameterPropertiesHelper.DOT;
import static fr.ifremer.isisfish.simulator.SimulationParameterPropertiesHelper.NUMBER_KEY;
import static fr.ifremer.isisfish.simulator.SimulationParameterPropertiesHelper.PARAMETERS_KEY;
import static fr.ifremer.isisfish.simulator.SimulationParameterPropertiesHelper.POPULATION_KEY;
import static fr.ifremer.isisfish.simulator.SimulationParameterPropertiesHelper.RULES_KEY;
import static org.nuiton.i18n.I18n.t;

/**
 * Params UI handler.
 */
public class ParamsHandler extends SimulatorTabHandler {

    /** Class logger. */
    private static final Log log = LogFactory.getLog(ParamsHandler.class);

    private static final SimpleDateFormat DATEFORMAT = new SimpleDateFormat("yyyy-MM-dd-HH-mm");

    protected ParamsUI tabUI;

    // instances variables déclaration
    protected SimulationServiceListener simulationListener;
    protected StorageChangeListener regionStorageListener;
    
    protected List<String> oldSimulNames = null;
    
    protected String simulationName;
    
    protected ParamsHandler(ParamsUI tabUI) {
        super(tabUI);
        this.tabUI = tabUI;
    }

    protected void initRegionComboModel(StorageChangeEvent evt) {
        SwingUtilities.invokeLater(() ->  {
            tabUI.simulParamsRegionModel.setElementList(RegionStorage.getRegionNames());
        });
    }

    protected void afterInit() {
        regionStorageListener = this::initRegionComboModel;
        RegionStorage.addStorageListener(regionStorageListener);
        this.initRegionComboModel(null);

        simulationListener = new SimulationServiceListener() {
            @Override
            public void simulationStart(SimulationService simService, SimulationJob job) {
            
            }

            @Override
            public void simulationStop(SimulationService simService, final SimulationJob job) {
                final GenericComboModel<String> model = (GenericComboModel<String>)tabUI.fieldSimulParamsSelect.getModel();
                SwingUtilities.invokeLater(() -> model.addElement(job.getId()));
            }

            @Override
            public void clearJobDone(SimulationService simService) {
            
            }
        };
        SimulationService.getService().addSimulationServiceListener(simulationListener);
    }
    
    public void setSimulationName(String simulationName) {
        this.simulationName = simulationName;
    }

    public void refresh() {
        tabUI.fieldSimulParamsDesc.setText(getParameters().getDescription());

        // reload region if needed
        tabUI.fieldSimulParamsRegion.setSelectedItem(getParameters().getRegionName());

        // invoke later to be invoked after the invokeLater in regionChange
        SwingUtilities.invokeLater(() -> {
            setListSimulParamsStrategiesItems();
            setListSimulParamsPopulationsItems();
        });
    }

    /**
     * Called on user region selection change.
     */
    protected void regionChange(ItemEvent event) {
        // le premier item est " " pour ne pas avoir de selection par defaut
        if (event.getStateChange() == ItemEvent.SELECTED) {
            final String selected = (String) tabUI.fieldSimulParamsRegion.getSelectedItem();
            if (StringUtils.isNotBlank(selected)) {
                tabUI.getParentContainer(WelcomePanelUI.class).setStatusMessage(t("isisfish.message.loading.region"));
                SwingUtilities.invokeLater(() -> {
                    regionChange(selected);
                    tabUI.getParentContainer(SimulationUI.class).refresh();
                    setSensitivityTabRegion();
                    tabUI.getParentContainer(WelcomePanelUI.class).setStatusMessage(t("isisfish.message.region.loaded"));
                });
            }
        }
    }

    /**
     * Change region in simulation launcher
     * 
     * @param regionName region name
     */
    public void regionChange(String regionName) {
        RegionStorage regionStorage = RegionStorage.getRegion(regionName);
        // TODO this could be changed by setting storage into simulation context
        tabUI.getParentContainer(SimulationUI.class).setContextValue(regionStorage);
        tabUI.getParentContainer(SimulationUI.class).setRegionStorage(regionStorage);
        SimulatorContext.setDb(regionStorage.getStorage().beginTransaction());
        getParameters().setRegionName(regionName);
        getParameters().reloadRegionChangeParameter();
    }

    /**
     * Load old simulation.
     * 
     * Reset some field to empty default values:
     * <ul>
     *  <li>params</li>
     *  <li>simulation plans</li>
     *  <li>factors list</li>
     * </ul>
     * 
     * Open old simulation:
     * <ul>
     *  <li>params copy</li>
     *  <li>factors</li>
     * </ul>
     * 
     * @param simulName name of simulation to load
     */
    public boolean loadOldSimulation(String simulName) {
        if (log.isDebugEnabled()) {
            log.debug("call loadOldSimulation: " + simulName);
        }
        try {
            SimulatorContext context = tabUI.getContextValue(SimulatorContext.class, "SimulatorContext");
            
            // read storage to get name
            SimulationStorage simulStorage = SimulationStorage.getSimulation(simulName);
            tabUI.getFieldSimulParamsName().setText(simulStorage.getName());
            
            SimulationParameter param = simulStorage.getParameter().copy();
            param.fixReloadContext(tabUI.isSensitivity());
            // all time reset number after load
            param.setSimulationPlanNumber(-1);
            context.setSimulationParameter(param);

            RegionStorage regionStorage = param.getRegion();
            if (regionStorage == null) {
                JOptionPane.showMessageDialog(tabUI, t("isisfish.error.simulation.loadoldsimulation.regionnotfound", param.getRegionName()),
                    t("isisfish.common.error"), JOptionPane.ERROR_MESSAGE);
                return false;
            }
            context.setRegionStorage(regionStorage);

            // Chargement des facteurs
            // clear list even if mexico file doesn't exists
            getFactorGroup().clearFactors();
            File f = SimulationStorage.getMexicoDesignPlan(SimulationStorage.getSimulationDirectory(simulName));
            if (f != null && f.canRead()) {
                if (log.isInfoEnabled()) {
                    log.info("Import design plan from : " + f.getAbsolutePath());
                }
                TopiaContext topiaContext = regionStorage.getStorage();
                DesignPlan designPlan = MexicoHelper.getDesignPlanFromXML(f, topiaContext);
                for (Factor factor : designPlan.getFactors()) {
                    if (log.isDebugEnabled()) {
                        log.debug("Find factor : " + factor.getName());
                    }
                    //factors.put(factor.getPath() + factor.getName(), factor);
                    FactorGroup factorGroup = designPlan.getFactorGroup();
                    context.setFactorGroup(factorGroup);
                }
            } else if (log.isInfoEnabled()) {
                log.info("No xml design plan file found");
            }
        } catch (Exception eee) {
            throw new IsisFishRuntimeException(t("isisfish.error.simulation.loadoldsimulation"), eee);
        }

        return true;
    }
    
    public SimulatorLauncher[] getSimulationLauncher() {
        return SimulationService.getService().getSimulationLaunchers().toArray(new SimulatorLauncher[0]);
    }

    protected void setSensitivityTabRegion() {
        if (tabUI.isSensitivity()) {
            try {
                // FIXME this transaction in never closed
                // and can't be closed because used in
                TopiaContext tx = tabUI.getContextValue(RegionStorage.class).getStorage().beginTransaction();
                FisheryRegion fisheryRegion = RegionStorage.getFisheryRegion(tx);
                tabUI.getParentContainer(SensitivityUI.class).getSensitivityInputUI().setFisheryRegion(fisheryRegion);
                tabUI.getParentContainer(SensitivityUI.class).getSensitivityInputUI().getHandler().setTreeModel();
            } catch (StorageException | TopiaException ex) {
                if (log.isErrorEnabled()) {
                    log.error("Can't reload factors", ex);
                }
            }
        }
    }
    protected void initSensitivityParams() {
        if (tabUI.isSensitivity()) {
            tabUI.getParentContainer(SensitivityUI.class).getSensitivityChooserUI().getHandler().refreshSelectedSensitivityAnalysis();
            tabUI.getParentContainer(SensitivityUI.class).getSensitivityInputUI().getHandler().setFactorModel();
            tabUI.getParentContainer(SensitivityUI.class).getSensitivityChooserUI().getHandler().setSensitivityExportListModel();
        }
    }

    protected void loadOldSimulation() {
        final String selected = tabUI.fieldSimulParamsSelect.getSelectedItem().toString();
        // le premier item est " " pour ne pas avoir de selection par defaut
        if (selected != null && !selected.equals(" ")) {
            tabUI.getParentContainer(WelcomePanelUI.class).setStatusMessage(t("isisfish.message.loading.old.simulation"));
            tabUI.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

            SwingUtilities.invokeLater(() -> {

                boolean loaded = loadOldSimulation(selected);
                if (loaded) {
                    tabUI.fieldSimulParamsRegion.setSelectedItem(getParameters().getRegionName());

                    tabUI.getParentContainer(SimulationUI.class).refresh();

                    // mise a jour des années
                    tabUI.fieldSimulParamsNbMois.setText(String.valueOf(getNumberOfMonths()));
                    initSensitivityParams();

                    tabUI.getParentContainer(WelcomePanelUI.class).setStatusMessage(t("isisfish.message.old.simulation.loaded"));
                }

                tabUI.setCursor(Cursor.getDefaultCursor());
            });
        }
    }

    /**
     * Lance la simulation.
     */
    protected void launchSimulation() {

        if (!tabUI.isSensitivity()) {
            launchSimulation(tabUI.fieldSimulParamsName.getText(), (SimulatorLauncher)tabUI.comboSelLauncher.getSelectedItem());
        } else {
            launchSimulationWithSensibility(tabUI.fieldSimulParamsName.getText(), (SimulatorLauncher)tabUI.comboSelLauncher.getSelectedItem());
        }
        
        // dans le cas d'une fenetre independante, il n'y a pas de ParentContainer
        WelcomeTabUI parent = tabUI.getParentContainer(WelcomeTabUI.class);
        if (parent != null) {
            parent.setQueueTabSelection();
        }
    }
    
    /**
     * Launch simulation with factors variations parameters.
     * 
     * @param simulationId id of the simulation to simulate
     * @param launcher launcher to use
     * 
     * @see SimulatorLauncher
     * @see SensitivityAnalysis
     * @see DesignPlan
     */
    public void launchSimulationWithSensibility(String simulationId,
            SimulatorLauncher launcher) {

        String fullSimulationId = "as_" + simulationId + "_"
                + DATEFORMAT.format(new java.util.Date());

        SensitivityAnalysis sensitivityAnalysis = getParameters().getSensitivityAnalysis();

        // log
        if (log.isDebugEnabled()) {
            log.debug("Launch factor simulation with custom launcher "
                    + launcher.toString());
            log.debug("Using sensitivityCalculator : "
                    + sensitivityAnalysis.getDescription());
        }

        try {
            if (checkAndPrepare(fullSimulationId)) {
                DesignPlan designPlan = new DesignPlan();
                designPlan.setFactorGroup(getFactorGroup());
                SimulationService.getService().submit(fullSimulationId, getParameters(),
                        launcher, 0, sensitivityAnalysis, designPlan);
            }
        } catch (Exception eee) {
            if (log.isErrorEnabled()) {
                log.error("Can't start simulation", eee);
            }
            ErrorHelper.showErrorDialog(t("isisfish.error.simulation.launchsimulation"), eee);
        }
    }
    
    protected boolean checkAndPrepare(String fullSimulationId) {
        boolean result = !("".equals(fullSimulationId)
                || SimulationStorage.localyExists(fullSimulationId)
                || SimulationService.getService().exists(fullSimulationId));

        if (result) {
            // force all necessary config simulation in tag value
            Map<String, String> m = new HashMap<>(
                    IsisFish.config.getDefaultSimulationConfig());
            Map<String, String> tv = getParameters().getTagValue();
            m.putAll(tv);
            getParameters().setTagValue(m);
        } else {
            ErrorHelper.showErrorDialog(t("isisfish.simulator.simulaction.badid",
                    fullSimulationId), null);
        }

        return result;
    }

    /**
     * Launch automatically the simulation, when is possible (no other simulation)
     * or wait for the last automatically simulation ended.
     *
     * @param simulationId id of the simulation to simulate
     * @param launcher launcher to use
     * 
     * @see SimulatorLauncher
     */
    public void launchSimulation(String simulationId, SimulatorLauncher launcher) {

        String fullSimulationId = "sim_" + simulationId + "_"
                + DATEFORMAT.format(new java.util.Date());

        // log
        if (log.isDebugEnabled()) {
            log.debug("Launch simulation with custom launcher "
                    + launcher.toString());
        }

        try {
            if (checkAndPrepare(fullSimulationId)) {
                SimulationService.getService().submit(fullSimulationId, getParameters(),
                        launcher, 0);
            }
        } catch (Exception eee) {
            if (log.isErrorEnabled()) {
                log.error("Can't start simulation", eee);
            }
            ErrorHelper.showErrorDialog(t("isisfish.error.simulation.launchsimulation"), eee);
        }
    }
    
    /**
     * Population selection changed.
     *
     * Fill matrix panel with population effective of selected population.
     */
    public void populationSelected() {
        CardLayout layout = (CardLayout)tabUI.getPopulationEffectivesPanel().getLayout();
        
        List<Population> selectedPopulationsValues = tabUI.getListSimulParamsPopulations().getSelectedValuesList();
        if (CollectionUtils.isNotEmpty(selectedPopulationsValues)) {
            tabUI.getPopulationEffectivesTabbedPane().removeAll();
            setPopulations(selectedPopulationsValues);

            for (Population selectedPopulation : selectedPopulationsValues) {
                final MatrixPanelEditor matrixPanel = new MatrixPanelEditor();
                final String populationName = selectedPopulation.getName();

                MatrixND populationEffectives = getParameters().getNumberOf(selectedPopulation);
                matrixPanel.setMatrix(populationEffectives);

                JPanel matrixPanelComponent = new JPanel(new BorderLayout());
                matrixPanelComponent.add(matrixPanel, BorderLayout.CENTER);

                // add addFactorButton with matrixPanel (just in sensitivity cas)
                if (tabUI.isSensitivity()) {
                    matrixPanel.putClientProperty("sensitivityPopulation", selectedPopulation);
                    // TODO add another thing that action
                    JButton addFactorButton = new JButton();
                    addFactorButton.setAction(new AbstractAction() {
                        public void actionPerformed(ActionEvent e) {
                            addFactorWithComponent(matrixPanel);
                        }
                        @Override
                        public Object getValue(String key) {
                            Object result = null;
                            if (key.equals(Action.SMALL_ICON)) {
                                result = SwingUtil.createImageIcon("fatcow/brick_add.png");
                            } else if (key.equals(Action.SHORT_DESCRIPTION)) {
                                result = t("isisfish.params.populationEffectives.factorTooltip", populationName);
                            }
                            return result;
                        }
                    });
                    matrixPanelComponent.add(addFactorButton, BorderLayout.EAST);
                }

                tabUI.getPopulationEffectivesTabbedPane().add(matrixPanelComponent,
                        t("isisfish.params.populationEffectives", populationName));
            }
            
            layout.show(tabUI.getPopulationEffectivesPanel(), "specific");
        }
        else {
            layout.show(tabUI.getPopulationEffectivesPanel(), "default");
        }
    }
    
    /**
     * Action appelée lors du clic sur les boutons a coté des composants factorisables.
     *
     * Contrairement à l'interface 'input', on edite ici des facteurs existants,
     * (créé à la première demande).
     *
     * @param source la source de l'event
     */
    public void addFactorWithComponent(JComponent source) {

        if (log.isDebugEnabled()) {
            log.debug("Event intercepted on " + source);
        }

        Factor selectedFactor = null;

        // new factor with rule domain
        if (source instanceof RuleChooser) {
            String factorPath = PARAMETERS_KEY + DOT + RULES_KEY;
            selectedFactor = getFactor(factorPath);
            if (selectedFactor == null) {
                selectedFactor = new Factor(t("isisfish.sensitivity.rulesfactorname"));
                selectedFactor.setPath(factorPath);
                //selectedFactor.setDomain(new RuleDiscreteDomain());
            }
        }

        // new factor with matrix continous domain
        else if (source instanceof MatrixPanelEditor) {
            Population population = (Population)source.getClientProperty("sensitivityPopulation");
            String factorPath = PARAMETERS_KEY + DOT + POPULATION_KEY + DOT + population.getName() + DOT + NUMBER_KEY;
            selectedFactor = getFactor(factorPath);
            if (selectedFactor == null) {
                selectedFactor = new Factor(t("isisfish.sensitivity.populationfactorname", population.getName()));
                selectedFactor.setPath(factorPath);
                //MatrixND populationEffectives = getSimulationParameter().getNumberOf(population);
                /*MatrixContinuousDomain factorDomain = new MatrixContinuousDomain();
                factorDomain.setReferenceValue(populationEffectives.copy());
                factorDomain.setCoefficient(0.0);
                ContinuousDomain domain = new ContinuousDomain(Distribution.QUNIFPC);
                domain.addDistributionParam(Distribution.QUNIFPC.getDistibutionParams()[0].getName(), populationEffectives.copy());
                domain.addDistributionParam(Distribution.QUNIFPC.getDistibutionParams()[1].getName(), 0.0d);
                selectedFactor.setDomain(domain);*/
            }
        }

        if (selectedFactor != null) {
            FactorWizardUI wizard = new FactorWizardUI(tabUI);
            FactorWizardHandler handler = wizard.getHandler();
            handler.initExistingFactor(wizard, selectedFactor);
            wizard.pack();
            wizard.setLocationRelativeTo(tabUI);
            wizard.setVisible(true);
        }
    }
    
    /**
     * Search factor in factor group tree by path.
     * 
     * @param factorPath factor path to search
     * @return found factor
     */
    public Factor getFactor(String factorPath) {
        return getFactor(getFactorGroup(), factorPath);
    }

    /**
     * Recursive search for factor in factor group by path.
     * 
     * @param factorGroup factor group to search to
     * @param factorPath factor path to search
     * @return found factor
     */
    protected Factor getFactor(FactorGroup factorGroup, String factorPath) {
        Factor result = null;
        for (Factor factor : factorGroup.getFactors()) {
            if (factor instanceof FactorGroup) {
                result = getFactor((FactorGroup)factor, factorPath);
            }
            if (factorPath.equals(factor.getPath())) {
                result = factor;
            }
        }
        return result;
    }

    protected void setListSimulParamsStrategiesItems() {
        List<Strategy> strategiesSelected = getParameters().getStrategies();
        DefaultListModel<Strategy> listSimulParamsStrategiesModel = new DefaultListModel<>();
        List<Strategy> strategies = getStrategies();
        for (Strategy s : strategies){
            listSimulParamsStrategiesModel.addElement(s);
        }
        tabUI.listSimulParamsStrategies.setModel(listSimulParamsStrategiesModel);
        if (listSimulParamsStrategiesModel.size() != 0) {
            for (Strategy s : strategiesSelected){
                int index = listSimulParamsStrategiesModel.indexOf(s);
                tabUI.listSimulParamsStrategies.addSelectionInterval(index, index);
            }
            //strategySelected();
        }
    }
    protected void setListSimulParamsPopulationsItems(){
        List<Population> populationsSelected = getParameters().getPopulations();
        DefaultListModel<Population> listSimulParamsPopulationsModel = new DefaultListModel<>();
        List<Population> populations = getPopulations();
        for (Population p : populations){
            listSimulParamsPopulationsModel.addElement(p);
        }
        tabUI.listSimulParamsPopulations.setModel(listSimulParamsPopulationsModel);
        if (listSimulParamsPopulationsModel.size() != 0) {
            for (Population p : populationsSelected) {
                int index = listSimulParamsPopulationsModel.indexOf(p);
                tabUI.listSimulParamsPopulations.addSelectionInterval(index, index);
            }
            //getSimulAction().populationSelected(this);
        }
    }

    protected void strategySelected() {
        setStrategies(tabUI.listSimulParamsStrategies.getSelectedValuesList());
    }

    protected ComboBoxModel<String> getSimulParamsSelectModel() {
        return getSimulParamsSelectModel(false);
    }

    protected ComboBoxModel<String> getSimulParamsSelectModel(boolean force) {
        List<String> items = getFilteredOldSimulatorNames(force);
        GenericComboModel<String> result = new GenericComboModel<>(items);
        return result;
    }

    protected void resetFilter(){
        resetOldSimulatorNames();
        tabUI.fieldSimulParamsSelect.setModel(getSimulParamsSelectModel(false));
    }
    
    /**
     * Return old simulations.
     *
     * @return old simulations
     */
    public List<String> getOldSimulationItem() {

        //try {
            oldSimulNames = new ArrayList<>();

            //FilterModel<SimulationProperties, String> filterModel;
            // keep in context list of old simulation names (for filter process)
            oldSimulNames.addAll(SimulationStorage.getSimulationNames());
            // create filter model
            //filterModel = SimulationFilterUtil.createFilterModel(oldSimulNames);
            // to used directly model.getFilteredResult() in xml
            // we must fill filterModel result with original items
            //filterModel.selectAll();

            // chatellier : Ajout d'une entrée vide pour forcer la sélection
            // et que le changement lance un evenement sur la liste
            // Add empty item after, otherwise, un " " directory
            // will be created
            oldSimulNames.add(0, " ");

            return oldSimulNames;
        /*} catch (ParseException e) {
            if (log.isErrorEnabled()) {
                log.error("Can't get old simulation item", e);
            }
            ErrorHelper.showErrorDialog(t("isisfish.error.simulation.listoldsimulation"), e);
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Can't get old simulation item", e);
            }
            ErrorHelper.showErrorDialog(t("isisfish.error.simulation.listoldsimulation"), e);
        }
        return null;*/
    }

    public List<String> getFilteredOldSimulatorNames(boolean force) {
        if (oldSimulNames == null || force) {
            oldSimulNames = getOldSimulationItem();
        }
        return oldSimulNames;
    }

    public void setOldSimulatorNames(List<String> sn) {
        oldSimulNames = sn;
    }

    public void resetOldSimulatorNames() {
        oldSimulNames = getOldSimulationItem();
    }
    
    /**
     * Get strategies list to fill Jlist in ParamUI.
     * 
     * @return strategies list
     */
    public List<Strategy> getStrategies() {
        List<Strategy> result = new ArrayList<>();
        try {
            TopiaContext tx = getParameters().getRegion().getStorage().beginTransaction();
            result = RegionStorage.getFisheryRegion(tx).getStrategy();
            tx.rollbackTransaction();
            tx.closeContext();
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Can't get strategies", e);
            }
            ErrorHelper.showErrorDialog(t("isisfish.error.simulation.liststrategies"), e);
        }
        return result;
    }

    /**
     * Set parameters strategies.
     * 
     * @param strategies
     */
    public void setStrategies(List<Strategy> strategies) {
        List<Strategy> result = new ArrayList<>(strategies);
        getParameters().setStrategies(result);
    }

    /**
     * Get population list to fill JList in paramUI.
     * 
     * @return populations list
     */
    public List<Population> getPopulations() {

        List<Population> result = new ArrayList<>();
        try {
            TopiaContext tx = getParameters().getRegion().getStorage().beginTransaction();
            List<Species> species = RegionStorage.getFisheryRegion(tx)
                    .getSpecies();
            for (Species s : species) {
                Collection<Population> populations = s.getPopulation();

                // FIXME initialiaze lazy hibernate collection
                for (Population p : populations) {
                    p.getPopulationGroup().size();
                    p.getPopulationZone().size();
                }

                result.addAll(populations);
            }
            tx.rollbackTransaction();
            tx.closeContext();
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Can't get population", e);
            }
            ErrorHelper.showErrorDialog(t("isisfish.error.simulation.listpopulation"), e);
        }
        return result;
    }

    /**
     * Set parameter population.
     * 
     * @param populations populations to set
     */
    public void setPopulations(List<Population> populations) {
        List<Population> result = new ArrayList<>(populations);
        getParameters().setPopulations(result);
    }

    public int getNumberOfMonths() {
        return getParameters().getNumberOfMonths();
    }

    /**
     * Change number of simulation month by parsing string value.
     * Default to 1 if value is not parsable.
     *
     * @param months number of months to set
     */
    public void setNumberOfMonths(String months) {
        try {
            getParameters().setNumberOfMonths(Integer.parseInt(months));
        } catch (NumberFormatException ex) {
            // defaut to one year
            getParameters().setNumberOfMonths(1);
        }
    }
}
