/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2020 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.ui.simulator;

import fr.ifremer.isisfish.IsisFishException;
import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.datastore.RegionStorage;
import fr.ifremer.isisfish.datastore.SimulationPlanStorage;
import fr.ifremer.isisfish.datastore.StorageChangeEvent;
import fr.ifremer.isisfish.datastore.StorageChangeListener;
import fr.ifremer.isisfish.simulator.SimulationPlan;
import fr.ifremer.isisfish.ui.models.common.ScriptParametersParamModel;
import fr.ifremer.isisfish.ui.models.common.ScriptParametersParamNameRenderer;
import fr.ifremer.isisfish.ui.models.common.ScriptParametersParamValueEditor;
import fr.ifremer.isisfish.ui.models.common.ScriptParametersParamValueRenderer;
import fr.ifremer.isisfish.ui.widget.editor.ScriptParameterDialog;

import javax.swing.table.DefaultTableModel;
import java.util.List;

/**
 * Plan UI handler.
 */
public class PlanHandler extends SimulatorTabHandler {

    protected PlanUI planUI;

    protected StorageChangeListener simulationPlanStorageListener;

    protected PlanHandler(PlanUI planUI) {
        super(planUI);
        this.planUI = planUI;
    }



    protected void initSimulationPlansComboModel(StorageChangeEvent evt) {
        planUI.fieldSimulParamsSimulationPlansModel.setElementList(SimulationPlanStorage.getSimulationPlanNames());
    }

    protected void initSimulationSimulationPlansListModel() {
        planUI.listSimulParamsSimulationPlansModel.setElementList(getParamSimulationPlans());
    }

    protected void afterInit() {
        simulationPlanStorageListener = this::initSimulationPlansComboModel;
        SimulationPlanStorage.addStorageListener(simulationPlanStorageListener);
        this.initSimulationPlansComboModel(null);
        
        // model init
        initSimulationSimulationPlansListModel();
    }

    protected void switchSimulationPlanEnabled() {
        boolean selected = planUI.fieldSimulUseSimulationPlan.isSelected();
        getParameters().setUseSimulationPlan(selected);
        setTabSelectedIcon(planUI, selected);

        // clear selection
        planUI.fieldSimulParamsSimulationPlansSelect.setSelectedItem(null);
        planUI.listSimulParamsSimulationPlansList.clearSelection();
    }

    public void refresh() {
        planUI.fieldSimulUseSimulationPlan.setSelected(getParameters().getUseSimulationPlan());
        this.initSimulationPlansComboModel(null);
        this.initSimulationSimulationPlansListModel();
    }

    public List<SimulationPlan> getParamSimulationPlans() {
        return getParameters().getSimulationPlans();
    }

    protected void addSimulationPlan(PlanUI planUI, String name) {
        try {
            SimulationPlan sp = SimulationPlanStorage.getSimulationPlan(name).getNewInstance();
            
            // add it after autoconfiguration (if enabled)
            sp = (SimulationPlan)ScriptParameterDialog.displayConfigurationFrame(planUI, sp);
            if (sp != null) {
                getParameters().addSimulationPlan(sp);
            }
        } catch (IsisFishException ex) {
            throw new IsisFishRuntimeException("Can't add simulation plan", ex);
        }
    }

    protected void addSimulationPlan() {
        String selectedSimulationPlanName = (String) planUI.fieldSimulParamsSimulationPlansSelect.getSelectedItem();
        addSimulationPlan(planUI, selectedSimulationPlanName);
        initSimulationSimulationPlansListModel();
    }

    protected void removeSimulationPlan() {
        SimulationPlan selectedSimulationPlan = planUI.listSimulParamsSimulationPlansList.getSelectedValue();
        getParameters().removeSimulationPlan(selectedSimulationPlan);
        initSimulationSimulationPlansListModel();
        planUI.listSimulParamsSimulationPlansList.setSelectedValue(null, false);
    }

    protected void setSimulParamsSimulationPlans() {
        SimulationPlan selectedSimulationPlan = planUI.listSimulParamsSimulationPlansList.getSelectedValue();
        if (selectedSimulationPlan != null) {
            ScriptParametersParamModel model = new ScriptParametersParamModel(selectedSimulationPlan);
            planUI.simulParamsSimulationPlans.setModel(model);

            ScriptParametersParamNameRenderer nameRenderer = new ScriptParametersParamNameRenderer(selectedSimulationPlan);
            planUI.simulParamsSimulationPlans.getColumnModel().getColumn(0).setCellRenderer(nameRenderer);
            ScriptParametersParamValueRenderer valueRenderer = new ScriptParametersParamValueRenderer();
            planUI.simulParamsSimulationPlans.getColumnModel().getColumn(1).setCellRenderer(valueRenderer);

            ScriptParametersParamValueEditor cellEditor = new ScriptParametersParamValueEditor(selectedSimulationPlan);
            cellEditor.setRegionStorage(planUI.getContextValue(RegionStorage.class));
            planUI.simulParamsSimulationPlans.getColumnModel().getColumn(1).setCellEditor(cellEditor);
        } else {
            // clear table
            planUI.simulParamsSimulationPlans.setModel(new DefaultTableModel());
        }
    }
}
