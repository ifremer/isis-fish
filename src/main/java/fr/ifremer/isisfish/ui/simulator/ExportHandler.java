/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 - 2019 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.ui.simulator;

import static org.nuiton.i18n.I18n.t;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import javax.swing.event.ListSelectionEvent;

import fr.ifremer.isisfish.datastore.StorageChangeEvent;
import fr.ifremer.isisfish.datastore.StorageChangeListener;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.datastore.ExportStorage;
import fr.ifremer.isisfish.ui.WelcomePanelUI;

/**
 * Export UI handler.
 */
public class ExportHandler extends SimulatorTabHandler {

    /** Class logger. */
    private static final Log log = LogFactory.getLog(ExportHandler.class);

    protected ExportUI tabUI;

    protected StorageChangeListener exportStorageListener;

    protected ExportHandler(ExportUI tabUI) {
        super(tabUI);
        this.tabUI = tabUI;
    }

    protected void initListSimulExportChooseModel(StorageChangeEvent evt) {
        // get full list
        List<String> availableExports = ExportStorage.getExportNames();
        tabUI.listSimulExportChooseModel.setElementList(availableExports);

        // restore selection
        Collection<String> paramExports = getParameters().getExportNames();
        tabUI.listSimulExportChoose.clearSelection();
        for (String userExport : paramExports) {
            int index = availableExports.indexOf(userExport);
            tabUI.listSimulExportChoose.addSelectionInterval(index, index);
        }
    }

    protected void afterInit() {
        exportStorageListener = this::initListSimulExportChooseModel;
        ExportStorage.addStorageListener(exportStorageListener);
        initListSimulExportChooseModel(null);
    }
    
    public void refresh() {
        initListSimulExportChooseModel(null);
    }

    protected void saveSimulationExports(ListSelectionEvent event) {
        // Set exports (this can save empty list (not a big deal))
        List<String> exportNames = new LinkedList<>(tabUI.listSimulExportChoose.getSelectedValuesList());
        getParameters().setExportNames(exportNames);

        if (log.isDebugEnabled()) {
            log.debug("Set exports in simulation : " + exportNames);
        }
    }
    
    protected void saveConfigExports() {
        // Set exports (this can save empty list (not a big deal))
        List<String> exportNames = new LinkedList<>(tabUI.listSimulExportChoose.getSelectedValuesList());
        IsisFish.config.setDefaultExportNames(exportNames);

        setInfoText(t("isisfish.export.saved"));
    }
    
    protected void setInfoText(String txt) {
        // TODO remove getParentContainer use
        WelcomePanelUI root = tabUI.getParentContainer(WelcomePanelUI.class);
        root.setStatusMessage(txt);
    }
}
