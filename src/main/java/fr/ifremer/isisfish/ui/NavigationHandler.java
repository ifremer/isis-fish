/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

import javax.swing.tree.TreeModel;

import fr.ifremer.isisfish.entities.Cell;
import fr.ifremer.isisfish.entities.FisheryRegion;
import fr.ifremer.isisfish.entities.Gear;
import fr.ifremer.isisfish.entities.Metier;
import fr.ifremer.isisfish.entities.Observation;
import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.entities.Port;
import fr.ifremer.isisfish.entities.SetOfVessels;
import fr.ifremer.isisfish.entities.Species;
import fr.ifremer.isisfish.entities.Strategy;
import fr.ifremer.isisfish.entities.TripType;
import fr.ifremer.isisfish.entities.VesselType;
import fr.ifremer.isisfish.entities.Zone;
import fr.ifremer.isisfish.ui.input.InputContentUI;
import fr.ifremer.isisfish.ui.input.cell.CellUI;
import fr.ifremer.isisfish.ui.input.fisheryregion.FisheryRegionUI;
import fr.ifremer.isisfish.ui.input.gear.GearUI;
import fr.ifremer.isisfish.ui.input.metier.MetierUI;
import fr.ifremer.isisfish.ui.input.observation.ObservationUI;
import fr.ifremer.isisfish.ui.input.population.PopulationUI;
import fr.ifremer.isisfish.ui.input.port.PortUI;
import fr.ifremer.isisfish.ui.input.setofvessels.SetOfVesselsUI;
import fr.ifremer.isisfish.ui.input.species.SpeciesUI;
import fr.ifremer.isisfish.ui.input.strategy.StrategyUI;
import fr.ifremer.isisfish.ui.input.tree.FisheryTreeHelper;
import fr.ifremer.isisfish.ui.input.tree.FisheryTreeNode;
import fr.ifremer.isisfish.ui.input.triptype.TripTypeUI;
import fr.ifremer.isisfish.ui.input.vesseltype.VesselTypeUI;
import fr.ifremer.isisfish.ui.input.zone.ZoneUI;
import jaxx.runtime.JAXXContext;

/**
 * Handler for tree navigation related interfaces.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class NavigationHandler extends CommonHandler {

    /**
     * Cache pour n'instancier les ui qu'une seule fois
     * et eviter que l'affichage saute pour l'utilsateur.
     */
    protected Map<Class<?>, InputContentUI<?>> uiInstanceCache = new HashMap<>();

    /**
     * Get ui that can display internalClass.
     * 
     * @param internalClass internal class to get ui
     * @return navigationUI for class
     * @throws Exception 
     */
    protected InputContentUI<?> getUIInstanceForBeanClass(Class<?> internalClass, NavigationUI navigationUI) throws Exception {

        Class<? extends InputContentUI<?>> uiClass = null;
        if (FisheryRegion.class.isAssignableFrom(internalClass)) {
            uiClass = FisheryRegionUI.class;
        } else if (Cell.class.isAssignableFrom(internalClass)) {
            uiClass = CellUI.class;
        } else if (Gear.class.isAssignableFrom(internalClass)) {
            uiClass = GearUI.class;
        } else if (Metier.class.isAssignableFrom(internalClass)) {
            uiClass = MetierUI.class;
        } else if (Population.class.isAssignableFrom(internalClass)) {
            uiClass = PopulationUI.class;
        } else if (Port.class.isAssignableFrom(internalClass)) {
            uiClass = PortUI.class;
        } else if (SetOfVessels.class.isAssignableFrom(internalClass)) {
            uiClass = SetOfVesselsUI.class;
        } else if (Species.class.isAssignableFrom(internalClass)) {
            uiClass = SpeciesUI.class;
        } else if (Strategy.class.isAssignableFrom(internalClass)) {
            uiClass = StrategyUI.class;
        } else if (TripType.class.isAssignableFrom(internalClass)) {
            uiClass = TripTypeUI.class;
        } else if (VesselType.class.isAssignableFrom(internalClass)) {
            uiClass = VesselTypeUI.class;
        } else if (Zone.class.isAssignableFrom(internalClass)) {
            uiClass = ZoneUI.class;
        } else if (Observation.class.isAssignableFrom(internalClass)) {
            uiClass = ObservationUI.class;
        }

        // use map to implement UI cache
        InputContentUI<?> result = uiInstanceCache.get(uiClass);
        if (result == null) {
            Constructor<?> constructor = uiClass.getConstructor(JAXXContext.class);
            result = (InputContentUI<?>)constructor.newInstance(navigationUI);

            uiInstanceCache.put(uiClass, result);
        }

        return result;
    }

    /**
     * Change tree selection with new node id.
     * 
     * Called by specific UI (CellUI to change node).
     * 
     * @param inputUI context ui (to get context value tree helper...)
     * @param nodeId node id to select
     */
    public void setTreeSelection(InputContentUI<?> inputUI, String nodeId) {
        setTreeSelection(inputUI, null, nodeId);
    }

    /**
     * Change tree selection with new node id.
     * 
     * Called by specific UI (CellUI to change node).
     * 
     * @param inputUI context ui (to get context value tree helper...)
     * @param parentNodeId find node to select from this node
     * @param nodeId node id to select
     */
    public void setTreeSelection(InputContentUI<?> inputUI, String parentNodeId, String nodeId) {
        FisheryTreeHelper fisheryTreeHelper = inputUI.getContextValue(FisheryTreeHelper.class);
        TreeModel fisheryTreeModel = inputUI.getContextValue(TreeModel.class);
        FisheryTreeNode fromNode = (FisheryTreeNode)fisheryTreeModel.getRoot();
        if (parentNodeId != null) {
            fromNode = fisheryTreeHelper.findNode(fromNode, parentNodeId);
        }
        FisheryTreeNode newSelectNode = fisheryTreeHelper.findNode(fromNode, nodeId);
        if (newSelectNode != null) {
            fisheryTreeHelper.selectNode(newSelectNode);
        }
    }
}
