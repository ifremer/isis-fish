/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.result;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.ifremer.isisfish.IsisFishException;
import fr.ifremer.isisfish.datastore.ResultInfoStorage;

public class ResultInfoHelper {

    /** Class logger. */
    private static Log log = LogFactory.getLog(ResultInfoHelper.class);

    /**
     * Clean result set to remove ending ".java" if present.
     * 
     * @param results
     * @return set with clean result name
     */
    public static Set<String> cleanResultNames(Collection<String> results) {
        Set<String> returnSet = new HashSet<>();
        for (String result : results) {
            String cleanResult = StringUtils.removeEndIgnoreCase(result, ".java");
            returnSet.add(cleanResult);
        }
        return returnSet;
    }

    /**
     * Extract all necessary result from currentResult Set by adding all result's necessaryResults.
     * 
     * This class can cause Result class compilation.
     * 
     * @param currentResult input result set
     * @return output result set
     */
    public static Set<String> extractAllNecessaryResults(Set<String> currentResult) {
        Set<String> destResult = new HashSet<>();
        Set<String> inputResult = new HashSet<>(currentResult); // copy for local modification
        extractRecursiveResults(inputResult, destResult);
        return destResult;
    }

    /**
     * Extract all necessary result into destResult from initial currentResult Set.
     * 
     * @param inputResult src result list
     * @param destResult dest result list
     */
    protected static void extractRecursiveResults(Set<String> inputResult, Set<String> destResult) {
        Collection<String> unexplored = CollectionUtils.subtract(inputResult, destResult);

        for (String result : unexplored) {

            // mark as done
            // this way can handle infinite recursion defined by user scripts
            destResult.add(result);

            try {
                // add new
                ResultInfoStorage storage = ResultInfoStorage.getResultInfo(result);
                if (storage == null) {
                    if (log.isWarnEnabled()) {
                        log.warn("Can't find result : " + result);
                    }
                } else {
                    ResultInfo resultInfo = storage.getNewInstance();

                    if (resultInfo.getNecessaryResult() != null) {
                        Collections.addAll(inputResult, resultInfo.getNecessaryResult());
                    }
                }
            } catch (IsisFishException ex) {
                if (log.isErrorEnabled()) {
                    log.error("Result class can't be compiled", ex);
                }
            }
        }

        if (!unexplored.isEmpty()) { // recursion condition
            extractRecursiveResults(inputResult, destResult);
        }
    }
}
