/*
 * #%L
 * IsisFish
 *
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2020 Ifremer, Code Lutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.config;

import fr.ifremer.isisfish.actions.ExportAction;
import fr.ifremer.isisfish.actions.ImportAction;
import fr.ifremer.isisfish.actions.OtherAction;
import fr.ifremer.isisfish.actions.SimulationAction;
import fr.ifremer.isisfish.actions.ValidateSimulationAction;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

public enum IsisAction {
    HELP(n("Show help"), OtherAction.class.getName() + "#help", "-h", "--help"),
    VERSION(n("Show version"), OtherAction.class.getName() + "#version", "-v", "--version"),

    IMPORT_EXPORT(n(""), ImportAction.class.getName() + "#importExport", "--importExport"),
    IMPORT_RULE(n(""), ImportAction.class.getName() + "#importRule", "--importRule"),
    IMPORT_SCRIPT(n("Import one java file script source"), ImportAction.class.getName() + "#importScript", "--importScript"),
    IMPORT_SIMULATION_PLAN(n(""), ImportAction.class.getName() + "#importSimulationPlan", "--importSimulationPlan"),
    IMPORT_SIMULATOR(n(""), ImportAction.class.getName() + "#importSimulator", "--importSimulator"),
    IMPORT_FORMULA(n(""), ImportAction.class.getName() + "#importFormula", "--importFormula"),
    IMPORT_REGION(n(""), ImportAction.class.getName() + "#importRegion", "--importRegion"),
    IMPORT_REGION_AND_RENAME(n(""), ImportAction.class.getName() + "#importRegionAndRename", "--importRegionAndRename"),
    IMPORT_SIMULATION(n(""), ImportAction.class.getName() + "#importSimulation", "--importSimulation"),
    IMPORT_SCRIPT_MODULE(n("Import zipped file containing all scripts directory structure"), ImportAction.class.getName() + "#importScriptModule", "--importScriptModule"),

    LIST_EXPORT(n(""), ExportAction.class.getName() + "#listExport", "--listExport"),
    LIST_RULE(n(""), ExportAction.class.getName() + "#listRule", "--listRule"),
    LIST_SCRIPT(n(""), ExportAction.class.getName() + "#listScript", "--listScript"),
    LIST_SIMULATION_PLAN(n(""), ExportAction.class.getName() + "#listSimulationPlan", "--listSimulationPlan"),
    LIST_SIMULATOR(n(""), ExportAction.class.getName() + "#listSimulator", "--listSimulator"),
    LIST_FORMULA(n(""), ExportAction.class.getName() + "#listFormula", "--listFormula"),
    LIST_REGION(n(""), ExportAction.class.getName() + "#listRegion", "--listRegion"),
    LIST_SIMULATION(n(""), ExportAction.class.getName() + "#listSimulation", "--listSimulation"),

    EXPORT_EXPORT(n(""), ExportAction.class.getName() + "#exportExport", "--exportExport"),
    EXPORT_RULE(n(""), ExportAction.class.getName() + "#exportRule", "--exportRule"),
    EXPORT_SCRIPT(n(""), ExportAction.class.getName() + "#exportScript", "--exportScript"),
    EXPORT_SIMULATION_PLAN(n(""), ExportAction.class.getName() + "#exportSimulationPlan", "--exportSimulationPlan"),
    EXPORT_SIMULATOR(n(""), ExportAction.class.getName() + "#exportSimulator", "--exportSimulator"),
    EXPORT_FORMULA(n(""), ExportAction.class.getName() + "#exportFormula", "--exportFormula"),
    EXPORT_REGION(n(""), ExportAction.class.getName() + "#exportRegion", "--exportRegion"),
    EXPORT_SIMULATION(n(""), ExportAction.class.getName() + "#exportSimulation", "--exportSimulation"),

    SIMULATE_WITH_REGION(n(""), SimulationAction.class.getName() + "#simulateWithRegion", "--simulateWithRegion"),
    SIMULATE_WITH_SIMULATION(n(""), SimulationAction.class.getName() + "#simulateWithSimulation", "--simulateWithSimulation"),
    SIMULATE_WITH_SIMULATION_AND_SCRIPT(n(""), SimulationAction.class.getName() + "#simulateWithSimulationAndScript", "--simulateWithSimulationAndScript"),
    SIMULATE_REMOTELLY(n(""), SimulationAction.class.getName() + "#simulateRemotelly", "--simulateRemotelly"),
    SIMULATE_REMOTELLY_WITH_PRESCRIPT(n(""), SimulationAction.class.getName() + "#simulateRemotellyWithPreScript", "--simulateRemotellyWithPreScript"),
    SIMULATE_WITH_PRE_SCRIPT(n(""), SimulationAction.class.getName() + "#simulationWithRegionNameAndScript", "--simulationWithRegionNameAndScript"),

    VALIDATE_WITH_SIMULATION(n(""), ValidateSimulationAction.class.getName() + "#validateSimulation", "--validate");

    protected String description;
    protected String action;
    protected String[] aliases;

    IsisAction(String description, String action, String... aliases) {
        this.description = description;
        this.action = action;
        this.aliases = aliases;
    }

    public String getDescription() {
        return t(description);
    }

    public String getAction() {
        return action;
    }

    public String[] getAliases() {
        return aliases;
    }
}
