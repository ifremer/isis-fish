/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2024 Ifremer, Code Lutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.config;

import fr.ifremer.isisfish.IsisFishRuntimeException;
import fr.ifremer.isisfish.simulator.SimulationContext;
import fr.ifremer.isisfish.simulator.SimulationControl;
import fr.ifremer.isisfish.util.cache.IsisCacheBackend;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.config.ArgumentsParserException;
import org.nuiton.math.matrix.Vector;
import org.nuiton.util.StringUtil;
import org.nuiton.version.Version;
import org.nuiton.version.Versions;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static org.nuiton.i18n.I18n.t;

/**
 * Isis fish configuration.
 * 
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class IsisConfig extends ApplicationConfig {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(IsisConfig.class);

    /** Numero de version majeure d'isis fish. */
    public static final int ISIS_FISH_MAJOR_VERSION = 4;

    /**
     * Config step after init.
     *
     * @see org.nuiton.config.ApplicationConfig.Action.Step
     */
    public static final int STEP_AFTER_INIT = 0;
    
    /**
     * Config step after init vcs.
     * 
     * @see org.nuiton.config.ApplicationConfig.Action.Step
     */
    public static final int STEP_AFTER_INIT_VCS = 1;
    
    /**
     * Config step after ui.
     * 
     * @see org.nuiton.config.ApplicationConfig.Action.Step
     */
    public static final int STEP_AFTER_UI = 2;
    
    /**
     * Config step before exit.
     * 
     * @see org.nuiton.config.ApplicationConfig.Action.Step
     */
    public static final int STEP_BEFORE_EXIT = 3;
    
    /**
     * La version du logiciel constitue de l.d.a.r
     * <ul>
     * <li>l: le numero de version du logiciel</li>
     * <li>d: le numero de version du schema de la base de donnees</li>
     * <li>a: le numero de version de l'api des scripts</li>
     * <li>r: le numero de version de de l'interface graphique ou autre modif mineur</li>
     * </ul>
     * <p>
     * lors de l'increment de l, d, a et r sont remis a 0
     * lors de l'increment de d, a et r sont remis a 0
     * lors de l'increment de a, r est remis a 0
     * <p>
     * Un changement d'UI ne modifie jamais le numero de version de database
     * Un changement de schema de base pour lequel on ne peut pas faire de
     * migration de donnees demande automatiquement un changement de version
     * d'application.
     */
    protected static Version version;
    protected static Version databaseVersion;
    protected static Version apiVersion;
    public static final String COMPRESSION_EXTENSION = ".gz";

    /**
     * Le nombre global ex: 3.2.0.0
     * 
     * @return full version
     */
    static public String getVersion() {
        String result = version.toString();
        return result;
    }

    /**
     * La version de la base ex: 3.2
     * 
     * @return database version
     */
    public static Version getDatabaseVersion() {
        return databaseVersion;
    }

    /**
     * La version de l'api de programmation ex: 3.2.0
     * 
     * @return api version
     */
    public static Version getApiVersion() {
        return apiVersion;
    }
    
    /** separateur de liste */
    static final public String SEP = ",";
    
    /**
     * Mémorise une unité de temps en millisecondes.
     * 
     * Auparavant, c'etait {@link System#nanoTime()}, mais cette unité de temps
     * n'est pas lié a une date.
     * Par exemple, ca permet juste de mesuré avec une precision de 10-9 le temps
     * ecoulé, par l'heure qu'il est à cette unité de temps.
     */
    protected long startingTime = System.currentTimeMillis();

    public static File currentTempDirectory;
    
    public IsisConfig() {

        for (Option o : Option.values()) {
            if (o.defaultValue != null) {
                setDefaultOption(o.key, o.defaultValue);
            }
        }
        
        for (IsisAction a : IsisAction.values()) {
            for (String alias : a.aliases) {
                addActionAlias(alias, a.action);                
            }
        }
    }

    public static String getEnvUserHome() {
        String result;/* = System.getenv("SCRATCH");
        if (StringUtils.isNotBlank(result)) {
            if (log.isInfoEnabled()) {
                log.info("Using datarmor scratch home : " + result);
            }
        } else {*/
            result = ApplicationConfig.getUserHome();
            if (log.isInfoEnabled()) {
                log.info("Using user home : " + result);
            }
        //}
        return result;
    }

    @Override
    public ApplicationConfig parse(String... args) throws ArgumentsParserException {
        ApplicationConfig config = super.parse(args);
        postInitWithVersion(config.getOption("isisfish.version"));
        return config;
    }

    /**
     * Initialize avec des valeurs par defaut des options dans la valeur dépend de la version.
     * 
     * @param fullVersion full version (ex 4.3.1.9)
     */
    protected void postInitWithVersion(String fullVersion) {
        IsisConfig.version = Versions.valueOf(fullVersion);
        IsisConfig.databaseVersion = Versions.extractVersion(IsisConfig.version, 0, 1);
        IsisConfig.apiVersion = Versions.extractVersion(IsisConfig.version, 0, 2);

        setDefaultOption(Option.SIMULATOR_DATARMOR_SSH_ISISHOME.key, "/appli/emh-commun/isis-fish/isis-fish-" + apiVersion.toString());
    }
    
    //////////////////////////////////////////////////
    // Methode d'acces aux options
    //////////////////////////////////////////////////

    public String getIsisHomeDirectory() {
        String result = getOption(Option.ISIS_HOME_DIRECTORY.key);
        return result;
    }

    public void setIsisHomeDirectory(String homeDirectory) {
        setOption(Option.ISIS_HOME_DIRECTORY.key, homeDirectory);
    }

    /**
     * Retourne le repertoire racine de toutes les donnees (script, simulation
     * region, ...)
     * 
     * @return database directory
     */
    public File getDatabaseDirectory() {
        File result = getOptionAsFile(Option.DATABASE_DIRECTORY.key);
        return result;
    }
    
    /**
     * Retourne le repertoire racine de toutes les donnees communauté (commit utilisateur).
     * 
     * @return community database directory
     */
    public File getCommunityDatabaseDirectory() {
        File result = getOptionAsFile(Option.COMMUNITY_DATABASE_DIRECTORY.key);
        return result;
    }

    /**
     * Get database directory to use for script.
     * 
     * Defaut to {@link #getDatabaseDirectory()} value, but to
     * {@link SimulationContext#getScriptDirectory()} during a simulation.
     * 
     * @return script database directory
     */
    public File getContextDatabaseDirectory() {

        // add a subdirectory for simulation context
        SimulationContext simContext = SimulationContext.get();
        File scriptDirectory = simContext.getScriptDirectory();

        if (scriptDirectory == null) {
            // if null, not in simulation 
            scriptDirectory = getDatabaseDirectory();
        }

        return scriptDirectory;
    }

    /**
     * Retourne le lock a utiliser pour la base h2.
     * 
     * Par defaut la base utilise 'file'.
     * 
     * Les valeurs acceptées sont :
     *  - file
     *  - FS
     *  - socket
     *  - no
     * 
     * http://www.h2database.com/html/features.html#database_file_locking
     * 
     * @return h2 db lock mode
     */
    public String getDatabaseLockMode() {
        String result = getOption(Option.DATABASE_LOCK_MODE.key);
        return result;
    }

    /**
     * Retourne le repertoire ou sont stockes les scripts compiles.
     * 
     * Create directory if not exists.
     * 
     * Make a specific build directory for running simulation.
     * 
     * @return compilation directory
     */
    public File getCompileDirectory() {
        File result = getOptionAsFile(Option.COMPILATION_DIRECTORY.key);

        // add a subdirectory for simulation context
        SimulationContext simContext = SimulationContext.get();
        SimulationControl control = simContext.getSimulationControl();
        if (control != null) {
            result = getOptionAsFile(Option.COMPILATION_SIM_DIRECTORY.key);
            result = new File(result, control.getId());
        }

        return result;
    }

    /**
     * Retourne le repertoire ou sont stockées les informations relatives
     * aux simulations en cours.
     *
     * @return monitoring directory
     */
    public File getRegionCacheDirectory() {
        File result = getOptionAsFile(Option.REGION_CACHE_DIRECTORY.key);
        if (!result.exists()) {
            result.mkdirs();
        }
        return result;
    }

    /**
     * Retourne le repertoire ou sont stockées les informations relatives
     * aux simulations en cours.
     * 
     * @return monitoring directory
     */
    public File getMonitoringDirectory() {
        File result = getOptionAsFile(Option.MONITORING_DIRECTORY.key);
        if (!result.exists()) {
            result.mkdirs();
        }
        return result;
    }

    /**
     * Get javadoc directory.
     * 
     * Create directory if not exists.
     *
     * @return javadoc directory
     */
    public File getJavadocDirectory() {
        File result = getOptionAsFile(Option.JAVADOC_DIRECTORY.key);
        if (!result.exists()) {
            result.mkdirs();
        }
        return result;
    }

    public File getCurrentTempDirectory() {
        if (currentTempDirectory == null) {
            File tempDirectory = getOptionAsFile(Option.TEMP_DIRECTORY.key);
            String yyyyMMddHHmmss = DateFormatUtils.format(startingTime, "yyyyMMddHHmmss");
            currentTempDirectory = new File(tempDirectory, "tmp-" + yyyyMMddHHmmss + "-" + RandomUtils.nextInt());
            if (!currentTempDirectory.exists()) {
                currentTempDirectory.mkdirs();
            }
        }
        return currentTempDirectory;
    }

    public static void clearCurrentTempDirectory() {
        currentTempDirectory = null;
    }

    /**
     * Get log directory.
     *
     * Create directory if not exists.
     *
     * @return log directory
     */
    public File getLogDirectory() {
        File result = getOptionAsFile(Option.LOG_DIRECTORY.key);
        if (!result.exists()) {
            result.mkdirs();
        }
        return result;
    }

    /**
     * Get swing session file.
     *
     * @return swing session file
     */
    public File getSwingSessionFile() {
        File result = getOptionAsFile(Option.SWING_SESSION_FILE.key);
        return result;
    }

    /**
     * Retourne l'objet {@link Locale} a utilise pour la langue.
     * 
     * @return application {@link Locale}
     */
    public Locale getLocale() {
        String value = getOption(Option.LOCALE.key);
        Locale result = (Locale)ConvertUtils.convert(value, Locale.class);
        return result;
    }

    /**
     * Change application locale.
     * (used during application config command line parse)
     * 
     * @param locale new locale
     */
    public void setLocale(String locale) {
        setOption(Option.LOCALE.key, locale);
    }

    /**
     * Retourne l'encoding a utiliser pour les fichiers textes.
     * 
     * @return encoding to use
     */
    public String getEncoding() {
        String result = getOption(Option.ENCODING.key);
        return result;
    }

    /**
     * Retourne le nom usuel de l'utilisateur.
     * 
     * @return username
     */
    public String getUserName() {
        String result = getOption(Option.USER_NAME.key);
        return result;
    }
    
    /**
     * Retourne l'email de l'utilisateur.
     * 
     * @return user email
     */
    public String getUserMail() {
        String result = getOption(Option.USER_MAIL.key);
        return result;
    }
    
    public String getSimulatorClassfile() {
        String result = getOption(Option.SIMULATOR_CLASSFILE.key);
        return result;
    }
    
    public void setSimulatorClassfile(String value) {
        setOption(Option.SIMULATOR_CLASSFILE.key, value);
    }

    /**
     * Retourne le nombre maximum de thread à utiliser en in process.
     * 
     * @return number of thread to use
     */
    public int getSimulatorInMaxThreads() {
        int result = getOptionAsInt(Option.SIMULATOR_IN_MAXTHREADS.key);
        return result;
    }

    public void setSimulatorInMaxThreads(int value) {
        setOption(Option.SIMULATOR_IN_MAXTHREADS.key, String.valueOf(value));
    }

    /**
     * Retourne le nombre de sous processus maximum a creer.
     * 
     * @return number of sub process to create
     */
    public int getSimulatorSubMaxProcess() {
        int result = getOptionAsInt(Option.SIMULATOR_SUB_MAXPROCESS.key);
        return result;
    }

    public void setSimulatorSubMaxProcess(int value) {
        setOption(Option.SIMULATOR_SUB_MAXPROCESS.key, String.valueOf(value));
    }
    
    /**
     * Retourne la quantité de ram (Xmx) a alloue au sous processus.
     * 
     * @return memory amount to use for sub process
     */
    public String getSimulatorSubMaxMemory() {
        String result = getOption(Option.SIMULATOR_SUB_MAXMEMORY.key);
        return result;
    }

    public void setSimulatorSubMaxMemory(String value) {
        setOption(Option.SIMULATOR_SUB_MAXMEMORY.key, value);
    }

    /**
     * Retourne l'url du serveur de simulation accessible via SSH.
     * 
     * @return simulator server
     */
    public String getSimulatorSshServer() {
        String result = getOption(Option.SIMULATOR_DATARMOR_SSH_SERVER.key);
        return result;
    }

    /**
     * Change ssh server url value.
     */
    public void setSimulatorSshServer(String sshServer) {
        setOption(Option.SIMULATOR_DATARMOR_SSH_SERVER.key, sshServer);
    }

    /**
     * Retourne le login pour acceder au serveur de simulation accessible via SSH.
     * 
     * @return simulator username
     */
    public String getSimulatorSshUsername() {
        String result = getOption(Option.SIMULATOR_DATARMOR_SSH_USERNAME.key);
        return result;
    }

    /**
     * Change ssh username.
     * 
     * @param username username
     */
    public void setSimulatorSshUsername(String username) {
        setOption(Option.SIMULATOR_DATARMOR_SSH_USERNAME.key, username);
    }

    /**
     * Retourne le chemin distant ou est installé isis.
     * 
     * @return remote isis home
     */
    public String getSimulatorSshIsisHome() {
        String result = getOption(Option.SIMULATOR_DATARMOR_SSH_ISISHOME.key);
        return result;
    }

    /**
     * Change isis home on ssh server.
     * 
     * @param isishome isis home
     */
    public void setSimulatorSshIsisHome(String isishome) {
        setOption(Option.SIMULATOR_DATARMOR_SSH_ISISHOME.key, isishome);
    }
    
    /**
     * PBS qsub options (command line).
     * 
     * See man qsub for available options.
     * 
     * @return options
     */
    public String getSimulatorSshPbsQsubOptions() {
        String result = getOption(Option.SIMULATOR_DATARMOR_QSUB_OPTIONS.key);
        return result;
    }

    /**
     * Change PBS qsub options (command line).
     * 
     * @param options new options
     */
    public void setSimulatorSshPbsQsubOptions(String options) {
        setOption(Option.SIMULATOR_DATARMOR_QSUB_OPTIONS.key, options);
    }
    
    /**
     * Java path directory.
     * 
     * @return path
     */
    public String getSimulatorSshJavaPath() {
        String result = getOption(Option.SIMULATOR_DATARMOR_SSH_JAVAPATH.key);
        return result;
    }

    /**
     * Change Java bin path.
     * 
     * @param path new path
     */
    public void setSimulatorSshJavaPath(String path) {
        setOption(Option.SIMULATOR_DATARMOR_SSH_JAVAPATH.key, path);
    }

    /**
     * Retourne l'interval de temps a utiliser
     * pour recuperer le fichier de control.
     * 
     * @return time (in seconds)
     */
    public int getSimulatorSshControlCheckInterval() {
        int result = getOptionAsInt(Option.SIMULATOR_DATARMOR_SSH_CONTROLCHECKINTERVAL.key);
        return result;
    }

    /**
     * Change control check interval.
     * 
     * @param interval interval
     */
    public void setSimulatorSshControlCheckInterval(int interval) {
        setOption(Option.SIMULATOR_DATARMOR_SSH_CONTROLCHECKINTERVAL.key, String.valueOf(interval));
    }
    
    /**
     * Retourne le nombre de thread simultane a utiliser par le pool de thread
     * du launcher SSH.
     * 
     * @return number of thread to use
     */
    public int getSimulatorSshMaxThreads() {
        int result = getOptionAsInt(Option.SIMULATOR_DATARMOR_MAXTHREADS.key);
        return result;
    }

    /**
     * Set number of ssh simulation thread to use.
     * 
     * @param max max
     */
    public void setSimulatorSshMaxThreads(int max) {
        setOption(Option.SIMULATOR_DATARMOR_MAXTHREADS.key, String.valueOf(max));
    }

    /**
     * Retourne la quantité de ram (Xmx) a alloue au processus sur caparmor.
     * 
     * @return number of thread to use
     */
    public String getSimulatorSshMaxMemory() {
        String result = getOption(Option.SIMULATOR_DATARMOR_MAXMEMORY.key);
        return result;
    }

    /**
     * Set ssh simulation process memory to use
     * 
     * @param memory memory
     */
    public void setSimulatorSshMaxMemory(String memory) {
        setOption(Option.SIMULATOR_DATARMOR_MAXMEMORY.key, memory);
    }

    public String getSimulatorFtpServer() {
        String result = getOption(Option.SIMULATOR_DATARMOR_FTP_SERVER.key);
        return result;
    }

    public void setSimulatorFtpServer(String value) {
        setOption(Option.SIMULATOR_DATARMOR_FTP_SERVER.key, value);
    }

    public String getSimulatorFtpLogin() {
        String result = getOption(Option.SIMULATOR_DATARMOR_FTP_LOGIN.key);
        return result;
    }

    public void setSimulatorFtpLogin(String value) {
        setOption(Option.SIMULATOR_DATARMOR_FTP_LOGIN.key, value);
    }

    public String getSimulatorFtpPassword() {
        String result = getOption(Option.SIMULATOR_DATARMOR_FTP_PASSWORD.key);
        return result;
    }

    public void setSimulatorFtpPassword(String value) {
        setOption(Option.SIMULATOR_DATARMOR_FTP_PASSWORD.key, value);
    }

    /**
     * Retourne la clé privée de l'utilisteur courant.
     * 
     * @return private ssh key path
     */
    public File getSSHPrivateKeyFilePath() {
        File result = getOptionAsFile(Option.SSH_KEY_FILE.key);
        return result;
    }

    /**
     * Set ssh private key file path.
     * 
     * @param sshFile new value
     */
    public void setSSHPrivateKeyFilePath(File sshFile) {
        if (sshFile == null) {
            setOption(Option.SSH_KEY_FILE.key, null);
        } else {
            setOption(Option.SSH_KEY_FILE.key, sshFile.getAbsolutePath());
        }
    }

    /**
     * Get launch.ui option value.
     * 
     * @return launch ui option value
     */
    public boolean isLaunchUI() {
        boolean result = getOptionAsBoolean(Option.LAUNCH_UI.key);
        return result;
    }

    /**
     * Get perform vcs update option value.
     * 
     * @return perform vcs update option value
     */
    public boolean isPerformVcsUpdate() {
        boolean result = getOptionAsBoolean(Option.PERFORM_VCS_UPDATE.key);
        return result;
    }

    /**
     * Get perform cron option value.
     * 
     * @return perform cron option value
     */
    public boolean isPerformCron() {
        boolean result = getOptionAsBoolean(Option.PERFORM_CRON.key);
        return result;
    }

    /**
     * Get script auto configuration option value.
     * 
     * @return autoconfiguration
     */
    public boolean isScriptAutoconfig() {
        boolean result = getOptionAsBoolean(Option.SIMULATION_SCRIPT_AUTOCONFIG.key);
        return result;
    }

    public String getDefaultExportNames() {
        String result = getOption(Option.DEFAULT_EXPORT_NAMES.key);
        return result;
    }

    /**
     * Force la compression des fichiers d'export
     *
     * @return
     * @since 4.4.0.0
     */
    public boolean getExportForceCompression() {
        ApplicationConfig config = SimulationContext.get().getConfig();
        boolean result = config.getOptionAsBoolean(Option.EXPORT_FORCE_COMPRESSION.key);
        return result;
    }

    public void setExportForceCompression(boolean value) {
        setOption(Option.EXPORT_FORCE_COMPRESSION.key, String.valueOf(value));
    }

    public void setDefaultExportNames(List<String> exportNames) {
        StringBuilder sb = new StringBuilder();
        for (String exportName : exportNames) {
            sb.append(SEP).append(exportName);
        }
        String value = sb.toString();
        // remove first SEP
        if (value.length() > 0) {
            value = value.substring(1);
        }
        setOption(Option.DEFAULT_EXPORT_NAMES.key, value);
        saveForUser();
    }

    /**
     * @return la liste des noms d'exports par defaut sous forme de liste,
     *         a partir de la propriete {@link Option#DEFAULT_EXPORT_NAMES}
     *         ou null si ils n'ont jamais ete sauves par l'utilisateur.
     *         by user.
     * @see Option#DEFAULT_EXPORT_NAMES
     */
    public List<String> getDefaultExportNamesAsList() {
        List<String> result = null;
        String exportNamesList = getDefaultExportNames();
        if (exportNamesList != null) {
            String[] exportNames = StringUtil.split(exportNamesList, ",");
            result = new ArrayList<>(Arrays.asList(exportNames));
        }
        return result;
    }

   public String getDefaultMapFilename() {
        String result = getOption(Option.DEFAULT_MAP_FILENAME.key);
        return result;
    }

   public String getDefaultResultNames() {
        String result = getOption(Option.DEFAULT_RESULT_NAMES.key);
        return result;
    }

    public void setDefaultResultNames(List<String> resultNames) {
        StringBuilder sb = new StringBuilder();
        for (String resultName : resultNames) {
            sb.append(SEP).append(resultName);
        }
        String value = sb.toString();
        // remove first SEP
        if (value.length() > 0) {
            value = value.substring(1);
        }
        setOption(Option.DEFAULT_RESULT_NAMES.key, value);
        saveForUser();
    }

    /**
     * @return les resultats par defaut d'une simulation sous forme de liste
     *         a partir de la propriete {@link Option#DEFAULT_RESULT_NAMES}
     * @see Option#DEFAULT_RESULT_NAMES
     */
    public List<String> getDefaultResultNamesAsList() {
        List<String> result = null;
        String resultNamesList = getDefaultResultNames();
        if (resultNamesList != null) {
            String[] resultNames = StringUtil.split(resultNamesList, ",");
            result = new ArrayList<>(Arrays.asList(resultNames));
        }
        return result;
    }
    
    public String getDefaultTagValue() {
        String result = getOption(Option.DEFAULT_TAG_VALUE.key);
        return result;
    }
    
    public void setDefaultTagValues(Map<String, String> tagValues) {
        StringBuilder sb = new StringBuilder(" ");
        for (Map.Entry<String, String> entry : tagValues.entrySet()) {
            sb.append(SEP).append('"').append(entry.getKey()).append("\":\"").append(entry.getValue()).append('"');
        }
        String value = sb.toString().trim();
        setOption(Option.DEFAULT_TAG_VALUE.key, value.isEmpty() ? "" : value.substring(1));
        saveForUser();
    }

    /**
     * Retourne toutes les configurations specifiques aux simulations.
     * Ces configurations sont cotes clients, mais lorsqu'on fait une simulation
     * sur un serveur, il faut quelles soient utilisees a la place des valeurs
     * du serveur
     *
     * @return
     */
    public Map<String, String> getDefaultSimulationConfig() {
        Map<String, String> result = new HashMap<>();

        result.put(Option.SIMULATION_MATRIX_VECTOR_CLASS.key, getOption(Option.SIMULATION_MATRIX_VECTOR_CLASS.key));
        result.put(Option.SIMULATION_MATRIX_VECTOR_SPARSE_CLASS.key, getOption(Option.SIMULATION_MATRIX_VECTOR_SPARSE_CLASS.key));
        result.put(Option.SIMULATION_MATRIX_THRESHOLD_USE_SPARSE_CLASS.key, getOption(Option.SIMULATION_MATRIX_THRESHOLD_USE_SPARSE_CLASS.key));
        result.put(Option.SIMULATION_MATRIX_USE_LAZY_VECTOR.key, getOption(Option.SIMULATION_MATRIX_USE_LAZY_VECTOR.key));
        result.put(Option.SIMULATION_STORE_RESULT_ON_DISK.key, getOption(Option.SIMULATION_STORE_RESULT_ON_DISK.key));
        result.put(Option.SIMULATION_STORE_RESULT_CACHE_STEP.key, getOption(Option.SIMULATION_STORE_RESULT_CACHE_STEP.key));

        result.put(Option.CACHE_BACKEND_FACTORY_CLASS.key, getOption(Option.CACHE_BACKEND_FACTORY_CLASS.key));

        result.put(Option.EXPORT_FORCE_COMPRESSION.key, getOption(Option.EXPORT_FORCE_COMPRESSION.key));

        return result;
    }
    
    /**
     * @return le dictionnaire des tags par defaut d'une simulation a partir
     *         de la propriete  {@link Option#DEFAULT_TAG_VALUE}
     * @see Option#DEFAULT_TAG_VALUE
     */
    public Map<String, String> getDefaultTagValueAsMap() {
        Map<String, String> result = new HashMap<>();
        String tagValuesList = getDefaultTagValue();
        if (tagValuesList != null) {
            String[] tagValues = StringUtil.split(tagValuesList, ",");
            for (String tagValue : tagValues) {
                String[] tagAndValue = StringUtil.split(tagValue, ":");

                String tag = tagAndValue[0].trim();
                tag = tag.substring(1, tag.length() - 1); // remove "..."

                String value = tagAndValue[1].trim();
                value = value.substring(1, value.length() - 1); // remove "..."

                result.put(tag, value);
            }
        }
        return result;
    }
    
    public String getIsisFishURL() {
        String result = getOption(Option.ISIS_URL.key);
        return result;
    }

    public String getJavadocIsisURL() {
        String result = getOption(Option.JAVADOC_ISIS_URL.key);
        return result;
    }

    public String getJavadocMatrixURL() {
        String result = getOption(Option.JAVADOC_MATRIX_URL.key);
        return result;
    }

    public String getJavadocTopiaURL() {
        String result = getOption(Option.JAVADOC_TOPIA_URL.key);
        return result;
    }

    public String getJavadocJavaURL() {
        String result = getOption(Option.JAVADOC_JAVA_URL.key);
        return result;
    }

    /**
     * Retourne le temps de départ de l'application en millisecondes.
     *
     * @return application start millis time
     */
    public long getStartingTime() {
        return startingTime;
    }
    
    /**
     * Retourne le temps écoulé depuis de debut de l'application (en secondes).
     * 
     * @return le temps écoulé en seconde sous forme de chaine
     */
    public String getElapsedTimeAsString() {
        long diff = System.currentTimeMillis() - getStartingTime();
        String result = DurationFormatUtils.formatDuration(diff, "s'.'S");
        return result;
    }

    /**
     * Retourne un class loader contenant le repertoire de compilation
     * Il permet alors de charger des classes qui viennent d'etre compilees
     * dans isis
     * If current thread is in simulation then return specific simulation
     * compilation directory, else default compilation directory
     *
     * @return the class loader adequate
     */
    public ClassLoader getScriptClassLoader() {
        SimulationContext simContext = SimulationContext.get();
        ClassLoader result = simContext.getClassLoader();
        if (result == null) {
            // on est pas dans une simulation, il faut retourner un nouveau
            // a chaque fois. on force la creation d'un nouveau classloader
            // a chaque fois pour
            // que l'ancienne class compiler et charge ne soit pas presente
            URL[] cp = getScriptDirectoryURLs();

            // il faut prendre le ClassLoader du thread courant comme parent
            // car pour les simulations il a ete modifie, et il faut
            // que les classes de script soit recherche dedans avant
            // la recherche dans le getCompileDirectory().
            // ce qui est le cas avec les URLClassLoader
            ClassLoader parent = Thread.currentThread().getContextClassLoader();
            result = new URLClassLoader(cp, parent);

        }
        return result;
    }

    public URL[] getScriptDirectoryURLs() {
        URL[] cp;
        File f = getCompileDirectory();
        try {
            cp = new URL[]{f.toURI().toURL()};
        } catch (MalformedURLException eee) {
            throw new IsisFishRuntimeException(t("isisfish.error.load.classloader", f, eee.getMessage()), eee);
        }
        return cp;
    }

    /**
     * L'implementation de la class gérant les implémentations de vecteur de
     * matrice pour les fichiers mappés pour le rendu des resultats
     *
     * @return
     * @since 4.2.1.1
     */
    public Class getMappedResultMatrixVectorClass() {
        Class result = getOptionAsClass(Option.MAPPED_RESULT_MATRIX_VECTOR_CLASS.key);
        return result;
    }

    /**
     * L'implementation de la class gérant les implémentations de vecteur de
     * matrice pleine pour les simulations
     *
     * @return
     * @since 4.2.0.2
     */
    public Class<Vector> getSimulationMatrixVectorClass() {
        ApplicationConfig config = SimulationContext.get().getConfig();
        Class<Vector> result = (Class<Vector>)config.getOptionAsClass(Option.SIMULATION_MATRIX_VECTOR_CLASS.key);
        return result;
    }

    public void setSimulationMatrixVectorClass(Class<Vector> value) {
        setOption(Option.SIMULATION_MATRIX_VECTOR_CLASS.key, value.getName());
    }

    /**
     * L'implementation de la class gérant les implémentations de vecteur de
     * matrice creuse pour les simulations
     *
     * @return
     * @since 4.2.0.2
     */
    public Class<Vector> getSimulationMatrixVectorSparseClass() {
        ApplicationConfig config = SimulationContext.get().getConfig();
        Class<Vector> result = (Class<Vector>)config.getOptionAsClass(Option.SIMULATION_MATRIX_VECTOR_SPARSE_CLASS.key);
        return result;
    }

    public void setSimulationMatrixVectorSparseClass(Class<Vector> value) {
        setOption(Option.SIMULATION_MATRIX_VECTOR_SPARSE_CLASS.key, value.getName());
    }

    /**
     * Seuil d'utilisation des matrices creuse
     *
     * @return
     * @since 4.3.1.0
     */
    public int getSimulationMatrixThresholdUseSparse() {
        ApplicationConfig config = SimulationContext.get().getConfig();
        int result = config.getOptionAsInt(Option.SIMULATION_MATRIX_THRESHOLD_USE_SPARSE_CLASS.key);
        return result;
    }

    public void setSimulationMatrixThresholdUseSparse(int value) {
        setOption(Option.SIMULATION_MATRIX_THRESHOLD_USE_SPARSE_CLASS.key, String.valueOf(value));
    }

    /**
     * Seuil d'utilisation des matrices creuse
     *
     * @return
     * @since 4.3.1.0
     */
    public boolean getSimulationMatrixdUseLazyVector() {
        ApplicationConfig config = SimulationContext.get().getConfig();
        boolean result = config.getOptionAsBoolean(Option.SIMULATION_MATRIX_USE_LAZY_VECTOR.key);
        return result;
    }

    public void setSimulationMatrixdUseLazyVector(boolean value) {
        setOption(Option.SIMULATION_MATRIX_USE_LAZY_VECTOR.key, String.valueOf(value));
    }

    /**
     * Indique le nombre de pas qui doivent etre sauve, en partant du dernier pas
     * de temps.
     *
     * @return
     * @since 4.3.1.0
     */
    public int getSimulationStoreResultOnDisk() {
        ApplicationConfig config = SimulationContext.get().getConfig();
        int result = config.getOptionAsInt(Option.SIMULATION_STORE_RESULT_ON_DISK.key);
        return result;
    }

    public void setSimulationStoreResultOnDisk(int value) {
        setOption(Option.SIMULATION_STORE_RESULT_ON_DISK.key, String.valueOf(value));
    }

    /**
     * Indique le nombre de pas qui doivent rester en memoire durant la simulation
     *
     * @return
     * @since 4.3.1.0
     */
    public int getSimulationStoreResultCacheStep() {
        ApplicationConfig config = SimulationContext.get().getConfig();
        int result = config.getOptionAsInt(Option.SIMULATION_STORE_RESULT_CACHE_STEP.key);
        return result;
    }

    public void setSimulationStoreResultCacheStep(int value) {
        setOption(Option.SIMULATION_STORE_RESULT_CACHE_STEP.key, String.valueOf(value));
    }

    /**
     * Retourne la factory a utilise pour le cache
     *
     * @return factory a utilise pour le backend de cache
     * @since 4.2.1.2
     */
    public IsisCacheBackend.Factory getCacheBackendFactoryClass() {
        ApplicationConfig config = SimulationContext.get().getConfig();
        IsisCacheBackend.Factory result = config.getOptionAsObject(
                IsisCacheBackend.Factory.class, Option.CACHE_BACKEND_FACTORY_CLASS.key);
        return result;
    }

    public String getVcsUserName() {
        String result = getOption(Option.VCS_USER_NAME.key);
        return result;
    }

    public void setVcsUserName(String value) {
        setOption(Option.VCS_USER_NAME.key, value);
    }

    public String getVcsUserPassword() {
        String result = getOption(Option.VCS_USER_PASSWORD.key);
        return result;
    }

    public void setVcsUserPassword(String value) {
        setOption(Option.VCS_USER_PASSWORD.key, value);
    }

    public String getVcsCommunityUserName() {
        String result = getOption(Option.VCS_COMMUNITY_USER_NAME.key);
        return result;
    }

    public void setVcsCommunityUserName(String value) {
        setOption(Option.VCS_COMMUNITY_USER_NAME.key, value);
    }

    public String getVcsCommunityUserPassword() {
        String result = getOption(Option.VCS_COMMUNITY_PASSWORD.key);
        return result;
    }

    public void setVcsCommunityUserPassword(String value) {
        setOption(Option.VCS_COMMUNITY_PASSWORD.key, value);
    }

    public String getBugReportUrl() {
        String result = getOption(Option.BUG_REPORT_URL.key);
        return result;
    }

    public String getKeystorePasswords() {
        String result = getOption(Option.KEYSTORE.key);
        return result;
    }

    public void setKeystorePasswords(String value) {
        setOption(Option.KEYSTORE.key, value);
    }

    public LocalDate getJavaVersionCheckDate() {
        String result = getOption(Option.JAVA_VERSION_CHECK_DATE.key);
        LocalDate localDate = null;
        if (StringUtils.isNotBlank(result)) {
            localDate = LocalDate.parse(result);
        }
        return localDate;
    }

    public void setJavaVersionCheckDate(LocalDate value) {
        setOption(Option.JAVA_VERSION_CHECK_DATE.key, String.valueOf(value));
    }

}
