/*
 * #%L
 * IsisFish
 *
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2024 Ifremer, Code Lutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.config;

import fr.ifremer.isisfish.simulator.launcher.InProcessSimulatorLauncher;
import fr.ifremer.isisfish.simulator.launcher.SSHSimulatorLauncher;
import fr.ifremer.isisfish.simulator.launcher.SimulationService;
import fr.ifremer.isisfish.simulator.launcher.SubProcessSimulationLauncher;
import fr.ifremer.isisfish.util.cache.IsisCacheBackendOnGuava;
import fr.ifremer.isisfish.vcs.VCS;
import org.nuiton.math.matrix.DoubleBigVector;
import org.nuiton.math.matrix.DoubleSparseHashVector;

import java.io.File;

import static fr.ifremer.isisfish.config.IsisConfig.ISIS_FISH_MAJOR_VERSION;
import static fr.ifremer.isisfish.config.IsisConfig.getEnvUserHome;
import static org.nuiton.config.ApplicationConfig.CONFIG_FILE_NAME;
import static org.nuiton.config.ApplicationConfig.getUserHome;
import static org.nuiton.i18n.I18n.n;

/**
 * Options.
 *
 * Set it protected to force getOption() call.
 */
public enum Option {

    CONFIG_FILE(CONFIG_FILE_NAME, n("isisfish.config.main.configFileName.description"), "isis-config-" + ISIS_FISH_MAJOR_VERSION),
    ISIS_HOME_DIRECTORY("isis.home.directory", n("isisfish.config.main.compileDirectory.description"), getEnvUserHome() + File.separator + "isis-fish-" + ISIS_FISH_MAJOR_VERSION),
    COMPILATION_DIRECTORY("compilation.directory", n("isisfish.config.main.compileDirectory.description"), "${isis.home.directory}" + File.separator + "isis-build"),
    COMPILATION_SIM_DIRECTORY("compilation.sim.directory", n("isisfish.config.main.compileSimDirectory.description"), "${isis.home.directory}" + File.separator + "isis-sim-build"),
    REGION_CACHE_DIRECTORY("region.cache.directory", n("isisfish.config.main.regionCacheDirectory.description"), "${isis.home.directory}" + File.separator + "isis-region-cache"),
    MONITORING_DIRECTORY("monitoring.directory", n("isisfish.config.main.monitoringDirectory.description"), "${isis.home.directory}" + File.separator + "isis-monitoring"),
    JAVADOC_DIRECTORY("javadoc.directory", n("isisfish.config.main.javadocDirectory.description"), "${isis.home.directory}" + File.separator + "isis-docs"),
    TEMP_DIRECTORY("temp.directory", n("isisfish.config.main.defaultTempDirectory.description"), "${isis.home.directory}" + File.separator + "isis-temp"),
    LOG_DIRECTORY("log.directory", n("isisfish.config.main.defaultLogDirectory.description"), "${isis.home.directory}" + File.separator + "isis-log"),
    SWING_SESSION_FILE("swing.session.file", n("isisfish.config.main.swingSession.description"), "${isis.home.directory}" + File.separator + "session.xml"),

    EXPORT_FORCE_COMPRESSION("export.force.compression", n("isisfish.config.export.force.compression.description"), "false"),
    DEFAULT_EXPORT_NAMES("default.export.names", n("isisfish.config.main.defaultExportNames.description"), ""),
    DEFAULT_RESULT_NAMES("default.result.names", n("isisfish.config.main.defaultResultNames.description"), ""),
    DEFAULT_MAP_FILENAME("default.map.filename", n("isisfish.config.main.defaultMapFile.description"), "maps/vmap_area_thin.shp"),
    DEFAULT_TAG_VALUE("default.tagvalue", n("isisfish.config.main.defaultTagValue.description"), ""),
    ENCODING("encoding", n("isisfish.config.main.encoding.description"), "UTF-8"),

    ISIS_URL("isis.url", null, "http://isis-fish.org/"),
    JAVADOC_ISIS_URL("javadoc.isis.url", null, "http://api.isis-fish.org/"),
    JAVADOC_MATRIX_URL("javadoc.matrix.url", null, "http://nuiton.page.nuiton.org/nuiton-matrix/nuiton-matrix/apidocs/"),
    JAVADOC_JAVA_URL("javadoc.java.url", null, "https://docs.oracle.com/javase/8/docs/api/"),
    JAVADOC_TOPIA_URL("javadoc.topia.url", null, "http://nuiton.page.nuiton.org/topia-2.x/topia-persistence/apidocs/"),

    SIMULATOR_CLASSFILE("simulator.classfile", n("isisfish.config.main.defaultSimulator.description"), "DefaultSimulator.java"),
    /** prevu pour l'architecture de lancement en plugin: local, isis-server, caparmor, ... */
    SIMULATOR_LAUNCHER(SimulationService.SIMULATION_LAUNCHER + ".3", n("isisfish.config.main.localSimulator.description"), InProcessSimulatorLauncher.class.getName()),
    SIMULATOR_LAUNCHER2(SimulationService.SIMULATION_LAUNCHER + ".1", n("isisfish.config.main.subSimulator.description"), SubProcessSimulationLauncher.class.getName()),
    SIMULATOR_LAUNCHER_REMOTE(SimulationService.SIMULATION_LAUNCHER + ".2", n("isisfish.config.main.remoteDatarmor.description"), SSHSimulatorLauncher.class.getName()),


    // pour les simulations
    SIMULATION_MATRIX_VECTOR_CLASS("simulation.matrix.vector.class", n("isisfish.config.simulation.matrix.vector.class.description"), DoubleBigVector.class.getName()),
    SIMULATION_MATRIX_VECTOR_SPARSE_CLASS("simulation.matrix.vector.sparse.class", n("isisfish.config.simulation.matrix.vector.sparse.class.description"), DoubleSparseHashVector.class.getName()),
    SIMULATION_MATRIX_THRESHOLD_USE_SPARSE_CLASS("simulation.matrix.threshold.use.sparse.class", n("isisfish.config.simulation.matrix.threshold.use.sparse.class.description"), "1000"),
    SIMULATION_MATRIX_USE_LAZY_VECTOR("simulation.matrix.use.lazy.vector", n("isisfish.config.simulation.matrix.use.lazy.vector.description"), "true"),
    SIMULATION_STORE_RESULT_ON_DISK("simulation.store.result.ondisk", n("isisfish.config.simulation.store.result.ondisk.description"), "-1"),
    SIMULATION_STORE_RESULT_CACHE_STEP("simulation.store.result.cachestep", n("isisfish.config.simulation.store.result.cachestep.description"), "13"),

    /** Nombre maximum de thread de simulation in process. */
    SIMULATOR_IN_MAXTHREADS("simulation.in.max.threads", n("isisfish.config.main.simulation.in.max.threads.description"), "1"),

    /** Nombre max de sous processus à lancer (-1 = pas de limite). */
    SIMULATOR_SUB_MAXPROCESS("simulation.sub.max.process", n("isisfish.config.main.simulation.sub.max.process.description"), "-1"),
    /** Mémoire max (Xmx) allouée aux sous processus. */
    SIMULATOR_SUB_MAXMEMORY("simulation.sub.max.memory", n("isisfish.config.main.simulation.sub.max.memory.description"), "1024M"),

    /** Serveur accessible par ssh : address */
    SIMULATOR_DATARMOR_SSH_SERVER("simulation.datarmor.ssh.server", n("isisfish.config.main.simulation.ssh.server.description"), "datarmor.ifremer.fr"),
    /** Serveur accessible par ssh : login */
    SIMULATOR_DATARMOR_SSH_USERNAME("simulation.datarmor.ssh.username", n("isisfish.config.main.simulation.ssh.username.description"), ""),
    /** Serveur accessible par ssh : remote isis home install */
    SIMULATOR_DATARMOR_SSH_ISISHOME("simulation.datarmor.ssh.isishome.emh.448", n("isisfish.config.main.simulation.ssh.isishome.description"), null),
    /** Serveur accessible par SSH : emplacement de Java (full path) */
    SIMULATOR_DATARMOR_SSH_JAVAPATH("simulation.datarmor.ssh.javapath.emh.17", n("isisfish.config.main.simulation.ssh.javapath.description"), "/appli/emh-commun/isis-fish/jdk17/bin/java"),
    /** Serveur accessible par SSH : option de l'executable qsub (defaut to -m n = no mail) */
    SIMULATOR_DATARMOR_QSUB_OPTIONS("simulation.datarmor.qsub.options", n("isisfish.config.main.simulation.datarmor.qsub.options.description"), "-m n -l mem=2GB -l walltime=01:00:00"),
    /** Serveur accessible par SSH : interval de check du fichier de control */
    SIMULATOR_DATARMOR_SSH_CONTROLCHECKINTERVAL("simulation.datarmor.ssh.control.check.interval", n("isisfish.config.main.simulation.ssh.control.check.interval.description"), "120"),
    /** Serveur accessible par SSH : nombre de thread au maximum a utilise simultanement */
    SIMULATOR_DATARMOR_MAXTHREADS("simulation.datarmor.ssh.max.threads", n("isisfish.config.main.simulation.ssh.max.threads.description"), "1"),
    /** Serveur accessible par SSH : mémoire (Xmx) allouée pour les process java sur datarmor */
    SIMULATOR_DATARMOR_MAXMEMORY("simulation.datarmor.ssh.max.memory", n("isisfish.config.main.simulation.ssh.max.memory.description"), "2000M"),

    SIMULATOR_DATARMOR_FTP_SERVER("simulation.datarmor.ftp.server", n("isisfish.config.main.simulation.ftp.server.description"), "eftp.ifremer.fr"),
    SIMULATOR_DATARMOR_FTP_LOGIN("simulation.datarmor.ftp.login", n("isisfish.config.main.simulation.ftp.login.description"), ""),
    SIMULATOR_DATARMOR_FTP_PASSWORD("simulation.datarmor.ftp.password", n("isisfish.config.main.simulation.ftp.password.description"), ""),

    /** Application locale (for i18n init). */
    LOCALE("locale", n("isisfish.config.main.locale.description"), "fr_FR"),

    /** Perform ui launch option. */
    LAUNCH_UI("launch.ui", n("isisfish.config.main.launchUI.description"), "true"),
    /** Perform vcs update option. */
    PERFORM_VCS_UPDATE("perform.vcsupdate", n("isisfish.config.main.performvcsupdate.description"), "true"),
    /** Perform cron option. */
    PERFORM_CRON("perform.cron", n("isisfish.config.main.performcron.description"), "true"),
    /** Auto configure script parameter when addind it */
    SIMULATION_SCRIPT_AUTOCONFIG("script.autoconfig", n("isisfish.config.main.script.autoconfig.description"), "true"),

    /** Database lock mode (h2).*/
    DATABASE_LOCK_MODE("database.lockmode", n("isisfish.config.database.lockmode.description"), "file"),

    // SSH (global, for both VCS and datarmor)
    SSH_KEY_FILE("ssh.key.file", n("isisfish.config.ssh.key.file.description"), getUserHome() + File.separator + ".ssh" + File.separator + "isis_rsa"),

    DATABASE_DIRECTORY("database.directory", n("isisfish.config.vcs.localDatabasePath.description"), "${isis.home.directory}" + File.separator + "isis-database"),
    VCS_TYPE(VCS.VCS_TYPE, n("isisfish.config.vcs.type.description"), VCS.TYPE_SVN),
    VCS_PROTOCOL(VCS.VCS_PROTOCOL, n("isisfish.config.vcs.protocol.description"), "http"),
    VCS_USER_NAME(VCS.VCS_USER_NAME, n("isisfish.config.vcs.userName.description"), ""),
    VCS_USER_PASSWORD(VCS.VCS_USER_PASSWORD, n("isisfish.config.vcs.userPassword.description"), ""),
    VCS_HOST_NAME(VCS.VCS_HOST_NAME, n("isisfish.config.vcs.hostName.description"), "svn.forge.codelutin.com"),
    VCS_PATH(VCS.VCS_PATH, n("isisfish.config.vcs.remotePath.description"), "/svn/isis-fish-data"),

    // community vsc
    COMMUNITY_DATABASE_DIRECTORY("community.database.directory", n("isisfish.config.vcs.localCommunityDatabasePath.description"), "${isis.home.directory}" + File.separator + "isis-community-database"),
    VCS_COMMUNITY_TYPE("vcs.community.type", n("isisfish.config.vcs.community.type.description"), VCS.TYPE_SVN),
    VCS_COMMUNITY_PROTOCOL("vcs.community.protocole", n("isisfish.config.vcs.community.protocol.description"), "http"),
    VCS_COMMUNITY_USER_NAME("vcs.community.username", n("isisfish.config.vcs.community.username.description"), ""),
    VCS_COMMUNITY_PASSWORD("vcs.community.password", n("isisfish.config.vcs.community.password.description"), ""),
    VCS_COMMUNITY_HOST_NAME("vcs.community.hostname", n("isisfish.config.vcs.community.hostname.description"), "svn.forge.codelutin.com"),
    VCS_COMMUNITY_PATH("vcs.community.path", n("isisfish.config.vcs.community.remotepath.description"), "/svn/isis-fish-community"),

    // misc options
    USER_NAME("user.name", n("isisfish.config.main.userName.description"), System.getProperty("user.name")),
    SMTP_SERVER("smtpServer", n("isisfish.config.main.smtpServer.description"), "smtp"),
    USER_MAIL("userMail", n("isisfish.config.main.userMail.description"), USER_NAME.defaultValue + "@" + VCS_HOST_NAME.defaultValue),
    BUG_REPORT_URL("bug.report.url", n("isisfish.config.bug.report.url.description"), "https://forge.codelutin.com/projects/isis-fish/issues/new"),
    MAPPED_RESULT_MATRIX_VECTOR_CLASS("mapped.result.matrix.vector.class", n("isisfish.config.mapped.result.matrix.vector.class.description"), DoubleBigVector.class.getName()),
    CACHE_BACKEND_FACTORY_CLASS("cache.backend.factory.class", n("isisfish.config.cache.backend.factory.class.description"), IsisCacheBackendOnGuava.IsisCacheBackendOnGuavaFactory.class.getName()),
    JAVA_VERSION_CHECK_DATE("isisfish.java.version.checkdate", null, null),
    KEYSTORE("isisfish.keystore", null, null);

    public String key;
    public String description;
    public String defaultValue;

    Option(String key, String description, String defaultValue) {
        this.key = key;
        this.description = description;
        this.defaultValue = defaultValue;
    }
}
