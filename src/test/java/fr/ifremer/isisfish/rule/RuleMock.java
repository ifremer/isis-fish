/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2012 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.rule;

import fr.ifremer.isisfish.entities.Metier;
import fr.ifremer.isisfish.simulator.SimulationContext;
import fr.ifremer.isisfish.types.TimeStep;

/**
 * A basic rule implementation.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class RuleMock extends AbstractRule {

    public String description;
    
    public RuleMock(String description) {
        this.description = description;
    }

    @Override
    public String[] getNecessaryResult() {
        return null;
    }

    @Override
    public String getDescription() throws Exception {
        return description;
    }

    @Override
    public void init(SimulationContext context) throws Exception {

    }

    @Override
    public boolean condition(SimulationContext context, TimeStep step, Metier metier)
            throws Exception {
        return false;
    }

    @Override
    public void preAction(SimulationContext context, TimeStep step, Metier metier)
            throws Exception {
        // TODO Auto-generated method stub

    }

    @Override
    public void postAction(SimulationContext context, TimeStep step, Metier metier)
            throws Exception {

    }
}
