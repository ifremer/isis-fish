/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 - 2018 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.equation;

import fr.ifremer.isisfish.AbstractIsisFishTest;
import fr.ifremer.isisfish.util.EvaluatorHelper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Equation related tests.
 * 
 * @author chatellier
 */
public class EquationTest extends AbstractIsisFishTest {
    
    /**
     * Try to compile all equation with a default content to detect basic
     * compile error.
     */
    @Test
    public void testCompileAllEquations() {
        test(EmigrationEquation.class, EmigrationEquation.DEFAULT_CONTENT, new Object[5]);
        test(ImmigrationEquation.class, ImmigrationEquation.DEFAULT_CONTENT, new Object[5]);
        test(MigrationEquation.class, ImmigrationEquation.DEFAULT_CONTENT, new Object[6]);
        test(PopulationCapturabilityEquation.class, ImmigrationEquation.DEFAULT_CONTENT, new Object[4]);
        test(PopulationGrowth.class, PopulationGrowth.DEFAULT_CONTENT, null, 0.0, null);
        test(PopulationGrowthReverse.class, PopulationGrowthReverse.DEFAULT_CONTENT, null, 0.0, null);
        test(PopulationMaturityOgiveEquation.class, PopulationMaturityOgiveEquation.DEFAULT_CONTENT, new Object[2]);
        test(PopulationMeanWeight.class, PopulationMeanWeight.DEFAULT_CONTENT, new Object[2]);
        test(PopulationNaturalDeathRate.class, PopulationNaturalDeathRate.DEFAULT_CONTENT, new Object[4]);
        test(PopulationPrice.class, PopulationPrice.DEFAULT_CONTENT, new Object[2]);
        test(PopulationRecruitmentEquation.class, PopulationRecruitmentEquation.DEFAULT_CONTENT, new Object[5]);
        test(PopulationReproductionEquation.class, PopulationReproductionEquation.DEFAULT_CONTENT, null, null, null, null, 0.0, null, null, null, null);
        test(PopulationReproductionRateEquation.class, PopulationReproductionRateEquation.DEFAULT_CONTENT, new Object[2]);
        test(SelectivityEquation.class, SelectivityEquation.DEFAULT_CONTENT, new Object[4]);
        test(SoVTechnicalEfficiencyEquation.class, SoVTechnicalEfficiencyEquation.DEFAULT_CONTENT, new Object[3]);
        test(StrategyInactivityEquation.class, StrategyInactivityEquation.DEFAULT_CONTENT, new Object[3]);
        test(TargetSpeciesTargetFactorEquation.class, TargetSpeciesTargetFactorEquation.DEFAULT_CONTENT, null, null, null, null, false);
        test(VariableEquation.class, VariableEquation.DEFAULT_CONTENT, new Object[3]);
    }

    /**
     * Try to set content in all equation content and test to get this result.
     * 
     * @param clazz class to test
     * @param args equation args
     */
    protected void test(Class clazz, String content, Object... args) {
        double result = (double)EvaluatorHelper.evaluate("fr.isisfish.equation",
                    "TestCompileAll", clazz, content, args);
        // XXX: a améliorer, mais à l'heure actuelle, c'est les seules valeurs retournées
        Assertions.assertTrue(result == 0.0 || result == 1.0);
    }
}
