/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2012 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish;

import fr.ifremer.isisfish.config.IsisConfig;
import fr.ifremer.isisfish.config.Option;
import fr.ifremer.isisfish.datastore.AutoMigrationIsisH2Config;
import fr.ifremer.isisfish.datastore.CodeSourceStorage.Location;
import fr.ifremer.isisfish.datastore.DataStorageTestHelper;
import freemarker.cache.ClassTemplateLoader;
import freemarker.ext.beans.BeansWrapper;
import freemarker.template.Configuration;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.HiddenFileFilter;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.nuiton.config.ArgumentsParserException;
import org.nuiton.util.FileUtil;

import java.io.File;

/**
 * Abstract test case for isis fish.
 * 
 * Contains BeforeClass and AfterClass method to proper init cases.
 * 
 * Each test is done in a isolated isis-database directory, and user.home
 * is set to this directory.
 *
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public abstract class AbstractIsisFishTest {

    protected static File dirIsisBase;

    /**
     * Return specific temp dir.
     * 
     * @return system temp dir
     */
    public static File getTestDirectory() {
        String tempdir = System.getProperty("java.io.tmpdir");

        File tempdirfile = new File(tempdir);
        if (!tempdirfile.exists()) {
            tempdirfile.mkdirs();
        }

        return tempdirfile;
    }

    /**
     * Toujours appeler cette method pour les test d'isis.
     * (sinon, il ira ecrire dans le isis-database officiel).
     * 
     * Create a temp dir and init isis with that temp dir as database.
     * 
     * @throws Exception
     */
    @BeforeAll
    public static void init() throws Exception {

        File mavenTestDir = getTestDirectory();
        dirIsisBase = FileUtil.createTempDirectory("isisdbtest", "", mavenTestDir);

        System.setProperty("user.home", dirIsisBase.getAbsolutePath());
        System.setProperty(Option.LAUNCH_UI.key, "false");
        System.setProperty(Option.ISIS_HOME_DIRECTORY.key, dirIsisBase.getAbsolutePath());
        System.setProperty(Option.SSH_KEY_FILE.key, dirIsisBase.getAbsolutePath() + File.separator + "ssh" + File.separator + "isis_test_dsa");

        IsisFish.init();
        IsisFish.initVCS();
        IsisFish.initCommunityVCS(); // for ui testing

        IsisConfig.clearCurrentTempDirectory(); // prevent tests to fail

        // install a new topia migration service callback
        // to not ask for user for migration during test
        AutoMigrationIsisH2Config.setTestMigrationCallBack();

        // reset static caches
        DataStorageTestHelper.clearAllCache();

        FileUtils.copyDirectory(new File("src/test/resources/test-database").getAbsoluteFile(),
            IsisFish.config.getDatabaseDirectory(), HiddenFileFilter.VISIBLE);
    }

    /**
     * Some tests modify configuration, reset it before all tests.
     * @throws ArgumentsParserException 
     */
    @BeforeEach
    public void resetConfig() throws ArgumentsParserException {
        IsisFish.config = new IsisConfig();
        IsisFish.config.parse();

        // fix static cache in enum
        Location.OFFICIAL.setDirectory(IsisFish.config.getDatabaseDirectory());
        Location.COMMUNITY.setDirectory(IsisFish.config.getCommunityDatabaseDirectory());
        Location.ALL.setDirectory(IsisFish.config.getDatabaseDirectory(), IsisFish.config.getCommunityDatabaseDirectory());
    }
    
    /**
     * Return current database directory.
     * 
     * @return current database directory
     */
    protected static File getCurrentDatabaseDirectory() {
        return dirIsisBase;
    }

    /**
     * Delete created temp directory.
     */
    @AfterAll
    public static void clean() {
        System.clearProperty("user.home");
        if (dirIsisBase != null) {
            FileUtils.deleteQuietly(dirIsisBase);
            dirIsisBase = null;
        }
    }

    /**
     * Return common script freemarker configuration.
     * 
     * @return freemarker {@link Configuration}
     */
    protected static Configuration getFreemarkerConfiguration() {
        Configuration freemarkerConfiguration = new Configuration(Configuration.VERSION_2_3_25);

        // needed to overwrite "Defaults to default system encoding."
        // fix encoding issue on some systems
        freemarkerConfiguration.setDefaultEncoding("utf-8");

        // specific template loader to get template from jars (classpath)
        ClassTemplateLoader templateLoader = new ClassTemplateLoader(AbstractIsisFishTest.class, "/");
        freemarkerConfiguration.setTemplateLoader(templateLoader);

        freemarkerConfiguration.setObjectWrapper(new BeansWrapper(Configuration.VERSION_2_3_25));

        return freemarkerConfiguration;
    }
}
