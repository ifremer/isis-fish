/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2012 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator.sensitivity;

import fr.ifremer.isisfish.rule.Rule;
import fr.ifremer.isisfish.rule.RuleMock;
import fr.ifremer.isisfish.simulator.sensitivity.domain.ContinuousDomain;
import fr.ifremer.isisfish.simulator.sensitivity.domain.DiscreteDomain;
import fr.ifremer.isisfish.simulator.sensitivity.domain.RuleDiscreteDomain;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.nuiton.math.matrix.MatrixFactory;
import org.nuiton.math.matrix.MatrixND;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Factors test.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$ By : $Author$
 */
public class FactorTest {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    private static Log log = LogFactory.getLog(FactorTest.class);

    /**
     * Test to build new factors with int values.
     */
    @Test
    public void testIntFactor() {

        Factor factor = new Factor("testint");
        ContinuousDomain domain = new ContinuousDomain(Distribution.QUNIFMM);
        domain.addDistributionParam("min", 0.0);
        domain.addDistributionParam("max", 50.0);
        factor.setDomain(domain);
        factor.setPath("org.nuiton.factor#1234567890#0.12242345354#name");
        factor.setValueForIdentifier(0.5);

        Assertions.assertEquals(25.0, factor.getValue());

        if (log.isInfoEnabled()) {
            log.info("factor#toString() = " + factor);
        }
    }

    /**
     * Test factor with matrix.
     * 
     * @see MatrixND
     */
    @Test
    public void testMatrixFactor() {

        // matrix 1
        MatrixND matrix1 = MatrixFactory.getInstance().create("test1",
                new int[] { 3, 2 }, new String[] { "col1", "col2" });
        matrix1.setValue(new int[] { 0, 0 }, 13);
        matrix1.setValue(new int[] { 0, 1 }, -14);
        matrix1.setValue(new int[] { 1, 0 }, 21);
        matrix1.setValue(new int[] { 1, 1 }, 2);
        matrix1.setValue(new int[] { 2, 0 }, 12);
        matrix1.setValue(new int[] { 2, 1 }, -1);

        // matrix 2
        MatrixND matrix2 = MatrixFactory.getInstance().create("test2",
                new int[] { 2, 3 }, new String[] { "col1", "col2" });
        matrix2.setValue(new int[] { 0, 0 }, 9999);
        matrix2.setValue(new int[] { 0, 1 }, 15000);
        matrix2.setValue(new int[] { 0, 2 }, -40000);
        matrix2.setValue(new int[] { 1, 0 }, 21345);
        matrix2.setValue(new int[] { 1, 1 }, 81000);
        matrix2.setValue(new int[] { 1, 2 }, -13000);

        // factor
        Factor factor = new Factor("testmatrix");
        DiscreteDomain domain = new DiscreteDomain();
        domain.getValues().put("m1", matrix1);
        domain.getValues().put("m2", matrix2);
        factor.setDomain(domain);
        factor.setPath("org.nuiton.math.matrix.MatrixND#563456293453#2.456347646#dim");
        factor.setValueForIdentifier("m2");

        Assertions.assertEquals(matrix2, factor.getValue());

        try {
            factor.setValueForIdentifier("blah blah");
            Assertions.fail("Can't set identifier not present in domain");
        } catch (IllegalArgumentException e) {
            if (log.isInfoEnabled()) {
                log.info("Exception normally thrown");
            }
        }

        if (log.isInfoEnabled()) {
            log.info("factor#toString() = " + factor);
        }
    }

    /**
     * Test factor with matrix.
     * 
     * @see MatrixND
     */
    @Test
    public void testEquationContinuousFactor() {

        // factor
        Factor factor = new Factor("testequation");
        ContinuousDomain domain = new ContinuousDomain(Distribution.QUNIFPC);
        domain.addDistributionParam("reference", 3.0);
        domain.addDistributionParam("coefficient", 0.1);
        factor.setEquationVariableName("Linf");
        factor.setDomain(domain);
        factor.setPath("org.nuiton.math.matrix.MatrixND#563456293453#2.456347646#dim");
        factor.setValueForIdentifier(0.1);

        Assertions.assertEquals(2.76, (Double)factor.getValue(),0.0000001);
        //Assertions.assertEquals(2.7,(Double)((ContinuousDomain)factor.getDomain()).getMinBound(),0);
        //Assertions.assertEquals(3.3,(Double)((ContinuousDomain)factor.getDomain()).getMaxBound(),0);

        // factor 2
        Factor factor2 = new Factor("testequation");
        ContinuousDomain domain2 = new ContinuousDomain(Distribution.QUNIFPC);
        domain2.addDistributionParam("reference", 3.0);
        domain2.addDistributionParam("coefficient", 0.1);
        factor2.setEquationVariableName("Linf");
        factor2.setDomain(domain2);
        factor2.setPath("org.nuiton.math.matrix.MatrixND#563456293453#2.456347646#dim");
        factor2.setValueForIdentifier(0.1);

        Assertions.assertEquals(2.76, (Double)factor2.getValue(),0.0000001);
        //Assertions.assertEquals(2.7,(Double)((ContinuousDomain)factor.getDomain()).getMinBound(),0);
        //Assertions.assertEquals(3.3,(Double)((ContinuousDomain)factor.getDomain()).getMaxBound(),0);

        // factor 3
        Factor factor3 = new Factor("testequation");
        ContinuousDomain domain3 = new ContinuousDomain(Distribution.QUNIFPC);
        domain3.addDistributionParam("reference", 3.0);
        domain3.addDistributionParam("coefficient", 0.1);
        factor3.setEquationVariableName("Linf");
        factor3.setDomain(domain3);
        factor3.setPath("org.nuiton.math.matrix.MatrixND#563456293453#2.456347646#dim");
        factor3.setValueForIdentifier(0.2);

        Assertions.assertEquals(2.82, (Double)factor3.getValue(),0.0000001);
        //Assertions.assertEquals(2.7,(Double)((ContinuousDomain)factor.getDomain()).getMinBound(),0);
        //Assertions.assertEquals(3.3,(Double)((ContinuousDomain)factor.getDomain()).getMaxBound(),0);

        // factor 4
        Factor factor4 = new Factor("testequation");
        ContinuousDomain domain4 = new ContinuousDomain(Distribution.QUNIFPC);
        domain4.addDistributionParam("reference", 3.0);
        domain4.addDistributionParam("coefficient", 0.05);
        factor4.setEquationVariableName("Linf");
        factor4.setDomain(domain4);
        factor4.setPath("org.nuiton.math.matrix.MatrixND#563456293453#2.456347646#dim");
        factor4.setValueForIdentifier(0.1);

        Assertions.assertEquals(2.88, (Double)factor4.getValue(),0.0000001);
        //Assertions.assertEquals(2.7,(Double)((ContinuousDomain)factor.getDomain()).getMinBound(),0);
        //Assertions.assertEquals(3.3,(Double)((ContinuousDomain)factor.getDomain()).getMaxBound(),0);

        if (log.isInfoEnabled()) {
            log.info("factor#toString() = " + factor);
        }
    }
    
    /**
     * Test factor with matrix.
     * 
     * @see MatrixND
     */
    @Test
    public void testRuleFactor() {

        // available rules
        Rule ruleA = new RuleMock("hour restriction");
        Rule ruleB = new RuleMock("zone restriction");
        Rule ruleC = new RuleMock("ship number restriction");

        // rules set
        Collection<Rule> rules1 = new ArrayList<>(); // 0 rule
        Collection<Rule> rules2 = new ArrayList<>(); // 1 rule
        rules2.add(ruleA);
        Collection<Rule> rules3 = new ArrayList<>(); // 3 rule
        rules3.add(ruleA);
        rules3.add(ruleB);
        rules3.add(ruleC);

        // factor and domain definition
        Factor factor = new Factor("testrule");
        RuleDiscreteDomain ruleDomain = new RuleDiscreteDomain();
        ruleDomain.getValues().put("rules1", rules1);
        ruleDomain.getValues().put("rules2", rules2);
        ruleDomain.getValues().put("rules3", rules3);
        factor.setDomain(ruleDomain);

        // TODO post r operation, and some asserts
    }
    
    /**
     * Assert that clone do work (with inheritance).
     */
    @Test
    public void testClone() {
        
        Factor factor = new Factor("testclone");
        factor.setPath("fr.ifremer.isisfish.entities.Cell#lenght");
        factor.setComment("answer to life");
        
        ContinuousDomain domain = new ContinuousDomain(Distribution.QUNIFMM);
        domain.addDistributionParam("min", 0.0);
        domain.addDistributionParam("max", 50.0);
        factor.setDomain(domain);
        factor.setCardinality(2);

        factor.setValueForIdentifier(0.84);

        // clone
        Factor clone = (Factor)factor.clone();

        // test on clone
        Assertions.assertEquals("testclone", clone.getName());
        Assertions.assertEquals("fr.ifremer.isisfish.entities.Cell#lenght", clone.getPath());
        Assertions.assertEquals("answer to life", clone.getComment());
        Assertions.assertEquals(42.0, clone.getValue());
        Assertions.assertNotSame(domain, clone.getDomain());
        Assertions.assertEquals(42.0, clone.getDomain().getValueForIdentifier(0.84));
        Assertions.assertEquals(2, clone.getCardinality());
    }
}
