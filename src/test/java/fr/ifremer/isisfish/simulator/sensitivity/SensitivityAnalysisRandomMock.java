/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2011 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator.sensitivity;

import java.io.File;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.simulator.launcher.SimulationServiceTest;
import fr.ifremer.isisfish.simulator.sensitivity.domain.ContinuousDomain;
import fr.ifremer.isisfish.simulator.sensitivity.domain.DiscreteDomain;

/**
 * Implementation of SensitivityAnalysis that take random value in available
 * factors.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$ By : $Author$
 */
public class SensitivityAnalysisRandomMock extends AbstractSensitivityAnalysis {

    private static final Log log = LogFactory.getLog(SimulationServiceTest.class);

    public String getDescription() {
        return "Implementation of random sensibility analysis";
    }

    /**
     * Retourne vrai si le calculateur sait gerer la cardinalité des facteurs
     * continue.
     * 
     * @return {@code true} s'il sait la gerer
     */
    public boolean canManageCardinality() {
        return false;
    }

    /**
     * Envoi un plan a faire analyser par l'outils d'analyse de sensibilité.
     * 
     * Retourne un {@link SensitivityScenarios} qui représente l'ensemble des
     * scenarios à prendre en compte pour les simulations.
     * 
     * @param plan plan a analyser
     * @param outputdirectory master sensitivity export directory
     * 
     * @return un {@link SensitivityScenarios}
     * @throws SensitivityException if calculator impl fail to execute
     * 
     * @see DesignPlan
     * @see Scenario
     * @see SensitivityScenarios
     */
    public SensitivityScenarios compute(DesignPlan plan, File outputdirectory)
            throws SensitivityException {

        if (log.isDebugEnabled()) {
            log.info("Call random mock compute()");
        }

        // return result
        SensitivityScenarios sensitivityScenarios = new SensitivityScenarios();

        List<Factor> factors = plan.getFactors();
        for (int i = 0; i < factors.size() * 2; ++i) {

            Scenario scenario = new Scenario();
            // choose a 0 < number <  factors.size()
            int pickedFactor = (int) (Math.random() * factors.size());
            Factor factor = factors.get(pickedFactor);

            Domain domain = factor.getDomain();
            if (domain instanceof ContinuousDomain) {
                ContinuousDomain cDomain = (ContinuousDomain) domain;
                Object minValue = 10.5; //cDomain.getMinBound();
                Object maxValue = 15.5; //cDomain.getMaxBound();

                factor.setValueForIdentifier(minValue);
                scenario.addFactor(factor);
                factor.setValueForIdentifier(maxValue);
                scenario.addFactor(factor);
            } else {
                DiscreteDomain dDomain = (DiscreteDomain) domain;
                for (Object sValue : dDomain.getValues().keySet()) {
                    factor.setValueForIdentifier(sValue);
                    scenario.addFactor(factor);
                }
            }

            sensitivityScenarios.getScenarios().add(scenario);
        }

        return sensitivityScenarios;

    }

    /**
     * Permet de renvoyer les resultats de simulations à l'outils de d'analyse
     * de sensibilité.
     * 
     * @param simulationStorages
     *            ensemble des {@link SimulationStorage} qui ont résultés des
     *            simulations
     * @param outputdirectory
     *            master sensitivity export directory
     * @throws SensitivityException
     *             if calculator impl fail to execute
     * 
     * @see SensitivityScenarios
     */
    public void analyzeResult(List<SimulationStorage> simulationStorages,
            File outputdirectory) throws SensitivityException {

    }

}
