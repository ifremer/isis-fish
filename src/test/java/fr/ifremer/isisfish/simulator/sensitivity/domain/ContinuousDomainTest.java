/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.simulator.sensitivity.domain;

import fr.ifremer.isisfish.simulator.sensitivity.Distribution;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ContinuousDomainTest {

    /**
     * Test jdistlib pour verifier que les parametres sont bien compris en compte.
     * 
     * Ici : valeurs positives non gérées correctement.
     */
    @Test
    public void testJDistLib1() {
        
        ContinuousDomain d = new ContinuousDomain(Distribution.QUNIFPC);
        d.getDistributionParameters().put("reference", 0.83);
        d.getDistributionParameters().put("coefficient", 0.5);
        
        Assertions.assertEquals(0.581, (Double)d.getValueForIdentifier(0.2), 0.001);
        Assertions.assertEquals(1.162, (Double)d.getValueForIdentifier(0.9), 0.001);
    }
    
    /**
     * Test jdistlib pour verifier que les parametres sont bien compris en compte.
     * 
     * Ici : valeurs négatives non gérées correctement (min &gt; max).
     */
    @Test
    public void testJDistLib2() {
        
        ContinuousDomain d = new ContinuousDomain(Distribution.QUNIFPC);
        d.getDistributionParameters().put("reference", -0.83);
        d.getDistributionParameters().put("coefficient", 0.5);
        
        Assertions.assertEquals(-1.079, (Double)d.getValueForIdentifier(0.2), 0.001);
        Assertions.assertEquals(-0.498, (Double)d.getValueForIdentifier(0.9), 0.001);
    }
}
