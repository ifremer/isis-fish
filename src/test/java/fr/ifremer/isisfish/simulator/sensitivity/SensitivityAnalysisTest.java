/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2013 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.simulator.sensitivity;

import fr.ifremer.isisfish.AbstractIsisFishTest;
import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.IsisFishException;
import fr.ifremer.isisfish.datastore.SensitivityAnalysisStorage;
import fr.ifremer.isisfish.simulator.sensitivity.domain.ContinuousDomain;
import fr.ifremer.isisfish.simulator.sensitivity.domain.DiscreteDomain;
import fr.ifremer.isisfish.util.RUtil;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.util.FileUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.nuiton.j2r.RException;
import org.nuiton.j2r.RProxy;

import java.io.File;
import java.io.IOException;

/**
 * Test les scripts d'AS disponible dans les ressources de tests.
 */
public class SensitivityAnalysisTest extends AbstractIsisFishTest {

    private static final Log log = LogFactory.getLog(SensitivityAnalysisTest.class);

    /** Simulation dir used to save Rdata files. */
    protected File simulationsDir;

    /**
     * Ces tests utilisent directement le moteur R, s'il n'est pas disponible,
     * on va eviter de faire les tests.
     */
    @BeforeEach
    public void setUp() {

        System.setProperty(RUtil.R_TYPE_PROPERTY, "jni");
        try {
            new RProxy();
        } catch (RException e) {
            if (log.isDebugEnabled()) {
                log.debug("R not available. Skipping current test");
            }
            Assumptions.assumeTrue(false);
        }
        simulationsDir = FileUtil.getTempDir("simulations");
    }

    protected DesignPlan getDesignPlan(Factor... factors) {
        DesignPlan result = new DesignPlan();
        int factorIndex = 1;
        for (Factor factor : factors) {
            factor.setName("f" + factorIndex++);
            result.addFactor(factor);
        }
        return result;
    }

    protected FactorGroup getFactorGroup(boolean continuous, Factor... factors) {
        FactorGroup result = new FactorGroup("", continuous);
        for (Factor factor : factors) {
            result.addFactor(factor);
        }
        return result;
    }

    protected Factor getContinuousDoubleUniformMMFactor() {
        // factor 1, min/max on int
        Factor factor1 = new Factor("test unifmm");
        factor1.setCardinality(2);
        ContinuousDomain domain1 = new ContinuousDomain(Distribution.QUNIFMM);
        domain1.addDistributionParam("min", 0.0);
        domain1.addDistributionParam("max", 2.0);
        factor1.setDomain(domain1);
        factor1.setPath("org.nuiton.factor#1234567890#0.12242345354#name");
        return factor1;
    }

    protected Factor getContinuousDoubleUniformPcFactor() {
        // factor 1, min/max on int
        Factor factor2 = new Factor("test unifpc");
        factor2.setCardinality(2);
        ContinuousDomain domain2 = new ContinuousDomain(Distribution.QUNIFPC);
        domain2.addDistributionParam("reference", 40.0);
        domain2.addDistributionParam("coefficient", 0.05);
        factor2.setDomain(domain2);
        factor2.setPath("org.nuiton.factor#1234567890#0.12242345354#name");
        return factor2;
    }

    /*protected Factor getContinuousMatrixUniformPCFactor() {
        // matrix 1
        MatrixND matrix1 = MatrixFactory.getInstance().create("test1",
                new int[] { 3, 2 }, new String[] { "col1", "col2" });
        matrix1.setValue(new int[] { 0, 0 }, 1);
        matrix1.setValue(new int[] { 0, 1 }, -14);
        matrix1.setValue(new int[] { 1, 0 }, 21);
        matrix1.setValue(new int[] { 1, 1 }, 2);
        matrix1.setValue(new int[] { 2, 0 }, 3);
        matrix1.setValue(new int[] { 2, 1 }, -1);

        // factor
        Factor factor2 = new Factor("tes tmatrix unifpc");
        factor2.setCardinality(3);
        ContinuousDomain domain2 = new ContinuousDomain(Distribution.QUNIFPC);
        domain2.addDistributionParam("reference", matrix1);
        domain2.addDistributionParam("coefficient", 0.1);
        factor2.setDomain(domain2);
        factor2.setPath("org.nuiton.math.matrix.MatrixND#563456293453#2.456347646#dim");
        return factor2;
    }*/
    
    protected Factor getContinuousDoubleCauchyFactor() {
        // factor 1, min/max on int
        Factor factor2 = new Factor("test cauchy");
        factor2.setCardinality(2);
        ContinuousDomain domain2 = new ContinuousDomain(Distribution.DCAUCHY);
        domain2.addDistributionParam("location", 0.0);
        domain2.addDistributionParam("scale", 1.0);
        factor2.setDomain(domain2);
        factor2.setPath("org.nuiton.factor#1234567890#0.12242345354#name");
        return factor2;
    }

    protected Factor getContinuousDoubleChisqFactor() {
        // factor 1, min/max on int
        Factor factor1 = new Factor("test dchisq");
        factor1.setCardinality(2);
        ContinuousDomain domain1 = new ContinuousDomain(Distribution.DCHISQ);
        domain1.addDistributionParam("df", 1.0);
        factor1.setDomain(domain1);
        factor1.setPath("org.nuiton.factor#1234567890#0.12242345354#name");
        return factor1;
    }

    protected Factor getDiscreteIntFactor() {
        // factor 1, min/max on int
        Factor factor1 = new Factor("test int");
        DiscreteDomain domain1 = new DiscreteDomain();
        domain1.getValues().put("1", 42.0);
        domain1.getValues().put("2", 43.0);
        domain1.getValues().put("3", 44.0);
        factor1.setDomain(domain1);
        factor1.setPath("org.nuiton.factor#1234567890#0.12242345354#name");
        return factor1;
    }

    protected void displayScenarios(SensitivityScenarios scenarii) {
        if (log.isDebugEnabled()) {
            int scIndex = 1;
            for (Scenario scenario : scenarii.getScenarios()) {
                log.debug("Scenario " + scIndex++);
                for (Factor factor : scenario.getFactors()) {
                    log.debug("Factor " + factor.getName() + " = " + factor.getValue());
                }
            }
        }
    }

    /**
     * Test tmp files/dir.
     * 
     * @throws IOException 
     */
    @AfterEach
    public void clear() throws IOException {
        if (simulationsDir != null) {
            FileUtils.deleteDirectory(simulationsDir);
        }
    }

    /**
     * Test de génération des scenarios via la methode Morris.
     * 
     * @throws IsisFishException 
     * @throws SensitivityException 
     */
    @Test
    public void testMorris() throws IsisFishException, SensitivityException {
        SensitivityAnalysis script = SensitivityAnalysisStorage.getSensitivityAnalysis("Morris").getNewInstance();
        Assertions.assertNotNull(script, "Morris script not found in test data");

        // uniform min/max
        DesignPlan designPlan = getDesignPlan(getContinuousDoubleUniformMMFactor(), getContinuousDoubleUniformMMFactor());
        SensitivityScenarios scenarii = script.compute(designPlan, simulationsDir);
        Assertions.assertTrue(scenarii.getScenarios().size() >= 3); // random, can be 9, 12
        for (Scenario scenario : scenarii.getScenarios()) {
            for (Factor factor : scenario.getFactors()) {
                double value = (Double)factor.getValue();
                Assertions.assertTrue(value >= 0.0 && value <= 2.0);
            }
        }

        // uniform pc
        designPlan = getDesignPlan(getContinuousDoubleUniformPcFactor(), getContinuousDoubleUniformPcFactor());
        scenarii = script.compute(designPlan, simulationsDir);
        Assertions.assertTrue(scenarii.getScenarios().size() >= 3); // random, can be 9, 12
        for (Scenario scenario : scenarii.getScenarios()) {
            for (Factor factor : scenario.getFactors()) {
                double value = (Double)factor.getValue();
                Assertions.assertTrue(value >= 30.0 && value <= 50.0);
            }
        }

        /* matrix uniform
        designPlan = getDesignPlan(getContinuousMatrixUniformPCFactor(), getContinuousMatrixUniformPCFactor());
        scenarii = script.compute(designPlan, simulationsDir);
        Assertions.assertTrue(scenarii.getScenarios().size() >= 3); // random, can be 9, 12
        for (Scenario scenario : scenarii.getScenarios()) {
            for (Factor factor : scenario.getFactors()) {
                MatrixND value = (MatrixND)factor.getValue();
                Assertions.assertTrue(value.getValue(0, 0) >= 0.9 && value.getValue(0, 0) <= 1.1);
            }
        }*/

        // check fail is discrete
        designPlan = getDesignPlan(getDiscreteIntFactor(),getContinuousDoubleUniformMMFactor());
        try {
            script.compute(designPlan, simulationsDir);
            Assertions.fail("Exception should have been thrown at this stage");
        } catch (SensitivityException eee){
            //do nothing
        }

    }

    /**
     * Test de génération des scenarios via la methode DOptimal.
     * 
     * This method accept discretes factors.
     * 
     * @throws IsisFishException 
     * @throws SensitivityException 
     */
    @Test
    @Disabled
    public void testDOptimal() throws IsisFishException, SensitivityException {
        SensitivityAnalysis script = SensitivityAnalysisStorage.getSensitivityAnalysis("DOptimal").getNewInstance();
        Assertions.assertNotNull(script, "DOptimal script not found in test data");

        DesignPlan designPlan = getDesignPlan(getContinuousDoubleUniformMMFactor(), getContinuousDoubleUniformMMFactor());
        SensitivityScenarios scenarii = script.compute(designPlan, simulationsDir);
        for (Scenario scenario : scenarii.getScenarios()) {
            for (Factor factor : scenario.getFactors()) {
                double value = (Double)factor.getValue();
                Assertions.assertTrue(value >= 0.0 && value <= 2.0);
            }
        }
        
        // test discretes
        designPlan = getDesignPlan(getDiscreteIntFactor(), getDiscreteIntFactor());
        scenarii = script.compute(designPlan, simulationsDir);
        displayScenarios(scenarii);
    }
    
    /**
     * Test de génération des scenarios via la methode Fast.
     * 
     * @throws IsisFishException 
     * @throws SensitivityException 
     */
    @Test
    public void testFast() throws IsisFishException, SensitivityException {
        SensitivityAnalysis script = SensitivityAnalysisStorage.getSensitivityAnalysis("Fast").getNewInstance();
        Assertions.assertNotNull(script, "Fast script not found in test data");

        // uniform min/max
        DesignPlan designPlan = getDesignPlan(getContinuousDoubleUniformMMFactor(), getContinuousDoubleUniformMMFactor());
        SensitivityScenarios scenarii = script.compute(designPlan, simulationsDir);
        Assertions.assertEquals(40, scenarii.getScenarios().size());
        for (Scenario scenario : scenarii.getScenarios()) {
            for (Factor factor : scenario.getFactors()) {
                double value = (Double)factor.getValue();
                // TODO: fix NaN values here
                //Assertions.assertTrue(value >= 0.0 && value <= 2.0);
            }
        }

        // uniform pc
        designPlan = getDesignPlan(getContinuousDoubleUniformPcFactor(), getContinuousDoubleUniformPcFactor());
        scenarii = script.compute(designPlan, simulationsDir);
        Assertions.assertEquals(40, scenarii.getScenarios().size());
        displayScenarios(scenarii);

        // check fail is discrete
        designPlan = getDesignPlan(getDiscreteIntFactor(),getContinuousDoubleUniformMMFactor());
        try {
            script.compute(designPlan, simulationsDir);
            Assertions.fail("Exception should have been thrown at this stage");
        } catch (SensitivityException eee){
            //do nothing
        }
        
    }
    
    /**
     * Test de génération des scenarios via la methode OptimumLHS.
     * 
     * @throws IsisFishException 
     * @throws SensitivityException 
     */
    @Test
    public void testOptimumLHS() throws IsisFishException, SensitivityException {
        SensitivityAnalysis script = SensitivityAnalysisStorage.getSensitivityAnalysis("OptimumLHS").getNewInstance();
        Assertions.assertNotNull(script, "OptimumLHS script not found in test data");

        // uniform min/max
        DesignPlan designPlan = getDesignPlan(getContinuousDoubleUniformMMFactor(), getContinuousDoubleUniformMMFactor());
        SensitivityScenarios scenarii = script.compute(designPlan, simulationsDir);
        Assertions.assertEquals(10, scenarii.getScenarios().size());
        for (Scenario scenario : scenarii.getScenarios()) {
            for (Factor factor : scenario.getFactors()) {
                double value = (Double)factor.getValue();
                Assertions.assertTrue(value >= 0.0 && value <= 2.0);
            }
        }

        // uniform pc
        designPlan = getDesignPlan(getContinuousDoubleUniformPcFactor(), getContinuousDoubleUniformPcFactor());
        scenarii = script.compute(designPlan, simulationsDir);
        Assertions.assertEquals(10, scenarii.getScenarios().size());
        displayScenarios(scenarii);

        // check fail is discrete
        designPlan = getDesignPlan(getDiscreteIntFactor(),getContinuousDoubleUniformMMFactor());
        try {
            script.compute(designPlan, simulationsDir);
            Assertions.fail("Exception should have been thrown at this stage");
        } catch (SensitivityException eee){
            //do nothing
        }
        
    }
    
    /**
     * Test de génération des scenarios via la methode RandomLHS.
     * 
     * @throws IsisFishException 
     * @throws SensitivityException 
     */
    @Test
    public void testRandomLHS() throws IsisFishException, SensitivityException {
        SensitivityAnalysis script = SensitivityAnalysisStorage.getSensitivityAnalysis("RandomLHS").getNewInstance();
        Assertions.assertNotNull(script, "RandomLHS script not found in test data");

        // uniform min/max
        DesignPlan designPlan = getDesignPlan(getContinuousDoubleUniformMMFactor(), getContinuousDoubleUniformMMFactor());
        SensitivityScenarios scenarii = script.compute(designPlan, simulationsDir);
        Assertions.assertEquals(10, scenarii.getScenarios().size());
        for (Scenario scenario : scenarii.getScenarios()) {
            for (Factor factor : scenario.getFactors()) {
                double value = (Double)factor.getValue();
                Assertions.assertTrue(value >= 0.0 && value <= 2.0);
            }
        }

        // uniform pc
        designPlan = getDesignPlan(getContinuousDoubleUniformPcFactor(), getContinuousDoubleUniformPcFactor());
        scenarii = script.compute(designPlan, simulationsDir);
        Assertions.assertEquals(10, scenarii.getScenarios().size());
        displayScenarios(scenarii);

        // check fail is discrete
        designPlan = getDesignPlan(getDiscreteIntFactor(),getContinuousDoubleUniformMMFactor());
        try {
            script.compute(designPlan, simulationsDir);
            Assertions.fail("Exception should have been thrown at this stage");
        } catch (SensitivityException eee){
            //do nothing
        }
        
    }
    
    /**
     * Test de génération des scenarios via la methode RegularExpandGrid.
     * 
     * This method accept discretes factors.
     * 
     * @throws IsisFishException 
     * @throws SensitivityException 
     */
    @Test
    @Disabled
    public void testRegularExpandGrid() throws IsisFishException, SensitivityException {
        SensitivityAnalysis script = SensitivityAnalysisStorage.getSensitivityAnalysis("RegularExpandGrid").getNewInstance();
        Assertions.assertNotNull(script, "RegularExpandGrid script not found in test data");

        DesignPlan designPlan = getDesignPlan(getContinuousDoubleUniformMMFactor(), getContinuousDoubleUniformMMFactor());
        SensitivityScenarios scenarii = script.compute(designPlan, simulationsDir);
        for (Scenario scenario : scenarii.getScenarios()) {
            for (Factor factor : scenario.getFactors()) {
                double value = (Double)factor.getValue();
                Assertions.assertTrue(value >= 0.0 && value <= 2.0);
            }
        }
        
        // test discretes
        designPlan = getDesignPlan(getDiscreteIntFactor(), getDiscreteIntFactor());
        scenarii = script.compute(designPlan, simulationsDir);
        displayScenarios(scenarii);
    }
    
    /**
     * Test de génération des scenarios via la methode RegularFractions.
     * 
     * @throws IsisFishException 
     * @throws SensitivityException 
     */
    @Test
    public void testRegularFractions() throws IsisFishException, SensitivityException {
        SensitivityAnalysis script = SensitivityAnalysisStorage.getSensitivityAnalysis("RegularFractions").getNewInstance();
        // this script need pathToFunction parameter to find R script in current directory
        SensitivityAnalysisStorage.setParameterValue(script,"pathToFunction",
                IsisFish.config.getDatabaseDirectory().getAbsolutePath() + File.separator + "sensitivityanalysis");
        Assertions.assertNotNull(script, "RegularFractions script not found in test data");

        // test continuous
        DesignPlan designPlan = getDesignPlan(getContinuousDoubleUniformMMFactor(), getContinuousDoubleUniformMMFactor());
        SensitivityScenarios scenarii = script.compute(designPlan, simulationsDir);
        displayScenarios(scenarii);

    }
    
    /**
     * Test de génération des scenarios via la methode Sobol.
     * 
     * @throws IsisFishException 
     * @throws SensitivityException 
     */
    @Test
    public void testSobol() throws IsisFishException, SensitivityException {
        SensitivityAnalysis script = SensitivityAnalysisStorage.getSensitivityAnalysis("Sobol").getNewInstance();
        Assertions.assertNotNull(script, "Sobol script not found in test data");

        // sobol avec uniform MM
        DesignPlan designPlan = getDesignPlan(getContinuousDoubleUniformMMFactor(), getContinuousDoubleUniformMMFactor());
        SensitivityScenarios scenarii = script.compute(designPlan, simulationsDir);
        Assertions.assertEquals(80, scenarii.getScenarios().size());
        for (Scenario scenario : scenarii.getScenarios()) {
            for (Factor factor : scenario.getFactors()) {
                double value = (Double)factor.getValue();
                Assertions.assertTrue(value >= 0.0 && value <= 2.0);
            }
        }

        // sobol avec cauchy/chisq
        designPlan = getDesignPlan(getContinuousDoubleCauchyFactor(), getContinuousDoubleChisqFactor());
        scenarii = script.compute(designPlan, simulationsDir);
        Assertions.assertEquals(80, scenarii.getScenarios().size());
        for (Scenario scenario : scenarii.getScenarios()) {
            for (Factor factor : scenario.getFactors()) {
                double value = (Double)factor.getValue();
                Assertions.assertTrue(value != 0.0);
            }
        }

        // check fail is discrete
        designPlan = getDesignPlan(getDiscreteIntFactor(),getContinuousDoubleUniformMMFactor());
        try {
            script.compute(designPlan, simulationsDir);
            Assertions.fail("Exception should have been thrown at this stage");
        } catch (SensitivityException eee){
            //do nothing
        }

    }
}
