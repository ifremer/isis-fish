/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2012 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator.sensitivity;

import fr.ifremer.isisfish.IsisFishDAOHelper;
import fr.ifremer.isisfish.datastore.IsisH2Config;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaContextFactory;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.TopiaNotFoundException;
import org.nuiton.topia.persistence.TopiaDAO;
import org.nuiton.topia.persistence.TopiaEntity;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Properties;

/**
 * Test for SensitivityUtils class.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class SensitivityUtilsTest {

    /** Logger for this class */
    private static final Log log = LogFactory.getLog(SensitivityUtilsTest.class);

    /**
     * Test le résultat attendu de l'echappement des noms de facteurs.
     */
    @Test
    public void testEspaceFactorName() {
        Assertions.assertEquals("test_factor_pour_R", SensitivityUtils.espaceFactorName("test factor pour R"));
        Assertions.assertEquals("factor2_selectivite", SensitivityUtils.espaceFactorName("factor2.sélectivité"));
    }
    
    /**
     * Return a basic valid topia context.
     * 
     * @throws TopiaNotFoundException 
     */
    protected TopiaContext getTopiaContext() throws TopiaNotFoundException {
        Properties config = new Properties();
        IsisH2Config.addMemDatabaseConfig(config, "test");
        IsisH2Config.addHibernateMapping(config);
        TopiaContext context = TopiaContextFactory.getContext(config);
        return context;
    }

    /**
     * Test (par introspection) que tous les facteurs existent.
     * 
     * @throws ClassNotFoundException 
     * @throws IllegalAccessException 
     * @throws InstantiationException 
     * @throws NoSuchMethodException 
     * @throws SecurityException 
     * @throws InvocationTargetException 
     * @throws IllegalArgumentException 
     * @throws TopiaException 
     */
    @Test
    public void testFactorExistence() throws IllegalAccessException, SecurityException, NoSuchMethodException, IllegalArgumentException,
        InvocationTargetException, TopiaException {
        Properties factors = SensitivityUtils.getProperties();

        TopiaContext testTC = getTopiaContext();
        TopiaContext context = testTC.beginTransaction();
        for (String factorName : factors.stringPropertyNames()) {
            String className = factorName.substring(0, factorName.indexOf("."));
            String propertyName = StringUtils.capitalize(factorName.substring(factorName.indexOf(".") + 1));

            // Harder method
            // but topia context is needed by some getXXX() methods
            Method mStatic = IsisFishDAOHelper.class.getMethod("get" + className + "DAO", TopiaContext.class);
            TopiaDAO<TopiaEntity> dao = (TopiaDAO<TopiaEntity>) mStatic.invoke(null, new Object[] { context });
            Assertions.assertNotNull(dao, "No DOA found for factor " + factorName);

            // call proper property
            if (log.isDebugEnabled()) {
                log.debug(" and call get" + propertyName + "() on " + className);
            }
            Method m = dao.getEntityClass().getMethod("get" + propertyName);
            Assertions.assertNotNull(m, "getter not found for " + factorName);
        }
        context.closeContext();
    }
}
