/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2011 Ifremer,  CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator.sensitivity.group;

import fr.ifremer.isisfish.simulator.sensitivity.Domain;
import fr.ifremer.isisfish.simulator.sensitivity.Factor;
import fr.ifremer.isisfish.simulator.sensitivity.FactorGroup;
import fr.ifremer.isisfish.simulator.sensitivity.domain.DiscreteDomain;
import org.junit.jupiter.api.Test;

/**
 * Test for FactorGroup.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class FactorGroupTest {
    
    /**
     * Test to add a factor into group.
     */
    @Test
    public void testFactorGroupAdd() {
        Factor f1 = new Factor("test");
        Domain d1 = new DiscreteDomain();
        f1.setDomain(d1);
        Factor f2 = new Factor("test 2");
        Domain d2 = new DiscreteDomain();
        f2.setDomain(d2);
        FactorGroup g = new FactorGroup("test group");
        g.addFactor(f1);
        g.addFactor(f2);
    }
    
}
