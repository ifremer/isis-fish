/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2011 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator;

import fr.ifremer.isisfish.AbstractIsisFishTest;
import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.entities.PopulationImpl;
import fr.ifremer.isisfish.util.converter.ConverterUtil;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.ConvertUtilsBean;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.nuiton.math.matrix.MatrixFactory;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.math.matrix.MatrixNDImpl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.List;

/**
 * Test for simulation prescript.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class SimulationPreScriptTest extends AbstractIsisFishTest {

    /**
     * Test que les chaine represantant des matrices sont correctement
     * transformée en MatrixND.
     * 
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     */
    @Test
    public void testMatrixFactorBeanUtils() throws IllegalAccessException, InvocationTargetException {
        String factorAsString = "[isisfish.population.capturability,[10, 3],[\"isisfish.population.group\", \"isisfish.population.season\"],[[org.nuiton.topia.persistence.TopiaEntity(fr.ifremer.isisfish.entities.PopulationGroup#1223471369174#0.28237914052272917), org.nuiton.topia.persistence.TopiaEntity(fr.ifremer.isisfish.entities.PopulationGroup#1223471369077#0.6322222161721446), org.nuiton.topia.persistence.TopiaEntity(fr.ifremer.isisfish.entities.PopulationGroup#1223471369117#0.9144815378932778), org.nuiton.topia.persistence.TopiaEntity(fr.ifremer.isisfish.entities.PopulationGroup#1223471369145#0.40303037362612626), org.nuiton.topia.persistence.TopiaEntity(fr.ifremer.isisfish.entities.PopulationGroup#1223471369157#0.7385235788350787), org.nuiton.topia.persistence.TopiaEntity(fr.ifremer.isisfish.entities.PopulationGroup#1223471369129#0.7322508787399089), org.nuiton.topia.persistence.TopiaEntity(fr.ifremer.isisfish.entities.PopulationGroup#1223471369123#0.9794507456491978), org.nuiton.topia.persistence.TopiaEntity(fr.ifremer.isisfish.entities.PopulationGroup#1223471369109#0.6142040515900707), org.nuiton.topia.persistence.TopiaEntity(fr.ifremer.isisfish.entities.PopulationGroup#1223471369163#0.4236915489033358), org.nuiton.topia.persistence.TopiaEntity(fr.ifremer.isisfish.entities.PopulationGroup#1223471369151#0.9128085136422524)], [org.nuiton.topia.persistence.TopiaEntity(fr.ifremer.isisfish.entities.PopulationSeasonInfo#1223471369187#0.14365248282751242), java.lang.String(org.nuiton.topia.persistence.TopiaEntity(fr.ifremer.isisfish.entities.PopulationSeasonInfo#1223471369341#0.8526969683760768)), java.lang.String(org.nuiton.topia.persistence.TopiaEntity(fr.ifremer.isisfish.entities.PopulationSeasonInfo#1223471369569#0.18989201268161482))]],[[0.0, 0.0, 0.0], [2.2001803893501406E-4, 1.7501434915285209E-4, 1.0300844550139294E-4], [6.600541168050422E-4, 0.0023401918686724223, 0.0011700959343362112], [4.040331260442985E-4, 0.002400196788381971, 0.0014301172530775913], [2.2401836691565065E-4, 0.002060168910027859, 7.160587085339548E-4], [9.530781413866745E-5, 0.0017101402117221545, 4.380359138797098E-4], [6.710550187517929E-5, 0.0011800967542878026, 2.830232046300408E-4], [3.530289442911816E-5, 7.840642842047773E-4, 1.6901385718189713E-4], [2.030166450173084E-5, 6.250512469744717E-4, 1.1600951143846195E-4], [1.0200836350623379E-5, 4.0103288005882107E-4, 8.360685479530534E-5]]]";
        Population p = new PopulationImpl();
        p.setCapturability(MatrixFactory.getInstance().create("ee" , new List[]{ Collections.singletonList("eee")}));
        
        ConvertUtilsBean beanUtils = ConverterUtil.getConverter(null);
        Object value = beanUtils.convert(factorAsString, MatrixNDImpl.class);
        BeanUtils.setProperty(p, "capturability", value);
        
        Assertions.assertNotNull(value);
        Assertions.assertTrue(value instanceof MatrixND);
    }
}
