/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2010 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator;

import fr.ifremer.isisfish.AbstractIsisFishTest;
import fr.ifremer.isisfish.config.IsisConfig;
import fr.ifremer.isisfish.types.Month;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.nuiton.util.SortedProperties;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.Properties;

/**
 * Simulation parameter test class.
 *
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class SimulationParameterTest extends AbstractIsisFishTest {

    /** Logger for this class. */
    private static Log log = LogFactory.getLog(SimulationParameterTest.class);
    
    /**
     * Test les valeurs par defaut des parametres de simulations.
     */
    @Test
    public void testDefaultProperties() {
        SimulationParameter params = new SimulationParameterImpl();
        
        Assertions.assertEquals(IsisConfig.getVersion(), params.getIsisFishVersion());
        Assertions.assertEquals("", params.getDescription());
        Assertions.assertEquals("", params.getRegionName());
        Assertions.assertEquals(Month.NUMBER_OF_MONTH, params.getNumberOfMonths());
        Assertions.assertEquals(1, params.getNumberOfYear());
        Assertions.assertEquals("DefaultSimulator.java", params.getSimulatorName());
        Assertions.assertTrue(params.getUseCache());
        Assertions.assertFalse(params.getUseStatistic());
        Assertions.assertFalse(params.getUseOptimization());
        Assertions.assertTrue(params.getStrategies().isEmpty());
        Assertions.assertTrue(params.getPopulations().isEmpty());
        // Not easy to test it
        //Assertions.assertNotNull(params.getNumberOf(new PopulationImpl()));
        Assertions.assertTrue(params.getRules().isEmpty());
        Assertions.assertTrue(params.getExtraRules().isEmpty());
        Assertions.assertTrue(params.getSimulationPlans().isEmpty());
        Assertions.assertTrue(params.getExportNames().isEmpty());
        Assertions.assertFalse(params.getUsePreScript());
        Assertions.assertEquals("", params.getPreScript());
        Assertions.assertFalse(params.getUseSimulationPlan());
        Assertions.assertEquals(-1, params.getSimulationPlanNumber());
        Assertions.assertTrue(params.getSensitivityExport().isEmpty());
        Assertions.assertEquals(-1, params.getNumberOfSensitivitySimulation());
        Assertions.assertNull(params.getSensitivityAnalysis());
        Assertions.assertTrue(params.getResultEnabled().isEmpty());
        Assertions.assertTrue(params.getTagValue().isEmpty());
        Assertions.assertEquals("info", params.getSimulLogLevel());
        Assertions.assertEquals("info", params.getScriptLogLevel());
        Assertions.assertEquals("error", params.getLibLogLevel());
    }

    /**
     * Test to build one simulation parameters.
     * 
     * Write it on disk, read it from disk.
     * And test it.s
     */
    @Test
    public void testToPropertiesFromProperties() {

        SimulationParameter params = new SimulationParameterImpl();
        params.setIsisFishVersion("3.2.0.4");
        params.setDescription("desc");
        params.setResultEnabled(Collections.singleton("bakh"));

        // make transform
        Properties props = params.toProperties();
        SimulationParameter params2 = new SimulationParameterImpl();
        params2.fromProperties(props);

        Assertions.assertEquals("3.2.0.4", params2.getIsisFishVersion());
        Assertions.assertEquals("desc", params2.getDescription());
        Assertions.assertEquals(1, params2.getResultEnabled().size());
    }

    /**
     * Test de lecture d'un fichier existant.
     * 
     * Test que les valeurs string sont correcte.
     * Et que les instances regles/export... sont correctes.
     * 
     * Cas 1 : file basic
     * @throws IOException 
     */
    @Test
    public void testBasicFilePropertiesLoading() throws IOException {

        InputStream basicFileStream = SimulationParameterTest.class.getResourceAsStream("parameters_basic.properties");

        Properties props = new SortedProperties();
        props.load(basicFileStream);
        SimulationParameterImpl param = new SimulationParameterImpl();
        param.fromProperties(props);
        
        Assertions.assertEquals("3.2.0.6", param.getIsisFishVersion());
        
        // test des regles
        Assertions.assertNull(param.rules);
        Assertions.assertEquals(2, param.getRules().size());
        Assertions.assertNotNull(param.rules);
        
        // test des exports
        Assertions.assertNull(param.exportNames);
        
        // test des export de sensibilité
        Assertions.assertNull(param.sensitivityExports);
        Assertions.assertEquals(0, param.getSensitivityExport().size());
        Assertions.assertNotNull(param.sensitivityExports);
    }
    
    /**
     * Test une copies de simulation parameters sans instancier
     * les regles, export, plans... pour verifier qu'il ne perde pas
     * les informations des paramètres sur lesquels getxxx() n'a pas été
     * appelé.
     * @throws IOException
     */
    @Test
    public void testBasicFilePropertiesCopyWithoutInstancation() throws IOException {
        InputStream basicFileStream = SimulationParameterTest.class.getResourceAsStream("parameters_basic.properties");

        Properties props = new SortedProperties();
        props.load(basicFileStream);
        SimulationParameterImpl param = new SimulationParameterImpl();
        param.fromProperties(props);
        
        Properties props2 = param.toProperties();
        SimulationParameter param2 = new SimulationParameterImpl();
        param2.fromProperties(props2);
        
        // test rules
        Assertions.assertNull(param.rules);
        Assertions.assertEquals(2, param2.getRules().size());
        
        // test analyse plans
        Assertions.assertNull(param.simulationPlans);
        Assertions.assertEquals(0, param2.getSimulationPlans().size());
        
        // test sur les populations
        Assertions.assertNull(param.populations);
        Assertions.assertEquals(1, param2.getPopulations().size());
    }
    
    /**
     * Test de copie, avec une classe manquante dans le config.
     * L'intanciation de la classe ne doit pas etre appelée.
     * Et la configuration de la classe doit être copiée.
     * 
     * @throws IOException
     */
    @Test
    public void testPlanFileWithMissingClasses() throws IOException {
        InputStream basicFileStream = SimulationParameterTest.class.getResourceAsStream("parameters_plan.properties");

        Properties props = new SortedProperties();
        props.load(basicFileStream);
        SimulationParameterImpl param = new SimulationParameterImpl();
        param.fromProperties(props);
        
        Properties props2 = param.toProperties();
        SimulationParameterImpl param2 = new SimulationParameterImpl();
        param2.fromProperties(props2);

        // test simulation plans
        Assertions.assertNull(param.simulationPlans);
        Assertions.assertTrue(param2.propertiesParameters.containsKey("plans"));
        // ca rend zero parce que l'instanciation doit echouer
        Assertions.assertEquals(0, param2.getSimulationPlans().size());
    }

    /**
     * Test toString() method.
     * 
     * @throws IOException 
     */
    @Test
    public void testSimulationParametersCopy() throws IOException {
        InputStream basicFileStream = SimulationParameterTest.class.getResourceAsStream("parameters_basic.properties");

        Properties props = new SortedProperties();
        props.load(basicFileStream);
        SimulationParameter param = new SimulationParameterImpl();
        param.fromProperties(props);
        
        // force some properties instantiation
        param.getSimulationPlans();
        param.getStrategies();
        // not populations
        
        SimulationParameter param2 = param.copy();

        Assertions.assertEquals("3.2.0.6", param2.getIsisFishVersion());
        Assertions.assertEquals("Done for unit testing", param2.getDescription());
        // test some collections
        Assertions.assertEquals(0, param2.getSimulationPlans().size());
        Assertions.assertEquals(1, param2.getPopulations().size());
        Assertions.assertEquals(3, param2.getStrategies().size());
    }
    
    /**
     * Test toString() method.
     * 
     * @throws IOException 
     */
    @Test
    public void testSimulationParametersToString() throws IOException {
        InputStream basicFileStream = SimulationParameterTest.class.getResourceAsStream("parameters_basic.properties");

        Properties props = new SortedProperties();
        props.load(basicFileStream);
        
        SimulationParameter param = new SimulationParameterImpl();
        param.fromProperties(props);
        
        String toString = param.toString();
        if (log.isInfoEnabled()) {
            log.info("toString() result is : " + toString);
        }
    }
    
    /**
     * Test copy method without setting internals properties.
     */
    @Test
    public void testCopyWithoutPropertiesLoad() {
        SimulationParameter params = new SimulationParameterImpl();
        params.setIsisFishVersion("3.2.0.4");
        params.setDescription("desc");
        params.setResultEnabled(Collections.singleton("bakh"));

        SimulationParameter params2 = params.copy();

        Assertions.assertEquals("3.2.0.4", params2.getIsisFishVersion());
        Assertions.assertEquals("desc", params2.getDescription());
        Assertions.assertEquals(1, params2.getResultEnabled().size());
    }

    /**
     * Test que le log level est bien correctement recopié.
     */
    @Test
    public void testLogLevelCopy() {
        SimulationParameter params = new SimulationParameterImpl();
        params.setSimulLogLevel("error");
        SimulationParameter newInstance = params.deepCopy();
        Assertions.assertEquals("error", newInstance.getSimulLogLevel());
    }
}
