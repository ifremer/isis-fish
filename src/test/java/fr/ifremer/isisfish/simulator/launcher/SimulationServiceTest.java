/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator.launcher;

import fr.ifremer.isisfish.AbstractIsisFishTest;
import fr.ifremer.isisfish.IsisFishException;
import fr.ifremer.isisfish.datastore.IsisH2Config;
import fr.ifremer.isisfish.datastore.RegionStorage;
import fr.ifremer.isisfish.datastore.ResultInfoStorage;
import fr.ifremer.isisfish.datastore.RuleStorage;
import fr.ifremer.isisfish.datastore.ScriptStorage;
import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.datastore.SimulatorStorage;
import fr.ifremer.isisfish.datastore.StorageException;
import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.entities.PopulationImpl;
import fr.ifremer.isisfish.entities.Strategy;
import fr.ifremer.isisfish.entities.StrategyImpl;
import fr.ifremer.isisfish.rule.Rule;
import fr.ifremer.isisfish.rule.RuleHelper;
import fr.ifremer.isisfish.simulator.SimulationParameter;
import fr.ifremer.isisfish.simulator.SimulationParameterImpl;
import fr.ifremer.isisfish.simulator.sensitivity.DesignPlan;
import fr.ifremer.isisfish.simulator.sensitivity.Distribution;
import fr.ifremer.isisfish.simulator.sensitivity.Factor;
import fr.ifremer.isisfish.simulator.sensitivity.FactorGroup;
import fr.ifremer.isisfish.simulator.sensitivity.FactorHelper;
import fr.ifremer.isisfish.simulator.sensitivity.Scenario;
import fr.ifremer.isisfish.simulator.sensitivity.SensitivityAnalysis;
import fr.ifremer.isisfish.simulator.sensitivity.SensitivityAnalysisRandomMock;
import fr.ifremer.isisfish.simulator.sensitivity.domain.ContinuousDomain;
import fr.ifremer.isisfish.simulator.sensitivity.domain.DiscreteDomain;
import fr.ifremer.isisfish.simulator.sensitivity.domain.EquationDiscreteDomain;
import fr.ifremer.isisfish.simulator.sensitivity.domain.RuleDiscreteDomain;
import fr.ifremer.isisfish.types.Month;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.nuiton.math.matrix.MatrixFactory;
import org.nuiton.math.matrix.MatrixHelper;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaContextFactory;
import org.nuiton.topia.TopiaException;
import org.nuiton.util.FileUtil;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

/**
 * Test for {@link SimulationService}.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$ By : $Author$
 */
public class SimulationServiceTest extends AbstractIsisFishTest {

    /** Commons logging log. */
    private static Log log = LogFactory.getLog(SimulationServiceTest.class);

    /**
     * Lance une simulation simple avec le simulateur par cellule.
     * 
     * Test is wrong if it take more than 2 min.
     * 
     * @throws InterruptedException 
     */
    @Test
    @Timeout(value = 2, unit = TimeUnit.MINUTES)
    public void testSimulationEffortByCell() throws InterruptedException {

        SimulationParameter params = new SimulationParameterImpl();
        // set params region
        params.setRegionName("DemoRegion");
        // simulator
        params.setSimulatorName("SimulatorEffortByCell.java");
        // set population
        List<Population> pops = new ArrayList<>();
        Population pop = new PopulationImpl();
        pop.setName("test population");
        pop.setPopulationGroup(new ArrayList<>());
        pop.setPopulationZone(new ArrayList<>());
        pops.add(pop);
        params.setPopulations(pops);
        // strat
        List<Strategy> strats = new ArrayList<>();
        Strategy strategy = new StrategyImpl();
        strategy.setName("stratest");
        strats.add(strategy);
        params.setStrategies(strats);
        // number of year
        params.setNumberOfYear(1);
        // add first result only (cicular deps only)
        params.setResultEnabled(Collections.singletonList("MatrixTestDep1"));

        // run simulation
        SimulationService service = SimulationService.getService();
        SimulatorLauncher launcher = new InProcessSimulatorLauncher();
        service.addSimulationLauncher(launcher);
        String simulationId = "effort by cell test number 1";
        service.submit("effort by cell test number 1", params, launcher, 0);

        // temporise la fin de test , sinon junit tue tous les process
        do {
            Thread.sleep(2000);
        } while (!service.jobs.isEmpty());

        // suite au changement de la compilation, on vérifie que certains fichier java
        // doivent être automatiquement inclut dans les sources
        File simDir = SimulationStorage.getSimulationDirectory(simulationId);
        File simulatorDir = new File(simDir, SimulatorStorage.SIMULATOR_PATH);
        File scriptDir = new File(simDir, ScriptStorage.SCRIPT_PATH);
        File resultInfoDir = new File(simDir, ResultInfoStorage.RESULT_INFO_PATH);
        Assertions.assertTrue(new File(simulatorDir, "DefaultSimulator.java").exists());
        Assertions.assertTrue(new File(simulatorDir, "SimulatorEffortByCell.java").exists());
        Assertions.assertTrue(new File(resultInfoDir, "MatrixAbundance.java").exists());
        Assertions.assertTrue(new File(scriptDir, "SiMatrix.java").exists());
        Assertions.assertTrue(new File(scriptDir, "GravityModel.java").exists());
        Assertions.assertFalse(new File(scriptDir, "RuleUtil.java").exists());
    }

    /**
     * Build a test {@link DesignPlan}.
     * 
     * @return a test {@link DesignPlan}
     */
    protected DesignPlan getTestDesignPlan() {
        DesignPlan designPlan = new DesignPlan();

        // factor 1
        Factor factor1 = new Factor("factor 1 (double)");
        ContinuousDomain domain1 = new ContinuousDomain(Distribution.QUNIFMM);
        domain1.addDistributionParam("min", 0.0);
        domain1.addDistributionParam("max", 50.0);
        factor1.setCardinality(4);
        factor1.setDomain(domain1);
        factor1.setPath("fr.ifremer.isisfish.entities.PopulationGroup#1156461521013#0.1715620681984218#maxLength");

        // factor 2
        Factor factor2 = new Factor("factor 2 (double)");
        DiscreteDomain domain2 = new DiscreteDomain();
        domain2.getValues().put(0.0, 12.3);
        domain2.getValues().put(1.0, 70.9);
        domain2.getValues().put(2.0, 21.0);
        domain2.getValues().put(3.0, -12.1);
        domain2.getValues().put(4.0, -8.45);
        factor2.setDomain(domain2);
        factor2.setPath("fr.ifremer.isisfish.entities.PopulationGroup#1156461521064#0.022976136053553198#minLength");

        // factor 3
        Factor factor3 = new Factor("factor 3 (double)");
        ContinuousDomain domain3 = new ContinuousDomain(Distribution.QUNIFMM);
        domain3.addDistributionParam("min", 12.0);
        domain3.addDistributionParam("max", 99.0);
        factor3.setCardinality(4);
        factor3.setDomain(domain3);
        factor3.setPath("fr.ifremer.isisfish.entities.PopulationGroup#1156461521076#0.6526656643346673#minLength");

        designPlan.addFactor(factor1);
        //designPlan.add(factor2);
        designPlan.addFactor(factor3);

        return designPlan;
    }

    /**
     * Build a test {@link DesignPlan} filled with matrix.
     * 
     * @return a test {@link DesignPlan}
     */
    protected DesignPlan getTestMatrixDesignPlan() {
        DesignPlan designPlan = new DesignPlan();

        // matrix 1
        MatrixND matrix1 = MatrixFactory.getInstance().create("test1",
                new int[] { 3, 2 }, new String[] { "col1", "col2" });
        matrix1.setValue(new int[] { 0, 0 }, 13);
        matrix1.setValue(new int[] { 0, 1 }, -14);
        matrix1.setValue(new int[] { 1, 0 }, 21);
        matrix1.setValue(new int[] { 1, 1 }, 2);
        matrix1.setValue(new int[] { 2, 0 }, 12);
        matrix1.setValue(new int[] { 2, 1 }, -1);

        // matrix 2
        MatrixND matrix2 = MatrixFactory.getInstance().create("test1",
                new int[] { 3, 2 }, new String[] { "col1", "col2" });
        matrix2.setValue(new int[] { 0, 0 }, 13);
        matrix2.setValue(new int[] { 0, 1 }, -14);
        matrix2.setValue(new int[] { 1, 0 }, 21);
        matrix2.setValue(new int[] { 1, 1 }, 2);
        matrix2.setValue(new int[] { 2, 0 }, 12);
        matrix2.setValue(new int[] { 2, 1 }, -1);

        // factor 1
        Factor factor1 = new Factor("factor 1 (matrixND)");
        DiscreteDomain domain1 = new DiscreteDomain();
        domain1.getValues().put(0.0, matrix1);
        domain1.getValues().put(1.0, matrix2);
        factor1.setDomain(domain1);
        factor1.setPath("fr.ifremer.isisfish.entities.StrategyMonthInfo#1156808754768#0.7282750856395208#proportionMetier");

        designPlan.getFactors().add(factor1);

        return designPlan;
    }

    /**
     * Lance une simulation avec des facteurs de sensibilité.
     * 
     * Test is wrong if it take more than 5 min.
     * 
     * @throws InterruptedException
     *
     * Disabled because not working very well in threaded env
     */
    @Test
    @Timeout(value = 10, unit = TimeUnit.MINUTES)
    @Disabled
    public void testRunSensivitySimulation() throws InterruptedException {

        SimulationParameter params = new SimulationParameterImpl();
        // set params region
        params.setRegionName("DemoRegion");
        // set population
        List<Population> pops = new ArrayList<>();
        Population pop = new PopulationImpl();
        pop.setName("test population");
        pop.setPopulationGroup(new ArrayList<>());
        pop.setPopulationZone(new ArrayList<>());
        pops.add(pop);
        params.setPopulations(pops);
        // strat
        List<Strategy> strats = new ArrayList<>();
        Strategy strategy = new StrategyImpl();
        strategy.setName("stratest");
        strats.add(strategy);
        params.setStrategies(strats);
        // number of year
        params.setNumberOfMonths(Month.NUMBER_OF_MONTH);

        SimulationService service = SimulationService.getService();
        SimulatorLauncher launcher = new InProcessSimulatorLauncher();
        service.addSimulationLauncher(launcher);

        SensitivityAnalysis sensitivityAnalysis = new SensitivityAnalysisRandomMock();

        DesignPlan designPlan = getTestDesignPlan();

        service.submit("sensitivity test number 1", params, launcher, 0,
                sensitivityAnalysis, designPlan);

        // temporise la fin de test , sinon junit tue tous les process
        do {
            Thread.sleep(2000);
        } while (!service.jobs.isEmpty());

    }

    /**
     * Test la generation de facteur simple.
     * 
     * @throws StorageException 
     * @throws TopiaException
     */
    @Test
    public void testFactorPreScriptFactor() throws StorageException, TopiaException {
        
        // factor
        Factor factor = new Factor("test");
        ContinuousDomain domain = new ContinuousDomain(Distribution.QUNIFMM);
        domain.addDistributionParam("min", 0.0);
        domain.addDistributionParam("max", 50.0);
        factor.setDomain(domain);
        factor.setPath("fr.ifremer.entities.Cell#1234567890#length");
        factor.setValueForIdentifier(0.5);
        
        Assertions.assertEquals(25.0, factor.getValue());
        
        // scenario
        Scenario scenario = new Scenario();
        List<Factor> factors = new ArrayList<>();
        factors.add(factor);
        scenario.setFactors(factors);

        String scriptContent = FactorHelper.generatePreScript(scenario);
        Assertions.assertTrue(scriptContent.contains("java.lang.Double"));
        Assertions.assertTrue(scriptContent.contains("db.findByTopiaId(\"fr.ifremer.entities.Cell#1234567890\")"));
        Assertions.assertTrue(scriptContent.contains("BeanUtils.setProperty(entity0, \"length\""));
    }
    
    /**
     * Creer un simulation.
     * Creer un populationSeasonInfoDAO.
     * 
     * @throws StorageException 
     * @throws TopiaException
     */
    @Test
    public void testFactorPreScriptFactorMatrix() throws StorageException, TopiaException {

        // matrix 1
        MatrixND matrix1 = MatrixFactory.getInstance().create("test1",
                new int[] { 3, 2 }, new String[] { "col1", "col2" });
        matrix1.setValue(new int[] { 0, 0 }, 13);
        matrix1.setValue(new int[] { 0, 1 }, -14);
        matrix1.setValue(new int[] { 1, 0 }, 21);
        matrix1.setValue(new int[] { 1, 1 }, 2);
        matrix1.setValue(new int[] { 2, 0 }, 12);
        matrix1.setValue(new int[] { 2, 1 }, -1);

        // matrix 2
        MatrixND matrix2 = MatrixFactory.getInstance().create("test2",
                new int[] { 2, 3 }, new String[] { "col1", "col2" });
        matrix2.setValue(new int[] { 0, 0 }, 9999);
        matrix2.setValue(new int[] { 0, 1 }, 15000);
        matrix2.setValue(new int[] { 0, 2 }, -40000);
        matrix2.setValue(new int[] { 1, 0 }, 21345);
        matrix2.setValue(new int[] { 1, 1 }, 81000);
        matrix2.setValue(new int[] { 1, 2 }, -13000);

        // factor
        Factor factor1 = new Factor("testmatrix");
        DiscreteDomain domain1 = new DiscreteDomain();
        domain1.getValues().put("m1", matrix1);
        factor1.setDomain(domain1);
        factor1.setPath("org.nuiton.math.matrix.MatrixND#563456293453#2.456347646#dim");
        factor1.setValueForIdentifier("m1");

        // factor
        Factor factor2 = new Factor("testmatrix");
        DiscreteDomain domain2 = new DiscreteDomain();
        domain2.getValues().put("m2", matrix1);
        factor2.setDomain(domain2);
        factor2.setPath("org.nuiton.math.matrix.MatrixND#563456293453#2.456347646#dim");
        factor2.setValueForIdentifier("m2");

        // scenario
        Scenario scenario = new Scenario();
        List<Factor> factors = new ArrayList<>();
        factors.add(factor1);
        factors.add(factor2);
        scenario.setFactors(factors);

        String scriptContent = FactorHelper.generatePreScript(scenario);
        //log.info("Script = " + scriptContent);
        Assertions.assertTrue(scriptContent.contains("org.nuiton.math.matrix.MatrixNDImpl "));
        Assertions.assertTrue(scriptContent.contains("db.findByTopiaId(\"org.nuiton.math.matrix.MatrixND#563456293453#2.456347646\");"));
        Assertions.assertTrue(scriptContent.contains("BeanUtils.setProperty(entity0, \"dim\""));
    }

    /**
     * Creer un scenario basé sur les EquationDomain.
     * 
     * @throws StorageException 
     * @throws TopiaException 
     */
    @Test
    public void testFactorPreScriptEquation() throws StorageException, TopiaException {

        // factor
        Factor factorEquation = new Factor("test.equation.name");
        ContinuousDomain domain1 = new ContinuousDomain(Distribution.QUNIFPC);
        domain1.addDistributionParam("reference", 10.0);
        domain1.addDistributionParam("coefficient", 0.1);
        factorEquation.setEquationVariableName("K1");
        factorEquation.setDomain(domain1);
        factorEquation.setPath("fr.ifremer.equation1#testFactorPreScriptEquation");
        factorEquation.setValueForIdentifier(0.4);

        // scenario
        Scenario scenario = new Scenario();
        List<Factor> factors = new ArrayList<>();
        factors.add(factorEquation);
        scenario.setFactors(factors);

        String scriptContent = FactorHelper.generatePreScript(scenario);
        //log.info("Script = " + scriptContent);

        // le nom de la valeur de la variable n'apparait pas "en elle meme"
        // dans le prescript, il y a seulement le nom
        // du facteur. Charge à l'UI de savoir comment l'afficher
        Assertions.assertTrue(scriptContent.contains("context.setComputeValue(\"test.equation.name.K1\",9.8)"));
    }

    /**
     * Test que la generation du prescript avec l'ajout des populations
     * de départ et des regles de IsisFish 3.4.0.0 fonctionne correctement.
     * 
     * @throws IsisFishException 
     */
    @Test
    public void testPrescriptGenerationPopAndRules() throws IsisFishException {

        // get a context to a database
        RegionStorage regionStorage = RegionStorage.getRegion("BaseMotosICA");
        TopiaContext regionContext = regionStorage.getStorage();

        // build test scenario
        Scenario scenario1 = new Scenario();

        // factor pop
        Factor factorPop = new Factor("myPopulationParams");
        factorPop.setDomain(new ContinuousDomain());
        factorPop.setPath("parameters.population.xxx.number");
        MatrixND mat = MatrixFactory.getInstance().create(new int[]{2, 3});
        MatrixHelper.convertToId(mat);
        factorPop.setValue(mat);

        // factor rule, species = c'est une des especes de la base BaseMotosICA
        Rule ruleTacPoids = RuleStorage.getRule("TACpoids").getNewInstance();
        Properties props = new Properties();
        props.setProperty("rule.0.parameter.species", "fr.ifremer.isisfish.entities.Species#1169028645830#0.022262364425031378");
        RuleHelper.populateRule(0, regionContext, ruleTacPoids, props);
        Rule ruleTailleMin = RuleStorage.getRule("TailleMin").getNewInstance();
        props = new Properties();
        props.setProperty("rule.0.parameter.species", "fr.ifremer.isisfish.entities.Species#1169028645830#0.022262364425031378");
        props.setProperty("rule.0.parameter.propSurvie", "42");
        RuleHelper.populateRule(0, regionContext, ruleTailleMin, props);
        Factor factorRule = new Factor("myRuleParams");
        factorPop.setDomain(new RuleDiscreteDomain());
        factorRule.setPath("parameters.rules");
        List<Rule> rules = new ArrayList<>();
        rules.add(ruleTacPoids);
        rules.add(ruleTailleMin);
        factorRule.setValue(rules);

        List<Factor> factors = new ArrayList<>();
        factors.add(factorPop);
        factors.add(factorRule);
        scenario1.setFactors(factors);

        // generate prescript for that wonderfull scenario
        String preScriptContent = FactorHelper.generatePreScript(scenario1);
        log.info("Script = " + preScriptContent);

        Assertions.assertTrue(preScriptContent.contains("params.setProperty(\"population.xxx.number\""));
        Assertions.assertTrue(preScriptContent.contains("params.setProperty(\"rule.0.parameter.species\",\"fr.ifremer.isisfish.entities.Species#1169028645830#0.022262364425031378:EngEnc\");"));
        Assertions.assertTrue(preScriptContent.contains("params.setProperty(\"rule.1.parameter.propSurvie\",\"42.0\");"));
        Assertions.assertTrue(preScriptContent.contains("rules\",\"TACpoids,TailleMin\")"));
    }
    
    /**
     * Test que la generation du prescript avec l'ajout des facteurs sur
     * les parametres de regles.
     * 
     * @since 4.0.0.0
     * @throws IsisFishException 
     */
    @Test
    public void testPrescriptGenerationRulesParameters() throws IsisFishException {

        // build test scenario
        Scenario scenario1 = new Scenario();

        // factor pop
        Factor factorPop = new Factor("parameters of rule TACpoids");
        factorPop.setDomain(new ContinuousDomain());
        factorPop.setPath("parameters.rule.0.parameter.tacInTons");
        factorPop.setValue(42.0);

        List<Factor> factors = new ArrayList<>();
        factors.add(factorPop);
        scenario1.setFactors(factors);

        // generate prescript for that wonderfull scenario
        String preScriptContent = FactorHelper.generatePreScript(scenario1);
        log.info("Script = " + preScriptContent);

        Assertions.assertTrue(preScriptContent.contains("params.setProperty(\"rule.0.parameter.tacInTons\",\"42.0\");"));
    }
    
    /**
     * Test que la generation du prescript avec l'ajout des groupes des facteurs
     * depuis IsisFish 4.0.0.0 fonctionne correctement.
     * 
     * @throws IsisFishException
     * 
     * FIXME echatellier replace setValueForIdentifier code
     */
    @Disabled
    public void testPrescriptGenerationGroups() throws IsisFishException {

        // build test scenario
        Scenario scenario1 = new Scenario();

        // factor 1
        Factor factor1 = new Factor("factor 1 (double)");
        ContinuousDomain domain1 = new ContinuousDomain(Distribution.QUNIFMM);
        domain1.addDistributionParam("min", 0.0);
        domain1.addDistributionParam("max", 50.0);
        factor1.setCardinality(4);
        factor1.setDomain(domain1);
        factor1.setPath("fr.ifremer.isisfish.entities.PopulationGroup#1156461521013#0.1715620681984218#maxLength");

        // factor 2
        Factor factor2 = new Factor("factor 2 (double)");
        ContinuousDomain domain2 = new ContinuousDomain(Distribution.QUNIFMM);
        domain2.addDistributionParam("min", 1.0);
        domain2.addDistributionParam("max", 9.0);
        factor2.setCardinality(3);
        factor2.setDomain(domain2);
        factor2.setPath("fr.ifremer.isisfish.entities.PopulationGroup#1156461521064#0.022976136053553198#minLength");

        // factor 3
        Factor factor3 = new Factor("factor 3 (double)");
        DiscreteDomain domain3 = new DiscreteDomain();
        domain3.getValues().put(0.0, 12.3);
        domain3.getValues().put(1.0, 70.9);
        domain3.getValues().put(2.0, 21.0);
        domain3.getValues().put(3.0, -12.1);
        domain3.getValues().put(4.0, -8.45);
        factor3.setDomain(domain3);
        factor3.setPath("fr.ifremer.isisfish.entities.PopulationGroup#1156461521013#0.1715620681984218#maxLength");

        // factor 4
        Factor factor4 = new Factor("factor 4 (double)");
        DiscreteDomain domain4 = new DiscreteDomain();
        domain4.getValues().put(0.0, -7.3);
        domain4.getValues().put(1.0, 4.9);
        domain4.getValues().put(2.0, -42.0);
        domain4.getValues().put(3.0, 27.1);
        domain4.getValues().put(4.0, 53.32);
        factor4.setDomain(domain4);
        factor4.setPath("fr.ifremer.isisfish.entities.PopulationGroup#1156461521064#0.022976136053553198#minLength");
        
        // group continu
        FactorGroup group1 = new FactorGroup("myContinuousGroup");
        group1.addFactor(factor1);
        group1.addFactor(factor2);
        
        // group discret
        FactorGroup group2 = new FactorGroup("myDiscreteGroup");
        group2.addFactor(factor3);
        group2.addFactor(factor4);
        
        // compute AS => scenario
        group1.setValueForIdentifier(0.75);
        scenario1.addFactor(group1);
        group2.setValueForIdentifier(2.0);
        scenario1.addFactor(group2);

        // generate prescript for that wonderfull scenario
        String preScriptContent = FactorHelper.generatePreScript(scenario1);
        //log.info("Script = " + preScriptContent);

        Assertions.assertTrue(preScriptContent.contains("/* factor group : myContinuousGroup */"));
        Assertions.assertTrue(preScriptContent.contains("beanUtils.convert(\"37.5\""));
        Assertions.assertTrue(preScriptContent.contains("beanUtils.convert(\"8.5\""));
        Assertions.assertTrue(preScriptContent.contains("/* factor group : myDiscreteGroup */"));
        Assertions.assertTrue(preScriptContent.contains("beanUtils.convert(\"21.0\""));
        Assertions.assertTrue(preScriptContent.contains("value4 = beanUtils.convert(\"-42.0\""));
    }
    
    /**
     * Test que la generation des prescript des equation discrete donne un résultat attendu.
     * 
     * @throws IsisFishException
     */
    @Test
    public void testPrescriptGenerationDiscreteEquation() throws IsisFishException {

        // build test scenario
        Scenario scenario1 = new Scenario();

        // factor 1
        Factor factor1 = new Factor("factor 1 (equation)");
        EquationDiscreteDomain domain1 = new EquationDiscreteDomain();
        SortedMap<Object, Object> values = new TreeMap<>();
        values.put("1", "String myValue=\"test\";\nreturn 0.0;");
        values.put("2", "String myValue=\"test2\";\nreturn 0.2;");
        domain1.setValues(values);
        factor1.setCardinality(2);
        factor1.setDomain(domain1);
        factor1.setPath("fr.ifremer.isisfish.entities.PopulationGroup#1156461521013#0.1715620681984218#growthEquation");
        
        // compute AS => scenario
        factor1.setValueForIdentifier("2");

        scenario1.addFactor(factor1);

        // generate prescript for that wonderfull scenario
        String preScriptContent = FactorHelper.generatePreScript(scenario1);
        log.info("Script = " + preScriptContent);

        Assertions.assertTrue(preScriptContent.contains("beanUtils.convert(\"String myValue=\\\"test2\\\""));
        Assertions.assertTrue(preScriptContent.contains("BeanUtils.setProperty(entity0, \"growthEquationContent\""));
    }

    /**
     * Test que lorsque l'on lance beaucoup de simulations, il ne reste oas des threads inutiles qui occupent toutes
     * les ressources de la machines.
     * @throws InterruptedException 
     */
    @Test
    @Disabled
    public void testNonClosedThreadsWhenRunningALotOfSimulations() throws IOException, InterruptedException {
        //RegionStorage region = RegionStorage.getRegion("DemoRegion");

        File dirTest = FileUtil.createTempDirectory("sims-", "test", new File("target")).getAbsoluteFile();

        for (int i = 0; i <= 20; i++) {
            // V1
            // SimulationStorage sim = SimulationStorage.importAndRenameZip(
            //        dirTest, initialZip, "sim" + i);

            // v2
            //SimulationStorage result = new SimulationStorage(dirTest, "test" + i, null);
            //TopiaContext tx = result.getStorage().beginTransaction();
            //tx.closeContext();
            //result.closeStorage();

            // v3
            File simDir = new File(dirTest, "sim" + i);
            Properties config = new Properties();
            IsisH2Config.addDatabaseConfig(config, simDir);
            TopiaContext storage = TopiaContextFactory.getContext(config);
            storage.closeContext();
        }

        Thread.getAllStackTraces().keySet().forEach(t -> System.out.println(t.getName()));

        // une seule simulation en prends 10
        // donc meme pour 100 simulations, ca ne devrait pas être beaucoup plus
        Assertions.assertTrue(Thread.activeCount() < 100, String.format("Il y a %d threads", Thread.activeCount()));
    }
}
