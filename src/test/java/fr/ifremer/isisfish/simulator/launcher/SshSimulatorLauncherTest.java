/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2008 - 2019 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator.launcher;

import fr.ifremer.isisfish.AbstractIsisFishTest;
import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.util.ftp.CopyStreamListener;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.io.CopyStreamEvent;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test portant sur la classe qui doit lancer des 
 * simulation au travers de SSH.
 *
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class SshSimulatorLauncherTest extends AbstractIsisFishTest {

    /** Commons logging log. */
    private static Log log = LogFactory.getLog(SshSimulatorLauncherTest.class);

    /**
     * Test l'instantiation du script qsub (datarmor).
     * 
     * Le script contient plusieurs variables qui doivent être remplacées.
     * 
     * @throws IOException when freemarker throws it
     */
    @Test
    public void testFreemarkerTemplateSimpleScript() throws IOException {

        String simulationId = "sim_test1_2009-11-10-18-08_122";
        String simulationZip = "simulation-" + simulationId + "-preparation.zip";
        String simulationResultZip = "/tmp/simulation" + simulationId + "-result.zip";
        String simulationPreScript = "simulation-" + simulationId + "-prescript.bsh";
        String isisHome = IsisFish.config.getSimulatorSshIsisHome();
        String isisTemp = "$SCRATCH/eftp/isis-temp-4";

        SSHSimulatorLauncher launcher = new SSHSimulatorLauncher();
        String content = launcher.getSimulationScriptLaunchContent(
                SSHSimulatorLauncher.QSUB_SCRIPT_TEMPLATE, simulationId,
                simulationZip, true, simulationResultZip, simulationPreScript, false);

        if (log.isDebugEnabled()) {
            log.debug("Script content = " + content);
        }

        // simulation parameters
        assertThat(content).contains(simulationId);
        assertThat(content).contains(simulationZip);
        assertThat(content).contains(simulationResultZip);
        assertThat(content).contains(simulationPreScript);
        assertThat(content).contains(" --simulateRemotellyWithPreScript ");
        assertThat(content).contains("/appli/emh-commun/isis-fish/jdk17/bin/java");

        // isis location
        assertThat(content).contains(isisHome);
       
        // isis temp
        assertThat(content).contains(isisTemp);
    }

    /**
     * Test l'instantiation du script qsub (datarmor).
     * 
     * Test sans script de pre simu.
     * 
     * Le script contient plusieurs variables qui doivent être remplacées.
     * 
     * @throws IOException when freemarker throws it
     */
    @Test
    public void testFreemarkerTemplateSimpleAndScript() throws IOException {

        String simulationId = "sim_test1_2009-11-10-18-08_122";
        String simulationZip = "simulation-" + simulationId + "-preparation.zip";
        String simulationResultZip = "/tmp/simulation" + simulationId + "-result.zip";
        String simulationPreScript = null;
        String isisHome = IsisFish.config.getSimulatorSshIsisHome();
        String isisTemp = "$SCRATCH/eftp/isis-temp-4";

        SSHSimulatorLauncher launcher = new SSHSimulatorLauncher();
        String content = launcher.getSimulationScriptLaunchContent(
                SSHSimulatorLauncher.QSUB_SCRIPT_TEMPLATE, simulationId,
                simulationZip, true, simulationResultZip, simulationPreScript, false);

        if (log.isDebugEnabled()) {
            log.debug("Script content = " + content);
        }

        // simulation parameters
        assertThat(content).contains(simulationId);
        assertThat(content).contains(simulationZip);
        assertThat(content).contains(simulationResultZip);
        assertThat(content).contains(" --simulateRemotelly ");

        // isis location
        assertThat(content).contains(isisHome);
        
        // isis temp
        assertThat(content).contains(isisTemp);
    }
    
    /**
     * Test l'instantiation du script qsub en multijobs, sans prescript.
     * 
     * Test sans script de pre simu.
     * 
     * Le script contient plusieurs variables qui doivent être remplacées.
     * 
     * Also test configuration off remote Java path.
     * 
     * @throws IOException when freemarker throws it
     */
    @Test
    public void testFreemarkerTemplateMultiJobs() throws IOException {

        // some things to test !
        String shortSimulationId = "sim_test1_2009-11-10-18-08";
        String simulationId = shortSimulationId + "_122";
        String simulationZip = "simulation-" + simulationId + "-preparation.zip";
        String simulationResultZip = "/tmp/simulation-" + simulationId + "-result.zip";
        String simulationPreScript = null;
        String isisHome = IsisFish.config.getSimulatorSshIsisHome();
        String isisTemp = "$SCRATCH/eftp/isis-temp-4";
        String javaPath = "/home1/noumea/cluster";
        IsisFish.config.setSimulatorSshJavaPath(javaPath);

        SSHSimulatorLauncher launcher = new SSHSimulatorLauncher();
        String content = launcher.getSimulationScriptLaunchContent(
                SSHSimulatorLauncher.QSUB_SCRIPT_TEMPLATE, shortSimulationId,
                simulationZip, true, simulationResultZip, simulationPreScript, true);

        if (log.isDebugEnabled()) {
            log.debug("Script content = " + content);
        }

        // simulation parameters
        assertThat(content).contains(shortSimulationId);
        assertThat(content).contains("simulation-" + shortSimulationId + "_$PBS_ARRAY_INDEX-preparation.zip");
        assertThat(content).contains("/tmp/simulation-" + shortSimulationId + "_$PBS_ARRAY_INDEX-result.zip");
        assertThat(content).contains(isisTemp + "/simulation-" + shortSimulationId + "_$PBS_ARRAY_INDEX-output.txt\"");
        assertThat(content).contains(" --simulateRemotelly ");
        assertThat(content).contains(javaPath);

        // isis location
        assertThat(content).contains(isisHome);
        
        // isis temp
        assertThat(content).contains(isisTemp);
    }
    
    /**
     * Test l'instantiation du script qsub en multijobs, avec prescript.
     * 
     * Le script contient plusieurs variables qui doivent être remplacées.
     * 
     * Dans ce test, le fichier zip est configuré pour être non standalone
     * également.
     * 
     * @throws IOException when freemarker throws it
     */
    @Test
    public void testFreemarkerTemplateMultiJobsAndScript() throws IOException {

        String shortSimulationId = "as_blah_2009-11-27-10-39";
        String simulationId = shortSimulationId + "_122";
        String simulationZip = "simulation-" + simulationId + "-preparation.zip";
        String simulationResultZip = "/tmp/simulation-" + simulationId + "-result.zip";
        String simulationPreScript = "simulation-" + simulationId + "-prescript.bsh";
        String isisHome = IsisFish.config.getSimulatorSshIsisHome();
        String isisTemp = "$SCRATCH/eftp/isis-temp-4";

        SSHSimulatorLauncher launcher = new SSHSimulatorLauncher();
        String content = launcher.getSimulationScriptLaunchContent(
                SSHSimulatorLauncher.QSUB_SCRIPT_TEMPLATE, shortSimulationId,
                simulationZip, false, simulationResultZip, simulationPreScript, true);

        if (log.isDebugEnabled()) {
            log.debug("Script content = " + content);
        }

        // simulation parameters
        assertThat(content).contains(shortSimulationId + "_$PBS_ARRAY_INDEX\"");
        assertThat(content).contains("simulation-" + simulationId + "-preparation.zip");
        assertThat(content).contains("/tmp/simulation-" + shortSimulationId + "_$PBS_ARRAY_INDEX-result.zip");
        assertThat(content).contains("simulation-" + shortSimulationId + "_$PBS_ARRAY_INDEX-prescript.bsh");
        assertThat(content).contains(isisTemp + "/simulation-" + shortSimulationId + "_$PBS_ARRAY_INDEX-output.txt\"");
        assertThat(content).contains(" --simulateRemotellyWithPreScript ");

        // isis location
        assertThat(content).contains(isisHome);
        
        // isis temp
        assertThat(content).contains(isisTemp);
        assertThat(content).doesNotContain("/scratch"); // ftp only
    }
    
    /**
     * Test simulation names with special char.
     * 
     * as_ICES2010_M+_10%_152Param_2010-11-08-17-18.
     * 
     * Script now contains \\Q and \\E for thta case.
     * 
     * @throws IOException when freemarker throws it
     */
    @Test
    public void testFreemarkerTemplateScriptSpecialchar() throws IOException {

        // some things to test !
        String shortSimulationId = "as_ICES2010_M+_10%_152Param_2010-11-08-17-18";
        String simulationId = shortSimulationId + "_122";
        String simulationZip = "simulation-" + simulationId + "-preparation.zip";
        String simulationResultZip = "/tmp/simulation-" + simulationId + "-result.zip";
        String simulationPreScript = null;
        String isisTemp = "$SCRATCH/eftp/isis-temp-4";

        SSHSimulatorLauncher launcher = new SSHSimulatorLauncher();
        String content = launcher.getSimulationScriptLaunchContent(
                SSHSimulatorLauncher.QSUB_SCRIPT_TEMPLATE, shortSimulationId,
                simulationZip, true, simulationResultZip, simulationPreScript, true);

        if (log.isInfoEnabled()) {
            log.info("Script content = " + content);
        }

        // simulation parameters
        assertThat(content).contains(shortSimulationId);
        assertThat(content).contains("simulation-" + shortSimulationId + "_$PBS_ARRAY_INDEX-preparation.zip");
        assertThat(content).contains("/tmp/simulation-" + shortSimulationId + "_$PBS_ARRAY_INDEX-result.zip");
        assertThat(content).contains(isisTemp + "/simulation-" + shortSimulationId + "_$PBS_ARRAY_INDEX-output.txt\"");
        assertThat(content).contains(" --simulateRemotelly ");
    }
    
    /**
     * Test that prescript starts with shebang.
     * 
     * @throws IOException when freemarker throws it
     */
    @Test
    public void testFreemarkerTemplateScriptShebang() throws IOException {

        // some things to test !
        String shortSimulationId = "as_ICES2010_M+_10%_152Param_2010-11-08-17-18";
        String simulationId = shortSimulationId + "_122";
        String simulationZip = "simulation-" + simulationId + "-preparation.zip";
        String simulationResultZip = "/tmp/simulation-" + simulationId + "-result.zip";
        String simulationPreScript = null;

        SSHSimulatorLauncher launcher = new SSHSimulatorLauncher();
        String content = launcher.getSimulationScriptLaunchContent(
                SSHSimulatorLauncher.QSUB_SCRIPT_TEMPLATE, shortSimulationId,
                simulationZip, true, simulationResultZip, simulationPreScript, true);

        if (log.isInfoEnabled()) {
            log.info("Script content = " + content);
        }

        // simulation parameters
        assertThat(content).startsWith("#!/bin/csh");
    }

    /**
     * Test que ${R_HOME} n'est pas interpreté par freemarker.
     * 
     * @throws IOException
     */
    @Test
    public void testFreemarkerTemplatePathEscape() throws IOException {
        SSHSimulatorLauncher launcher = new SSHSimulatorLauncher();
        String content = launcher.getSimulationScriptLaunchContent(
                SSHSimulatorLauncher.QSUB_SCRIPT_TEMPLATE, "test1234",
                "", true, "", "", true);

        assertThat(content).contains("${R_HOME}/lib");
        assertThat(content).contains("-Duser.home=$SCRATCH"); // ne doit pas être remplacé
        assertThat(content).contains("$SCRATCH/isis-fish-4/isis-database/simulations/test1234"); // ne doit pas être remplacé
    }
    
    /**
     * Test que memoire définie dans la configuration est bien présente.
     */
    @Test
    public void testFreemarkerTemplateMemory() throws IOException {
        SSHSimulatorLauncher launcher = new SSHSimulatorLauncher();
        String content = launcher.getSimulationScriptLaunchContent(
                SSHSimulatorLauncher.QSUB_SCRIPT_TEMPLATE, "",
                "", true, "", "", true);

        assertThat(content).contains("Xmx2000M");
    }

    /**
     * Test que le fichier est bien encodé avec des lines return linux (\n) et pas windows.
     */
    @Test
    public void testFreemarkerTemplateLineReturn() throws IOException {
        SSHSimulatorLauncher launcher = new SSHSimulatorLauncher();
        String content = launcher.getSimulationScriptLaunchContent(
                SSHSimulatorLauncher.QSUB_SCRIPT_TEMPLATE, "",
                "", true, "", "", true);

        assertThat(content).doesNotContain("\r\n");
    }

    /**
     * En sequentiel, ca fonctionne.
     */
    @Test
    @Disabled
    public void testStandaloneFtpUpload() throws IOException, InterruptedException {
        SSHSimulatorLauncher launcher = new SSHSimulatorLauncher();
        IsisFish.config.setSimulatorFtpLogin("ec1f723");
        FTPClient ftpClient = launcher.getFtpClient();
        FTPClient ftpClient2 = launcher.getFtpClient();
        File file = new File("/home/chatellier/isis-fish-4/isis-temp/isisfish-simulation-1578314558756-preparation.zip");
        assertThat(file).exists();

        ftpClient.setCopyStreamListener(new CopyStreamListener() {
            @Override
            public void bytesTransferred(CopyStreamEvent event) {
                System.out.print(".");
            }

            @Override
            public void bytesTransferred(long totalBytesTransferred, int bytesTransferred, long streamSize) {
                System.out.print(":");
            }
        });
        for (int i = 0; i < 10 ; i++) {
            System.out.println("\nUploading file " + i);
            try (InputStream is = new FileInputStream(file)) {
                ftpClient.storeFile("/scratch/test-upload-" + i, is);
            }
            Thread.sleep(1000);
            System.out.println("\nDownloading file " + i);
            try (OutputStream out = new FileOutputStream(new File("target/test-download-" + i))) {
                ftpClient2.retrieveFile("/scratch/test-upload-" + i, out);
            }
            Thread.sleep(5000);
        }
    }

    /**
     * Ca fail si deux threads font des choses en meme temps sur le même serveur.
     */
    @Test
    @Disabled
    public void testConcurrentFtpUpload() throws IOException, InterruptedException {
        SSHSimulatorLauncher launcher = new SSHSimulatorLauncher();
        IsisFish.config.setSimulatorFtpLogin("ec1f723");
        FTPClient ftpClient = launcher.getFtpClient();
        File file = new File("/home/chatellier/isis-fish-4/isis-temp/isisfish-simulation-1578314558756-preparation.zip");
        assertThat(file).exists();

        Thread upThread = new Thread(() -> {
            try {
                for (int i = 0; i < 10; i++) {
                    synchronized (ftpClient) {
                        System.out.println("\nUploading file " + i);
                        try (InputStream is = new FileInputStream(file)) {
                            ftpClient.storeFile("/scratch/test-upload-" + i, is);
                        }
                    }
                    Thread.sleep(1000);
                }
            } catch (IOException|InterruptedException ex) {
                System.out.println("Up exception");
                ex.printStackTrace();
            }
        });

        Thread downThread = new Thread(() -> {
            try {
                for (int i = 0; i < 10; i++) {
                    synchronized (ftpClient) {
                        System.out.println("\nDownloading file " + i);
                        try (OutputStream out = new FileOutputStream(new File("target/test-download-" + i))) {
                            ftpClient.retrieveFile("/scratch/test-upload-" + i, out);
                        }
                    }
                    Thread.sleep(5000);
                }
            } catch (IOException|InterruptedException ex) {
                System.out.println("Down exception");
                ex.printStackTrace();
            }
        });

        upThread.start();
        downThread.start();
        upThread.join();
        downThread.join();
    }
}
