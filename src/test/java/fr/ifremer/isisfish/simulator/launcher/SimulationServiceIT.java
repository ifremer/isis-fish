/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2010 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator.launcher;

import fr.ifremer.isisfish.AbstractIsisFishTest;
import fr.ifremer.isisfish.datastore.ResultStorage;
import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.entities.Result;
import fr.ifremer.isisfish.simulator.SimulationControl;
import fr.ifremer.isisfish.types.TimeStep;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.nuiton.topia.TopiaContext;

import java.io.File;
import java.net.URL;
import java.util.List;

/**
 * Integration tests for {@link SimulationService}.
 * 
 * Dans ces deux tests, les simulations ont été effectuées sur isis-fish-3.2.0.9.
 * Les resultats ont été exporté dans le fichier data-backup.sql.gz. Le zip
 * contient cet export et le fichier de parametres.
 * 
 * Il ne contient pas les scripts pour utiliser ceux de la base de test.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$ By : $Author$
 */
@Disabled
public class SimulationServiceIT extends AbstractIsisFishTest {

    /** Class logger. */
    private static Log log = LogFactory.getLog(SimulationServiceIT.class);

    /**
     * Read a zipped simulation file.
     * Redo simulation with same parameters, and check that result are all equals.
     * 
     * @param zipName
     * @throws Exception
     */
    protected void testSimulate(String zipName) throws Exception {

        int resultChecked = 0;
        
        // recuperation du fichier zip de la simulation de test
        File zip = new File(zipName);

        // import de cette simulation
        String name = "simulation-test-" + System.currentTimeMillis();
        SimulationStorage simRef = SimulationStorage.importAndRenameZip(zip, name + "-ref");

        // la simulation testée ici
        SimulationStorage sim = SimulationStorage.importAndRenameZip(zip, name + "-temp");
        
        try {
            // suppression des resultats de cette simulation
            TopiaContext tx = sim.getStorage().beginTransaction();
            tx.execute("DELETE from " + Result.class.getName());
            tx.commitTransaction();
            tx.closeContext();

            // lancement de la simulation
            SimulationControl control = new SimulationControl(sim.getName());
            control.setStep(new TimeStep());
            InProcessSimulatorLauncher launcher = new InProcessSimulatorLauncher();
            sim = launcher.localSimulate(control, sim);

            // if simulation ends with exception, no need to continue
            if (!StringUtils.isEmpty(sim.getInformation().getException())) {
                Assertions.fail(sim.getInformation().getException());
            }

            // verification qu'on retrouve les memes resulats pour les deux simulations
            ResultStorage resultRef = simRef.getResultStorage();
            ResultStorage result = sim.getResultStorage();

            List<String> resultNames = resultRef.getResultName();
            Assertions.assertEquals(resultNames, result.getResultName());

            TimeStep lastStep = resultRef.getLastStep();

            
            if (log.isInfoEnabled()) {
                log.info("Check result ...");
            }

            for (TimeStep step = new TimeStep(0); step.beforeOrEquals(lastStep); step = step.next()) {
                for (String resultName : resultNames) {
                    if (log.isDebugEnabled()) {
                        log.debug("Check result " + step + " " + resultName);
                    }
                    Assertions.assertEquals(resultRef.getMatrix(step, resultName),result.getMatrix(step, resultName),
                            "Date " + step.getStep() + " result " + resultName);
                    
                    resultChecked++;
                }
            }
            
            // nombre de résultat sur 12 mois, sur 5 ans
            Assertions.assertEquals(resultNames.size() * 12 * 5, resultChecked, "All expected results have not been checked !");

        } finally {
            // fermeture des bases
            simRef.getStorage().closeContext();
            sim.getStorage().closeContext();
            // suppresion des deux simulations
            simRef.delete(false);
            sim.delete(false);
        }
        
        // Test qu'au moins quelques resultats ont été verifié
        Assertions.assertTrue(resultChecked > 100, "At least some result must be done");
    }

    /**
     * Do simulation test on gdg database.
     * 
     * Simulation properties :
     *  - 10 years
     *  - all strategies
     *  - one population
     * 
     * @throws Exception
     */
    @Test
    public void testSimlateGdG() throws Exception {
        URL zipURL = this.getClass().getResource("sim_test-gdg-3.2-3.3.zip");
        testSimulate(zipURL.getFile());
    }
    
    /**
     * Do simulation test on ICA database.
     * 
     * Simulation properties :
     *  - 10 years
     *  - all strategies
     *  - one population
     * 
     * @throws Exception
     */
    @Test
    public void testSimlateICA() throws Exception {
        URL zipURL = this.getClass().getResource("sim_test-ica-3.2-3.3.zip");
        testSimulate(zipURL.getFile());
    }
}
