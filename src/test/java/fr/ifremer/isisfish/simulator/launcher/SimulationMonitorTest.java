/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2010 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.simulator.launcher;

import fr.ifremer.isisfish.AbstractIsisFishTest;
import fr.ifremer.isisfish.simulator.SimulationControl;
import fr.ifremer.isisfish.simulator.SimulationParameterImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.AbstractMap.SimpleEntry;
import java.util.Comparator;
import java.util.Date;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.function.Function;

/**
 * Test de la class SimulationMonitor.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class SimulationMonitorTest extends AbstractIsisFishTest {

    protected SortedSet<SimpleEntry<Date, SimulationJob>> checkSet = new TreeSet<>(Comparator.comparing((Function<SimpleEntry<Date, SimulationJob>, Date>) SimpleEntry::getKey).thenComparing(SimpleEntry::getValue));
    
    @Test
    public void testSimulationSetUniqueNess() {
        SimulationService service = SimulationService.getService();
        
        SimulationControl control1 = new SimulationControl("test1");
        SimulationItem item1 = new SimulationItem(control1, new SimulationParameterImpl());
        SimulationJob job1 = new SimulationJob(service, item1, 0);
        
        SimulationControl control2 = new SimulationControl("test2");
        SimulationItem item2 = new SimulationItem(control2, new SimulationParameterImpl());
        SimulationJob job2 = new SimulationJob(service, item2, 0);
        
        Date d = new Date();
        checkSet.add(new SimpleEntry<>(d, job1));
        checkSet.add(new SimpleEntry<>(d, job2));
        
        Assertions.assertEquals(2, checkSet.size());
    }
}
