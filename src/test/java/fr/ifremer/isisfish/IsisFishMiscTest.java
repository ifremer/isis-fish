/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2010 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish;

import org.apache.commons.io.FileUtils;
import org.h2.engine.Constants;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Misc test on isis config.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class IsisFishMiscTest {

    /**
     * Test that isisfish.bat file in crlf valid for windows.
     * 
     * If this test fail, use unix2dos command on unix.
     * 
     * @throws IOException 
     */
    @Test
    public void testCRLFOnBat() throws IOException {
        File batFile = new File("src" + File.separator + "main" + File.separator + "assembly" + File.separator + "isisfish.bat");
        batFile = batFile.getAbsoluteFile(); // for maven

        String content = FileUtils.readFileToString(batFile, StandardCharsets.UTF_8);
        System.out.println(content);
        String[] lines = content.split("\n");
        
        // -1, last line have no \r
        for (int index = 0 ; index < lines.length - 1; ++ index) {
            Assertions.assertTrue(lines[index].endsWith("\r"), "File is not valide for Windows on line " + index);
        }
    }

    /**
     * Test that isisfish.sh file in lf valid for windows.
     *
     * If this test fail, use dos2unix command on unix.
     *
     * @throws IOException
     */
    @Test
    public void testLFOnSh() throws IOException {
        File batFile = new File("src" + File.separator + "main" + File.separator + "assembly" + File.separator + "isisfish.sh");
        batFile = batFile.getAbsoluteFile(); // for maven

        String content = FileUtils.readFileToString(batFile, StandardCharsets.UTF_8);
        System.out.println(content);
        String[] lines = content.split("\n");

        // -1, last line have no \r
        for (int index = 0 ; index < lines.length - 1; ++ index) {
            Assertions.assertTrue(!lines[index].contains("\n") && !lines[index].contains("\r"), "File is not valide for linux on line " + index);
        }
    }

    /**
     * Fix la version de H2 à la version 1.4.199.
     *
     * Les bases en 1.4.199 sont "en partie" corrompues, mais fonctionnent quand même.
     * La version 1.4.200 refuse de les ouvrir.
     *
     * Mettre à jour plus tard en effectuant une migration de base de données (import/export). En 5.0 ?
     */
    @Test
    public void fixH2Version() {
        assertThat(Constants.getVersion()).isEqualTo("1.4.199");
    }
}
