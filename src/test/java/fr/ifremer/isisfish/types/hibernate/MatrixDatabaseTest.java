/*
 * #%L
 * IsisFish
 *
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2018 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.types.hibernate;

import fr.ifremer.isisfish.AbstractIsisFishTest;
import fr.ifremer.isisfish.datastore.RegionStorage;
import org.junit.jupiter.api.Test;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class MatrixDatabaseTest extends AbstractIsisFishTest {

    /**
     * Test que si une entity est utilisée dans une semantique de matrice, elle est bien retrouvée.
     */
    @Test
    public void testSearchUsedEntitiesInMatrix() {
        RegionStorage region = RegionStorage.getRegion("DemoRegion");
        TopiaContext tx = region.getStorage().beginTransaction();

        TopiaEntity zoneTest1 = tx.findByTopiaId("fr.ifremer.isisfish.entities.Zone#1156460939641#0.5524360458352713");
        assertThat(zoneTest1).isNotNull();
        List<TopiaEntity> topiaEntities = MatrixDatabaseHelper.checkUsedObjects(tx, zoneTest1);
        assertThat(topiaEntities).hasSize(7); // 2 pop, 5 season

        TopiaEntity zoneTest4 = tx.findByTopiaId("fr.ifremer.isisfish.entities.Zone#1156460942154#0.2798689936968851");
        topiaEntities = MatrixDatabaseHelper.checkUsedObjects(tx, zoneTest4);
        assertThat(topiaEntities).isEmpty();
    }
}
