/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2010 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.types;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.nuiton.math.matrix.MatrixFactory;
import org.nuiton.math.matrix.MatrixND;

import java.util.LinkedList;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Test on {@link Month}.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class MonthTest {

    /**
     * Test that indexOf works on list of month. 
     */
    @Test
    public void listTest() {
        
        List<Month> monthList = new LinkedList<>();
        monthList.add(new Month(0));
        monthList.add(new Month(1));
        monthList.add(new Month(2));
        
        Assertions.assertTrue(monthList.contains(Month.JANUARY));
        Assertions.assertFalse(monthList.contains(Month.AUGUST));
        
        Assertions.assertEquals(0, monthList.indexOf(Month.JANUARY));
        Assertions.assertEquals(-1, monthList.indexOf(Month.AUGUST));
    }
    
    @Test
    public void maxtrixWithMonth() {
        
        List<Month> monthList = new LinkedList<>();
        monthList.add(new Month(0));
        monthList.add(new Month(1));
        monthList.add(new Month(2));
        
        List<Month> monthList2 = new LinkedList<>();
        monthList2.add(Month.FEBRUARY);
        
        MatrixND tmp = MatrixFactory.getInstance().create(
                "test",
                new List[] { monthList },
                new String[] { t("isisfish.populationSeasonInfo.months") });
        
        MatrixND result = MatrixFactory.getInstance().create(
                "test",
                new List[] { monthList2 },
                new String[] { t("isisfish.populationSeasonInfo.months") });

        tmp.pasteSemantics(result);
    }
}
