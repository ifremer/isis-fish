/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2012 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.datastore;

import fr.ifremer.isisfish.AbstractIsisFishTest;
import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.IsisFishException;
import fr.ifremer.isisfish.datastore.CodeSourceStorage.Location;
import fr.ifremer.isisfish.simulator.Simulator;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * SimulatorStorageTest.
 * 
 * Created: 7 août 2006 11:07:57
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class SimulatorStorageTest extends AbstractIsisFishTest {

    private static final Log log = LogFactory.getLog(SimulatorStorageTest.class);
    
    protected Configuration freemarkerConfiguration;

    @BeforeEach
    public void setUp() {
        freemarkerConfiguration = getFreemarkerConfiguration();
    }

    @Test
    public void testNewSimulatorWithCompilation() throws Exception {

        String fileName = "TestSimulator1";

        SimulatorStorage simulatorStorage = SimulatorStorage
                .createSimulator(fileName, Location.COMMUNITY);

        // get template
        Template template = freemarkerConfiguration
                .getTemplate(SimulatorStorage.SIMULATOR_TEMPLATE);

        // context values
        Map<String, Object> root = new HashMap<>();
        root.put("name", fileName);
        root.put("date", new Date());
        root.put("author", IsisFish.config.getUserName());
        root.put("email", IsisFish.config.getUserMail());

        // process template
        Writer out = new StringWriter();
        template.process(root, out);
        out.flush();
        simulatorStorage.setContent(out.toString());

        // 0 = compile success
        int compileResult = simulatorStorage.compile(false, null);
        Assertions.assertEquals(0, compileResult);
    }

    /**
     * Test compilation on {@link JavaSourceStorage} and instanciation.
     * 
     * There was a bug who not compiled if file was never of a non existent file.
     * @throws IsisFishException 
     */
    @Test
    public void testSimulatorStorage() throws IsisFishException {
        
        // Make sur that class file does'nt exists !!!
        File f = new File(IsisFish.config.getCompileDirectory(),
                "simulators" + File.separator + "DefaultSimulator.class");
        if (f.exists()) {
            if (log.isDebugEnabled()) {
                log.debug("Delete file : " + f);
            }
            f.delete();
        }
        else {
            if (log.isDebugEnabled()) {
                log.debug("File : " + f + " doesn't exists !");
            }
        }

        SimulatorStorage simulatorStorage = SimulatorStorage.getSimulator("DefaultSimulator");
        Simulator simulator = simulatorStorage.getNewInstance();
        Assertions.assertNotNull(simulator);
        Assertions.assertTrue(f.exists());
    }
}
