/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2012 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.datastore;

import fr.ifremer.isisfish.AbstractIsisFishTest;
import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.datastore.CodeSourceStorage.Location;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.StringWriter;
import java.io.Writer;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * SensitivityExportStorageTest.
 * 
 * Created: jeu. sept.  3 12:16:08 CEST 2009
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class SensitivityExportStorageTest extends AbstractIsisFishTest {

    protected Configuration freemarkerConfiguration;

    @BeforeEach
    public void setUp() {
        freemarkerConfiguration = getFreemarkerConfiguration();
    }

    @Test
    public void testNewSensitivityExportWithCompilation() throws Exception {

        String fileName = "TestSensitivityExport1";

        SensitivityExportStorage sensitivityExportStorage = SensitivityExportStorage.createSensitivityExport(fileName, Location.COMMUNITY);

        // get template
        Template template = freemarkerConfiguration
                .getTemplate(SensitivityExportStorage.SENSITIVITY_EXPORT_TEMPLATE);

        // context values
        Map<String, Object> root = new HashMap<>();
        root.put("name", fileName);
        root.put("date", new Date());
        root.put("author", IsisFish.config.getUserName());
        root.put("email", IsisFish.config.getUserMail());

        // process template
        Writer out = new StringWriter();
        template.process(root, out);
        out.flush();
        sensitivityExportStorage.setContent(out.toString());

        // 0 = compile success
        int compileResult = sensitivityExportStorage.compile(false, null);
        Assertions.assertEquals(0, compileResult);
    }

}
