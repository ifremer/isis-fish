/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2010 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.datastore;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Test for {@link SimulationInformation} class.
 * 
 * No need for isolated directory (system directory not used).
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class SimulationInformationTest {

    /** Log. */
    protected static Log log = LogFactory.getLog(SimulationInformationTest.class);
    
    /**
     * Test simulation information with no data.
     * 
     * @throws IOException 
     */
    @Test
    public void testSimulationEmpty() throws IOException {

        File file = File.createTempFile("information-empty", ".txt");
        file.deleteOnExit();

        SimulationInformation info = new SimulationInformation(file);
        Assertions.assertNull(info.getSimulationStart());
        Assertions.assertNull(info.getSimulationEnd());
        Assertions.assertFalse(info.hasError());
        Assertions.assertNull(info.getException());
        Assertions.assertEquals(0, info.getExportTime("test"));
        Assertions.assertNotNull(info.toString());
    }

    /**
     * Test que le format du fichier de simulation ne change pas.
     * 
     * @throws ParseException 
     */
    @Test
    public void testExistingSimulationFile() throws ParseException {
        URL informationURL = SimulationInformationTest.class.getResource("information");
        File file = new File(informationURL.getPath());
        
        Assertions.assertTrue(file.exists());
        
        // Do not use same format as {@link SimulationInformation#dateFormat}
        DateFormat df = new SimpleDateFormat("yyyy/dd/MM hh:mm:ss");
        
        SimulationInformation info = new SimulationInformation(file);
        Assertions.assertEquals(df.parse("2009/31/08 14:44:57"), info.getSimulationStart());
        Assertions.assertEquals(df.parse("2009/31/08 14:48:00"), info.getSimulationEnd());
        Assertions.assertEquals(1482, info.getExportTime("VesselMargin.java"));
        Assertions.assertFalse(info.hasError());
        Assertions.assertTrue(info.getOptimizationUsage().indexOf("Cache used") > 0);
        Assertions.assertNull(info.getStatistic());
        Assertions.assertTrue(info.toString().indexOf("VesselMargin.java") > 0);
    }
    
    /**
     * Test que le format du fichier de simulation ne change pas.
     * 
     * Cas d'un fichier avec une erreur.
     */
    @Test
    public void testExistingSimulationFileException() {
        URL informationURL = SimulationInformationTest.class.getResource("exception.information");
        File file = new File(informationURL.getPath());
        
        Assertions.assertTrue(file.exists());
        
        SimulationInformation info = new SimulationInformation(file);
        Assertions.assertTrue(info.hasError());
        Assertions.assertTrue(info.getException().indexOf("TopiaException") > 0);
        Assertions.assertTrue(info.toString().indexOf("TopiaException") > 0);
    }

    /**
     * Test simulation date storage.
     * 
     * @throws IOException 
     * @throws ParseException 
     */
    @Test
    public void testSimulationDate() throws IOException, ParseException {

        // Do not use same format as {@link SimulationInformation#dateFormat}
        DateFormat df = new SimpleDateFormat("hh:mm:ss a");

        File file = File.createTempFile("information-date", ".txt");
        file.deleteOnExit();

        SimulationInformation info = new SimulationInformation(file);

        Date d1 = df.parse("3:30:32 pm");
        Date d2 = df.parse("3:50:47 pm");
        info.setSimulationStart(d1);
        info.setSimulationEnd(d2);

        info.store();

        SimulationInformation testInfo = new SimulationInformation(file);
        Assertions.assertEquals(d1, testInfo.getSimulationStart());
        Assertions.assertEquals(d2, testInfo.getSimulationEnd());
        Assertions.assertTrue(testInfo.toString().indexOf("15:50:47") > 0);
    }
    
    /**
     * Test simulation exception storage.
     * 
     * @throws IOException 
     */
    @Test
    public void testSimulationException() throws IOException {
        File file = File.createTempFile("information-exception", ".txt");
        file.deleteOnExit();

        SimulationInformation info = new SimulationInformation(file);
        Assertions.assertFalse(info.hasError());
        
        Exception e = new Exception("Oula la, ya eu une exception super grave");
        info.setException(e);
        info.store();

        SimulationInformation testInfo = new SimulationInformation(file);
        Assertions.assertTrue(testInfo.hasError());
        Assertions.assertTrue(testInfo.getException().indexOf("grave") > 0);
        Assertions.assertTrue(testInfo.toString().indexOf("grave") > 0);

    }
    
    /**
     * Test simulation export time.
     * @throws IOException 
     */
    @Test
    public void testSimulationExportTime() throws IOException {
        File file = File.createTempFile("information-export", ".txt");
        file.deleteOnExit();

        SimulationInformation info = new SimulationInformation(file);
        info.addExportTime("export1", 30);
        info.addExportTime("export2", 40);
        info.addExportTime("export3", 50);
        info.addExportTime("export4", 60);
        
        info.store();

        SimulationInformation testInfo = new SimulationInformation(file);
        Assertions.assertFalse(testInfo.hasError());
        Assertions.assertEquals(30, testInfo.getExportTime("export1"));
        Assertions.assertEquals(40, testInfo.getExportTime("export2"));
        Assertions.assertEquals(50, testInfo.getExportTime("export3"));
        Assertions.assertEquals(60, testInfo.getExportTime("export4"));
        Assertions.assertTrue(testInfo.toString().indexOf("export1") > 0);
    }
    
    /**
     * Test simulation rule time.
     * 
     * @throws IOException 
     */
    @Test
    public void testSimulationRuleTime() throws IOException {
        File file = File.createTempFile("information-rule", ".txt");
        file.deleteOnExit();

        SimulationInformation info = new SimulationInformation(file);
        info.addRuleInitTime("rule1", 30);
        info.addRuleInitTime("rule2", 8);
        info.addRuleInitTime("rule3", 321);
        info.addRuleInitTime("rule4", 123);
        
        // time must be added
        info.addRulePreTime("rule1", 20);
        info.addRulePostTime("rule1", 25);
        info.addRulePreTime("rule1", 20);
        info.addRulePostTime("rule1", 25);
        
        info.addRulePreTime("rule2", 18);
        info.addRulePostTime("rule2", 500);
        
        info.addRulePreTime("rule3", 20);
        // no additional time for rule 4
        
        info.store();

        SimulationInformation testInfo = new SimulationInformation(file);
        Assertions.assertFalse(testInfo.hasError());
        Assertions.assertEquals(30, testInfo.getRuleInitTime("rule1"));
        Assertions.assertEquals(8, testInfo.getRuleInitTime("rule2"));
        Assertions.assertEquals(321, testInfo.getRuleInitTime("rule3"));
        Assertions.assertEquals(123, testInfo.getRuleInitTime("rule4"));
        
        Assertions.assertEquals(20 + 20, testInfo.getRulePreTime("rule1"));
        Assertions.assertEquals(25 + 25, testInfo.getRulePostTime("rule1"));
        
        Assertions.assertEquals(18, testInfo.getRulePreTime("rule2"));
        Assertions.assertEquals(500, testInfo.getRulePostTime("rule2"));
        
        Assertions.assertEquals(20, testInfo.getRulePreTime("rule3"));
        Assertions.assertEquals(0, testInfo.getRulePostTime("rule3"));
        
        Assertions.assertEquals(0, testInfo.getRulePreTime("rule4"));
        Assertions.assertEquals(0, testInfo.getRulePostTime("rule4"));

        // total time present rule2
        Assertions.assertTrue(testInfo.toString().indexOf("rule2 : 0.526") > 0);
    }
}
