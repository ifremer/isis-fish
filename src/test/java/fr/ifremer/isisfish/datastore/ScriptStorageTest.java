/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2012 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.datastore;

import fr.ifremer.isisfish.AbstractIsisFishTest;
import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.datastore.CodeSourceStorage.Location;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.StringWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * ScriptStorageTest.
 *
 * Created: 7 août 2006 11:07:57
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class ScriptStorageTest extends AbstractIsisFishTest {

    protected Configuration freemarkerConfiguration;

    @BeforeEach
    public void setUp() {
        freemarkerConfiguration = getFreemarkerConfiguration();
    }

    /*
     * Test method for 'fr.ifremer.isisfish.datastore.ScriptStorage.getScript(String)'
     */
    @Test
    public void testGetScript() throws Exception {
        String content1 = "public class test { public static void main(String[]args) {System.out.println(\"The test 1 :)\")} ";
        String content2 = "public class test { public static void main(String[]args) {System.out.println(\"The test 2 :) with different length :(\")} ";

        ScriptStorage script = ScriptStorage.createScript("test", Location.COMMUNITY);
        try {
            //System.out.println("File: " + script.getFile());
            //System.out.println("Content: '" + script.getContent() + "'");

            Assertions.assertFalse(script.exists());
            Assertions.assertEquals("", script.getContent());

            script.setContent(content1);
            Assertions.assertTrue(script.exists());

            //System.out.println("Content: '" + script.getContent() + "'");
            Assertions.assertEquals(content1, script.getContent());

            FileUtils.writeStringToFile(script.getFile(), content2, StandardCharsets.UTF_8);
            //System.out.println("Content: '" + script.getContent() + "'");
            Assertions.assertEquals(content2, script.getContent());
        } finally {
            script.delete(false);
            Assertions.assertFalse(script.exists());
        }
    }

    @Test
    public void testNewScriptWithCompilation() throws Exception {

        String fileName = "TestScript1";

        ScriptStorage scriptStorage = ScriptStorage.createScript(fileName, Location.COMMUNITY);

        // get template
        Template template = freemarkerConfiguration
                .getTemplate(ScriptStorage.SCRIPT_TEMPLATE);

        // context values
        Map<String, Object> root = new HashMap<>();
        root.put("name", fileName);
        root.put("date", new Date());
        root.put("author", IsisFish.config.getUserName());
        root.put("email", IsisFish.config.getUserMail());

        // process template
        Writer out = new StringWriter();
        template.process(root, out);
        out.flush();
        String content = out.toString();

        scriptStorage.setContent(content);

        // 0 = compile success
        int compileResult = scriptStorage.compile(false, null);
        Assertions.assertEquals(0, compileResult);
    }
}
