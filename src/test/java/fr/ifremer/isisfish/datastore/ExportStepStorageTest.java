/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2014 Ifremer, CodeLutin, Poussin Benjamin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.datastore;

import fr.ifremer.isisfish.AbstractIsisFishTest;
import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.datastore.CodeSourceStorage.Location;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.StringWriter;
import java.io.Writer;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * ExportStorageTest.
 * 
 * Created: 19 dec. 2014 18:22
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class ExportStepStorageTest extends AbstractIsisFishTest {

    protected Configuration freemarkerConfiguration;

    @BeforeEach
    public void setUp() {
        freemarkerConfiguration = getFreemarkerConfiguration();
    }

    @Test
    public void testNewExportWithCompilation() throws Exception {

        String fileName = "TestExport1";

        ExportStorage exportStorage = ExportStorage.createExport(fileName, Location.COMMUNITY);

        // get template
        Template template = freemarkerConfiguration
                .getTemplate(ExportStorage.EXPORT_TEMPLATE);

        // context values
        Map<String, Object> root = new HashMap<>();
        // FIXME what is category ?
        root.put("category", "");
        root.put("name", fileName);
        root.put("date", new Date());
        root.put("author", IsisFish.config.getUserName());
        root.put("email", IsisFish.config.getUserMail());

        // process template
        Writer out = new StringWriter();
        template.process(root, out);
        out.flush();
        exportStorage.setContent(out.toString());

        // 0 = compile success
        int compileResult = exportStorage.compile(false, null);
        Assertions.assertEquals(0, compileResult);
    }

}
