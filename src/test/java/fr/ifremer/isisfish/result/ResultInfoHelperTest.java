/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.result;

import fr.ifremer.isisfish.AbstractIsisFishTest;
import fr.ifremer.isisfish.IsisFishException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.Set;

public class ResultInfoHelperTest extends AbstractIsisFishTest {

    /**
     * Test qu'à partir d'un result, on arrive bien recursivement à en extraire les resultats
     * dépendant et sans boucle infinie.
     * 
     * @throws IsisFishException 
     */
    @Test
    public void testResultRecursiveExtract() throws IsisFishException {
        Set<String> necessaryResults = Collections.singleton("MatrixTestDep1");
        Set<String> allNecessaryResult = ResultInfoHelper.extractAllNecessaryResults(necessaryResults);
        
        Assertions.assertEquals(3, allNecessaryResult.size()); //MatrixTestDep1, MatrixTestDep2, MatrixTestDep3
    }

}
