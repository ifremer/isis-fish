/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2012 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.mexico.export;

import fr.ifremer.isisfish.AbstractIsisFishTest;
import fr.ifremer.isisfish.datastore.RegionStorage;
import fr.ifremer.isisfish.datastore.StorageException;
import fr.ifremer.isisfish.entities.FisheryRegion;
import fr.ifremer.isisfish.simulator.sensitivity.SensitivityUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * Test class for {@link RegionExplorer}.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class RegionExplorerTest extends AbstractIsisFishTest {

    /** Class logger. */
    private static Log log = LogFactory.getLog(RegionExplorerTest.class);

    /**
     * Explore all region values and perform export implementation as xml.
     * 
     * @throws TopiaException 
     * @throws StorageException 
     * @throws IOException 
     */
    @Test
    public void testExportRegionFactorsAsXml() throws TopiaException, StorageException, IOException {

        // get region to export
        RegionStorage regionStorage = RegionStorage.getRegion("BaseMotosICA");
        TopiaContext context = regionStorage.getStorage().beginTransaction();
        FisheryRegion fisheryRegion = RegionStorage.getFisheryRegion(context);

        // export implementation
        File file = File.createTempFile("xmlexport-", ".xml");
        file.deleteOnExit();
        RegionExportFactorXML xmlFactorExport = new RegionExportFactorXML(file);

        // explore region (export as xml)
        RegionExplorer explorer = new RegionExplorer();
        explorer.explore(fisheryRegion, xmlFactorExport);
        String xmlExport = FileUtils.readFileToString(file, StandardCharsets.UTF_8);

        if (log.isDebugEnabled()) {
            log.debug("Export xml is : \n" + xmlExport);
        }
        
        // ========== here some tests ==========
        // all factors must have a non null name
        Assertions.assertFalse(xmlExport.contains("name=\"null\""), "A factor has a null name");
        // TODO this assert is correct, for <factor name="">
        // but fail on <mx name="">
        //Assertions.assertTrue("A factor has no name", xmlExport.indexOf("name=\"\"") == -1);
        
        // must be present (for BaseMotosICA base)
        // equation is a special case (entity too)
        Assertions.assertTrue(xmlExport.contains("Population.Anchois_long.growth"), "An equation factor is missing");

        // close all
        context.closeContext();
    }
    
    /**
     * Test que toutes les entités et propriétés définies dans le fichier de
     * sensibilité sont présentes dans le contenu xml.
     * 
     * @throws TopiaException 
     * @throws StorageException 
     * @throws IOException 
     */
    @Test
    public void testAllEntityPresence() throws TopiaException, StorageException, IOException {

        // get region to export
        RegionStorage regionStorage = RegionStorage.getRegion("Golfe de Gascogne");
        TopiaContext context = regionStorage.getStorage().beginTransaction();
        FisheryRegion fisheryRegion = RegionStorage.getFisheryRegion(context);

        // export implementation
        File file = File.createTempFile("xmlexport-", ".xml");
        file.deleteOnExit();
        RegionExportFactorXML xmlFactorExport = new RegionExportFactorXML(file);

        // explore region (export as xml)
        RegionExplorer explorer = new RegionExplorer();
        explorer.explore(fisheryRegion, xmlFactorExport);
        String xmlExport = FileUtils.readFileToString(file, StandardCharsets.UTF_8);

        // test that all sensitivity properties appear in xml content
        // entity part and property part must appear in factors names
        for (String property : SensitivityUtils.getProperties().stringPropertyNames()) {
            String entityPart = property.substring(0, property.indexOf('.'));
            
            if ("Variable".equals(entityPart)) {
                // variable added since 4.1 and not yet present in test database
                continue;
            }

            Assertions.assertTrue(xmlExport.contains(entityPart), "Entity " + entityPart + " is not present in xml export");
            
            String propertyPart = property.substring(property.indexOf('.'));
            Assertions.assertTrue(xmlExport.contains(propertyPart), "Property " + property + " is not present in xml export");
        }

        // close all
        context.closeContext();
    }
}
