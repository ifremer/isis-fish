/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2011 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.mexico;

import fr.ifremer.isisfish.AbstractIsisFishTest;
import fr.ifremer.isisfish.IsisFishDAOHelper;
import fr.ifremer.isisfish.IsisFishException;
import fr.ifremer.isisfish.datastore.RegionStorage;
import fr.ifremer.isisfish.datastore.RuleStorage;
import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.entities.PopulationImpl;
import fr.ifremer.isisfish.entities.PopulationSeasonInfo;
import fr.ifremer.isisfish.entities.PopulationSeasonInfoImpl;
import fr.ifremer.isisfish.entities.Strategy;
import fr.ifremer.isisfish.entities.Zone;
import fr.ifremer.isisfish.rule.Rule;
import fr.ifremer.isisfish.rule.RuleHelper;
import fr.ifremer.isisfish.simulator.sensitivity.DesignPlan;
import fr.ifremer.isisfish.simulator.sensitivity.Distribution;
import fr.ifremer.isisfish.simulator.sensitivity.Factor;
import fr.ifremer.isisfish.simulator.sensitivity.FactorGroup;
import fr.ifremer.isisfish.simulator.sensitivity.domain.ContinuousDomain;
import fr.ifremer.isisfish.simulator.sensitivity.domain.DiscreteDomain;
import fr.ifremer.isisfish.simulator.sensitivity.domain.EquationDiscreteDomain;
import fr.ifremer.isisfish.simulator.sensitivity.domain.RuleDiscreteDomain;
import fr.ifremer.isisfish.types.TimeStep;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.nuiton.math.matrix.MatrixFactory;
import org.nuiton.math.matrix.MatrixHelper;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Test for MexicoHelper class.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update: $Date$ by :
 * $Author$
 */
public class MexicoHelperTest extends AbstractIsisFishTest {

    /** Commons logging log. */
    private static Log log = LogFactory.getLog(MexicoHelperTest.class);

    protected TopiaContext regionContext;

    @BeforeEach
    public void setUpRegion() {
        RegionStorage regionStorage = RegionStorage.getRegion("BaseMotosICA");
        regionContext = regionStorage.getStorage();
    }

    /**
     * Get a test design plan composed with all possibles value types.
     * 
     * @param use40Features build design plan with sub factor groups
     * @param use401Features build plan with distribution definition
     * @return a test design plan
     * @throws IsisFishException 
     * @throws TopiaException 
     */
    protected DesignPlan getTestDesignPlan(boolean use40Features, boolean use401Features) throws IsisFishException, TopiaException {
        DesignPlan designPlan = new DesignPlan();

        // matrix 1
        MatrixND matrix1 = MatrixFactory.getInstance().create("test1",
                new int[] { 3, 2 }, new String[] { "col1", "col2" });
        matrix1.setValue(new int[] { 0, 0 }, 13);
        matrix1.setValue(new int[] { 0, 1 }, -14);
        matrix1.setValue(new int[] { 1, 0 }, 21);
        matrix1.setValue(new int[] { 1, 1 }, 2);
        matrix1.setValue(new int[] { 2, 0 }, 12);
        matrix1.setValue(new int[] { 2, 1 }, -1);

        // rule 1, species = c'est une des especes de la base BaseMotosICA
        Rule ruleTacPoids = RuleStorage.getRule("TACpoids").getNewInstance();
        Properties props = new Properties();
        props.setProperty("rule.0.parameter.species", "fr.ifremer.isisfish.entities.Species#1169028645830#0.022262364425031378");
        RuleHelper.populateRule(0, regionContext, ruleTacPoids, props);

        // rule 2, species = c'est une des especes de la base BaseMotosICA
        Rule ruleTailleMin = RuleStorage.getRule("TailleMin").getNewInstance();
        props = new Properties();
        props.setProperty("rule.0.parameter.species", "fr.ifremer.isisfish.entities.Species#1169028645830#0.022262364425031378");
        props.setProperty("rule.0.parameter.propSurvie", "42");
        RuleHelper.populateRule(0, regionContext, ruleTailleMin, props);

        // factor 1
        Factor factorContinuous = new Factor("factor 1 (double continuous)");
        ContinuousDomain domain1 = new ContinuousDomain(Distribution.QUNIFMM);
        domain1.addDistributionParam("min", 0.0);
        domain1.addDistributionParam("max", 50.0);
        factorContinuous.setDomain(domain1);
        factorContinuous.setPath("fr.ifremer.isisfish.entities.PopulationGroup#1156461521013#0.1715620681984218#maxLength");

        // factor 2
        Factor factorDiscrete = new Factor("factor 2 (double discrete)");
        DiscreteDomain domain2 = new DiscreteDomain();
        domain2.getValues().put(0.0, 12.3);
        domain2.getValues().put(1.0, 70.9);
        domain2.getValues().put(2.0, 21.0);
        domain2.getValues().put(3.0, -12.1);
        domain2.getValues().put(4.0, -8.45);
        factorDiscrete.setDomain(domain2);
        factorDiscrete.setPath("fr.ifremer.isisfish.entities.PopulationGroup#1156461521064#0.022976136053553198#minLength");
        factorDiscrete.setValue(3.0);

        // factor 3
        Factor factorContinuous2 = new Factor("factor 3 (integer discrete)");
        DiscreteDomain domain3 = new DiscreteDomain();
        domain3.getValues().put(0.0, 13);
        domain3.getValues().put(1.0, 14);
        domain3.getValues().put(2.0, 45);
        factorContinuous2.setDomain(domain3);
        factorContinuous2.setPath("fr.ifremer.isisfish.entities.PopulationGroup#1156461521076#0.6526656643346673#minLength");

        // factor 4
        Factor factorMatrixContinuous = new Factor("factor 4 (MatrixContinuous)");
        ContinuousDomain domain4 = new ContinuousDomain(Distribution.QUNIFPC);
        domain4.addDistributionParam("reference", matrix1);
        domain4.addDistributionParam("coefficient", 0.799);
        factorMatrixContinuous.setDomain(domain4);
        factorMatrixContinuous.setPath("fr.ifremer.isisfish.entities.PopulationGroup#1156461521076#0.6526656643346673#minLength");

        // factor 5
        Factor factorEquationContinuous = new Factor("factor 5 (EquationContinuous)");
        factorEquationContinuous.setEquationVariableName("Ktemp");
        ContinuousDomain domain5 = new ContinuousDomain(Distribution.QUNIFPC);
        domain5.addDistributionParam("reference", 45.0);
        domain5.addDistributionParam("coefficient", 0.1);
        factorEquationContinuous.setDomain(domain5);
        factorEquationContinuous.setPath("fr.ifremer.isisfish.entities.PopulationGroup#1156461521076#0.6526656643346673#maxLength");

        // factor 1
        Factor factorContinuousPercentage = new Factor("factor 6 (double continuous percentage)");
        ContinuousDomain domain6 = new ContinuousDomain(Distribution.QUNIFPC);
        domain6.addDistributionParam("reference", 14.0);
        domain6.addDistributionParam("coefficient", 5.0);
        factorContinuousPercentage.setCardinality(5);
        factorContinuousPercentage.setDomain(domain6);
        factorContinuousPercentage.setPath("fr.ifremer.isisfish.entities.PopulationGroup#1142003453434#0.223499349929004#size");

        // 4.0.0.0
        if (use40Features) {

            // factor 7 : rule factor
            Factor factorRuleDiscrete = new Factor("factor 7 (RuleDiscrete)");
            RuleDiscreteDomain domain7 = new RuleDiscreteDomain();
            SortedMap<Object, Object> rules = new TreeMap<>();
            rules.put("ruleset1", Arrays.asList(ruleTacPoids, ruleTailleMin));
            rules.put("ruleset2", Arrays.asList(ruleTailleMin));
            domain7.setValues(rules);
            factorRuleDiscrete.setDomain(domain7);
            factorRuleDiscrete.setPath("parameters.rules");
            
            FactorGroup group1 = new FactorGroup("discretegrp");
            group1.addFactor(factorDiscrete);
            group1.addFactor(factorRuleDiscrete);
            FactorGroup group2 = new FactorGroup("continuousgrp", true);
            group2.addFactor(factorContinuous);
            group2.addFactor(factorMatrixContinuous);
            group2.addFactor(factorEquationContinuous);
            
            designPlan.addFactor(group1);
            designPlan.addFactor(group2);
            designPlan.addFactor(factorContinuous2);
            
            // factor 8 : discrete rule params factor
            Factor factorRuleParamsDiscrete = new Factor("factor 8 (RuleParamsDiscrete)");
            DiscreteDomain domain8 = new DiscreteDomain();
            SortedMap<Object, Object> values8 = new TreeMap<>();
            TopiaContext context = regionContext.beginTransaction();
            values8.put("1", IsisFishDAOHelper.getStrategyDAO(context).findByName("PelProfil1"));
            values8.put("2", IsisFishDAOHelper.getStrategyDAO(context).findByName("PelProfil2"));
            context.closeContext();
            domain8.setValues(values8);
            factorRuleParamsDiscrete.setDomain(domain8);
            designPlan.addFactor(factorRuleParamsDiscrete);
        }
        else {
            // 3.3.0.0
            designPlan.addFactor(factorContinuous);
            designPlan.addFactor(factorDiscrete);
            designPlan.addFactor(factorContinuous2);
            designPlan.addFactor(factorMatrixContinuous);
            designPlan.addFactor(factorEquationContinuous);
            designPlan.addFactor(factorContinuousPercentage);
        }

        return designPlan;
    }

    /**
     * Test method {@link MexicoHelper#getDesignPlanAsXML(DesignPlan, String...)}.
     * 
     * Sans groupe de facteur, tel qu'il était en 3.3.0.0.
     * 
     * @throws IOException
     * @throws IsisFishException 
     * @throws TopiaException 
     */
    @Test
    public void testGetDesignPlanAsXML() throws IOException, IsisFishException, TopiaException {

        DesignPlan testDesignPlan = getTestDesignPlan(false, false);

        // test xml export
        String xml = MexicoHelper.getDesignPlanAsXML(testDesignPlan);

        if (log.isDebugEnabled()) {
            log.debug("testGetDesignPlanAsXML xml = " + xml);
        }

        // factor 1
        Assertions.assertTrue(xml.contains("distributionParameter name=\"min\" type=\"decimal\">0.0"));
        Assertions.assertTrue(xml.contains("distributionParameter name=\"max\" type=\"decimal\">50.0"));
        // factor 2
        Assertions.assertTrue(xml.contains("name=\"factor 2 (double discrete)\""));
        Assertions.assertTrue(xml.contains("<level>70.9</level>"));
        // factor 3
        Assertions.assertTrue(xml.contains("<level>14</level>"));
        // factor 4
        Assertions.assertTrue(xml.contains("<d>-14.0</d>"));
        Assertions.assertTrue(xml.contains("0.799"));
        Assertions.assertTrue(xml.contains("<mx name=\"test1\">"));
        // factor 5
        Assertions.assertTrue(xml.contains("name=\"equationVariableName\">Ktemp"));
        Assertions.assertTrue(xml.contains("distributionParameter name=\"coefficient\" type=\"decimal\">0.1"));
        Assertions.assertTrue(xml.contains("distributionParameter name=\"reference\" type=\"decimal\">45.0"));
    }

    /**
     * Test method {@link MexicoHelper#getDesignPlanFromXML(File, TopiaContext)}.
     * 
     * Sans groupe de facteur.
     * 
     * @throws IOException
     */
    @Test
    public void testGetDesignPlanFromXML() throws IOException {

        File testFile = new File("src/test/resources/mexico/mexicohelper_designplan.xml");
        testFile = testFile.getAbsoluteFile(); // for maven

        // topia context can be null in tests
        DesignPlan plan = MexicoHelper.getDesignPlanFromXML(testFile, null);
        List<Factor> factors = plan.getFactors();

        Assertions.assertEquals(6, factors.size());

        String xml = MexicoHelper.getDesignPlanAsXML(plan);
        if (log.isDebugEnabled()) {
            log.debug("testGetDesignPlanFromXML xml = " + xml);
        }

        // assert on continuous factor (percentage, non percentage) (3.4.0.0)
        ContinuousDomain domain1 = (ContinuousDomain)factors.get(0).getDomain();
        Assertions.assertNull(domain1.getDistributionParameters().get("coefficient"));
        ContinuousDomain domain6 = (ContinuousDomain)factors.get(5).getDomain();
        Assertions.assertNotNull(domain6.getDistributionParameters().get("coefficient"));
    }

    /**
     * Test que l'export XML de l'import XML produise le meme xml.
     * 
     * @throws IOException
     * @throws IsisFishException 
     * @throws TopiaException 
     */
    @Test
    public void testExportImport() throws IOException, IsisFishException, TopiaException {

        Date date = new Date();

        // first export
        DesignPlan testDesignPlan = getTestDesignPlan(false, false);
        String xml1 = MexicoHelper.getDesignPlanAsXML(testDesignPlan, "date", date.toString());
        if (log.isDebugEnabled()) {
            log.debug("xml 1 = " + xml1);
        }

        // export
        File testFile = new File("src/test/resources/mexico/mexicohelper_designplan.xml");
        testFile = testFile.getAbsoluteFile(); // for maven

        // topia context can be null in tests
        DesignPlan plan = MexicoHelper.getDesignPlanFromXML(testFile, regionContext);
        String xml2 = MexicoHelper.getDesignPlanAsXML(plan, "date", date.toString());
        if (log.isDebugEnabled()) {
            log.debug("xml 2 = " + xml2);
        }
        Assertions.assertEquals(xml1, xml2);
    }

    /**
     * Test le chargement des groupes de facteurs et des facteurs de type
     * regles depuis isis 4.0.0.0.
     * 
     * @throws IOException
     * @throws IsisFishException 
     * @throws TopiaException 
     */
    @Test
    public void testFactorGroupAndRule() throws IOException, IsisFishException, TopiaException {
        
        DesignPlan testDesignPlan = getTestDesignPlan(true, false);

        // test xml export
        String xml = MexicoHelper.getDesignPlanAsXML(testDesignPlan);

        if (log.isDebugEnabled()) {
            log.debug("testFactorGroupAndRule xml = " + xml);
        }

        // factor groups
        Assertions.assertTrue(xml.contains("<feature name=\"group\">continuousgrp"));
        Assertions.assertTrue(xml.contains("<feature name=\"grouptype\">continuous"));
        Assertions.assertTrue(xml.contains("<feature name=\"group\">discretegrp"));
        Assertions.assertTrue(xml.contains("<feature name=\"group\">discrete"));

        // factor 6
        Assertions.assertTrue(xml.contains("<rule name=\"TACpoids\">"));
        Assertions.assertTrue(xml.contains("<rule name=\"TailleMin\">"));
        Assertions.assertTrue(xml.contains("<param key=\"rule.0.parameter.propSurvie\">42.0</param>"));
        Assertions.assertTrue(xml.contains("<param key=\"rule.0.parameter.species\">fr.ifremer.isisfish.entities.Species#1169028645830#0.022262364425031378:EngEnc</param>"));
    }
    
    /**
     * Test que l'export XML de l'import XML produise le meme xml.
     * 
     * @throws IOException
     * @throws IsisFishException 
     * @throws TopiaException 
     */
    @Test
    public void testExportImportWithGroup() throws IOException, IsisFishException, TopiaException {

        Date date = new Date();

        // first export
        DesignPlan testDesignPlan = getTestDesignPlan(true, false);
        String xml1 = MexicoHelper.getDesignPlanAsXML(testDesignPlan, "date", date.toString());
        if (log.isDebugEnabled()) {
            log.debug("xml 1 = " + xml1);
        }
        
        // export
        File testFile = new File("src/test/resources/mexico/mexicohelper_factorgroupdp.xml");
        testFile = testFile.getAbsoluteFile(); // for maven

        // topia context can be null in tests
        TopiaContext context = regionContext.beginTransaction();
        DesignPlan plan = MexicoHelper.getDesignPlanFromXML(testFile, context);
        context.closeContext();
        String xml2 = MexicoHelper.getDesignPlanAsXML(plan, "date", date.toString());
        if (log.isDebugEnabled()) {
            log.debug("xml 2 = " + xml2);
        }
        Assertions.assertEquals(xml1, xml2);
    }

    /**
     * Test le chargement des groupes de facteurs et des facteurs de type
     * entite (ou autre) sur les parametres de regles.
     * 
     * @throws IOException
     * @throws IsisFishException 
     * @throws TopiaException 
     */
    @Test
    public void testFactorRuleParams() throws IOException, IsisFishException, TopiaException {
        
        DesignPlan testDesignPlan = getTestDesignPlan(true, false);

        // test xml export
        String xml = MexicoHelper.getDesignPlanAsXML(testDesignPlan);

        if (log.isDebugEnabled()) {
            log.debug("testFactorRuleParams xml = " + xml);
        }

        // factor groups
        Assertions.assertTrue(xml.contains("type=\"string\""));
        
        // factor 8
        Assertions.assertTrue(xml.contains("<level>Strategy:PelProfil1</level>"));
        Assertions.assertTrue(xml.contains("<level>Strategy:PelProfil2</level>"));
    }

    /**
     * Test l'ajout des nouveaux type de facteurs, min/max et pourcentage.
     * 
     * @throws IOException
     * @throws IsisFishException
     * @throws TopiaException
     * 
     * This test may fail because of date delay by one seconds due to slow testing
     */
    //@Test
    public void testPercentageMinMaxFactor() throws IOException, IsisFishException, TopiaException {

        // get test plan and add some STRANGES factors
        DesignPlan testDesignPlan = getTestDesignPlan(false, false);
        
        // matrix 1
        MatrixND matrix1 = MatrixFactory.getInstance().create("test1",
                new int[] { 3, 2 }, new String[] { "col1", "col2" });
        matrix1 = MatrixHelper.convertToId(matrix1);
        
        // factor x1
        Factor factorContinuous = new Factor("factor x1");
        ContinuousDomain domain1 = new ContinuousDomain(Distribution.QUNIFPC);
        domain1.addDistributionParam("reference", 42.0);
        domain1.addDistributionParam("coefficient", 0.05);
        factorContinuous.setDomain(domain1);
        factorContinuous.setPath("fr.ifremer.isisfish.entities.PopulationGroup#1156461521013#0.1715620681984218#maxLength");

        // factor x4
        Factor factorMatrixContinuous = new Factor("factor x4");
        ContinuousDomain domain4 = new ContinuousDomain(Distribution.QUNIFMM);
        domain4.addDistributionParam("min", matrix1);
        domain4.addDistributionParam("max", matrix1);
        factorMatrixContinuous.setDomain(domain4);
        factorMatrixContinuous.setPath("fr.ifremer.isisfish.entities.PopulationGroup#1156461521076#0.6526656643346673#minLength");

        // factor x5
        Factor factorEquationContinuous = new Factor("factor x5");
        ContinuousDomain domain5 = new ContinuousDomain(Distribution.QUNIFMM);
        domain5.addDistributionParam("min", 40.0);
        domain5.addDistributionParam("max", 50.0);
        factorEquationContinuous.setEquationVariableName("Lx1");
        factorEquationContinuous.setDomain(domain5);
        factorEquationContinuous.setPath("fr.ifremer.isisfish.entities.PopulationGroup#1156461521076#0.6526656643346673#maxLength");
        
        testDesignPlan.addFactor(factorContinuous);
        testDesignPlan.addFactor(factorMatrixContinuous);
        testDesignPlan.addFactor(factorEquationContinuous);
        
        // test write
        String content = MexicoHelper.getDesignPlanAsXML(testDesignPlan);
        File tempFile = File.createTempFile("testdesignplan", ".xml");
        tempFile.deleteOnExit();
        FileUtils.writeStringToFile(tempFile, content, StandardCharsets.UTF_8);

        // test to read it and get content
        DesignPlan plan = MexicoHelper.getDesignPlanFromXML(tempFile, null);
        String reReadContent = MexicoHelper.getDesignPlanAsXML(plan);

        Assertions.assertEquals(content, reReadContent);
    }
    
    /**
     * Test l'ajout des nouveaux type de facteurs equation discrete.
     * 
     * @throws IOException
     * @throws IsisFishException
     * @throws TopiaException 
     */
    @Test
    public void testDiscreteEquationFactor() throws IOException, IsisFishException, TopiaException {

        // get test plan and add some STRANGES factors
        DesignPlan testDesignPlan = getTestDesignPlan(false, false);

        // factor x6
        Factor factorEquationDiscrete = new Factor("factor x6");
        EquationDiscreteDomain domain6 = new EquationDiscreteDomain();
        domain6.getValues().put("1", "if ( context.getValue() < 1.0) {\n\treturn 1.0;\n} else {\treturn context.getValue();\n}");
        domain6.getValues().put("2", "if ( context.getValue() < 1.666) {\n\treturn 1.666;\n} else {\treturn context.getValue();\n}");
        factorEquationDiscrete.setDomain(domain6);
        factorEquationDiscrete.setPath("fr.ifremer.isisfish.entities.PopulationGroup#1156461521076#0.6526656643346673#maxLength");

        testDesignPlan.addFactor(factorEquationDiscrete);

        Date date = new Date();
        // test write
        String content = MexicoHelper.getDesignPlanAsXML(testDesignPlan, "date", date.toString());
        Assertions.assertTrue(content.contains("context.getValue() &lt; 1.0"));

        File tempFile = File.createTempFile("testdesignplan", ".xml");
        tempFile.deleteOnExit();
        FileUtils.writeStringToFile(tempFile, content, StandardCharsets.UTF_8);

        // test to read it and get content
        DesignPlan plan = MexicoHelper.getDesignPlanFromXML(tempFile, null);
        String reReadContent = MexicoHelper.getDesignPlanAsXML(plan, "date", date.toString());
        
        Assertions.assertEquals(content, reReadContent);
    }
    
    /**
     * Test que l'export XML de l'import XML produise le meme xml.
     * Pareil, mais pour une version v2 du schema xml.
     * 
     * @throws IOException
     * @throws IsisFishException 
     * @throws TopiaException 
     */
    @Test
    public void testExportImportV2() throws IOException, IsisFishException, TopiaException {

        Date date = new Date();

        // first export
        DesignPlan testDesignPlan = getTestDesignPlan(false, false);
        String xml1 = MexicoHelper.getDesignPlanAsXML(testDesignPlan, "date", date.toString());
        if (log.isDebugEnabled()) {
            log.debug("xml 1 = " + xml1);
        }

        // export
        File testFile = new File("src/test/resources/mexico/mexicohelper_designplanV2.xml");
        testFile = testFile.getAbsoluteFile(); // for maven

        // topia context can be null in tests
        DesignPlan plan = MexicoHelper.getDesignPlanFromXML(testFile, regionContext);
        String xml2 = MexicoHelper.getDesignPlanAsXML(plan, "date", date.toString());
        if (log.isDebugEnabled()) {
            log.debug("xml 2 = " + xml2);
        }
        Assertions.assertEquals(xml1, xml2);
    }

    /**
     * Test la representation string d'un object dans le format mexico.
     */
    @Test
    public void testGetStringFromObject() {
        Population pop = new PopulationImpl();
        pop.setName("tutu");
        Assertions.assertEquals("Population:tutu", MexicoHelper.getStringFromObject(pop));

        PopulationSeasonInfo psi = new PopulationSeasonInfoImpl();
        Assertions.assertNull(MexicoHelper.getStringFromObject(psi));
        
        TimeStep step = new TimeStep(42);
        Assertions.assertEquals("TimeStep:42", MexicoHelper.getStringFromObject(step));
    }

    /**
     * Test que lors de la lecture du design plan, les types representés en string
     * seront correctement convertit en objets.
     * @throws TopiaException 
     */
    @Test
    public void testGetObjectFormString() throws TopiaException {
        TopiaContext context = regionContext.beginTransaction();
        
        Zone zone = (Zone)MexicoHelper.getObjectFromString("Zone:port", context);
        Assertions.assertEquals("port", zone.getName());
        
        Strategy strat = (Strategy)MexicoHelper.getObjectFromString("Strategy:PelProfil1", context);
        Assertions.assertEquals("PelProfil1", strat.getName());
        
        TimeStep step = (TimeStep)MexicoHelper.getObjectFromString("TimeStep:42", context);
        Assertions.assertEquals(42, step.getStep());
        
        context.closeContext();
    }

    /**
     * Test que le xml généré est valide avec la xsd mexico (exp design).
     * 
     * Test is currently disabled, isis fish doesn't respect all mexico
     * file format since it's not possible.
     * 
     * @throws Exception 
     */
    public void validOutputXml() throws Exception {

        DesignPlan testDesignPlan = getTestDesignPlan(true, true);
        String xml1 = MexicoHelper.getDesignPlanAsXML(testDesignPlan);
        URL schemaUrl = MexicoHelperTest.class.getResource("/mexico/expDesign.xsd");

        SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema =  factory.newSchema(schemaUrl);
        Validator validator = schema.newValidator();
        // at last perform validation:
        validator.validate(new StreamSource(new StringReader(xml1)));
    }

    /**
     * Test que l'export XML de l'import XML produise le meme xml.
     * Pareil, mais pour une version v3 du schema xml.
     * 
     * @throws IOException
     * @throws IsisFishException 
     * @throws TopiaException 
     */
    @Test
    public void testExportImportV3() throws IOException, IsisFishException, TopiaException {

        TopiaContext context = regionContext.beginTransaction();
        Date date = new Date();

        // first export
        DesignPlan testDesignPlan = getTestDesignPlan(true, true);
        String xml1 = MexicoHelper.getDesignPlanAsXML(testDesignPlan, "date", date.toString());
        if (log.isDebugEnabled()) {
            log.debug("xml 1 = " + xml1);
        }

        // export
        File testFile = new File("src/test/resources/mexico/mexicohelper_designplanV3.xml");
        testFile = testFile.getAbsoluteFile(); // for maven

        // topia context can be null in tests
        DesignPlan plan = MexicoHelper.getDesignPlanFromXML(testFile, context);
        String xml2 = MexicoHelper.getDesignPlanAsXML(plan, "date", date.toString());
        if (log.isDebugEnabled()) {
            log.debug("xml 2 = " + xml2);
        }
        Assertions.assertEquals(xml1, xml2);
        
        context.closeContext();
    }
}
