package fr.ifremer.isisfish.util;

/*-
 * #%L
 * ISIS-Fish
 * %%
 * Copyright (C) 2020 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.isisfish.entities.Season;
import fr.ifremer.isisfish.entities.SeasonImpl;
import fr.ifremer.isisfish.types.Month;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;

public class IsisUtilTest {

    @Test
    public void isSeasonOverlap() {
        Season janvierJanvier = new SeasonImpl();
        Season fevrierMars = new SeasonImpl();
        fevrierMars.setFirstMonth(Month.FEBRUARY);
        fevrierMars.setLastMonth(Month.MARCH);
        Season avrilNovembre = new SeasonImpl();
        avrilNovembre.setFirstMonth(Month.APRIL);
        avrilNovembre.setLastMonth(Month.NOVEMBER);
        Season juilletAout = new SeasonImpl();
        juilletAout.setFirstMonth(Month.JULY);
        juilletAout.setLastMonth(Month.AUGUST);
        Season decembreJanvier = new SeasonImpl();
        decembreJanvier.setFirstMonth(Month.DECEMBER);
        decembreJanvier.setLastMonth(Month.JANUARY);

        Collection<Season> test1 = Arrays.asList(janvierJanvier, janvierJanvier);
        assertThat(IsisUtil.isSeasonOverlap(test1)).isTrue();
        Collection<Season> test2 = Arrays.asList(avrilNovembre, decembreJanvier);
        assertThat(IsisUtil.isSeasonOverlap(test2)).isFalse();
        Collection<Season> test3 = Arrays.asList(avrilNovembre, juilletAout);
        assertThat(IsisUtil.isSeasonOverlap(test3)).isTrue();
        Collection<Season> test4 = Arrays.asList(janvierJanvier, fevrierMars, avrilNovembre);
        assertThat(IsisUtil.isSeasonOverlap(test4)).isFalse();
        Collection<Season> test5 = Arrays.asList(janvierJanvier, decembreJanvier);
        assertThat(IsisUtil.isSeasonOverlap(test5)).isTrue();
    }
}
