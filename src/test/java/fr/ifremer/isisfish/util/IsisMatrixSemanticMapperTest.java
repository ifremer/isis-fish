/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2010 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.util;

import fr.ifremer.isisfish.AbstractIsisFishTest;
import fr.ifremer.isisfish.IsisFishDAOHelper;
import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.simulator.SimulationContext;
import fr.ifremer.isisfish.types.TimeStep;
import fr.ifremer.isisfish.util.matrix.IsisMatrixSemanticMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.nuiton.math.matrix.MatrixFactory;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Test que le semantic mapper est bien pris en compte lors de l'import
 * de matrix nd et qu'il converti bien tous les types requis.
 * 
 * @author echatellier
 * @since 4.1.0.3
 */
public class IsisMatrixSemanticMapperTest extends AbstractIsisFishTest {

    @BeforeEach
    public void setUp() throws IOException, TopiaException {
        MatrixFactory.setSemanticMapper(new IsisMatrixSemanticMapper());
        URL zipURL = this.getClass().getResource("/simulations/test-nonregression-20090203.zip");
        SimulationStorage simRef = SimulationStorage.importAndRenameZip(new File(zipURL.getFile()), "test-" + System.currentTimeMillis());
        SimulationContext.get().setSimulationStorage(simRef);
    }

    @AfterEach
    public void clearThreadLocal() {
        SimulationContext.remove();
    }

    /**
     * Test la conversion des entités.
     * @throws IOException 
     * @throws TopiaException 
     */
    @Test
    public void testImportEntityImportString() throws IOException, TopiaException {
        String data = "[1, 1, 1]\n" +
                "Population:test population\n" +
                "Gear:Testengin\n" +
                "Zone:Zone test1\n" +
                "0;0;0;42.0";

        TopiaContext tx = SimulationContext.get().getDB();
        
        List[] sems = new List[] {
                Collections.singletonList(IsisFishDAOHelper.getPopulationDAO(tx).findByName("test population")),
                Collections.singletonList(IsisFishDAOHelper.getGearDAO(tx).findByName("Testengin")),
                Collections.singletonList(IsisFishDAOHelper.getZoneDAO(tx).findByName("Zone test1")),
        };
        MatrixND m = MatrixFactory.getInstance().create(sems);
        m.importCSV(new StringReader(data), null);

        Assertions.assertEquals(42.0, m.getValue(0, 0, 0), 0.001);
    }

    /**
     * Test la conversion des entités.
     */
    @Test
    public void testImportEntity() throws IOException, TopiaException {
        String data = "[1, 1, 1]\n" +
                "Population:test population\n" +
                "Gear:Testengin\n" +
                "Zone:Zone test1\n" +
                "0;0;0;42.0";

        TopiaContext tx = SimulationContext.get().getDB();
        
        List[] sems = new List[] {
                Collections.singletonList(IsisFishDAOHelper.getPopulationDAO(tx).findByName("test population")),
                Collections.singletonList(IsisFishDAOHelper.getGearDAO(tx).findByName("Testengin")),
                Collections.singletonList(IsisFishDAOHelper.getZoneDAO(tx).findByName("Zone test1")),
        };
        MatrixND m = MatrixFactory.getInstance().create(sems);
        m.importCSV(new StringReader(data), null);

        Assertions.assertEquals(42.0, m.getValue(0, 0, 0), 0.001);
    }
    
    /**
     * Test la conversion des entités et TimeStep
     */
    @Test
    public void testImportEntityTimeStep() throws IOException, TopiaException {
        String data = "[1, 1, 2]\n" +
                "Population:test population\n" +
                "Gear:Testengin\n" +
                "TimeStep:1,0\n" +
                "0;0;0;1.0\n" +
                "0;0;1;2.0";

        TopiaContext tx = SimulationContext.get().getDB();
        
        List[] sems = new List[] {
                Collections.singletonList(IsisFishDAOHelper.getPopulationDAO(tx).findByName("test population")),
                Collections.singletonList(IsisFishDAOHelper.getGearDAO(tx).findByName("Testengin")),
                Arrays.asList(new TimeStep(0), new TimeStep(1)),
        };
        MatrixND m = MatrixFactory.getInstance().create(sems);
        m.importCSV(new StringReader(data), null);

        Assertions.assertEquals(2.0, m.getValue(0, 0, 0), 0.001);
        Assertions.assertEquals(1.0, m.getValue(0, 0, 1), 0.001);
    }
    
    /**
     * Test la conversion des entités goup et population season info.
     */
    @Test
    public void testImportPopulationGroupAndSeason() throws IOException, TopiaException {
        String data = "[1, 3, 2]\n" +
                "Population:test population\n" +
                "PopulationGroup:test population+2,test population+1,test population+0\n" +
                "PopulationSeasonInfo:test population+0+0,test population+1+11\n" +
                "0;2;0;42.0";

        TopiaContext tx = SimulationContext.get().getDB();

        Population pop = IsisFishDAOHelper.getPopulationDAO(tx).findByName("test population");
        List[] sems = new List[] {
                Collections.singletonList(pop),
                Arrays.asList(pop.getPopulationGroup().get(0), pop.getPopulationGroup().get(1), pop.getPopulationGroup().get(2)),
                Arrays.asList(pop.getPopulationSeasonInfo().get(0), pop.getPopulationSeasonInfo().get(1)),
        };
        MatrixND m = MatrixFactory.getInstance().create(sems);
        m.importCSV(new StringReader(data), null);

        Assertions.assertEquals(42.0, m.getValue(0, 0, 0), 0.001);
    }
}
