/*
 * #%L
 * IsisFish
 *
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2020 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.util.converter;

import fr.ifremer.isisfish.AbstractIsisFishTest;
import fr.ifremer.isisfish.IsisFishDAOHelper;
import fr.ifremer.isisfish.datastore.RegionStorage;
import fr.ifremer.isisfish.entities.Zone;
import fr.ifremer.isisfish.entities.ZoneDAO;
import org.apache.commons.beanutils.ConvertUtilsBean;
import org.junit.jupiter.api.Test;
import org.nuiton.topia.TopiaContext;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class ConverterUtilTest extends AbstractIsisFishTest {

    /**
     * Test la conversion des collections d'entités.
     */
    @Test
    public void testConvertionListToString() {
        RegionStorage regionStorage = RegionStorage.getRegion("BaseMotosICA");
        TopiaContext regionContext = regionStorage.getStorage();
        TopiaContext tx = regionContext.beginTransaction();
        ConvertUtilsBean converter = ConverterUtil.getConverter(tx);
        ZoneDAO zoneDAO = IsisFishDAOHelper.getZoneDAO(tx);

        // test List
        List<Zone> zones = zoneDAO.findAll().subList(0, 3);
        String convert = converter.convert(zones);
        assertThat(convert).contains("fr.ifremer.isisfish.entities.Zone#1169028645759#0.8226513521396226");
        assertThat(convert).contains("fr.ifremer.isisfish.entities.Zone#1169028645760#0.4239734371131064");
        assertThat(convert).contains("fr.ifremer.isisfish.entities.Zone#1169028645761#0.20927318168797204");

        // test Set
        Set<Zone> zones2 = new HashSet<>(zones);
        String convert2 = converter.convert(zones2);
        assertThat(convert2).contains("fr.ifremer.isisfish.entities.Zone#1169028645759#0.8226513521396226");
        assertThat(convert2).contains("fr.ifremer.isisfish.entities.Zone#1169028645760#0.4239734371131064");
        assertThat(convert2).contains("fr.ifremer.isisfish.entities.Zone#1169028645761#0.20927318168797204");
    }

    @Test
    public void testConvertionStringToList() {
        String test = "fr.ifremer.isisfish.entities.Zone#1169028645759#0.8226513521396226:SenneBreton,fr.ifremer.isisfish.entities.Zone#1169028645760#0.4239734371131064,fr.ifremer.isisfish.entities.Zone#1169028645761#0.20927318168797204:Rochebonne";

        RegionStorage regionStorage = RegionStorage.getRegion("BaseMotosICA");
        TopiaContext regionContext = regionStorage.getStorage();
        TopiaContext tx = regionContext.beginTransaction();
        ZoneDAO zoneDAO = IsisFishDAOHelper.getZoneDAO(tx);
        ConvertUtilsBean converter = ConverterUtil.getConverter(tx);

        // zone attendues
        List<Zone> expectedZones = zoneDAO.findAll().subList(0, 3);

        // test Collection
        Collection<Zone> convert = (Collection<Zone>)converter.convert(test, Collection.class);
        assertThat(convert).containsExactlyElementsOf(expectedZones);

        // test List
        List<Zone> convert2 = (List<Zone>)converter.convert(test, List.class);
        assertThat(convert2).containsExactlyElementsOf(expectedZones);

        // test Set
        Set<Zone> convert3 = (Set<Zone>)converter.convert(test, Set.class);
        assertThat(convert3).containsExactlyElementsOf(expectedZones);
    }
}
