/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.util;

import fr.ifremer.isisfish.AbstractIsisFishTest;
import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.IsisFishException;
import fr.ifremer.isisfish.datastore.JavaSourceStorage;
import fr.ifremer.isisfish.datastore.RuleStorage;
import fr.ifremer.isisfish.datastore.ScriptStorage;
import fr.ifremer.isisfish.datastore.SimulatorStorage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Set;

/**
 * Test concernant la gestion du bytecode des scripts utilisateur.
 */
public class DependencyUtilTest extends AbstractIsisFishTest {

    /**
     * Extract dependency for test.
     * 
     * @param storage storage
     * @return deps
     * @throws IsisFishException 
     */
    protected static Set<String> extractDependencies(JavaSourceStorage storage) throws IsisFishException {
        String clazzName = storage.getCodeClass().getName();
        File classFile = new File(IsisFish.config.getCompileDirectory(), clazzName.replace('.', File.separatorChar) + ".class");
        Set<String> result = DependencyUtil.extractDependencies(IsisFish.config.getCompileDirectory(), Collections.singletonList(classFile));
        return result;
    }

    /**
     * Récupère une classe parmis les scripts de test et vérifie que les dépendances sont bien
     * extraites.
     * 
     * @throws IsisFishException 
     * @throws IOException 
     */
    @Test
    public void testExtractDependencies1() throws IsisFishException, IOException {
        SimulatorStorage script = SimulatorStorage.getSimulator("SimulatorEffortByCell");
        Set<String> deps = extractDependencies(script);
        // premier niveau
        Assertions.assertTrue(deps.contains("simulators.DefaultSimulator"));
        // deuxieme niveau de dependance
        Assertions.assertTrue(deps.contains("scripts.SiMatrix"));
        Assertions.assertTrue(deps.contains("scripts.GravityModel"));
        // troisieme niveau
        Assertions.assertTrue(deps.contains("scripts.MinimisationUtil"));
        Assertions.assertTrue(deps.contains("scripts.ObjectiveFunction"));
        Assertions.assertTrue(deps.contains("scripts.ObjectiveFunctionBaranov"));
    }
    
    /**
     * Récupère une classe parmis les scripts de test et vérifie que les dépendances sont bien
     * extraites.
     * 
     * @throws IsisFishException 
     * @throws IOException 
     */
    @Test
    public void testExtractDependencies2() throws IsisFishException, IOException {
        SimulatorStorage script = SimulatorStorage.getSimulator("DefaultSimulator");
        Set<String> deps = extractDependencies(script);
        Assertions.assertTrue(deps.contains("scripts.GravityModel"));
        Assertions.assertTrue(deps.contains("resultinfos.MatrixBiomass"));
        Assertions.assertTrue(deps.contains("scripts.SiMatrix"));
        
    }
    
    /**
     * Récupère une classe parmis les scripts de test et vérifie que les dépendances sont bien
     * extraites.
     * 
     * @throws IsisFishException 
     * @throws IOException 
     */
    @Test
    public void testExtractDependencies3() throws IsisFishException, IOException {
        RuleStorage script = RuleStorage.getRule("Cantonnement");
        Set<String> deps = extractDependencies(script);
        Assertions.assertTrue(deps.contains("scripts.ResultName"));
    }
    
    /**
     * Récupère une classe parmis les scripts de test et vérifie que les dépendances sont bien
     * extraites.
     * 
     * @throws IsisFishException 
     * @throws IOException 
     */
    @Test
    public void testExtractDependencies4() throws IsisFishException, IOException {
        ScriptStorage script = ScriptStorage.getScript("SiMatrix");
        Set<String> deps = extractDependencies(script);
        Assertions.assertTrue(deps.contains("scripts.ObjectiveFunctionBaranov"));
        Assertions.assertTrue(deps.contains("scripts.ObjectiveFunction"));
        Assertions.assertTrue(deps.contains("scripts.MinimisationUtil"));
        Assertions.assertTrue(deps.contains("resultinfos.MatrixBiomass"));
    }
    
    /**
     * Récupère une classe parmis les scripts de test et vérifie que les dépendances sont bien
     * extraites.
     * 
     * @throws IsisFishException 
     * @throws IOException 
     */
    @Test
    public void testExtractDependencies5() throws IsisFishException, IOException {
        ScriptStorage script = ScriptStorage.getScript("GravityModel");
        Set<String> deps = extractDependencies(script);
        Assertions.assertTrue(deps.contains("scripts.SiMatrix"));
        Assertions.assertTrue(deps.contains("resultinfos.MatrixBiomass"));
    }
    
    /**
     * Récupère une classe parmis les scripts de test et vérifie que les dépendances sont bien
     * extraites.
     * 
     * @throws IsisFishException 
     * @throws IOException 
     */
    @Test
    public void testExtractDependencies6() throws IsisFishException, IOException {
        ScriptStorage script = ScriptStorage.getScript("ObjectiveFunctionBaranov");
        Set<String> deps = extractDependencies(script);
        Assertions.assertTrue(deps.contains("scripts.ObjectiveFunction"));
    }
    
    /**
     * Test que les inners types (anonyme $1) ne sont pas extrait.
     * 
     * @throws IsisFishException 
     * @throws IOException 
     */
    @Test
    public void testExtractDependencies7() throws IsisFishException, IOException {
        RuleStorage script = RuleStorage.getRule("GraviteVPUE1");
        Set<String> deps = extractDependencies(script);
        Assertions.assertFalse(deps.contains("rules.GraviteVPUE1$1"));
    }
    
    /**
     * Test que les classes de résultats sont bien extraites en incluant les dépendances
     * récursives entre les résultats.
     * 
     * @throws IsisFishException 
     * @throws IOException 
     */
    @Test
    public void testExtractDependencies8() throws IsisFishException, IOException {
        RuleStorage script = RuleStorage.getRule("Cantonnement");
        Set<String> deps = extractDependencies(script);
        Assertions.assertTrue(deps.contains("resultinfos.MatrixTestDep1"));
        Assertions.assertTrue(deps.contains("resultinfos.MatrixTestDep2"));
        Assertions.assertTrue(deps.contains("resultinfos.MatrixTestDep3"));
    }
}
