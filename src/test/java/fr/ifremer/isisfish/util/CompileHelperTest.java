/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2010 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.util;

import fr.ifremer.isisfish.AbstractIsisFishTest;
import fr.ifremer.isisfish.annotations.Args;
import fr.ifremer.isisfish.entities.FisheryRegion;
import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.entities.Zone;
import fr.ifremer.isisfish.equation.PopulationReproductionEquation;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.topia.TopiaContext;
import org.nuiton.util.FileUtil;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * CompileHelperTest.
 * 
 * Created: 12 janv. 2006 16:20:33
 * 
 * @author poussin
 * 
 * @version $Revision$
 * 
 * Last update: $Date$
 * by : $Author$
 */
public class CompileHelperTest extends AbstractIsisFishTest {

    /** Class logger */
    private static Log log = LogFactory.getLog(CompileHelperTest.class);

    /**
     * Return a Java code content in Java 6.
     * 
     * @param name Class name
     * 
     * @return java code
     */
    protected String getFirstClassContent(String name) {
        StringBuilder content = new StringBuilder();
        content.append("public class " + name + " {");
        content.append("    /** Java 6 code */");
        content.append("    public boolean isEmpty(String s) {");
        content.append("        return s.isEmpty();");
        content.append("    }");
        content.append("}");
        return content.toString();
    }

    /**
     * Return a Java code content in Java 6 that extends a class.
     * 
     * @param name
     * @param extendsName
     * @return java code
     */
    protected String getSecondClassContent(String name, String extendsName) {
        StringBuilder content = new StringBuilder();
        content.append("public class " + name + " extends " + extendsName + " {");
        content.append("    @Override");
        content.append("    public boolean isEmpty(String s) {");
        content.append("        return false;");
        content.append("    }");
        content.append("}");
        return content.toString();
    }

    /**
     * Test une compilation de classe et une
     * instantiation de la classe compilée.
     * 
     * @throws IOException 
     * @throws ClassNotFoundException 
     */
    @Test
    public void testCompile() throws IOException, ClassNotFoundException {
        File f = File.createTempFile("testCompile", ".java", getTestDirectory());
        String filename = FileUtil.basename(f, ".java");
        String code = getFirstClassContent(filename);
        FileUtils.writeStringToFile(f, code, StandardCharsets.UTF_8);

        List<File> classpath = new ArrayList<>();
        classpath.add(f.getParentFile());

        CompileHelper.compile(classpath, Collections.singletonList(f), f
                .getParentFile(), null);

        // essai de chargement de la classe
        URL[] cp = new URL[] { f.getParentFile().toURI().toURL() };
        try (URLClassLoader cl = new URLClassLoader(cp)) {
            Class<?> c = cl.loadClass(filename);
            log.info("class name: " + c.getName());
            Assertions.assertEquals(filename, c.getName());
        }

        // delete
        File fclass = new File(f.getAbsolutePath().replaceAll("java$", "class"));
        fclass.delete();
        f.delete();
    }

    /**
     * Test de compiler deux classes dont l'un etend l'autre.
     * Puis instancie les deux.
     * @throws IOException 
     * @throws ClassNotFoundException 
     */
    @Test
    public void testCompileDepend() throws IOException, ClassNotFoundException {

        File fA = File.createTempFile("testCompileA", ".java", getTestDirectory());
        String filenameA = FileUtil.basename(fA, ".java");
        String codeA = getFirstClassContent(filenameA);
        FileUtils.writeStringToFile(fA, codeA, StandardCharsets.UTF_8);

        File fB = File.createTempFile("testCompileB", ".java", getTestDirectory());
        String filenameB = FileUtil.basename(fB, ".java");
        String codeB = getSecondClassContent(filenameB, filenameA);
        FileUtils.writeStringToFile(fB, codeB, StandardCharsets.UTF_8);

        File dest = new File(fB.getParentFile(), "testCompile");

        List<File> classpath = new ArrayList<>();
        classpath.add(fB.getParentFile());
        int result = CompileHelper.compile(classpath, Collections.singletonList(fB), dest, null);
        Assertions.assertEquals(0, result);

        // essai de chargement de la classe
        URL[] cp = new URL[] { dest.toURI().toURL() };
        try (URLClassLoader cl = new URLClassLoader(cp)) {
            Class<?> c = cl.loadClass(filenameB);
            log.info("class name: " + c.getName());
            Assertions.assertEquals(filenameB, c.getName());
        }

        // essai de chargement de la classe
        cp = new URL[] { dest.toURI().toURL() };
        //URL[] cp = new URL[] { dest.toURL() };
        try (URLClassLoader cl = new URLClassLoader(cp)) {
            Class<?> c = cl.loadClass(filenameA);
            log.info("class name: " + c.getName());
            Assertions.assertEquals(filenameA, c.getName());
        }

        // delete
        File fclassA = new File(fA.getAbsolutePath().replaceAll("java$",
                "class"));
        fclassA.delete();
        File fclassB = new File(fB.getAbsolutePath().replaceAll("java$",
                "class"));
        fclassB.delete();
        fA.delete();
        fB.delete();
        FileUtils.deleteQuietly(dest);
    }

    /**
     * Test que le contenu du fichier java correspondant à l'equation est
     * correctement généré.
     * Notemment suite à l'utilsation de paranamer et aux retrait des
     * annotations.
     */
    @Test
    public void testExtractDoc() {
        String content = CompileHelper.extractDoc("PopulationReproduction", "Test", PopulationReproductionEquation.class);
        content = content.replaceAll(" href='[^']+'", "");
        assertThat(content).contains("<td>N</td>","<td><a>MatrixND</a></td>", "<td>Effectif de la population", "<strong>Sémantiques : </strong> <a>PopulationGroup</a>, <a>Zone</a></td>");
        assertThat(content).contains("<td>zones</td>","<td><a>List</a>&lt;<a>Zone</a>&gt;</td>","<td>La liste des zones de la population (dimension 1 de N)</td>");
        assertThat(content).contains("la valeur retournée n'est pas utilisée");
        assertThat(content).contains("return 0.0;");
    }

    interface TestDocRecurvsive {
        @Args({"onegen", "twogen", "twogenrec"})
        double compute(List<Population> onegen, Map<Population, Zone> twogen, Map<Map<TopiaContext, Zone>, Map<FisheryRegion, MatrixND>> twogenrec);
    }

    /**
     * Test que le contenu du fichier java correspondant à l'equation est
     * correctement généré.
     * Notemment suite à l'utilsation de paranamer et aux retrait des
     * annotations.
     */
    @Test
    public void testExtractDocRecursive() {
        String content = CompileHelper.extractDoc("PopulationReproduction", "Test", TestDocRecurvsive.class);
        content = content.replaceAll(" href='[^']+'", "");
        assertThat(content).contains("<a>PopulationReproduction</a> - Test");
        assertThat(content).contains("<td>onegen</td>","<td><a>List</a>&lt;<a>Population</a>&gt;</td>");
        assertThat(content).contains("<td>twogen</td>","<td><a>Map</a>&lt;<a>Population</a>, <a>Zone</a>&gt;</td>");
        assertThat(content).contains("<td>twogenrec</td>","<td><a>Map</a>&lt;<a>Map</a>&lt;<a>TopiaContext</a>, <a>Zone</a>&gt;, <a>Map</a>&lt;<a>FisheryRegion</a>, <a>MatrixND</a>&gt;&gt;</td>");
    }
}
