/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2018 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.util;

import fr.ifremer.isisfish.AbstractIsisFishTest;
import fr.ifremer.isisfish.equation.EmigrationEquation;
import fr.ifremer.isisfish.equation.PopulationGrowth;
import fr.ifremer.isisfish.equation.PopulationGrowthReverse;
import fr.ifremer.isisfish.equation.PopulationReproductionEquation;
import fr.ifremer.isisfish.simulator.SimulationContext;
import fr.ifremer.isisfish.simulator.SimulationPreScript;
import fr.ifremer.isisfish.simulator.SimulationPreScriptListener;
import fr.ifremer.isisfish.simulator.SimulationPreScriptTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.nuiton.topia.TopiaContext;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test class for {@link EvaluatorHelper}.
 *
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class EvaluatorHelperTest extends AbstractIsisFishTest {

    /**
     * Test #normalizeClassName().
     */
    @Test
    public void testNormalizeClassName() {
        Assertions.assertEquals("Test", EvaluatorHelper.normalizeClassName("Test"));
        Assertions.assertEquals("Te_t", EvaluatorHelper.normalizeClassName("Te/t"));
        Assertions.assertEquals("org_test_Test", EvaluatorHelper.normalizeClassName("org.test.Test"));
    }

    /**
     * Test #check().
     */
    @Test
    public void testCheck() {
        String equationContent = "return 12.0;";

        int result = EvaluatorHelper.check(EmigrationEquation.class,
                equationContent, null);
        Assertions.assertEquals(0, result);
    }

    /**
     * Test #evaluate().
     */
    @Test
    public void testEvaluate() {
        String equationContent = "return age * 2.0;";

        Object result = EvaluatorHelper.evaluate("fr.isisfish.equation",
                "TestEvaluate", PopulationGrowth.class, equationContent, /*context*/null, /*length*/2.0, /*group*/null);

        Assertions.assertNotNull(result);
        Assertions.assertEquals("4.0", result.toString());
    }

    /**
     * Test #evaluate() but twice to check that previous
     * class don't stay in class loader.
     */
    @Test
    public void testDoubleContentEvaluate() {

        Map<String, Object> args = new HashMap<>();
        args.put("context", null);
        args.put("length", 2.0);
        args.put("group", null);

        String equationContent = "return length * 2.0;";
        Object result = EvaluatorHelper.evaluate("fr.isisfish.equation",
                "TestDoubleContentEvaluate", PopulationGrowthReverse.class,
                equationContent, /*context*/null, /*length*/2.0, /*group*/null);
        Assertions.assertNotNull(result);
        Assertions.assertEquals("4.0", result.toString());

        equationContent = "return length * 3.0;";
        result = EvaluatorHelper.evaluate("fr.isisfish.equation",
                "TestDoubleContentEvaluate", PopulationGrowthReverse.class,
                equationContent, /*context*/null, /*length*/2.0, /*group*/null);
        Assertions.assertNotNull(result);
        Assertions.assertEquals("6.0", result.toString());
    }
    
    /**
     * Test que le repertoire des scripts en bien présent
     * dans le classpath de compilation.
     */
    @Test
    public void testEquationWithDatabaseContent() {

        String equationContent = "String name = scripts.ResultName.MATRIX_CATCH_WEIGHT_PER_STRATEGY_MET_PER_ZONE_POP;";
        equationContent += "return length * 2.0;";
        Object result = EvaluatorHelper.evaluate("fr.isisfish.equation",
                "TestEquationWithDatabaseContent", PopulationGrowthReverse.class,
                equationContent, /*context*/null, /*length*/2.0, /*group*/null);
        Assertions.assertNotNull(result);
        Assertions.assertEquals("4.0", result.toString());
    }
    
    /**
     * Test that commons logging logger in present for use in equation.
     */
    @Test
    public void testEquationWithLogger() {

        String equationContent = "log.info(\"Test logger\");";
        equationContent += "return 1.0;";
        Object result = EvaluatorHelper.evaluate("fr.isisfish.equation",
                "TestEquationWithDatabaseContent", PopulationGrowthReverse.class,
                equationContent, /*context*/null, /*length*/2.0, /*group*/null);
        Assertions.assertNotNull(result);
        Assertions.assertEquals("1.0", result.toString());
    }

    /**
     * Test que le contenu du fichier java correspondant à l'equation est
     * corectement généré.
     * Notemment suite à l'utilsation de paranamer et aux retrait des
     * annotations.
     */
    @Test
    public void testGenerateContent() {
        Class clazz = PopulationReproductionEquation.class;
        Method method = clazz.getDeclaredMethods()[0];
        String content = EvaluatorHelper.generateContent("fr.ifremer.isisfish.equation",
                "Test", clazz, method, "return 42.0;");

        assertThat(content).contains("fr.ifremer.isisfish.simulator.SimulationContext context");
        assertThat(content).contains("java.util.List<fr.ifremer.isisfish.entities.PopulationGroup> groups");
    }

    /**
     * Test d'execution d'un prescript.
     */
    @Test
    public void testPrescriptContent() {
        String content = "if (context != null) {\n" +
                "  // test\n" +
                "}\n";
        EvaluatorHelper.evaluate("fr.isisfish.prescript",
                "TestPrescript", SimulationPreScript.class, content, /*context*/SimulationContext.get(), /*db*/null);
    }

    /**
     * Test d'extraction des imports.
     */
    @Test
    public void testImportContent() {
        Class clazz = PopulationReproductionEquation.class;
        Method method = clazz.getDeclaredMethods()[0];
        String content = "import java.lang.String;\nimport  java.lang.StringBuffer ;\n" +
                "return 0;";
        content = EvaluatorHelper.generateContent("fr.ifremer.isisfish.equation", "Test", clazz, method, content);

        assertThat(content).contains("import java.lang.String;"); // still present
        assertThat(content).contains("import  java.lang.StringBuffer ;"); // still present
        assertThat(content).contains("{\n\nreturn 0;\n}"); // but removed from content
    }

    /**
     * Test que la ligne 'necessaryResult' est bien parsée.
     */
    @Test
    public void testNecessaryResultContent() {
        Class clazz = PopulationReproductionEquation.class;
        Method method = clazz.getDeclaredMethods()[0];
        String content = "String[] necessaryResult = {MatrixAbundance.class.getSimpleName(),\nMatrixTestDep1.class.getSimpleName()};\n" +
                "return 0;";
        content = EvaluatorHelper.generateContent("fr.ifremer.isisfish.equation", "Test", clazz, method, content);

        assertThat(content).contains("@Override public String[] getNecessaryResult ()");
        assertThat(content).contains("private String[] necessaryResult = {MatrixAbundance.c");
    }

    /**
     * Test que la ligne 'necessaryResult' est bien evaluée.
     */
    @Test
    public void testNecessaryResultEvaluate() {
        String content = "String[] necessaryResult = {MatrixAbundance.class.getSimpleName()};return 0;";

        String[] necessaryResult = EvaluatorHelper.evaluateNecessaryResult("fr.ifremer.isisfish.equation", "Test", PopulationReproductionEquation.class, content);
        assertThat(necessaryResult).contains("MatrixAbundance");
    }

    @Test
    public void testNoNecessaryResultOnPrescripts() {
        String content = "String[] necessaryResult = {MatrixAbundance.NAME};\nreturn 0;";
        Class clazz = SimulationPreScript.class;
        Method method = clazz.getDeclaredMethods()[0];
        content = EvaluatorHelper.generateContent("fr.ifremer.isisfish.simulator", "SimulationPreScriptGeneratedPreScript", clazz, method, content);
        assertThat(content).doesNotContain("String[] necessaryResult = {MatrixAbundance");
        assertThat(content).doesNotContain("@Override public String[] getNecessaryResult ()");
        assertThat(content).contains("return 0;");
    }
}
