/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2012 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.util;

import fr.ifremer.isisfish.AbstractIsisFishTest;
import fr.ifremer.isisfish.IsisFish;
import fr.ifremer.isisfish.datastore.SimulationPlanStorage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.PrintWriter;

/**
 * CompileHelperTest.
 * 
 * Created: 12 janv. 2006 16:20:33
 * 
 * @author poussin
 * 
 * @version $Revision$
 * 
 * Last update: $Date$
 * by : $Author$
 */
public class JavadocHelperTest extends AbstractIsisFishTest {

    /**
     * Try to generate javadoc on current isis-fish-data directory.
     * (single java file case)
     */
    @Test
    public void testIsisFishDataJavadoc() {

        File currentDirectory = IsisFish.config.getDatabaseDirectory();
        File destinationDirectory = new File(getTestDirectory(), "isis-docs");
        File testJavaFile = new File(currentDirectory, SimulationPlanStorage.SIMULATION_PLAN_PATH
                + File.separatorChar + "CalibrationSimplexePasVariable2Capturabilite.java");

        PrintWriter out = new PrintWriter(System.out);
        int result = JavadocHelper.generateJavadoc(currentDirectory, testJavaFile, destinationDirectory, out);

        // 0 = pas d'erreur
        Assertions.assertEquals(0, result);
    }
    
    /**
     * Try to generate javadoc on current isis-fish-data directory.
     * (all directory java files)
     */
    @Test
    public void testIsisFishDataJavadocAll() {

        File currentDirectory = getCurrentDatabaseDirectory();
        File destinationDirectory = new File(getTestDirectory(), "isis-docs-all");

        PrintWriter out = new PrintWriter(System.out);
        int result = JavadocHelper.generateJavadoc(currentDirectory, destinationDirectory, out);

        // 0 = pas d'erreur
        Assertions.assertEquals(0, result);
    }
}
