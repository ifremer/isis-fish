/*
 * #%L
 * IsisFish
 *
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2020 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.util.exec;

import org.apache.commons.lang3.SystemUtils;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

class ExecHelperTest {

    @Test
    void textExecLinux() throws IOException, InterruptedException {
        Assumptions.assumeTrue(SystemUtils.IS_OS_LINUX);

        ExecResult exec = ExecHelper.exec("ls", "-l", "/");
        assertThat(exec.getTime()).isGreaterThan(0);
        assertThat(exec.getOutput()).contains("bin", "dev", "lib", "usr", "var");
        assertThat(exec.getError()).isEmpty();
        assertThat(exec.getReturnCode()).isEqualTo(0);
    }

    @Test
    void textExecLinuxFail() throws IOException, InterruptedException {
        Assumptions.assumeTrue(SystemUtils.IS_OS_LINUX);

        ExecResult exec = ExecHelper.exec("ls", "-ZWC", "/");
        assertThat(exec.getTime()).isGreaterThan(0);
        assertThat(exec.getOutput()).isEmpty();
        assertThat(exec.getError()).contains("ls --help");
        assertThat(exec.getReturnCode()).isEqualTo(2);
    }
}