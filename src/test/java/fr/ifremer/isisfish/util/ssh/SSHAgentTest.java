/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2015 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.util.ssh;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.KeyPair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * Class de test créé pour tester et centraliser la gestion
 * des clés SSH et leur passphrase dans Isis.
 *
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class SSHAgentTest {

    protected static File keyFile;
    
    /**
     * Find ssh key file.
     * 
     * @throws URISyntaxException 
     */
    @BeforeAll
    public static void init() throws URISyntaxException {
        URL keyURL = SSHAgentTest.class.getResource("isistestkey");
        keyFile = new File(keyURL.getPath());
    }
    
    /**
     * Test que la clé existe.
     */
    @Test
    public void testKeyExist() {
        Assertions.assertNotNull(keyFile);
        Assertions.assertTrue(keyFile.isFile());
    }
    
    /**
     * Test si une passphrase est valide pour une clé SSH.
     * 
     * @throws JSchException 
     */
    @Test
    public void testIsValidPassphrase() throws JSchException {
        char[] passphrase = "isispassphrase".toCharArray();
        
        JSch jsch = new JSch();
        KeyPair kpair = KeyPair.load(jsch, keyFile.getAbsolutePath());

        Assertions.assertTrue(kpair.isEncrypted()); // cle protegee
        Assertions.assertTrue(kpair.decrypt(SSHAgent.toBytes(passphrase))); // decodage fonctionne
        
    }
    
    /**
     * Test qu'une passphrase n'est pas valide pour une clé.
     * @throws JSchException 
     */
    @Test
    public void testIsNotValidPassphrase() throws JSchException {
        char[] passphrase = "passphare not good".toCharArray();
        
        JSch jsch = new JSch();
        KeyPair kpair = KeyPair.load(jsch, keyFile.getAbsolutePath());

        Assertions.assertTrue(kpair.isEncrypted()); // cle protegee
        Assertions.assertFalse(kpair.decrypt(SSHAgent.toBytes(passphrase))); // decodage fonctionne
    }
}
