/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2008 - 2018 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.vcs;

import fr.ifremer.isisfish.AbstractIsisFishTest;
import fr.ifremer.isisfish.vcs.VCS.Status;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.nuiton.version.Version;
import org.nuiton.version.Versions;
import org.tmatesoft.svn.core.SVNCommitInfo;
import org.tmatesoft.svn.core.SVNDepth;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.io.SVNRepositoryFactory;
import org.tmatesoft.svn.core.wc.SVNClientManager;
import org.tmatesoft.svn.core.wc.SVNCopySource;
import org.tmatesoft.svn.core.wc.SVNRevision;
import org.tmatesoft.svn.core.wc.SVNStatus;
import org.tmatesoft.svn.core.wc.SVNStatusType;
import org.tmatesoft.svn.core.wc.SVNUpdateClient;
import org.tmatesoft.svn.core.wc.SVNWCClient;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class de test VCS.
 * 
 * Created: 12 janv. 2006 16:20:33
 * 
 * @author poussin
 * 
 * @version $Revision$
 * 
 * Last update: $Date$ by : $Author$
 */
public class VCSSVNTest extends AbstractIsisFishTest {

    /** Class logger */
    private static Log log = LogFactory.getLog(VCSSVNTest.class);

    /** Run test out of target directory for SVN to not find parent .svn with not supported svn format. */
    public static final File TMPDIR = new File("target").getAbsoluteFile();

    public static final String FILECONTENTTAG = "Version 3.1.0";
    public static final String FILECONTENTTRUNK = "Version 3.2.0";

    protected static File template;
    protected static File remoteRepo;
    protected static File localRepo;
    protected static File localRepoTrunk;

    protected VCSSVN vcs = null;

    @BeforeAll
    public static void setUpOnce() {
        // XXX: c'est temporaire. Juste pour desactiver les tests SVN sur docker qui ne supporte pas le lock
        Assumptions.assumeTrue(!java.awt.GraphicsEnvironment.isHeadless());
    }

    /**
     * Init call before each test.
     * @throws IOException 
     * @throws SVNException 
     */
    @BeforeEach
    public void setUp() throws IOException, SVNException {
        template = new File(TMPDIR, "testsvn-template");
        remoteRepo = new File(TMPDIR, "testsvn-repo");
        localRepo = new File(TMPDIR, "testsvn-local");
        localRepoTrunk = new File(TMPDIR, "testsvn-localTrunk");

        // un peu de nettoyage
        tearDown();

        // creation de l'instance de notre VCS pour les tests
        // on le fait au debut pour que la l'init de la lib svn soit fait
        // par la classe elle meme et ainsi tester que ca marche
        vcs = new VCSSVN(localRepo, "file", "", remoteRepo.getAbsolutePath()
                + "/isis-fish-data", null, "", "");

        // Creation d'un repo local avec un trunk et un tag
        // durant le test, on modifie version.txt pour utiliser
        // fileContentTrunk et modifier le contenu. On peut alors tester
        // le changement de tag en fonction du contenu du fichier

        // creation d'un template de directory
        new File(template, "regions" + File.separator + "DemoRegion"
                + File.separator + "data").mkdirs();
        new File(template, "simulations" + File.separator + "simu1"
                + File.separator + "data").mkdirs();
        new File(template, "scripts").mkdirs();

        FileUtils.writeStringToFile(new File(template, "scripts" + File.separator
                + "version.txt"), FILECONTENTTAG, StandardCharsets.UTF_8);

        // creation du repo pour les tests
        SVNRepositoryFactory.createLocalRepository(remoteRepo, false, true);

        // ajout de source dans le repo
        SVNURL svnURL = SVNURL.parseURIEncoded("file://"
                + remoteRepo.getAbsolutePath());
        SVNURL svnRoot = svnURL.appendPath("isis-fish-data", false);
        SVNURL svnTrunk = svnRoot.appendPath("trunk", false);
        SVNURL svnTags = svnRoot.appendPath("tags", false);

        SVNClientManager svnManager = SVNClientManager.newInstance();
        // creation de l'arborescence
        svnManager.getCommitClient().doMkDir(
                new SVNURL[] { svnRoot, svnTrunk, svnTags }, "add dir");
        //svnManager.getCommitClient().doImport(template, svnTrunk, "initial import", true, true);
        svnManager.getCommitClient().doImport(template, svnTrunk,
                "initial import", null, true, true, SVNDepth.INFINITY);

        // 1.1.x svnManager.getCopyClient().doCopy(svnTrunk, SVNRevision.HEAD, svnTags.appendPath("3.1.0", false), false, true, "Create tag");
        SVNCopySource source = new SVNCopySource(SVNRevision.HEAD,
                SVNRevision.HEAD, svnTrunk);

        svnManager.getCopyClient().doCopy(new SVNCopySource[] { source },
                svnTags.appendPath("3.1.0", false), false, /*isMove*/
                true, /*makeParents*/
                true, /*failWhenDstExists*/
                "Create tag", null);

    }

    /**
     * Clean repo directories.
     */
    @AfterEach
    public void tearDown() {
        // un peu de nettoyage
        if (template.exists()) {
            FileUtils.deleteQuietly(template);
        }
        if (remoteRepo.exists()) {
            FileUtils.deleteQuietly(remoteRepo);
        }
        if (localRepo.exists()) {
            FileUtils.deleteQuietly(localRepo);
        }
        if (localRepoTrunk.exists()) {
            FileUtils.deleteQuietly(localRepoTrunk);
        }
    }

    /**
     * Test of getSVNManager method, of class VCSSVN.
     */
    @Test
    public void testgetSVNManager() {

        // log
        if (log.isInfoEnabled()) {
            log.info("testgetSVNManager()");
        }

        SVNClientManager result = vcs.getSVNManager();
        Assertions.assertNotNull(result);
    }

    /**
     * Test of getRemoteURL method, of class VCSSVN.
     * @throws SVNException 
     */
    @Test
    public void testgetRemoteURL() throws SVNException {

        // log
        if (log.isInfoEnabled()) {
            log.info("testgetRemoteURL()");
        }

        SVNURL expResult = SVNURL.create("file", null, "", -1, remoteRepo
                .getAbsolutePath()
                + "/" + "isis-fish-data", true);
        SVNURL result = vcs.getRemoteURL();
        Assertions.assertEquals(expResult, result);

    }

    /**
     * Test of isVersionnableFile method, of class VCSSVN.
     */
    @Test
    public void testIsVersionnableFile() {

        // log
        if (log.isInfoEnabled()) {
            log.info("testIsVersionnableFile()");
        }

        // un fichier special, on refuse
        File file = new File(".svn");
        boolean result = vcs.isVersionnableFile(file);
        Assertions.assertFalse(result);

        // Pas dans le repository local, on refuse
        file = new File("Toto.java");
        result = vcs.isVersionnableFile(file);
        Assertions.assertFalse(result);

        // enfin un bout fichier :)
        file = new File(vcs.getLocalRepository(), "Toto.java");
        result = vcs.isVersionnableFile(file);
        Assertions.assertTrue(result);

        // match pas (svn ignore)
        file = new File(vcs.getLocalRepository(), "libaudio.la");
        result = vcs.isVersionnableFile(file);
        Assertions.assertFalse(result);

        // match pas (svn ignore)
        file = new File(vcs.getLocalRepository(), "libaudio.lo");
        result = vcs.isVersionnableFile(file);
        Assertions.assertFalse(result);

        // match (svn ignore)
        file = new File(vcs.getLocalRepository(), "libaudio.lads");
        result = vcs.isVersionnableFile(file);
        Assertions.assertTrue(result);

        // match pas (svn ignore)
        file = new File(vcs.getLocalRepository(), "libaudio.lads~");
        result = vcs.isVersionnableFile(file);
        Assertions.assertFalse(result);

        // match pas (svn ignore)
        file = new File(vcs.getLocalRepository(), "commit.rev1234.rej");
        result = vcs.isVersionnableFile(file);
        Assertions.assertFalse(result);
    }

    /**
     * Test of checkout method, of class VCSSVN.
     * @throws IOException 
     * @throws VCSException 
     */
    @Test
    public void testAll() throws IOException, VCSException {

        // log
        if (log.isInfoEnabled()) {
            log.info("testIsVersionnableAbleFile()");
        }

        // trunk contient une copie complete de trunk, elle permet de test
        // status lors que l'on fait des commits sur le trunk sur instance
        VCSSVN trunk = new VCSSVN(localRepoTrunk, "file", "", remoteRepo
                .getAbsolutePath()
                + "/" + "isis-fish-data", null, "", "");

        trunk.checkout(null, true);

        // on ne checkout rien juste le .svn dans le repertoire racine
        vcs.checkout(null, false);
        Assertions.assertTrue(vcs.getLocalRepository().exists());
        Assertions.assertTrue(vcs.getTag().startsWith("/trunk"));

        // update scripts dir
        vcs.update(new File(vcs.getLocalRepository(), "scripts"), true);
        File version = new File(vcs.getLocalRepository(), "scripts"
                + File.separator + "version.txt");
        Assertions.assertTrue(version.exists());
        Assertions.assertEquals(FILECONTENTTAG, FileUtils.readFileToString(version, StandardCharsets.UTF_8));

        // modification du fichier version.txt
        FileUtils.writeStringToFile(version, FILECONTENTTRUNK, StandardCharsets.UTF_8);
        vcs.commit(null, "modif du fichier version");
        Assertions.assertEquals(FILECONTENTTRUNK, FileUtils.readFileToString(version, StandardCharsets.UTF_8));

        // recuperation de DemoRegion
        File demo = new File(vcs.getLocalRepository(), "regions"
                + File.separator + "DemoRegion");
        vcs.update(demo, true);
        Assertions.assertTrue(demo.exists());

        // suppression de DemoRegion
        vcs.delete(Collections.singletonList(demo), "suppression d'une region");
        Assertions.assertFalse(demo.exists());

        // ajout d'un fichier sur le trunk avant passage sur le tag
        File fileToAdd = new File(vcs.getLocalRepository(), "newfile.txt");
        String lecontent = "Le nouveau fichier";
        FileUtils.writeStringToFile(fileToAdd, lecontent, StandardCharsets.UTF_8);
        vcs.add(Arrays.asList(fileToAdd), "ajout d'un fichier");

        // test switchTag
        vcs.setTag(Versions.valueOf("3.1.0"));
        Assertions.assertTrue(vcs.getTag().startsWith("/tags/3.1.0"));
        Assertions.assertEquals(FILECONTENTTAG, FileUtils.readFileToString(version, StandardCharsets.UTF_8));
        Assertions.assertTrue(demo.exists());

        // recherche du status des fichiers
        File fileVersion = new File(trunk.getLocalRepository(), "scripts"
                + File.separator + "version.txt");
        File fileDeleted = new File(trunk.getLocalRepository(), "regions"
                + File.separator + "DemoRegion");
        File newfileAdded = new File(trunk.getLocalRepository(), "newfile.txt");
        Map<File, SVNStatus> map = trunk.getRemoteStatus(trunk
                .getLocalRepository(), true);
        Assertions.assertEquals(3, map.size()); // version.txt modifie, DemoRegion supprimee
        Assertions.assertTrue(map.containsKey(fileVersion));
        Assertions.assertTrue(map.containsKey(fileDeleted));
        Assertions.assertTrue(map.containsKey(newfileAdded));

        // modif dans repo trunk de version pour qu'il y ait un conflit
        FileUtils.writeStringToFile(fileVersion, "Le nouveau content de version", StandardCharsets.UTF_8);

        // update global du repo trunk
        List<File> conflictFile = trunk.update(null, true);
        if (log.isInfoEnabled()) {
            log.info("conflictFile: " + conflictFile.toString());
        }
        Assertions.assertEquals(1, conflictFile.size());
        Assertions.assertTrue(conflictFile.contains(fileVersion));

        File fileAdded = new File(trunk.getLocalRepository(), "newfile.txt");
        Assertions.assertEquals(lecontent, FileUtils.readFileToString(fileAdded, StandardCharsets.UTF_8));
        Assertions.assertFalse(fileDeleted.exists());

        // recherche du status des fichiers en remote, il ne doit plus y avoir de diff
        Map<File, SVNStatus> map2 = trunk.getRemoteStatus(trunk
                .getLocalRepository(), true);
        Assertions.assertEquals(0, map2.size());

        // il doit toujours y avoir version.txt qui est modifier localement
        Map<File, SVNStatus> map3 = trunk.getLocalStatus(trunk
                .getLocalRepository(), true);
        Assertions.assertEquals(1, map3.size());
        Assertions.assertTrue(map3.containsKey(fileVersion));

        // on commit le fichier version.txt pour verifier qu'en local et remote
        // il n'y a plus de modif
        trunk.commit(Arrays.asList(fileVersion),
                "Commit fichier version, avec les conflits");
        Map<File, SVNStatus> map4 = trunk.getRemoteStatus(trunk
                .getLocalRepository(), true);
        Assertions.assertEquals(0, map4.size());
        Map<File, SVNStatus> map5 = trunk.getLocalStatus(trunk
                .getLocalRepository(), true);
        Assertions.assertEquals(0, map5.size());
    }

    /**
     * Test of add method, of class VCSSVN.
     *
     * @throws Exception 
     */
    @Test
    public void testAdd() throws Exception {

        // log
        if (log.isInfoEnabled()) {
            log.info("testAdd()");
        }

        // checkout a TAG
        vcs.checkout(null, true);

        // now, make a modification, on LOCAL repo
        File firstFile = new File(vcs.getLocalRepository() + File.separator
                + "scripts" + File.separator + "version.txt");
        FileUtils.writeStringToFile(firstFile, "Version 3.2.1", StandardCharsets.UTF_8);

        List<File> files = Collections.singletonList(firstFile);

        vcs.add(files, "test commit");
    }

    /**
     * Verifie la connexion et si le protocole a change, switch le repository
     * pour utiliser le nouveau protocole. Si on est en mode interface (mode
     * graphique) et que le switch se passe mal, demande a l'utilisateur
     * de nouvelle valeur pour le protocole (+ identifiant, ...)
     * @throws VCSException
     */
    @Test
    public void testCheckProtocol() throws VCSException {

        vcs.checkout(null, false);

        vcs.checkProtocol();
    }

    /**
     * Test cleanup.
     * 
     * @throws VCSException
     */
    @Test
    public void testCleanup() throws VCSException {

        vcs.checkout(null, false);

        vcs.cleanup(null);
    }

    /**
     * Return all changelog between local file version and remote repository
     * file version.
     * 
     * @throws VCSException 
     * @throws IOException 
     * @throws SVNException 
     */
    @Test
    public void testGetChanglog() throws VCSException, IOException,
            SVNException {

        // log
        if (log.isInfoEnabled()) {
            log.info("testGetChanglog()");
        }

        File firstFile = new File(vcs.getLocalRepository() + File.separator
                + "scripts" + File.separator + "version.txt");
        File secondFile = new File(vcs.getLocalRepository() + File.separator
                + "simulations" + File.separator + "simu1" + File.separator
                + "simulation.properties");
        List<File> files = new ArrayList<>();
        files.add(firstFile);
        files.add(secondFile);

        // first, checkout trunk
        vcs.checkout(null, true);

        // now, make a modification, on LOCAL repo
        FileUtils.writeStringToFile(firstFile, "version 3.2.1", StandardCharsets.UTF_8);
        FileUtils.writeStringToFile(secondFile, "name = test1", StandardCharsets.UTF_8);
        SVNClientManager svnManager = SVNClientManager.newInstance();
        svnManager.getWCClient().doAdd(secondFile, // File path
                true, // boolean force
                false, // boolean mkdir
                true, // boolean climbUnversionedParents
                SVNDepth.FILES, // SVNDepth depth
                false, // boolean includeIgnored
                true); // boolean makeParents
        svnManager.getCommitClient().doCommit(
                new File[] { firstFile, secondFile }, false,
                "mise a jour de la version", null, null, false, true,
                SVNDepth.INFINITY);

        Map<File, String> result = vcs.getChanglog(files);
        
        Map<File, String> expResult = new HashMap<>();
        expResult.put(firstFile, "mise a jour de la version");
        expResult.put(secondFile, "mise a jour de la version");
        Assertions.assertEquals(expResult, result);
    }

    /**
     * Test of getDiff method, of class VCSSVN.
     * @throws VCSException 
     * @throws IOException 
     */
    @Test
    public void testGetDiff() throws VCSException, IOException {

        // log
        if (log.isInfoEnabled()) {
            log.info("testGetDiff()");
        }

        File firstFile = new File(vcs.getLocalRepository() + File.separator
                + "scripts" + File.separator + "version.txt");

        vcs.checkout(null, true);

        // now, make a modification, on LOCAL repo
        FileUtils.writeStringToFile(firstFile, "Version 3.2.1", StandardCharsets.UTF_8);

        String result = vcs.getDiff(firstFile);

        // quelques tests sur la sortie, pas d'égalité parfaite
        Assertions.assertTrue(result.indexOf("version.txt") > 0);
        Assertions.assertTrue(result.indexOf("-Version 3.1.0") > 0);
        Assertions.assertTrue(result.indexOf("+Version 3.2.1") > 0);

    }

    /**
     * Test of getFileList method, of class VCSSVN.
     * @throws VCSException 
     */
    @Test
    public void testGetFileList() throws VCSException {

        // log
        if (log.isInfoEnabled()) {
            log.info("testGetFileList()");
        }

        vcs.checkout(null, true);

        File directory = null;
        List<String> expResult = new ArrayList<>(3);
        expResult.add("regions");
        expResult.add("scripts");
        expResult.add("simulations");
        List<String> result = vcs.getFileList(directory);
        Assertions.assertEquals(expResult, result);
    }

    /**
     * Test of getFileList method, of class VCSSVN.
     * @throws VCSException 
     * @throws SVNException 
     */
    @Test
    public void testGetFileList2() throws VCSException, SVNException {

        // log
        if (log.isInfoEnabled()) {
            log.info("testGetFileList2()");
        }

        vcs.checkout(null, true);

        // add new directory
        File newDir = new File(vcs.getLocalRepository() + "/testadddir");
        newDir.mkdir();

        SVNWCClient wcClient = vcs.getSVNManager().getWCClient();
        wcClient.doAdd(newDir, // File path
                true, // boolean force
                false, // boolean mkdir
                true, // boolean climbUnversionedParents
                SVNDepth.FILES, // SVNDepth depth
                false, // boolean includeIgnored
                true); // boolean makeParents

        // previous added dir, should not appear in list
        File directory = null;
        List<String> expResult = new ArrayList<>(3);
        expResult.add("regions");
        expResult.add("scripts");
        expResult.add("simulations");
        List<String> result = vcs.getFileList(directory);
        Assertions.assertEquals(expResult, result);
    }

    /**
     * Test of getFileList method, of class VCSSVN.
     * 
     * Add some dir on remote repo.
     * 
     * @throws VCSException 
     * @throws SVNException 
     */
    @Test
    public void testGetFileList3() throws VCSException, SVNException {

        // log
        if (log.isInfoEnabled()) {
            log.info("testGetFileList3()");
        }

        // checkout current trunk
        vcs.checkout(null, true);

        // now, make a modification, on REMOTE repo
        SVNURL svnURL = SVNURL.parseURIEncoded("file://"
                + remoteRepo.getAbsolutePath());
        SVNURL svnURLNewDir = svnURL.appendPath("isis-fish-data", false)
                .appendPath("trunk", false).appendPath("test", false);
        SVNClientManager svnManager = SVNClientManager.newInstance();
        svnManager.getCommitClient().doMkDir(new SVNURL[] { svnURLNewDir },
                "add test dir");

        // previous added dir, should appear in list
        File directory = null;
        List<String> expResult = new ArrayList<>(3);
        expResult.add("regions");
        expResult.add("scripts");
        expResult.add("simulations");
        expResult.add("test");
        List<String> result = vcs.getFileList(directory);
        Assertions.assertEquals(expResult, result);
    }

    /**
     * Test of getUpdatedFile method, of class VCSSVN.
     * @throws VCSException 
     * @throws IOException 
     * @throws SVNException 
     */
    @Test
    public void testGetUpdatedFile() throws VCSException, IOException,
            SVNException {

        // log
        if (log.isInfoEnabled()) {
            log.info("testGetUpdatedFile()");
        }

        List<File> expResult = new ArrayList<>();
        expResult.add(new File(vcs.getLocalRepository().getAbsolutePath()
                + File.separator + "test"));
        expResult.add(vcs.getLocalRepository());

        // checkout current trunk
        vcs.checkout(null, true);

        // now, make a modification, on REMOTE repo
        SVNURL svnURL = SVNURL.parseURIEncoded("file://"
                + remoteRepo.getAbsolutePath());
        SVNURL svnURLNewDir = svnURL.appendPath("isis-fish-data", false)
                .appendPath("trunk", false).appendPath("test", false);
        SVNClientManager svnManager = SVNClientManager.newInstance();
        svnManager.getCommitClient().doMkDir(new SVNURL[] { svnURLNewDir },
                "add test dir");

        List<File> result = vcs.getUpdatedFile();
        Assertions.assertEquals(expResult, result);
    }

    /**
     * Test of getUpdatedFile method, of class VCSSVN.
     * @throws VCSException 
     * @throws IOException 
     * @throws SVNException 
     */
    @Test
    public void testGetUpdatedFileEmptyResult() throws VCSException,
            IOException, SVNException {

        // log
        if (log.isInfoEnabled()) {
            log.info("testGetUpdatedFile()");
        }

        List<File> expResult = new ArrayList<>();

        // checkout current trunk
        vcs.checkout(null, true);

        List<File> result = vcs.getUpdatedFile();
        Assertions.assertEquals(expResult, result);

    }

    /**
     * Ask if there are some new or modified files on server.
     * 
     * @throws VCSException 
     * @throws SVNException 
     */
    @Test
    public void testHaveUpdate() throws VCSException, SVNException {

        // log
        if (log.isInfoEnabled()) {
            log.info("testHaveUpdate()");
        }

        // checkout current trunk
        vcs.checkout(null, true);

        // now, make a modification, on REMOTE repo
        SVNURL svnURL = SVNURL.parseURIEncoded("file://"
                + remoteRepo.getAbsolutePath());
        SVNURL svnURLNewDir = svnURL.appendPath("isis-fish-data", false)
                .appendPath("trunk", false).appendPath("test", false);
        SVNClientManager svnManager = SVNClientManager.newInstance();
        svnManager.getCommitClient().doMkDir(new SVNURL[] { svnURLNewDir },
                "add test dir");

        boolean result = vcs.haveUpdate();
        Assertions.assertTrue(result);
    }

    /**
     * Ask if there are some new or modified files on server.
     * 
     * @throws VCSException 
     * @throws SVNException 
     */
    @Test
    public void testHaveNoUpdate() throws VCSException, SVNException {

        // log
        if (log.isInfoEnabled()) {
            log.info("testHaveNoUpdate()");
        }

        // checkout current trunk
        vcs.checkout(null, true);

        boolean result = vcs.haveUpdate();
        Assertions.assertFalse(result);
    }

    /**
     * Test of isConnected method, of class VCSSVN.
     */
    @Test
    public void testIsConnected() {

        // log
        if (log.isInfoEnabled()) {
            log.info("testIsConnected()");
        }

        boolean result = vcs.isConnected();
        Assertions.assertTrue(result);
    }

    /**
     * Test of isOnRemote method, of class VCSSVN.
     * @throws VCSException 
     */
    @Test
    public void testIsOnRemote() throws VCSException {

        // log
        if (log.isInfoEnabled()) {
            log.info("testIsOnRemote()");
        }

        vcs.checkout(null, false);

        File file = null;
        boolean result = vcs.isOnRemote(file);
        Assertions.assertTrue(result);
    }

    /**
     * Test of isOnRemote method, of class VCSSVN.
     * @throws VCSException 
     */
    @Test
    public void testIsOnRemote2() throws VCSException {

        // log
        if (log.isInfoEnabled()) {
            log.info("testIsOnRemote2()");
        }

        vcs.checkout(null, false);

        File file = new File(vcs.getLocalRepository() + File.separator
                + "scripts" + File.separator + "version.txt");
        boolean result = vcs.isOnRemote(file);
        Assertions.assertTrue(result);
    }

    /**
     * Test of isOnRemote method, of class VCSSVN.
     * @throws VCSException 
     * @throws SVNException 
     */
    @Test
    public void testIsOnRemote3() throws VCSException, SVNException {

        // log
        if (log.isInfoEnabled()) {
            log.info("testIsOnRemote3()");
        }

        vcs.checkout(null, false);

        // now, make a modification, on REMOTE repo
        SVNURL svnURL = SVNURL.parseURIEncoded("file://"
                + remoteRepo.getAbsolutePath());
        SVNURL svnURLNewDir = svnURL.appendPath("isis-fish-data", false)
                .appendPath("trunk", false).appendPath("test", false);
        SVNClientManager svnManager = SVNClientManager.newInstance();
        svnManager.getCommitClient().doMkDir(new SVNURL[] { svnURLNewDir },
                "add test dir");

        File file = new File(vcs.getLocalRepository() + File.separator + "test");
        boolean result = vcs.isOnRemote(file);
        Assertions.assertTrue(result);
    }

    /**
     * Test of isOnRemote method, of class VCSSVN.
     * @throws VCSException 
     * @throws SVNException 
     */
    @Test
    public void testIsOnRemote4() throws VCSException, SVNException {

        // log
        if (log.isInfoEnabled()) {
            log.info("testIsOnRemote4()");
        }

        vcs.checkout(null, false);

        // now, make a modification, on REMOTE repo
        SVNURL svnURL = SVNURL.parseURIEncoded("file://"
                + remoteRepo.getAbsolutePath());
        SVNURL svnURLNewDir = svnURL.appendPath("isis-fish-data", false)
                .appendPath("trunk", false).appendPath("scripts", false);
        SVNClientManager svnManager = SVNClientManager.newInstance();
        svnManager.getCommitClient().doDelete(new SVNURL[] { svnURLNewDir },
                "del scripts dir");

        File file = new File(vcs.getLocalRepository() + File.separator
                + "scripts");
        boolean result = vcs.isOnRemote(file);
        Assertions.assertFalse(result);
    }

    /**
     * Test of isOnRemote method, of class VCSSVN.
     * @throws VCSException 
     * @throws SVNException 
     * @throws IOException 
     */
    @Test
    public void testIsOnRemote5() throws VCSException, SVNException,
            IOException {

        // log
        if (log.isInfoEnabled()) {
            log.info("testIsOnRemote5()");
        }

        vcs.checkout(null, false);

        // modif on local repo
        File file = new File(vcs.getLocalRepository() + File.separator
                + "test.txt");
        FileUtils.writeStringToFile(file, "name = test1", StandardCharsets.UTF_8);
        SVNClientManager svnManager = SVNClientManager.newInstance();
        svnManager.getWCClient().doAdd(file, // File path
                true, // boolean force
                false, // boolean mkdir
                true, // boolean climbUnversionedParents
                SVNDepth.FILES, // SVNDepth depth
                false, // boolean includeIgnored
                true); // boolean makeParents

        boolean result = vcs.isOnRemote(file);
        Assertions.assertFalse(result);
    }

    /**
     * Test of isTag method, of class VCSSVN.
     * @throws VCSException 
     */
    @Test
    public void testIsTag() throws VCSException {

        // log
        if (log.isInfoEnabled()) {
            log.info("testGetDiff()");
        }

        Version version = Versions.valueOf("3.1.0");
        Assertions.assertTrue(vcs.isTag(version));

        version = Versions.valueOf("3.2.0");
        Assertions.assertFalse(vcs.isTag(version));
    }

    /**
     * Test of isUpToDate method, of class VCSSVN.
     * @throws VCSException 
     */
    @Test
    public void testIsUpToDate() throws VCSException {

        // log
        if (log.isInfoEnabled()) {
            log.info("testIsUpToDate()");
        }

        vcs.checkout(null, false);

        File file = null;
        boolean result = vcs.isUpToDate(file);
        Assertions.assertTrue(result);

    }

    /**
     * Test of isUpToDate method, of class VCSSVN.
     * @throws VCSException 
     * @throws IOException 
     * @throws SVNException 
     */
    @Test
    public void testIsUpToDate2() throws VCSException, IOException,
            SVNException {

        // log
        if (log.isInfoEnabled()) {
            log.info("testIsUpToDate2()");
        }

        // chechout
        vcs.checkout(null, false);

        // modif on local repo
        File file = new File(vcs.getLocalRepository() + File.separator
                + "test.txt");
        FileUtils.writeStringToFile(file, "name = test1", StandardCharsets.UTF_8);
        SVNClientManager svnManager = SVNClientManager.newInstance();
        svnManager.getWCClient().doAdd(file, // File path
                true, // boolean force
                false, // boolean mkdir
                true, // boolean climbUnversionedParents
                SVNDepth.FILES, // SVNDepth depth
                false, // boolean includeIgnored
                true); // boolean makeParents

        boolean result = vcs.isUpToDate(file);
        Assertions.assertFalse(result);

    }

    /**
     * Test of isUpToDate method, of class VCSSVN.
     * @throws VCSException 
     * @throws IOException 
     * @throws SVNException 
     */
    @Test
    public void testIsUpToDate3() throws VCSException, IOException,
            SVNException {

        // log
        if (log.isInfoEnabled()) {
            log.info("testIsUpToDate3()");
        }

        // chechout
        vcs.checkout(null, false);

        // now, make a modification, on REMOTE repo
        SVNURL svnURL = SVNURL.parseURIEncoded("file://" + remoteRepo.getAbsolutePath());
        SVNURL svnURLNewDir = svnURL.appendPath("isis-fish-data", false)
                .appendPath("trunk", false).appendPath("test", false);
        SVNClientManager svnManager = SVNClientManager.newInstance();
        svnManager.getCommitClient().doMkDir(new SVNURL[] { svnURLNewDir },
                "add test dir");

        File file = new File(vcs.getLocalRepository() + File.separator + "test");
        boolean result = vcs.isUpToDate(file);
        Assertions.assertFalse(result);

    }

    /**
     * Test an update with a local locked file.
     * 
     * - Checkout
     * - commit modification
     * - update at revision -1
     * - lock one file
     * - update at head
     * 
     * @throws VCSException 
     * @throws SVNException 
     * @throws IOException 
     */
    @Test
    public void testUpdateWithLock() throws VCSException, SVNException,
            IOException {

        // log
        if (log.isInfoEnabled()) {
            log.info("testUpdateWithLock()");
        }

        // chechout
        vcs.checkout(null, false);

        // modif on remote repo
        File file = new File(vcs.getLocalRepository(), "scripts"
                + File.separator + "version.txt");
        FileUtils.writeStringToFile(file, "aaaaaz", StandardCharsets.UTF_8);
        SVNClientManager svnManager = SVNClientManager.newInstance();
        SVNCommitInfo rev = svnManager.getCommitClient().doCommit(
                new File[] { file },// File[] paths,
                false, //boolean keepLocks,
                "modify version", //String commitMessage,
                null, //SVNProperties revisionProperties,
                null,// String[] changelists,
                false, //boolean keepChangelist,
                false, //boolean force,
                SVNDepth.INFINITY); //SVNDepth depth);

        // display : At revision 4
        log.debug("Commited, new revision is " + rev.getNewRevision());

        // do a new checkout at revision n-1
        SVNRevision revision = SVNRevision.create(rev.getNewRevision() - 1);
        SVNClientManager svnManagerLocal = vcs.getSVNManager();
        SVNUpdateClient updateClient = svnManagerLocal.getUpdateClient();
        long newRevision = updateClient.doUpdate(vcs.getLocalRepository(), // File file
                revision, // SVNRevision revision
                SVNDepth.INFINITY, // SVNDepth depth
                false, // boolean allowUnversionedObstructions
                false); // boolean depthIsSticky
        log.debug("Updated at revision " + newRevision);

        // lock that file
        /*File[] filesLocal = { new File(vcs.getLocalRepository(), "scripts"
                + File.separator + "version.txt") };
        SVNWCClient wcClient = svnManagerLocal.getWCClient();
        wcClient.doLock(filesLocal, true, "add lock");*/

        Map<File, SVNStatus> files = vcs.getRemoteStatus(null, true);
        log.info("Will update file = " + files.keySet());
        Assertions.assertEquals(1, files.size(), "Must be one file updated");

        // try to update...
        List<File> filesInConflict = vcs.update(null, true);
        Assertions.assertTrue(filesInConflict.isEmpty(), "No file should be in conflit");
    }

    /**
     * Test an update with file in conflict.
     * 
     * - Checkout
     * - commit modification
     * - update at revision -1
     * - modify one file
     * - update at head
     * 
     * @throws VCSException 
     * @throws SVNException 
     * @throws IOException 
     */
    @Test
    public void testUpdateWithConflict() throws VCSException, SVNException,
            IOException {

        // log
        if (log.isInfoEnabled()) {
            log.info("testUpdateWithConflict()");
        }

        // chechout
        vcs.checkout(null, false);

        // modif on remote repo
        File file = new File(vcs.getLocalRepository(), "scripts"
                + File.separator + "version.txt");
        FileUtils.writeStringToFile(file, "aaaaaz", StandardCharsets.UTF_8);
        SVNClientManager svnManager = SVNClientManager.newInstance();
        SVNCommitInfo rev = svnManager.getCommitClient().doCommit(
                new File[] { file },// File[] paths,
                false, //boolean keepLocks,
                "modify version", //String commitMessage,
                null, //SVNProperties revisionProperties,
                null,// String[] changelists,
                false, //boolean keepChangelist,
                false, //boolean force,
                SVNDepth.INFINITY); //SVNDepth depth);

        // display : At revision 4
        log.debug("Commited, new revision is " + rev.getNewRevision());

        // do a new checkout at revision n-1
        SVNRevision revision = SVNRevision.create(rev.getNewRevision() - 1);
        SVNClientManager svnManagerLocal = vcs.getSVNManager();
        SVNUpdateClient updateClient = svnManagerLocal.getUpdateClient();
        long newRevision = updateClient.doUpdate(vcs.getLocalRepository(), // File file
                revision, // SVNRevision revision
                SVNDepth.INFINITY, // SVNDepth depth
                false, // boolean allowUnversionedObstructions
                false); // boolean depthIsSticky
        log.debug("Updated at revision " + newRevision);

        // modify one file
        File file2 = new File(vcs.getLocalRepository(), "scripts"
                + File.separator + "version.txt");
        FileUtils.writeStringToFile(file2, "oooooo", StandardCharsets.UTF_8);

        // try to update...
        List<File> filesInConflict = vcs.update(null, true);
        Assertions.assertNotNull(filesInConflict);
        Assertions.assertEquals(1, filesInConflict.size());
        log.debug("Conflicts are : " + filesInConflict);
    }
    
    /**
     * Test a trunk-to-trunk switch with file in conflict.
     * 
     * - Checkout tag
     * - commit modification
     * - swith to trunk
     * - modify same file
     * - switch to tag
     * 
     * @throws VCSException 
     * @throws SVNException 
     * @throws IOException 
     */
    @Test
    public void testSwitchWithConflict() throws VCSException, SVNException,
            IOException {

        // log
        if (log.isInfoEnabled()) {
            log.info("testSwitchWithConflict()");
        }

        // chechout tags
        vcs.checkout(Versions.valueOf("3.1.0"), false);

        // modif on remote repo on tag :)
        File file = new File(vcs.getLocalRepository(), "scripts"
                + File.separator + "version.txt");
        FileUtils.writeStringToFile(file, "aaaaaz", StandardCharsets.UTF_8);
        SVNClientManager svnManager = SVNClientManager.newInstance();
        SVNCommitInfo rev = svnManager.getCommitClient().doCommit(
                new File[] { file },// File[] paths,
                false, //boolean keepLocks,
                "modify version", //String commitMessage,
                null, //SVNProperties revisionProperties,
                null,// String[] changelists,
                false, //boolean keepChangelist,
                false, //boolean force,
                SVNDepth.INFINITY); //SVNDepth depth);

        // display : At revision 4
        log.debug("Commited, new revision is " + rev.getNewRevision());

        // switch to trunk
        vcs.setTag(null);

        // modify one file
        File file2 = new File(vcs.getLocalRepository(), "scripts"
                + File.separator + "version.txt");
        FileUtils.writeStringToFile(file2, "oooooo", StandardCharsets.UTF_8);

        // try to update...
        List<File> filesInConflict = vcs.setTag(Versions.valueOf("3.1.0"));
        Assertions.assertNotNull(filesInConflict);
        Assertions.assertEquals(1, filesInConflict.size());
        log.debug("Conflicts are : " + filesInConflict);
    }

    /**
     * Test a checkFileStatus (witch is doing an update)
     * 
     * - Checkout
     * - commit modification
     * - update at revision -1
     * - modify one file
     * - update at head
     * 
     * @throws VCSException 
     * @throws SVNException 
     * @throws IOException 
     */
    @Test
    public void testCheckFileStatusWithConflict() throws VCSException, SVNException,
            IOException {

        // log
        if (log.isInfoEnabled()) {
            log.info("testCheckFileStatusWithConflict()");
        }

        // chechout
        vcs.checkout(null, false);

        // modif on remote repo
        File file = new File(vcs.getLocalRepository(), "scripts"
                + File.separator + "version.txt");
        FileUtils.writeStringToFile(file, "aaaaaz", StandardCharsets.UTF_8);
        SVNClientManager svnManager = SVNClientManager.newInstance();
        SVNCommitInfo rev = svnManager.getCommitClient().doCommit(
                new File[] { file },// File[] paths,
                false, //boolean keepLocks,
                "modify version", //String commitMessage,
                null, //SVNProperties revisionProperties,
                null,// String[] changelists,
                false, //boolean keepChangelist,
                false, //boolean force,
                SVNDepth.INFINITY); //SVNDepth depth);

        // display : At revision 4
        log.debug("Commited, new revision is " + rev.getNewRevision());

        // do a new checkout at revision n-1
        SVNRevision revision = SVNRevision.create(rev.getNewRevision() - 1);
        SVNClientManager svnManagerLocal = vcs.getSVNManager();
        SVNUpdateClient updateClient = svnManagerLocal.getUpdateClient();
        long newRevision = updateClient.doUpdate(vcs.getLocalRepository(), // File file
                revision, // SVNRevision revision
                SVNDepth.INFINITY, // SVNDepth depth
                false, // boolean allowUnversionedObstructions
                false); // boolean depthIsSticky
        log.debug("Updated at revision " + newRevision);

        // modify one file
        File file2 = new File(vcs.getLocalRepository(), "scripts"
                + File.separator + "version.txt");
        FileUtils.writeStringToFile(file2, "oooooo", StandardCharsets.UTF_8);

        // try to update...
        List<File> filesInConflict = vcs.checkFileStatus();
        Assertions.assertNotNull(filesInConflict);
        Assertions.assertEquals(1, filesInConflict.size());
        log.debug("Conflicts are : " + filesInConflict);
        
        // do it twice doesn't return results
        // checkFileStatus is doing a merge
        filesInConflict = vcs.checkFileStatus();
        Assertions.assertNull(filesInConflict);
    }

    /**
     * Test file local status.
     * 
     * - Checkout
     * - make modification
     * - tests
     * 
     * @throws VCSException 
     * @throws SVNException 
     * @throws IOException 
     */
    @Test
    public void testGetLocalStatusModified() throws VCSException, SVNException,
            IOException {

        // log
        if (log.isInfoEnabled()) {
            log.info("testGetLocalStatusModified()");
        }

        // chechout
        vcs.checkout(null, false);

        // modif on remote repo
        File file = new File(vcs.getLocalRepository(), "scripts"
                + File.separator + "version.txt");
        FileUtils.writeStringToFile(file, "modified content", StandardCharsets.UTF_8);

        Assertions.assertEquals(Status.STATUS_MODIFIED, vcs.getLocalStatus(file));
    }

    /**
     * Test file local status.
     * 
     * - Checkout
     * - delete file (file system delete)
     * - tests
     * 
     * @throws VCSException 
     * @throws SVNException 
     * @throws IOException 
     */
    @Test
    public void testGetLocalStatusMissing() throws VCSException, SVNException,
            IOException {

        // log
        if (log.isInfoEnabled()) {
            log.info("testGetLocalStatusMissing()");
        }

        // chechout
        vcs.checkout(null, false);

        // modif on remote repo
        File file = new File(vcs.getLocalRepository(), "scripts"
                + File.separator + "version.txt");
        Assertions.assertTrue(file.delete());

        Map<File, SVNStatus> statusFiles = vcs.getLocalStatus(file, true, SVNStatusType.STATUS_MISSING);
        Assertions.assertEquals(1, statusFiles.size());
    }
}
