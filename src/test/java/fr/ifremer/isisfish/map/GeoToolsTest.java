/*-
 * #%L
 * ISIS-Fish
 * %%
 * Copyright (C) 2022 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.map;

import fr.ifremer.isisfish.AbstractIsisFishTest;
import fr.ifremer.isisfish.datastore.RegionStorage;
import fr.ifremer.isisfish.entities.Cell;
import fr.ifremer.isisfish.entities.FisheryRegion;
import fr.ifremer.isisfish.entities.Zone;
import static org.assertj.core.api.Assertions.assertThat;

import org.assertj.core.data.Offset;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.junit.jupiter.api.Test;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.MultiPolygon;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.Polygon;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKTReader;
import org.nuiton.topia.TopiaContext;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

public class GeoToolsTest extends AbstractIsisFishTest {

    @Test
    public void testContainsPoint() throws ParseException {
        WKTReader reader = new WKTReader();

        MultiPolygon multiPolygon = (MultiPolygon)reader.read("MULTIPOLYGON (((-5 47.5, -5 48, -4.5 48, -4.5 47.5, -5 47.5)))");
        Point point = (Point)reader.read("POINT (-5 47.5)");
        assertThat(multiPolygon.contains(point)).isFalse(); // j'aimerais bien que cela soit vrai
        assertThat(multiPolygon.touches(point)).isTrue();

        Point point2 = (Point)reader.read("POINT (-4.99 47.51)");
        assertThat(multiPolygon.contains(point2)).isTrue();
    }

    @Test
    public void testZoneIntersection() throws ParseException {
        WKTReader reader = new WKTReader();

        MultiPolygon multiPolygon = (MultiPolygon)reader.read("MULTIPOLYGON (((-5 47.5, -5 48, -4.5 48, -4.5 47.5, -5 47.5)))");
        MultiPolygon other = (MultiPolygon)reader.read("MULTIPOLYGON (((-5 47.6, -5 48, -4.6 48, -4.6 47.6, -5 47.6)))");
        assertThat(multiPolygon.intersects(other)).isTrue();

        Geometry intersection = multiPolygon.intersection(other);
        assertThat(intersection.getArea()).isCloseTo(0.16, Offset.offset(0.001));
    }

    @Test
    public void testZoneIntersection2() {
        GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory();
        Coordinate[] coords  = new Coordinate[] {new Coordinate(-5, 47.5),
                new Coordinate(-5, 48),
                new Coordinate(-4.5, 48),
                new Coordinate(-4.5, 47.5),
                new Coordinate(-5, 47.5)};
        Polygon polygon = geometryFactory.createPolygon(coords);

        Coordinate[] coords2  = new Coordinate[] {new Coordinate(-5, 47.6),
                new Coordinate(-5, 48),
                new Coordinate(-4.6, 48),
                new Coordinate(-4.6, 47.6),
                new Coordinate(-5, 47.6)};
        Polygon polygon2 = geometryFactory.createPolygon(coords2);

        Geometry intersection = polygon.intersection(polygon2);
        assertThat(intersection.getArea()).isCloseTo(0.16, Offset.offset(0.001));
    }

    @Test
    public void testZoneIntersection3() throws ParseException {

        WKTReader reader = new WKTReader();
        MultiPolygon multiPolygon = (MultiPolygon)reader.read("MULTIPOLYGON (((-5 47.5, -5 48, -4.5 48, -4.5 47.5, -5 47.5)))");

        GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory();
        Coordinate[] coords2  = new Coordinate[] {new Coordinate(-5, 47.6),
                new Coordinate(-5, 48),
                new Coordinate(-4.6, 48),
                new Coordinate(-4.6, 47.6),
                new Coordinate(-5, 47.6)};
        Polygon polygon2 = geometryFactory.createPolygon(coords2);

        Geometry intersection = multiPolygon.intersection(polygon2);
        assertThat(intersection.getArea()).isCloseTo(0.16, Offset.offset(0.001));
    }

    @Test
    public void testZoneIntersection4() throws ParseException {
        WKTReader reader = new WKTReader();

        MultiPolygon multiPolygon = (MultiPolygon)reader.read("MULTIPOLYGON (((-5 47.5, -5 48, -4.5 48, -4.5 47.5, -5 47.5)))");
        Polygon other = (Polygon)reader.read("POLYGON ((-5 47.6, -5 48, -4.6 48, -4.6 47.6, -5 47.6))");
        assertThat(multiPolygon.intersects(other)).isTrue();

        Geometry intersection = multiPolygon.intersection(other);
        assertThat(intersection.getArea()).isCloseTo(0.16, Offset.offset(0.001));
    }

    @Test
    public void testExportImportShapefile() throws IOException {
        RegionStorage regionStorage = RegionStorage.getRegion("BaseMotosICA");
        TopiaContext regionContext = regionStorage.getStorage();
        TopiaContext tx = regionContext.beginTransaction();
        Zone zone = (Zone)tx.findByTopiaId("fr.ifremer.isisfish.entities.Zone#1169028645759#0.8226513521396226");
        FisheryRegion fisheryRegion = zone.getFisheryRegion();

        File file = File.createTempFile("isis-test-export-", ".shp");
        System.out.println(file);
        file.deleteOnExit();

        assertThat(zone.getCell()).hasSize(4);
        GeoTools.setCellToShapefile(fisheryRegion, zone.getCell(), zone.getName(), file);
        Collection<Cell> importedCells = GeoTools.getCellFromShapefile(fisheryRegion, fisheryRegion.getCell(), file);
        assertThat(importedCells).hasSize(4);
    }
}
