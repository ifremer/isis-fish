/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2010 Ifremer, CodeLutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.entity;

import fr.ifremer.isisfish.AbstractIsisFishTest;
import fr.ifremer.isisfish.IsisFishDAOHelper;
import fr.ifremer.isisfish.datastore.RegionStorage;
import fr.ifremer.isisfish.entities.Cell;
import fr.ifremer.isisfish.entities.CellDAO;
import fr.ifremer.isisfish.entities.EffortDescription;
import fr.ifremer.isisfish.entities.EffortDescriptionDAO;
import fr.ifremer.isisfish.entities.FisheryRegion;
import fr.ifremer.isisfish.entities.FisheryRegionDAO;
import fr.ifremer.isisfish.entities.Gear;
import fr.ifremer.isisfish.entities.GearDAO;
import fr.ifremer.isisfish.entities.Metier;
import fr.ifremer.isisfish.entities.MetierDAO;
import fr.ifremer.isisfish.entities.MetierSeasonInfo;
import fr.ifremer.isisfish.entities.MetierSeasonInfoDAO;
import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.entities.PopulationDAO;
import fr.ifremer.isisfish.entities.PopulationGroup;
import fr.ifremer.isisfish.entities.PopulationGroupDAO;
import fr.ifremer.isisfish.entities.PopulationSeasonInfo;
import fr.ifremer.isisfish.entities.PopulationSeasonInfoDAO;
import fr.ifremer.isisfish.entities.Port;
import fr.ifremer.isisfish.entities.PortDAO;
import fr.ifremer.isisfish.entities.Result;
import fr.ifremer.isisfish.entities.ResultDAO;
import fr.ifremer.isisfish.entities.Season;
import fr.ifremer.isisfish.entities.SeasonDAO;
import fr.ifremer.isisfish.entities.Selectivity;
import fr.ifremer.isisfish.entities.SelectivityDAO;
import fr.ifremer.isisfish.entities.SetOfVessels;
import fr.ifremer.isisfish.entities.SetOfVesselsDAO;
import fr.ifremer.isisfish.entities.Species;
import fr.ifremer.isisfish.entities.SpeciesDAO;
import fr.ifremer.isisfish.entities.Strategy;
import fr.ifremer.isisfish.entities.StrategyDAO;
import fr.ifremer.isisfish.entities.StrategyMonthInfo;
import fr.ifremer.isisfish.entities.StrategyMonthInfoDAO;
import fr.ifremer.isisfish.entities.TargetSpecies;
import fr.ifremer.isisfish.entities.TargetSpeciesDAO;
import fr.ifremer.isisfish.entities.TripType;
import fr.ifremer.isisfish.entities.TripTypeDAO;
import fr.ifremer.isisfish.entities.VesselType;
import fr.ifremer.isisfish.entities.VesselTypeDAO;
import fr.ifremer.isisfish.entities.Zone;
import fr.ifremer.isisfish.entities.ZoneDAO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.nuiton.topia.TopiaContext;

import java.util.Collection;

/**
 * PersistenceTest.
 *
 * Created: 3 août 2004
 *
 * @author Benjamin Poussin &lt;poussin@codelutin.com&gt;
 * @version $Revision$
 *
 * Mise a jour: $Date$
 * par : $Author$
 */
public class PersistenceTest extends AbstractIsisFishTest {

    /**
     * Logger for this class
     */
    private static final Log log = LogFactory.getLog(PersistenceTest.class);
    
    // Persistence
    protected TopiaContext context = null;
    protected RegionStorage rs;
    
    @BeforeEach
    public void setUp() throws Exception {
        rs = RegionStorage.create("test");
        context = rs.getStorage();
    }

    @AfterEach
    public void tearDown() throws Exception {
        rs.delete(false);
    }
    
    @Test
    public void testInstanciate() throws Exception {
        TopiaContext context = this.context.beginTransaction();

        FisheryRegionDAO fisheryRegionDAO = IsisFishDAOHelper.getFisheryRegionDAO(context);
        FisheryRegion fisheryRegion = fisheryRegionDAO.create();
        log.debug(fisheryRegion + " - " + fisheryRegion.getClass().getName());
        
        EffortDescriptionDAO effortDescriptionDAO = IsisFishDAOHelper.getEffortDescriptionDAO(context);
        EffortDescription effortDescription = effortDescriptionDAO.create();
        log.debug(effortDescription + " - " + effortDescription.getClass().getName());
        
        GearDAO gearDAO = IsisFishDAOHelper.getGearDAO(context);
        Gear gear = gearDAO.create();
        log.debug(gear + " - " + gear.getClass().getName());

        CellDAO cellDAO = IsisFishDAOHelper.getCellDAO(context);
        Cell cell = cellDAO.create();
        log.debug(cell + " - " + cell.getClass().getName());
        
        SpeciesDAO speciesDAO = IsisFishDAOHelper.getSpeciesDAO(context);
        Species species = speciesDAO.create();
        log.debug(species + " - " + species.getClass().getName());

        MetierDAO metierDAO = IsisFishDAOHelper.getMetierDAO(context);
        Metier metier = metierDAO.create();
        log.debug(metier + " - " + metier.getClass().getName());

        MetierSeasonInfoDAO metierSeasonInfoDAO = IsisFishDAOHelper.getMetierSeasonInfoDAO(context);
        MetierSeasonInfo metierSeasonInfo = metierSeasonInfoDAO.create();
        log.debug(metierSeasonInfo + " - " +metierSeasonInfo.getClass().getName());
        
        PopulationGroupDAO populationGroupDAO = IsisFishDAOHelper.getPopulationGroupDAO(context);
        PopulationGroup populationGroup = populationGroupDAO.create();
        log.debug(populationGroup + " - " + populationGroup.getClass().getName());

        PopulationDAO populationDAO = IsisFishDAOHelper.getPopulationDAO(context);
        Population population = populationDAO.create();
        log.debug(population + " - " + population.getClass().getName());

        PortDAO portDAO = IsisFishDAOHelper.getPortDAO(context);
        Port port = portDAO.create();
        log.debug(port + " - " + port.getClass().getName());

        ResultDAO resultDAO = IsisFishDAOHelper.getResultDAO(context);
        Result result = resultDAO.create();
        log.debug(result + " - " + result.getClass().getName());

        SeasonDAO seasonDAO = IsisFishDAOHelper.getSeasonDAO(context);
        Season season = seasonDAO.create();
        log.debug(season + " - " + season.getClass().getName());

        PopulationSeasonInfoDAO PopulationSeasonInfoDAO = IsisFishDAOHelper.getPopulationSeasonInfoDAO(context);
        PopulationSeasonInfo PopulationSeasonInfo = PopulationSeasonInfoDAO.create();
        log.debug(PopulationSeasonInfo + " - " + PopulationSeasonInfo.getClass().getName());

        SelectivityDAO selectivityDAO = IsisFishDAOHelper.getSelectivityDAO(context);
        Selectivity selectivity = selectivityDAO.create();
        log.debug(selectivity + " - " + selectivity.getClass().getName());

        SetOfVesselsDAO setOfVesselsDAO = IsisFishDAOHelper.getSetOfVesselsDAO(context);
        SetOfVessels setOfVessels = setOfVesselsDAO.create();
        log.debug(setOfVessels + " - " + setOfVessels.getClass().getName());

        StrategyMonthInfoDAO strategyMonthInfoDAO = IsisFishDAOHelper.getStrategyMonthInfoDAO(context);
        StrategyMonthInfo strategyMonthInfo = strategyMonthInfoDAO.create();
        log.debug(strategyMonthInfo + " - " + strategyMonthInfo.getClass().getName());

        StrategyDAO strategyDAO = IsisFishDAOHelper.getStrategyDAO(context);
        Strategy strategy = strategyDAO.create();
        log.debug(strategy + " - " + strategy.getClass().getName());

        TargetSpeciesDAO targetSpeciesDAO = IsisFishDAOHelper.getTargetSpeciesDAO(context);
        TargetSpecies targetSpecies = targetSpeciesDAO.create();
        log.debug(targetSpecies + " - " + targetSpecies.getClass().getName());

        TripTypeDAO tripTypeDAO = IsisFishDAOHelper.getTripTypeDAO(context);
        TripType tripType = tripTypeDAO.create();
        log.debug(tripType + " - " + tripType.getClass().getName());

        VesselTypeDAO vesselTypeDAO = IsisFishDAOHelper.getVesselTypeDAO(context);
        VesselType vesselType = vesselTypeDAO.create();
        log.debug(vesselType + " - " + vesselType.getClass().getName());

        ZoneDAO zoneDAO = IsisFishDAOHelper.getZoneDAO(context);
        Zone zone = zoneDAO.create();
        log.debug(zone + " - " + zone.getClass().getName());

        context.rollbackTransaction();
        context.closeContext();
    }


    @Test
    public void testMakePersistence() throws Exception {
        TopiaContext context = this.context.beginTransaction();
//        {
//            AgeGroupDAO ps = IsisFishDAOHelper.getAgeGroupDAO(context);
//            AgeGroup o = ps.create();
//            o = ps.update(o);
//        }
        {
            FisheryRegionDAO ps = IsisFishDAOHelper.getFisheryRegionDAO(context);
            FisheryRegion o = ps.create();
            o = ps.update(o);
        }
//        {
//            CompositeZoneDAO ps = IsisFishDAOHelper.getCompositeZoneDAO(context);
//            CompositeZone o = ps.create();
//            o = ps.update(o);
//        }
        {
            EffortDescriptionDAO ps = IsisFishDAOHelper.getEffortDescriptionDAO(context);
            EffortDescription o = ps.create();
            o = ps.update(o);
        }
//        {
//            EmigrationDAO ps = IsisFishDAOHelper.getEmigrationDAO(context);
//            Emigration o = ps.create();
//            o = ps.update(o);
//        }
//        {
//            EquationDAO ps = IsisFishDAOHelper.getEquationDAO(context);
//            Equation o = ps.create();
//            o = ps.update(o);
//        }
        {
            GearDAO ps = IsisFishDAOHelper.getGearDAO(context);
            Gear o = ps.create();
            o = ps.update(o);
        }
//        {
//            ImmigrationDAO ps = IsisFishDAOHelper.getImmigrationDAO(context);
//            Immigration o = ps.create();
//            o = ps.update(o);
//        }
//        {
//            LengthGroupDAO ps = IsisFishDAOHelper.getLengthGroupDAO(context);
//            LengthGroup o = ps.create();
//            o = ps.update(o);
//        }
//        {
//            MappingZoneReproZoneRecruDAO ps = IsisFishDAOHelper.getMappingZoneReproZoneRecruDAO(context);
//            MappingZoneReproZoneRecru o = ps.create();
//            o = ps.update(o);
//        }
        {
            CellDAO ps = IsisFishDAOHelper.getCellDAO(context);
            Cell o = ps.create();
            o = ps.update(o);
        }
        {
            SpeciesDAO ps = IsisFishDAOHelper.getSpeciesDAO(context);
            Species o = ps.create();
            o = ps.update(o);
        }
        {
            MetierDAO ps = IsisFishDAOHelper.getMetierDAO(context);
            Metier o = ps.create();
            o = ps.update(o);
        }
        {
            MetierSeasonInfoDAO ps = IsisFishDAOHelper.getMetierSeasonInfoDAO(context);
            MetierSeasonInfo o = ps.create();
            o = ps.update(o);
        }
//        {
//            MigrationDAO ps = IsisFishDAOHelper.getMigrationDAO(context);
//            Migration o = ps.create();
//            o = ps.update(o);
//        }
        {
//            try{
                PopulationGroupDAO ps = IsisFishDAOHelper.getPopulationGroupDAO(context);
                PopulationGroup o = ps.create();
                o = ps.update(o);
//                assertTrue(false); // normalement on a pas le droit d'instancier une entity abstraite
//            }catch(TopiaException eee){
//                assertTrue(true);
//            }
       }
        {
            PopulationDAO ps = IsisFishDAOHelper.getPopulationDAO(context);
            Population o = ps.create();
            o = ps.update(o);
        }
        {
            PortDAO ps = IsisFishDAOHelper.getPortDAO(context);
            Port o = ps.create();
            o = ps.update(o);
        }
        {
            ResultDAO ps = IsisFishDAOHelper.getResultDAO(context);
            Result o = ps.create();
            o = ps.update(o);
        }
        {
            PopulationSeasonInfoDAO ps = IsisFishDAOHelper.getPopulationSeasonInfoDAO(context);
            PopulationSeasonInfo o = ps.create();
            o = ps.update(o);
        }
        {
            SelectivityDAO ps = IsisFishDAOHelper.getSelectivityDAO(context);
            Selectivity o = ps.create();
            o = ps.update(o);
        }
        {
            SetOfVesselsDAO ps = IsisFishDAOHelper.getSetOfVesselsDAO(context);
            SetOfVessels o = ps.create();
            o = ps.update(o);
        }
//        {
//            SimpleZoneDAO ps = IsisFishDAOHelper.getSimpleZoneDAO(context);
//            SimpleZone o = ps.create();
//            o = ps.update(o);
//        }
        {
            StrategyMonthInfoDAO ps = IsisFishDAOHelper.getStrategyMonthInfoDAO(context);
            StrategyMonthInfo o = ps.create();
            o = ps.update(o);
        }
        {
            StrategyDAO ps = IsisFishDAOHelper.getStrategyDAO(context);
            Strategy o = ps.create();
            o = ps.update(o);
        }
        {
            TargetSpeciesDAO ps = IsisFishDAOHelper.getTargetSpeciesDAO(context);
            TargetSpecies o = ps.create();
            o = ps.update(o);
        }
        {
            TripTypeDAO ps = IsisFishDAOHelper.getTripTypeDAO(context);
            TripType o = ps.create();
            o = ps.update(o);
        }
        {
            VesselTypeDAO ps = IsisFishDAOHelper.getVesselTypeDAO(context);
            VesselType o = ps.create();
            o = ps.update(o);
        }
        {
            ZoneDAO ps = IsisFishDAOHelper.getZoneDAO(context);
            Zone o = ps.create();
            o = ps.update(o);
        }
        context.rollbackTransaction();
        context.closeContext();
    }

    @Test
    public void testFindAll() throws Exception {
        TopiaContext context = this.context.beginTransaction();
//        {
//            AgeGroupDAO ps = IsisFishDAOHelper.getAgeGroupDAO(context);
//            Collection<AgeGroup> list = ps.findAll();
//            log.debug(list + " - " +ps.getClass().getName());
//        }
        {
            FisheryRegionDAO ps = IsisFishDAOHelper.getFisheryRegionDAO(context);
            Collection<FisheryRegion> list = ps.findAll();
            log.debug(list + " - " +ps.getClass().getName());
        }
//        {
//            CompositeZoneDAO ps = IsisFishDAOHelper.getCompositeZoneDAO(context);
//            Collection list<CompositeZone> = ps.findAll();
//        }
        {
            EffortDescriptionDAO ps = IsisFishDAOHelper.getEffortDescriptionDAO(context);
            Collection<EffortDescription> list = ps.findAll();
            log.debug(list + " - " +ps.getClass().getName());
        }
//        {
//            EmigrationDAO ps = IsisFishDAOHelper.getEmigrationDAO(context);
//            Collection<Emigration> list = ps.findAll();
//            log.debug(list + " - " +ps.getClass().getName());
//        }
        {
            GearDAO ps = IsisFishDAOHelper.getGearDAO(context);
            Collection<Gear> list = ps.findAll();
            log.debug(list + " - " +ps.getClass().getName());
        }
//        {
//            ImmigrationDAO ps = IsisFishDAOHelper.getImmigrationDAO(context);
//            Collection<Immigration> list = ps.findAll();
//            log.debug(list + " - " +ps.getClass().getName());
//        }
//        {
//            LengthGroupDAO ps = IsisFishDAOHelper.getLengthGroupDAO(context);
//            Collection<LengthGroup> list = ps.findAll();
//            log.debug(list + " - " +ps.getClass().getName());
//        }
//        {
//            MappingZoneReproZoneRecruDAO ps = IsisFishDAOHelper.getMappingZoneReproZoneRecruDAO(context);
//            Collection<MappingZoneReproZoneRecru> list = ps.findAll();
//            log.debug(list + " - " +ps.getClass().getName());
//        }
        {
            CellDAO ps = IsisFishDAOHelper.getCellDAO(context);
            Collection<Cell> list = ps.findAll();
            log.debug(list + " - " +ps.getClass().getName());
        }
        {
            SpeciesDAO ps = IsisFishDAOHelper.getSpeciesDAO(context);
            Collection<Species> list = ps.findAll();
            log.debug(list + " - " +ps.getClass().getName());
        }
        {
            MetierDAO ps = IsisFishDAOHelper.getMetierDAO(context);
            Collection<Metier> list = ps.findAll();
            log.debug(list + " - " +ps.getClass().getName());
        }
        {
            MetierSeasonInfoDAO ps = IsisFishDAOHelper.getMetierSeasonInfoDAO(context);
            Collection<MetierSeasonInfo> list = ps.findAll();
            log.debug(list + " - " +ps.getClass().getName());
        }
//        {
//            MigrationDAO ps = IsisFishDAOHelper.getMigrationDAO(context);
//            Collection<Migration> list = ps.findAll();
//            log.debug(list + " - " +ps.getClass().getName());
//        }
        {
            PopulationGroupDAO ps = IsisFishDAOHelper.getPopulationGroupDAO(context);
            Collection<PopulationGroup> list = ps.findAll();
            log.debug(list + " - " +ps.getClass().getName());
        }
        {
            PopulationDAO ps = IsisFishDAOHelper.getPopulationDAO(context);
            Collection<Population> list = ps.findAll();
            log.debug(list + " - " +ps.getClass().getName());
        }
        {
            PortDAO ps = IsisFishDAOHelper.getPortDAO(context);
            Collection<Port> list = ps.findAll();
            log.debug(list + " - " +ps.getClass().getName());
        }
        {
            ResultDAO ps = IsisFishDAOHelper.getResultDAO(context);
            Collection<Result> list = ps.findAll();
            log.debug(list + " - " +ps.getClass().getName());
        }
        {
            PopulationSeasonInfoDAO ps = IsisFishDAOHelper.getPopulationSeasonInfoDAO(context);
            Collection<PopulationSeasonInfo> list = ps.findAll();
            log.debug(list + " - " +ps.getClass().getName());
        }
        {
            SelectivityDAO ps = IsisFishDAOHelper.getSelectivityDAO(context);
            Collection<Selectivity> list = ps.findAll();
            log.debug(list + " - " +ps.getClass().getName());
        }
        {
            SetOfVesselsDAO ps = IsisFishDAOHelper.getSetOfVesselsDAO(context);
            Collection<SetOfVessels> list = ps.findAll();
            log.debug(list + " - " +ps.getClass().getName());
        }
//        {
//            SimpleZoneDAO ps = IsisFishDAOHelper.getSimpleZoneDAO(context);
//            Collection<SimpleZone> list = ps.findAll();
//        }
        {
            StrategyMonthInfoDAO ps = IsisFishDAOHelper.getStrategyMonthInfoDAO(context);
            Collection<StrategyMonthInfo> list = ps.findAll();
            log.debug(list + " - " +ps.getClass().getName());
        }
        {
            StrategyDAO ps = IsisFishDAOHelper.getStrategyDAO(context);
            Collection<Strategy> list = ps.findAll();
            log.debug(list + " - " +ps.getClass().getName());
        }
        {
            TargetSpeciesDAO ps = IsisFishDAOHelper.getTargetSpeciesDAO(context);
            Collection<TargetSpecies> list = ps.findAll();
            log.debug(list + " - " +ps.getClass().getName());
        }
        {
            TripTypeDAO ps = IsisFishDAOHelper.getTripTypeDAO(context);
            Collection<TripType> list = ps.findAll();
            log.debug(list + " - " +ps.getClass().getName());
        }
        {
            VesselTypeDAO ps = IsisFishDAOHelper.getVesselTypeDAO(context);
            Collection<VesselType> list = ps.findAll();
            log.debug(list + " - " +ps.getClass().getName());
        }
        {
            ZoneDAO ps = IsisFishDAOHelper.getZoneDAO(context);
            Collection<Zone> list = ps.findAll();
            log.debug(list + " - " +ps.getClass().getName());
        }
        context.rollbackTransaction();
        context.closeContext();
    }
/*
    public void testDelete() throws Exception {
        TopiaContext context = this.context.beginTransaction();
        {
            AgeGroupDAO ps = IsisFishDAOHelper.getAgeGroupDAO(context);
            Collection list = ps.findAll();
            for(Iterator i=list.iterator(); i.hasNext();){
                AgeGroup o = (AgeGroup)i.next();
                ps.delete(o);
            }
        }
        {
            RegionDAO ps = IsisFishDAOHelper.getRegionDAO(context);
            Collection list = ps.findAll();
            for(Iterator i=list.iterator(); i.hasNext();){
                Region o = (Region)i.next();
                ps.delete(o);
            }
        }
        {
            CompositeZoneDAO ps = IsisFishDAOHelper.getCompositeZoneDAO(context);
            Collection list = ps.findAll();
            for(Iterator i=list.iterator(); i.hasNext();){
                CompositeZone o = (CompositeZone)i.next();
                ps.delete(o);
            }
        }
        {
            EffortDescriptionDAO ps = IsisFishDAOHelper.getEffortDescriptionDAO(context);
            Collection list = ps.findAll();
            for(Iterator i=list.iterator(); i.hasNext();){
                EffortDescription o = (EffortDescription)i.next();
                ps.delete(o);
            }
        }
        {
            EmigrationDAO ps = IsisFishDAOHelper.getEmigrationDAO(context);
            Collection list = ps.findAll();
            for(Iterator i=list.iterator(); i.hasNext();){
                Emigration o = (Emigration)i.next();
                ps.delete(o);
            }
        }
        {
            EquationDAO ps = IsisFishDAOHelper.getEquationDAO(context);
            Collection list = ps.findAll();
            for(Iterator i=list.iterator(); i.hasNext();){
                Equation o = (Equation)i.next();
                ps.delete(o);
            }
        }
        {
            GearDAO ps = IsisFishDAOHelper.getGearDAO(context);
            Collection list = ps.findAll();
            for(Iterator i=list.iterator(); i.hasNext();){
                Gear o = (Gear)i.next();
                ps.delete(o);
            }
        }
        {
            ImmigrationDAO ps = IsisFishDAOHelper.getImmigrationDAO(context);
            Collection list = ps.findAll();
            for(Iterator i=list.iterator(); i.hasNext();){
                Immigration o = (Immigration)i.next();
                ps.delete(o);
            }
        }
        {
            LengthGroupDAO ps = IsisFishDAOHelper.getLengthGroupDAO(context);
            Collection list = ps.findAll();
            for(Iterator i=list.iterator(); i.hasNext();){
                LengthGroup o = (LengthGroup)i.next();
                ps.delete(o);
            }
        }
        {
            MappingZoneReproZoneRecruDAO ps = IsisFishDAOHelper.getMappingZoneReproZoneRecruDAO(context);
            Collection list = ps.findAll();
            for(Iterator i=list.iterator(); i.hasNext();){
                MappingZoneReproZoneRecru o = (MappingZoneReproZoneRecru)i.next();
                ps.delete(o);
            }
        }
        {
            CellDAO ps = IsisFishDAOHelper.getCellDAO(context);
            Collection list = ps.findAll();
            for(Iterator i=list.iterator(); i.hasNext();){
                Cell o = (Cell)i.next();
                ps.delete(o);
            }
        }
        {
            SpeciesDAO ps = IsisFishDAOHelper.getSpeciesDAO(context);
            Collection list = ps.findAll();
            for(Iterator i=list.iterator(); i.hasNext();){
                Species o = (Species)i.next();
                ps.delete(o);
            }
        }
        {
            MetierDAO ps = IsisFishDAOHelper.getMetierDAO(context);
            Metier o = ps.create();
        }
        {
            MetierSeasonInfoDAO ps = IsisFishDAOHelper.getMetierSeasonInfoDAO(context);
            Collection list = ps.findAll();
            for(Iterator i=list.iterator(); i.hasNext();){
                MetierSeasonInfo o = (MetierSeasonInfo)i.next();
                ps.delete(o);
            }
        }
        {
            MigrationDAO ps = IsisFishDAOHelper.getMigrationDAO(context);
            Collection list = ps.findAll();
            for(Iterator i=list.iterator(); i.hasNext();){
                Migration o = (Migration)i.next();
                ps.delete(o);
            }
        }
        {
            MonthDAO ps = IsisFishDAOHelper.getMonthDAO(context);
            Collection list = ps.findAll();
            for(Iterator i=list.iterator(); i.hasNext();){
                Month o = (Month)i.next();
                ps.delete(o);
            }
        }
        {
            PopulationGroupDAO ps = IsisFishDAOHelper.getPopulationGroupDAO(context);
            Collection list = ps.findAll();
            for(Iterator i=list.iterator(); i.hasNext();){
                PopulationGroup o = (PopulationGroup)i.next();
                ps.delete(o);
            }
        }
        {
            PopulationDAO ps = IsisFishDAOHelper.getPopulationDAO(context);
            Collection list = ps.findAll();
            for(Iterator i=list.iterator(); i.hasNext();){
                Population o = (Population)i.next();
                ps.delete(o);
            }
        }
        {
            PortDAO ps = IsisFishDAOHelper.getPortDAO(context);
            Collection list = ps.findAll();
            for(Iterator i=list.iterator(); i.hasNext();){
                Port o = (Port)i.next();
                ps.delete(o);
            }
        }
        {
            ResultManagerDAO ps = IsisFishDAOHelper.getResultManagerDAO(context);
            Collection list = ps.findAll();
            for(Iterator i=list.iterator(); i.hasNext();){
                ResultStorage o = (ResultStorage)i.next();
                ps.delete(o);
            }
        }
        {
            SeasonDAO ps = IsisFishDAOHelper.getSeasonDAO(context);
            Collection list = ps.findAll();
            for(Iterator i=list.iterator(); i.hasNext();){
                Season o = (Season)i.next();
                ps.delete(o);
            }
        }
        {
            PopulationSeasonInfoDAO ps = IsisFishDAOHelper.getPopulationSeasonInfoDAO(context);
            Collection list = ps.findAll();
            for(Iterator i=list.iterator(); i.hasNext();){
                PopulationSeasonInfo o = (PopulationSeasonInfo)i.next();
                ps.delete(o);
            }
        }
        {
            SelectivityDAO ps = IsisFishDAOHelper.getSelectivityDAO(context);
            Collection list = ps.findAll();
            for(Iterator i=list.iterator(); i.hasNext();){
                Selectivity o = (Selectivity)i.next();
                ps.delete(o);
            }
        }
        {
            SetOfVesselsDAO ps = IsisFishDAOHelper.getSetOfVesselsDAO(context);
            Collection list = ps.findAll();
            for(Iterator i=list.iterator(); i.hasNext();){
                SetOfVessels o = (SetOfVessels)i.next();
                ps.delete(o);
            }
        }
        {
            SimpleZoneDAO ps = IsisFishDAOHelper.getSimpleZoneDAO(context);
            Collection list = ps.findAll();
            for(Iterator i=list.iterator(); i.hasNext();){
                SimpleZone o = (SimpleZone)i.next();
                ps.delete(o);
            }
        }
        {
            StrategyMonthInfoDAO ps = IsisFishDAOHelper.getStrategyMonthInfoDAO(context);
            Collection list = ps.findAll();
            for(Iterator i=list.iterator(); i.hasNext();){
                StrategyMonthInfo o = (StrategyMonthInfo)i.next();
                ps.delete(o);
            }
        }
        {
            StrategyDAO ps = IsisFishDAOHelper.getStrategyDAO(context);
            Collection list = ps.findAll();
            for(Iterator i=list.iterator(); i.hasNext();){
                Strategy o = (Strategy)i.next();
                ps.delete(o);
            }
        }
        {
            TargetSpeciesDAO ps = IsisFishDAOHelper.getTargetSpeciesDAO(context);
            Collection list = ps.findAll();
            for(Iterator i=list.iterator(); i.hasNext();){
                TargetSpecies o = (TargetSpecies)i.next();
                ps.delete(o);
            }
        }
        {
            TimeUnitDAO ps = IsisFishDAOHelper.getTimeUnitDAO(context);
            Collection list = ps.findAll();
            for(Iterator i=list.iterator(); i.hasNext();){
                TimeUnit o = (TimeUnit)i.next();
                ps.delete(o);
            }
        }
        {
            TripTypeDAO ps = IsisFishDAOHelper.getTripTypeDAO(context);
            Collection list = ps.findAll();
            for(Iterator i=list.iterator(); i.hasNext();){
                TripType o = (TripType)i.next();
                ps.delete(o);
            }
        }
        {
            VesselTypeDAO ps = IsisFishDAOHelper.getVesselTypeDAO(context);
            Collection list = ps.findAll();
            for(Iterator i=list.iterator(); i.hasNext();){
                VesselType o = (VesselType)i.next();
                ps.delete(o);
            }
        }
        {
            ZoneDAO ps = IsisFishDAOHelper.getZoneDAO(context);
            Collection list = ps.findAll();
            for(Iterator i=list.iterator(); i.hasNext();){
                Zone o = (Zone)i.next();
                ps.delete(o);
            }
        }
        context.rollbackTransaction();
    }
    */
} // Persistence

