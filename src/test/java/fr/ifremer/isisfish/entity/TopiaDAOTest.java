/*
 * #%L
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 - 2014 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.entity;

import fr.ifremer.isisfish.AbstractIsisFishTest;
import fr.ifremer.isisfish.IsisFishDAOHelper;
import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.datastore.StorageException;
import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.entities.PopulationDAO;
import fr.ifremer.isisfish.simulator.SimulationParameter;
import fr.ifremer.isisfish.simulator.SimulationParameterImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;

import java.io.IOException;
import java.util.List;

/**
 * Copie d'un test important de topia concernant le flush mode.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class TopiaDAOTest extends AbstractIsisFishTest {

    /**
     * Test de creer une entité et de verifier qu'elle est
     * présente dans la persistence au sein de la transaction.
     *
     * @throws TopiaException
     * @throws IOException
     * @throws StorageException 
     */
    @Test
    public void testCreateAndFindInTransaction() throws TopiaException, IOException, StorageException {

        SimulationParameter parameters = new SimulationParameterImpl();
        SimulationStorage simulation = SimulationStorage.create("testdao", parameters);
        TopiaContext rootContext = simulation.getStorage();

        TopiaContext context = rootContext.beginTransaction();
        context.createSchema();

        PopulationDAO populationDAO = IsisFishDAOHelper.getPopulationDAO(context);

        // appel 1 find all
        Population population = populationDAO.create();
        population.setName("langoustine");
        List<Population> allPopulation = populationDAO.findAll();
        Assertions.assertEquals(1, allPopulation.size());
        context.commitTransaction();

        // recherce la personne créée dans la même transaction
        Population population2 = populationDAO.create();
        population2.setName("thon");
        allPopulation = populationDAO.findAll();
        Assertions.assertEquals(2, allPopulation.size());
        Assertions.assertTrue(allPopulation.contains(population2));

        context.rollbackTransaction();

        // meme test apres roolback
        Population population3 = populationDAO.create();
        population3.setName("requin");
        allPopulation = populationDAO.findAll();
        Assertions.assertEquals(2, allPopulation.size());
        Assertions.assertTrue(allPopulation.contains(population3));

        context.commitTransaction();
        simulation.closeStorage();
    }
}
