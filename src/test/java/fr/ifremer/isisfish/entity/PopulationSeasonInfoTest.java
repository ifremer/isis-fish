/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2010 Ifremer, Code Lutin, Cédric Pineau, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.entity;

import fr.ifremer.isisfish.AbstractIsisFishTest;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.jupiter.api.Test;
import org.nuiton.math.matrix.MatrixFactory;
import org.nuiton.math.matrix.MatrixIterator;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.math.matrix.gui.MatrixPanelEditor;

import javax.swing.Box;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import java.awt.HeadlessException;
import java.util.ArrayList;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * PopulationSeasonInfoTest.
 *
 * Created: 29 juin 2006 20:19:32
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class PopulationSeasonInfoTest extends AbstractIsisFishTest {

    /** Logger for this class */
    private static final Log log = LogFactory.getLog(PopulationSeasonInfoTest.class);

    /**
     * Test method for 'fr.ifremer.isisfish.entities.PopulationSeasonInfoImpl.getGroupChangeMatrix(Month)'
     */
    @Test
    public void testGetGroupChangeMatrix() throws InterruptedException {

        int nbrAge = 3;
        int nbrZone = 2;
        boolean groupplus = true;

        List<String> sem = new ArrayList<>();

        for (int i = 0; i < nbrAge; i++) {
            for (int j = 0; j < nbrZone; j++) {
                sem.add("g" + i + "/z" + j);
            }
        }

        MatrixND mat = MatrixFactory.getInstance().create(
                new List[] { sem, sem });
        for (MatrixIterator mi = mat.iterator(); mi.next();) {
            int[] dim = mi.getCoordinates();
            int i = dim[0];
            int j = dim[1];

            if (
            // un element de la diagonale dans le block choisi
            (i + nbrZone == j) || // calcul pour savoir s'il y a le groupe plus
                    (groupplus
                    // regarde si on est bien dans le dernier block
                            && (nbrAge - 1 == i / nbrZone)
                    // regarde si on est bien sur la diagonal
                    && (i == j))) {
                mi.setValue(1);
            }
        }


        try {
            final JDialog[] dialogs = new JDialog[1];
            SwingUtilities.invokeLater(() -> {
                dialogs[0] = new JDialog();
                MatrixPanelEditor panel = new MatrixPanelEditor(false, 800, 300);
                panel.setMatrix(mat);
                JOptionPane.showMessageDialog(dialogs[0], panel, t("Spacialized visualisation"), JOptionPane.INFORMATION_MESSAGE);
            });

            Thread t = new Thread(() -> {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    // do nothing
                }
                dialogs[0].dispose();
            });
            t.start();

            // it would really be nice to wait the thread, otherwise the test
            // means nothing!!!
            t.join();
        } catch (HeadlessException he) {
            if (log.isErrorEnabled()) {
                log.error("No X11 display available", he);
            }
        }
    }

    /**
     * Converte no spacialized matrix to spacialized matrix
     */
    @Test
    public void testSpacializeLengthChangeMatrix() throws InterruptedException {

        int nbsecteurs = 2;
        int nbclasses = 3;

        MatrixND mat = MatrixFactory.getInstance().create(
                new int[] { nbclasses, nbclasses });
        int i = 1;
        for (MatrixIterator mi = mat.iterator(); mi.next();) {
            mi.setValue(i++);
        }

        List<String> sem = new ArrayList<>();

        for (i = 0; i < nbclasses; i++) {
            for (int j = 0; j < nbsecteurs; j++) {
                sem.add("g" + i + "/z" + j);
            }
        }

        MatrixND bigmat = MatrixFactory.getInstance().create(
                new List[] { sem, sem });

        for (i = 0; i < nbclasses; i++) {
            for (int j = 0; j < nbclasses; j++) {
                MatrixND matId = MatrixFactory.getInstance().matrixId(
                        nbsecteurs);
                matId.mults(mat.getValue(i, j));
                bigmat.paste(new int[] { i * nbsecteurs, j * nbsecteurs },
                        matId);
            }
        }

        try {
            final JDialog[] dialogs = new JDialog[1];
            SwingUtilities.invokeLater(() -> {
                dialogs[0] = new JDialog();
                MatrixPanelEditor panel = new MatrixPanelEditor(false, 800, 300);
                panel.setMatrix(mat);
                MatrixPanelEditor bigpanel = new MatrixPanelEditor(false, 800, 300);
                bigpanel.setMatrix(bigmat);

                Box box = Box.createVerticalBox();
                box.add(panel);
                box.add(bigpanel);
                JOptionPane.showMessageDialog(dialogs[0], box, t("Spacialized visualisation"), JOptionPane.INFORMATION_MESSAGE);
            });

            Thread t = new Thread(() -> {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    // do nothing
                }
                dialogs[0].dispose();
            });
            t.start();

            // it would really be nice to wait the thread, otherwise the test
            // means nothing!!!
            t.join();
        } catch (HeadlessException he) {
            if (log.isErrorEnabled()) {
                log.error("No X11 display available", he);
            }
        }
    }

}
