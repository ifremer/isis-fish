/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2010 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.entity;

import fr.ifremer.isisfish.IsisFishDAOHelper;
import fr.ifremer.isisfish.IsisFishDAOHelper.IsisFishEntityEnum;
import fr.ifremer.isisfish.entities.Equation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.nuiton.topia.TopiaException;
import org.nuiton.topia.persistence.TopiaEntity;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/**
 * Test for convention defined on entities.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class PersistenceConventionTest {

    /** Logger for this class. */
    private static final Log log = LogFactory.getLog(PersistenceConventionTest.class);

    /**
     * A convention has been defined on all entities for equation getter/setter.
     * 
     * If getter is :
     * <pre>Equation getXXX()</pre>
     * setter has to be
     * <pre>void setXXXContent(String)</pre>
     * 
     * @throws TopiaException 
     * @throws NoSuchMethodException 
     * @throws SecurityException 
     */
    @Test
    public void testEquationSetterName() throws TopiaException, SecurityException, NoSuchMethodException {

        for (IsisFishEntityEnum entityEnum : IsisFishDAOHelper.IsisFishEntityEnum.values()) {
            Class<? extends TopiaEntity> entity = entityEnum.getImplementation();

            if (log.isDebugEnabled()) {
                log.debug("Testing convention on : " + entity);
            }

            for (Method method : entity.getMethods()) {
                Class<?> returnType = method.getReturnType();

                // method return an equation, search for getter
                if (returnType.isAssignableFrom(Equation.class)) {
                    String methodName = method.getName();
                    methodName = methodName.replaceFirst("g", "s") + "Content";

                    try {
                        Method methodSet = entity.getMethod(methodName, String.class);
                        
                        Assertions.assertTrue(Modifier.isPublic(methodSet.getModifiers()), "Method " + entity.getName() + "#" +
                                methodName + "(" + String.class.getName() + ") is not accessible");
                    }
                    catch (NoSuchMethodException ex) {
                        Assertions.fail("Method " + entity.getName() + "#" +
                            methodName + "(" + String.class.getName() + ") not found");
                    }
                }
            }
        }
    }
}
