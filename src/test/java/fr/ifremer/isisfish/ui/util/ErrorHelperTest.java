/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2010 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.util;

import fr.ifremer.isisfish.AbstractIsisFishTest;
import fr.ifremer.isisfish.util.ErrorHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.awt.HeadlessException;

/**
 * Test class for ErrorHelper.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
@Disabled
public class ErrorHelperTest extends AbstractIsisFishTest {

    /** Logger for this class */
    private static final Log log = LogFactory.getLog(ErrorHelperTest.class);
    
    @Test
    public void showErrorDialogTest() {
        try {
            IllegalArgumentException e = new IllegalArgumentException("Just for test");
            
            ErrorHelper.showErrorDialog(null, "This is an error pane test", e);
        }
        catch(HeadlessException e) {
            if (log.isWarnEnabled()) {
                log.warn("User interface non available", e);
            }
        }
    }
}
