/*-
 * #%L
 * ISIS-Fish
 * %%
 * Copyright (C) 2020 - 2021 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.keystore;

import fr.ifremer.isisfish.AbstractIsisFishTest;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

public class PasswordManagerTest extends AbstractIsisFishTest {

    @Test
    public void storeAndRestorePassword() {
        PasswordManager passwordManager = PasswordManager.getInstance();

        passwordManager.addPassword("testkey@testlogin", "testpwd", true);
        passwordManager.parsePasswords();
        String pwd = passwordManager.getPassword("testkey@testlogin", false);
        Assertions.assertThat(pwd).isEqualTo("testpwd");
    }

    /**
     * Disabled : UI test.
     */
    @Test
    @Disabled
    public void displayKeystoreUI() {
        PasswordManager passwordManager = PasswordManager.getInstance();
        passwordManager.getPassword("testkey1@testlogin1", false);
    }
}
