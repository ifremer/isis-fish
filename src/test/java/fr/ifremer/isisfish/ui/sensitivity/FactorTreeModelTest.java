/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2012 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.sensitivity;

import fr.ifremer.isisfish.AbstractIsisFishTest;
import fr.ifremer.isisfish.simulator.sensitivity.Distribution;
import fr.ifremer.isisfish.simulator.sensitivity.Factor;
import fr.ifremer.isisfish.simulator.sensitivity.FactorGroup;
import fr.ifremer.isisfish.simulator.sensitivity.domain.ContinuousDomain;
import fr.ifremer.isisfish.simulator.sensitivity.domain.DiscreteDomain;
import fr.ifremer.isisfish.ui.sensitivity.model.FactorTreeCellRenderer;
import fr.ifremer.isisfish.ui.sensitivity.model.FactorTreeModel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.nuiton.math.matrix.MatrixFactory;
import org.nuiton.math.matrix.MatrixND;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import java.awt.HeadlessException;

import static org.nuiton.i18n.I18n.t;

/**
 * Sensitivity tree model test.
 *
 * Created: 29 juin 2006 20:19:32
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class FactorTreeModelTest extends AbstractIsisFishTest {

    /** Logger for this class */
    private static final Log log = LogFactory.getLog(FactorTreeModelTest.class);

    @BeforeAll
    public static void disableTests() {
        Assumptions.assumeTrue(!java.awt.GraphicsEnvironment.isHeadless());
    }

    /**
     * Return une liste de facteur à afficher.
     * 
     * @return la liste des facteur
     */
    protected FactorGroup getFactorGroup() {
        
        Factor factor1 = new Factor(
                "testint");
        ContinuousDomain domain1 = new ContinuousDomain(Distribution.QUNIFMM);
        domain1.addDistributionParam("min", 0.0);
        domain1.addDistributionParam("max", 50.0);
        factor1.setDomain(domain1);
        factor1.setPath("org.nuiton.factor#1234567890#0.12242345354#name");
        factor1.setValueForIdentifier(0.5);

        // matrix 1
        MatrixND matrix1 = MatrixFactory.getInstance().create("test1",
                new int[] { 3, 2 }, new String[] { "col1", "col2" });
        matrix1.setValue(new int[] { 0, 0 }, 13);
        matrix1.setValue(new int[] { 0, 1 }, -14);
        matrix1.setValue(new int[] { 1, 0 }, 21);
        matrix1.setValue(new int[] { 1, 1 }, 2);
        matrix1.setValue(new int[] { 2, 0 }, 12);
        matrix1.setValue(new int[] { 2, 1 }, -1);

        // matrix 2
        MatrixND matrix2 = MatrixFactory.getInstance().create("test2",
                new int[] { 2, 3 }, new String[] { "col1", "col2" });
        matrix2.setValue(new int[] { 0, 0 }, 9999);
        matrix2.setValue(new int[] { 0, 1 }, 15000);
        matrix2.setValue(new int[] { 0, 2 }, -40000);
        matrix2.setValue(new int[] { 1, 0 }, 21345);
        matrix2.setValue(new int[] { 1, 1 }, 81000);
        matrix2.setValue(new int[] { 1, 2 }, -13000);

        // factor
        Factor factor2 = new Factor(
                "testmatrix");
        DiscreteDomain domain2 = new DiscreteDomain();
        domain2.getValues().put("m1", matrix1);
        domain2.getValues().put("m2", matrix2);
        factor2.setDomain(domain2);
        factor2.setPath("org.nuiton.math.matrix.MatrixND#563456293453#2.456347646#dim");
        factor2.setValueForIdentifier("m2");

        FactorGroup factorGroup = new FactorGroup("test");
        factorGroup.addFactor(factor1);
        factorGroup.addFactor(factor2);

        return factorGroup;
    }

    /*
     * Test tree rendering.
     */
    @Test
    public void testJTreeModel() throws InterruptedException {

        try {
            final JDialog[] dialogs = new JDialog[1];
            SwingUtilities.invokeLater(() -> {
                dialogs[0] = new JDialog();
                JTree tree = new JTree();
                tree.setRootVisible(true);
                FactorTreeModel model = new FactorTreeModel(getFactorGroup());
                tree.setModel(model);
                tree.setCellRenderer(new FactorTreeCellRenderer());
                JOptionPane.showMessageDialog(dialogs[0], tree, t("Tree factor model"), JOptionPane.INFORMATION_MESSAGE);
            });

            Thread t = new Thread(() -> {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    // do nothing
                }
                dialogs[0].dispose();
            });
            t.start();

            // it would really be nice to wait the thread, otherwise the test
            // means nothing!!!
            t.join();
        } catch (HeadlessException he) {
            if (log.isErrorEnabled()) {
                log.error("No X11 display available", he);
            }
        }
    }
}
