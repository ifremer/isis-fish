/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2018 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.ui;

import fr.ifremer.isisfish.AbstractIsisFishTest;
import org.assertj.swing.edt.FailOnThreadViolationRepaintManager;
import org.assertj.swing.edt.GuiActionRunner;
import org.assertj.swing.edt.GuiQuery;
import org.assertj.swing.exception.WaitTimedOutError;
import org.assertj.swing.fixture.FrameFixture;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

/**
 * Test commun qui permet d'instancier une fenetre Isis à partir de laquelle dépendent
 * tous les tests.
 */
public abstract class AbstractAssertjIT extends AbstractIsisFishTest {

    protected FrameFixture mainWindow;

    @BeforeAll
    public static void setUpOnce() {
        Assumptions.assumeTrue(!java.awt.GraphicsEnvironment.isHeadless());
        FailOnThreadViolationRepaintManager.install();
    }

    @BeforeEach
    public void setUp() {
        WelcomeUI frame = GuiActionRunner.execute(new GuiQuery<WelcomeUI>() {
            protected WelcomeUI executeInEDT() {
              return new WelcomeUI(new WelcomeContext()); 
            }
        });
        mainWindow = new FrameFixture(frame);
        try {
            mainWindow.show(); // shows the frame to test
        } catch (WaitTimedOutError e) {
            // ok, but not a problem, isis is so slow...
        }
        mainWindow.maximize();
    }
    
    @AfterEach
    public void tearDown() {
        mainWindow.cleanUp();
    }
}
