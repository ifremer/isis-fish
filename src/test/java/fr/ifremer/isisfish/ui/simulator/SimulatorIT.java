/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.ui.simulator;

import fr.ifremer.isisfish.ui.AbstractAssertjIT;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * UI fest tests related to simulation params and launch.
 */
public class SimulatorIT extends AbstractAssertjIT {

    @BeforeEach
    @Override
    public void setUp() {
        super.setUp();
        mainWindow.tabbedPane("simulTabs").selectTab(1);
    }

    /**
     * Param and launch a simple simulation.
     * 
     * @throws InterruptedException
     */
    @Test
    public void testSimulationAndViewResults() throws InterruptedException {
        // first tab
        mainWindow.tabbedPane("bodyTabbedPane").selectTab(0);
        mainWindow.comboBox("fieldSimulParamsRegion").selectItem("DemoRegion");
        mainWindow.textBox("fieldSimulParamsName").setText("test");
        mainWindow.textBox("fieldSimulParamsDesc").setText("Ho la description de ouf :D");
        mainWindow.list("listSimulParamsStrategies").selectItem("stratest");
        mainWindow.list("listSimulParamsPopulations").selectItem("popage");
        
        // second tab
        mainWindow.tabbedPane("bodyTabbedPane").selectTab(4);
        mainWindow.tabbedPane("bodyTabbedPane").selectTab(5);
        mainWindow.tabbedPane("bodyTabbedPane").selectTab(6);
        
        // back to first tab
        mainWindow.tabbedPane("bodyTabbedPane").selectTab(0);
        mainWindow.comboBox("comboSelLauncher").selectItem(2);
        mainWindow.button("buttonSimulParamsSimulate").click();
        
        // may be on tab (Queue)
        int count = 0;
        boolean done = false;
        while (count < 30 && !done) {
            done = mainWindow.table("queueTableDone").rowCount() == 1;
            Thread.sleep(1000);
            count++;
        }
        
        // tab (result)
        mainWindow.tabbedPane("simulTabs").selectTab(3);
        // was working in the past:
        //mainWindow.textBox("filterText").setText("test");
        mainWindow.comboBox("filterBox").selectItem(0);
        mainWindow.button("openButton").click();
    }

}
