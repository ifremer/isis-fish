/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2015 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.ui.input;

import fr.ifremer.isisfish.ui.AbstractAssertjIT;
import org.assertj.swing.core.matcher.JButtonMatcher;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

/**
 * Swing tests.
 */
public class InputIT extends AbstractAssertjIT {

    @BeforeEach
    @Override
    public void setUp() {
        super.setUp();
        mainWindow.tabbedPane("simulTabs").selectTab(0);
    }

    protected void openDemoRegion() {
        mainWindow.comboBox("fieldCurrentRegion").selectItem("DemoRegion");
    }

    /**
     * Just open test.
     */
    @Test
    public void testOpenDemoRegion() {
        openDemoRegion();
        mainWindow.comboBox("fieldCurrentRegion").requireSelection(1);
    }

    /**
     * Edit and save region test.
     */
    @Test
    public void testEditRegionName() {
        openDemoRegion();
        mainWindow.tree("fisheryRegionTree").selectRow(0);
        mainWindow.textBox("fieldRegion").selectAll().enterText("New region name");
        mainWindow.button("save").click();
        Assertions.assertEquals(mainWindow.tree("fisheryRegionTree").valueAt(0), "New region name");
    }

    /**
     * Open, export, import and rename region test.
     * 
     * @throws IOException
     */
    @Test
    public void testImportRegion() throws IOException {
        openDemoRegion();
        mainWindow.menuItem("menuRegionExport").click();
        File f = File.createTempFile("isis-export", ".zip"); // bug file exists
        f.deleteOnExit();
        mainWindow.fileChooser().selectFile(f).approve();
        mainWindow.optionPane().yesButton().click(); // bug file exists
        mainWindow.menuItem("menuRegionImportRename").click();
        mainWindow.fileChooser().selectFile(f).approve();

        mainWindow.dialog().textBox().enterText("Testimport");
        mainWindow.dialog().button(JButtonMatcher.withText("OK")).click();
        mainWindow.comboBox("fieldCurrentRegion").selectItem("Testimport");
        Assertions.assertEquals(mainWindow.tree("fisheryRegionTree").valueAt(0), "Testimport");
    }
}
