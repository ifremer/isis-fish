/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2009 - 2012 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.isisfish.ui.input;

import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.entities.PopulationImpl;
import fr.ifremer.isisfish.entities.PopulationSeasonInfo;
import fr.ifremer.isisfish.entities.PopulationSeasonInfoImpl;
import fr.ifremer.isisfish.types.Month;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Test InputVerifier.
 */
public class InputSaveVerifierTest {

    /**
     * Test toString method.
     * @throws Exception 
     */
    @Test
    public void testToString() throws Exception {
        Population pop = new PopulationImpl();
        pop.setName("Test1");
        Assertions.assertEquals("Test1", new InputSaveVerifier().toString(pop));
        
        PopulationSeasonInfo psi = new PopulationSeasonInfoImpl();
        psi.setPopulation(pop);
        psi.setFirstMonth(Month.JANUARY);
        psi.setLastMonth(Month.MARCH);
        Assertions.assertEquals("Test1 saison janvier-mars", new InputSaveVerifier().toString(psi));
    }
}
