/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2002 - 2012 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.widget;

import fr.ifremer.isisfish.ui.widget.interval.Interval;
import fr.ifremer.isisfish.ui.widget.interval.IntervalPanel;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.swing.SwingUtilities;

/**
 * Test class for {@link IntervalPanel}.
 * 
 * @author chatellier
 * @version $Revision$
 * 
 * Last update : $Date$
 * By : $Author$
 */
public class IntervalPanelTest {

    @BeforeAll
    public static void disableTests() {
        Assumptions.assumeTrue(!java.awt.GraphicsEnvironment.isHeadless());
    }

    /**
     * Test to display a frame with interval panel.
     * 
     * @throws InterruptedException 
     */
    @Test
    public void testIntervalPanelDisplay() throws InterruptedException {

        final FormInterval[] frames = new FormInterval[1];
        SwingUtilities.invokeLater(() -> {
            Interval i = new Interval(0, 11);
            frames[0] = new FormInterval(i);
            frames[0].setVisible(true);
        });

        Thread t = new Thread(() -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                // do nothing
            }
            frames[0].dispose();
        });
        t.start();

        // it would really be nice to wait the thread, otherwise the test
        // means nothing!!!
        t.join();
    }
}
