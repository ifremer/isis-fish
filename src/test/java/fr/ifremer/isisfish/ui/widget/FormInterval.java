/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2001 - 2011 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.ui.widget;

import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import fr.ifremer.isisfish.ui.widget.interval.Interval;
import fr.ifremer.isisfish.ui.widget.interval.IntervalPanel;

/**
 * FormInterval.
 */
public class FormInterval extends Frame {
    
    /** serialVersionUID. */
    private static final long serialVersionUID = 4911081593147906648L;

    static class QuitListener extends WindowAdapter {
        public void windowClosing(WindowEvent e) {
            e.getWindow().dispose();
        }
    }

    public FormInterval(Interval i) {
        IntervalPanel ip = new IntervalPanel();
        this.add(ip);
        // ip.setEnabled(false);
        ip.setModel(i);

        this.addWindowListener(new QuitListener());
        pack();
    }
}
