/*
 * #%L
 * IsisFish
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2010 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.ifremer.isisfish.logging;

import fr.ifremer.isisfish.AbstractIsisFishTest;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@Disabled
public class SimulationLoggerUtilTest extends AbstractIsisFishTest {

    static private final String category = "fr.ifremer.isisfish.logging";

    static private final String appenderId = category+"."+"myLogger_" + System.currentTimeMillis();

    static private Log log = LogFactory.getLog(category);

    @Test
    public void testSimulationLoggerInfo() throws Exception {

        Log logInit = LogFactory.getLog(appenderId);
        Log logInit2 = LogFactory.getLog(appenderId + ".hip");

        List<String> messagesToWrite = new ArrayList<>();
        List<String> messagesToSkip = new ArrayList<>();

        logInfo(logInit, "before new config", null, null);
        logInfo(logInit2, "before new config", null, null);
        logInfo(log, "before new config", null, null);

        // use a logger we know to be scan as been keep for original level
        Logger hibernatelog = LogManager.getLogger("org.hibernate");

        Level hibernateLogLevel = hibernatelog.getLevel();

        String loggerFile = new File(getTestDirectory(), SimulationLoggerUtilTest.class.getSimpleName() + "_" + System.nanoTime() + ".log").getAbsolutePath();

        // check hibernate level was not changed
        Assertions.assertEquals(hibernateLogLevel, LogManager.getLogger("org.hibernate").getLevel());
        final Log logger = LogFactory.getLog(appenderId);


        String message = "THIS LOG MUST NOT APPEAR IN FILE : hibernate logger's level was kept to his original level (" + hibernateLogLevel + ") ";
        hibernatelog.info(message);
        messagesToSkip.add(message);
        message = "THIS LOG MUST APPEAR IN FILE : hibernate logger's level was kept to his original level (" + hibernateLogLevel + ") ";
        // to be sure the message will be treated, use fatal level...
        hibernatelog.fatal(message);
        messagesToWrite.add(message);

        logInfo(logInit, "after new config using old logger", messagesToWrite, messagesToSkip);

        logInfo(logger, "after new config using new logger", messagesToWrite, messagesToSkip);
        logInfo(log, "after new config using new logger", messagesToWrite, messagesToSkip);

        final String message0 = "BLOCK LOG must not appear in log file !!! since coming from another thread";
        messagesToSkip.add(message0);

        // run a thread and try to logInfo from it
        Thread t = new Thread(() -> {
            // here we are try to logInfo in a bad thread should no appear
            // in logInfo file
            logger.info(message0);
            log.info(message0);
        });
        t.start();

        // it would really be nice to wait the thread, otherwise the test
        // means nothing!!!
        t.join();
        Assertions.assertFalse(t.isAlive());

        message = "appender was destoyed WE MUST NOT SEE this message in simulation log file!!!";
        logInit.info(message);
        messagesToSkip.add(message);

        logInfo(logInit2, "ANOTHER CHANGE METHOD!!!", messagesToSkip, messagesToSkip);
        logInfo(log, "ANOTHER CHANGE METHOD!!!", messagesToSkip, messagesToSkip);

        File logFile = new File(loggerFile);

        String logFileContent = FileUtils.readFileToString(logFile, StandardCharsets.UTF_8);

        log.info("++ File content ----------------------------------------------------------");
        for (Object o : logFileContent.split("\n")) {
            log.info(o);
        }
        log.info("-- File content ----------------------------------------------------------");
        log.info("-- nb messages skipped : " + messagesToSkip.size());
        log.info("-- nb messages written : " + messagesToWrite.size());

        System.out.println("test messages to be skipped");
        for (String s : messagesToSkip) {
            Assertions.assertFalse(logFileContent.contains(s), "should not have write this entry : '" + s + "'");
        }

        System.out.println("test messages to be written");
        for (String s : messagesToWrite) {
            Assertions.assertTrue(logFileContent.contains(s), "should have write this entry : '" + s + "'");
        }

        logFile.delete();

    }

    @Test
    public void testSimulationLoggerDebug() throws Exception {

        Log logInit = LogFactory.getLog(appenderId);
        Log logInit2 = LogFactory.getLog(appenderId + ".hip");

        List<String> messagesToWrite = new ArrayList<>();
        List<String> messagesToSkip = new ArrayList<>();

        logDebug(logInit, "before new config", null);
        logDebug(logInit2, "before new config", null);
        logDebug(log, "before new config", null);

        // use a logger we know to be scan as been keep for original level
        Logger hibernatelog = LogManager.getLogger("org.hibernate");

        Level hibernateLogLevel = hibernatelog.getLevel();


        String loggerFile = "testLog.log";

        // check hibernate level was not changed
        Assertions.assertEquals(hibernateLogLevel, LogManager.getLogger("org.hibernate").getLevel());
        final Log logger = LogFactory.getLog(appenderId);


        String message = "THIS LOG MUST NOT APPEAR IN FILE : hibernate logger's level was kept to his original level (" + hibernateLogLevel + ") ";
        hibernatelog.info(message);
        messagesToSkip.add(message);
        message = "THIS LOG MUST APPEAR IN FILE : hibernate logger's level was kept to his original level (" + hibernateLogLevel + ") ";
        // to be sure the message will be treated, use fatal level...
        hibernatelog.fatal(message);
        messagesToWrite.add(message);

        logDebug(logInit, "after new config using old logger", messagesToWrite);

        logDebug(logger, "after new config using new logger", messagesToWrite);
        logDebug(log, "after new config using new logger", messagesToWrite);

        final String message0 = "BLOCK LOG must not appear in log file !!! since coming from another thread";
        messagesToSkip.add(message0);

        // run a thread and try to logInfo from it
        Thread t = new Thread(() -> {
            // here we are try to logInfo in a bad thread should no appear
            // in logInfo file
            logger.info(message0);
            log.info(message0);
        });
        t.start();

        // it would really be nice to wait the thread, otherwise the test
        // means nothing!!!
        t.join();
        Assertions.assertFalse(t.isAlive());

        message = "appender was destoyed WE MUST NOT SEE this message in simulation log file!!!";
        logInit.info(message);
        messagesToSkip.add(message);

        logDebug(logInit2, "ANOTHER CHANGE METHOD!!!", messagesToSkip);
        logDebug(log, "ANOTHER CHANGE METHOD!!!", messagesToSkip);

        File logFile = new File(loggerFile);

        String logFileContent = FileUtils.readFileToString(logFile, StandardCharsets.UTF_8);

        log.info("++ File content ----------------------------------------------------------");
        for (Object o : logFileContent.split("\n")) {
            log.info(o);
        }
        log.info("-- File content ----------------------------------------------------------");
        log.info("-- nb messages skipped : " + messagesToSkip.size());
        log.info("-- nb messages written : " + messagesToWrite.size());


        for (String s : messagesToSkip) {
            Assertions.assertFalse(logFileContent.contains(s));
        }

        for (String s : messagesToWrite) {
            Assertions.assertTrue(logFileContent.contains(s), s);
        }

        logFile.delete();

    }

    private void logInfo(Log logger, String prefix, List<String> messagesToWrite, List<String> messagesToSkip) {
        String message;


        logger.debug(message = prefix + " debug");
        if (messagesToWrite != null) {
            messagesToSkip.add(message);
        }

        logger.info(message = prefix + " info");
        if (messagesToWrite != null) {
            messagesToWrite.add(message);
        }

        logger.warn(message = prefix + " warn");
        if (messagesToWrite != null) {
            messagesToWrite.add(message);
        }

        logger.error(message = prefix + " error");
        if (messagesToWrite != null) {
            messagesToWrite.add(message);
        }

        logger.fatal(message = prefix + " fatal");
        if (messagesToWrite != null) {
            messagesToWrite.add(message);
        }
    }

    private void logDebug(Log logger, String prefix, List<String> messagesToWrite) {
        String message;


        logger.debug(message = prefix + " debug");
        if (messagesToWrite != null) {
            messagesToWrite.add(message);
        }

        logger.info(message = prefix + " info");
        if (messagesToWrite != null) {
            messagesToWrite.add(message);
        }

        logger.warn(message = prefix + " warn");
        if (messagesToWrite != null) {
            messagesToWrite.add(message);
        }

        logger.error(message = prefix + " error");
        if (messagesToWrite != null) {
            messagesToWrite.add(message);
        }

        logger.fatal(message = prefix + " fatal");
        if (messagesToWrite != null) {
            messagesToWrite.add(message);
        }
    }
}
