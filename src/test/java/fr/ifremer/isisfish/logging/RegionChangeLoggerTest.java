package fr.ifremer.isisfish.logging;

/*
 * #%L
 * ISIS-Fish
 * %%
 * Copyright (C) 2017 Ifremer, Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.isisfish.AbstractIsisFishTest;
import fr.ifremer.isisfish.IsisFishDAOHelper;
import fr.ifremer.isisfish.datastore.RegionStorage;
import fr.ifremer.isisfish.datastore.StorageException;
import fr.ifremer.isisfish.entities.Cell;
import fr.ifremer.isisfish.entities.CellDAO;
import fr.ifremer.isisfish.entities.CellImpl;
import fr.ifremer.isisfish.entities.FisheryRegion;
import fr.ifremer.isisfish.entities.FisheryRegionDAO;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.nuiton.topia.TopiaContext;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class RegionChangeLoggerTest extends AbstractIsisFishTest {

    /**
     * Test que les evenements sur les entités existantes sont bien logués.
     * 
     * @throws IOException si le fichier ne peux pas être lu
     */
    @Test
    public void testLoggingOnExistingEntities() throws IOException {
        RegionStorage regionStorage = RegionStorage.getRegion("BaseMotosICA");
        TopiaContext storage = regionStorage.getStorage();
        TopiaContext topiaContext = storage.beginTransaction();
        RegionChangeLogger regionChangeLogger = new RegionChangeLogger(regionStorage, topiaContext);
        topiaContext.addTopiaEntityListener(regionChangeLogger);

        FisheryRegionDAO fisheryRegionDAO = IsisFishDAOHelper.getFisheryRegionDAO(topiaContext);
        FisheryRegion baseMotosICA = fisheryRegionDAO.findByName("BaseMotosICA");
        baseMotosICA.setCellLengthLongitude(0.25f);
        baseMotosICA.setCellLengthLatitude(0.25f);
        baseMotosICA.update();
        topiaContext.commitTransaction();
        regionStorage.closeStorage();

        List<String> lines = FileUtils.readLines(regionChangeLogger.logFile, StandardCharsets.UTF_8);
        Assertions.assertEquals(3, lines.size());
        Assertions.assertTrue(lines.get(0).contains("\"date\";\"type\";\"entity\";\"name\";\"field\";\"oldvalue\";\"newvalue\""));
        Assertions.assertTrue(lines.get(1).contains("\"cellLengthLongitude\";\"0.5\";\"0.25\""));
        Assertions.assertTrue(lines.get(2).contains("\"cellLengthLatitude\";\"0.5\";\"0.25\""));
    }

    /**
     * Test que les evenements sur les nouvelles entités sont bien logués (ca ne fonctionnait pas à la première
     * implémentation pour les nouvelles entités).
     *
     * @throws IOException si le fichier ne peux pas être lu
     * @throws StorageException 
     */
    @Test
    public void testLoggingOnNewEntities() throws IOException, StorageException {
        RegionStorage regionStorage = RegionStorage.create("test");
        TopiaContext storage = regionStorage.getStorage();
        TopiaContext topiaContext = storage.beginTransaction();
        RegionChangeLogger regionChangeLogger = new RegionChangeLogger(regionStorage, topiaContext);
        topiaContext.addTopiaEntityListener(regionChangeLogger);
        CellDAO cellDAO = IsisFishDAOHelper.getCellDAO(topiaContext);

        Cell cell = new CellImpl();
        cell.setName("test");
        cellDAO.create(cell);
        
        Cell cell2 = cellDAO.create("name", "test 2");
        cell2.setName("test 2 (bis)");
        cellDAO.update(cell2);
        topiaContext.commitTransaction();

        cellDAO.delete(cell);
        cellDAO.delete(cell2);
        topiaContext.commitTransaction();
        regionStorage.closeStorage();

        List<String> lines = FileUtils.readLines(regionChangeLogger.logFile, StandardCharsets.UTF_8);
        Assertions.assertEquals(6, lines.size());
        Assertions.assertTrue(lines.get(0).contains("\"date\";\"type\";\"entity\";\"name\";\"field\";\"oldvalue\";\"newvalue\""));
        Assertions.assertTrue(lines.get(1).contains("\"CREATE\";\"Cell\";\"test\""));
        Assertions.assertTrue(lines.get(5).contains("\"DELETE\";\"Cell\";\"test 2 (bis)\""));
    }
}
