/*
 * #%L
 * IsisFish data
 * %%
 * Copyright (C) 2006 - 2014 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package simulationplans;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.simulator.SimulationPlanContext;
import fr.ifremer.isisfish.simulator.SimulationPlanIndependent;
import fr.ifremer.isisfish.annotations.Doc;

/**
 * ExempleSimulationsIndependantes.java
 * 
 * Created: 2 mars 2007
 * 
 * @author bpoussin
 * @version $Revision$
 * 
 * Last update: $Date$ by : $Author$
 */
public class SimulationNumberRestriction implements SimulationPlanIndependent {

    /** to use log facility, just put in your code: log.info("..."); */
    static private Log log = LogFactory.getLog(SimulationNumberRestriction.class);

    @Doc(value = "do the doc of param max")
    public int param_max = 10;

    protected String[] necessaryResult = {
        // put here all necessary result for this rule
        // example: 
        // ResultName.MATRIX_BIOMASS,
        // ResultName.MATRIX_NET_VALUE_OF_LANDINGS_PER_STRATEGY_MET,
    };

    @Override
    public String[] getNecessaryResult() {
        return this.necessaryResult;
    }

    /**
     * Permet d'afficher a l'utilisateur une aide sur le plan.
     * 
     * @return L'aide ou la description du plan
     */
    @Override
    public String getDescription() throws Exception {
        return "Permit to specify maximum simulation numbers";
    }

    /**
     * Appelé au démarrage de la simulation, cette méthode permet d'initialiser
     * des valeurs
     * 
     * @param context La simulation pour lequel on utilise cette regle
     */
    @Override
    public void init(SimulationPlanContext context) throws Exception {

    }

    /**
     * Call before each simulation
     * 
     * @param context plan context
     * @param nextSimulation storage used for next simulation
     * @return true if we must do next simulation, false to stop plan
     * @throws Exception
     */
    @Override
    public boolean beforeSimulation(SimulationPlanContext context,
            SimulationStorage nextSimulation) throws Exception {
        boolean result = nextSimulation.getParameter().getSimulationPlanNumber() < param_max;
        return result;
    }

    /**
     * Call after each simulation
     * 
     * @param context plan context
     * @param lastSimulation storage used for last simulation
     * @return true if we must do next simulation, false to stop plan
     * @throws Exception
     */
    @Override
    public boolean afterSimulation(SimulationPlanContext context,
            SimulationStorage lastSimulation) throws Exception {
        return true;
    }

}
