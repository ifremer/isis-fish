/*
 * #%L
 * IsisFish data
 * %%
 * Copyright (C) 2006 - 2014 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package exports;

import java.io.Writer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixIterator;
import org.nuiton.math.matrix.MatrixND;

import scripts.ResultName;
import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.entities.Metier;
import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.entities.PopulationGroup;
import fr.ifremer.isisfish.entities.Zone;
import fr.ifremer.isisfish.export.Export;
import fr.ifremer.isisfish.types.TimeStep;

/**
 * RejetsNombre.java
 * 
 * Created: 23 novembre 2006
 * 
 * @author anonymous
 * @version $Revision$
 * 
 * Last update: $Date$ by : $Author$
 */
public class RejetsNombre implements Export {

    /** to use log facility, just put in your code: log.info("..."); */
    static private Log log = LogFactory.getLog(RejetsNombre.class);

    protected String[] necessaryResult = {
        ResultName.MATRIX_DISCARDS_PER_STR_MET_PER_ZONE_POP
    };

    @Override
    public String[] getNecessaryResult() {
        return this.necessaryResult;
    }

    @Override
    public String getExportFilename() {
        return "RejetsNombre";
    }

    @Override
    public String getExtensionFilename() {
        return ".csv";
    }

    @Override
    public String getDescription() {
        return "Export les rejets en nombre de la simulation. tableau pop;metier;id;zone;date;nombre";
    }

    @Override
    public void export(SimulationStorage simulation, Writer out)
            throws Exception {
        TimeStep lastStep = simulation.getResultStorage().getLastStep();

        for (Population pop : simulation.getParameter().getPopulations()) {
            for (TimeStep step = new TimeStep(0); !step.after(lastStep); step = step
                    .next()) {
                MatrixND mat = simulation.getResultStorage().getMatrix(step,
                        pop, ResultName.MATRIX_DISCARDS_PER_STR_MET_PER_ZONE_POP);
                if (mat != null) { // can be null if simulation is stopped before last year simulation
                    mat = mat.sumOverDim(0); //sum on strategy
                    for (MatrixIterator i = mat.iterator(); i.hasNext();) {
                        i.next();
                        Object[] sems = i.getSemanticsCoordinates();
                        Metier metier = (Metier) sems[1];
                        PopulationGroup group = (PopulationGroup) sems[2];
                        Zone zone = (Zone) sems[3];

                        double val = i.getValue();
                        out.write(pop.getName() + ";" + metier.getName() + ";"
                                + group.getId() + ";" + zone.getName() + ";"
                                + step.getStep() + ";" + val + "\n");
                    }
                }
            }
        }
    }
}
