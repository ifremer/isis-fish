/*
 * #%L
 * IsisFish data
 * %%
 * Copyright (C) 2006 - 2014 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package exports;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Writer;

import org.nuiton.topia.TopiaContext;

import fr.ifremer.isisfish.entities.*;
import fr.ifremer.isisfish.export.Export;
import fr.ifremer.isisfish.datastore.SimulationStorage;

/**
 * RegionDefinition.java
 *
 * Created: 17 janvier 2007
 *
 * @author bpoussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class RegionDefinition implements Export {

    /** to use log facility, just put in your code: log.info("..."); */
    static private Log log = LogFactory.getLog(RegionDefinition.class);

    protected String [] necessaryResult = {
        // put here all necessary result for this rule
        // example: 
        // ResultName.MATRIX_BIOMASS,
        // ResultName.MATRIX_NET_VALUE_OF_LANDINGS_PER_STRATEGY_MET,
    };

    @Override
    public String[] getNecessaryResult() {
        return this.necessaryResult;
    }

    @Override
    public String getExportFilename() {
        return "RegionDefinition";
    }

    @Override
    public String getExtensionFilename() {
        return ".csv";
    }

    @Override
    public String getDescription() {
        return "Export region description";
    }

    @Override
    public void export(SimulationStorage simulation, Writer out) throws Exception {
        // NomRegion LatituteMin LatitudeMax LongitudeMin LongitudeMax PasLatitude PasLongitude
        TopiaContext tx = simulation.getStorage().beginTransaction();
        FisheryRegion region = SimulationStorage.getFisheryRegion(tx);
        
        out.write(region.getName() + ";"
                + region.getMinLatitude() + ";"
                + region.getMaxLatitude() + ";" 
                + region.getMinLongitude() + ";" 
                + region.getMaxLongitude() + ";"
                + region.getCellLengthLatitude() + ";" 
                + region.getCellLengthLongitude() +
                "\n");
        
        tx.closeContext();
    }
}
