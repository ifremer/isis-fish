/*
 * #%L
 * IsisFish data
 * %%
 * Copyright (C) 2009 - 2014 Ifremer, Code Lutin, Benjamin Poussin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package simulators;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.ifremer.isisfish.simulator.SimulationContext;
import scripts.SiMatrix;
import scripts.SiMatrixEffortByCell;

import org.nuiton.topia.TopiaException;

/**
 * Simulateur qui fixe la valeur de effortByCell a true pour force
 * l'utilisation des calculs par cellule plutot que par zone.
 *
 * @author poussin
 */
public class SimulatorEffortByCell extends DefaultSimulator {
    
    /** to use log facility, just put in your code: log.info("..."); */
    static private Log log = LogFactory.getLog(SimulatorEffortByCell.class);

    @Override
    protected SiMatrix newSiMatrix(SimulationContext context) throws TopiaException {
        return new SiMatrixEffortByCell(context);
    }

}
