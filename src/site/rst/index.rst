.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
=========
Isis-fish
=========

Isis-fish est un simulateur de pêcherie complexe. Il est entièrement écrit en
Java. Dans sa version 2 Le langage de script utilisé était ECMAScript, depuis
la version 3 on utilise directement du Java, ce qui permet de faire des
vérification syntaxique et indiquer à l'utilisateur la ligne en erreur.

Pour utiliser Isis-fish il vous faut Java d'installé sur votre machine.

Les caractéristiques sont:

- Base de données embarquée
- Création d'autant de région de pêche, contenant autant de population que
  l'on souhaite
- Création de mesures de gestion en langage de script
- Possibilité de modifier la façon de simuler en langage de script
- Possibilité d'éxecuter une suite de simulations en modifiant une ou des
  données de simulation
- Possibilité d'envoyer les simulations ce faire sur une autre machine
- Affichage des résultats spacialement (carte) ou temporellemnt (graph)
- Possibilité de publier les résultats d'une simulation sur un
  serveur
- Possibilité de publier une région sur un serveur

Ce projet est un projet initié à la demande de l'Ifremer et financé par
celui-ci. L'analyse et la réalisation technique ont été confiées à différents
partenaires:

- Irin (Institut de Recherche en Informatique de Nantes)
- Cogitec (http://www.cogitec.fr)
- Code Lutin (http://www.codelutin.com)
