.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
Projets de recherche liés à ISIS-Fish
=====================================

=====================  ==============================================================================================================================  ============================================  ======================================================================================  ===========  =========
Nom                    Projet                                                                                                                          Rôle dans le projet                           Financement                                                                             Année début  Année fin
=====================  ==============================================================================================================================  ============================================  ======================================================================================  ===========  =========
UNCOVER                Evaluer l'impact de changements biologiques pour mettre en place des plans de restauration                                      partenaire                                    EU - VIth PCRD- FP6-2004-SSP-4 (Priority 8.1.B.1.3 Task 2)                              2006         2008
EFIMAS                 Operational evaluation tools for fisheries management options                                                                   partenaire                                    EU FP6 n° SSP8-CT-2003-502516                                                           2004         2007
PROTECT                Potential of marine protected areas for marine environmental PROTECTion                                                         coordination d'une tâche sur la modélisation  EU FP6 n° SSP8-CT-2004-513670                                                           2004         2007
Liteau-AMP             Développement d'outils diagnostics et exploratoires d'aide à la décision pour évaluer la performance d'Aires Marines Protégées  dirigeant                                     Lit'eau 2 (MEDD)                                                                        2004         2006
ISIS-Fish development  Développement d'un outil de simulation de pêcherie complexe avec l'Université de Nantes, COGITEC et Code Lutin                  responsable de projet                         IFREMER n° 99/210448, 99/210500, 01/210676, 02/210898, 03/210136, 04/210316, PNEC/ART4  1998         2006
TECTAC                 TEChnological developments and TACtical adaptations of important EU fleets                                                      partenaire                                    European Union n°Q5RS-2002-01291                                                        2002         2005
=====================  ==============================================================================================================================  ============================================  ======================================================================================  ===========  =========
