.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
=========================================
Les évènements autour du projet Isis-FISH
=========================================

2010
----

- *20-24 septembre 2010* : `Conference annuelle du CIEM - Nantes`_ , FRANCE
- *7 juillet 2010* : Conférence ISIS-Fish aux RMLL_ - Bordeaux, FRANCE
- *7-11 juin 2010* : `Ecole chercheur`_ (Analyse de sensibilité) - Giens, FRANCE

2009
----

- *1-3 décembre 2009* : Atelier collaboration IFREMER-MPO dans le cadre de l’Approche Écosystémique  - Rimouski, CANADA
- *21-26 septembre 2009* : `Conference annuelle du CIEM - Berlin`_ , ALLEMAGNE
- *14-18 septembre 2009* : Séminaire Utilisateurs ISIS-Fish - Nantes, FRANCE - Inscriptions_      
- *8-11 septembre 2009* : `Atelier franco-australien - modeles bio-economiques`_
- *29 août - 4 septembre 2009* : `Groupe de travail CIEM`_ (WKSHORT) : comparaison de modèles pour l'Anchois
- *11-14 mai 2009* : Ecole chercheur (Analyse de sensibilité)

2008
----

- *3-4 décembre 2008* : réunion Mexico_ à Montpellier, France
- *28 novembre 2008* : causerie au département EMH, le modèle ISIS-Fish anchois dans le Golfe de Gascogne
- *22-26 septembre 2008* : `Conference annuelle du CIEM - Halifax`_ , CANADA
- *2 juillet 2008* : Présentation Isis-FISH aux RMLL_
- *5 et 6 juin 2008* : réunion Mexico_ à Nantes, FRANCE


...................................Links...........................................

.. _RMLL::http://rmll.info
.. _Conference annuelle du CIEM - Nantes::http://www.ices.dk/iceswork/asc/2010/index.asp
.. _Ecole chercheur:: http://www.reseau-mexico.fr/node/165
.. _Conference annuelle du CIEM - Berlin::http://www.ices.dk/iceswork/asc/2009/index.asp
.. _Inscriptions::http://pollen.chorem.org/pollen/poll/VoteFor/3e830a78-a9a9-4043-9da6-6232d47f4a67
.. _Atelier franco-australien - modeles bio-economiques::http://www.umr-amure.fr/pg_workshop_fast.php
.. _Conference annuelle du CIEM - Halifax::http://www.ices.dk/iceswork/asc/2008/index.asp
.. _Groupe de travail CIEM::http://www.ices.dk/workinggroups/ViewWorkingGroup.aspx?ID=345
.. _Mexico::http://www.avignon.inra.fr/mexico/index.php/Accueil
