.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
====================
Architecture globale
====================

Isis-Fish est composé de trois modules:

- Les données et leurs persistences
- Les interfaces utilisateurs
- Le moteur de simulation


.. image:: ../../images/devel/isis-architecture-modules.png


Le domaine est d'abord décrit (modélisé) en UML. A partir de cette
modélisation un générateur de code est utilisé pour la création de toutes
les classes représentant le métier. Seul les méthodes métiers ne sont pas
généré, elles sont alors écrites en héritant des classes générées. Dans
Isis-FISH le code des méthodes représente 15% du code des classes métiers et
des classes de persistence.


.. image:: ../../images/devel/isis-architecture-legend.png
.. image:: ../../images/devel/isis-architecture-modelisation.png


La persistence s'appuie sur ToPIA qui permet d'abstraire l'application des
librairie de persistence choisie. Actuellement nous utilisons Hibernate
comme OMR et H2 comme base de données embarquée.

Des librairies ont été écrites pour la gestion des fichiers textes tel que
les scripts. Toutes les données peuvent être synchronisées avec un serveur
(Région, Simulation, Script) pour cela nous utilisons une librairie SVN
Java.


.. image:: ../../images/devel/isis-architecture-persistence.png


Les intefaces utilisateurs s'appuie sur la librairie Java Swing. Pour
simplifier l'écriture et la maintenance ainsi que pour avoir un découpage
propre entre le code des interfaces et le code métier nous utilisons la
librairie Jaxx qui nous permet de décrire les interfaces utilisateur en XML.
L'avantage est qu'il est possible d'utiliser des fichiers CSS pour modifier
l'aspect de l'interface. Au paravant nous utilisions la librairie SwiXAT,
l'avantage de Jaxx sur SwiXAT est qu'il est compilé et produit des fichiers
Java directement réutilisable dans du code traditionnel.

De nombreux composants graphiques sont utilisés dans les interfaces.

La plupart des interfaces utilisateurs ne sont pas spécifique Isis-FISH et
pourrait facilement réutilisé dans d'autre projet de simulateur. Seul les
interfaces de saisie des données du modèle métier sont spécifiques, ainsi
qu'un petit nombre d'onglet du lanceur de simulation.


.. image:: ../../images/devel/isis-architecture-interface.png


Le simulateur est lui aussi non spécifique Isis-FISH. Tout le code
spécifique Isis-FISH est en faite écrit en Script (Java) et modifiable via
les interfaces de saisie des scripts de l'application.

Les autres composants peuvent être directement réutilisé.


.. image:: ../../images/devel/isis-architecture-simulateur.png
.. image:: ../../images/devel/isis-architecture-simulation.png
