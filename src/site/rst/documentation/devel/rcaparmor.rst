.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2012 Ifremer, CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
Utilisation de R sur caparmor
=============================

Version caparmor
----------------

La version de R doit avoir été compilée avec l'option par "--enable-R-shlib"
sinon, le fichier "libR.so" est manquant::

  ll /appli/R/2.14.2-gnu-4.3/lib64/R/lib
  -rwxr-xr-x 1 root root  467061 Mar  5 14:13 libRblas.so
  -rwxr-xr-x 1 root root 3977966 Mar  5 14:13 libRlapack.so


Recompilation de R
------------------

Télécharger manuellement R et le recompiler avec l'option manquante::

  ./configure --prefix=/home3/caparmor/poussin/R-2.15 --enable-R-shlib

Verifier la présence du fichier libR.so::

  ll /home3/caparmor/poussin/R-2.15/lib64/R/lib
  -rwxrwxr-x 1 poussin emh 10797333 Apr  2 08:51 libR.so
  -rwxrwxr-x 1 poussin emh   467045 Apr  2 08:51 libRblas.so
  -rwxrwxr-x 1 poussin emh  3977950 Apr  2 08:51 libRlapack.so


Installation des librairies XSA
-------------------------------

Installation des librairies pour la version que l'on vient de compiler::

  module load R/2.15.0-gnu-4.3
  install.packages("FLCore", repos="http://flr-project.org/R")
  install.packages("FLXSA", repos="http://flr-project.org/R")


Configuration de R pour Isis
----------------------------

Ajouter dans les script csh::

  setenv R_HOME /appli/R/2.15.0-gnu-4.3/lib64/R
  setenv LD_LIBRARY_PATH ${R_HOME}/lib

Et lancer les simulations avec tous les composants en 64 bits
(java, R, isis).
