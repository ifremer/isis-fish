.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
Documentation utilisateur
-------------------------

- Introduction_
- `Interface utilisateur`_

  + `Saisie de pecheries`_
  + `Lanceur de simulations`_
  + `Lanceur d analyse de sensibilite`_
  + `Rendu des resultats`_
  + `Editeur de script`_
  + `Queue de simulation`_
- `Plan d experience-Plan d analyse`_
- `Analyses de sensibilite`_

.. _Introduction:: userManual/introduction.html
.. _Interface utilisateur:: userManual/GUI.html
.. _Saisie de pecheries:: userManual/inputs.html
.. _Lanceur de simulations:: userManual/launcher.html
.. _Lanceur d analyse de sensibilite:: userManual/sensitivity.html
.. _Rendu des resultats:: userManual/results.html
.. _Editeur de script:: userManual/scripts.html
.. _Queue de simulation:: userManual/queue.html
.. _Plan d experience-Plan d analyse:: userManual/analysisPlan.html
.. _Analyses de sensibilite:: ../downloads/manuel_sensitivity.pdf
