.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
Installation et lancement d'ISIS-Fish
=====================================

Matériel requis
---------------

Afin de pouvoir faire tourner ISIS-Fish sur votre machine, il est
nécessaire d'avoir au moins la vesion 1.6.0_18 de Java d'installée. Il est
possible de connaître la version actuellement installée sur votre
ordinateur en tapant dans une commande DOS : «java -version». Si vous
aboutissez à un message d'erreur, cela signifie que Java n'est pas
installé. Si la version installée est antérieure à la version 1.6.0_18 ou
que Java n'est pas installé, vous devez télécharger la dernière
version de Java SE Development Kit (JDK). Ceci peut se faire
directement à cette adresse : http://java.sun.com/javase/downloads/index.jsp

Comment se procurer ISIS-Fish
-----------------------------

Il est possible de télécharger ISIS-Fish à cette adresse :
http://isis-fish.labs.libre-entreprise.org/download/version3. les
fichiers contenant ISIS-Fish et tout ce qui est nécessaire à son bon
fonctionnement sont nommés : «isis-fish-x.y.z.zip» où x.y.z
correspond au numéro de la version.

Installation
------------

Une fois le zip téléchargé, il faut le dézipper dans le répertoire de
votre choix.

Sous linux vous pouvez taper::

  unzip isis-fish-x.y.z.zip

Un nouveau répertoire isis-fish-x.y.z apparaît

Lancement
---------

Fichier de lancement par défaut .bat et .sh
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Par défaut ISIS-Fish peut allouer au maximum 512Mo de Ram pour son
utilisation. Si votre région contient de très nombreux métiers,
stratégies et populations vous pouvez devoir augmenter cette valeur
pour cela éditez le fichier *go.bat* ou *go.sh* suivant votre
plateforme (windows, Unix/Linux).

Par défaut les fichiers de lancement redirigent les sorties vers un
fichier error.txt. Si jamais un problème se produit durant
l'utilisation d'ISIS-Fish ce fichier peut vous permettre de comprendre
plus simplement le problème. Si vous faites appel aux mailing-lists pour
demander de l'aide, il vous sera demander de joindre ce fichier à votre demande.

Lancer ISIS-Fish sous windows
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Double-cliquer sur le fichier .bat créé. Une fenêtre DOS va alors
s'ouvrir. Puis l'interface d'ISIS.

.. image:: mainWindow.png

Si l'interface n'apparaît pas, cela signifie qu'une erreur s'est
produite lors du lancement. Le message d'erreur est situé dans le
fichier error.txt, mais il peut dans certains cas être difficilement
compréhensible. Si vous ne pouvez comprendre ce message, envoyez le sur
la liste utilisateur (voir partie Contact-Assistance_ )

.. _Contact-Assistance: community

Lancer ISIS-Fish depuis un porte document windows
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Dans le go.bat ou go.sh du porte doc, il faut passer à la ligne apres start et
ajouter::

  c:
  cd c:\la position du fichier go.bat du porte doc (par exemple, cd c:\"Documents and settings"\smahevas\Bureau\PorteDoc\Isis_v3\isis-fish-3.1.1)

Lancer ISIS-Fish sous Unix/Linux
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Placez-vous dans le répertoire contenant le fichier go.sh et exécutez
le avec::

  ./go.sh

Option de la ligne de commande
------------------------------

Il est possible de passer des paramètres à la ligne de commande par exemple::

  ./go.sh --help

l'option --help permet d'afficher la liste des options disponibles

Voici les autres options.

.. FIXME Wait for simple table fix in JRST : http://nuiton.org/issues/show/651

.. ================================================  =======================================================================================================================================================================================================
.. Commande                                          Description
.. ================================================  =======================================================================================================================================================================================================
.. -help, -h                                        Affiche l'aide
.. -listRegion                                      Affiche la liste des régions disponibles
.. -importRegion region                             Importe une région dans la base locale. Il est possible d'importer des régions depuis ISIS-Fish version 2 ou 3. Suivant l'extension du fichier, ISIS-Fish fera la bonne opération
.. -importAndRenameRegion region newName            Importe une région dans la base local et modifie son nom durant l'import.
.. -exportRegion regionName output.zip              Exporte une région existante dans la base locale sous la forme d'un fichier zip importable ultérieurement.
.. -listSimulation                                  Affiche la liste des simulations disponibles
.. -importSimulation simulation                     Importe une simulation dans la base locale
.. -exportSimulation simulationName simulation.zip  Exporte une simulation existante dans la base locale sous la forme d'un fichier zip importable ultérieurement.
.. -update <true|false>                             Force la mise à jour des scripts comme lors du premier lancement d'ISIS-Fish
.. -create-ssh-key                                  Permet de créer une paire de clé ssh. Ces clés sont utilisées pour pouvoir modifier les scripts pour les personnes ayant un login sur le CVS hébergeant les sources des scripts. (Voir le chapitre CVS)
.. -ssh-key-file pathToPrivateKey                   Permet d'indiquer à ISIS-Fish d'utiliser une paire de clés ssh existantes pour l'accès au CVS. (Voir le chapitre CVS)
.. -config name value                               Permet de modifier des valeurs de configuration. Cela revient au même que de modifier le fichier $HOME/.isis-config-3, ou utiliser l'interface depuis le menu fenêtre->configuration.
.. ================================================  =======================================================================================================================================================================================================

Liste des options disponibles:

.. =======================  ==================================================================================================================================================  ======================================================================================================  ===================================================
.. option                   description                                                                                                                                         type                                                                                                    exemple
.. =======================  ==================================================================================================================================================  ======================================================================================================  ===================================================
.. compileDirectory         répertoire utilisé pour la compilation des scripts                                                                                                  chemin vers un répertoire                                                                               /tmp/isis-build
.. language                 La langue à utiliser                                                                                                                                ISO Language Code http://www.loc.gov/standards/iso639-2/englangn.html                                   fr
.. country                  La variable pays pour la langue                                                                                                                     ISO Country Code http://www.iso.ch/iso/en/prods-services/iso3166ma/02iso-3166-code-lists/list-en1.html  FR
.. database                 répertoire utilisé pour stocker les informations                                                                                                    chemin vers un répertoire                                                                               /home/poussin/isis-database-3
.. defaultSimulator         nom du fichier à utiliser comme simulateur                                                                                                          nom d'un fichier .java contenu dans le répertoire $database/simulators                                  DefaultSimulator.java
.. javadocUrl               URL pointant vers la javadoc du simulateur                                                                                                          URL                                                                                                     http://isis-fish.labs.libre-entreprise.org/apidocs/
.. defaultExportDirectory   Répertoire ou les exports de simulation sont placé par défaut                                                                                       chemin vers un répertoire                                                                               /home/poussin/isis-export
.. defaultExportNames       la liste des noms des exports par défaut à utiliser                                                                                                 liste séparée par des virgules                                                                          Abundances.java,Biomasses.java
.. defaultResultNames       la liste des résultats que l'on souhaite conserver durant la simulation pour les visualiser ultérieurement via l'interface de rendu des résultats.  liste séparé par des virgules                                                                           matrixDiscardsWeightPerStrMet,matrixPrice
.. defaultMap               la carte à utiliser par défaut pour nouvelle région                                                                                                 le chemin d'un fichier de carte .shp sans l'extension                                                   maps/vmap_area_thin
.. defaultTagValue          la liste des tags values à utiliser par défaut pour les simulations                                                                                 liste séparé par des virgules de couple "nom":"valeur"                                                  "ecoResult":"true","maVal":"truc"
.. cvsHostName              le nom du serveur contenant l'arbre CVS des données                                                                                                 le nom d'un serveur ou une ip                                                                           labs.libre-entreprise.org
.. cvsRepository            le répertoire racine du CVS sur le serveur                                                                                                          chemin vers un répertoire                                                                               /cvsroot/isis-fish
.. cvsDataBase              le nom du module CVS contenant les données sur le serveur                                                                                           String                                                                                                  isis-fish-data
.. cvsHost                  fichier contenant les cles des serveurs CVS                                                                                                         chemin d'un fichier xml                                                                                 /home/poussin/.isis-ssh-host.xml
.. cvsUserName              le nom de l'utilisateur pouvant accéder au CVS                                                                                                      login                                                                                                   anonymous ou bpoussin
.. cvsSsh2Connexion         indique si l'on utilise ou non un accès sécurisé au CVS. Cela n'est pas utile pour le compte anonymous et nécessite une clé SSH                     boolean                                                                                                 true
.. cvsKeyFile               le fichier contenant la clé privée SSH                                                                                                              chemin vers un fichier                                                                                  /home/poussin/.ssh/id_dsa
.. smtpServer               le nom du serveur SMTP à utiliser pour envoyer des mails.                                                                                           nom d'un serveur ou ip                                                                                  smtp.codelutin.com
.. localSimulator           valeur par défaut utilisée pour savoir si les simulations que l'on exécute doivent être faites localement ou sur un serveur de simulation           boolean                                                                                                 true
.. simulatorServer          URL permettant de contacter le serveur de simulations                                                                                               URL                                                                                                     http://simulateur.ifremer.fr:9090
.. simulatorServerLogin     le login à utiliser pour le serveur de simulations                                                                                                  login                                                                                                   bpoussin
.. simulatorServerPassword  le mot de passe à utiliser pour le serveur de simulations                                                                                           mot de passe                                                                                            guest
.. =======================  ==================================================================================================================================================  ======================================================================================================  ===================================================

Premier lancement et fichier de configuration
---------------------------------------------

Lors du premier lancement ISIS-Fish vous posera un certain nombre de
question.

Si vous souhaitez les modifier ultérieurement il faut éditer le
fichier $HOME/.isis-config-3.

$HOME est une variable d'environnement sous Unix/Linux qui pointe vers
le répertoire utilisateur de la personne logguée. Sous Windows
l'utilisateur à aussi un espace de travail personnel, mais son
emplacement varie suivant la version de Windows utilisée.

Il est possible qu'ISIS-Fish ne se lance pas avec l'erreur "la configuration
n'est pas correcte". Dans ce cas, il faut éditer le fichier .isis-config-3 et y
ajouter::

  vcs.typeRepo=HEAD
  vcs.remoteDatabase=3.1.3 (ou le numero de version utilisé)

Données et scripts initiaux
---------------------------

Lors du premier lancement il est conseillé d'être connecté à Internet,
car ISIS-Fish récupère une région de démonstration et les scripts de
simulation, export, ...

Si vous ne pouvez pas être connecté, depuis la version 3.0.16 il vous
est possible de récupérer à l'adresse:
http://isis-fish.labs.libre-entreprise.org/download/version3 le fichier se
nommant isis-database.x.y.z.zip où x.y.z correspond au numéro de la version.

Décompressez ce fichier et placez le répertoire obtenu dans votre répertoire
$HOME. Tous les scripts normalement récupéré par Isis-fish se trouvent dans
ce répertoire. Par contre, la région de démonstration n'est pas valide et vous
ne pouvez pas l'utiliser.

Mettre à jour les scripts
-------------------------

Les scripts de simulation sont modifiés indépendament de la version
d'ISIS-Fish. Il est donc possible de mettre à jour les scripts sans
modifier sa version d'ISIS-Fish. Dans certain cas, les nouveaux
scripts ne fonctionneront qu'avec une nouvelle version d'ISIS-Fish,
par exemple en cas de modification de la structure des régions (ajout
de champs à un objet).

Pour mettre à jour les scripts, il faut dans la fenêtre principale
aller dans le menu **fichier->Synchronisation serveur**.

Vous devez voir apparaitre une fenêtre vous invitant à sélectionner
les éléments à mettre à jour ou les nouveaux éléments à récupérer.

.. image:: synchroWindow.png

Dans la zone texte du bas vous pouvez voir les changements effectués
sur le fichier sélectionné.

Changer le répertoire de simulations  : .isis-config-3
------------------------------------------------------

Pour windows : .isis-config-3 se trouve dans le répertoire home (C:\Documents
and  settings\chezsoi).
[ou sur Mac : Users/<account>/ .isis-config-3
(Comme le nom de fichier commence par un "." il peut être caché)]
Par défault, la zone de travail est C:\Documents and Settings\chezsoi (zone où
sont créés les répertoires isis-database-3, isis-build,...). Pour changer de
zone de travail, il suffit de changer le répertoire par défaut dans le
isis-config par le répertoire choisi.

Fichier exemple ::

  #Last saved Tue Sep 15 09:25:33 CEST 2009
  #Tue Sep 15 09:25:33 CEST 2009

  compilation.directory=D\:\\SimulationsISIS\\isis-build
  database.directory=D\:\\SimulationsISIS\\isis-database-3
  default.export.directory=D\:\\SimulationsISIS\\isis-export


Installer Isis-FISH pour effectuer les simulations sur le super-calculateur CAPARMOR
------------------------------------------------------------------------------------

Afin d'utiliser Caparmor pour effectuer les simulations, il convient de suivre
une procédure particulière (et d'avoir l'autorisation d'utiliser Caparmor). La
procédure décrite dans le fichier joint devrait vous permettre de configurer et
utiliser ISIS et Caparmor correctement.

`Installation d ISIS-Caparmor`_

.. _Installation d ISIS-Caparmor::installation-isis-caparmor.pdf
