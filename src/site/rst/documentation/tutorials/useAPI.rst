.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
Tuto d'utilisation des API
==========================

Avant tout il faut savoir, que toutes les valeurs saisies dans l'interface de
saisie par l'utilisateur sont stoquées dans les objets d'ISIS correspondants.
Pour modifier ces valeurs avant ou en cours de simulation, il faut connaitre
leur nature et leur structure, savoir comment y accéder et les modifier.
Pour savoir comment y accéder il faut connaitre les dépendances entre objets
(objets qui sont des attributs d'autres objets). Pour cela on consulte
l'architecture de la base de donnée ( `Modele ISIS-Fish`_ ) qui se reflète dans
l'architecture de l'interface de saisie.

Pour savoir comment passer d'un objet a un autre il faut connaitre les méthodes
appliquables aux objets : tout est dans l'API.

Remarque : Il vaut mieux commencer par se familiariser avec les differents types
d'objets JAVA (double, boolean, string, integer, equation, matrix,...), les
attributs et les methodes (cf tutoriaux JAVA).

Voici un exemple "pas à pas" pour le cas ou l'on voudrait modifier l'equation de
mortalité naturelle dans un plan d'analyse par exemple. On veut remplacer
provisoirement la valeur de la base par une autre. On crée un objet String
rempli avec l'autre valeur par exemple::

  String mortalitenaturelle = "if (groupe.getId() == 0) return 1.5; else return 0.2;"

Se poser la question : "Dans la base de donnée où se trouve la mortalité naturelle ?"
-------------------------------------------------------------------------------------

Réponse : dans l'interface de saisie, elle est dans la branche "population" et
l'onglet "equations".
(au passage le titre de l'onglet nous renseigne sur le type d'objet auquel on a
affaire : ya de grandes chances pour que parmi la batterie d'objets possibles
(int, double, matrice...) la mortalité naturelle soit une equation ;-) )

En francais on dirait "dans population, va chercher l'objet equation de
mortalité naturelle, et attribut lui la valeur du string mortalitenaturelle", ya
pu qu'a le dire en java.

Ouvrir la page des APIs
-----------------------

Dans la colonne de gauche de l'API (sous "all classes") on cherche l'entite
"population" et on clique dessus.

Les méthodes disponibles
------------------------

Toutes méthodes que l'on peut appliquer à un objet population (colonne de
droite) s'affichent , et le type d'objet qu'elles renvoient (colonne de gauche).

c'est a dire que les lignes de commandes qu'on ecrira seront de la forme::

  objet_renvoyé_indiqué_à_gauche nomObjet = population.methode_ecrite_à_droite(argument de la methode);

Il y a surtout deux grands types de méthodes :

* les méthodes "get" : qui vont chercher un objet
* les méthodes "set" : qui assignent une valeur a un objet.

Dans notre exemple on veut changer l'equation de mortalité naturelle, c est donc
un set !

On cherche une méthode qui parle de mortalité naturelle...
----------------------------------------------------------

On trouve getNaturalDeathRate() qui renvoit un objet equation (c'est l'objet
equation qui contient la valeur de l'equation de mortalite naturelle remplie par
l'utilisateur) et setNaturalDeathRate(Equation naturalDeathRate) pour laquelle
on doit passer un objet equation (une equation de naturalDeathRate) en argument.

(en cliquant sur le nom de la méthode on a une description sommaire de ce
qu'elle fait)

setNaturalDeathRate() parait être ce qu'on veut faire...
mais la méthode setNaturalDeathRate prend en argument une equation, on ne peut
donc pas faire::

  pop.setNaturalDeathRate(mortalitenaturelle);

puisque mortalitenaturelle n'est pas une equation mais un string.


Créer une équation
------------------

Du coup il faut trouver autre chose qui fasse le lien entre un string et une
equation. En cliquant sur "equation" dans la page d'API on tombe sur la page des
méthodes qui s'appliquent aux objets qui sont des equations. On trouve une
méthode .setContent(String ) qui prend en argument un string. Ca veut dire que
si on a une equation, on peut lui changer sa valeur en utilisant cette methode
avec un string en argument.
L'equation dont on veut changer la valeur on sait la récupérer::

  Equation eqMortalite = pop.getNaturalDeathRate();

L'objet eqMortalite est un objet equation::

  eqMortalite.setContent(mortalitenaturelle);

et là on a le droit vu que mortalitenaturelle est un string et que la méthode
prend un string en argument.

et voilà c'est fait !

remarque : on pourrait le faire en 1 ligne::

  pop.getNaturalDeathRate().setContent(mortalitenaturelle);

.. _Modele ISIS-Fish:: http://isis-fish.labs.libre-entreprise.org/wiki-moin/instance/cgi-bin/moin.cgi/isisfishmodel
