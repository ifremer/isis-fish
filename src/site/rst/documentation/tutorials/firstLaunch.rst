.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
Tutoriel : Utilisation d'ISIS-Fish (V3.2.0) pas à pas
=====================================================

Ce tutoriel couvre tous les points d'ISIS, de la création d'une base à l'analyse
de sensibilité. Il n'est en aucun cas une documentation détaillée de
l'utilisation d'ISIS-Fish, mais une aide à travers les différentes étapes
d'utilisation du logiciel. Tous les points "classiques" sont censés être
abordés, pour toutes les configurations complexes ou "exotiques", veuillez vous
référer au manuel utilisateur ou aux listes de diffusion.

Le tutoriel est basé sur des saisies d'écran commentées à la manière des bandes
dessinées. La lecture s'effectue de la même manière que dans ces dernières :
Ordre des bulles : de gauche à droite et de haut en bas. Afin de simplifier les
écrans et garantir l'ordre de lecture (et des opérations), les bulles ont été
numérotées.

`Diaporama pour visualisation hors-ligne`_

Ce tutoriel est présenté sous la forme d'un diaporama. Pour lancer le diaporama,
cliquez sur "démarrer" en haut de la page. Pour atteindre un point particulier
de la présentation, cliquez sur le lien correspondant ci-dessous :

.. _Diaporama pour visualisation hors-ligne:: ../../downloads/demo.odp


.. TODO JC20100604 Make the links to each page

