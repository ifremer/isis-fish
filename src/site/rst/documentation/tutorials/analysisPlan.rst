.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
Tutoriaux pour l'utilisation des plans d'analyse
================================================

Un plan d’analyse est un ensemble d’expériences (simulations) pour lesquels on
va modifier la valeur de certains paramètres selon un protocole défini pour
répondre à une question. On distingue des plans d’analyse pour lesquels

* *les expériences sont indépendantes* . C'est à dire que l’ordre des
  simulations n’a pas d’importance et que l’on pourrait par exemple les lancer
  sur des ordinateurs différents. Les valeurs que les paramètres devront prendre
  à chaque nouvelle simulation sont déterminées au préalable.

* *les expériences sont séquentielles* . Les valeurs des paramètres de la
  simulation s+1 dépendent des résultats de la simulation s.

Expériences Indépendantes : exemple d'une analyse de sensibilité
----------------------------------------------------------------

Dans le cas d'expériences indépendantes, la première étape est de définir les
paramètres à modifier et les différentes valeurs qu’ils devront prendre
(appelées « modalités »). On les organise ensuite selon un plan d’expérience
adéquat (plan complet, factoriel, optimisé…) qu’on écrit sous forme de matrice.
Des méthodes sont déjà disponibles pour réaliser ce type de plans sous
ISIS-Fish. Ils sont décrits ici mais libre à l’utilisateur d’écrire ses propres
scripts si la méthode actuellement utilisée ne répond pas à ses besoins. Voici
la description et le fonctionnement des scripts existants : ils nécessitent
l'écriture de 3 types de fichiers : un script de plan d'analyse, une matrice
d'expérience et des fichiers paramètre :

* *La matrice d’expérience* est une matrice [expériences x paramètres] : chaque
  colonne correspond à l’un des paramètres à modifier et chaque ligne contient
  les modalités des paramètres pour une expérience donnée. Les modalités sont
  codées par des entiers (-1 ;0 ;1 etc). Cette matrice est copiée dans un
  fichier .txt sans en-tête de colonnes ni de lignes.

* *Les fichiers paramètres* : un fichier .txt par paramètre donnant la
  correspondance entre la valeur de la modalité (-1 ;0 ;1) dans la matrice et la
  valeur du paramètre. Ce paramètre peut être un réel, une liste de réels, une
  équation…  Si l’on nomme les modalités –1 et 1, le fichier est de la forme

  - Pour un paramètre qui est une liste

::

    -1=0.5;0.8;0.9;0.11
    1=0.5;0.11;0.15;0.41

  - Pour un paramètre qui est une équation

::

    -1=if(condition = true) return 5 ; else return 3 ;
    1=if(condition = true) return 7 ; else return 3 ;

  - Pour un paramètre qui est un réel

::

    -1=0.8
    1=1.2

  - On peut aussi modifier les paramètres des règles de gestion...(voir
    exemple). Il faut alors indiquer dans le .txt tous les paramètres public de
    la règle. Attention pour certaines règles de gestion il faut définir la
    population, l’engin, la zone... ciblée par la règle dans les paramètres.
    Pour ce faire il n’est pas possible d’écrire le nom de la population, de la
    zone... il faut déterminer le code correspondant qui est du genre

::

      fr.ifremer.isisfish.entities.Zone#1169028645767#0.37798185123822536

    Pour cela on peut faire tourner une simulation avec la règle en question
    correctement paramétrée et aller voir dans les logs (fichier debug.txt dans
    le même dossier que le .bat de lancement), ces codes sont inscrits à coté du
    nom du paramètre au moment de l’initialisation des règle de gestion.

*Attention !!!!* dans ces fichiers la syntaxe est importante:

* Pas d’espace
* Pas de « ; » à la fin des lignes
* Pas de « + » devant un chiffre quand il désigne une modalité (+1=NON ; 1=OUI)

Les fichier matrix.txt et les fichiers paramètres.txt doivent être placés *dans
un même dossier* de préférence localisé dans le dossier contenant le .bat de
lancement d’ISIS-Fish (qui est la racine pour le logiciel).

Exemple
~~~~~~~

On propose ici la matrice et les fichiers paramètres d'une analyse
de sensibilité sur les paramètres de croissance, capturabilité, selectivité et
la période de fermeture d'un cantonnement.

`Fichiers parametres et matrice`_

.. _Fichiers parametres et matrice:: ../../downloads/Exemple_directory.zip

*Le script de plan d'analyse* (écrit à partir de l’éditeur de scripts)
permet de récupérer et modifier les valeurs des paramètres en fonction de la
matrice d'expérience pour chaque simulation.  (Voir le manuel pour l’explication
de la structure des scripts de plans d’analyse.)

On joint ici le script commenté correspondant à l'analyse de sensibilité
précédante. Le script contient les méthodes nécessaires à lire les fichiers
matrice.txt et les fichiers contenant les modalités des paramètres. Il contient
aussi le code servant à modifier les valeurs de ces paramètres dans la base de
donnée (pour connaitre les méthodes permettant d'accéder aux différents
paramètres de la base de donnée, se référer aux APIs).

`Script d analyse de sensibilite`_

.. _Script d analyse de sensibilite:: ../../downloads/Exemple_PlanAnalyse.java

Plans sequentiels : exemple de calibration par la méthode du simplexe
---------------------------------------------------------------------

Dans ce cas, les modalités prises par les paramètres à la simulation suivante
dépendent des résultats des simulations précédentes. Il faut donc écrire
l’algorithme de calcul de ces nouvelles valeurs en fonction des résultats de
simulation dans le script du plan d’analyse. Il faut également écrire le code
permettant de modifier la valeur des paramètres pour la remplacer par la
nouvelle valeur calculée.

Exemple
~~~~~~~

L'exemple reprend un script qui permet de calibrer deux paramètres (ici la
capturabilité) par la méthode du simplexe à pas variable à partir des
débarquements trimestriels.

`Script de calibration`_

.. _Script de calibration:: ../../downloads/calibration.java

Lancement d'un Plan d'analyse
-----------------------------

Quand les scripts sont écrits, c’est tout simple !

Dans l’interface de lancement de simulation on prépare sa simulation comme
d’habitude à l’exception des règles de gestion : il ne faut pas charger une
règle si elle doit être modifiée par le plan car elle sera ajoutée par le plan
d’analyse à la suite des règles entrées dans l’interface. On coche la case
« Utiliser le plan d’analyse ». On sélectionne le plan Exemple_PlanAnalyse ou
CalibrationExpeceq1q2 dans la liste déroulante. On remplit les paramètres du
plan et on lance la simulation.

PS : Attention à bien vérifier les différentes étapes listées dans le Pense-Bête
sinon gare aux 500 simulations sans effectifs initiaux... ;-)

Biblio intéressante
-------------------

* Drouineau, H., Mahévas, S., Pelletier, D. and Beliaeff, B. 2006. Assessing the
  impact of different management options using ISIS-Fish: the French
  Hake-Nephrops mixed fishery of the Bay of Biscay. Aquatic living resource,
  19 : 15-29.
* Saltelli, A., Tarantola, S., Campolongo, F. and Ratto, M. 2004. Sensitivity
  Analysis in Practise. A guide to Assessing Scientific Models. J.W.&. Sons. pp.
* Kleijnen, J.P.C. 1998. Experimental Design for Sensitivity Analysis,
  Optimization, and Validation of Simulations Models. In Handbook of simulation.
  Principles, Methodology, Advances, Applications and Practise, pp. 173-224. Ed.
  by Banks, J. Wiley, New York. Engeneering and Management Press. 864 pp.
* Walters, F.H., Parker, L.R., Morgan, S.L. and Deming, S.N. 1991. Sequential
  Simplex optimization: a technique for improving quality and productivity in
  research, development, and manufacturing (Chemometrics series). B.R. CRC Press
  LLC. 402 pp.
