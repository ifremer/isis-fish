.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
Quelques commande utiles sur CAPARMOR (ou un autre supercalculateur)
====================================================================

Sur CAPARMOR quand vous êtes logués sous votre nom,
tous les fichiers de simulation sont dans le répertoire isis-tmp

Lire les logs
-------------

En cas de plantage pour consulter les logs, lire le fichier simulation-sim...-output.txt::

  cat simulation-sim...-output.txt

Relancer les simulations d'un plan
----------------------------------

Si besoin de relancer les simus d'un plan (simu 5 à 10 par ex)::

  qsub -J 5-10 simulation-sim_...-script.seq

Récupérer des résultats récalcitrants
-------------------------------------

Si la simulation s'est terminée mais les résultats n'ont pas été rapatriés, il
faut récupérer le fichier simulation-sim_...-result.zip en le copiant sur le
serveur d'échange par exemple ::

  cp *-result.zip /home/navidad/partages/echange/sigrid

Commandes Unix
--------------
Une page web bien utile pour les commandes Unix en general :
http://www.infres.enst.fr/~danzart/unix_abrege.html#exemples
