.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
Paramétrisation d'une pêcherie (Isis-3.1.3)
===========================================

Le document proposé contient un exemple de paramétrisation d'une pêcherie
imaginaire, afin d'aider à la prise en main du logiciel et la formulation des
hypothèses de modelisation sous une forme interprétable par le logiciel. Il
contient également des exemples de simulations.

`Telecharger l exemple en PDF`_

`Telecharger la base zippee`_

.. _Telecharger l exemple en PDF:: ../../downloads/ExempleParametrisation.pdf

.. _Telecharger la base zippee:: ../../downloads/Testparam.zip
