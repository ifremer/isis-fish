.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
=======================
L'interface utilisateur
=======================

Fenêtre principale
==================

L'interface utilisateur est constituée d'une fenêtre composée d'onglets. Chaque
onglet permet d'utiliser le simulateur différement.

.. image:: mainWindow.png

Cette fenêtre contient six onglets :

- de lancement de simulation
- de rendu des résultats
- de saisie de région
- de saisie de script, règles de gestion, export, plan d'analyse, ...
- de lancement d'analyse de sensibilité
- de queue de simulation

Il est aussi possible d'ouvrir ces onglets dans une nouvelle fenêtre depuis le
menu *fenêtre*.

Le menu fichier
---------------

Le premier sous menu *synchronisation serveur* permet de synchroniser
les fichiers de scripts avec le serveur.

Le deuxième sous menu *quitter* permet de fermer ISIS-Fish

Le menu fenêtre
---------------

Les six sous menus sont l'équivalent des quatres onglets. Ils ouvrent l'onglet
correspondant dans une nouvelle fenêtre.

Le menu configuration
---------------------

Les sous-menu permettent de configurer différentes partie d'ISIS-Fish :
* Configuration générale
* Configuration VCS
* Configuration de l'utilisation de Caparmor

Pour certaines options il faut relancer ISIS-Fish pour que les
nouvelles valeurs soient prises en compte, comme par exemple la
langue.

Le menu aide
------------

Les sous menus permettent d'accéder à différentes aides sur ISIS-Fish

* Ce site
* La documentation des différentes API
* La fenêtre des logs de l'application (utile en cas d'erreur ou de comportement
  anormal)
* La fenêtre d'informations sur le simulateur.

La barre de statut
------------------

La barre de statut, située en bas de la fenêtre indique à droite la mémoire
utilisé puis la mémoire actuellement alloué pour ISIS-Fish.

La zone centrale permet à ISIS-Fish de d'afficher des messages et la
partie gauche indique l'avancement de tâche en train de s'exécuter.

Lancement de simulation
=======================

L'onglet de lancement de simulation permet de paramètrer les
simulations et de les exécuter.

.. image:: simulationTab.png

Cette onglet est consitué de plusieurs onglets qui permettent de modifier:

- **Paramètres** de base de la simulation: identifiant, description, région,
  stratégies, populations, règles, nombre d'années
- **Script de présimulation**, pour l'activer il faut coché la
  case *utiliser un script de pré-simulation* dans l'onglet Paramètres. Le
  script de pré-simulation est du BSH exécuté avant toutes les simulations et
  permet de modifier la base de données.
- **Plan de simulation**, pour l'activer il faut cocher la case
  *Utiliser le plan d'analyse* dans l'onglet Paramètres.
- **Export des résultats** qui permet de sélectionner les scripts d'export à
  utiliser et le répertoire dans lequel on souhaite exporter.
- **Choix des résultats** qui permet de sélectionner les résultats à conserver
  pour une visualisation ultérieure via l'interface de rendu des résultats
- **Paramètres avancés** qui permet de changer le simulateur à utiliser, activer
  ou désactiver le cache, activer ou désactiver les statistiques, ajouter des
  paramètres libres récupérable dans les règles de gestion, le simulateur, ...

Si une règle de gestion ou un export a besoin d'un résultat qui n'est
pas sélectionné alors ce résultat est automatiquement ajouté a la
liste des résultats souhaités.

Lors du lancement de la simulation, automatiquement la date courante
sera ajouté au nom de la simulation.

Rendu des résultats
===================

L'onglet de rendu des résultats permet de visualiser les résultats
en graphe, en carte, ou en nombre. Il permet aussi de supprimer
d'anciennes simulations.

.. image:: resultTab.png

Pour ouvrir une ancienne simulation, sélectionnez la simulation
souhaitée dans la combobox et cliquez sur *ouvrir une nouvelle fenêtre*.

.. image:: resultWindowGraph.png

.. image:: resultWindowMap.png

Saisie de région
================

La fenêtre saisie de régions permet de visualiser et modifier les
régions.

Vous pouvez sélectionner dans la combobox votre région pour la faire
apparaître.

.. image:: inputTab.png

.. image:: inputTabRegion.png

Saisie de scripts
=================

L'onglet de script permet de modifier tous les scripts:

- scripts
- simulateurs
- exports
- règles
- plans d'analyses
- modèle d'équations
- analyses de sensibilité

.. image:: scriptTab.png

Tous ces scripts sont des fichiers textes sur le système de
fichier. Il est donc possible d'utiliser n'importe quel éditeur pour
les modifier. Voir
http://isis-fish.labs.libre-entreprise.org/othertools.html
pour plus d'explications.

Lancement d'analyse de sensibilité
==================================
L'onglet de lancement d'analyse de sensibilité permet de paramètrer les
analyses de sensibilité, de les exécuter et de les analyser.

.. image:: sensitivityTab.png

Cette onglet est consitué de plusieurs onglets qui permettent de modifier:

- **Paramètres** de base de l'analyse de sensibilité: identifiant, description,
  région, stratégies, populations, règles, nombre d'années
- **Analyse de sensibilité** permet de déterminer les facteurs étudiés.
- **Méthode de la sensibilité** permet de choisir le type d'analyse utilisé et
  de la paramétrée.
- **Export des résultats** qui permet de sélectionner les scripts d'export à
  utiliser et le répertoire dans lequel on souhaite exporter.
- **Choix des résultats** qui permet de sélectionner les résultats à conserver
  pour une visualisation ultérieure via l'interface de rendu des résultats
- **Paramètres avancés** qui permet de changer le simulateur à utiliser, activer
  ou désactiver le cache, activer ou désactiver les statistiques, ajouter des
  paramètres libres récupérable dans les règles de gestion, le simulateur, ...
- **Analyse des résultats** qui permet de voir les résultats (simples) d'une
  analyse de sensibilité terminée.

Si une règle de gestion ou un export a besoin d'un résultat qui n'est
pas sélectionné alors ce résultat est automatiquement ajouté a la
liste des résultats souhaités.

Lors du lancement de l'analyse de sensibilité, automatiquement la date courante
sera ajouté au nom de l'analyse. Le nom de chaque simulation est terminée par _
suivi du numéro de la simulation dans l'analyse de sensibilité.
