.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
======================================
Lancement d'une analyse de sensibilité
======================================

.. |tip| image:: tip.png

L'onglet de lancement d'analyse de sensibilité permet de créer, lancer et
exploiter des analyse de sensibilité.

.. contents::

Description des différentes zones de l'interface
------------------------------------------------

.. image:: sensitivity_00.png

#. les menus du lanceur d'analyse de sensibilité.
#. les onglets du lanceur de sensibilité : 6 pour la paramétrisation de
   l'analyse de sensibilité, le dernier pour l'affichage des résultats.
#. le contenu de chaque onglet.
#. la zone classique de statut général.

Les différentes actions des menus
---------------------------------

On détaille dans cette section, l'ensemble des actions réalisables par menu.
Il s'agit d'actions globales sur les analyses de sensibilité.

Menu **Simulation**
===================

.. image:: sensitivity_01_menuSimulation.png


#. **Sauver les paramètres de la simulation** :
   Sauver les paramètres de la simulation afin qu'ils soient utilisés par défaut
   par les prochaines analyses de sensibilité.
#. **Restaurer les paramètres d'une simulation** :
   Réutiliser les paramètres d'une simulation déjà lancée.

Créer une nouvelle analyse de sensibilité
-----------------------------------------

Le lancement d'une analyse de sensibilité s'effectue en plusieurs étapes :

#. Onglet Paramètres - Paramètres des simulations
#. Onglet Analyse de sensibilité - Détermination des facteurs
#. Onglet Méthode de la sensibilité - Détermination de la méthode d'exploration
   des domaines des facteurs et des variables à étudier.
#. Onglet Export des résultats - Choix des exports annexes
#. Onglet Choix de résultats - Choix des résultats à enregistrer pour
   visualisation.
#. Onglet Paramètres avancés - Choix des paramètres avancés des simulations de
   l'analyse de sensibilité.
#. Onglet Analyse des résultats - Visualisation des résultats d'analyses de
   sensibilité

Onglet **Paramètres** - Paramètres des simulations
==================================================

Une fois la région créée ou chargée voila à quoi ressemble l'interface.

.. image:: sensitivity_02_parameters.png

il s'agit de l'onglet principal des paramètres de base du simulateur.

#. **Charger une ancienne simulation**
   La liste déroulante contient l'ensemble des simulations existantes
   localement.
   La liste est vide lors du premier lancement d'ISIS-Fish, puisqu'aucune
   simulation n'existe localement.
   Une fois une simulation exécutée (avec succès), elle sera ajoutée dans cette
   liste au prochain lancement d'ISIS-Fish.
#. **Filtrer** les simulations existantes (nouveau depuis 3.1.0)
   Il est possible de filtrer les simulations existantes localement en cliquant
   sur le bouton filtrer.
   Un dialogue de filtre de simulation apparaît (voir section filtreSimulation)
#. **Réinitialiser** la liste des simulations (nouveau depuis 3.1.0)
   Il est possible en cliquant sur ce bouton de repositionner la liste de toutes
   les simulations existantes localement (en supprimant tout filtre précédemment
   saisi).
#. **Nom de la simulation**
   Pour saisir le nom d'une nouvelle analyse de sensibilité.
   Si vous chargez une ancienne simulation, le nom de celle-ci sera renseignée
   ici.

   |tip| On peut très facilement créer une nouvelle simulation à partir d'une
   ancienne en la chargeant et en changeant son nom.
#. **Description**
   Pour saisir la description d'une nouvelle analyse de sensibilité.
   Si vous chargez une ancienne simulation, la description de celle-ci sera
   renseignée ici.
#. **Région**
   Pour choisir la région associée à la simulation.
   Le chargement de la région renseignera les zones de stratégies et de
   populations.

   |tip| commencer toujours par charger la région avant de renseigner les règles
   car celles-ci sont liées aux données de la région chargée.
#. **Stratégies**
   Une fois une région chargée, on retrouve ici la liste des stratégies connues
   de la pêcherie.
   La stratégie utilisé par le simulateur sera celle sélectionnée dans cette
   liste.

   |tip| Par défaut, la première stratégie est sélectionnée, utilisez les
   touches Maj et Ctrl lors de vos sélections pour sélectionner toutes,aucune ou
   quelques stratégies.
#. **Populations**
   Une fois une région chargée, on retrouve ici la liste des populations connues
   de la pêcherie.

   La sélection d'une population, remplit les données à saisir de cette
   population dans la zone (9).

   La population utilisé par le simulateur sera celle sélectionnée dans cette
   liste.
#. **Données d'une population**
   Une fois une population sélectionnée, apparaît ici la matrice des données à
   saisir sur la population.
#. **Liste des règles disponibles localement**
   Il s'agit de la liste des règles connues par ISIS-Fish (voir section script).

   |tip| en survolant les règles dans la liste, une info bulle apparaît en
   affichant la description de la règle (voir section script).
#. **Liste des règles ajoutées**
   Contient la liste des règles que vous avez ajoutées.

   La sélection d'une des règles ajoutées, remplit la zone (15) des paramètres
   de configuration de la règle.

   Toutes les règles ajoutées dans cette liste seront utilisées par le
   simulateur.

   |tip| en survolant les règles dans la liste, une info bulle apparaît en
   affichant la description de la règle (voir section script).
#. **Ajouter une règle**
   Permet d'ajouter la règle sélectionné dans la liste déroulante (10).
   Une fois ajoutée, la règle apparaît dans la zone (11).
#. **Supprimer une règle ajoutée**
   Permet de supprimer la règle actuellement sélectionné dans la liste des
   règles ajoutées (11).
   Cette action n'est possible que si une règle ajoutée est sélectionnée.
#. **Supprimer toutes les règles ajoutées**
   Permet de supprimer tous les règles que vous avez ajoutées.
   Cette action n'est possible que si vous avez au moins ajouté un règle.
#. **Paramètres d'un règle ajoutée**
   Une fois une règle ajoutée sélectionnée dans la liste (11), apparaissent ici
   ses paramètres.

   |tip| en survolant les nom des paramètres, une info bulle apparaît en
   affichant la documentation du paramètre de la règle (voir section script
   @Doc).
#. **Nombre d'années**
   Pour indiquer le nombre d'années à utiliser par le simulateur.
   Si vous avez chargé une ancienne simulation, son nombre d'années sera
   renseigné ici.
#. **Simuler**
   Pour lancer la simulation en utilisant le mode renseigné dans la liste (21)
#. **Exporter uniquement des simulations**
   Permet d'exporter ??? TODO
#. **Lanceur de simulations**
   Pour choisir le lanceur de simulations qui sera utilisé pour réaliser la
   simulation :

   - dans un sous-processus (par défaut) lance les simulations dans des
     sous-processus. Si plusieurs simulations sont lancées an même temps,
     ISIS-Fish utilisera les différents cœurs de la machine utilisée pour
     effectuer plusieurs simulations en parallèle. ISIS-Fish lancera une
     simulation par cœur (2 cœurs = 2 simulations en parallèle).

   - sur le serveur CAPARMOR lance les simulations sur le super-calculateur de
     l'IFREMER CAPARMOR. Pour utiliser correctement CAPARMOR, reportez-vous à la
     section Installation de ce manuel.
#. **Sauver les paramètres de la simulation**
   Enregistre les paramètres enregistrés afin de les recharger lors du prochain
   lancement de simulation.

Onglet **Analyse de sensibilité** - Détermination des facteurs
==============================================================

.. image:: sensitivity_02_sensitivityFactors.png

L'onglet de saisie des facteurs permet à l'utilisateur de naviguer dans la
pêcherie afin de choisir les facteurs qu'il souhaite étudier et de les
paramétrer.

#. **Arbre de navigation dans la pêcherie** :
   L'arbre permet de naviguer dans la pêcherie (interface identique à la saisie
   de pêcherie) afin de sélectionner les éléments factorisables.
#. **Liste des facteurs étudiés** :
   Liste des facteurs qui seront étudiés lors de l'analyse de sensibilité.

   |tip| Un clic sur un facteur permet de le modifier. Un clic droit sur un
   facteur fait apparaître un menu contextuel pour le supprimer.

#. **Element factorisable** :
   Le petit icône présent sur certains éléments et qui change d'aspect au survol
   de ce dernier indique que l'élément est factorisable. Il suffit alors de
   cliquer dessus pour que l'interface de mise en facteur s'ouvre.


Saisie d'un facteur continu à partir d'un paramètre quantitatif
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: sensitivity_02_sensitivityFactorsContinuousNumber.png

Lorsqu'un paramètre quantitatif peut être factorisable, il peut être soit
continu, soit discret. Pour un facteur continu :

#. **Facteur continu** :
   Détermine que le facteur est continu
#. **Première valeur** :
   Valeur minimale du facteur.
#. **Dernière valeur** :
   Valeur maximale du facteur.
#. **Commentaires** :
   Commentaire sur le facteur. Permet de préciser des informations sur le
   facteur.
#. **Annuler** :
   Annuler les modifications effectuées (si le facteur était en cours de
   création, il n'est pas créé).
#. **Sauver** :
   Enregistrer le facteur. On peut ensuite retrouver le facteur dans la liste
   des facteurs de l'analyse de sensibilité.

Saisie d'un facteur discret à partir d'un paramètre quantitatif
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: sensitivity_02_sensitivityFactorsDiscreteNumber.png

Lorsqu'un paramètre quantitatif peut être factorisable, il peut être soit
continu, soit discret. Pour un facteur discret :

#. **Facteur discret** :
   Détermine que le facteur est discret.
#. **Nombre de facteurs** :
   Le nombre de modalités du facteur.
#. **Valider** :
   Valide le nombre de modalités entrées et crée un onglet par modalité. Il
   faudra par la suite rentrer la valeur prise par chaque modalité dans chaque
   onglet.
#. **Onglets de modalité** :
   Permet de choisir et d'afficher les modalités prises par le facteur.
#. **Contenu d'onglet de modalité** :
   Chaque onglet de modalité contient la valeur prise par le facteur pour chaque
   modalité. Il faut éditer chacune de ces valeurs.
#. **Commentaires** :
   Commentaire sur le facteur. Permet de préciser des informations sur le
   facteur.
#. **Annuler** :
   Annuler les modifications effectuées (si le facteur était en cours de
   création, il n'est pas créé).
#. **Sauver** :
   Enregistrer le facteur. On peut ensuite retrouver le facteur dans la liste
   des facteurs de l'analyse de sensibilité.

Saisie d'un facteur continu à partir d'une équation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: sensitivity_02_sensitivityFactorsContinuousEquation.png

Lorsqu'un paramètre équation peut être factorisable, il peut être soit continu,
soit discret. Pour un facteur continu :

#. **Facteur continu** :
   Détermine que le facteur est continu
#. **Name** :
   Nom de la constante de l'équation a faire varier
#. **Value** :
   Valeur nominale (usuellement valeur par défaut dans l'équation).
#. **Coef (in %)** :
   Coefficient modificateur de la valeur, exprimée en pourcentage (pour un
   coefficient de 0.05, entrer 5).
#. **Operator** :
   Opérateur modificateur de la valeur. Cet opérateur, combiné avec le
   coefficient permet de calculer les valeurs minimales et maximales prises par
   la constante. Ces valeurs sont calculées de la manière suivante :

   min = value - (value (operator) coefficient)

   max = value + (value (operator) coefficient)

#. **Valider** :
   Vérifie que la constante existe bien, et modifie l'équation en base afin que
   le facteur soit pris en compte. La modification de l'équation en base ne
   change pas les résultats des simulations qui peuvent être lancer avec cette
   dernière.
   |tip| Il est possible de créer plusieurs facteurs pour une même équation dans
   le cas où cette dernière aurait plusieurs constantes. ISIS-Fish crée autant
   de facteurs que de constantes validées et déclarées dans cette interface.
#. **Ajouter** :
   Ajouter une constante factorisable.
#. **Supprimer** :
   Supprimer une constante factorisable.
#. **Commentaires** :
   Commentaire sur le facteur. Permet de préciser des informations sur le
   facteur.
#. **Annuler** :
   Annuler les modifications effectuées (si le facteur était en cours de
   création, il n'est pas créé).
#. **Sauver** :
   Enregistrer le(s) facteur(s). On peut ensuite retrouver le(s) facteur(s) dans
   la liste des facteurs de l'analyse de sensibilité.

Saisie d'un facteur discret à partir d'une équation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: sensitivity_02_sensitivityFactorsDiscreteEquation.png

Lorsqu'un paramètre équation peut être factorisable, il peut être soit continu,
soit discret. Pour un facteur discret :

#. **Facteur discret** :
   Détermine que le facteur est discret.
#. **Nombre de facteurs** :
   Le nombre de modalités du facteur.
#. **Valider** :
   Valide le nombre de modalités entrées et crée un onglet par modalité. Il
   faudra par la suite rentrer la valeur prise par chaque modalité dans chaque
   onglet.
#. **Onglets de modalité** :
   Permet de choisir et d'afficher les modalités prises par le facteur.
#. **Contenu d'onglet de modalité** :
   Chaque onglet de modalité contient la valeur prise par le facteur pour cette
   dernière. Il faut éditer chacune de ces valeurs.
   [tip| Chaque onglet contient un éditeur d'équation, il est donc possible
   d'ouvrir l'éditeur externe pour avoir plus d'information sur l'équation ou
   sauver comme modèle.
#. **Commentaire** :
   Commentaire sur le facteur. Permet de préciser des informations sur le
   facteur.
#. **Annuler** :
   Annuler les modifications effectuées (si le facteur était en cours de
   création, il n'est pas créé).
#. **Sauver** :
   Enregistrer le facteur. On peut ensuite retrouver le facteur dans la liste
   des facteurs de l'analyse de sensibilité.

Saisie d'un facteur continu à partir d'une matrice
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: sensitivity_02_sensitivityFactorsContinuousEquation.png

Lorsqu'un paramètre équation peut être factorisable, il peut être soit continu,
soit discret. Pour un facteur continu :

#. **Facteur continu** :
   Détermine que le facteur est continu
#. **Nom du facteur** :
   Nom du facteur
#. **Value** :
   Valeur nominale de la matrice (usuellement et par défaut valeur dans la base).
#. **Coefficient (en %)** :
   Coefficient modificateur de la valeur, exprimée en pourcentage (pour un
   coefficient de 0.05, entrer 5).
#. **Opérateur** :
   Opérateur modificateur de la valeur. Cet opérateur, combiné avec le
   coefficient permet de calculer les valeurs minimales et maximales prises par
   la matrice. Ces valeurs sont calculées de la manière suivante :

   min = value - (value (operator) coefficient)

   max = value + (value (operator) coefficient)

#. **Commentaires** :
   Commentaire sur le facteur. Permet de préciser des informations sur le
   facteur.
#. **Annuler** :
   Annuler les modifications effectuées (si le facteur était en cours de
   création, il n'est pas créé).
#. **Sauver** :
   Enregistrer le facteur. On peut ensuite retrouver le facteur dans la liste
   des facteurs de l'analyse de sensibilité.

Saisie d'un facteur discret à partir d'une matrice
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: sensitivity_02_sensitivityFactorsDiscreteMatrix.png

Lorsqu'un paramètre matrice peut être factorisable, il peut être soit continu,
soit discret. Pour un facteur discret :

#. **Facteur discret** :
   Détermine que le facteur est discret.
#. **Nombre de facteurs** :
   Le nombre de modalités du facteur.
#. **Valider** :
   Valide le nombre de modalités entrées et crée un onglet par modalité. Il
   faudra par la suite rentrer la valeur prise par chaque modalité dans chaque
   onglet.
#. **Onglets de modalité** :
   Permet de choisir et d'afficher les modalités prises par le facteur.
#. **Contenu d'onglet de modalité** :
   Chaque onglet de modalité contient la valeur prise par le facteur pour cette
   dernière. Il faut éditer chacune de ces valeurs.
#. **Commentaire** :
   Commentaire sur le facteur. Permet de préciser des informations sur le
   facteur.
#. **Annuler** :
   Annuler les modifications effectuées (si le facteur était en cours de
   création, il n'est pas créé).
#. **Sauver** :
   Enregistrer le facteur. On peut ensuite retrouver le facteur dans la liste
   des facteurs de l'analyse de sensibilité.

Onglet **Méthode de la sensibilité** - Détermination de la méthode d'exploration des domaines des facteurs et des variables à étudier.
======================================================================================================================================

.. image:: sensitivity_04_sensitivityMethod.png

#. **Méthode** :
   Choix de la méthode d'exploration a utiliser
#. **Paramètres de la méthode d'exploration** :
   Permet de paramétrer la méthode d'exploration. Des valeurs par défaut sont
   pré-remplies, elles peuvent ne pas être adaptées au cas d'étude. Il faut
   toujours vérifier ces paramètres.
#. **Modalités** :
   Certaines méthodes d'exploration discrétisent le domaine d'exploration des
   facteurs continus, il faut alors indiquer le nombre de modalité souhaité pour
   chaque facteur. (Ce tableau n'apparaît que si nécessaire).
#. **Export** :
   Liste des exports de sensibilité (variables à étudier) connues par ISIS-Fish.
   Il est possible ici d'effectuer des multi-sélection.
#. **Liste des exports de sensibilité utilisés** :
   Liste des exports de sensibilité (variables à étudier) qui seront utilisées
   dans l'analyse de sensibilité.
#. **Ajouter** un export de sensibilité :
   Ajoute un ou plusieurs exports de sensibilité à l'analyse.
#. **Supprimer** un export de sensibilité :
   Supprime un export de sensibilité de l'analyse.
#. **Effacer** :
   Supprime tous les exports de sensibilité de l'analyse.
#. Paramètres des exports de sensibilité** :
   Quand un export de sensibilité assigné à l'analyse est sélectionné, ses
   paramètres apparaissent dans ce tableau. Afin de voir l'analyse de
   sensibilité se dérouler correctement, il est nécessaire de renseigner tous
   les paramètres de tous les exports de sensibilité sélectionnés.

Onglet **Export des résultats** - Choix des exports annexes
===========================================================
Cet onglet permet de sélectionner les résultats de la simulation que l'on veut
exporter.

.. image:: sensitivity_05_tabExportResult.png

#. **liste des résultats exportables** : on sélectionne parmis la liste des
   résultats connus par ISIS-Fish, ceux que l'on veut exporter une fois la
   simulation terminée.  TODO Voir lien avec résultat choisis.
#. **chemin du répertoire** : chemin du répertoire où exporter les résultats
   (voir configuration)
#. **...** : pour rechercher un répertoire en local, où l'on veut exporter les
   résultats.
#. **sauvegarder pour prochaine simulation** : pour sauvegarder le choix des
   résultats à exporter ainsi que le répertoire où exporter pour une prochaine
   simulation.

Onglet **Choix de résultats** - Choix des résultats à enregistrer pour visualisation.
=====================================================================================

.. image:: sensitivity_06_tabResultChoice.png

#. **résultats disponibles** : liste des résultats disponibles pour la simulation.
   On sélectionne dans cette liste, ceux que l'on veut traiter. TODO lien avec les exports
#. **sauvegarder pour prochaine simulation** : pour sauvegarder le choix des
   résultats à traiter pour une prochaine simulation.

Onglet **Paramètres avancés** - Choix des paramètres avancés des simulations de l'analyse de sensibilité.
=========================================================================================================

.. image:: sensitivity_07_tabAdvancedParams.png

#. **configuration du simulateur** :  Cette première zone permet de configurer
   le simulateur à utiliser pour simuler. Elle comprend :

   #. **sélection du simulateur** : une liste déroulante de simulateurs connus
      par ISIS-Fish.
   #. **simulation statique** : cochez ici pour indiquer une simulation
      statique, (décoché signifie TODO).
   #. **simulation cache** : cochez ici pour indiquer une simulation cache,
      (décoché signifie TODO).

#. **paramétrage des niveaux de tracage** : cette zone permet la configuration
   des niveaux de log à utiliser pendant une simulation.
   On distingue troix types de tracage différents:

   #. **tracage du simulateur** qui regroupe les traces propres au simulateur
      (par défaut niveau **INFO**)
   #. **tracage des scripts** qui regroupe les traces propres aux scripts (par
      défaut niveau **INFO**)
   #. **tracage des librairies** qui regroupe les traces du reste. (par défaut
      niveau **ERREUR**)

#. **ajout de paramètres libres** : dans cette zone on peut ajouter de nouveaux
   paramètres libres. Le boutton **ajouter** ajoute dans la liste des paramètres
   ajoutés, le paramètres défini par le nom de son tag et la valeur du tag.
#. **liste des paramètres libres ajoutés** : cette liste contient l'ensemble
   des paramètres libres ajoutés.
#. **supprimer**, pour supprimer un paramètre libre ajouté (il faut
   sélectionner avant un paramètre libre.)
#. **sauver pour la prochaine simulation** : pour sauvegarder la configuration
   du simulateur pour une prochaine simulation. TODO

Onglet **Analyse des résultats** - Visualisation des résultats d'analyses de sensibilité
========================================================================================

.. image:: sensitivity_08_results.png

#. **Liste des analyses de sensibilité** :
   Liste toute les analyses de sensibilité connues par ISIS-Fish (attention,
   certaines peuvent ne pas être terminées).
#. **Analyse des résultats** :
   Lance l'analyse des résultats à partir des résultats des simulations et des
   scripts d'analyse de sensibilité et les affiches dans l'espace de
   visualisation.

   |tip| L'analyse des résultats à toujours lieu lors de la fin de la dernière
   simulation de l'analyse. Ce bouton permet de relancer des calculs d'analyse
   de sensibilité à partir des résultats des simulations sans avoir à relancer
   ces dernières. Cela permet entre autre de modifier les scripts d'analyse de
   sensibilité après que les simulations aient tourné.

#. **Afficher les résultats** :
   Affiche les résultats de l'analyse de sensibilité.

#. **Espace de visualisation** :
   Espace où sont affichés les différents résultats des analyse de sensibilité.
   Ne sont affichés que les résultats qui ont été exportés par le script
   d'analyse de sensibilité.

Récupération des résultats d'analyse de sensibilité dans R
==========================================================

Les scripts d'analyse de sensibilité utilisent R pour réaliser leurs calculs.
Les objets utilisés sont standardisés et la session R utilisée est enregistrée.
Il est donc possible de récupérer les résultats des analyses de sensibilité dans
R afin de les traiter plus en détail.

Trouver le fichier .RData
~~~~~~~~~~~~~~~~~~~~~~~~~

Les résultats sont situés dans un fichier « .RData » situé dans le répertoire
isis-export déclaré dans votre fichier de configuration. Il est nommé de la
manière suivante : nomDeLAnalyse.RData

Contenu du fichier .RData
~~~~~~~~~~~~~~~~~~~~~~~~~

La session R contenue dans le fichier .RData contient de nombreux objets R
obtenus aux différentes étapes de l'analyse dans Isis.

isis.factor
!!!!!!!!!!!

isis.factor est un data.frame à 5 colonnes et une ligne par facteur organisé
comme ceci :

- colonne 1 : nomFacteur
- colonne 2 : Nominal (valeur dans la base)
- colonne 3 : Continu (TRUE/FALSE)
- colonne 4 : Binf (valeur minimum)
- colonne 5 : Bsup (valeur maximum si continu, nombre de modalité si discret)

Il a les attributs suivants :

- un attribut par facteur discret : nomFacteur : list(modalités)
- un attribut nomModel : « isis-fish-externeR »

isis.factor est enregistré dans R de la manière suivante :
nomdel'analyse_0.isis.factor (tous les espaces sont enlevés dans R).

isis.factor.distribution
!!!!!!!!!!!!!!!!!!!!!!!!

Isis.factor.distribution est un data.frame à 3 colonnes et une ligne par facteur
organisé comme ceci :

- colonne 1 : NomFacteur
- colonne 2 : NomDistribution
- colonne 3 : ParametreDistribution

isis.factor.distribution est enregistré dans R de la manière suivante :
nomdel'analyse_0.isis.factor.distribution (tous les espaces sont enlevés dans
R).

isis.methodExp
!!!!!!!!!!!!!!

Isis.methodExp est une liste contenant trois objets :

- objet 1 : isis.factor
- objet 2 : isis.factor.distribution
- objet 3 : call

Il a les attributs suivants :

- un attribut nomModel : « isis-fish-externeR »

isis.methodExp est enregistré dans R de la manière suivante :
nomdel'analyse_0.isis.methodExp (tous les espaces sont enlevés dans R).

isis.simule
!!!!!!!!!!!

Isis.simule est un data.frame avec une ligne par simulation et un nombre de
colonne équivalent au nombre de facteur additionné au nombre de résultats pour
l'analyse :

- colonne 1 à k : valeurs des k facteurs.
- colonne k à n : valeurs des résultats des simulations

Il a les attributs suivants :

- un attribut nomModel : « isis-fish-externeR »
- un attribut call : la méthode qui a généré les simulations.

isis.simule est enregistré dans R de la manière suivante :
nomdel'analyse_0.isis.simule (tous les espaces sont enlevés dans R).

isis.methodAnalyse
!!!!!!!!!!!!!!!!!!

Isis.methodAnalyse est une liste contenant 5 objets :

- objet 1 : isis.factor
- objet 2 : isis.factor.distribution
- objet 3 : isis.simule
- objet 4 : call_method
- objet 5 : analysis_result (objet R contenant les résultats de l'analyse, dans
le cas d'un résultat calculé par une aov, l'objet est une liste contenant l'aov
et les indices de sensibilité)

isis.methodAnalyse est enregistré dans R de la manière suivante :
nomdel'analyse_0.nomduresultat.isis.methodAnalyse (tous les espaces sont enlevés
dans R).

Liste de tous les objets de la session R
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

N'oubliez pas que vous pouvez obtenir tous les objets de la session R en
utilisant la fonction ls() dans R.
