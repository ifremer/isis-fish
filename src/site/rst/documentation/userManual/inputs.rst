.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
=====================
Saisie d'une pêcherie
=====================

La fenêtre de saisie de pêcherie permet de créer, visualiser et modifier des pêcheries.

.. contents::

Description des différentes zones de l'interface
------------------------------------------------

.. image:: input_00.png

#. les menus de l'éditeur de pêcheries.
#. le nom d'une nouvelle région à créer.
#. pour créer une nouvelle région.
#. la liste des régions détectées par ISIS-Fish.
#. la zone de navigation dans les objets de la pêcherie.
#. la zone d'édition des données de la pêcherie.
#. la zone classique de statut général.

Une pêcherie contient une région.

On peut soit créer entièrement la région en renseignant son nom (zone 2) puis
en cliquant sur le bouton **Nouvelle région** (zone 3).

On peut aussi sélectionner une région existante localement dans la liste
déroulante (zone 4).

Un dossier du nom de votre cas d'étude a ainsi été créé dans le menu déroulant et dans la colonne de gauche
ont été créées des sous dossiers correspondant à toutes les composantes de la pêcherie (zone 5).

Dans les deux cas, une fois la région chargée ou créée, les objets de la
pêcherie sont disponibles dans leur zone de navigation (zone 6).

La carte est affichée à droite.

On peut aussi éditer les caractéristiques de la région.


Les différentes actions des menus
---------------------------------

On détaille dans cette section, l'ensemble des actions réalisables par menu.
Il s'agit d'actions globales sur les pêcheries.

Menu **Région**
===============

.. image:: input_01_menuFile.png


#. **Importer une région** :
   Importer une région précédemment exportée depuis une autre instance d'IsisFish v3.
#. **Importer une région et la rennommer** :
   Importer une région précédemment exportée depuis une autre instance d'IsisFish v3 et
   renommer le nom de la région.
#. **Importer une région de la V2 d'IsisFish** :
   Importer une région précédemment exportée depuis une autre instance d'IsisFish v2.
#. **Importer la région d'une simulation** :
   Importer une région depuis une simulation localement existante.
   Une interface de sélection de simulation s'affiche.
   Une fois le nom de la simulation choisi, le nom de la région est demandé.
   Ensuite, la région est extraite de la simulation et sauvegardée localement.
#. **Exporter la région** :
   Exporter la région dans un fichier compressé. L'archive produite pourra être
   importée dans une autre instance d'IsisFish v3.
   Une région doit être sélectionnée au préalable.
#. **Copier région** :
   Permet la recopie simple de région.
   Une région doit être sélectionnée au préalable.
#. **Supprimer localement** une région :
   Permet de supprimer localement une région.
   Une région doit être sélectionnée au préalable.
   Attention, cette opération est irréversible, une fois supprimée, la pêcherie
   ne sera plus récupérable.

Menu **Serveur**
================

.. image:: input_02_menuServer.png

Ce menu regroupe les fonctionnalités de communication avec le serveur de données.

#. **Ajouter une région** :
   Permet d'envoyer au serveur distant une nouvelle région
   Une boite de dialogue apparaît où sont affichés les différents messages de
   sauvegarde déjà rentrés pour cette pêcherie.
   En appuyant sur le bouton Ok, la pêcherie est ajoutée sur le serveur distant si vous avez les
   droits d'écriture sur ce serveur (voir chapitre configuration vcs).
#. **Sauvegarder les changements** d'une région :
   TODO est-ce actif, je ne crois pas...
#. **Supprimer localement et à distance** :
   Permet de supprimer une région localement mais aussi sur le serveur distant.
   Vous devez disposer de droits d'écriture sur le serveur distant.
   Attention : A utiliser avec précaution!


Créer une nouvelle pêcherie
---------------------------

La saisie d'une pêcherie se déroule en plusieurs étapes :

#. création ou sélection de la région à utiliser dans la pêcherie
#. saisie des mailles
#. saisie des zones
#. saisie des ports
#. saisie des espèces
#. saisie des populations
#. saisie des engins
#. saisie des métiers
#. saisie des marées
#. saisie des types de navire
#. saisie des flottilles
#. saisie des stratégies


1. **Création** ou **sélection** de région
==========================================

Une fois la région créée ou chargée voila à quoi ressemble l'interface.

.. image:: input_04_create.png

Les latitudes et longitudes sont en degrés décimaux.

#. **Nom de la région**
   Le nom de la région (il s'agit d'un nom d'objet de la pêcherie, il est
   préférable de normaliser ces noms, par exemple n'utiliser que des caractères
   alphanumériques sans ponctuation).
   La région est un rectangle avec une grille régulière de mailles.
#. **Latitude minimum** :
   latitude minimum de la région (valeur réelle)
#. **Latitude maximum** :
   latitude maximum de la région (valeur réelle)
#. **Longitude minimum** :
   longitude minimum de la région (valeur réelle)
#. **Longitude maximum** :
   longitude maximum de la région (valeur réelle)
#. résolution spatiale  (**latitude**) :
   étendue de chaque maille en latitude (valeur réelle)
#. résolution spatiale  (**longitude**) :
   étendue de chaque maille en longitude (valeur réelle)
#. liste des cartes : TODO
#. **Ajouter une carte** :
   Si vous désirez changer la carte par défaut pour une carte plus
   précise, cliquez sur Ajouter carte et sélectionnez la carte à
   ajouter grâce au menu défilant.
   Il n'est nécessaire d'ajouter que la carte au format shp néanmoins les deux cartes shp et ssx doivent être situées dans le
   même dossier
#. **Supprimer la carte** :
   Pour supprimer une carte précédemment ajoutée.
#. **Commentaires** :
   Des informations complémentaires sur la région peuvent être ajoutées
   grâce aux encadrés **Commentaires**. Elles seront sauvegarder en même temps
   que la région.
#. **Ajouter un fichier de mailles** :
   Un fichier contenant des mailles prédéfinies peut aussi être chargé.
   Cliquez sur **...***
#. **Sauver** la région :
   Une fois que les frontières de la région ont été définies (la carte
   et le fichier de mailles prédéfinies ajoutés si nécessaire), cliquez
   sur **Sauver**.
#. **Annuler** :
   Pour annuler les modifications effectuées et revenir à la dernière version
   enregistrée.
#. **Vérifier** :
   Pour vérifier la cohérence de la pêcherie.
#. **Continuer** vers les **mailles** :
   A la fin de ce stade, la région ainsi qu'une grille de mailles ont été crées.

   Afin de continuer la paramétrisation, cliquez sur **Continuer vers les mailles**
   ou sélectionner l'onglet **Mailles** dans le menu de gauche.

2. **Saisie des mailles**
=========================

.. image:: input_05_inputMailles.png

La feuille de saisie des mailles permet à l'utilisateur de renommer les
mailles. Il est aussi possible de définir si la maille est en fait de la terre
en cochant la case **Terre**.

#. **Maille** dans la navigation :
   On peut sélectionner une maille à partir de cette zone. Les autres zones
   seront alors mises à jour avec les données de cette maille.
#. **Maille** dans la liste :
   On peut sélectionner une maille à partir de cette liste déroulante des noms
   des mailles de la région. Les autres zones seront alors mises à jour avec les
   données de cette maille.
#. **Maille** sur la carte :
   On peut enfin sélectionner une maille en la sélectionnant sur la carte. Les
   autres zones seront alors mises à jour avec les données de cette maille.
#. **Nom** de maille :
   Pour modifier le nom de la maille, par défaut un nom de maille est l'expression
   de sa latitude suivi de sa longitude; exemple : La44.5Lo-3.0
#. **Latitude** :
   latitude du coin bas gauche de la maille sélectionnée exprimée en degré.
#. **Longitude** :
   longitude du coin bas gauche de la maille sélectionnée exprimée en degré.
#. **Terre** :
   Case à cocher permettant de spécifier si la maille est sur terre ou non.
#. **Commentaires** :
   Pour saisir un commentaire lié à cette maille. Des informations
   complémentaires peuvent être ajoutées grâce cette fenêtre. Chaque
   commentaire est lié à une maille, il est ainsi possible d'ajouter un
   commentaire pour chaque maille préalablement définie.
#. **Sauver** :
   Pour sauver les modifications sur cette maille. Ne pas oublier de sauver en
   cliquant sur ce bouton pour chaque modification de maille.
#. **Annuler** :
   Pour annuler les modifications sur cette maille et revenir à la dernière
   version sauvegardée.
#. **Continuer** vers les zones :
   Afin de poursuivre, cliquez sur **Continuer vers les Zones** ou sélectionner
   l'onglet **Zones** dans le menu de gauche.


3. **Saisie des zones**
=======================

Les zones **Populations**, les zones **Métier** et les zones de gestion doivent être
créées respectivement avant les populations, les métiers et les mesures de
gestion.

.. image:: input_06_inputZones.png

#. **Nouveau** :
   On commence toujours par créer la zone avant de modifier ses valeurs, on
   clique ici pour créer la zone vide. Une fois la zone créée, elle apparaît
   dans la combo-box **Choisir une zone** et il est possible de la sélectionner et
   de la renommer.
#. **Nom** :
   Pour renseigner le nom de la zone une fois celle-ci créée. Voir remarque sur
   les noms des objets de la pêcherie.
#. **Sauver** :
   Permet de sauver en base la nouvelle zone.
   Ne pas oublier de sauver à la fin de ces opérations.

   A ce stade, la zone a été créée, il faut désormais lui associer des mailles.
#. **Choisir une zone** :
   La **zone** dans la zone de navigation des objets de la pêcherie.
#. **Sélection des mailles**  de la zone :
   Sélectionner les mailles composant cette zone soit en cochant les cases une
   par une soit en les sélectionnant directement sur la carte.
#. **les mailles de la zone représentées sur la carte** :
   cliquer sur la maille à sélectionner, elle devient verte une fois sélectionnée.
#. **Annuler** :
   Pour annuler les changements effectués dans la zone sélectionnée.
#. **Supprimer** :
   Pour supprimer une zone de la pêcherie. Attention cette opération est
   irréversible, une fois la zone supprimée, elle n'est plus récupérable.
#. **Commentaires** :
   Des informations complémentaires peuvent être ajoutées grâce cette fenêtre.
   Chaque commentaire est lié à une **Zone**, il est ainsi possible d'ajouter un
   commentaire pour chaque Zone préalablement définie.
#. **Continuer** vers les ports :
   Afin de poursuivre la saisie des paramètres, cliquer sur **continuer vers
   les ports** ou cliquer sur l'onglet Ports dans le menu de gauche.

ATTENTION : il est possible de créer des zones population se chevauchant pour une même population


4. **Saisie des ports**
=======================

Les ports sont utilisés afin de pouvoir calculer les coûts de transport.
Un port est contenu dans une maille correspondant à la localisation
géographique du port.

Chaque port est localisé exactement sur une maille.

.. image:: input_07_inputPorts.png

#. **Créer** :
   On commence toujours par créer le port avant d'effectuer des modifications.
   On clique ici pour créer un port vide. Une fois le port créé, il apparaît
   dans la zone de navigation **Choisir un port** et il est possible de le
   sélectionner et de l'éditer.
#. **Nom du port** :
   Le nom du port à renseigner (Voir remarque sur les noms des objets de la
   pêcherie.)
#. **Sauver** :
   Pour sauver le port, en base. Ne pas oublier de sauver à la fin de ces
   opérations.

   A ce stade, le port a été créé, il faut désormais lui associer une maille.
#. **Choisir un port** :
   Le port dans la zone de navigation des objets de la pêcherie.
#. **Sélection de la maille du port** :
   Sélectionner la maille où se situe le port en cochant une case ou en les
   sélectionnant directement sur la carte.
#. **La maille du port représenté sur la carte** :
   Cliquer sur la maille à sélectionner, elle devient alors verte.
#. **Annuler** :
   Pour annuler les changements effectués sur le port sélectionné.
#. **Supprimer** :
   Pour supprimer le port de la pêcherie. Attention cette opération est
   irréversible, une fois le port supprimé, il ne sera plus récupérable.
#. **Commentaires** :
   Des informations complémentaires peuvent être ajoutées grâce à cette fenêtre.
   Chaque commentaire est lié à un Port, il est ainsi possible d'ajouter un
   commentaire pour chaque Port préalablement défini.
#. **Continuer** vers les espèces :
   Afin de poursuivre la saisie des paramètres, cliquer sur l'onglet
   **Espèces** dans le menu de gauche.


5. **Saisie des espèces**
=========================

.. image:: input_08_inputSpecies.png

#. **Créer** une espèce :
   On commence toujours par créer l'espèce avant de modifier ses
   caractéristiques. On clique ici pour créer une espèce vide.
   Une fois l'espèce créée, elle apparaît dans la zone de navigation des
   objets de la pêcherie. Il est possible de la sélectionner et de l'éditer.
#. **Nom de l'espèce** :
   On nomme l'espèce une fois celle-ci créée. (Voir remarque sur les noms des
   objets de la pêcherie.)
#. **Sauver** :
   Permet de sauver en base la nouvelle espèce.
   Ne pas oublier de sauver à la fin de ces opérations.

   A ce stade, l'espèce est crée, il faut désormais terminer la configuration
   de ses caractéristiques.
#. **Nom scientifique** :
   Pour renseigner le nom scientifique de l'espèce. Le nom commun uniquement sera
   utilisé par la suite. La plupart des paramètres biologiques à définir
   ensuite dépendent de la Dynamique choisie. Les populations peuvent soit
   avoir une dynamique en âge, soit avoir une dynamique en stades (incluant
   aussi la structuration en longueur).
#. **Code rubbin** TODO
#. **CEE** TODO
#. **Structuré en age** :
   Dynamique de la population en age, cela correspondant au nombre de classes
   d'âge.
#. **Structuré** en longueur :
   Dynamique de la population en longueur, cela correspond au nombre de classes
   de longueur.
#. **Commentaires** :
   Des informations complémentaires peuvent être ajoutées grâce à cette fenêtre.
   Chaque commentaire est lié à une espèce, il est ainsi possible d'ajouter un
   commentaire pour chaque espèce préalablement définie.
#. **Annuler** :
   Pour annuler les changements effectués sur l'espèce sélectionnée et revenir
   à la dernière version sauvegardée.
#. **Supprimer** :
   Pour supprimer l'espèce de la pêcherie. Attention cette opération est
   irréversible, une fois l'espèce supprimée, elle ne sera plus récupérable.
#. **Choisir une espèce** :
   L'espèce dans la zone de navigation des objets de la pêcherie.
#. **Continuer** vers les populations  :
   Afin de poursuivre la saisie des paramètres, cliquer sur l'onglet
   **Populations** de l'espèce dans le menu de gauche.

6. **Saisie des populations**
=============================

Les populations sont décrites au travers de huit onglets. Une fois la population
créée, renseignez les différents paramètres dans l'ordre suivant :

.. image:: input_09_inputPopulations.png

#. onglet de saisie des paramètres de base de la population
#. onglet de saisie des zones de la population
#. onglet de saisie des saisons de la population
#. onglet de saisie des équations de population
#. onglet de saisie des équations de reproduction de la population
#. onglet de saisie des groupes de populations
#. onglet de saisie des équations de capturabilité de la population
#. onglet de saisie des migrations de la population
#. **population** dans la navigation :
   Vous pouvez choisir dans la zone de navigation des objets de la pêcherie,
   les populations à éditer.
#. **continuer**
   Une fois les modifications sur les populations terminées, vous pouvez
   continuer vers la saisie des engins.

La saisie des populations requiert la définition d'un certain nombre d'équations
dont voici la liste:

#. **équation de croissance** :
   Si le dernier groupe est défini comme un **groupe plus**, la fonction de
   croissance de la population (lorsqu'elle est structurée en âge) et les
   paramètres correspondant.

   .. image:: inputEquation_00_Growth.png

#. **équation de croissance inverse** :
   Fonction inverse de la croissance de la population (lorsqu'elle est
   structurée en stade).

   .. image:: inputEquation_01_GrowthReverse.png

#. **équation de taux de mortalité naturelle** :

   .. image:: inputEquation_02_NaturalDeathRate.png

#. **équation de poids principal** :

   .. image:: inputEquation_03_MeanWeight.png

#. **équation de prix** :

   .. image:: inputEquation_04_Price.png

#. **équation de migration** :

   .. image:: inputEquation_05_Migration.png

#. **équation d'émigration** :

   .. image:: inputEquation_06_Emigration.png

#. **équation d'immigration** :

   .. image:: inputEquation_07_Immigration.png

#. **équation de reproduction** :
   Une équation donnant le nombre d'œufs émis en fonction de l'abondance des
   reproducteurs et d'autres paramètres dont, par exemple le coefficient de la
   fécondité.

   .. image:: inputEquation_08_GearSelectivity.png


Onglet **Saisie des populations**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Les paramètres de base incluent tous les paramètres biologiques définis à
l'échelle de la population.

.. image:: input_10_inputPopulations_tabBase.png

#. **Nouveau** :
   On commence toujours par créer une nouvelle population, avant d'en modifier
   les caractéristiques.
#. **Sauver** :
   Sauver la population en base.
#. **Annuler** :
   Annuler les modifications effectuées sur la population et revenir sur la
   dernière version sauvegardée.
#. **Supprimer** :
   Supprimer la population sélectionnée de la pêcherie.
#. **Reconstruire** les groupes :
   Créer les groupes de la population.
   Cela ouvrira différentes boites de dialogue selon la manière dont les
   populations sont structurées, ainsi que de la manière dont l'utilisateur
   veut décrire les classes. TODO (voir figures des dialogues).

   Pour effectuer cette opération, il est possible d'avoir besoin des équations de
   croissance et autres. Il faudra alors les avoir préalablement renseignées.
#. **Structuration dynamique** :
   Une fois que les interfaces concernant les fonctions de croissance ou les
   fonctions inverse de croissance ont été renseignées, il est possible de
   vérifier les paramètres grâce à la table.
   Si les paramètres de croissance ont été modifiés, ne pas oublier de sauver.
#. **Sauver comme modèle** :
   Pour sauver une équation de type croissance dans les scripts d'ISIS-Fish (formules/growth TODO)

   Si la fonction de croissance créée peut a priori être réutilisée pour
   d'autres espèces, il peut être utile de la sauver comme modèle en
   utilisant ce bouton.
#. **Ouvrir l'éditeur** :
   Pour ouvrir l'éditeur d'équation de croissance (voir figure
   input_XX_equationGrowth).
#. **Sauver comme modèle** :
   Pour sauver une équation de type décroissance inverse dans les scripts d'ISIS-Fish (formules/reversegrowth TODO)

   Si la fonction de croissance inverse créée peut a priori être réutilisée
   pour d'autres espèces il peut être utile de la sauver comme modèle en
   utilisant ce bouton.
#. **Ouvrir l'éditeur** :
   Pour ouvrir l'éditeur d'équation de croissance inverse (voir figure
   input_XX_equationReverseGrowth).
#. **Nom** :
   Pour renseigner le nom de la population (voir remarque sur les noms des
   objets d'une pêcherie).
#. **Identifant géographique** :
   Pour renseigner l'identifiant de la population. Un petit commentaire de
   description de la population peut être ajouté ici.
#. **Nombre de groupes** :
   Une fois les classes de la population créées, on voit ici le nombre de
   groupes de la population.
#. **Groupe de maturité** :
   Pour choisir le groupe de maturité. Il faut avoir créé les classes de la
   population avant. Il s'agit de l'âge auquel 50% de la population est mature.

   Saisir l'âge de maturité des individus. Cette valeur pourra être utilisée
   comme variable dans l'équation de reproduction. L'âge de maturité est
   exprimée en années.
#. **plusGroup** :
   Pour définir une population de type **plusGroup**
   Cocher la case si la dernière classe de la population est définie comme un
   groupe plus.
   Si le dernier groupe est définit comme un groupe plus, la fonction de
   croissance de la population (lorsqu'elle est structurée en age) et les
   paramètres  correspondants.
#. **choix équation croissance** :
   Pour choisir l'équation de croissance à utiliser pour la population.
#. **éditeur équation croissance** :
   Pour éditer l'équation de croissance L'interface de saisie de la fonction
   de croissance permettant de calculer la taille en fonction de l'âge n'est a
   renseigner (et ne sera utilisée) que dans le cas de populations structurées
   en âge.

   Il est possible d'utiliser soit un modèle prédéfini et dans ce cas la seule
   opération consiste à changer les paramètres pour ceux correspondant au cas d'étude.
   La seconde option consiste à écrire votre propre modèle en utilisant l'éditeur.

   Les ages sont exprimés en années (Classe Mature et Age) mais la variable
   age dans la fonction de croissance est exprimée en mois.
#. **choix équation croissance inverse** :
   Pour choisir l'équation de croissance inverse à utiliser pour la population
   (lorsqu'elle est structurée en longueur).

#. **éditeur équation croissance inverse** :
   Pour éditer l'équation de croissance inverse.

   Ne renseigner cette partie que si la population est structurée en stades
   (commentaires identique à ceux formulés au dessus pour les fonctions de
   croissances et les populations structurées en âge).
#. **commentaires** :
   Des informations complémentaires peuvent être ajoutées grâce à cette fenêtre.
   Chaque commentaire est lié à une population, il est ainsi possible d'ajouter
   un commentaire pour chaque population préalablement définie.


Onglet **Saisie des groupes de populations**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Permet d'éditer les paramètres de chaque groupe de la population.

.. image:: input_11_inputPopulations_tabGroup.png

#. **Liste des groupes** :
   Cette liste déroulante contient l'ensemble des groupes connus pour cette
   espèces (ils ont été générés pendant la création des classes).
#. **Poids principal** du groupe sélectionné :
   Il s'agit du poids moyen de la classe (en kg).
#. **Prix** du groupe sélectionné :
   Il s'agit du prix moyen de la classe (en euros par kg).
#. **Taux de mortalité naturelle** du groupe sélectionné :
   Il s'agit du résultat de l'équation de mortalité naturelle par année.
#. **Taux de reproduction** du groupe sélectionné :
   Intensité relative de la reproduction durant les mois de la saison de
   reproduction (i.e la proportion de matures se reproduisant durant le mois
   courant de la saison de reproduction).
   autrement dit : nombre d'œufs  - par kg de femelles de la classe (ne pas
   oublier de multiplier le poids moyen d'un individu de la classe et par le sexe
   ratio dans l'équation de reproduction). - par femelle (ne pas oublier de
   multiplier ensuite par le sexe ratio). - par individus.
#. **Age** du groupe sélectionné :
   Si la population est structurée en stades, cette valeur est l'âge moyen
   d'une classe (en années).
#. **Longueur** du groupe sélectionné :
   Si la population est structurée en âge, cette valeur est la longueur moyenne
   de la classe d'âge (en ???).
#. **Commentaires** :
   permet d'ajouter des commentaires.
#. **Sauver** :
   sauver les modifications sur les groupes de la population
#. **Annuler** :
   annuler les modifications effectuées sur les groupes de la population.


Onglet **Saisie des équations de population**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: input_12_inputPopulations_tabEquation.png

Pour saisir les équations de mortalité naturelle, poids principal et prix de la
population.

#. **Liste des populations** :
   Cette liste déroulante contient l'ensemble des équations de mortalité
   naturelles connues pour cette espèce.
#. **équation de taux de mortalité naturelle** :
   Pour éditer sans passer par l'éditeur d'équations, l'équation de mortalité
   naturelle.
#. **liste des populations** :
   Cette liste déroulante contient l'ensemble des équations de poids moyen
   connues pour cette espèce.
#. **équation de poids moyen** :
   Pour éditer sans passer par l'éditeur d'équations, l'équation de poids moyen.
#. **Liste des populations** :
   Cette liste déroulante contient l'ensemble des équations de prix
   connues pour cette espèce.
#. **équation de prix** :
   Pour éditer sans passer par l'éditeur d'équations, l'équation de prix.
#. **Sauver comme modèle** pour équation de taux de mortalité naturelle
   pour sauver une équation de type taux de mortalité naturelle dans les scripts d'ISIS-Fish (formules/NaturalDeathRate TODO)

   Si la fonction créée peut a priori être réutilisée pour d'autres population
   il peut être utile de la sauver comme modèle en utilisant ce bouton.
#. **Ouvrir l'éditeur** pour équation de taux de mortalité naturelle :
   pour ouvrir l'éditeur d'équation de croissance (voir figure input_XX_equationNaturalDeathRate TODO).
#. **Sauver comme modèle** pour équation de poids principal
   pour sauver une équation de type poids principal dans les scripts d'ISIS-Fish (formules/meanWeight TODO).

   Si la fonction créée peut a priori être réutilisée pour d'autres population
   il peut être utile de la sauver comme modèle en utilisant ce bouton.
#. **Ouvrir l'éditeur** pour équation de poids principal
   pour ouvrir l'éditeur d'équation de poids principal(voir figure
   input_XX_equationMeanWeight).
#. **Sauver comme modèle** pour équation de prix :
   pour sauver une équation de type prix dans les scripts d'ISIS-Fish (formules/price TODO)

   Si la fonction créée peut a priori être réutilisée pour d'autres population
   il peut être utile de la sauver comme modèle en utilisant ce bouton.
#. **Ouvrir l'éditeur** pour équation de prix
   pour ouvrir l'éditeur d'équation de prix  (voir figure input_XX_equationPrice).
#. **Sauver**
   sauver les modifications sur les équations de la population
#. **Annuler**
   annuler les modifications effectuées sur les équations de la population et
   revenir à la dernière version sauvegardée.


Onglet **Saisie des zones de populations**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: input_13_inputPopulations_tabZone.png

On définit ici les zones d'apparition et reproduction

#. **sélection des zones** :
   Dans cette liste, on retrouve l'ensemble des zones de la pêcherie.
#. **Sélection des zones de présence** :
   Dans cette liste, on retrouve l'ensemble des zones de la pêcherie. On doit
   sélectionner les zones de présence de la population durant l'année.
#. **Sélection des zones de reproduction** :
   Dans cette liste, on retrouve l'ensemble des zones de la pêcherie. On doit
   sélectionner les zones où se dérouleront les reproductions.
#. **Éditeur des correspondances** :
   Pour spécifier les correspondances entre les zones de reproduction, et les
   zones de recrutements si il y a plusieurs aires, i.e définir la proportion
   d'œufs émis de la zone de reproduction qui sont recrutés dans une zone de
   recrutement.
#. **Sauver** :
   Sauver les modifications effectuées sur les zones de la population.
#. **Annuler** :
   Annuler les modifications effectuées sur les zones de la population et
   revenir à la version précédente.

Onglet **Saisie des saisons** (population en age)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

On définit ici les saisons pour une population structurée en age.

.. image:: input_14_inputPopulations_tabSeason.png

#. **Liste des saisons** :
   Cette liste déroulante contient l'ensemble des saisons actuellement définies
   pour cette population.
#. **Plage de la saison** :
   On définit la saison en utilisant le curseur. Les mois en jaune correspondant
   aux mois sélectionnés de la saison.
#. **Changement de groupe** :
   Pour une saison donnée, si cette case est cochée, les poissons changeront de
   classe le premier mois de la saison.
   Pour une population structurée en age, le changement de groupe ne peut être appliqué que sur une seule saison par an.
#. **Reproduction** :
   Pour une saison donnée, si cette case est cochée, un tableau décrivant la
   distribution de la reproduction apparaît, chaque cellule doit être renseignée
   avec l'intensité de la reproduction par mois de la saison.
#. **Répartition des apparitions** :
   Chaque cellule doit être renseignée avec l'intensité de la reproduction par
   mois de la saison.
   Les valeurs dans le tableau ne sont enregistrées qu'après avoir validé en
   appuyant sur la touche **Entrée**. Les paramètres ont cependant besoin
   d'être sauvés en cliquant sur le bouton **Sauver**.
#. **Commentaires** :
   Des informations complémentaires peuvent être ajoutées grâce à cette zone.
   Chaque commentaire est lié à une saison, il est ainsi possible d'ajouter
   un commentaire pour chaque saison préalablement définie.
#. **Sauver** :
   Permet de sauver les modifications effectuées sur les saisons de la population.
#. **Annuler** :
   Permet d'annuler les modifications effectuées et de revenir à la dernière
   version sauvegardée.
#. **Nouveau** :
   On doit toujours commencer par créer une saison avant d'en modifier le
   contenue. Une fois la saison créée, on définit sa position dans l'année.
   Deux saisons ne peuvent pas se chevaucher. Par contre une saison peut
   commencer une année et finir l'année suivante (commencer en novembre et finir
   en février de l'année suivante par exemple).
#. **Supprimer** :
   Permet de supprimer une saison de la population, cette opération est
   irréversible, une fois supprimée la saison n'est plus récupérable.

Remarque : S'assurer que le changement de groupe n'est coché que pour une
saison, sinon les poissons pourront changer de classe plus d'une fois par an.

Onglet **Saisie des saisons** (population en longueur)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
TODO faire screenshot pour ce type de classe

On définit ici les saisons pour une population structurée en longueur.

.. image:: input_14_inputPopulations_tabSeasonStade.png

#. **Liste des saisons** :
   Cette liste déroulante contient l'ensemble des saisons actuellement définies
   pour cette population.
#. **Plage de la saison** :
   On définit la saison en utilisant le curseur. Les mois en jaune correspondant
   aux mois sélectionnés de la saison.
#. **Reproduction** :
   Pour une saison donnée, si cette case est cochée, un tableau décrivant la
   distribution de la reproduction apparaît, chaque cellule doit être renseignée
   avec l'intensité de la reproduction par mois de la saison.
#. **Distribution de la reproduction** :
   Chaque cellule doit être renseignée avec l'intensité de la reproduction par
   mois de la saison.
   Les valeurs dans le tableau ne sont enregistrées qu'après avoir validé en
   appuyant sur la touche **Entrée**. Les paramètres ont cependant besoin
   d'être sauvés en cliquant sur le bouton **Sauver**.
#. **Changement de groupe** :
   Sélectionner si le changement de classe doit se faire de manière spatialisée
   ou non (cocher la case appropriée).
   Renseigner la matrice de coefficient de changement de classes. Si la matrice
   est spatialisée, la taille de la matrice sera le nombre de zones de présence * le nombre de groupes.
   Pour chaque mois le coefficient correspond à la proportion d'individus passant
   de la classe i (en ligne) à la classe j(en colonne). Ces coefficients peuvent
   être définis par zones.
   Les valeurs dans le tableau ne sont enregistrées qu'après avoir validé en
   appuyant sur la touche **Entrée**. Les paramètres ont cependant besoin
   d'être sauvés en cliquant sur le bouton **Sauver**.
#. **Commentaires** :
   Des informations complémentaires peuvent être ajoutées grâce à cette zone.
   Chaque commentaire est lié à une saison, il est ainsi possible d'ajouter
   un commentaire pour chaque saison préalablement définie.
#. **Sauver** :
   Permet de sauver les modifications effectuées sur les saisons de la population.
#. **Annuler** :
   Permet d'annuler les modifications effectuées et de revenir à la dernière
   version sauvegardée.
#. **Nouveau** :
   On doit toujours commencer par créer une saison avant d'en modifier le
   contenue. Une fois la saison créée on définit sa position dans l'année.
   Deux saisons ne peuvent pas se chevaucher. Par contre une saison peut
   commencer une année et finir l'année suivante (commencer en novembre et finir
   en février de l'année suivante par exemple).
#. **Supprimer** :
   Permet de supprimer une saison de la population, cette opération est
   irréversible, une fois supprimée la saison n'est plus récupérable.

Onglet **Saisie de la capturabilité**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pour chaque groupe et chaque saison, le coefficient de capturabilité est défini
comme la probabilité d'un individu du groupe, présent dans la zone au
cours d'une saison, d'être capturé par une unité d'effort standardisé
appliquée par un engin non sélectif.

.. image:: input_15_inputPopulations_tabCapturability.png

#. **Édition** :
   Saisir ces coefficients pour chaque classe et chaque saison.
   Les valeurs dans la table ne seront saisies qu'après avoir appuyé sur **Entrée**
   (sinon les cellules restent bleues, ce qui signifie qu'elles ne seront pas sauvées).
#. **Commentaires** :
   Pour saisir des commentaire concernant la capturabilité de la population, ils seront
   sauvegardé en même temps que la population.
#. **Sauver** :
   Pour enregistrer les modifications effectuées sur cet écran en base.
#. **Annuler** :
   Pour annuler les modification effectuées sur cet écran et revenir à la
   dernière version enregistrée.

Onglet **Saisie des reproductions**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pour configurer les reproductions et recrutements de la population.

L'équation de reproduction est une équation qui permet de calculer le nombre
d'œufs produits en fonction de l'abondance des reproducteurs ainsi que
d'autres paramètres, pouvant inclure le coefficient de fécondité et le groupe Mature.

On définit l'étalement de recrutement : Proportion du groupe naissant qui
est effectivement recrutée chaque mois.
Cet étalement reflète la variabilité individuelle des œufs et des larves
dans le développement d'une cohorte mensuelle. Par défaut, le recrutement
d'une cohorte dure un mois, cela signifie que tous les individus de la
cohorte recrutent durant le même mois lorsqu'ils sont près à être recrutés.

.. image:: input_16_inputPopulations_tabRecruetment.png

#. **Liste des équations de reproduction** :
   On retrouve dans cette liste déroulante, l'ensemble des équation de
   reproduction connues dans la pêcherie.
#. **Éditeur de l'équation de reproduction** :
   Pour éditer l'équation de reproduction sans passer par l'éditeur d'équation
   externe.
#. **Nombre de mois** entre la ponte et le recrutement :
   Temps requis au groupe naissant pour être recruté. Cela détermine le
   début du recrutement à partir de la saison de reproduction. ce paramètre peut être
   égal à 0 (i.e. les juvéniles se recrutent instantanément au mois de
   reproduction : si reproduction en juin et nombre de mois entre ponte et recrutement = 0 alors recrutement en juin)
#. **Distribution du recrutement** :
   Pour modifier les proportions dans la distribution du recrutement.
   Par défaut toutes les valeurs sont à zéro.
#. **Nouvelle matrice** :
   Cliquer sur ce bouton afin de faire apparaître une boite de dialogue
   permettant de définir la durée du recrutement.
#. **Commentaires** :
   Des informations complémentaires peuvent être ajoutées grâce à cette zone
   d'édition. Chaque commentaire est lié à une population, il est ainsi possible
   d'ajouter un commentaire pour chaque population préalablement définie.
#. **Sauver comme modèle** pour équation de reproduction :
   pour sauver une équation de type reproduction dans les scripts d'ISIS-Fish (formules/reproduction TODO)
   Si la fonction créée peut a priori être réutilisée pour d'autres population
   il peut être utile de la sauver comme modèle en utilisant ce bouton.
#. **Ouvrir l'éditeur** pour l'équation de reproduction
   pour ouvrir l'éditeur d'équation de reproduction (voir figure
   input_XX_equationReproduction).
#. **Sauver**
   Sauver les modifications effectuées.
#. **Annuler**
   Annuler les modifications effectuées et revenir à la dernière version
   sauvegardée..

Remarque :  Si la seule relation disponible est une relation Stock/Recrutement,
il suffit de saisir cette relation dans l'équation de reproduction et de mettre
la mortalité naturelle du groupe naissant ainsi que le nombre de mois entre
la reproduction et le recrutement à zéro.

Onglet **Migration**
~~~~~~~~~~~~~~~~~~~~

Saisie des migrations, émigrations et immigrations sur les groupes de la
population.

Jusqu'à la version 2.2.2, les migrations, immigrations et émigrations sont
définies comme des coefficients ou des effectifs relatifs à chaque classe.
À partir de la version 2.2.2, il est possible de décrire ces migrations à
l'aide d'équations ayant pour arguments les groupes, les zones de départ et
d'arrivée et les matrices d'effectifs.

Les migrations se réalisent instantanément au début de la saison sélectionnée.
(voir les différents articles sur ISIS-Fish)

On peut le réaliser de deux manières:

#. en saisissant directement les mouvements de populations
#. en saisissant des équations décrivant les mouvements de populations

Onglet **Migration** (Migration de population)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Les migrations se réalisent instantanément au début de la saison sélectionnée.
(voir les différents articles sur ISIS-Fish)

Correspond aux migrations entre les zones de la région.

.. image:: input_17_inputPopulations_tabMigration.png

#. **Sélection de la saison** :
   Liste déroulante contenant l'ensemble des saisons actuellement enregistrées
   pour l'espèce dans la pêcherie. Il faut en sélectionner une pour définir une
   migration.
#. **Sélection du groupe de population à migrer** :
   Liste déroulante contenant l'ensemble des groupes de la population.
   Les migrations sont détaillées par groupe. On sélectionne ici le groupe
   affecté par l'immigration à détailler.
#. **Sélection de la zone de départ** :
   Liste déroulante des zones de départ parmi les différents aires préalablement
   définies. Il faut sélectionner un zone de départ pour la classe sélectionnée.
#. **Coefficient de migration** :
   il s'agit de la proportion du groupe sélectionné affectée par la migration.
#. **Sélection de la zone d'arrivée** :
   Liste déroulante des zones d'arrivé parmi les différents aires préalablement
   définies. Il faut sélectionner une zone d'arrivée pour la classe sélectionnée.
#. **Ajouter la migration** :
   Une fois sélectionnés les paramètres précédemment cités, on appuie ici pour
   créer la migration; les paramètres de la migration apparaissent comme une
   ligne dans la table ci-dessous.
#. **Les immigrations enregistrées** :
   Liste des migrations actuellement renseignées.
#. **Supprimer une migration** :
   Pour supprimer une migration précédemment ajoutée. Attention cette opération
   est irréversible, une fois supprimée la migration ne sera plus récupérable.
#. **Commentaires** :
   Des informations complémentaires peuvent être ajoutées grâce à cette zone
   d'édition. Chaque commentaire est lié à une population, il est ainsi possible
   d'ajouter un commentaire pour chaque population préalablement définie.
#. **Sauver** :
   Sauver les modifications.
#. **Annuler** :
   Annuler les modifications effectuées et revenir à la dernière version
   enregistrée..

Remarque :  Il n'y a pas de test sur les valeurs saisies, faire attention à
saisir un coefficient pour les migrations et les émigrations et un nombre pour
les immigrations. Il est en effet possible de saisir « 1000 » pour un
coefficient de migration sans générer d'erreur.

Onglet **Migration** (Immigration)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Il s'agit de la proportion de poissons migrant d'en dehors de la région vers la région.

.. image:: input_19_inputPopulations_tabImmigration.png

#. **Sélection de la saison** :
   Liste déroulante contenant l'ensemble des saisons actuellement enregistrées
   pour l'espèce dans la pêcherie. Il faut en sélectionner une pour définir une
   immigration.
#. **Sélection du groupe de population à immigrer** :
   Liste déroulante contenant l'ensemble des groupes de la population.
   Les immigrations sont détaillées par groupe. On sélectionne ici le groupe
   affecté par l'immigration à détailler.
#. **Sélection de la zone d'arrivée** :
   Liste déroulante des zones d'arrivée parmi les différents aires préalablement
   définies. Il faut sélectionner un zone d'arrivée pour le groupe sélectionné.
#. **Coefficient d'immigration** :
   il s'agit de la proportion du groupe sélectionné affectée par l'immigration.
#. **Ajouter l'immigration** :
   Une fois sélectionnés les paramètres précédemment cités, on appuie ici pour
   créer l'immigration; les paramètres de l'immigration apparaissent comme une
   ligne dans la table ci-dessous.
#. **Les immigrations enregistrées** :
   Liste des immigrations actuellement renseignées.
#. **Supprimer une immigration** :
   Pour supprimer une immigration précédemment ajoutée. Attention cette opération
   est irréversible, une fois supprimée la migration ne sera plus récupérable.
#. **Commentaires** :
   Des informations complémentaires peuvent être ajoutées grâce à cette zone
   d'édition. Chaque commentaire est lié à une population, il est ainsi possible
   d'ajouter un commentaire pour chaque population préalablement définie.
#. **Sauver** :
   Sauver les modifications sur les immigrations.
#. **Annuler** :
   Annuler les modifications effectuées et revenir à la dernière version
   enregistrée.

Onglet **Migration** (Migration)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Il s'agit de la proportion de poissons migrant hors de la région.

.. image:: input_18_inputPopulations_tabEmigration.png

#. **Sélection de la saison** :
   Liste déroulante contenant l'ensemble des saisons actuellement enregistrée
   pour l'espèce dans la pêcherie. Il faut en sélectionner une pour définir une
   émigration.
#. **Sélection du groupe de population à émigrer** :
   Liste déroulante contenant l'ensemble des groupes de la population.
   Les émigrations sont détaillées par groupe. On sélectionne ici le groupe
   affecté par l'émigration à détailler.
#. **Sélection zone de départ** :
   Liste déroulante des zones de départ parmi les différents aires préalablement
   définies. Il faut sélectionner une zone de départ pour la classe sélectionnée.
#. **Coefficient d'émigration** :
   Il s'agit de la proportion de la classe sélectionnée affectée par l'émigration.
#. **Ajouter l'émigration** :
   Une fois sélectionnés les paramètres précédemment cités, on appuie ici pour
   créer l'émigration; les paramètres de l'émigration apparaissent comme une
   ligne dans la table ci-dessous.
#. **Les immigrations enregistrées** :
   Liste des émigrations actuellement renseignées.
#. **Supprimer une émigration** :
   Pour supprimer une émigration précédemment ajoutée. Attention cette opération
   est irréversible, une fois supprimée la migration ne sera plus récupérable.
#. **Commentaires** :
   Des informations complémentaires peuvent être ajoutées grâce à cette zone
   d'édition. Chaque commentaire est lié à une population, il est ainsi possible
   d'ajouter un commentaire pour chaque population préalablement définie.
#. **Sauver** :
   Sauver les modifications sur les émigrations.
#. **Annuler** :
   Annuler les modifications effectuées et revenir à la dernière version
   enregistrée.

Onglet **Migration** (par équation)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: input_20_inputPopulations_tabMigrationEquation.png

#. **Sélection de la saison** :
   Liste déroulante contenant l'ensemble des saisons actuellement enregistrées
   pour l'espèce dans la pêcherie. Il faut en sélectionner une pour définir une
   migration-émigration ou immigration.
#. pour utiliser le mode équation, il faut cocher ici.
#. **Sélection équation de migration** :
   Liste déroulante contenant l'ensemble des équations de migration connues
   dans la pêcherie.
#. **Éditeur équation de migration** :
   Pour éditer l'équation de migration sans passer par l'éditeur d'équations
   externe.
#. **Sélection équation d'émigration** :
   Liste déroulante contenant l'ensemble des équations d'émigration connues
   dans la pêcherie.
#. **Éditeur équation de émigration** :
   Pour éditer l'équation d'émigration sans passer par l'éditeur d'équations
   externe.
#. **Sélection équation d'immigration** :
   Liste déroulante contenant l'ensemble des équations d'immigration connues
   dans la pêcherie.
#. **Éditeur équation d'immigration** :
   Pour éditer l'équation d'immigration sans passer par l'éditeur d'équations
   externe.
#. **Sauver comme modèle** pour équation de migration
   pour sauver une équation de type migration dans les scripts d'IsisFish (formules/migration TODO)
   Si la fonction créée peut a priori être réutilisée pour d'autres population
   il peut être utile de la sauver comme modèle en utilisant ce bouton.
#. **Ouvrir l'éditeur** pour équation de migration :
   pour ouvrir l'éditeur d'équation de migration (voir figure inputEquation_XX_migration).
#. **Sauver comme modèle** pour équation d'émigration :
   pour sauver une équation de type émigration dans les scripts d'IsisFish (formules/emigration TODO)
   Si la fonction créée peut a priori être réutilisée pour d'autres population
   il peut être utile de la sauver comme modèle en utilisant ce bouton.
#. **Ouvrir l'éditeur** pour équation d'émigration :
   pour ouvrir l'éditeur d'équation d'émigration (voir figure inputEquation_XX_emigration).
#. **Sauver comme modèle** pour équation de immigration :
   pour sauver une équation de type immigration dans les scripts d'IsisFish (formules/immigration TODO)
   Si la fonction créée peut a priori être réutilisée pour d'autres population
   il peut être utile de la sauver comme modèle en utilisant ce bouton.
#. **Ouvrir l'éditeur** pour équation de immigration :
   pour ouvrir l'éditeur d'équation de migration (voir figure inputEquation_XX_immigration).
#. **Commentaires** :
   Des informations complémentaires peuvent être ajoutées grâce à cette zone
   d'édition. Chaque commentaire est lié à une population, il est ainsi possible
   d'ajouter un commentaire pour chaque population préalablement définie.
#. **Sauver**
   Sauver les modifications sur les équations.
#. **Annuler** :
   Pour annuler les modifications effectuées et revenir à la dernière version enregistrée.


7. **Saisie des engins**
========================

La saisie d'un engin se compose de deux onglets:

#. **Engin** : pour renseigner les caratéristiques générales de l'engin.
#. **Sélectivité** : pour renseigner les caractéristisques de sélectivité de
   l'engin (assocation equation de sélectivite - pouplation).

Onglet **Engin**
~~~~~~~~~~~~~~~~

.. image:: input_21_inputEngins.png

#. **Nouvel engin** :
   On commence toujours par créer l'engin, avant de renseigner ces caractéristiques.
#. **Nom de l'engin** :
   le nom de l'engin (il faut utiliser de préférence un nom standardisé comme
   pour tous les objets de la pêcherie).
#. **Unité d'effort** :
   pour renseigner l'unité dans laquelle est mesurée l'effort.
#. **Facteur de standardisation** :
   facteur permettant de standardiser l'effort de pêche entre les différents
   engins (réel positif).
#. **Paramètre technique** : le nom du paramère technique se référent à l'engin (maille, nombre d'hameçons...) qui pourra intervenir dans l'équation de selectivité ou être modifié par une règle de gestion
#. **Type de valeur** :
   pour définir le type des valeurs de l'intervalle renseigné ci-dessous.
#. **Intervalle de valeurs** :
   Valeurs possibles du paramètres est l'intervalle des valeurs pouvant être
   prises par le paramètre technique. Cela peut se présenter sous la forme
   d'une liste de valeurs (pour les paramètres discrets (séparés par des ";")) ou d'un intervalle (pour les paramètres continus (séparés par un "-")). Ces valeurs sont renseignées à titre informatif et non utilisées dans le moteur de simulation.
   Ex:
   80-100
   ou 10;30;50
   ou petit;moyen;grand
#. **Commentaires** :
   Pour permettre de saisir des commentaires propres à cet engin. Ils seront
   sauvegarder en base lors de la sauvegarde de l'engin.
#. **Sauver** :
   Sauver les modifications apportés sur l'engin.
#. **Annuler** :
   Annuler les modifications effectuées depuis la dernière suavegarde.
#. **Supprimer** :
   Pour supprimer un engin de la pêcherie. Attention cette opération est
   irréversible, une fois supprimé, l'engin n'est pas récupérable.
#. **engins dans la navigation** :
   positionnement de l'engin dans les objets de la pêcherie.
#. **Continuer** vers les métiers  :
   Une fois le paramétrage de l'engin terminé, continuer vers la prochaine étape
   de la configuration de la pêcherie : les métiers.

Onglet **Sélectivité**
~~~~~~~~~~~~~~~~~~~~~~

L'équation de sélectivité définie pour chaque espèce pouvant être capturée
par l'engin.

La sélectivité doit être saisie pour chaque population susceptible d'être
capturée par l'engin.

.. image:: input_22_inputEnginsSelectivity.png

#. **Sélection de la population** :
   Dans cette liste déroulante, sont représentées les populations connues de la
   pêcherie.
#. **Sélection de l'équation de sélectivité** :
   Dans cette liste déroulante, on retrouve l'ensemble des équation de
   sélectivités connues dans la pêcherie. Quand vous sauvegarderez l'engin,
   l'équation utilisée sera ajoutée ici (s'il s'agit d'une nouvelle équation).
   Si l'équation de sélectivité à appliquer à la population a déjà été saisie
   pour une autre population, la sélectionner parmi les équation existantes.
#. **Éditeur de l'équation de sélectivité** :
   Pour éditer dans la fenêtre de configuration de l'engin l'équation de
   sélectivité sans ouvrir l'éditeur d'équation.
#. **Sauver comme modèle** :
   permet de sauvegarder une équation comme modèle, qui peut être ensuite
   réutilisée ailleurs.
#. **Ouvrir dans l'éditeur** d'équation de sélectivité :
   Pour éditer dans l'éditeur d'équation l'équation de sélectivité.(TODO figure)
#. **Ajouter** :
   Permet d'ajouter une association équation de sélectivité - population.
#. **Liste ajoutés** :
   On retrouve ici l'ensemble des associations équations de sélectivité - population
   ajoutés dans la pêcherie pour cet engin.
#. **Supprimer** :
   Pour supprimer une association équation de sélectivité- population
   précédemment ajoutée à l'engin.
#. **Sauver** :
   Sauver les modifications.
#. **Annuler** :
   Annuler les modifications effectuées.
#. **Engins dans la navigation** :
   Position de l'engin dans l'arborescence de navigation des objets de la pêcherie.
#. **Continuer** vers les métiers
   Une fois le paramétrage de l'engin terminé, continuer vers la prochaine étape
   de la configuration de la pêcherie : les métiers.

8. **Saisie des métiers**
=========================

Trois différents onglets sont disponibles afin de décrire les métiers:

#. **Métier** : où les caractéristiques du métier sont entrées.
#. **Saison/Zones** : où sont spécifiées les saisons et zones
   correspondant à la description du métier.
#. **Espèces capturables** : où sont décrites les espèces capturées.

Onglet **Métier**
~~~~~~~~~~~~~~~~~

.. image:: input_23_inputMetiers.png

#. **Nouveau** :
   On commence toujours par ajouter un métier avant de renseigner ses caractéristiques.
   Une fois ajouté, le métier apparaît dans la zone de navigation des objets de
   la pêcherie.
#. **Nom du métier** :
   Le nom donné pour ce métier (comme pour tous les objets, il faut que ce soit
   un nom normé, puisque sauvé en base).
#. **Engin** :
   Dans cette liste déroulante, on retrouve l'ensemble des engins connus de la
   pêcherie. Il faut en sélectionner un car un métier est lié à un engin.
#. **Valeurs** :
   La valeur du paramètre contrôlable est la valeur du paramètre technique
   utilisée par le métier. Cette valeur doit être sélectionnée parmi les valeurs
   possibles du paramètre.
#. **Commentaires** :
   Des informations complémentaires peuvent être ajoutées grâce cette zone.
   Chaque commentaire est lié à un métier, il est ainsi possible d'ajouter un
   commentaire pour chaque métier préalablement défini.
#. **Sauver** :
   Pour sauvegarder ce métier en base.
#. **Annuler** :
   Pour annuler toutes les modifications et revenir à la dernière sauvegarde.
#. **Supprimer** :
   Permet de supprimer un métier de la pêcherie. Attention cette opération est
   irréversible, une fois le métier supprimé, il ne sera plus récupérable.
#. **Métier** dans la zone de navigation :
   Positionnement du métier dans la navigation des objets de la pêcherie.
#. **Continuer** vers les types de trajets :
   Une fois terminée la configuration des métiers, on passe à la prochaine étape
   de la saisie, à savoir la configuration des types de trajets (ou marées).


Onglet **Saison/zones**
~~~~~~~~~~~~~~~~~~~~~~~

.. image:: input_24_inputMetiersSeasonZone.png

#. **Nouveau** :
   Toujours commencer par ajouter une nouvelle association saison-zone. Une fois
   celle-ci ajoutée, une nouvelle entrée est ajouté dans la liste de sélection
   des saisons.
#. **Sélectionnez une saison** :
   Dans cette liste déroulante sont représentées les associations saisons-zones
   ajoutées pour ce métier.
#. **Saison** :
   Définir un ensemble de mois durant lesquels le métier est pratiqué en utilisant
   le curseur et les cellules jaunes/bleue (jaune: le mois est inclus dans la
   saison, bleu: il ne l'est pas).
#. **Zones sélectionnées** :
   Dans cette liste, on retrouve l'ensemble des zones de la pêcherie.
   Sélectionner les zones dans lesquelles le métier est pratiqué pendant la
   saison définie parmi les zones prédéfinies.
#. **Commentaires** :
   Des informations complémentaires peuvent être ajoutées grâce à cette zone.
   Chaque commentaire est lié à une saison, il est ainsi possible d'ajouter un
   commentaire pour chaque saison préalablement définie.
#. **Sauver** :
   Pour sauvegarder en base, les modifications apportées à l'écran.
#. **Annuler** :
   Pour annuler les modifications depuis la dernière sauvegarde.
#. **Supprimer** :
   Permet de supprimer une association Saison-Zone pour le métier en cours de modification.
   Attention, cette opération est irréversible, une fois supprimée, l'association
   n'est plus récupérable.
#. **métier** dans la zone de navigation :
   Positionnement du métier dans la navigation des objets de la pêcherie.
#. **Continuer** vers les types de trajets :
   Une fois terminée la configuration des métiers, on passe à la prochaine étape
   de la saisie, à savoir configuration des types de trajets (ou marées).

Onglet **Espèces capturables**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Le **Facteur de ciblage** quantifie la manière avec laquelle un métier cible
la population. Cela comprend la puissance de pêche, le calibrage des engins,
etc. (voir les différents articles sur ISIS-Fish).
Pour une population donnée, il peut être vu comme le ratio des captures entre
deux métiers pêchant au même endroit, au même moment avec le même engin.

.. image:: input_25_inputMetierCapturability.png

#. **Sélectionnez une saison** :
   Dans cette liste déroulante apparaît l'ensemble des saisons définies dans
   l'onglet précédent. Il faut sélectionner une.
#. **Choix de l'espèce** :
   Dans cette liste déroulante, on retrouve l'ensemble des espèces de population
   connues parmi les populations du cas d'étude..
#. **Facteur de ciblage**
   Dans cette liste déroulante, on retrouve l'ensemble des facteurs cibles.
   Il faut en sélectionner un.
#. **Éditeur équation Facteur **
   Pour éditer une équation de **facteur cible** sans utiliser l'éditeur externe
   d'équation.
#. **Sauver comme modèle**
   Permet de sauvegarder l'équation saisie dans la zone précédente en tant que
   modèle et la rendre ainsi réutilisable.
#. **Ouvrir l'éditeur**
   Permet d'ouvrir l'éditeur externe d'équation de facteur cible.
#. **Espèces principales pour le métier**
   Permet de spécifier si l'espèce actuellement sélectionnée est une capture
   principale pour ce métier.
#. **Ajouter** :
   Une fois sélectionnée la saison, l'espèce et le facteur cible, on appuie sur
   ce bouton afin d'ajouter l'association. Elle sera alors ajoutée dans la table
   juste en dessous.
#. **Espèces ajoutées** :
   On retrouve ici l'ensemble des associations ajoutées pour ce métier.

   Tip : en cliquant sur la cellule facteur cible d'une des entrées l'éditeur
   d'équations externe s'ouvre avec l'équation correspondante.
#. **Sauver** :
   Pour sauvegarder en base, les modifications apportés à l'écran.
#. **Annuler** :
   Pour annuler les modifications depuis la dernière sauvegarde.
#. **Supprimer** :
   Permet de supprimer une association Saison-Zone pour le métier en cours.
   Attention, cette opération est irréversible, une fois supprimée, l'association
   n'est plus récupérable.
#. **Continuer** vers les types de trajets :
   Une fois terminée la configuration des métiers, on passe à la prochaine étape
   de la saisie, à savoir configuration des types de trajets (ou marées).

Remarque : Il n'y a pas de test sur le facteur de ciblage, il est possible
d'ajouter plusieurs facteurs de ciblage pour le même métier et la même espèce
pour la même période sans générer d'erreur.


9. **Saisie des marées**
========================

.. image:: input_26_inputTripType.png

#. **Nouveau** type de trajet :
   On commence toujours par ajouter un nouveau type de trajet avant d'en modifier
   ses caractéristiques. Une fois le type de trajet créé, il apparaît dans la
   zone de navigation des objets de la pêcherie avec un nom initialement générique.
#. **Nom** du type de trajet :
   Pour modifier le nom d'un type de trajet
#. **Durée** du trajet :
   Pour indiquer le nombre d'heures en mer par marée.
#. **Temps minimal entre deux voyages** :
   Pour indiquer la durée minimale entre deux voyages pour ce type de trajet (en jours).
#. **Commentaires** :
   Des informations complémentaires peuvent être ajoutées grâce à cette zone.
   Chaque commentaire est lié à un type de marée, il est ainsi possible d'ajouter
   un commentaire pour chaque type de marée préalablement défini.
#. **Sauver** :
   Pour sauvegarder en base les caractéristiques de ce type de trajet.
#. **Annuler** :
   Permet d'annuler les modifications depuis la dernière sauvegarde du type de
   trajet.
#. **Supprimer** :
   Pour supprimer ce type de trajet de la pêcherie. Attention cette opération
   est irréversible, une fois un type de trajet supprimé, il ne sera plus
   récupérable.
#. **Continuer** vers les types de navires
   Une fois terminées les modifications sur les types de trajets de la pêcherie,
   on passe à l'étape suivante de la configuration : les types de navires.

10. **Saisie des types de navires**
===================================

.. image:: input_27_inputVesselTypes.png

#. **Nom** du type de navire :
   Pour indiquer le nom du type de navire. Ce nom doit être normalisé comme le
   nom de tous les objets de la pêcherie.
#. **Longueur** :
   Longueur moyenne (en m) d'un bateau appartenant à ce type de bateau.
#. **Vitese** :
   Permet de définir la vitesse moyenne de ce type de navire (en km.h-1).
#. **Duréee maximale du trajet** :
   Pour définir la durée maximale du trajet en jours.
#. **Intervalle d'activité**:
   Pour définir le temps d'activité d'un bateau TODO Revoir
#. **Taille minimale d'équipage** :
   Pour définir la taille minimale de l'équipage d'un navire de ce type (en nombre de personnes).
#. **Coût d'un trajet en fuel** :
   Permet de définir le coût en fuel d'un trajet pour un navire de ce type.
#. **Type de trajet** :
   Dans cette liste apparaissent tous les types de trajets connus dans la pêcherie.
   Un type de trajet doit être sélectionné.
#. **Commentaires** :
   Des informations complémentaires peuvent être ajoutées grâce à cette zone.
   Chaque commentaire est lié à un type de bateau, il est ainsi possible d'ajouter
   un commentaire pour chaque type de bateau préalablement défini.
#. **Sauver** :
   Pour sauver le type de navire.
#. **Nouveau** :
   Permet de construire un nouveau type de navire. C'est l'action à réaliser
   en premier avant toute modification des caractéristiques de ce type de navire.
   Une fois crée, le type de navire apparaît dans la zone de navigation des
   objets de la pêcherie.
#. **Annuler** :
   Pour annuler les modifications effectuées et revenir à la dernière version
   sauvegardée.
#. **Supprimer** :
   Pour supprimer le type de navire de la base. Attention, cette opération est
   irréversible et une fois supprimé, le type de navire n'est plus récupérable.
#. **Continuer** vers les ensembles de navires :
   Une fois terminées les modifications sur les types de navires de la pêcherie,
   on passe à l'étape suivante de la configuration : les flottilles.

11. **Saisie des flottilles**
=============================

Trois onglets permettent de décrire les flottilles:

#. **Caractéristiques** qui permet de décrire les caractéristiques de la flottille
#. **Métiers pratiqués** qui permet de détailler les différents métiers possibles.
#. **Paramètres des métiers** qui permet de détailler les coûts associés à chaque métier pratiqué.

Onglet **Caractéristiques**
~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: input_28_inputVessels.png

#. **Nouveau** :
   On commence toujours par créer une flottille avant d'en modifier les
   caractéristiques. Une fois crée, la flottille apparaît dans la zone de
   navigation des objets de la pêcherie avec un nom générique.
#. **Nom** :
   Pour modifier le nom de la flottille.
#. **Port** d'attache :
   Dans cette liste déroulante apparaissent les différents ports connus dans la
   pêcherie. Il faut sélectionner un port d'attache par flottille.
#. **Type de navire** :
   Dans cette liste déroulante apparaissent les différents type de navires
   connus dans la pêcherie. Il faut en sélectionner un pour définir le type de
   navires dont est composée le flottille.
#. **Nombre de navires** :
   Pour indiquer le nombre de navire dont est composé la flottille.
#. **Coûts fixe** :
   Pour renseigner les **coûts fixes** imputés à chaque propriétaire d'un
   bateau de la flottille en euros par an.
#. **Commentaires** :
   Des informations complémentaires peuvent être ajoutées grâce à cette zone.
   Chaque commentaire est lié à une flottille, il est ainsi possible d'ajouter
   un commentaire pour chaque flottille préalablement définie.
#. **Sauver** :
   Pour sauvegarder les modifications effectuées sur la flottille.
#. **Annuler** :
   Permet d'annuler les modifications effectuées depuis la dernière sauvegarde.
#. **Supprimer** :
   Pour supprimer la flottille de la pêcherie. Attention cette opération est
   irréversible, une fois supprimée la flottille ne sera pus récupérable.
#. **flottilles** dans la navigation :
   Position de la flottille dans la zone de navigation des objet de la pêcherie.
#. **continuer** vers les stratégies :
   Une fois terminées les modifications sur les flottilles de la pêcherie, on
   passe à la dernière étape de la configuration : les stratégies.

Onglet **Métiers pratiqués**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: input_29_inputVesselsEffortDescription.png

#. **Sélection du métier** :
   Dans cette liste apparaissent la liste des métiers connues dans la pêcherie.
   Il faut en sélectionner en pour l'ajouter à la flottille et le paramétrer.
#. **Ajouter** :
   On ajoute les différents métiers susceptibles d'être opérés lors d'une marée
   en sélectionnant le métier dans la liste des différents métier.
#. **Métiers pratiqués** :
   Liste des métiers pratiqués par la flottille.
#. **Supprimer** :
   Pour supprimer un métier de la pêcherie. Attention cette opération est
   irréversible, une fois supprimé, le métier n'est plus récupérable.
#. **Sauver** :
   Pour sauvegarder les modifications effectuées sur l'engin.
#. **Annuler** :
   Pour annuler les modifications effectuées depuis la dernière sauvegarde.
#. **Continuer** vers les stratégies :
   Une fois terminées les modifications sur les flottilles de la pêcherie,
   on passe à la dernière étape de la configuration : les stratégies.

Onglet **Paramètres des métiers**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: input_29_inputVesselsEffortDescriptionParameters.png

#. **Métiers** :
   Sélectionnez un des métiers pratiqués par la flottille pour entrer ses paramètres.
#. **Opération de pêche** :
   Pour définir le nombre d'opérations de pêche par jour pour ce métier et
   cette flottille.
#. **Durée de la pêche** :
   Pour définir la durée d'une opération de pêche en heure pour ce métier et
   cette flottille.
#. **Nombre d'engins par opération** :
   Pour indiquer le nombre d'engins à utiliser par opération de pêche pour ce
   métier et cette flottille.
#. **Taille de l'équipage** :
   Pour indiquer la taille de l'équipage d'une pêche pour ce métier et cette flottille.
#. **Coût unitaire de la pêche** :
   Pour indiquer le coût unitaire de la pêche, à savoir le carburant, l'huile
   et glace pour un bateau de cette flottille pratiquant ce métier (en euros par opération de pêche).
#. **Salaire d'équipage fixe** :
   Pour indiquer la part fixe du salaire de l'équipage de cette flottille
   pratiquant ce métier (en euros par mois).
#. **Coût de l'alimentation pour l'équipage** :
   Pour indiquer le coût de l'alimentation pour l'équipage pour un bateau de
   cette flottille pratiquant ce métier.
#. **Taux de partage de l'équipage** :
   Pour indiquer la part variable du salaire de l'équipage pour un bateau de
   cette flottille pratiquant ce métier.
#. **Coût de maintenance de l'engin** :
   Pour renseigner le coût de réparation et de maintenance des engins de pêche
   par jour pour un bateau de cette flottille pratiquant ce métier (en euros
   par jour).
#. **Coût de revient à terre** :
   Pour renseigner le coût de débarquement (taux) lié au métier et au port
   de la stratégie.
#. **Autres coûts** :
   Pour indiquer les autres coûts d'exploitation de cette flottille pratiquant
   ce métier (en euros par heure).

Remarque : Tous les paramètres économiques n'ont pas besoin d'être renseignés
afin de faire tourner une simulation, il est possible de faire tourner une
simulation (avec uniquement la partie biologique du modèle), en laissant les
coûts à zéro (valeurs par défaut).

12. **Saisie des stratégies**
=============================

Deux onglets permettent de décrire les stratégies:

#. **cararctéristiques** de la stratégie
#. **description des informations mensuelles**

Onglet **Caractéristiques**
~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: input_30_inputStrategies.png

#. **Nouveau** :
   On commence toujours par crée une stratégie avant d'en modifier les
   caractéristiques. Une fois créée, la stratégie apparaît dans la zone de
   navigation des objets de la pêcherie avec un nom générique.
#. **Nom de la stratégie** :
   Pour indiquer le nom de la stratégie, comme les autres noms d'objets dans
   la pêcherie, il est préférable que le nom soit normalisé.
#. **Sélection de la flottille** :
   Dans cette liste déroulante, on affiche l'ensemble des flottilles connues pour
   la pêcherie. Il faut en sélectionner une pour l'associer à la stratégie
   courante.
#. **Proportion de la flottille** :
   Pour renseigner la proportion du nombre de bateaux de la flottille qui suivent
   cette stratégie.
#. **Commentaires** :
   Des informations complémentaires peuvent être ajoutées grâce à cette zone.
   Chaque commentaire est lié à une stratégie, il est ainsi possible d'ajouter
   un commentaire pour chaque stratégie préalablement définie.
#. **Sauver** :
   Pour sauvegarder les modifications effectuées sur la stratégie.
#. **Annuler** :
   Pour annuler les modifications effectuées depuis la dernière sauvegarde.
#. **Supprimer** :
   Pour supprimer une stratégie de la pêcherie. Attention cette opération est
   irréversible, une fois supprimée, la stratégie n'est plus récupérable.
#. **Stratégies** dans la navigation :
   Position de la stratégie dans la zone de navigation des objets de la pêcherie.


Onglet **Saisie des mois**
~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: input_31_inputStrategiesMonthInfo.png

Cet écran permet de décrire par mois la stratégie. On retrouve douze zones pour
configurer chaque mois. On décrit ici la configuration d'un mois.

#. **Sélection du type de trajet** :
   Dans cette liste déroulante sont affichés les types de trajet connus dans la
   pêcherie. Il faut en sélectionner un afin de pouvoir configurer pour
   le métier associé à l'engin pour ce mois. Une fois sélectionné, on voit
   s'afficher juste en dessous le nombre de trajets possibles calculé pour ce mois.
#. **Nombre de jours d'inactivités** :
   Une fois un type de trajet sélectionné, on peut renseigner ici le nombre
   minimum de jours d'inactivité,  correspondant au nombre minimal de jours
   d'inactivité dans le mois pour un bateau pratiquant cette stratégie le mois
   donné.
#. **Proportion** :
   Pour renseigner la proportion du temps passé par les bateaux de la stratégie
   pour un métier et un mois donné. (réel entre 0 et 1).
#. **Sauver** :
   Pour sauvegarder les modifications effectués sur cet écran.
#. **Annuler** :
   Pour annuler les modifications effectués sur cet écran.
