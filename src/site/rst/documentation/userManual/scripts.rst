.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
=======================
Gestionnaire de scripts
=======================

Le but de ce module est la gestion des différents scripts utilisés dans
IsisFish.

On peut y créer, éditer, supprimer des scripts en local.

On peut aussi communiquer avec un serveur de scripts pour récupérer de
nouveau scripts, ou d'en soumettre,...

Il faut pour toutes ces opérations avoir une configuration correcte concernant
le serveur de script (voir configuration vcs).

Il est aussi possible ici de compiler et tester des scripts.

description des zones de la fenetre
-----------------------------------

.. image:: script_00.png
   :align: center
   :scale: 50


#. le menu regroupe tous les actions possibles.
#. la liste de tous les types de scripts connus par IsisFish.
#. la barre d'actions sur le(s) script(s) sélectionné(s).
   Ce sont des raccourcis des actions les plus courantes des menus.
#. l'arborescence des scripts trouvés localement par IsisFish. Les types de
   scripts sont représentés par des noeuds, et les scripts sont des feuilles de
   l'arbre.
#. la zone d'édition d'un script sélectionné. Pour éditer un script, il suffit
   de le sélectionner dans la zone de navigation de script (4).
#. la zone de message  des différentes opérations (évaluation, diff,...)
   effectuées sur les scripts.
#. la zone classique de status.

Les différentes actions du gestionnaire
---------------------------------------

On détaille dans cette section, l'ensemble des actions réalisables par menu.

menu **fichier**
================

.. image:: script_01_menuFile.png
   :align: center
   :scale: 75


1. **nouveau** script
~~~~~~~~~~~~~~~~~~~~~

Cette action est accessible de deux manières :

a. Soit par le sous-menu **nouveau**, en choisissisant un type de script à créer
b. Soit par le boutton à droite de la barre d'actions, après avoir sélectionné
   dans la liste des types de scripts à droite du boutton.

Ensuite l'application vous demande le nom du nouveau script à créer.

.. important:: Le nom du script doit toujours commencer par une majuscule, et ne
               doit contenir que des caractères alphanumérique (ou le caractère
               '_').

Pour l'instant, il n'est pas possible de définir des sous-types de scripts (sauf
pour les formules).


Pour les formules il suffit de préfixer lors de la création son nom par le nom
du sous-type suivi d'un '/'.


Après avoir rentré un nom de script valide, le script est crée localement, et il
apparait à sa bonne place dans la zone de naivgation de scripts. Il est
sélectionné dans la zone de navigation (1) et son contenu apparait dans
l'éditeur (2).


2. **Sauver** un script
~~~~~~~~~~~~~~~~~~~~~~~

sauvegarder localement les modification apportées sur un script.

Cette action n'est disponible que lorsqu'un script est sélectionné dans
l'éditeur. On la retrouve aussi dans la barre d'action.


3. **Importer** des scripts
~~~~~~~~~~~~~~~~~~~~~~~~~~~

permet d'importer des scripts à partir d'une archive de scripts préalablement
exportée par IsisFish.

L'application demande un fichier d'import de scripts.

Ensuite un dialogue de confirmation apparaît

#. On retrouve ici la liste des scripts trouvés dans le fichier qui sont déjà
   présents localement.
#. Vous pouvez sélectionner ou deselectionner les scripts que vous voulez
   écraser localement.
#. En bas à droite, on peut effectuer cette opération sur l'ensemble des scripts
   de la liste.
#. indique le chemin du fichier d'import.
#. pour lancer l'import (seront importer tous les fichiers non présents
   localement, plus ceux dont vous avez confirmé l'écrasement).
#. pour ne pas effectuer l'import.

.. image:: script_05_importConfirm.png

4. **Exporter** des scripts
~~~~~~~~~~~~~~~~~~~~~~~~~~~

exporter des scripts qui pourront ensuite être importer dans une autre instance
d'IsisFish. Cette action n'est disponible que lorsqu'au moins un script est
sélectionné. Pour exporter tous les scripts d'un type, il suffit de
sélectionner le noeud du type de script dans l'arborescence des script.

L'application demande un répertoire où stocker le fichier d'export.

Ensuite un dialogue de confirmation apparaît

#. On retrouve ici la liste des scripts à exporter, il est de sélectionner ou
   déselectionner des scripts à exports grâce aux cases à cocher  sur la gauche.
#. En bas à droite, on peut effectuer cette opération sur l'ensemble des scripts
   de la liste.
#. le chemin du fichier d'export, par défaut le nom d'export suit le pattern
   suivant : exportScript-YYYY-MM-DD-mm-ss.zip.
#. Vous pouvez changer le répertoire du ficher d'export (le nom sera appliqué
   sur le nouveau répertoire choisi).
#. pour lancer l'export
#. pour ne pas effectuer l'export.

.. image:: script_06_exportConfirm.png

5. **Supprimer localement** un script
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Permet de supprimer des scripts localement, sans les supprimer sur le serveur
distant.

.. danger::  A utiliser avec précaution!


6. **Fermer** le gestionnaire
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

fermer le gestionnaire de scripts.

menu **Edit**
=============

Dans ce menu on retrouve les actions classiques de manipulation de texte dans
l'éditeur de script (4).

Elles ne sont utilisables que lorsqu'un script est sélectionné et présent dans
l'éditeur de script.

Les Toutes ces actions sont aussi disponibles depuis la barre d'action, à
savoir :

#. **couper** : couper un bout de code depuis l'éditeur de script.
#. **copier** : copier un bout de code dans l'éditeur de script.
#. **coller** : coller un bout de code dans l'éditeur de script à partir du
   presse-papier.

.. image:: script_07_menuEdit.png

menu **Serveur**
================

Ce menu regroupe les fonctionnalités de communication avec le serveur de script.

Pour toutes ces actions, vous devez avoir sélectionné un script au préalable.

.. image:: script_08_menuServer.png

1. **commiter** un script sur le serveur distant
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

permet d'envoyer au serveur distant une nouvelle version d'un script à
sauvegarder.

Un message de commit vous sera demandé dans une boite de dialogue.


Vous avez les droits d'écriture sur ce serveur (voir chapitre configuration
vcs). On retrouve aussi cette action dans la barre d'actions.

2. **voir les différences** avec la dernière version du serveur
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

permet d'afficher dans la zone (5) les différences entre votre version locale
d'un script et la dernière version enregistrée sur le serveur distant.

3. **mettre à jour** un serveur depuis le serveur
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

permet de récupérer la dernière version à partir du serveur distant.

Pour exécuter cette action vous ne devez pas avoir modifié localement le script
(dans ce cas, il faudra d'abord revenir à la version précédente voir operation
de synchronisation au serveur). Cette opération est aussi présente dans la barre
d'actions.

4. **supprimer** localement et sur le serveur distant
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

permet de supprimer un script localement mais aussi sur le serveur distant.


Vous devez disposer de droits d'écriture sur le serveur distant.

.. DANGER::  A utiliser avec précaution!

menu **Code**
=============

.. image:: script_09_menuCode.png

Ce menu regroupe les opérations de code possibles sur un script sélectionné.

Les actions suivantes necessitent donc d'avoir au préalable sélectionné un
script dans la zone de navigation.

Toutes ces actions sont toutes présentes dans le barre d'actions.

1. **Vérifier** du code
~~~~~~~~~~~~~~~~~~~~~~~

Lancement une opération de compilation du script sélectionné.

Suite à l'opération, on voit apparaître dans la zone de notification (5) les
traces du compilateur.


Il est à noter que des messages d'informations peuvent apparaître à une
vérification, même si la compilation s'est bien déroulée.


2. **Evaluer** du code
~~~~~~~~~~~~~~~~~~~~~~

Permet d'évaluer un script possédant une méthode main.

Une opération de compilation est lancée si nécessaire au préalable.

Cette opération n'est possible que  si un script est sélectionné dans l'éditeur
et que le script contient une méthode plublique static sans retour nommé
**main** et ayant pour paramètre un tableau de String.


autres figures
==============

.. image:: script_02_menuFile_new.png

pour créer un script depuis le sous-menu **nouveau** du menu **fichier**

.. image:: script_03_combo_new.png

pour créer un script depuis la liste déroulante de la barre d'action

.. image:: script_04_new.png

après la création d'une règle **ReturnZero**

.. image:: script_10_verifSyntax_ok.png

après une vérification avec succès mais avec un message d'avertissement du
compilateur

.. image:: script_11_verifSyntax_ko.png

après une vérification qui a échoué
