.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
===================
Rendu des résultats
===================

La fenêtre de rendu des résultats permet de visualiser les résultats
en graph, en carte, ou en nombre. Elle permet aussi de supprimer
d'ancienne simulation, de viusaliser les logs d'une simulation.


Description des actions de la fenetre
-------------------------------------

.. image:: result_00.png

#. le menu (pour le moment uniquement l'action pour quitter la fenêtre).
#. la liste déroulante des simulations présentes en local.
#. la bouton pour permettre de filter les simulations (voir TODO ).
   Il s'agit de la même interface que dans le lanceur de simulation.
#. Pour ouvrir dans la zone centrale les résultats de la simulation.
   Il faut avant tout avoir sélectionner une simulation.
#. Pour supprimer une simulation sélectionnée.
#. Pour visualiser les logs de la simulation sélectionnée.
#. la zone centrale où apparaissent les résultats d'une simulation sélectionnée.

Description de la vue résumé
----------------------------

.. image:: result_01.png

#. **choix des résultats à afficher** : en sélectionnant l'un des radio boutons
   on change les données de la zone centrale et sur la gauche.
   Par défaut, on arrive sur la page de résumé comme indiqué dans le figure
   ci-dessus.
#. **liste déroulante des matrices à afficher**. TODO à revoir.
#. **liste des exports** ce menu permet de sauvegarder un des exports de la
   simulation; on sélectionnant un des export, une boite de dialogue apparaît
   pour vous demander un chemin où sauvegarder le fichier.
#. **liste des données de la dimension temps**, on sélectionner les tranches
   qui nous interessent.
#. **liste des données de la dimension 1**, on sélectionner les plages qui
   nous interessent.
#. **liste des données de la dimension 2**, on sélectionner les plages qui
   nous interessent.
#. **Somme par année** Effectue la somme des données par année.
#. **Somme des dates** Effectue la somme sur toutes les dates.
#. **Somme sur la dimension 1** Effectue la somme sur la dimension 1
#. **Somme sur la dimension 2** Effectue la somme sur la dimension 2
#. Une fois tous les dimensions sélectionnées, on appuye ici pour afficher
   les rapports (voir figure suivante).
#. Le résumé de la simulation ouverte (unqiuement visible dans la vue résumé).

Description de la vue graphe
----------------------------

.. image:: result_02.png

#. Après sélection des données à mettre en rapport, et en appuyant sur ce
   bouton, on change de vue.
#. On bascule en mode graphe.
#. dans la zone centrale, sont affichés les résultats sélectionnés.
#. **liste déroulante** des types de graphiques disponibles. On sélectionne et
   les données apparaissent sous une nouvelle forme.
#. On peut dans cet zone, **configurer les graphiques générés**.
#. appuyez ici pour **mettre en application vos modifications**.

Description de la vue carte
---------------------------

.. image:: result_03.png

#. pour passer en mode **carte**
#. la zone centrale contient la carte de la pêcherie, avec les données
   sélectionnées dans les listes de gauche. TODO Comment ça marche?

Description de la vue données
-----------------------------

.. image:: result_04.png

#. pour passer en mode **données**
#. la zone centrale contient la matrice des données sélectionnées.
#. il est possible d'exporter ces données sous la forme d'un fichier au format
   CSV. Un dialogue vous demandera d'indiquer le chemin où sauvegarder le
   fichier.
   
