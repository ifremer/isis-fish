.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
======================
Lanceur de simulations
======================

.. |tip| image:: tip.png

Le but de ce module est de lancer des simulations en utilisant des scripts
d'ISIS-Fish (voir section script) et une region préalablement renseignée (voir
section input).

Introduction
------------

Il contient toutes les interfaces utilisateur permettant la configuration d'une
simulation.

On y retrouve enfin l'interface graphique de la queue de simulation, où l'on
peut lancer des simulations locales.

La fenêtre de lancement de simulation permet de paramètrer les
simulations et de les exécuter.

Cette fenêtre est consituée de plusieurs onglets qui permettent de modifier:

- **Paramètres** de base de la simulation: identifiant, description, région,
  stratégies, populations, règles, nombre d'années
- **Script de présimulation**, pour l'activer il faut coché la
  case *utiliser un script de pré-simulation*. Le script de pré-simulation est du BSH
  exécuter avant toutes les simulations et permet de modifier la base de données.
- **Analyse de sensibilité**, pour l'activer il faut coché la case
  *Utiliser le plan d'analyse*.
- **Export des résultats** qui permet de sélectionner les scripts d'export à utiliser
  et le répertoire dans lequel on souhaite exporter.
- **Choix des résultats** qui permet de sélectionner les résultats à conserver
  pour une visualisation ultérieure via l'interface de rendu des résultats
- **Paramètres avancés** qui permet de changer le simulateur à utiliser, activer ou
  désactiver le cache, activer ou désactiver les statistiques, ajouter des
  paramètres libres récupérable dans les règles de gestion, le simulateur, ...

Si une rêgle de gestion ou un export a besoin d'un résultat qui n'est
pas sélectionné alors ce résultat est automatiquement ajouté a la
liste des résultats souhaités.

Lors du lancement de la simulation, automatiquement la date courante
sera ajouté au nom de la simulation.


Description détaillée de l'interface
------------------------------------

On présente dans cette section, les différents options configurables via
l'interface graphique (par onglet).

Les zones de l'interface
========================

.. image:: simulation_00.png

#. le menu
   La zone des menus du simulateur, (pour le moment juste une action fermer
   dans le menu fichier pour fermer le lanceur de simulations, et l'action A
   propos dans le menu Aide).
#. les différents onglets de configurations, 6 pour la configuration et le
   dernier pour afficher l'interface de surveillance des simulations locales en
   cours.
#. le contenu de chaque onglet.
#. la zone de classique de status.

Onglet **Paramètres**
=====================

.. image:: simulation_01_tabParams.png

il s'agit de l'onglet principal des paramètres de base du simulateur.

#. **Charger une ancienne simulation**
   La liste déroulante contient l'ensemble des simulations existantes localement.
   La liste est vide lors du premier lancement d'ISIS-Fish, puisqu'aucune
   simulation n'existe localement.
   Une fois une simulation exécutée (avec succes), elle sera ajoutée dans cette liste au prochain
   lancement d'ISIS-Fish.
#. **Filtrer** les simulations existantes (nouveau depuis 3.1.0)
   Il est possible de filtrer les simulations existantes localement en cliquant
   sur le boutton filtrer.
   Un dialogue de filtre de simulation apparaît (voir section filtreSimulation)
#. **Réinitialiser** la liste des simulations (nouveau depuis 3.1.0)
   Il est possible en cliquant sur ce boutton de repositionner la liste de toutes
   les simulations existantes localement (en supprimant tout filtre précemment saisi).
#. **Nom de la simulation**
   Pour saisir le nom d'une nouvelle simulation.

   Si vous chargez une ancienne simulation, le nom de celle-ci sera renseignée ici.

   |tip| On peut très facilement créer une nouvelle simulation à partir d'une ancienne en la chargeant et en changeant son nom.
#. **Description**
   Pour saisir la description d'une nouvelle simulation.

   Si vous chargez une ancienne simulation, la description de celle-ci sera renseignée ici.
#. **Region**
   Pour choisir la région associée à la simulation.

   Le chargement de la région renseignera les zones de stratégies et de populations.

   |tip| commencer toujours par charger la région avant de renseigner les règles
   car celles-ci sont liées aux données de la région chargée.
#. **Stratégies**
   Une fois une région chargée, on retrouve ici la liste des stratégies connues de la pêcherie.
   La stratégie utilisé par le simulateur sera celle sélectionnée dans cette liste.
#. **Populations**
   Une fois une région chargée, on retrouve ici la liste des populations connues
   de la pêcherie.

   La sélection d'une population, remplit les données à saisir de cette
   population dans la zone (9).

   La population utilisé par le simulateur sera celle sélectionnée dans cette liste.
#. **Données d'une population**
   Une fois une population sélectionné, apparaît ici la matrice des données à
   saisir sur la population.
#. **Liste des règles disponibles localement**
   Il s'agit de la liste des règles connues par ISIS-Fish (voir section script).

   |tip| en survolant les règles dans la liste, une info bulle apparaît en affichant la description de la règle (voir section script).
#. **Liste des règles ajoutées**
   Contient la liste des règles que vous avez ajoutées.

   La sélection d'une des règles ajoutées, remplit la zone (15) des paramètres
   de configuration de la règle.

   Toutes les règles ajoutées dans cette liste seront utilisées par le simulateur.

   |tip| en survolant les règles dans la liste, une info bulle apparaît en
   affichant la description de la règle (voir section script).
#. **Ajouter une règle**
   Permet d'ajouter la règle sélectionné dans la liste déroulante (10).
   Une fois ajoutée, la règle apparaît dans la zone (11).
#. **Supprimer une règle ajoutée**
   Permet de supprimer la règle actuellement sélectionné dans la liste des règles
   ajoutées (11).
   Cette action n'est possible que si une règle ajoutée est sélectionnée.
#. **Supprimer toutes les règles ajoutées**
   Permet de supprimer tous les règles que vous avez ajoutées.
   Cette action n'est possible que si vous avez au moins ajouté un règle.
#. **Paramètres d'un règle ajoutée**
   Une fois une règle ajoutée sélectionnée dans la liste (11), apparaissent ici
   ses paramètres.

   |tip| en survolant les nom des paramètres, une info bulle apparaît en
   affichant la documentation du paramètre de la règle (voir section script @Doc).
#. **Nombre d'années**
   Pour indiquer le nombre d'années à utiliser par le simulateur.
   Si vous avez chargé une ancienne simulation, son nombre d'années sera
   renseigné ici.
#. **Simuler**
   Pour lancer la simulation en utilisant le mode renseigné dans la liste (21)
#. **Utiliser un script de pré-simulation**
   Permet d'utiliser (ou pas) un script de pré simulation.
   L'onglet 'pré-simulation' est activé ou désactivé par ce controle.

   Pour ajouter un script de pré-simulation, la case à cocher doit être activée,
   on bascule alors dans l'onglet 'Script de pré-simulation' où l'on saisit
   le script à exécuter avant la simulation.

   Pour ne plus utiliser un script de pré-simulation, il faut désactiver la case
   à cocher. L'onglet 'pré-simulation' est alors déactivé.
#. **Utiliser le plan d'analyse**
   Permet d'utiliser (ou pas) un plan d'analyse sur la simulation.
   L'onglet **analyse de sensibilité** est activé ou désactivé par ce controle.

   Pour utiliser un plan d'analyse, la case à cocher doit être activée, on
   bascule alors dans l'onglet **analyse de sensibilité** (qui est alors activé)
   où l'on paramètre l'analyse.

   Pour ne plus utiliser un plan d'analyse, il faut désactiver la case à cocher.
   L'onglet **analyse de sensibilité** est alors désactivé.
#. **Exporter uniquement des simulations**
   Permet d'exporter ??? TODO
#. **Lanceur de simulations**
   Pour choisir le lanceur de simulations qui sera utilisé pour réaliser la
   simulation :

   - dans un sous-processus (par défaut) lance les simulations dans des
     sous-processus. Si plusieures simulations sont lancées an même temps,
     ISIS-Fish utilisera les différents coeurs de la machine utilisée pour
     effectuer plusieures simulations en parallèle. ISIS-Fish lancera une
     simulation par coeur (2 coeurs = 2 simulations en parallèle).
   - sur le serveur CAPARMOR lance les simulations sur le super-calculateur de
     l'IFREMER CAPARMOR. Pour utiliser correctement CAPARMOR, reportez-vous à la
     section Installation de ce manuel.
#. **Sauver les paramètres de la simulation**
   Enregistre les paramètres enregistrés afin de les recharger lors du prochain
   lancement de simulation.

Onglet **Script de pré-simulation**
===================================

.. image:: simulation_03_tabPreSimulScript.png

#. **éditeur du script de pré-simulation**.
#. **Retour aux paramètres** : pour revenir à l'onglet des paramètres de base
   de la simulation une fois le script défini.

Onglet **Plan de simulation**
=============================

Cet onglet contient l'interface de configuration des plans d'analyse à appliquer pendant la simulation.


.. image:: simulation_04_tabAnalyzePlan.png

#. **Liste des plans existants localement**

   Contient la liste de tous les plans d'analyse connus par ISIS-Fish (voir section script).

   |tip| en survolant les plans d'analyse dans la liste, une info bulle apparaît en affichant la description de la règle (voir section script @Doc).
#. **Liste des plans d'analyse ajoutés**
   Contient la liste des plans d'analyse que vous avez ajouté.
   La sélection d'un des plans d'analyse ajoutés, remplit la zone (6) des paramètres de configuration de ce plan d'analyse.
   Toutes les plans d'analyse ajoutés dans cette liste seront utilisés par le simulateur.

   |tip| en survolant les plans d'analyse dans la liste, une info bulle apparaît en affichant la description du plan (voir section script).
#. **Ajouter un plan d'analyse**
   Permet d'ajouter le plan d'analyse sélectionné dans la liste déroulante (1).
   Une fois ajoutée, la règle apparaît dans la zone (2).
#. **Supprimer un plan d'analyse ajouté**
   Permet de supprimer le plan d'analyse précédemment ajouté actuellement
   sélectionné dans la liste des plans d'analyse ajoutés (2).
   Cette action n'est possible que si un plan d'analyse ajouté est sélectionné.
#. **Supprimer toutes les plans d'analyse ajoutés**
   Permet de supprimer tous les plans d'analyse que vous avez précédemment ajoutés.
   Cette action n'est possible que si vous avez au moins ajouté un plan d'analyse.
#. **Paramètres d'un plan d'analyse ajouté**
   Une fois un plan d'analyse ajouté et sélectionné dans la liste (2), apparaissent ici ses paramètres à renseigner.

   |tip| en survolant les nom des paramètres, une info bulle apparaît en affichant la documentation du paramètre (voir section script @Doc)
#. **Retour aux paramètres**
   Une fois, les plans d'analyse configurés, cliquez ici pour retourner à l'onglet **Paramètre** et continuer la configuration de votre simulation.

Onglet **Export des résultats**
===============================
Cet onglet permet de sélectionner les résultats de la simulation que l'on veut exporter.

.. image:: simulation_05_tabExportResult.png

#. **liste des résultats exportables** : on sélectionne parmis la liste des
   résultats connus par ISIS-Fish, ceux que l'on veut exporter une fois la simulation
   terminée.  TODO Voir lien avec résultat choisis.
#. **chemin du répertoire** : chemin du répertoire où exporter les résultats (voir configuration)
#. **...** : pour rechercher un répertoire en local, où l'on veut exporter les
   résultats.
#. **sauvegarder pour prochaine simulation** : pour sauvegarder le choix des
   résultats à exporter ainsi que le répertoire où exporter pour une prochaine simulation.

Onglet **Choix de résultats**
=============================

.. image:: simulation_06_tabResultChoice.png

#. **résultats disponibles** : liste des résultats disponibles pour la simulation.
   On sélectionne dans cette liste, ceux que l'on veut traiter. TODO lien avec les exports
#. **sauvegarder pour prochaine simulation** : pour sauvegarder le choix des
   résultats à traiter pour une prochaine simulation.

Onglet **Paramètres avancés**
=============================

.. image:: simulation_07_tabAdvancedParams.png

#. **configuration du simulateur** :  Cette première zone permet de configurer
   le simulateur à utiliser pour simuler. Elle comprend :

   #. **sélection du simulateur** : une liste déroulante de simulateurs connus par ISIS-Fish.
   #. **simulation statique** : cochez ici pour indiquer une simulation statique, (décoché signifie TODO).
   #. **simulation cache** : cochez ici pour indiquer une simulation cache, (décoché signifie TODO).

#. **paramétrage des niveaux de tracage** : cette zone permet la configuration
   des niveaux de log à utiliser pendant une simulation.
   On distingue troix types de tracage différents:

   #. **tracage du simulateur** qui regroupe les traces propres au simulateur (par défaut niveau **INFO**)
   #. **tracage des scripts** qui regroupe les traces propres aux scripts (par défaut niveau **INFO**)
   #. **tracage des librairies** qui regroupe les traces du reste. (par défaut niveau **ERREUR**)

#. **ajout de paramètres libres** : dans cette zone on peut ajouter de nouveaux
   paramètres libres. Le boutton **ajouter** ajoute dans la liste des paramètres
   ajoutés, le paramètres défini par le nom de son tag et la valeur du tag.
#. **liste des paramètres libres ajoutés** : cette liste contient l'ensemble
   des paramètres libres ajoutés.
#. **supprimer**, pour supprimer un paramètre libre ajouté (il faut
   sélectionner avant un paramètre libre.)
#. **sauver pour la prochaine simulation** : pour sauvegarder la configuration
   du simulateur pour une prochaine simulation. TODO


Filtre de simulation
--------------------

.. image:: simulation_dialog_00_filter.png

TODO

Créer une nouvelle simulation
-----------------------------

TODO
Après cette description détaillé de tout la configuration, un petit tuto pour
réaliser ''en trois clics'' une simulation.
