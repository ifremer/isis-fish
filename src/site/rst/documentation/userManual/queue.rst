.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
===================
Queue de simulation
===================

La queue de simulation permet de visualiser toutes les simulations qui sont en
cours (ainsi que leur progression), celles qui sont terminées et celles qu'il
reste a effectuer.

.. image:: simulation_08_tabQueue.png

#. **simulations en cours** : cette zone contient l'ensemble des simulations
   locales en attente de traitement ou en cours de simulation.
#. **simulations locales terminées** : cette zone contient l'ensemble des
   simulations locales terminées.
#. **lanceur de la queue des simulations** : permet de lancer l'ensemble des
   simulations locales placées dans la queue de simulation et qui ne sont pas
   déjà en cours de traitement.
#. **arrêter une simulation** : permet d'arrêter une simulation en cours
   de traitement. (pour réaliser cette action, une simulation doit être
   en cours de traitement et être sélectionnée dans la zone 1).
#. **voir les logs de la simulation** : permet d'ouvrir la console de log d'une
   simulation **terminée**. (Pour réaliser cette action, il faut sélectionner
   une simulation terminée dans la zone 2).
#. **Redémarrer la simulation** : permet de redémarrer une simulation en échec.
   (Pour réaliser cette action, il faut sélectionner une simulation terminée dans
   la zone 2).
#. **Retirer les simulations terminées** : Vide la liste des simulations
   terminées.

Console de log de simulation
----------------------------

A partir de la version 3.0.22?, on peut afficher dans une console, les traces
retenues lors d'une simulation.

.. image:: simulation_dialog_01_consoleLog.png

#. **filtre sur niveau de log** : on peut ici filtrer sur certains niveaus de log
   en cochant la cache à cocher correspondant au niveau que l'on veut afficher.

   Si plusieurs niveaux sont sélectionnés, alors on affiche toutes lignes de tous
   les niveaux sélectionnés.

   Par défaut, aucun niveau de log n'est sélectionné, ce qui veut dire : afficher
   tous les niveaux de log.
#. **filtre sur mot** : on peut ici saisir un mot sur lequel on veut filtrer.
#. **appliquer le filtre** : permet de lancer une recherche de toutes les lignes
   contenant le mot recherché.

   Ce filtre s'applique en plus du filtre sur niveau de log. C'est à dire que l'on
   va rechercher les lignes contenant le mot souhaité uniquement sur les lignes
   des niveaux de log choisis.
#. **réinitiliser filtre** : permet de réinitialiser les filtres.
   On se retrouve alors avec aucun mot de filtrage et utilisation de tous les
   niveaux de log.
#. **envoyer par courriel** : permet d'ouvrir une nouvelle boite de dialogue pour
   envoyer la simulation par courriel.

Envoyer simulation par courriel
===============================

.. image:: simulation_dialog_02_sendMail.png

#. **envoyer la simulation complête** : cochez cette case pour envoyer dans le
   courriel une pièce-jointe avec l'ensemble de la simulation zippé.

   Si la case n'est pas coché, on envoie uniquement la trace de log zippé.

   Par défaut, cette option est cochée.
#. **adresse du destinataire** : zone où saisir l'adresse du destinataire du
   courriel.
#. **corps du message** : corps du courriel à envoyer.
#. **envoyer** : pour envoyer le courriel.
#. **annuler** : pour annuler l'envoie de courriel et revenir à la console de log.
