.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
Passer de la version 3.1.3 à la version 3.2.0
=============================================

Extrait du mail sur les listes de diffusion.

La nouvelle version d'IsisFish est a present disponible au telechargement.
Il s'agit de la version 3.2.

Cette version initie la branche 3.2 d'IsisFish, tous les utilisateurs
sont invites a utiliser cette nouvelle version. Les versions 3.1 ne
beneficieront plus d'aucun support.


Avant de commencer
------------------
Cette version apporte de nouvelles fonctionnalites et de profonds
changements.
Avant toutes chose, il est conseille d'effectuer une sauvegarde de vos
donnees a partir de la version 3.1.3 d'IsisFish.

Action a effectuer:
 - exportez votre base de donnees de travail pour la V3.1.3
   Dans l'interface de saisie de la pecherie, ouvrez votre region,
   et cliquez dans Fichier > Exporter la region et choisissez
   un repertoire pour  enregistrer votre region
 - fermez maintenant IsisFish-3.1.3
 - renommez le dossier "isis-database-3" en "isis-database-3.1.3"
   (ce dossier est normalement situe dans votre repertoire
    utilisateur, sous windows C:\Documents and settings\<user>\)


Telechargement de la nouvelle version
-------------------------------------
Rendez vous ensuite sur le site http://isis-fish.labs.libre-entreprise.org/.
Dans le menu, dirigez vous ensuite sur "Telechargement".
Et telechargez la derniere version (par exemple isis-fish-3.2.x.y-bin.zip).

Decompressez l'archive que vous venez de telecharger.


Apres avoir telecharge IsisFish
--------------------------------
Lancez la nouvelle version en cliquant sur le fichier "go.bat" present
dans le repertoire d'IsisFish apres decompression.

IsisFish  telecharge un nouveau "isis-database-3" et vous
propose une migration de la base de donnees. Il vous proposera
egalement cette migration pour toutes vos bases de donnees.

Il est obligatoire d'accepter cette migration, sans quoi ces bases ne
fonctionneront pas.

Vous n'avez aucun problemes
~~~~~~~~~~~~~~~~~~~~~~~~~~~
Dans le cas ou vous n'auriez aucun probleme apparent (IsisFish se lance)
verifiez quand meme que le fichier debug.txt ne contient aucune erreur.
S'il en contient, n'hesitez pas a demander de l'aide sur
isis-fish-users@list.isis-fish.org en envoyant votre fichier debug.txt.

Dirigez-vous dans l'interfaces des scripts et verifiez que vos scripts
ne contiennent pas d'erreurs et compilent (en cliquant sur verifier).

Vous avez des problemes
~~~~~~~~~~~~~~~~~~~~~~~
Si vous avez des problemes (isis ne se lance pas, ou rien ne fonctionne).
Envoyez un message a la liste isis-fish-users@list.isis-fish.org en
precisant
le comportement d'IsisFish ou les erreurs rencontrees et accompagnez
votre message du fichier debug.txt.


Apres le premier lancement
--------------------------
Si le premier lancement d'IsisFish s'est bien passe, vous pouvez :
 - reimporter vos bases de donnees (en acceptant la migration demandee
normalement)
 - faire tourner une simulation

Si la simulation n'a pas fonctionne,  vous pouvez demander de l'aide
sur isis-fish-users@list.isis-fish.org en joignant votre fichier debug.txt

Vous pouvez ensuite, recopier vos anciens scripts (plans d'analyses,
regles, export)
depuis le dossier "isis-database-3.1.3" que vous avez sauvegarde vers le
dossier
"isis-database-3". N'oubliez pas ensuite de valider tous vos scripts
dans l'interfaces
dans l'interface de gestion des scripts et de verifier qu'ils compilent
bien.


Informations complementaires
----------------------------
Les informations complementaires concernant IsisFish sont presentes sur
le wiki.

En particulier :
 - le manuel :
http://isis-fish.labs.libre-entreprise.org/wiki-moin/instance/cgi-bin/moin.cgi/v3/usermanual
 - les changements :
http://isis-fish.labs.libre-entreprise.org/wiki-moin/instance/cgi-bin/moin.cgi/v3/news
