.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
Liste des changements apportés à Isis-Fish (Version 3)
======================================================

isis-fish (3.3.0.3) chatellier 20100426
  * Add new option to customize qsub options
  * Update libs (log4j, junit)
  * Fix first age use in group creation wizard

isis-fish (3.3.0.2) chatellier 20100407
  * Fix a NPE when reloading in process simulation
  * Fix a migration problem on region with non null empty map list
  * Fix a problem with failing region saves (was blocking all saving actions)

isis-fish (3.3.0.1) chatellier 20100330
  * Make java path configurable on cluster server (caparmor & noumea)

isis-fish (3.3.0.0) chatellier 20100224
  * Replace zone selection event from mouse click to correct valueChanged
  * Fix a NullPointerException during region check
  * Disable all list filtering functionalities (don't work anymore with a lot of simulations)

isis-fish (3.3.0.0-rc1) chatellier 20100128
  * Enable to add factors from tables (target factor equation, selectivity equation)
  * Restore simulation control auto save features (usefull for remote simulation monitoring)
  * Make all data storage isolated on simulation directory during a simulation
  * Check if result is enabled by getNecessaryResult on SensitivityExport
  * Use mexico export format for matrix
  * Restore simulation tests at integration-test phase
  * Add region factorisable value export (mexico)
  * Improve simulation stopping algorithm (now faster)
  * Locally save simulation zip to allow simulation restart even after isis shutdown
  * In sensitivity analysis, upload only one zip to caparmor for all simulations
  * Add option to control number of SSH thread to use
  * Add split pane between queued and done simulations tables
  * Add free space actions on caparmor configuration UI
  * Remove "isis-export" directory
  * Fix editor deactivation in input ui
  * Remove deprecated method SimulationStorage#getFisheryRegion()
  * Update to jfreechart 1.0.13 (major version)
  * #1585 : Analyze plans are now reloaded before each simulation
  * Editing sensitivity factors after simulation reload now works
  * Add confirmation message before launching groups creation wizard
  * #1914 : Fix metierSeasonInfo deletion
  * Remove some deprecated unused UI classes
  * Remove all GenericCell reference (now model based implementation)
  * Add missing log category in simulation filter appender
  * Add -m n option to qsub to not send email for each simulation
  * Remove duplicated SSH key configuration, common configuration for VCS and caparmor
  * Global VCS improvement (code, script UI, input UI)
  * Update to svnkit 1.3.2
  * Unification of error frames
  * Add ".shp" extension to database map files list (database migration)
  * Move codelutin librairies to nuiton.org (database migration)

isis-fish (3.2.0.10) chatellier 20091112
  * Increase default java memory for caparmor jobs (2Go)
  * Fix a problem with dependant plans not blocked on current simulation

isis-fish (3.2.0.9) chatellier 20091021
  * Add file redirection in caparmor simulation launch scripts

isis-fish (3.2.0.8) chatellier 20091020
  * Fix compilation directory per simulation problem (aspect were defined before simulation context definition)

isis-fish (3.2.0.7) chatellier 20090928
  * Fix a missing hashCode() problem in month (cause problem in matrixND)
  * Add time unit (seconds) in simulation summary
  * Make compilation directory per simulation (caparmor concurrency problem)
  * Fix plan generator was not closing database after finishing sub job
  * Delete remote launcher result archive after use
  * Modify analyze plans starting to take extra rules
  * Set ssh key in autorized_keys when connected with password
  * Isis now start with 1Go of allocated memory
  * Fix strategies/population selection on old simulation reloading
  * Always use logger for force writing in debug.txt file
  * Instantiate simulation parameters objects only when needed (to work on caparmor)
  * Add log in equation content
  * Fix export names selection problem for current simulation
  * Fix result names selection problem for current simulation
  * Improve check region code (null object, invalid names...)
  * Refactor storage modifications notification (done for regions)
  * Fix a little trace util object for both Cache and Trace aspect
  * Add aspect to get rule time (init/pre/post) in simulation summary

isis-fish (3.2.0.6) chatellier 20090914
  * Add message if scripts can't be compiled
  * Restore local simulation launcher (not as default)
  * Fix force parameter when adding result with composed names

isis-fish (3.2.0.5) chatellier 20090904
  * Fix export loaded too early (not in correct classloader)
  * Move sensitivity exports to their own package
  * Fix a date comparison problem (frequent in windows)
  * Restore running status progress bar indeterminate state

isis-fish (3.2.0.5-rc3) chatellier 20090827
  * Fix a beansutils conversion problem for MatrixND

isis-fish (3.2.0.5-rc2) chatellier 20090827
  * Can copy openmap map to system clipboard
  * Don't extract database for sensitivity analysis (excepted first)
  * Add user script javadoc generation
  * Move script import/export to Jaxx
  * Fix nano time problem (not related to real date)
  * @Doc annotation is now only field target
  * Fix creation group assistant's labels
  * Improve simulation summary
  * #1905 : nom des labels pour somme par année

isis-fish (3.2.0.5-rc1) chatellier 20090616
  * Remove inprocess launcher (due to memory consumption)
  * Readd tools.jar (isis don't need jdk anymore)
  * Fix bug 1772 ( https://labs.libre-entreprise.org/tracker/index.php?func=detail&aid=1772&group_id=8&atid=113 )
  * Fix bug 1900 ( https://labs.libre-entreprise.org/tracker/index.php?func=detail&aid=1900&group_id=8&atid=113 )
  * Fix simulation plan subjob number : start at 0
  * Fix bug, database migration asked twice
  * Fix VCSSVN getRemoteStatus(), not showed added status file
  * Rename "factors" package to "sensitivity"
  * Add tests on each storage for testing template content compilation
  * Add script templates based on freemarker template engine

isis-fish (3.2.0.4) chatellier 20090508
  * Add sensitivity analysis

isis-fish (3.2.0.4-rc1) letellier 20080225
  * Fix ask passphrase in svn+ssh mode
  * Bugfix release

isis-fish (3.2.0.3) chemit 20081218
  * Change simulation monitoring to work better with SSH launcher
  * Update XML-RPC launcher to xmlprc 3
  * Add ssh simulation launcher
  * Add Jaxx UI for all interfaces
  * Remove SwixAT UI
  * switch to lutinproject 3.3
  * switch to topia-service 1.0.1
  * switch to topia-persistence 2.1.1
  * remove tools.jar (now isis need JDK !!!)

isis-fish (3.2.0.2) chatellier 20081119
  * fix missing lutinxml dependencies

isis-fish (3.2.0.1) chemit 20081013
  * all ui are now in src/main/java
  * use maven 2 directory layout
  * use org.codelutin:lutinproject 3.0
  * passage to UTF-8

isis-fish (3.2.0.0) poussin 20080902
  * refactoring option and vcs
  * refactoring simulation engin
  * new launcher type (subprocess, caparmor ...) in progress
  * modify script to use cell and not zone in some computation
  * add TechnicalEfficiency equation
  * add Inactivity equation support

isis-fish (3.1.3) tchemit 20080319
  * bug correction when migration failed, still launch application on a new database
  * bug correction changing local database in configuration ui has no effect on next launch
  * bug correction if no svn connexion, application could not start
  * use commandline 0.4 (improve init and user options)
  * improve i18n loading (lutinutil)
  * refactor init and vcs init
  * bug correction #1605 add annotation on scripts for contextual help
  * bug correction #1617 Non prise en compte du changement de base locale
  * add new ui for configuration
  * bug correction #1601 Exception et erreur apres une simulation
  * bug correction #1602 Gros probleme de mise a jour de database entre v3.0 vers 3.1 perte des pecheries
  * bug i18n (loose some translate while using Isis-Fish from  a jar)

isis-fish (3.0.22) poussin 20071107
  * bug correction when launch many simulation with AnalysePlan (OutOfMemory)
    now we don't use cglib but javassist

ver-3.0.21 poussin 20071107
  * bug correction in ResultDatastore, bad database used to get result during simulation
  * add call to close method on SimulationStorage for in memory database
  * bug correction when launch many simulation with AnalysePlan (OutOfMemory)

ver-3.0.20 poussin 20071102
  * add clean temp directory simulation preparation
  * add IsisFish version number in parameter
  * add getPopulations method in PopulationMonitor
  * bug correction #1592 in totalFishingMortality during reduction of matrix
  * bug correction #1583 about too many file open
  * bug in export script template
  * bug correction in rule Cantonnement
  * modify database simulation usage, now we use in memory database for
    data, and file storage for result, to separate result and data. This
    improve performance (70%).
  * bug switch to h2 database version 1.0.60, this prevent rollback exception
    during simulation

ver-3.0.19 poussin 200706??
  * bug in cache for String and number in parameter
  * bug RuleMonitor add Rule as parameter
  * add extraRules field in SimulationParameter to permit Analyse Plan to
    add rules in parameter

ver-3.0.18 poussin 20070525
  * bug go.bat have correct DOS end of line
  * improve build-release.sh to send email to user and devel list after deploy
  * bug correction in delete simulation, remove close context at begin of
    clear method
  * bug permit simulation without SimulationControl
  * bug in queue model test if no more simulation to prevent Index Out of
    bound Exception

ver-3.0.17 poussin 20070524
  * add support for filename in export

ver-3.0.16 poussin 20070521
  * add ssj jar to have random library
  * add support to auto upgrade database (topia migration service)
  * bug force reload parameter in thread simulation to prevent class cast
    exception because same class is loaded in two different classloader

ver-3.0.15 poussin 20070406
  * change database lock_mode to permit read with out lock
  * change database version to 1.0.20070304
  * bug end line in equation editor

ver-3.0.14 poussin 20070402
  * add beforeOrEquals and afterOrEquals methods to Date and Month

ver-3.0.13 poussin 20070330
  * add simulation information support
  * change aspect deployment classloader (not used Agent)
  * bug in cache aspect when used without trace aspect
  * add checkout maven file option
  * change MatrixPanel context menu
  * bug in datastore closeContext (nullify storage)
  * bug in datastore getStorage (if closed create new)

ver-3.0.12 poussin 20070320
  * bug in cache, help garbage with clear on collection
  * add on matrix sumOverDim(dim, start, nb)
  * change statistic is not used by default

ver-3.0.11 poussin 200703??
  * add result support in analyse plan
  * bug in AnalysePlanContext values access

ver-3.0.10 poussin 20060305
  * Analyse Plan implementation
  * feature #1531 date automaticaly added to simulation id
  * bug in Range value inversion of integer and real
  * bug #1492 matrix index error for result matrix
  * bug #1493 simulation, region deletion
  * bug #1495 view population number with one population
  * bug #1496 simulation, region order
  * bug #1528 in min size in wizard class creation
  * bug #1535 save/cancel button activation/desactivation
  * bug #1536 simulation queue

ver-3.0.9 poussin 20060208
  * add equation editor with syntaxe checking on all equation
  * implement Region checking mecanisme

ver-3.0.8 poussin 20060205
  * force checkout of directory not checkouted at startup

ver-3.0.7 poussin 20060125
  * add import region and rename menu
  * bug in update script

ver-3.0.6 poussin 20060125
  * add support for user prompt update file at startup
  * bug saisons (bad converter init)
  * simulation thread completely rewriten
  * first version of systray
  * region copy
  * bug tree refresh after delete region
  * cvs synchronisation menu
  * test de non regression
  * bug in exports
  * bug in rules

ver-3.0.5 poussin 20061020
  * add support for delete prompt message and delete cascade prompt message

ver-3.0.4 (poussin 20061017)
  * improve cache key computation (use string), (gain 80%)

ver-3.0.3 (poussin 20061016)
  * bug in getMonth if date is negative
  * bug in message error during equation compilation, now we show the
    equation type

ver-3.0.2 (poussin 20061013)
  * add import from isis-fish v2
  * change rules from region to root
  * improve equation frame editor (equation documentation)

ver-3.0.1 (poussin 20060926)
  * bug selection cell (selected in list -> note selected in map)
  * add extension to file for model equation
  * improve refresh in input
  * add argument force to ResultStorage.addResult method
  * bug put new Population in right Species when more than one Species (tree problem)
  * bug put new Rule script in right region (tree problem)
  * add firstNull='true' for gear values types
  * bug when load gear with no values (null)
  * change method call: Input.CommitRegionInCVS -> Input.commitRegionInCVS
  * add Equation model editor
  * bug in script editor, don't used translated String for script type
  * bug in script editor, menu save/delete/deleteCVS work now
  * change use for(int i...) in GravityModel and SiMatrix for matrix access
    (gain factor 4 on matrix access and 65% on simulation)
  * change use Soft cache in ResultStorage (gain 10%)
  * improve ResultStorage keep in memory all result available to prevent unecessary
    query on database. Useful with Soft cache.
  * change use commons-collection in Cache aspect (prevent garbage bug in HashMapMultiKey)
  * bug in tools.jar search pattern for Windows
  * bug when create SetOfVessels in Effort description Add button
    was always grey
  * add getNecessaryResult in Rule to know result necessary for this rule
    and activate it automaticaly
  * improve all results are now optionnal
  * add compute and add as result discard weight if necessary
  * add TACweight in DemoRegion
  * add result MATRIX_NO_ACTIVITY
  * bug can set rule parameter
  * bug rule parameter can be saved and restored
  * improve when TopiaContext is used to read, error if another try to write
    (lock table timeout).
  * bug many correction in persistence (lock problem, load cycle problem)

ver-3.0.0
  * Utililsation de DoubleBigVector par defaut pour les Matrix
    (gain facteur entre 2 et 8 suivant parcours (Iterator, Semantic, Index)
  * Suppression de tous les scripts utilisation de Java et compilation
    (gain facteur 1000 pour l'eval des equations donc 91% sur la simulation)
