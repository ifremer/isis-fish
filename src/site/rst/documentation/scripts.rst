.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
Page de dépôt de scripts
========================

Cette page a pour vocation d'accueillir des scripts non génériques des
utilisateurs qui souhaitent les partager.
Merci d'indiquer
 * l'auteur,
 * la version pour laquelle le script fonctionne,
 * la région,
 * de préciser, pour les plans d'analyse, de quel type ils sont (exploration, analyse de sensibilité,...)
 * et de produire un descriptif de l'action du script.

Plans d'analyse
---------------

P_As_planFactFractV-12param sur la pêcherie Anchois
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Sigrid V3.1.3, Plan d'analyse : pour l'analyse de sensibilité sur la pêcherie
Anchois du GdG:

Méthode de groupe screening, les facteurs de l'analyse sont 12 groupes de
paramètres (facteurs de standardisation, mortalité naturelle adulte, mortalité
naturelle juvénile, valeurs de capturabilité,valeurs de fécondité, coefficients
de migration, bornes des classes de longueur,équations de sélectivité,équations
des facteurs de ciblage, valeur d'inactivité, valeur du TAC (français et
espagnol), paramètres de l'aire marine protegee), le plan est un plan factoriel
fractionnaire de résolution V (256 simulations).

`Plan d analyse et Fichiers Parametres et matrice`_

.. _Plan d analyse et Fichiers Parametres et matrice:: ../downloads/P_As_planFactFract_12param_Anchois.zip

Règles de gestion
-----------------

Exports
-------

Analyses de sensibilité
-----------------------

Exports pour analyse de sensibilité
-----------------------------------

Simulateurs
-----------

Équations
---------

Scripts
-------
