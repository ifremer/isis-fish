.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
Liste des tutoriels
===================

* `Premiere utilisation d ISIS-Fish`_
* `Exemple de parametrisation d une pecherie`_
* `Les plans d analyse`_
* `Tutoriaux JAVA`_
* `Un site tres pedagogique pour apprendre a programmer en JAVA`_
* `Comment utiliser les APIs`_
* `Quelques commandes utiles pour les simulations sur CAPARMOR`_

.. _Premiere utilisation d ISIS-Fish:: tutorials/firstLaunch.html

.. _Tutoriaux JAVA:: http://java.sun.com/docs/books/tutorial/

.. _Un site tres pedagogique pour programmer en JAVA:: http://www.siteduzero.com/tutoriel-3-10601-programmation-en-java.html

.. _Quelques commandes utiles pour les simulations sur CAPARMOR:: tutorials/utilCaparmor.html

.. _Les plans d analyse:: tutorials/analysisPlan.html

.. _Exemple de parametrisation d une pecherie::tutorials/parameterising.html

.. _Comment utiliser les APIs:: tutorials/useAPI.html
