.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
Documentation des APIs
======================

* `Le modele de donnees`_
* `Javadoc ISIS-Fish`_ ( src_ )
* `Javadoc JAVA`_

Librairies
----------

* `Javadoc ToPIA`_ (accès base de données)
* `Javadoc Nuiton-Matrix`_ et plus particulièrement MatrixND_
* `Javadoc Nuiton-Utils`_ (ensemble de méthodes couramment utilisées)
* `Javadoc Nuiton-j2r`_ (interface entre IsisFish et R)

.. _Le modele de donnees:: isisFishModel.html
.. _Javadoc ISIS-Fish:: http://isis-fish.labs.libre-entreprise.org/isis-fish/apidocs/index.html
.. _src:: http://isis-fish.labs.libre-entreprise.org/isis-fish/xref/index.html
.. _Javadoc JAVA:: http://java.sun.com/javase/6/docs/api/
.. _Javadoc ToPIA:: http://maven-site.nuiton.org/topia/topia-persistence/apidocs/index.html
.. _Javadoc Nuiton-Matrix:: http://maven-site.nuiton.org/nuiton-matrix/apidocs/index.html
.. _MatrixND:: http://maven-site.nuiton.org/nuiton-matrix/apidocs/org/nuiton/math/matrix/MatrixND.html
.. _Javadoc Nuiton-Utils:: http://maven-site.nuiton.org/nuiton-utils/apidocs/index.html
.. _Javadoc Nuiton-j2r:: http://maven-site.nuiton.org/nuiton-j2r/apidocs/index.html
