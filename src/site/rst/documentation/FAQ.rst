.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
FAQ
===

.. contents::

Que veulent dire les numérotations 2.1.3.1 ou 3.0.0.0 ?
-------------------------------------------------------

La numérotation suit le schéma suivant:

 * un premier numéro qui donne la version majeur d'Isis si on a 2.1.3 la version
   est 2, si on a 3.0.0 la version est 3.
 * le deuxième donne la version de base de données dans cette version majeur. si
   on a 2.1.3 cela veut dire qu'on est a la version 1 des données et dans 3.0.0
   qu'on est à la version 0.
 * le troisième numéro représente la version des scripts (isis-database-3) à
   utiliser
 * le dernier numéro représente la version mineur dans 2.1.3.1 le numéro mineur
   est 1

Lorsque l'on a déjà IsisFish en place on peut utiliser n'importe quelle version
d'Isis du moment que le numéro majeur, de base et de script sont les mêmes que
ceux actuellement utilisé, c'est à dire que seul le numéro mineur change.

Lorsque l'on souhaite utiliser une version d'IsisFish avec un numéro de base
supérieur, IsisFish convertira tout seul les données dans cette nouvelle
version, il ne vous sera plus possible d'utiliser une version d'IsisFish avec un
numéro de base inférieur.

Il est possible d'utiliser deux versions majeures d'IsisFish en même temps car
deux versions majeures ne partagent pas les mêmes fichiers de configuration. Ce
sont donc deux applications complètement distinctes.

Il est possible d'avoir une version 2 et une version 3 lancées en même temps.

Quels sont mes limites lorsque j'écris des scripts ?
----------------------------------------------------

Il ne faut jamais modifier un objet retourné par une méthode d'un script car un
système de cache est utilisé. Si vous faites ainsi, vous modifiez l'objet en
cache, et au prochain appel, vous aurez l'objet modifié et non l'objet attendu.
Par exemple si vous
retournez une List, il faut dans la méthode qui récupère la List faire une copie
avant de la modifier. Si on ne fait que lire le contenu de la List, il n'y a
rien a faire.

Scripts : objets et syntaxe
---------------------------

Connaître les objets et méthodes d'Isis
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* listes des objets, méthodes et liens entre objets (modèle UML) :
  http://isis-fish.labs.libre-entreprise.org/v3/devel/images/IsisFishModel.png .
  Cette figure permet d'avoir les noms des objets et les noms des méthodes qui
  s'y rattachent
* API ISIS :
  http://isis-fish.labs.libre-entreprise.org/isis-fish/apidocs/index.html
* Accès aux scripts: dans le logiciel, par le biais de l'interface d'édition des
  scripts, la majorité des équations sont dans script/SiMatrix.java. Le reste
  des scripts est accessible en ligne :
  http://isis-fish.labs.libre-entreprise.org/isis-fish/xref/ . Les sources sont
  aussi visualisable directement dans le repo svn:
  https://labs.libre-entreprise.org/scm/viewvc.php/isis-fish/trunk/src/main/java/fr/ifremer/isisfish/?root=isis-fish

Mise à jour des scripts
~~~~~~~~~~~~~~~~~~~~~~~

Ceci peut être fait par synchronisation avec le serveur : Fichier/serveur
synchronisation. Cette action compare les scripts présents sur le PC a ceux
présents sur le serveur et indique ceux qui sont différents. Il faut ensuite
cocher les scripts que l'on veut mettre à jour et valider.

Attention : Si une erreur est signalée dans un script, la synchronisation
modifiera le script en question.  Si la ligne à laquelle la modification est
réalisée diffère en local de celle présente sur le serveur, ceci est signalé par
les signes <<<< pour ce qu il y avait en local et pour ce qui se trouve sur le
serveur. Il faut choisir la ligne a conserver et effacer ou mettre en
commentaire l'autre, pour que le script fonctionne.

Par ailleurs, tous les scripts sont disponibles à cette adresse :
https://labs.libre-entreprise.org/scm/viewvc.php/trunk/?root=isis-fish-data

Scripts locaux
~~~~~~~~~~~~~~

Si des scripts ont été écrits en local, la synchronisation ne les vérifie pas

Scripts hérités
~~~~~~~~~~~~~~~

Si un script local a été écrit à partir d'un script présent sur le serveur, il
est nécessaire de garder une trace de ce script d'origine. Ainsi en cas de
modification du script d'origine sur le serveur, l'utilisateur peut retrouver
les scripts locaux hérités et les corriger à leur tour.

Ajout d'une aide contextuelle pour les règles et les plans d'analyse
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Dans le script de la règle ou du plan (éditeur de script),

* au niveau des imports ajouter : import fr.ifremer.isisfish.annotations.Doc
* au dessus du paramètre à documenter (par exemple public Zone param_zone =
  null;) ajouter @Doc("le parametre Zone correspond à ???")

Insérer des commentaires dans un script
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

System.out.println ();

Comment on utilise les API ?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

cf le tuto_ .

.. _tuto:: tutorials/useAPI.html


Comment créer des scripts de plan d'analyse
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

`cf le tutoriel`_

.. _cf le tutoriel:: tutorials/analysisPlan.html

Comment se structure une règle de gestion ou un plan d'analyse ?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

cf déroulement d'une simulation au bas de `cette page`_ et `celle-ci`_

.. _cette page:: devel/architecture.html

.. _celle-ci:: userManual/analysisPlan.html

Un paramètre de ma règle se comporte bizarrement, qu'est-ce qui se passe ?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Les variables paramètre de règle (celles qui commencent par \param_) sont
initialisées ua début de la simulation et non au début de chaque pas de temps.
Il ne faut donc pas les modifier mais se servir de variables intermédiaires.

Interfaces
----------

Vérification du paramétrage de la pêcherie
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Pour toute la pêcherie. Dans l'interface région : bouton en bas vérifier

  - en bleu : ok
  - en orange : paramètres avec pb
* Pour une équation. Dans l'éditeur d'équation : bouton check - vérification de
  la syntaxe
* Pour un script (rule, AnalysePlan,...). Dans l'éditeur de script : bouton
  vérifier - vérification de la syntaxe. Possibilité de voir les différences
  entre le script sur la machine utilisateur et sur le serveur en allant sur
  menu serveur de l'interface éditeur de script

A quoi sert tag dans l'interface de lancement de simulation (paramètres avancés)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Paramétrer des exports, le simulateur ou des équations

exemple : dans le simulateur, simuler sans les variables économiques::

 if (!"false".equalsIgnoreCase(param.getTagValue().get("ecoResult"))) {
   control.setText("Add economics results");
   saveGravityModel(date, resManager, gravityModel);
 }

ici le tag s'appelle ecoResult

Effort description
~~~~~~~~~~~~~~~~~~

Les variables "Opération de pêche" et "nombre d'engins par opération" entrent
dans les équation de standardisation de l effort par marée de cette manière::

  StdEffortPerHour = Fstd * FishingOperationNumber * GearNumberPerOperation.

Donc a priori (???) elles doivent être remplies quand Fstd est calculé a
l'échelle opération de pêche*engin.

Si Fstd est calculé à l'échelle de la marée il faut ABSOLUMENT mettre 1 dans les
deux cases (sinon les captures seront nulles).  la variable "durée de la pêche"
n'est pas utilisée dans les équations.

Unités...
~~~~~~~~~
A confirmer mais très probablement :

trip types :

 * durée de marée en heures
 * temps minimal entre deux marées : inutilisé

vessel type

 * durée max de marée : inutilisée
 * vitesse en km/h
 * intervalle d activité : inutilisé

Entrer le mois d'application d'une règle de gestion dans l'interface de lancement de Simulation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

C'est le numéro du mois qui est attendu, soit un chiffre entre 0 et 11 (0 :
janvier et 11: décembre)  Sinon ça bug !

Est-ce que la simulation a bien tourné ?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* S'assurer dans la queue de simulation de l'état de la simulation : simulation
  terminée

* Sélectionner la simulation dans la table (ou dans l'interface des résultats de
  simulation)  cliquer sur "voir les logs":

* sélectionner fatale, erreur et warning : si quelque chose apparaît, c'est
  qu'il y a un bug! en dessous du ERROR, est affiché l'exception. Il faut
  parcourir le stack trace et identifier une ligne qui porte sur
  ifremer.isisfish et voir à quoi cela se rapporte.

Affichage des effectifs et biomasses dans la fenêtre de résultats
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Une petite subtilité : dans la fenêtre Résultats, les effectifs/biomasse d'une
population affichés pour un mois donné correspondent à ces effectifs/biomasses à
la fin du pas de temps précédent.

Vous n'utilisez pas le dépôt correct pour votre version d'Isis-Fish
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Le message suivant apparaît ::

  Vous n'utilisez pas le dépôt correct pour votre version d'Isis-Fish : 3.2.0.8.
  Voulez-vous changer de dépôt ?

Ce message apparaît lors de l'utilisation d'une nouvelle version majeure
d'IsisFish, il vous demande simplement si vous désirez utiliser la nouvelle
version des scripts qui fonctionne avec cette version.

Astuces de modélisation
-----------------------

Séparer mâles et femelles ayant des croissances différentes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

On crée des classes de longueur consécutives pour les mâles et d'autres pour les
femelles qui seront donc à la suite.  Par ex. pour les mâles, on va dire qu il y
a les classes suivantes ::

  60-70 70-77 77-110 110-166

et pour les femelles ::

  60-70 70-78 78-86

on clique sur 'recruter les classes'. Piur la première taille, on rentre '60'.
Ensuite pour les tailles maximales de groupe, on rentre :
'70;77;110;166;70;78;86' et on valide. Cela crée une classe 4 de taille
minimale 166 et maximale 70 !

Dans la saisie des groupes de population pour le groupe 4, le premier groupe de
femelles, on modifie la valeur de la borne inférieure : de 166 à 60. On sauve et
c'est tout !!!

Pour ne pas comptabiliser le temps de trajet dans l'effort
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

L'astuce consiste à mettre une vitesse de navires très importante par exemple
10^9, la durée de trajet devient tres petite ~ 0

Le Modèle
---------

Définition des saisons pour une population
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Dans ISIS, les saisons correspondent à une modification de la paramétrisation
liée à l'occurrence d'événements affectant la population : Reproduction,
changement de classe, migration...

Pour définir les saisons il convient donc de placer ces différents événements
sur une ligne de temps, les saisons seront donc les mois consécutifs entre deux
événements.

Chronologie des événements biologiques et migratoires à l'échelle de l'année
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Attention : les changements de classe et la reproduction ont lieu à chaque mois
de la saison contrairement à la migration qui a lieu au premier mois de la
saison. Il n'est pas necessaire de définir une saison pour le recrutement,
celui-ci peut avoir lieu a n'importe quel moment d'une saison.

FIXME Mail de Stéphanie du 11/07/07 : le changement de classe pour une
population structurée en age ne se fait qu'une fois dans la saison, le code
pourrait etre modifié pour éviter d'avoir à définir une saison d'un mois pour le
changement de classe. Il y a des avantages et inconvénients (cf réponse
d'Hilaire) On abandonne ?

Attention aux hypothèses de modélisation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Hypothèse de base d’ISIS : toutes les variables d’états sont supposées homogènes
à l’échelle de la zone définie – par exemple Si une classe d’une pop est
distribuée sur Zpop à un pas de temps t, alors l’effectif de cette classe est
homogènement distribué sur Zpop, un effort sur une partie de Zpop impacte les
effectifs de toute la zone en se diluant sur toute la zone pop::

  (N(Zpop,t+1) ~= N(Zpop,t)-F(met)*(N(Zpop,t))).

Ne serait-il pas plus judicieux de faire impacter uniquement la zone
intersection entre la zone metier et la zone pop ::
  (N(Zpop,t+1) ~= N(Zpop,t)-F(met)*(N(Zpop,t)*(inter(Zmet,Zpop)/Zpop)) ?

Si on veut évaluer l’impact d’une AMP Zg incluse dans Zpop,

* Ne pas distinguer 2 zones de population dans Zpop [Z1=Zpop-Zg et Z2=Zg], a pour
  conséquence que l’abondance dans Z2 sera impactée par les captures possibles
  dans Z1 (car à chaque pas de temps les captures pouvant avoir lieu dans Z1 vont
  impacter l’abondance de Zpop::

    N(Zpop,t+1) ~= N(Zpop,t)-F(met)*(N(Zpop,t))

  et avec l’hypothèse d’homogénéité dans Zpop, les effectifs dans Z2 seront égaux
  à N(Zpop,t+1)*Z2/Zpop.

* Distinguer 2 zones de population Z1=Zpop-Zg et Z2=Zg, a pour conséquence que
  l’abondance de Z2 ne sera pas impactée par les captures dans Z1 (permet de
  modéliser le fait que les poissons dans Z2 sont inféodés à cette zone)::

  N(Z1,t+1) ~= N(Z1,t)-F(met)* N(Z1,t), N(Z2,t+1)= N(Z2,t+1)-exp(-M/12) N(Z2,t) (si pas de migrations)

* Pour décrire un phénomène de refuge, il faudrait peut-être rajouter une
  migration des individus de Z1 dans Z2 en densité dépendance... ?

Impact de la taille d'une AMP si l'amp est incluse dans la zone pop

 * Si une seule zone pop Zpop
 * Si Zg incluse dans Zpop
 * Si le métier impactant la pop est distribué sur Zmet=Zpop
 * Si le métier réalloue son effort sur la zone restante : Zmet-Zg=Z1,

N(Zpop,t+1) = N(Zpop,t)-F(met)/(F(met)+M/12)exp(-F(met)+M/12)*N(Zpop,t), N(Z1,t+1) = N(Zpop,t+1)*Z1/Zpop, N(Z2,t+1) = N(Zpop,t+1)*Z2/Zpop,

 * Si Zg grandit (par exemple nZg=kZg), F(met) ne change pas, les effectifs

N(Zpop,t+1) = N(Zpop,t)-{F(met)/(F(met)+M/12)*exp(-F(met)+M/12)}* N(Zpop,t) N(Z1,t+1) = N(Zpop,t+1)*[Zpop-kZg]/Zpop, N(Z2,t+1) = N(Zpop,t+1)*kZg/Zpop,

**ie qu’avec une modélisation de la zone de distribution de la pop en une seule zone (ie pas d’hétérogénéité entre la partie de la zone pop dans l’amp et l’autre), un changement de taille de la zone ne peut pas avoir d’impact**

Si on fait l'hypothèse proposée : N(Zpop,t+1) ~= N(Zpop,t)-F(met)*(N(Zpop,t)*(inter(Zmet,Zpop)/Zpop)).

**Dans ce cas l’impact (différence entre les effectifs pour la grande AMP – effectifs pour petite AMP) = (k-1)* Zg)/Zpop F(met)/(F(met)+M/12)*exp(-F(met)+M/12)}* N(Zpop,t)**

Remarque additionnelle : superposition de zones population
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

:?: ''A revoir/repenser mais à priori : ''

Si nécessaire, il est possible de créer des zones population se chevauchant pour
une même population. Si la mortalité naturelle est la même dans les deux zones,
les captures dans la zone d'intersection seront correctement calculées. En effet
si la mortalité naturelle est la même dans les deux zones, l'effort
E(str, met, grp) puis la mortalité par pêche F(str, met, grp) et le taux de
capture CR(str, met, grp) sont calculés indépendamment pour chaque zone pop mais
identiques, on a donc

 * zone pop 1 : E -> F -> CR
 * zone pop 2 : E -> F -> CR

et les captures seront B1*CR + B2*CR = (B1+B2)*CR

(et ca marche qu'on soit dans le modèle par cellule ou dans le modèle par zone)

Simulations
-----------

Ce qui se passe à t = 0
~~~~~~~~~~~~~~~~~~~~~~~

Lors d'une simulation, le premier pas de temps est particulier, et tous les
événements se déroulant habituellement en janvier n'arrivent pas forcement :

 * la migration a lieu
 * le changement de classe n'a pas lieu

FIXME ca sera modifié ???

:-( Attention en entrant les effectifs initiaux  et les résultats de janvier de
l'année 0 ne sont pas comparables à ceux des autres années de simulation.

Simulation par cellule
~~~~~~~~~~~~~~~~~~~~~~

Pour effectuer une simulation par cellule, dans l'interface de lancement de
simulation (ou d'analyse de sensibilité), dans l'onglet Paramètres avancés, on
ajoute un tag effortByCell dont la valeur est à true.

Analyse de sensibilité
----------------------

ISIS-Fish se ferme des que je lance une analyse de sensibilité
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Il s'agit d'une configuration de R incomplete. Reportez-vous au point "Les
analyses de sensibilités ne fonctionnent pas"

À partir de la version 3.3.0.0 d'Isis-fish, une erreur devrait s'afficher dans
le fichier debug.txt et ne plus se fermer.

Les analyses de sensibilités ne fonctionnent pas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

L'erreur la plus probable provient d'une configuration incomplète de R pour
ISIS-Fish.

Vous devez avoir la version 2.9.0 de R.

Pour Windows, vous devez configurer deux variables d'environnements :

 * `R_HOME` à C:\Program Files\R\R-2.9.2
 * `PATH` doit contenir : `%R_HOME%\bin`

Trucs et astuces
----------------

Remplir une table à partir d'un fichier txt ou csv
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Dans les interfaces, il suffit de cliquer droit sur la table à remplir et de
choisir importer à partir d'un fichier.

Les nombres entiers trop grands posent problème
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Il faut faire 39E3 au lieu 39000

Pourquoi 41/1000 ça fait 0 ?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Dans ma règle de gestion, je fait une division de 41 par 1000 et le résultat est
0, pourquoi ?

En java, tout est typé. Ce qui est fait dans l'opération précédente, c'est une
division euclidienne, 41 et 1000 étant des entiers. Pour avoir le résultat d'une
division "classique", il faudrait faire 41.0/1000.O, les deux nombres étant des
nombres à virgule (que ce soit double ou float), le résultat sera un nombre à
virgule (double ou float).

Plus généralement, en java, lors d'une opération entre deux types, java garde la
plus grande précision. Par exemple, une opération entre deux entiers donnera un
entier, entre deux doubles un double, mais entre un entier et un double, le
résultat sera un double. Vous pouvez avoir plus d'info par ici :
http://www.siteduzero.com/tutoriel-3-10276-les-variables-et-les-operateurs.html#ss_part_2

Comment je fait une exponentielle (ou tout autre fonction mathématique "évoluée")
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pour la plupart des fonctions mathématiques courantes, il faut utiliser les
méthodes statiques de la classe Math.

Pour plus d'infos : http://jmdoudoux.developpez.com/cours/developpons/java/chap-math.php
