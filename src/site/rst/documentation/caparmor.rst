.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
Note aux utilisateurs de Caparmor
---------------------------------

Performances
============

Actuellement, le fonctionnement d'ISIS-Fish sur Caparmor n'est pas encore
optimal. En effet, une simulation seule peut être plus longue a être exécutée
sur Caparmor que sur un PC personnel pour des raisons d'optimisation de calcul
et d'accès disque. Nous travaillons actuellement à rendre l'utilisation d'ISIS
sur Caparmor aussi rapide et aisée qu'une utilisation sur un poste local.

Le gain en performance que peut apporter Caparmor est sur le nombre de
simulations tournant en parallèle, ainsi sur Caparmor, un utilisateur peut avoir
jusqu'à 16 simulations en parallèle.

Plans de simulations dépendants
===============================

Il est actuellement possible d'effectuer un plan de simulation dépendant sur
Caparmor, mais il n'est pas possible d'en faire tourner plusieurs en parallèle.
De plus, Caparmor n'effectuant les simulations qu'une par une dans ce cas, il
perd de son utilité. Afin de ne pas perdre de temps, il est grandement conseillé
d'utiliser une machine locale pour effectuer les plans de simulations
dépendants.
