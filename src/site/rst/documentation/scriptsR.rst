.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
Exemple de scripts R
====================

Traitement des sorties
----------------------

Le document proposé contient un exemple de script R , pour le traitement des
résultats de simulation .l'objectif du programme : importations des résultats
des plans d'analyse (issus de 256 simulations) "l'analyse de sensibilité", avec
ce programme R, on peut importer les 256 résultats des simulations (Biomasse
Finale, Abondance finale, les captures cumulées de la dernière année, les
captures cumulées au cours de 5 dernières années de simulation, Effort de pêche
cumulé sur toute la durée de simulation) en même temps. Et en fin renvoyer tous
les resultats dans une même matrice.

`Script R pour le traitement des sorties`_

.. _Script R pour le traitement des sorties:: ../downloads/traitementSorties.R
