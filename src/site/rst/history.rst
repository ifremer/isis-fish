.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
Historique
==========

Isis-FISH a beaucoup évolué depuis le début du développement,
l'`historique <history/history.html>`_ vous rappelle l'histoire du simulateur.
Vous pouvez consulter le `journal des modifications <history/changelog.html>`_
de la version actuellement maintenue.

Anciennes versions
------------------

Vous trouverez ici les informations des anciennes versions du simulateur.

 * `Version 1 <v1/changelog.html>`_
 * `Version 2 <v2/index.html>`_

 
