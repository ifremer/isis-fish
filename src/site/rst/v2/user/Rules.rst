.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
=====================
Les règles de gestion
=====================

:Author: Benjamin Poussin <poussin@codelutin.com>
:Revision: $Revision$
:Date: $Date$

.. sectnum::

.. contents::


Etude d'une règle de gestion
============================

ISIS-FISH dispose d'une interface dédiée à la création et à l'édition des règles
de gestion :

.. image:: images/RulesInterface.jpg

Une règle de gestion se compose d'un nom (ici FermetureZone) et d'un ensemble de
paramètres de création de la règle (ici zone, dateDeb, dateFin, moisDeb et
moisFin). Ces paramètres ont un type et peuvent aussi voir une valeur par
défaut. Ils sont fournis une seule fois à la création de l'objet et ne doivent
pas être confondus avec les paramètres de simulation spécifiés plus loin.

Le corps de la règle de gestion se décompose lui même en :

- Un constructeur, qui est du code exécuté lors de la création de la règle,
- L'ensemble des paramètres de simulation qui sont réévalués à chaque pas de
  temps,
- d'une condition, qui permet d'appliquer ou non la règle de gestion,
- d'une *action avant*,
- d'une *action après*


Nous allons maintenant examiner le code source de ces différentes parties avec
l'exemple de la règle de gestion FermetureZone.

On trouve tout d'abord (premier onglet de l'interface) le constructeur de la
règle :
Il s'agit du code exécuté à la création de la règle. Son rôle est de récupérer
et de stocker les paramètres de création ainsi que d'initialiser les variables
qui serviront ensuite dans le reste de la règle de gestion.

Dans cet exemple, des raccourcis sont tout d'abord créées qui permettront
ensuite de ne pas avoir à taper le chemin complet des objets les plus utilisés::

  var StringBuffer = Packages.java.lang.StringBuffer;
  var Date = Packages.fr.ifremer.nodb.Date;
  var XMLUtil = Packages.org.codelutin.xml.XMLUtil;

On recupère ensuite les parametres de création pour les stocker::

  this.param = param;

On construit ensuite un nouvel objet (p) que l'on initialise avec ces
paramètres. Cet objet va stocker les différents paramètres pour une utilisation
au travers des différents codes constitutifs de la règle de gestion::

  //construction de p
  this.p = new Object();
  this.p.paramRegle = this.param;
  //la zone de fermeture
  this.p.zone= this.param.getValue("zone");
  writeln("zone:"+this.p.zone);
  //les mailles du secteur de fermeture
  this.p.mailles = this.p.zone.getMaille();

  this.p.dateDeb=this.param.getValue("dateDeb");
  this.p.dateFin=this.param.getValue("dateFin");

  this.p.moisDeb=this.param.getValue("moisDeb");
  this.p.moisFin=this.param.getValue("moisFin");


Le code de l'onglet « paramètres » enregistre les paramètres de simulation du
pas de temps courant dans notre objet param pour une utilisation dans le code de
la condition et des actions. Ces paramètres sont : le métier sur lequel la règle
est appliquée, la date, les objets **suivie** (historique de la simulation, voir
la Javadoc) et **gestion métier** (cf documentation de Maud)::

  this.p.metier = metier;
  this.p.date = date;
  this.p.suivie = suivie;
  this.p.gestionMetier = gestionMetier;

Le code de l'onglet Condition conditionne l'application de la règle. C'est ici
qu'on détermine si la règle de gestion s'applique ou pas pour le pas courant. La
condition est constituée de tests divers, et doit renvoyer un booléen : vrai
(elle s'applique) ou faux (elle n'est pas appliquée pour ce pas de temps). La
condition dans cet exemple s'exprime sur les dates et le lieu et l'on vérifie
que l'on est bien entre les dates de début et de fin et que l'on a une
intersection non vide entre la zone considérée et la zone de fermeture::

  writeln("condition fermeture zone");
  var mois = p.date.getMois().getNumMois();
  writeln("mois:"+mois);
  if(!(p.moisDeb<=mois && mois<=p.moisFin))
    return false;
  writeln("on est dans l'espace des mois possible");
  //on est dans l'espace des mois possible
  if(p.date.before(p.dateDeb))
    return false;
  if(p.date.after(p.dateFin))
    return false;

  var mailleMetier = p.metier.getSecteurMois(p.date.getMois()).getMaille();

  // s'il y a une intersection avec la zone fermee, alors la regle s'applique
  mailleMetier.retainAll(p.mailles)
  var result = mailleMetier.size() != 0;

  if(result)
    writeln("===== Fermeture Zone s'applique ======");
  return result;

Le code porté par l'onglet « Action Avant » spécifie les actions à effectuer
avant la simulation du pas de temps::

  writeln("fermeture zone Action avant metier:"+p.metier);
  writeln("Les mailles fermees sont:"+p.mailles);

  var zoneMetier = p.metier.getSecteurMois(p.date.getMois());
  var mailleMetier = zoneMetier.getMaille().copy();
  mailleMetier.removeAll(p.mailles);
  writeln("inter maille:"+mailleMetier.size());
  // test pour savoir si toute la zone de pratique du metier
  // (reunion de toutes les zones metiers  de metier a date)
  // est totalement incluse dans la zone fermeture
  if (mailleMetier.size() != 0){ //le cas intersection.length==mailleMetier est dans condition
    // si toute la zone de pratique du metier n'est pas incluse dans zone fermeture

    //creation du nouveau secteur de metier
    var secteurResult = MetaSecteurFactory.create("MetaFermeture-"+p.metier.getNom()+"-"+date.getDate(), p.metier.getRegion(), "Secteur creer durant la simulation.");
    writeln("nouveau secteur cree:"+secteurResult);
    var zonesMetier = zoneMetier.getAllSecteur();
    writeln("zonesMetier:"+zonesMetier);
    writeln("zonesMetier.size:"+zonesMetier.size());

    for(var i=0; i<zonesMetier.size(); i++){
      var zonemet = zonesMetier.get(i);
      writeln("zonemet:"+zonemet);
      // intersection entre la zeme zone metier a date et zone de fermeture
      var listemailleszonemetier = zonemet.getMaille().copy();
      var nbMailleZoneMetier = listemailleszonemetier.size();
      writeln("maille metier:"+listemailleszonemetier);
      writeln("maille fermeture:"+p.mailles);
      listemailleszonemetier.removeAll(p.mailles);
      if(0==listemailleszonemetier.size()){
      //si completement inclus on le supprime
      //donc on ne le met pas dans le nouveau
      writeln("on supprime la zone"+zonemet);
    }
    else if (listemailleszonemetier.size() != nbMailleZoneMetier) {
      // si la zone metier n'est pas totalement incluse dans zone de fermeture
      // on reduit  la zeme zone metier de cette intersection
      // pas de modif de la matrice de proportion strmet
      writeln("on cree une nouvelle zone a partir de:"+zonemet+" avec "+listemailleszonemetier);
      var simpleSecteur = SecteurSimpleFactory.create("Fermeture-"+p.metier.getNom()+"-"+zonemet+"-"+date.getDate(), p.metier.getRegion(), "Secteur creer durant la simulation.");
      simpleSecteur.addAllMaille(listemailleszonemetier);
      secteurResult.addSecteur(simpleSecteur);
     writeln("apres addSecteur");
  }else {
      //sinon on le met dans le nouveau
      writeln("On remet tel quelle la zone:"+zonemet);
      secteurResult.addSecteur(zonemet);
    }
  }// fin du for sur les zone metier de la zone de pratique du metier
  writeln("Zone metier avant"+zoneMetier.getMaille());
  writeln("Zone metier apres"+secteurResult);
  writeln("Zone metier apres"+secteurResult.getMaille());
  p.metier.setSecteurMois(secteurResult, p.date.getMois());
  }
  else {
    writeln("Toute la zone metier est ferme");
    // sinon toute la zone de pratique du metier est incluse dans zone fermeture
    // alors metier devient metier-nonactivite

    //listes des strategies contenant ce metier et tel que strmet(metier,date)!=0
    var listMetiers = db.oql("select * from fr.ifremer.db.StrMetFactory where metier=? and proportion!=0",
    (new DBArgument()).add(p.metier));

    writeln("");

    var listestrategiesdemetier = new DBUniqueCollection(StrategieFactory);
    for(var i=0; i<listMetiers.size(); i++)
      listestrategiesdemetier.add(listMetiers.get(i).getStrategie());

    writeln("Resultat de la requete:"+listestrategiesdemetier);
    var metChomage = MetierFactory.findByKey("nonActivite", p.metier.getRegion());
    writeln("Metier nonActivite:"+metChomage);
    for (var s=0; s<listestrategiesdemetier.size(); s++){
      var strategie = listestrategiesdemetier.get(s);
      writeln("debut pour str:"+strategie);
      var strMet = StrMetFactory.findByKey(strategie, p.metier, p.date.getMois());
      writeln("strmet:"+strMet);
      var strMetChomage = StrMetFactory.findByKey(strategie, metChomage, p.date.getMois());
      writeln("strmet chomage:"+strMetChomage);
      strMetChomage.setProportion(strMetChomage.getProportion()+strMet.getProportion());
      strMet.setProportion(0);
      writeln("fin pour str:"+strategie);
    }
  }// fin du else passage a metier-nonactivite
  writeln("fin fermeture zone action avant");
  return p.gestionMetier;


Enfin l'onglet « Action Apres » spécifie les actions effectuées après la
simulation du pas de temps :

writeln("Action apres");
return p.gestionMetier;
