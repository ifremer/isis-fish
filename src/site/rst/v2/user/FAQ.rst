.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
===================
Foire Aux Questions
===================


Que veulent dire les numérotations 2.1.3 ou 3.0.0
=================================================

La numérotation suit le schéma suivant:

- un premier numéro qui donne la version majeur d'Isis si on a 2.1.3 la
  version est 2, si on a 3.0.0 la version est 3.
- le deuxième donne la version de base de données dans cette version majeur
  si on a 2.1.3 cela veut dire qu'on est a la version 1 des données et dans
  3.0.0 qu'on est à la version 0.
- le dernier numéro représente la version mineur dans 2.1.3 le numero mineur
  est 3

Losrque l'on a déjà isis en place on peut utiliser n'importe quelle version
d'Isis du moment que le numero majeur et de base sont les mêmes que ceux
actuellement utilisé, c'est à dire que seul le numero mineur change.

Lorsque l'on souhaite utiliser une version d'isis avec un numero de base
supérieur, isis convertira tout seul les données dans cette nouvelle
version, il ne vous sera plus possible d'utiliser une version d'isis avec un
numero de base inférieur.

Il est possible d'utiliser deux versions majeur d'Isis en même temps car
deux version majer ne partage pas les mêmes fichiers de configuration. Ce
sont donc deux applications complètement distinct.

Il est possible d'avoir une version 2 et une version 3 lancées en même
temps.


Quels sont mes limites lorsque j'écris des scripts
==================================================

- Il ne faut jamais modifier un objet retourné par une méthode d'un script
  car si le cache est utilisé, vous modifier aussi l'objet en cache et donc
  au prochain appel vous aurez l'objet modifier et non l'objet attendu.
  Par exemple si vous retournez une List, il faut dans la méthode qui
  récupère la List faire une copie avant de la modifier. Si on ne fait que
  lire le contenu de la List, il n'y a rien a faire.

Comment ajouter des méthodes d'export
=====================================

Il faut créer un Objet **Export** dans l'éditeur ECMAScript s'il n'existe
pas déjà. Puis ajouter des méthodes à cet objet. Ces méthodes d'export
apparaitront automatiquement dans les menus d'exportation.

Paramètre des méthodes d'export
-------------------------------

Les méthodes d'export prennent 3 paramètres:

- l'identifiant de la simulation
- l'objet simulation
- l'objet ResultatManager2 de la simulation
