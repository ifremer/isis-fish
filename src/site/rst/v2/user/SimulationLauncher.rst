.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
==================
SimulationLauncher
==================

:Author: Benjamin Poussin <poussin@codelutin.com>
:Revision: $Revision$
:Date: $Date$

.. sectnum::

.. contents::


Lancement d'une simulation
==========================

Pour lancer une simulation plusieurs renseignement doivent être fournis:

- la région de simulation
- les populations
- les effectifs pour chaque population
- les stratégies
- un identifiant de simulation

et facultativement:

- un script de présimulation
- un plan d'analyse
- des méthodes d'export automatique


Export Automatique
==================

Avant de lancer une simulation il est possible de choisir des méthodes d'export
qui seront automatiquement appliqué après la simulation. Les fichiers générés
par ces méthodes seront alors mis dans un sous répertoire du répertoire indiqué
dans l'interface comme devant recevoir les exports.

Ces sous répertoires sont défini à partir sur nom de la simulation. On retrouve
dans ce répertoire un fichier texte pour chaque méthode d'export, un fichier
supplémentaire reprenant les informations de lancement de la simulation.


Script de présimulation
=======================

Le script de présimulation est du script dans lequel on peut mettre ce que
l'on veut. Ce script s'exécutera avant le lancement de la simulation.

Le plus courant pour ce script est de modifier une valeur dans la base de
données. Cela permet de ne pas modifier la base de données à chaque
simulation lorsque l'on veut faire une seul simulation en modifiant un
paramètre de la base.


Plan d'analyse
==============

Il permet de faire des choix complexes dans le choix des scripts que le
simulateur doit appliquer avant la simulation.

Le principe est la création d'un tableau dans lequel on va stocker tous les
différents scripts que l'on souhaite utiliser pour les simulations.

Pour chaque entrée dans ce tableau une simulation sera fait avec comme script
de pré-simulation de sequence l'entrée courante du tableau.

par exemple si l'on a le tableau suivant::

  new Array("var pop = PopulationFactory.findByName('Thon'); pop.setClasseMature(1)",
            "var pop = PopulationFactory.findByName('Thon'); pop.setClasseMature(2)",
            "var pop = PopulationFactory.findByName('Thon'); pop.setClasseMature(4)")

Il y aura trois simulations d'effectuées. Avec comme différence entre chaque
simulation le classe mature du *Thon*.

Pour générer ce tableau vous avez deux possibilité, soit l'écrire entièrement
à la main comme il apparait ci-dessus, soit le créer dans une boucle.

La deuxième façon est plus intéressante. Voici une façon de le coder::

  var result = new Array();

  for (var i=1; i<=4; i++){
    if ( i != 3 ){
      result.push("var pop = PopulationFactory.findByName('Thon'); pop.setClasseMature("+i+")");
    }
  }

  result;

Ce qui est important dans cet exemple est la dernière ligne. Car si on ne la
met pas, le résultat retourné sera indéterminé, alors qu'avec cette ligne,
c'est bien notre tableau qui est retourné.

La méthode push du tableau permet d'ajouter un élément au tableau.

La difficulté ici est de faire la différence entre le script qui permet de
généré le script et le script généré. Surtout si le script généré contient lui
même des chaîne de caractères. Le plus simple dans ce cas est d'utilisé des
marqueur de chaîne différent. Par exemple ici pour les chaînes du script de
génération de script on utilise **"**, et pour les chaînes inclues dans les
scripts générés on utilise **'**.

Pour le reste vous avez à votre disposition les mêmes éléments que dans le
code ECMAScript du simulateur.

