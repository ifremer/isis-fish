.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
==============
ECMAScriptEdit
==============

:Author: Benjamin Poussin <poussin@codelutin.com>
:Revision: $Revision$
:Date: $Date$

.. sectnum::

.. contents::

ECMAScriptEdit
--------------

L'editeur d'ECMAScript permet d'influer sur la façon dont isis-fish simule.
Une partie du simulateur est écrit grâce à cette interface, il suffit de
modifier ce code pour modifier la façon de simuler. Le code qui n'est pas
directement écrit en ECMAScript devra être réécrit pour modifier cette
partie du simulation. Cette partie du simulateur n'est pas écrite en
ECMAScript pour des raisons de performance, car des optimisations sont faite
lors de la compilation du code, et pour des raisons de débuggage.
