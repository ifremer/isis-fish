.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
==========
ECMAScript
==========

:Author: Benjamin Poussin <poussin@codelutin.com>
:Revision: $Revision$
:Date: $Date$

.. sectnum::

.. contents::


Vue d'ensemble
==============

Dans le projet ISIS-FISH, le coeur du simulateur est constitué d'un moteur de
simulation générique qui consiste en l'interprétation de routines de
simulations écrites en Ecmascript. L'Ecmascript est un langage de programmation
de script orienté objet, version standardisée par l'ECMA des langages de
scripts javascript et Jscrit.

Pour le projet ISIS-FISH, l'implémentation d'Ecmascript qui a été utilisée est
FESI. On  trouvera une documentation spécifique sur FESI à cette adresse :
http://home.worldcom.ch/jmlugrin/fesi/docindex.html


Intégration du Javascript à ISIS-FISH
=====================================

L'ecmascript est utilisé au sein d'ISIS-FISH pour écrire les règles de gestion.
Tous les objets java développés pour l'ifremer sont utilisables dans les règles
de gestion (Ils sont rendus disponibles au code de script au travers de
l'interpréteur). On accède à l'ensemble de ces services en spécifiant le chemin
complet d'accès à la classe Java préfixé du mot clef « Packages », c'est à dire
par exemple Packages.fr.ifremer.nodb.Date.

L'ensemble de ces objets sont documentés au travers de la Javadoc du projet
disponible en ligne sur le serveur du simulateur (La javadoc est un ensemble de
pages HTML générées à partir des commentaires des sources du projets).

Pour simplifier l'accès aux objets et méthodes fréquemment utilisés, des
raccourcis ont été créés de sorte que ces services puissent être utilisés sans
spécifier le chemin d'accès complet.

On trouve tout d'abord des objets provenant de DBOBJECT et pour lequels une
documentation existe en plus des javadocs (Manuel du développeur). Les objets
concernés sont les suivants :

- DBManager
- DBArgument
- DBUniqueCollection
- DBCollection
- l'ensemble des factory (Fabriques d'objets métiers).

Une méthode capitalize() a aussi été ajoutée qui permet de mettre en majuscule
la première lettre d'une chaine de caractères.

L'objet **global** permet quant à lui de définir des variables globales qui
seront ensuite utilisables dans l'ensemble des routines Ecmascript exécutée
dans le cadre d'une simulation (on pourra par ce biais passer de l'information
d'une règle de gestion à une autre). On définira ces valeurs ainsi :
global.maVariable = *maValeur*

L'objet **date** (fr.ifremer.nodb.Date) permet de gérer la date durant une
simulation. Cette date est constituée d'un numéro d'année et d'un numéro de
mois. La javadoc de l'objet fr.ifremer.nodb.Date détaille l'ensemble des
méthodes disponibles.

Enfin, une extension à FESI a été ajoutée pour pouvoir faire des opérations
d'entrées/sorties tel que write et writeln plus facilement. Cette extension se
nomme BasicIO et la liste complète des fonctions utilisables ainsi que la
documentation associée peuvent être consulter ici
http://home.worldcom.ch/jmlugrin/fesi/bioext.html.

