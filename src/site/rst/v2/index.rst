.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
Version 2
=========

Cette version n'est plus maintenue depuis 2006

Documentation
-------------

`FAQ <users/FAQ.html>`_
  Foire aux questions

`Langage ECMAScript <users/ECMAScript.html>`_
  Tutorial sur le langage ECMAScript

`Editeur ECMAScript <users/ECMAScriptEditor.html>`_
  Documentation sur l'utilisateur de l'éditeur de ISIS-Fish

`Règles de gestion <users/Rules.html>`_
  Documentation sur les règles de gestion et leur interface

`Lanceur de simulations <users/SimulationLauncher.html>`_
  Documentation sur l'interface de lancement de simulation

`Documentation d'Hilaire <users/ScriptHilaire.html>`_
  Document écrit et maintenu par Hilaire qui aide à la prise en main de tous les
  aspects avancés d'ISIS-Fish

Téléchargement
--------------

Cette version n'est plus disponible en téléchargement.

Information sur les changement de cette version
-----------------------------------------------

ver-2.3.2 (20060727)
  * correction de grossValueOfLandingsPerSpeciesPerStrategyMet qui ne tenait    pas compte du prix

ver-2.3.1 (20060727)
  * modification des equations economique qui contenait des erreurs

ver-2.3.0 (20060623)
  * passage en equation de `EspecesCapturables`

ver-2.2.6 (20060605)
  * sauvegarde des simulations directement dans un fichier compressé    sans passé par une représentation DOM pour une utilisation moindre    de la mémoire
  * getMailleSecteur retourne une liste vide si la zone pour lequelle    on souhaite les mailles est null. Ca evite une Exception.
  * ajout d'un log warning si un metier n'a pas de zone pour un mois    donné

ver-2.2.5 (20060601)
  * sauvegarde des simulations avant la sauvegarde des resultats (si la    sauvegarde des resultats echoue on conserve les parametres de lancement)
  * matrix ajout d'un ascenseur horizontal, ajout d'un menu contextuel    (copier/coller, export/import cvs)
  * correction bug introduit par l'optimisation (fishingMortality trop    souvent recupérée dans le cache, car par de discremination sur le metier)

ver-2.2.4 (20060523)
  * optimization de `HashMapMultiKey`, Matrix (checkPos, checkDim), `SiMatrice2`
  * utilisation de LRUMapMultiKey a la place de `HashMapMultiKey`
  * suppression de tous les caches autres que LRU
  * correction memory leak dans les Optimisations (il provenait de la modification    du hashCode des `MultiKey` au cours du temps, les maps ne trouvaient plus les    objet a supprimer)
  * ajout du support de la base de données h2

ver-2.2.3 (20060512)
  * correction mauvaise selection de mail pour les zones et les ports

ver-2.2.2 (20060327)
  * correction calcul de migration (N en 2D ou lieu de 1D passe en arg)

ver-2.2.1 (20060320)
  * ajout du parametre zone dans l'equation de mortalité

ver-2.2.0 (20060315)
  * mapping reproduction/recrutement avec un coefficient
  * modif traduction de 'er mois' en '{0}er mois' idem pour 'ème mois'

ver-2.1.1 (20060306)
  * On force l'encoding des fichiers en ISO-8859-1 partout
  * ajout de try{}catch lors du chargement des combo de formules, utile    lorsque la formule n'est pas correct (mauvais encodage)
  * ajout de I18n pour le wizard

ver-2.1.0 (20060303)
  * Passage de la mortalite naturelle en equation
  * Possibilite de definir les migrations en equation
  * modification du modele pour supporter ces fonctionnalités

ver-2.0.27 (20060302)
  * correction d'optimisation pour `EffortPerStrMet` (bis)    (suppression des methodes non optimisees)    (autres petites corrections pour des manques de parametres d'opti)

ver-2.0.26 (20060223)
  * correction d'optimisation pour `EffortPerStrMet`
  * support des \n pour les traductions

ver-2.0.25 (20060124)
  * ajout de chaine a traduire
  * ajout du jar client R dans le all

ver-2.0.24 (20051125)
  * correction bug dans lancement simulation    pour plusieurs fois la meme regle avec param different

ver-2.0.23 (20051123)
  * ajout de mot a traduire (arbre de saisie)
  * modification de la boucle matriceCatchRatePerStrategyMet    pour ne faire que les metiers de la strategie

ver-2.0.22 (20051117)
  * ajout pour chaque annee de simulation la memoire restant,    les stats du cache et les stats d'appels de methodes
  * correction cache, il libere les references
  * ajout d'un yield en fin de chaque methode d'optimisation    pour permettre au autre thread de travailler
  * config de ShiftOne a 20000 objet par defaut

ver-2.0.21 (20051115)
  * suppression de l'optimisation pour certain calcul, il ne faisait pas    gagner de temps et il consommait de la mémoire pour rien.
  * La fenêtre de log n'affiche plus que les 100 premières lignes de log    et les 200 dernières, cela évite de manger beaucoup de mémoire pour    les logs (la consomation semble etre stable dans les 200Mo)

ver-2.0.20 (20051114)
  * Inversion de la demande d'optimisation si false alors on la fait pas    plus sur que de detecter true
  * ajout lors du demarrage de l'affichage des variables d'environnement    et des options de lancement.

ver-2.0.19 (20051111)
  * correction des optimisations, ajout de dependance sur date.getMois()    pour toutes les methodes qui utilisent un argument date
  * amelioration de le liberation du cache

ver-2.0.18 (20051107)
  * correction des resultats pour que les captures en poids et nombre    se fasse avec les effectifs de au meme moment

ver-2.0.17 (20051102)
  * correction: La matrice créer pour le stockage des resultats n'etait pas    utilisée

ver-2.0.16 (20051025)
  * Ajout de l'option -DOptimization=[true|false] qui permet de desactiver    les optimisations totalement. (Permet de verifier que les optimisations    ne modifient pas les resultats)

ver-2.0.15 (20051025)
  * Toutes les matrices misent en résultat sont copiées.

ver-2.0.14 (20051021)
  * Correction probleme passage de matrice de changement d'age    de non spacialise a spacialise

ver-2.0.13 (20051020)
  * Correction probleme de dimension pour les pop en longueur dans la    matrice de changement d'age

ver-2.0.12 (20051013)
  * utilisation de la nouvelle implantation de matrice (val par defaut)    Si seulement 1/4 des elements de la matrice differe de la valeur par    defaut alors les performances sont meilleurs

ver-2.0.11 (20051005)
  * passage de toutes les matrices en float pour prendre 2 fois moins de    place
  * amelioration de la liberation du cache de calcul
  * ajout d'une condition pour les logs de `SiMatrice2`
  * ajout de l'exception lors de log de probleme grave dans ifremerdb.xml

ver-2.0.10 (20050923)
  * ajout du cache Hard qui est le meme qu'avant
  * suppression des l'anciennes stat des optimisations
  * ajout du support de l'option -DCallAnalyze=[true|false]
  * essai de non recalcule de Population.getMatriceZone
  * tous les logs de `SiMatrice` & co en /*#...#*/
  * modification du calcul de loopOver, on calcule sur les    metiers des `SetOfVessels` des Strategies

ver-2.0.9 (20050915)
  * ajout de 3 caches different pour l'optimisation des caculs    les caches sont selectionnable au lancement d'isis
  * ajout de la possibilité de faire les boucles Str/Met que sur    les metiers de la strategie dans `SiMatrice`. Cette fontion    est selectionnable au lancement d'isis

ver-2.0.8 (20050???)
  * selection plus fine des elements selectionnable dans une combobox

ver-2.0.7 (20050613)
  * ajout dans le `LogMonitor` des `StackTraces`
  * ajout d'une règle pour avoir les résultats économiques
  * Amélioration des matrices Eco (ajout d'un nom à la matrice et au    dimension, traitement special si pas d'effort)
  * Lors de la sauvegarde des resultats l'export n'est vraiment fait que    lorsqu'il y a au moins une méthode d'export selectionnée
  * Ajout d'un cache pour le XMLEncoderDecoder
  * On ne fait plus le Changement de classe si la `MetaPop` est en longueur

ver-2.0.6 (20050603)
  * correction bug#459, bug#460, mauvaise sauvegarde des correspondances    s'il n'y avait qu'une zone repro et une zone recru (mapping toujours    vide)
  * ajout dans lutinutil de la method utilisé par le serveur de simulation    pour supprimer recursivement un repertoire
  * ajout du numero de version de isis dans les logs au lancement

ver-2.0.5 (20050525)
  * correction du l'email d'envoi du bugreport pour le `LogMonitor`
  * ajout dans la status bar de la fenetre de log, l'utilisation memoire et    une horloge
  * correction pour les collections dans l'equation de reproduction
  * correction du modele de données pour mettre des cardinalitées 0-1, la on    il faut (corrige le probleme de creation d'objet)
  * correction de la boite de dialog a propos

ver-2.0.4 (20050518)
  * Utilisation de `SecteurSimpleFactory` pour l'affichage des Secteurs dans l'arbre
  * Modification de l'évaluation de la reproduction, utilisation de la    méthode demandée par Stéphanie et Hilaire (comme la version qui avait    été intégrée dans l'ancienne version du simulateur)
  * ajout d'une fenêtre montrant les logs directements dans l'application
  * ajout de l'attribut mapfiles dans Region
  * ajout support de l'utilisation de carte utilisateur (sur filesystem)
  * si le script de pre-simulation n'est pas selectionné il n'est pas pris    en compte
  * Message pour avertir que les données vont etre upgrade
  * Arret du logiciel si les données sont trop recente pour la version    d'isis utilisé
  * dans les equations de croissance ajout de la variable classe de type    `ClassePopulation`
  * fermeture bug #444

ver-2.0.3 (20050429)
  * Remplacement pour `Strategie->StrategyMonthInfo->numberOfTrips` du spinner    par un JLabel car il n'est pas modifiable (calculé)
  * Amélioration table de saisie du mapping repro->recru
  * Amélioration de la recherche des dépendances entre objets lors d'une    suppression.
  * fermeture bug #425, #434, #437, #438

ver-2.0.2 (20050414)
  * correction du bug de selection de maille sur la carte (fonctionne mais    pas pour les mailles tres petites)

ver-2.0.1 (20050408)
  * correction erreurs de simulation
  * correction erreurs de representation des matrices dans le rendu des résultats
  * permet de supprimer des métiers de `MetierEffortDescription` dans    `SetOfVessels`

ver-2-RC1 (20040326)
  * Secteur pour le nombre total de secteur retourne 1 et non 0, pour etre    coherent avec le getAllSecteur dans lequel il s'ajoute lui meme.
  * ajout sur Metier des methodes getValeurParamControlableAsString et    getValeurParamControlableAsNumber
  * modification de l'interface de Selectivite dans Engin, pour mettre une    modification plus simple des equations
  * modification de toutes l'interface de lancement des simulations
  * amelioration de l'interface de creation de script d'enchainement de    simulation. Si aucun editeur n'existe alors ne propose que les scripts
  * dans le tableau de population les longueurs min et max sont affichées si    les classes sont en longueur
  * correction d'une erreur lors de la creation de la matrice de changement    de classe pour une population en longueur n'ayant qu'un secteur de pop
  * amelioration du fichier java web start pour permettre d'avoir l'aide    avec le javahelp
  * amelioration du jar standalone pour avoir l'aide dedans
