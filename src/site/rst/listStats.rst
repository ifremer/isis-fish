.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
Statistiques des listes de diffusion
====================================

Liste users nombre de mail
--------------------------

 * 2003: 0
 * 2004: 0
 * 2005: 850
 * 2006: 755
 * 2007: 1185
 * 2008: 1035
 * 2009: 1183

Total pour la liste users: 5008

Liste devel nombre de mail
--------------------------

 * 2003: 12
 * 2004: 92
 * 2005: 848
 * 2006: 524
 * 2007: 652
 * 2008: 344
 * 2009: 613

Total pour la liste devel: 3085

Liste bugreport nombre de mail
------------------------------

 * 2003: 0
 * 2004: 0
 * 2005: 90
 * 2006: 54
 * 2007: 102
 * 2008: 52
 * 2009: 626

Total pour la liste bugreport: 924

Liste commits nombre de mail
----------------------------

 * 2003: 0
 * 2004: 0
 * 2005: 0
 * 2006: 0
 * 2007: 2
 * 2008: 956
 * 2009: 1702

Total pour la liste commits: 2660

Liste data-commits nombre de mail
---------------------------------

 * 2003: 0
 * 2004: 0
 * 2005: 0
 * 2006: 0
 * 2007: 16
 * 2008: 92
 * 2009: 224

Total pour la liste data-commits: 332

Total par année
---------------

 * 2003: 12
 * 2004: 92
 * 2005: 1788
 * 2006: 1333
 * 2007: 1957
 * 2008: 2479
 * 2009: 4348

Total : 12009
