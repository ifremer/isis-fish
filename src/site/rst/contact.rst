.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
Contacts/Assistance
===================

ISIS-Fish est un développement soutenu par l'Ifremer mais à pour but d'être le
plus communautaire possible. Pour cela de nombreux outils ont été mis en place
pour permettre de mieux communiquer et que cette communauté prenne une réelle
existance.

Liste de diffusion
------------------

Différentes listes de diffusion ont été mises en place. Chacune d'elle à un but
précis.

** Si vous avez un soucis lors de l'utilisation d'ISIS-Fish nous préférons que
vous utilisiez ces listes plutôt que de nous envoyer des e-mails directement.
Car votre question et la réponse qui lui est apportée peuvent intéresser
d'autres personnes, et surtout de nombreuses personnes de ces listes peuvent
vous aider.**

Même si la langue la plus parlée sur ces listes est le français, **vous pouvez
bien-sûr envoyer vos e-mails en anglais**.

`Statistiques d utilisation des listes`_

.. _Statistiques d utilisation des listes:: listStats.html

Les listes pour les utilisateurs
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 * `isis-fish-users@list.isis-fish.org <http://list.isis-fish.org/cgi-bin/mailman/listinfo/isis-fish-users>`_
   : Liste principale pour les utilisateurs d'ISIS-Fish (page d'inscription).
 * `Archives de la liste utilisateurs <http://list.isis-fish.org/pipermail/isis-fish-users/>`_
 * `isis-fish-data-commits@list.isis-fish.org <http://list.isis-fish.org/cgi-bin/mailman/listinfo/isis-fish-data-commits>`_
   (lecture seule) : Liste recevant toutes les notifications de modification du
   code source des scripts d'ISIS-Fish (page d'inscription).

Les listes pour les développeurs
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 * `isis-fish-devel@list.isis-fish.org <http://list.isis-fish.org/cgi-bin/mailman/listinfo/isis-fish-devel>`_
   : Liste pour les concepteurs du logiciel (page d'inscription).
 * `isis-fish-bugreport@list.isis-fish.org <http://list.isis-fish.org/cgi-bin/mailman/listinfo/isis-fish-bugreport>`_
   (lecture seule): Liste recevant tous les ajouts dans l'outil de report de
   bugs (page d'inscription).
 * `isis-fish-commits@list.isis-fish.org <http://list.isis-fish.org/cgi-bin/mailman/listinfo/isis-fish-commits>`_
   (lecture seule): Liste recevant toutes les notifications de modification du
   code source d'ISIS-Fish (page d'inscription).

Outils de suivis
----------------

Les outils de suivis permettent de garder les traces de demandes et de voir leur
évolution dans le temps.

Pour éviter le spam dans ces outils, vous devrez vous
`créer un compte sur le labs <http://labs.libre-entreprise.org/account/register.php>`_
avant de soumettre quelque chose. Cette inscription ne vous engage absolument à
rien.

 * `Report de bugs <http://labs.libre-entreprise.org/tracker?atid=113&group_id=8&func=browse>`_
   : Il sert à la notification d'un problème lors de l'utilisation d'ISIS-Fish.
 * `Demande d'amélioration <http://labs.libre-entreprise.org/tracker/?atid=116&group_id=8&func=browse>`_
   : Il sert à ajouter des idées d'amélioration ISIS-Fish.
 * `Report de problèmes de traduction <http://labs.libre-entreprise.org/tracker/?atid=438&group_id=8&func=browse>`_
   : Il sert pour reporter les erreurs de traduction.
