.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
=====================
Télécharger ISIS-Fish
=====================

ISIS-Fish (Version Java)
------------------------

La version Java d'ISIS-Fish est la seule supportée sur ce site. Vous pouvez
télécharger la dernière version (ou les anciennes) à partir d'ici.

Telechargement__ (dernière version : Avril 2010)

__ http://isis-fish.labs.libre-entreprise.org/download/version3/


Base Golfe de Gascogne
~~~~~~~~~~~~~~~~~~~~~~

Voici une base de démonstration utilisable sur la version 3.3.0.3 d'ISIS-Fish.

`Base Golfe de Gascogne`_

.. _Base Golfe de Gascogne::downloads/GolfeDeGascogneDemoV3.3.0.3.zip


ISIS-Fish (Version R)
---------------------

La version R d'ISIS-Fish nommée FLISIS et faisant partie du projet FLR n'est pas
supportée sur ce site. Vous pouvez néanmoins la télécharger à partir d'ici.

Telechargement__ (dernière version : Janvier 2009)

__ downloads/FLIsis_1.1.2.zip

`Site du projet`_

.. _Site du projet::http://flr-project.org/wiki/doku.php?id=applications:isisflr
