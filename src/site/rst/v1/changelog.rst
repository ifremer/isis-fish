.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
Version 1
=========

Cette version n'est plus maintenue depuis 2004

Téléchargement
--------------

Cette version n'est plus disponible en téléchargement.

Information sur les changement de cette version
-----------------------------------------------

ver-1-5RC2 (20040202)
  * bouton radio (spacialise/non spacialise) a la place de la case a coche
    matrice simple.
  * Changement de texte: Initialiser la matrice -> calcul des coefficients
  * message pour prevenir que la matrice va etre perdu si on passe de
    spacialise a non spacialise.
  * changement de texte: save as... -> sauver comme modele
  * bouton supplementaire qui ouvre la visulisation de la matrice en forme
    spacialise si on est en forme simple.
  * dans editeur de formule modifier le nom de l'equation avec le type de
    fonction
  * ajout d'un split dans plan d'analyse
  * amelioration de la verification de l'utilisation de la base par une autre
    application
  * correction des erreurs lors de la creation d'objet qui contiennent des
    equations (qui sont donc vide au depart)
  * debug de la demande de sauvegarde lors de la modification de l'age mature
    dans Pop
  * correction d'un bug lors de l'utilisation d'un DBManager.clone, qui
    faisait que les données n'était pas sauvegardé par la suite.
  * refactoring (factorisation) du code des noeuds de l'arbre
  * amelioration de l'envoie des events lorsque l'on modifie un objet de la
    base pour que les listeners de la factory ancetre soit aussi prevenu
  * correction du probleme dans l'arbre lors de la creation de zones, l'arbre
    n'etait pas mis a jour
  * correction du probleme dans l'arbre lors de l'ajout d'une pop, elle
    etait ajoutée dans toutes les meta-pops
  * reageancement de beaucoup d'interface de saisie
  * probleme lors de la suppression d'une pop -> exception
  * probleme lors de la suppression d'une zone, on demande a sauver la zone
    alors qu'on l'efface :(
  * probleme lors de la suppression d'un metier -> exception et demande de
    sauvegarde
  * amilioration du spport des lien 0-1, si l'objet du lien disparait
  * correction d'un bug dans la recherche de l'existance d'un objet par son id
    (on ne recherchait pas dans les sous classe)
  * Creation d'un arbre pour l'enchainement des simulations
  * refactoring du code existant pour l'enchainement des simulations
  * modification des interfaces de generation des classes
  * les fonctions de croissance et d'inverseCroissance prenne des ages en mois
    et retourne des ages en mois
  * correction d'un bug dans l'upgrade McKoi qui ne se produissait que sous
    windows 

ver-1-5RC1 (20040122)
  * Dans l'édition de la config choix de l'affichage des tooltips
  * correction du bug qui sauvegardait des saisons chevauchées
  * lorsque l'on change les dimensions de la saison, la taille de la matrice
    change lors de la sauvegarde)
  * pour la saison: dans le tableau de reproduction, les titres sont les
    mois de la saison
  * pour recrutement: dans le tableau mettre les titres sont
    1er mois, 2eme mois, 3eme mois, ...
  * dans immigration: secteur d'arrivé ajout d'un apostrophe
  * dans engin: cahngement du texte
    Paramètre de Gamme possible -> valeurs possibles du paramètre
  * dans metier: correction bug avec le type du paramètre controlable
  * dans engin: suppression Param Continu et Quantifiable.
  * Pour les valeurs continues le paramètre accepte les - (5-12)
    pour les autres des ; (12;15;17;90)
  * dans strategie: suppression du bouton 'Terminer'
  * dans pop: passage des ages en valeur entiere et non en float (2.0 -> 2)
  * ajout d'un link entre region et secteur
  * ajout de l'upgrade automatique des données de la base
  * ajout de l'upgrade automatique des données xml
  * ajout de l'upgrade automatique de mckoi
  * Simplifier le DBtoXML et XMLtoDB
  * plus besoin de faire un unquote apres la lecture XML
  * correction du probleme dans le XML avec les relation n
  * seul les secteurs simples sont visible dans la liste des zones de
    la region
  * suppression des méthodes d'export des factories, on utilise maintenant
    tout le temps celle utilisant DBDescription
  * support pour l'upgrade automatique des résulats
  * suppression de Cle/taille age, maintenant qu'il y a inverse croissance
  * ajout d'un lien de MetaPop vers Pop
  * ajout d'un methode PopulationFactory.findAllByRegion
  * modification de toutes les utilisations de methodes depreciees
  * ajout support des commentaires dans les equations
  * refactoring de la gestion des equations (les equations sont directement
    conserve dans les objets)
  * les équations sont maintenant conservées sont forme XML dans la base
  * copyright plus dans l'image mais ecrit apres
  * ajout du support de Ln dans les equations
  * ajout d'équation prédéfinies
  * ajout d'un lien de region vers (Maille, MetaPopulation, Strategie)
  * creation de classe pour la verification de la coherence des infos de la
    base
  * ajout d'un bouton permettant de verifier si la saisie d'une region
    contient des erreurs
  * fenetre de modification/suppression des formules
  * correction du bug lors de la sélection d'une ancien simulation les
    effectifs restait sur les anciens
  * les resultats de la verification est visualisé dans une table
  * ajout du tri sur les resultats de la verification
  * correction bug de lancement et sauvegarde de simulation du a l'auto
    upgrade de la base
  * Amélioration de la présentation de la liste des objects à effacer, lors
    de l'effacement d'un objet
  * remplacement de Classepopulation.longueur en longeurMin et longueurMax
  * remplisage de la matrice de changement de classe pour le modele en longueur.
  * ajout des dernieres equations
  * correction du bug dans MatriceChage qui ne tennait pas compte du group
    plus

ver-1-2 (20031017)
  * version modifier pour le contrat N 03/5 210 136
  * Classe en longueur
  * Script pre et post simulation
  * Interface de script d'enchainement de simulation
  * Export automatique en fin de simulation
  * correction de bug dans la suppression de méthode dans l'ecmascript
  * mise à jour des méthodes d'export

ver-1-1
  * La première version dite stable. Cette version à été livré en 2002.
