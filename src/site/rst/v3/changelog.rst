.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
ver-3.0.22 poussin 200711??
  * bug correction when launch many simulation with AnalysePlan (OutOfMemory) 
    now we don't use cglib but javassist

ver-3.0.21 poussin 20071107
  * bug correction in ResultDatastore, bad database used to get result during simulation
  * add call to close method on SimulationStorage for in memory database
  * bug correction when launch many simulation with AnalysePlan (OutOfMemory)

ver-3.0.20 poussin 20071102
  * add clean temp directory simulation preparation
  * add IsisFish version number in parameter
  * add getPopulations method in PopulationMonitor
  * bug correction #1592 in totalFishingMortality during reduction of matrix
  * bug correction #1583 about too many file open
  * bug in export script template
  * bug correction in rule Cantonnement
  * modify database simulation usage, now we use in memory database for
    data, and file storage for result, to separate result and data. This
    improve performance (70%).
  * bug switch to h2 database version 1.0.60, this prevent rollback exception
    during simulation

ver-3.0.19 poussin 200706??
  * bug in cache for String and number in parameter
  * bug RuleMonitor add Rule as parameter
  * add extraRules field in SimulationParameter to permit Analyse Plan to
    add rules in parameter

ver-3.0.18 poussin 20070525
  * bug go.bat have correct DOS end of line
  * improve build-release.sh to send email to user and devel list after deploy
  * bug correction in delete simulation, remove close context at begin of
    clear method
  * bug permit simulation without SimulationControl
  * bug in queue model test if no more simulation to prevent Index Out of               
    bound Exception

ver-3.0.17 poussin 20070524
  * add support for filename in export

ver-3.0.16 poussin 20070521
  * add ssj jar to have random library
  * add support to auto upgrade database (topia migration service)
  * bug force reload parameter in thread simulation to prevent class cast
    exception because same class is loaded in two different classloader

ver-3.0.15 poussin 20070406
  * change database lock_mode to permit read with out lock
  * change database version to 1.0.20070304
  * bug end line in equation editor 

ver-3.0.14 poussin 20070402
  * add beforeOrEquals and afterOrEquals methods to Date and Month

ver-3.0.13 poussin 20070330
  * add simulation information support
  * change aspect deployment classloader (not used Agent)
  * bug in cache aspect when used without trace aspect
  * add checkout maven file option
  * change MatrixPanel context menu
  * bug in datastore closeContext (nullify storage)
  * bug in datastore getStorage (if closed create new)

ver-3.0.12 poussin 20070320
  * bug in cache, help garbage with clear on collection
  * add on matrix sumOverDim(dim, start, nb)
  * change statistic is not used by default

ver-3.0.11 poussin 200703??
  * add result support in analyse plan
  * bug in AnalysePlanContext values access

ver-3.0.10 poussin 20060305
  * Analyse Plan implementation
  * feature #1531 date automaticaly added to simulation id
  * bug in Range value inversion of integer and real
  * bug #1492 matrix index error for result matrix
  * bug #1493 simulation, region deletion
  * bug #1495 view population number with one population
  * bug #1496 simulation, region order
  * bug #1528 in min size in wizard class creation
  * bug #1535 save/cancel button activation/desactivation
  * bug #1536 simulation queue

ver-3.0.9 poussin 20060208
  * add equation editor with syntaxe checking on all equation
  * implement Region checking mecanisme

ver-3.0.8 poussin 20060205
  * force checkout of directory not checkouted at startup

ver-3.0.7 poussin 20060125
  * add import region and rename menu
  * bug in update script

ver-3.0.6 poussin 20060125
  * add support for user prompt update file at startup
  * bug saisons (bad converter init)
  * simulation thread completely rewriten
  * first version of systray 
  * region copy
  * bug tree refresh after delete region
  * cvs synchronisation menu
  * test de non regression
  * bug in exports
  * bug in rules

ver-3.0.5 poussin 20061020
  * add support for delete prompt message and delete cascade prompt message

ver-3.0.4 (poussin 20061017)
  * improve cache key computation (use string), (gain 80%)

ver-3.0.3 (poussin 20061016)
  * bug in getMonth if date is negative
  * bug in message error during equation compilation, now we show the
    equation type

ver-3.0.2 (poussin 20061013)
  * add import from isis-fish v2
  * change rules from region to root
  * improve equation frame editor (equation documentation)

ver-3.0.1 (poussin 20060926)
  * bug selection cell (selected in list -> note selected in map)
  * add extension to file for model equation
  * improve refresh in input
  * add argument force to ResultStorage.addResult method
  * bug put new Population in right Species when more than one Species (tree problem)
  * bug put new Rule script in right region (tree problem)
  * add firstNull='true' for gear values types
  * bug when load gear with no values (null)
  * change method call: Input.CommitRegionInCVS -> Input.commitRegionInCVS
  * add Equation model editor
  * bug in script editor, don't used translated String for script type
  * bug in script editor, menu save/delete/deleteCVS work now
  * change use for(int i...) in GravityModel and SiMatrix for matrix access
    (gain factor 4 on matrix access and 65% on simulation)
  * change use Soft cache in ResultStorage (gain 10%)
  * improve ResultStorage keep in memory all result available to prevent unecessary
    query on database. Useful with Soft cache.
  * change use commons-collection in Cache aspect (prevent garbage bug in HashMapMultiKey)
  * bug in tools.jar search pattern for Windows
  * bug when create SetOfVessels in Effort description Add button
    was always grey
  * add getNecessaryResult in Rule to know result necessary for this rule
    and activate it automaticaly
  * improve all results are now optionnal
  * add compute and add as result discard weight if necessary
  * add TACweight in DemoRegion
  * add result MATRIX_NO_ACTIVITY
  * bug can set rule parameter
  * bug rule parameter can be saved and restored
  * improve when TopiaContext is used to read, error if another try to write
    (lock table timeout).
  * bug many correction in persistence (lock problem, load cycle problem)

ver-3.0.0
  * Utililsation de DoubleBigVector par defaut pour les Matrix
    (gain facteur entre 2 et 8 suivant parcours (Iterator, Semantic, Index)
  * Suppression de tous les scripts utilisation de Java et compilation
    (gain facteur 1000 pour l'eval des equations donc 91% sur la simulation)
