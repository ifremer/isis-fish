.. -
.. * #%L
.. * IsisFish
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2013 Ifremer, Codelutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
Mexico
------

Isis essaye de respecter le format mexico pour le stockage des plans d'expérience.

Le schema xsd est disponible à l'adresse suivante:
https://mulcyber.toulouse.inra.fr/scm/viewvc.php/trunk/XML/schemas/expDesign.xsd?revision=9&root=baomexico&view=markup
ou
http://www.reseau-mexico.fr/sites/reseau-mexico.fr/files/expDesign.xsd
