.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
Installation d'Isis sur clé USB
===============================

Procedure
---------

* Dézipper isis-fish-w.x.y.z-bin.zip
* Faire un checkout anonyme du tag correspondant dans le dosssier dezippé
* Ajouter les base de données
* Ajouter les simulations de référence
* Ajouter les regles specifiques
* Ajouter les export spécifiques
* Ajouter les fichiers de paramètres des règles

Configuration
-------------

Le fichier de configuration doit être placé dans le répertoire de lancemenent
et avoir son nom par défaut pour que ApplicationConfig le charge dans
le dossier courant.

Cela fonctionne également pour les simulations en sous processus car elle
trouve également ce fichier.

Il doit contenir (3.3.y.z) ::

  database.directory=isis-database-3
  user.name=mexicouser
  vcs.protocol=svn
  vcs.username=anonymous
  compilation.directory=isis-build
  monitoring.directory=isis-monitoring
