.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
Syntaxe des arguments d'une option
==================================

Le but de ce document est de formaliser la définition d'une option
dans une ligne de commande pour automatiser le mode batch.

La forme générale de la définition
----------------------------------

La définition d'une option est de la forme suivante :

-alias0|-alias1|...|-aliasN [argumentsDefinition]

Les alias commence naturellement par '-' ou '--'.

Les différents groupes d'arguments
----------------------------------

On définit deux types de groupements d'arguments possibles pour une option :

# groupement d'arguments facultatifs balisé par [ ]
# groupement d'arguments obligatoires balisé par < >

Les arguments à l'intérieur d'un groupement sont séparés par des |, cela veut
dire que pour un groupement donné, un seul de ses arguments doit être trouvé
dans une ligne de commande.

On peut écrire plusieurs groupes d'arguments obligatoires; par contre on
limite à  un unique groupe d'arguments facultatif et par convention, on le
place en dernier (même si le placer ailleurs fonctionne aussi).

Exemple :

-o '<arg0|arg00> <arg1> [arg2|arg22]' 

Les différents types d'arguments d'une option
---------------------------------------------

On propose trois types d'arguments possibles :

# les arguments dit constants : il s'agit d'une chaine imposée, il est
représenté simplement par sa valeur (implantation ConstantOptionArgument).

# les arguments dit valués : il s'agit d'arguments valuables, ils sont
représentés de la sorte :  'key:type' (implantation ValuedOptionArgument).
Dans une ligne de commande, un tel argument est représenté uniquement par sa
valeur. Il faudra bien s'assurer de la non collision des types sur ce type
d'arguments (voir plus bas).

# les arguments dit valués et nommés : il s'agit d'arguments valuables, et
possèdant un clef de nommage ils sont représentés de la sorte :  'key=type'
(implantation NamedValuedOptionArgument).

Les arguments valués et nommés doivent être écrits sous la forme key=value
dans les arguments d'une option.

Les différents typages possibles sont les suivants :

# boolean
# integer (un long est utilisé)
# float (un double est utilisé)
# class
# file (pour un fichier existant)
# newfile (pour un fichier potentiellement non existant)
# pattern ?
# string

Une contrainte doit cependant être posée sur les deux derniers typages afin
de s'assurer de toujours pouvoir retrouver le type d'un argument.

En effet il n'est pas possible de différencier un string d'un fichier non
existant, on pose alors comme contrainte de ne pas accepter simultannement
dans le même groupement d'argument, un argument valued (non nommé) de type
newfile et string

Exemple :

avec la définition suivante -o '<arg0|arg1=string> [arg2|arg3:boolean]'

sont valides les options :

# -o arg0 arg2
# -o arg1=valeur
# -o arg1=valeur true 

Par contre les options suivantes ne sont pas valides :

# -o arg0 arg0 (duplication argument obligatoire)
# -o arg1      (arg1 n'est pas un argument constante)
# -o arg0 value (value ne correspond à auncun argument possible)

Cardinalité et ordre des arguments d'une option
-----------------------------------------------

On impose un ordre sur les arguments obligatoires, alors que les arguments
facultatifs ne sont pas soumis à un ordre précis.

De plus les arguments obligatoires ont une cardinalité fixe de 1 : une option
obligatoire ne peut apparaître q'une seule fois. (le système autorise de changer
la cardinalité mais on n'en tient pas compte pour le moment dans le parseur).

Tous les arguments peuvent avoir une cardinalité, pour
représenter cette cardinalité, on utilisera les notations classiques :

arg0+ pour une cardinalité multiple, sinon arg0 pour une seule occurrence
possible.

arg{n,m} pour un cardinalité d'au moins n et d'au plus m. Avec un cas
particulier, si m=-1, pas de limite maximum.

arg{n} pour un cardinalité d'extactement n.

Exemple :

-o '<arg0|arg00> <arg1> [arg2|arg22+]' 

-o '<arg0|arg00> <arg1> [arg2{3}|arg22{0,1}]'

Algorithme de parsing des options d'une ligne de commande
=========================================================

On doit modifier l'algorithme précédemment utilisé pour intégrer cette nouvelle
syntaxe.

On procède désormais ainsi :

# premier parcours de tous les arguments de la ligne de commande afin de
construire des contextes d'options : contenant l'option requise et les arguments
récupérés de la ligne de commande qui lui appartiennent théoriquement (aucune
validation n'est faite à ce niveau).

# pour chaque context trouvé, on recherche les arguments de l'option encapsulée
qui sont en adéquation avec les arguments récupérés de la ligne de commande.
Pour chaque argument qui coïncide avec la définition de l'option, on
transforme la valeur brute de l'argument en une valeur typée selon le type
d'argument d'option présent.

# Les contextes sont traités dans leur ordre d'arrivée, puisque l'on doit
respecter l'ordre imposé par les arguments obligatoires de la définition de
l'option.

# Ensuite on valide chaque argument de chaque option trouvée :
 la cardinalité pour les arguments (facultatif?)
 la présence d'exactement un argument valide obligatoire par groupement
 d'arguments obligatoires.

# Efin on valide toutes les options (comme avant) sur leurs cardinalités imposées.


Les options d'IsisFish formalisées
==================================

# --list|-l '<analyseplans|exports|formules|regions|rules|scripts|simulations|simulators> [filter:string]'

--list|-l 'affiche la liste d'un certain type d'objects Isis (analyseplans|
 exports|formules|regions|rules|scripts|simulations|simulators) avec possibilité
 d'appliquer un filtre (filter)'

# --add|-a '<analyseplans|exports|rules|scripts|simulators> <file:file>'

--add|-a 'ajoute un scripts d'un certain type (analyseplans|exports|rules|
scripts|simulators) à partir d'un fichier existant (file)'

# --add|-a '<formules=string> <file:file>'

--add|-a 'ajoute une formule d'un certain type (formule=xxx) à partir d'un
fichier existant (file)'

# --import|-i '<fileZip:file>'

--import|-i 'importe un fichier existant (fileZip) précédemment exporté d'Isis '

# --export|-o '<fileZip:newfile> <region|simulation> <name:string>'

--export|-o 'export dans un nouveau fichier (fileZip) une region ou une
simulation selon son nom (name)'

# --export|-o '<fileZip:newfile> <analyseplan|export|formule=string|rule|script|simulator> <name:string|*>'

--export|-o 'export dans un nouveau fichier (fileZip) des scripts d'un certain
 (analyseplan|export|rule|script|simulator) avec possibilité d'effectuer un
 filtre (name) ou pas (*)'

# --export|-o '<fileZip:newfile> <formule=string> <name:string|*>'

--export|-o 'export dans un nouveau fichier (fileZip) des formules d'un certain type
de forumle (formule=xxx) avec possibilité d'effectuer un filtre (name) ou
  pas (*)'

# --vcs '<update|commit|state> [file:file+]'

--vcs 'effectue une opération de communication avec le serveur distant (update|commit|state)
avec possibilité de spécifier les fichiers cibles existants (file)

# --vcs '<add|remove> <file:file+>

--vcs 'ajoute ou supprime du répository local un ensemble de fichiers existants (file)'

# --simulate '<simulation-id:string> <simulation-parameter:file>'

--simulate 'lance une simulation à partir de son nom  (simulation-id) et en
 spécifiant le fichier de paramètres de simulation existant (file)'

TODO a finir
mavenFile(--mavenFile)	Checkout maven file from server (pom.xml)
importAndRenameRegion(--importAndRenameRegion)	import region from zip v3 file format and rename it [--import '<zip file>' '<new name>']
update(--update, -u)	not try to update local repository [true|false]
importSimulation(--importSimulation)	import simulation from zip v3 file format or from xml v3 format [--import '<zip file>']
config(--config)	change one config option [optionKey optionValue]
listRegion(--listRegion)	list all region available localy
exportSimulation(--exportSimulation)	export simulation in zip file format [--export '<simulation name>' '<zip file>']
create-ssh-key(--create-ssh-key, -c)	create key paire for cvs ssh access
importRegion(--importRegion, -i)	import region from zip v3 file format or from xml v2 format [--import '<zip file | v2.xml | v2.xml.gz>']
help(--help, -h)	Show this help
exportRegion(--exportRegion, -e)	export region in zip file format [--export '<region name>' '<zip file>']
listSimulation(--listSimulation, -l)	list all simulation available localy
resetConfig(--resetConfig)	Reset user configuration
ui(--ui)	not launch user interface [true|false]
ssh-key-file(--ssh-key-file, -k)	change private ssh key file path
