.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
Option de lancement
===================

Le but est de pouvoir faire les actions les plus courantes en ligne de
commande, comme lancer une simulation, exporter/importer une région,
exporter/importer une simulation, exporter/importer des scripts, mise à jour
du repository local, envoi des modifications du repository local, ...

Liste d'élément disponible
--------------------------

--list <analyseplans|exports|formules|regions|rules|scripts|simulations|simulators> [filter pattern]

Retourne la liste avec un élément par ligne

Ajout de fichiers de type script
--------------------------------

--add <analyseplans|exports|formules <type>|rules|scripts|simulators> <file>

Import de fichiers dans le repository local
-------------------------------------------

--import <file.zip> TODO

Le fichier doit avoir été exporté par isis-fish et donc respecter une
certaine norme (arborescence).

Export de fichiers depuis le repository local
---------------------------------------------

--export <file.zip> <region|simulation> <name>
--export <file.zip> <analyseplan|export|formule|rule|script|simulator> <name|'*'>

Export l'élément ayant le nom 'name' dans le fichier 'file'.
Il est possible de mettre à la suite plusieurs couple (type, name) pour
qu'ils soient tous mis dans le fichier zip.

'*' indique de mettre tous les éléments de ce type.

Pour les formules il faut mettre devant le nom le type de la formule, par
exemple: 'Growth/TheGrowth' ou 'NaturalDeathRate/*'

Action sur le repository local
------------------------------

--vcs <update|commit|state> [file [file [...]]]
--vcs <add|remove> file [file [...]]

add et remove font automatiquement un commit ensuite

State montre l'état du fichier, modifier, nouveau, ...

Lancement de simulation
-----------------------

--simulate <simulation id> --simulation-parameter <properties file> --
