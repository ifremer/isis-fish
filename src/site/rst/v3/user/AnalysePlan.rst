.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
==============
Plan d'analyse
==============

Les plans d'analyses servent à enchaîner plusieurs simulations. Chaque
simulation peut-être paramétrée en fonction des résultats des
anciennes simulations ou de valeurs aléatoires.

Il est possible de composé un plan d'analyse à partir de plusieurs
plans.  Par exemple créer sont propre plan et utiliser aussi le plan
*Max* qui permet de fixer le nombre maximum de simulation du plan.

Principe général
================

Le plan d'analyse est une classe Java contenant quelques méthodes:

- getDescription qui permet de décrire le plan
- init qui est appelé une seul fois pour toutes les simulations
- beforeSimulation qui est appelé avant chaque simulation
- afterSimulation qui est appelé après chaque simulation

La méthode init permet de modifier des paramètres pour toutes les
simulations ou de fixer des valeurs dans le contexte de
plan. Potentiellement il est aussi possible de faire apparaître de
boites de dialogues pour demander des informations supplémentaire à
l'utilisateur.

La méthode *beforeSimulation* permet de modifier les paramètres de la simulation ou
les données de la prochaine simulation. Elle prend en paramètre
*AnalysePlanContext* et *SimulationStorage*. Pour avoir plus
d'information sur ces deux objets reportez-vous à la Javadoc. La
méthode next retourne un booléen. Si la méthode retourne faux cela
indique que le plan doit se finir et que la prochaine simulation
n'aura pas lieu, et les autres méthodes before des plans ne seront
pas appelées.

La méthode *afterSimulation* est appelé après la simulation. Si l'un
des plans retourne false lors de l'appel à cette méthode alors la
prochaine simulation ne sera pas faite, mais les autres méthodes
after des plans seront tout de même appelé.

Il est possible de définir des attributs dans la classe qui seront
automatiquement paramètrable par l'interface de sélection des plans
d'analyses. Ces attributs doivent commencer par le préfix **param_**
et être soit de type primitif (String, int, double, ...), soit de type
Date ou Mois, soit de type entité (Metier, Stratégies, ...).

Choix d'implantation
====================

Il a été choisi d'offrir deux méthodes (before, after) dans les plans
d'analyses et non pas de permettre au plan d'analyse d'appeler eux même des
simulation, car si un plan d'analyse est lancé en queue de simulation et que
l'on exécute ensuite une simulation prioritaire, on souhaite que le plan
d'analyse laisse cette simulation prioritaire se faire au milieu du plan. Or
si le plan lançait lui même les simulations on ne pourrait pas reprendre la
main pour cela.

Exemple
=======

Plan d'analyse qui permet de fixer un nombre maximal de simulation::

  public class Max implements AnalysePlan {
    public int param_max = 10;

    public String getDescription() throws Exception {
        return _("Permit to specify maximum simulation numbers");
    }
    public void init(AnalysePlanContext context) throws Exception {
    }
    public boolean next(AnalysePlanContext context, SimulationStorage nextSimulation) throws Exception {
        boolean result = context.getNumber() < param_max;
        return result;
    }
  }

Il est possible de coupler ce plan avec un plan qui par exemple
modifirait aléatoirement et indéfiniment une variable de la base.

Modification des paramètres pour la simulation suivante
=======================================================

Pour modifier les paramètres il faut les récupérer avec la méthode
*nextSimulation.getParameter()* ce qui retourne un objet de type
*SimulationParameter*. Pour plus de documentation sur cet objet
reportez-vous à la javadoc.

Récupération d'une simulation précédente
========================================

exemple::

  // la dernière simulation
  SimulationStorage lastSim = context.getLastSimulation(); 
  // ou aussi 
  SimulationStorage lastSim = context.getSimulation(context.getNumber() - 1);
  // la première simulation
  SimulationStorage firstSim = context.getSimulation(0);

Récupération des résultats d'une simulation
===========================================

exemple::

  ResultStorage results = sim.getResultStorage();
  MatrixND n = results.getMatrix(ResultName.MATRIX_ABUNDANCE);

Modification de la base de données pour la prochaine simulation
===============================================================

exemple::

  TopiaContext db = nextSimulation.getStorage();
  MetierDAO dao = IsisFishDAOHelper.getMetierDAO(db);
  Metier metier = dao.findByName("Mon metier");
  metier.setGearParameterValue("30");

Exemple de méthode next qui modifie le paramètre de l'engin
===========================================================

::

    public boolean next(AnalysePlanContext context, SimulationStorage nextSimulation) throws Exception {
      String [] values = new String[]{"10", "20", "30"};
      int number = context.getNumber();

      if (number < values.length) {
        TopiaContext db = nextSimulation.getStorage().beginTransaction();
        MetierDAO dao = IsisFishDAOHelper.getMetierDAO(db);
        Metier metier = dao.findByName("Mon metier");
        metier.setGearParameterValue(values[number]);
        db.commitTransaction();
        db.closeContext();
        return true;
      } else {
        return false;
      }
    }


Dans cette exemple nous ferons trois simulation en modifiant la valeur
du paramètre controlable de l'engin pour le métier *Mon metier*. Une
fois ces trois simulations faites le plan s'arrêtera.
