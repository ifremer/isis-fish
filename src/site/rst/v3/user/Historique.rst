.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
Historique du projet ISIS-Fish
==============================

Prototype 1998-2000
-------------------

Un prototype du logiciel ISIS-Fish a été développé entre 1998 et 2000
d'après le modèle conceptuel élaboré par Pelletier et al. (2001)

Version 1.0 et 1.5 (2000-2003)
------------------------------

ISIS-Fish 1.0 est une boite à outil générique spatialement explicite
pour l'évaluation de l'impact de mesure de gestion sur la dynamique
des pêcheries (Mahévas et Pelletier 2004, Version 1.0 et Pelletier et
Mahévas 2005, Version 1.5). Le logiciel permet d'évaluer l'impact de
mesures de gestion sur la dynamique de pêcheries complexes. Le modèles
de simulation est générique afin de pouvoir être utilisé dans
différents types de pêcheries. Les connaissances existantes au sujet
de chaque pêcheries sont stockées dans une base de données inclue dans
le logiciel, et peut être facilement modifiable. Cela inclue les
paramètres de description de chaque population, de chaque activité de
pêche, des mesures de gestion ainsi que des résultats de la dynamique
des flottilles. Le modèle d'exploitation est spatialement explicite et
couple la dynamique de chaque population, chaque flottille et chaque
mesure de gestion. Le modèle est définit avec un pas de temps
mensuel. Les variations spatiales et saisonnière de la capturabilité,
les migration saisonnières, les processus de reproduction et de
recrutement sont les principales entités du modèle de dynamique de
population structurée en age.

Les interactions biologiques ne sont pas modélisées dans ISIS-Fish
dans la mesure où le modèle se focalise plus sur des problèmes
relatifs aux pêcheries complexes. Le modèle de dynamique de flottille
décrit la réponse des pêcheurs à la disponibilité des ressources et
aux mesures de gestion introduites pour réguler l'activité de pêche.

Le logiciel permet une certaine flexibilité pour plusieurs hypothèses
du modèle. Ainsi les mesures de gestion ainsi que le comportement des
pêcheurs à ces mesures peuvent être codés à travers d'un langage
Scipt. L'outil de simulation permet ainsi de comparer l'impact de
mesures de gestion conventionnelles comme le contrôle des captures et
de l'effort, et d'autres mesures telles que les Aires Marines
Protégées (AMP).

Les versions 1.0 et 1.5 du logiciel n'incluent pas pas de variables
économiques. Il n'est donc par conséquence pas possible de définir de
réaction des pêcheurs en fonction de certaines conditions économiques.

Pour de plus ample détails sur le logiciel, vous pouvez vous reporter
aux articles Mahévas et Pelletier (2004) et Pelletier et Mahévas
(2005). Le premier détaille les différents aspects du modèle et les
choix de développement du logiciel pour la version 1.0. le second se
focalise plus sur les capacités du logiciel à évaluer les effets
d'AMPs, basé sur une revue des différents modèle de simulation
existants et la version 1.5 d'ISIS-Fish.

Techniquement cette version était basé sur des EJBs et une base de
données distantes ce qui impliquait une utilisation connectée.

Version 2.0 (2004-2005)
-----------------------

La seconde version d'ISIS-Fish (ISIS-Fish 2.0) complète les versions
précédentes en y ajoutant des variables économiques et des processus
qui déterminent la dynamique des flottilles. Cela a conduit à
redéfinir les différents composants des flottilles (Figure 1). Les
bateaux sont caractérisés par leurs capacités techniques afin de
prendre en compte les coûts de transport, et ils sont liés à un port
d'attache pour calculer le temps de trajet et les coûts
correspondants. Chaque bateau appartient à un ensemble de bateaux
définit par une liste de métiers possibles avec une description des
paramètres d'effort, des définitions concernant l'équipage et les
coûts associés, les coûts concernant l'entretient des engins de pêche
et la maintenance du bateau ainsi que les autres frais
d'exploitation. Les stratégies sont des sub-groupes de bateaux
partageant les mêmes caractéristiques (appartenant au même set of
vessels) et ayant une même répartition de leur effort sur les
différents métier chaque mois. Les équations permettant de calculer la
mortalité par pêche par espèce, par classe, par zone et par métier
ainsi que les équations permettant de calculer les coûts et revenus
sont détaillés dans le chapitre 15-Equations.

La réponse des pêcheurs aux règles de gestion, aux fluctuations de
stocks et aux conditions économiques peuvent avoir des incidences sur
la réallocation de l'effort de pêche. La plupart des équations
contrôlant la dynamique des flottilles sont ainsi modifiables grâce à
un éditeur de langage script en sélectionnant des modèles prédéfinis
ou en créant son propre modèle. Parmi les modèles préalablement
écrits, on retrouve plusieurs modèles de gravité dont les coefficients
sont calculé soit à partir des captures ou des débarquements
précédentes en valeur ou en poids. Voir Chapitre 15-Modèles de
gravité.

.. Schéma : Description des objets dans la version 2.0 d'ISIS-Fish

Cette version utilise une base de données embarqués et n'utilise plus
les EJBs.

Version 3.0 (depuis 2006)
-------------------------

Cette version est une refonte complète de la persistence. Chaque
région a maintenant un répertoire particulier dans lequel on y trouve
une base de données embarquée, les simulations qui se rapportent à cette
région ont eux aussi leur propre répertoire incluant l'image de la
base de données à l'instant de la simulation, les résultats de la
simulation, les exports qui ont été fait, les scripts qui a permit la
simulation et les paramètres de la simulation. De cette façon tous les
éléments ayant permis l'obtention des résultats sont conservés et la
simulation peut-être réjouée (fonctionnalité non encore implantée).

Le moteur de simulation à lui aussi été revu pour permettre un suivi
plus simple et centralisé des différentes simulations en cours. Cette
réécriture à tenu compte de l'analyse faite durant le projet Mexico sur
les plans d'expérience.

Une analyse des performances de la version 2 à permit une optimisation
des calculs. Durant cette analyse il est resortie que les languages de
script était beaucoup plus lent que le Java (entre 1000 et 10000
fois). Tous les scripts et équations sont dans cette version écrits en
Java et compilé au besoin. L'utilisation du Java a aussi permis
d'ajouter des fonctionnalités lors de la saisie comme la vérification
du code écrit et l'indication des erreurs avec leur numéro de ligne.
