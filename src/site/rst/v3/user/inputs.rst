.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
====================
Saisie d'un pêcherie
====================

La fenêtre saisie de pêcherie permet de créer, visualiser et modifier des pêcheries.

description des différentes zones de l'interface
------------------------------------------------

.. image:: images/input_00.png

#. les menus de l'éditeur de pêcheries.
#. la nom d'une nouvelle région à créer.
#. pour créer une nouvelle région.
#. la liste des régions détectées par IsisFish.
#. la zone de navigation dans les objects de la pêcherie.
#. la zone d'édition des données de la pêcherie.
#. la zone classique de status générale.

Une pêcherie contient une région.

On peut soit créer entièrement la région en renseignant son nom (zone 1) puis
en cliquant sur le boutton **nouvelle région** (zone 2).

On peut aussi sélectionner une région existante localement dans la liste
déroulante (zone 3).

Un dossier du nom de votre cas d'étude a ainsi été créé dans le menu déroulant et dans la colonne de gauche
ont été créées des sous dossiers correspondant à tous les composants de la pêcherie.

Dans les deux cas, une fois la région chargée ou créée, les objects de la
pêcheries sont disponibles dans leur zone de navigation (zone 4).

La carte est affichée à droite.

On peut aussi éditer les caractéristiques de la région.


Les différentes actions des menus
---------------------------------

On détaille dans cette section, l'ensemble des actions réalisables par menu.
Il s'agit d'actions globales sur les pêcheries.

menu **fichier**
================

.. image:: images/input_01_menuFile.png


1. **importer** une région
~~~~~~~~~~~~~~~~~~~~~~~~~~

Importer une région précedemment exportée depuis une autre instance d'IsisFish v3.

2. **importer et renommer** une région
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Importer une région précedemment exportée depuis une autre instance d'IsisFish v3 et
renommer le nom de la région.


3. **importer de la V2 d'IsisFish** une région
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Importer une région précemment exportée depuis une autre instance d'IsisFish v2.


4. **importer depuis une simulation** une région
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Importer une région depuis une simulation localement existante.

Une interface de selection de simulation s'affiche


Une fois le nom de la simulation choisi, le nom de la région est demandé.

Ensuite, la région est extraite de la simulation et sauvegarder localement.


5. **exporter** une région
~~~~~~~~~~~~~~~~~~~~~~~~~~

Exporter une région dans un fichier compressé. L'archive produite pourra être
importée dans une autre instance d'IsisFish v3.

Une région doit être sélectionnée au préalable.

6. **copier** une région
~~~~~~~~~~~~~~~~~~~~~~~~

Permet la recopie simple de région.

Une région doit être sélectionnée au préalable.

7. **supprimer localement** une région
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Permet de supprimer localement une région.

Une région doit être sélectionnée au préalable.

Attention, cette opération est irréversible, une fois supprimée, la pêcherie
ne sera plus récupérable.

8. **Fermer** l'éditeur de pêcheries
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Ferme l'éditeur de régions. Si des modifications n'ont pas été sauvegardées, un
dialogue apparait pour demander un commentaire à ajouter concerant les
modifications à sauvegarder.

menu **Serveur**
================

.. image:: ./images/input_02_menuServer.png

Ce menu regroupe les fonctionnalités de communication avec le serveur de données.

1. **ajouter** une région
~~~~~~~~~~~~~~~~~~~~~~~~~

TODO Revoir

Permet d'envoyer au serveur distant une nouvelle région

Une boite de dialogue apparait où sont affichés les différents messages de
sauvegarde déjà rentrés pour cette pêcherie.

En appyuant sur le bouton Ok, la pêcherie est ajouté sur le serveur distant.

Vous avez les droits d'écriture sur ce serveur (voir chapitre configuration vcs).

2. **sauvegarder les modifications** d'une région
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

TODO est-ce actif, je ne crois spas...


3. **supprimer** localement et sur le serveur distant
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Permet de supprimer une région localement mais aussi sur le serveur distant.

Vous devez disposer de droits d'écriture sur le serveur distant.

Attention : A utiliser avec précaution!


menu **aide**
=============

.. image:: images/input_03_menuHelp.png

Une seule action pour le moment

#. **A propos** : about IsisFish


Créer une nouvelle pêcherie
---------------------------

La saisie d'une pêcherie se déroule en plusieurs étapes :

#. création ou sélection de la région à utiliser dans la pêcherie
#. saisie des mailles
#. saisie des zones
#. saisie des ports
#. saisie des espèces
#. saisie des populations
#. saisie des engins
#. saisie des métiers
#. saisie des marées
#. saisie des types de navire
#. saisie des flotilles
#. saisie des stratégies


1. **création** ou **sélection** de région
==========================================

Une fois la région créée ou chargée voila à quoi resssemble l'interface.

.. image:: ./images/input_04_create.png

Les latitudes et longitudes sont en degrés décimaux.

#. **nom de la région**
   Le nom de la région (il s'agit d'un nom d'objet de la pêcherie, il est
   préférable de normaliser ces noms, par exemple n'utiliser que des caractères
   alpahnumériques sans ponctuation).
   La région est un rectangle avec un grille régulière de mailles.
#. **Latitude minimum** :
   latitude minimum de la région (valeur réelle)
#. **Latitude maximum** :
   latitude maximum de la région (valeur réelle)
#. **Longitude minimum** :
   longitude minimum de la région (valeur réelle)
#. **Longitude maximum** :
   longitude maximum de la région (valeur réelle)
#. resolution spatiale  (**latitude**) :
   étendue de chaque maille en latitude (valeur réelle)
#. resolution spatiale  (**longitude**) :
   étendue de chaque maille en longitude (valeur réelle)
#. liste des cartes : TODO
#. **ajouter une carte** :
  Si vous désirez changer la carte par défaut pour une carte plus
  précise, cliquez sur Ajouter carte et sélectionnez la carte à
  ajouter grâce au menu défilant.
  Il n'est nécessaire que d'ajouter la carte au format shp mais par
  contre les deux cartes shp et ssx doivent être situées dans le
  même dossier
#. **supprimer la carte** :
  Pour supprimer une carte précdemment ajoutée.
#. **commentaires** :
  Des informations complémentaires sur la région peuvent être ajoutées
  grâce aux encadrés **Commentaires**. Elles seront sauvegarder en même temps
  que la region.
#. **ajouter un fichier de mailles** :
  Un fichier contenant des mailles prédéfinies peut aussi être chargé.
  Cliquez sur **...***
#. **sauver** la région :
  Une fois que les frontières de la région ont été définies (la carte
  et le fichier de mailles prédéfinies ajoutées si nécessaire), cliquez
  sur **sauver**.
#. **annuler** :
   Pour annuler les modifications efféctuées et revenir à la dernière version
   enregistrée.
#. **vérifier** :
   Pour vérifier la cohérence de la pêcherie.
#. **continuer** vers les **mailles** :
  A la fin de ce stade, la région ainsi qu'une grille de mailles ont été crées.

  Afin de continuer la paramétrisation, cliquez sur ** Continuer vers les mailles **
  ou sélectionner l'onglet ** Mailles ** dans le menu de gauche.

2. **saisie des mailles**
=========================

.. image:: images/input_05_inputMailles.png

La feuille de saisie des mailles permet à l'utilisateur de renommer les
mailles. Il est aussi possible de définir si la maille est en fait de la terre
en cochant la case **Terre**.

#. **maille** dans la navigation :
  On peut sélectionner une maille à partir de cette zone. Les autres zones
  seront alors mises à jour avec les données de cette maille.
#. **maille** dans liste :
  On peut sélectionner une maille à partir de cette liste déroulante des noms
  des mailles de la région. Les autres zones seront alors mises à jour avec les
  données de cette maille.
#. **maille** dans carte :
   On peut enfin sélectionner une maille en la sélectionnant sur la carte. Les
   autres zones seront alors mises à jour avec les données de cette maille.
#. **nom** de maille :
   Pour modifier le nom de la maille, par défaut un nom de maille est l'expression
   de sa latitude suivi de sa longitude; exemple : La44.5-Lo-3.0
#. **latitude** :
   latitude du coin bas gauche de la maille sélectionnée exprimée en degré.
#. **longitude** :
   longitude du coin bas gauche de la maille sélectionnée exprimée en degré.
#. **Terre** :
   Case à cocher permettant de spécifier si la maille est sur terre ou non.
#. **commentaires** :
   Pour saisir un commentaire lié à cette maille. Des informations
   complémentaires peuvent être ajoutées grâce cette fenêtre. Chaque
   commentaire est lié à une maille, il est ainsi possible d'ajouter un
   commentaire pour chaque maille préalablement définie.
#. **sauver** :
   Pour sauver les modifications sur cette maille. Ne pas oublier de sauver en
   cliquant sur ce boutton pour chaque modification de maille.
#. **annuler** :
   Pour annuler les modifications sur cette maille et revenir à la dernière
   version sauvegardée.
#. **continuer** vers les zones :
   Afin de poursuivre, cliquez sur **Continuer vers les Zones** ou sélectionner
   l'onglet **Zones** dans le menu de gauche.


3. **saisie des zones**
=======================

Les zones **populations**, les zones **métier** et les zones de gestion doivent être
créées respectivement avant les populations, les métiers et les mesures de
gestion.

.. image:: images/input_06_inputZones.png

#. **créer** :
   On commence toujours par créer la zone avant de modifier des valeurs, on
   clique ici pour créer la zone vide. Une fois la zone créée, elle apparaît
   dans la combo **Choisir une zone** et il est possible de la sélectionner et
   de la renommer.
#. **nom** :
   Pour renseigner le nom de la zone une fois celle-ci créée. Voir remarque sur
   les noms des objets de la pêcherie.
#. **sauver** :
   Permet de sauver en base la nouvelle zone.
   Ne pas oublier de sauver à la fin de ces opérations.

   A ce stade, la zone a été créée, il faut désormais lui associer des mailles.
#. **Choisir une zone** :
   La **zone** dans la zone de navigation des objets de la pêcherie.
#. **sélection des mailles**  de la zone :
   Sélectionner les mailles composants cette zone soit en cochant les cases une
   par une soit en les sélectionnant directement sur la carte.
#. **les mailles de la zone représentés sur la carte** :
   cliquer sur la maille à sélectionner, elle devient verte une fois sélectionnée.
#. **annuler** :
   Pour annuler les changements effectués dans la zone sélectionnée.
#. **supprimer** :
   Pour supprimer une zone de la pêcherie. Attention cette opération est
   irréversible, une fois la zone supprimée, elle n'est plus récupérable.
#. **commentaires** :
   Des informations complémentaires peuvent être ajoutées grâce cette fenêtre.
   Chaque commentaire est lié à une **Zone**, il est ainsi possible d'ajouter un
   commentaire pour chaque Zone préalablement définie.
#. ** continuer** vers les ports :
   Afin de poursuivre la saisie des paramètres, cliquer sur **continuer vers
   les ports** ou cliquer sur l'onglet Ports dans le menu de gauche.

4. **saisie des ports**
=======================

Les ports sont utilisés afin de pouvoir calculer les coûts de transport.
Un port est contenu dans une maille correspondant à la localisation
géographique du port.

Chaque port est localisé exactement sur une maille.

.. image:: images/input_07_inputPorts.png

#. **créer** :
   On commence toujours par crééer le port avant d'effectuer des modifications.
   On clique ici pour créer un port vide. Une fois le port créé, il apparaît
   dans la zone de navigation **Choisir un port** et il est possible de le
   sélectionner et de l'éditer.
#. **nom du port** :
   Le nom du port à renseigner (Voir remarque sur les noms des objets de la
   pêcherie.)
#. **sauver** :
   Pour sauver le port, en base. Ne pas oublier de sauver à la fin de ces
   opérations.

   A ce stade, le port a été créé, il faut désormais lui associer une maille.
#. **Choisir une port** :
   Le port dans la zone de navigation des objets de la pêcherie.
#. **sélection de la maille du port** :
   Sélectionner la maille où se situe le port en cochant une case ou en les
   sélectionnant directement sur la carte.
#. **la maille du port représenté sur la carte** :
   Cliquer sur la maille à sélectionner, elle devient alors verte.
#. **annuler** :
   Pour annuler les changements effectués sur le port sélectionné.
#. **supprimer** :
   Pour supprimer le port de la pêcherie. Attention cette opération est
   irréversible, une fois le port supprimé, il ne sera plus récupérable.
#. **commentaires** :
   Des informations complémentaires peuvent être ajoutées grâce à cette fenêtre.
   Chaque commentaire est lié à un Port, il est ainsi possible d'ajouter un
   commentaire pour chaque Port préalablement défini.
#. ** continuer** vers les espèces :
   Afin de poursuivre la saisie des paramètres, cliquer sur l'onglet
   **Métapopulations** dans le menu de gauche.


5. **saisie des espèces**
=========================

.. image:: images/input_08_inputSpecies.png

#. **créer** une espèce :
   On commence toujours par créer l'espèce avant de modifier ces
   caractéristiques. On clique ici pour créer une espèce vide.
   Une fois l'espèce créée, elle apparaît dans la zone de navigation des
   objets de la pêcherie. Il est possible de la sélectionner et de l'éditer.
#. **nom de l'espèce** :
   On nomme l'espèce une fois celle-ci créee. (Voir remarque sur les noms des
   objets de la pêcherie.)
#. **sauver** :
   Permet de de sauver en base le nouveeau port.
   Ne pas oublier de sauver à la fin de ces opérations.

   A ce stade, l'espèce est crée, il faut désormais termnier la configuration
   de ses caractéristiques.
#. **nom scientifique** :
   Pour renseigner le nom scientique de l'espèce. Le nom commun uniquement sera
   utilisé par la suite. La plupart des paramètres biologiques à définir
   ensuite dépendent de la Dynamique choisie. Les populations peuvent soit
   avoir une dynamique en âge, soit avoir une dynamique en stades (incluant
   aussi la structuration en longueur).
#. **code rubbin** TODO
#. **CEE** TODO
#. **structuré en age** :
   Dynamique de la population en age, cela correspondant au nombre de classes
   d'âge.
#. **structuré** en longueur (ou stade) :
   Dynamique de la population en stade, cela correspond au nombre de classes
   de stades.
#. **commentaires** :
   Des informations complémentaires peuvent être ajoutées grâce à cette fenêtre.
   Chaque commentaire est lié à une espèce, il est ainsi possible d'ajouter un
   commentaire pour chaque espèce préalablement définie.
#. **annuler** :
   Pour annuler les changements effectués sur l'espèce sélectionnée et revenir
   à la dernière version sauvegardée.
#. **supprimer** :
   Pour supprimer l'espèce de la pêcherie. Attention cette opération est
   irréversible, une fois l'espèce supprimée, elle ne sera plus récupérable.
#. **choisir une espèce** :
   L'espèce dans la zone de navigation des objets de la pêcherie.
#. ** continuer** vers les populations  :
   Afin de poursuivre la saisie des paramètres, cliquer sur l'onglet
   **Populations** de l'espèce dans le menu de gauche.

6. **saisie des populations**
=============================

Les populations sont décrites au travers de huit onglets. Une fois la population
créée, renseignez les différents paramètres dans l'ordre suivant.

.. image:: images/input_09_inputPopulations.png

#. onglet de saisie des paramètres de base de la population
#. onglet de saisie des groupes de populations
#. onglet de saisie des équations de population
#. onglet de saisie des zones de la population
#. onglet de saisie des saisons de la population
#. onglet de saisie des équations de capturabilité de la population
#. onglet de saisie des équations de recrutement de la population
#. onglet de saisie des migrations de la population
#. **population** dans la navigation :
   Vous pouvez choisir dans la zone de navigation des objets de la pêcherie,
   les populations à éditer.
#. **continuer**
   Une fois les modifications sur les populations terminées, vous pouvez
   continer vers la saisie des engins.

La saisie des populations requiere la définition d'un certain nombre d'équations
dont voici la liste:

#. **équation de croissance** :
   Si le dernier groupe est défini comme un **groupe plus**, la fonction de
   croissance de la population (lorsqu'elle est structurée en âge) et les
   paramètres correspondant.(todo figure)
#. **équation de croissance inverse** :
   Fonction inverse de la croissance de la population (lorsqu'elle est
   structurée en stade).(todo figure)
#. **équation de taux de mortalité naturelle** :
   todo figure)
#. **équation de poids principal** :
   (todo figure)
#. **équation de prix** :
   (todo figure)
#. **équation de migration** :
   (todo figure)
#. **équation de émigration** :
   (todo figure)
#. **équation de immigration** :
   (todo figure)
#. **équation de reproduction** :
   Une equation donnant le nombre d'oeufs émis en fonction de l'abondance des
   reproducteurs et d'autres paramètres dont, par exemple le coefficient de la
   fécondité.(todo figure)


onglet **Saisie des Populations**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Les paramètres de base incluent tous les paramètres biologiques définis à
l'échelle de la Population.

.. image:: images/input_10_inputPopulations_tabBase.png

#. **nouveau** :
   On commence toujours par créer une nouvelle population, avant d'ne modifier
   les caractéristiques.
#. **sauver** :
   sauver la population en base.
#. **annuler** :
   Annuler les modifications effectuées sur la population et revenir sur la
   dernière version sauvegardée.
#. **supprimer** :
   Supprimer la population sélectionnée de la pêcherie.
#. **recréer** les classes :
   Créer les groupes de la population.
   Cela ouvrira différentes boites de dialogue selon la manière dont les
   populations sont structurées, ainsi que de la manière dont l'utilisateur
   veut décrire les classes. TODO (voir figures des dialogues).

   Pour effectuer cette opération, il faut avoir renseigner les équations de
   croissances et autres.
#. **structuration dynamique** :
   Une fois que les interfaces concernant les fonctions de croissance ou les
   fonctions inverse de croissance ont été renseignées, il est possible de
   vérifier les paramètres grâce à la table.
   Si les paramètres de croissance ont été modifiés, ne pas oublier de sauver.
#. **save as model** :
   Pour sauver une équation de type croissance dans les scripts d'IsisFish (formules/growth TODO)

   Si la fonction de croissance créée peut a priori être réutilisée pour
   d'autres espèces il peut être utile de la sauver comme modèle en
   utilisant ce bouton.
#. **open editor** :
   Pour ouvrir l'éditeur d'équation de croissance (voir figure
   input_XX_equationGrowth).
#. **save as model** :
   Pour sauver une équation de type décroissance inverse dans les scripts d'IsisFish (formules/reversegrowth TODO)

   Si la fonction de croissance inverse créée peut a priori être réutilisée
   pour d'autres espèces il peut être utile de la sauver comme modèle en
   utilisant ce bouton.
#. **open editor** :
   Pour ouvrir l'éditeur d'équation de croissance inverse (voir figure
   input_XX_equationReverseGrowth).
#. **nom** :
   Pour renseigner le nom de la population (voir remarque sur les noms des
   objets d'une pêcherie).
#. **identifant géographique** :
   Pour renseigner l'identifiant de la population. Un petit commentaire de
   description de la population peut être ajouté ici.
#. **nombre de groupes** :
   Une fois les classes de la population créées, on voit ici le nombre de
   groupes de la population.
#. **groupe de maturité** :
   Pour choisir le groupe de maturité. Il faut avoir créer les classes de la
   population avant. Il s'agit de l'âge auquel 50% de la population est matûre.

   Saisir l'âge de maturité des individus. Cette valeur pourra être utilisée
   comme variable dans l'équation de reproduction. L'âge de maturité est
   exprimé en années.
#. **plusGroup** :
   Pour définir une population de type **plusGroup**
   Cocher la case si la dernière classe de la population est définie comme un
   groupe plus.
   Si le dernier groupe est définit comme un groupe plus, la fonction de
   croissance de la population (lorsqu'elle est structurée en age) et les 
   paramètres  correspondants.
#. **choix équation croissance** :
   Pour choisir l'équation de croissance à utiliser pour la population.
#. **éditeur équation croissance** :
   Pour éditer l'équation de croissance L'interface de saisie de la fonction
   de croissance permettant de calculer la taille en fonction de l'âge n'est à
   renseigner (et ne sera utilisée) que dans le cas de populations structurées
   en âge.

   Il est possible d'utiliser soit un modèle prédéfini et dans ce cas la seule
   opération consiste à changer les paramètres pour ceux correspondant au cas d'étude.
   La seconde option consiste à écrire votre propre modèle en utilisant l'éditeur.

   Les ages sont exprimés en années (Classe Mature et Age) mais la variable
   age dans la fonction de croissance est exprimée en mois.
#. **choix équation croissance inverse** :
   Pour choisir l'équation de croissance inverse à utiliser pour la population
   (lorsqu'elle est structurée en stade).

#. **éditeur équation croissance inverse** :
   Pour éditer l'équation de croissance inverse.

   Ne renseigner cette partie que si la population est structurée en stades
   (commentaires identique à ceux formulés au dessus pour les fonctions de
   croissances et les populations structurées en âge).
#. **commentaires** :
   Des informations complémentaires peuvent être ajoutées grâce à cette fenêtre.
   Chaque commentaire est lié à une Population, il est ainsi possible d'ajouter
   un commentaire pour chaque population préalablement définie.


onglet **Saisie des groupes de populations**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Permet d'éditer les paramètres de chaque groupe de la population.

.. image:: images/input_11_inputPopulations_tabGroup.png

#. **liste des groupes** :
   Cette liste déroulante contient l'ensemble des groupes connus pour cette
   espèces (ils ont été générés pendant la création des classes).
#. **poids principal** du groupe sélectionné :
   Il s'agit du poids moyen de la classe (en kg).
#. **prix** du groupe sélectionné :
   Il s'agit du prix moyen de la classe (en euros par kg).
#. **taux de mortalité naturelle** du groupe sélectionné :
   Il s'agit de l'équation de mortalité naturelle par année.
#. **taux de reproduction** du groupe sélectionné :
   Intensité relative de la reproduction durant les mois de la saison de
   reproduction (i.e la proportion de mature se reproduisant durant le mois
   courant de la saison de reproduction).
   autrement dit : nombre d'oeufs  - par kg de femelles de la classe (ne pas
   oublier de multiplier le poid moyen d'un individu de la classe et par le sexe
   ratio dans l'équation de reproduction). - par femelle (ne pas oublier de
   multiplier ensuitr par le sexe ratio). - par individus.
#. **age** du groupe sélectionné :
   Si la population est structurée en stades, cette valeur est l'âge moyen
   d'une classe.
#. **longueur** du groupe sélectionné :
   Si la population est structurée en âge, cette valeur est la longueur moyenne
   de la classe d'âge.
#. **commentaires** :
   permet d'ajouter des commentaires.
#. **sauver** :
   sauver les modifications sur les groupes de la population
#. **annuler** :
   annuler les modifications effectuées sur les groupes de la population.


onglet **Saisie des équations de population**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: images/input_12_inputPopulations_tabEquation.png

Pour saisir les équations de mortalité naturelle, poids principal et prix de la
population.


#. **liste des populations** :
   Cette liste déroulante contient l'ensemble des équations de mortalité
   naturelles connues pour cette espèce.
#. **équation de taux de mortalité naturelle** :
   Pour éditer sans passer par l'éditeur d'équations, l'équation de mortalité
   naturelle.
#. **liste des populations** :
   Cette liste déroulante contient l'ensemble des équations de poids principal
   connues pour cette espèce.
#. **équation de poids principal** :
   Pour éditer sans passer par l'éditeur d'équations, l'équation de poid  principal.
#. **liste des populations** :
   Cette liste déroulante contient l'ensemble des équations de prix
   connues pour cette espèce.
#. **équation de prix** :
   Pour éditer sans passer par l'éditeur d'équations, l'équation de prix.
#. **save as model** pour équation de taux de mortalité naturelle
   pour sauver une équation de type taux de mortalité naturelle dans les scripts d'IsisFish (formules/NaturalDeathRate TODO)

   Si la fonction créée peut a priori être réutilisée pour d'autres population
   il peut être utile de la sauver comme modèle en utilisant ce bouton.
#. **open editor** pour équation de taux de mortalité naturelle :
   pour ouvrir l'éditeur d'équation de croissance (voir figure input_XX_equationNaturalDeathRate TODO).
#. **save as model** pour équation de poids principal
   pour sauver une équation de type poids principal dans les scripts d'IsisFish (formules/meanWeight TODO).

   Si la fonction créée peut a priori être réutilisée pour d'autres population
   il peut être utile de la sauver comme modèle en utilisant ce bouton.
#. **open editor** pour équation de poids principal
   pour ouvrir l'éditeur d'équation de poids principal(voir figure
   input_XX_equationMeanWeight).
#. **save as model** pour équation de prix :
   pour sauver une équation de type prix dans les scripts d'IsisFish (formules/price TODO)

   Si la fonction créée peut a priori être réutilisée pour d'autres population
   il peut être utile de la sauver comme modèle en utilisant ce bouton.
#. **open editor** pour équation de prix
   pour ouvrir l'éditeur d'équation de prix  (voir figure input_XX_equationPrice).
#. **sauver**
   sauver les modifications sur les équations de la population
#. **annuler**
   annuler les modifications effectuées sur les équations de la population et
   revenir à la dernière version sauvegardée.


onglet **Saisie des zones de populations**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: images/input_13_inputPopulations_tabZone.png

On définit ici les zones d'apparition et reproduction

#. **sélection des zones** :
   Dans cette liste, on retrouve l'ensemble des zones de la pêcherie.
#. **Sélection des zones de présence** :
   Dans cette liste, on retrouve l'ensemble des zones de la pêcherie. On doit
   sélectionner les zones de présence de la population durant l'année.
#. **Sélection des zones de reproduction** :
   Dans cette liste, on retrouve l'ensemble des zones de la pêcherie. On doit
   sélectionner les zones où se dérouleront les reproductions.
#. **éditeur des correspondances** :
   Pour spécifier les correspondances entre les zones de reproduction, et les
   zones de recrutements si il y a plusieurs aires, i.e définir la proportion
   d'oeufs émis de la zone de reproduction qui sont recrutés dans une zone de
   recrutement.
#. **sauver** :
   Sauver les modifications effectuées sur les zones de la population.
#. **annuler** :
   Annuler les modifications effectuées sur les zones de la population et
   revenir à la version précédente.

onglet **Saisie des saisons** (population en age)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

On définit ici les saisons pour une population structurée en age.

.. image:: images/input_14_inputPopulations_tabSeason.png

#. **liste des saisons** :
   Cette liste déroulante contient l'ensemble des saisons actuellement définies
   pour cette population.
#. **plage de la saison** :
   on définit la saison en utilisant le curseur. Les mois en jaune correspondant
   aux mois sélectionnés de la saison.
#. **changement de groupe** :   
   Pour une saison donnée, si cette case est cochée, les poissons changeront de
   classe le premier moise de la saison.   
   Le changement de classe ne peut être appliqué que sur une seul saison par an.
#. **reproduction** :
   Pour une saison donnée, si cette case est cochée, un tableau décrivant la
   distribution de la reproduction apparaît, chaque cellule doit être renseignée
   avec l'intensité de la reproduction par mois de la saison.  
#. **répartition des apparitions** :
   Chaque cellule doit être renseignée avec l'intensité de la reproduction par
   mois de la saison.
   Les valeurs dans le tableau ne sont enregistrées qu'après avoir validé en
   appuyant sur la touche **entrée**. Les paramètres ont cependant besoin
   d'être sauvés en cliquant sur le bouton **sauver**.   
#. **commentaires** :
   Des informations complémentaires peuvent être ajoutées grâce à cette zone.
   Chaque commentaire est lié à une saison, il est ainsi possible d'ajouter
   un commentaire pour chaque saison préalablement définie.   
#. **sauver** :
   Permet de sauver les modifications effectuées sur les saisons de la population.
#. **annuler** :
   Permet d'annuler les modifications effectuées et de revenir à la dernière
   version sauvergardée.
#. **nouveau** :
   On doit toujours commencer par créer une saison avant d'en modifier le
   contenue. Une fois la saison crééen on définit sa position dans l'année.
   Deux saisons ne peuvent pas se chevaucher. Par contre une saison peut
   commencer une année et finir l'année suivante (commencer en novembre et finir
   en février de l'année suivante par exempl
#. **supprimer** :
   Permet de supprimer une saison de la population, cette opération est
   irréversible, une fois supprimée la saison n'est plus récupérable.

remarque : S'assurer que le changement de classe n'est coché que pour une
saison, sinon les poissons pourront changer de classe plus d'une fois par an.

onglet **Saisie des saisons** (population en stade)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
TODO faire screenshot pour ce type de classe

On définit ici les saisons pour une population structurée en age.

.. image:: images/input_14_inputPopulations_tabSeasonStade.png

#. **liste des saisons** :
   Cette liste déroulante contient l'ensemble des saisons actuellement définies
   pour cette population.
#. **plage de la saison** :
   On définit la saison en utilisant le curseur. Les mois en jaune correspondant
   aux mois sélectionnés de la saison.
#. **changement de groupe** :
   Pour les classes en age : Pour une saison donnée, si cette case est cochée,
   les poissons changeront de classe le premier moise de la saison.
   pour les populations en stade, proportion d'individus qui changent de classe
   chaque mois de la saison(possibilité de spatialiser ce changement de classes).
   Le changement de classe ne peut être appliqué que sur une seul saison par an.
#. **reproduction** :
   Pour une saison donnée, si cette case est cochée, un tableau décrivant la
   distribution de la reproduction apparaît, chaque cellule doit être renseignée
   avec l'intensité de la reproduction par mois de la saison.
#. **répartition des apparitions** :
   Sélectionner si le changement de classe doit se faire de manière spatialisé
   ou non (cocher la case appropriée).
   Renseigner la matrice de coefficient de changement de classes. Si la matrice
   est spatialisée, la taille de la matrice sera le nombre de zones de présence.
   Pour chaque mois le coefficient correspond à la proportion d'individus passant
   de la classe i (en ligne) à la classe j(en colonne). Ces coefficients peuvent
   être définis par aires.
   Les valeurs dans le tableau ne sont enregistrées qu'après avoir validé en
   appuyant sur la touche **entrée**. Les paramètres ont cependant besoin
   d'être sauvés en cliquant sur le bouton **sauver**.
#. **commentaires** :
   Des informations complémentaires peuvent être ajoutées grâce à cette zone.
   Chaque commentaire est lié à une saison, il est ainsi possible d'ajouter
   un commentaire pour chaque saison préalablement définie.
#. **sauver** :
   Permet de sauver les modifications effectuées sur les saisons de la population.
#. **annuler** :
   Permet d'annuler les modifications effectuées et de revenir à la dernière
   version sauvergardée.
#. **nouveau** :
   On doit toujours commencer par créer une saison avant d'en modifier le
   contenue. Une fois la saison crééen on définit sa position dans l'année.
   Deux saisons ne peuvent pas se chevaucher. Par contre une saison peut
   commencer une année et finir l'année suivante (commencer en novembre et finir
   en février de l'année suivante par exempl
#. **supprimer** :
   Permet de supprimer une saison de la population, cette opération est
   irréversible, une fois supprimée la saison n'est plus récupérable.

onglet **Saisie de la capturabilité**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pour chaque classe et chaque saison, le coefficient de capturabilité définit
comme la probabilité d'un individus de la classe, présent dans la zone au
cours d'une saison, d'être capturé par une unité d'effort standardisée
appliquée par un engin non sélectif.

.. image:: images/input_15_inputPopulations_tabCapturability.png

#. **édition** :
   Saisir ces coefficients pour chaque classe et chaque saison.
   Les valeurs dans la table ne seront saisies qu'après avoir appuyé sur entrée
   (sinon les cellules restent bleues, ce qui signifie qu'elles ne seront pas sauvées).   
#. **commentaires** :
   Pour saisir des commentaire concernant la capturabilité de la population, ils seront
   sauvgardé en même temps que la population.
#. **sauver** :
   Pour enregistrer les modifications effectuées sur cet écran en base.
#. **annuler** :
   Pour annuler les modification effectuées sur cet écran et revenir à la
   dernière version enregistrée.

onglet **Saisie des reproductions**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pour configurer les reproductions et recrutements de la population.

L'équation de reproduction est une équation qui permet de calculer le nombre
d'oeufs produits en fonction de l'abondance des reproducteurs ainsi que
d'autres paramètres, pouvant inclure le coefficient de fécondité et la
Classe Mature.

On définit l'étalement de recrutement : Proportion de la classe naissance qui
est effectivement recrutée chaque mois.
Cet étalement reflète la variabilité individuelle des oeufs et des larves
dans le développement d'une cohorte mensuelle. Par défaut, le recrutement
d'une cohorte dure un mois, cela signifie que tous les individus de la
cohorte recrutent durant le même mois lorsqu'ils sont près à être recrutés.

.. image:: images/input_16_inputPopulations_tabRecruetment.png

#. **liste des équations de reproduction** :
   On retrouve dans cette liste déroulante, l'ensemble des équation de
   reproduction connues dans la pêcherie.
#. **éditeur de l'équation de reproduction** :
   Pour éditer l'équation de reproduction sans passer par l'éditeur d'équation
   externe.
#. **nombre de mois** entre la reproduction et le recrutement :
   Temps requis à la classe naissance pour être recrutée. Cela détermine le
   début du recrutement à partir de la saison de reproduction.
#. **distribution du recrutement** :
   Pour modifier les proportions dans la distribution du recrutement.
   Par défaut toutes les valeurs sont à zéro.
#. **nouvelle matrice** :
   Cliquer sur ce bouton afin de faire apparaître une boite de dialogue
   permettant de définir la durée du recrutement.
#. **commentaires** :
   Des informations complémentaires peuvent être ajoutées grâce à cette zone
   d'édition. Chaque commentaire est lié à une population, il est ainsi possible
   d'ajouter un commentaire pour chaque population préalablement définie.
#. **save as model** pour équation de reproduction :
  pour sauver une équation de type reproduction dans les scripts d'IsisFish (formules/reproduction TODO)
  Si la fonction créée peut a priori être réutilisée pour d'autres population
  il peut être utile de la sauver comme modèle en utilisant ce bouton.
#. **open editor** pour équation de reproduction
  pour ouvrir l'éditeur d'équation de reproduction (voir figure
  input_XX_equationReproduction).
#. **sauver**
  sauver les modifications effectuées.
#. **annuler**
  annuler les modifications effectuées et revenir à la dernière version
  sauvegardée..

remarque :  Si la seule relation disponible est une relation Stock/Recrutement,
il suffit de saisir cette relation dans l'équation de reproduction et de mettre
la mortalité naturelle de la classe naissance ainsi que le nombre de mois entre
la reproduction et le recrutement à zéro.

onglet **Saisie des migrations de populations**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Saisie des migrations, émigrations et immigrations sur les groupes de la
population.

Jusqu'à la version 2.2.2, les migrations, immigrations et émigrations sont
définies comme des coefficients ou des effectifs relatifs à chaque classe.
À partir de la version 2.2.2, il est possible de décrire ces migrations à
l'aide d'équations ayant pour arguments les classes, les zones de départ et
d'arrivée et les matrices d'effectifs.

Les migrations se réalisent instantanément au début de la saison sélectionnée.
(voir les différents articles sur ISIS-Fish)

On peut le résaliser de deux manières:

#. en saisissant directement les mouvements de populations
#. en saisissant des équations décrivant les mouvements de populations

onglet **Saisie des migrations de populations** (Migration)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Les migrations se réalisent instantanément au début de la saison sélectionnée.
(voir les différents articles sur ISIS-Fish)

Correspond aux migrations entre les zones de la région.

.. image:: images/input_17_inputPopulations_tabMigration.png

#. **sélection de la saison** :
   Liste déroulante contenant l'ensemble des saisons actuellement enregistrée
   pour l'espèce dans la pêcherie. Il faut en sélectionner une pour définir une
   migration.
#. **sélection groupe de population à migrer** :
   Liste déroulante contenant l'ensemble des classes de la population.
   Les migrations sont détaillées par classes. On sélectionne ici la classe
   affectée par l'immigration à détailler.
#. **sélection zone de départ** :
   Liste déroulante des zones de départ parmi les différents aires préalablement
   définies. Il faut sélectionner un zone de départ pour la classe sélectionnée.
#. **coefficent de migration** :
   il s'agit de la proportion de la classe sélectionnée affectée par la migration.
#. **sélection zone d'arrivée** :
   Liste déroulante des zones d'arrivé parmi les différents aires préalablement
   définies. Il faut sélectionner un zone d'arrivée pour la classe sélectionnée.
#. **ajouter la migration** :
   Une fois sélectionnés les paramètres précédemment cités, on appuye ici pour
   crééer la migration; les paramètres de la migration apparaissent comme une
   ligne dans la table ci-dessous.
#. **les immigrations enregistrées** :
   Liste des migrations actuellement renseignéés.
#. **supprimer une migration** :
   Pour supprimer une migration précedemment ajoutée. Attention cette opération
   est irréversible, une fois supprimée la migration ne sera plus récupérable.
#. **commentaires** :
   Des informations complémentaires peuvent être ajoutées grâce à cette zone
   d'édition. Chaque commentaire est lié à une population, il est ainsi possible
   d'ajouter un commentaire pour chaque population préalablement définie.
#. **sauver** :
  sauver les modifications.
#. **annuler** :
  annuler les modifications effectuées et revenir à la dernière version
  enregistrée..

Remarque :  Il n'y a pas de test sur les valeurs saisies, faire attention à
saisir un coefficient pour les migrations et les émigrations et un nombre pour
les immigrations. Il est en effet possible de saisir « 1000 » pour un
coefficient de migration sans générer d'erreur.

onglet **Saisie des migrations de populations** (Emigration)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Il s'agit de la proportion de poissons migrant hors de la région.

.. image:: images/input_18_inputPopulations_tabEmigration.png

#. **sélection de la saison** :
   Liste déroulante contenant l'ensemble des saisons actuellement enregistrée
   pour l'espèce dans la pêcherie. Il faut en sélectionner une pour définir une
   émigration.
#. **sélection groupe de population à émigrer** :
   Liste déroulante contenant l'ensemble des classes de la population.
   Les émigrations sont détaillées par classes. On sélectionne ici la classe
   affectée par l'émigration à détailler.
#. **sélection zone de départ** :
   Liste déroulante des zones de départ parmi les différents aires préalablement
   définies. Il faut sélectionner un zone de départ pour la classe sélectionnée.
#. **coefficent d'émigration** :
   Il s'agit de la proportion de la classe sélectionnée affectée par l'émigration.
#. **ajouter l'émigration** :
   Une fois sélectionnés les paramètres précédemment cités, on appuye ici pour
   crééer l'émigration; les paramètres de l'émigration apparaissent comme une 
   ligne dans la table ci-dessous.
#. **les immigrations enregistrées** :
   Liste des émigrations actuellement renseignéés.
#. **supprimer une émigration** :
   Pour supprimer une émigration précedemment ajoutée. Attention cette opération
   est irréversible, une fois supprimée la migration ne sera plus récupérable.
#. **commentaires** :
   Des informations complémentaires peuvent être ajoutées grâce à cette zone
   d'édition. Chaque commentaire est lié à une population, il est ainsi possible
   d'ajouter un commentaire pour chaque population préalablement définie.
#. **sauver** :
  sauver les modifications sur les émigrations.
#. **annuler** :
  annuler les modifications effectuées et revenir à la dernière version
  enregistrée.

onglet **Saisie des migrations de populations** (Immigration)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Il s'agit de la proportion de poissons migrant d'en dehors de la région vers la région.

.. image:: images/input_19_inputPopulations_tabImmigration.png

#. **sélection de la saison** :
   Liste déroulante contenant l'ensemble des saisons actuellement enregistrée
   pour l'espèce dans la pêcherie. Il faut en sélectionner une pour définir une
   immigration.
#. **sélection groupe de population à immigrer** :
   Liste déroulante contenant l'ensemble des classes de la population.
   Les immigrations sont détaillées par classes. On sélectionne ici la classe
   affectée par l'immigration à détailler.
#. **sélection zone d'arrivée** :
   Liste déroulante des zones d'arrivé parmi les différents aires préalablement
   définies. Il faut sélectionner un zone d'arrivée pour la classe sélectionnée.
#. **coefficent d'immigration** :
   il s'agit de la proportion de la classe sélectionnée affectée par l'immigration.
#. **ajouter l'immigration** :
   Une fois sélectionnés les paramètres précédemment cités, on appuye ici pour
   crééer l'immigration; les paramètres de l'immigration apparaissent comme une 
   ligne dans la table ci-dessous.
#. **les immigrations enregistrées** :
   Liste des immigrations actuellement renseignéés.
#. **supprimer une immigration** :
   Pour supprimer une immigration précedemment ajoutée. Attention cette opération
   est irréversible, une fois supprimée la migration ne sera plus récupérable.
#. **commentaires** :
   Des informations complémentaires peuvent être ajoutées grâce à cette zone
   d'édition. Chaque commentaire est lié à une population, il est ainsi possible
   d'ajouter un commentaire pour chaque population préalablement définie.
#. **sauver** :
  sauver les modifications sur les immigrations.
#. **annuler** :
  annuler les modifications effectuées et revenir à la dernière version
  enregistrée.

onglet **Saisie des migrations de populations** (par équation)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: images/input_20_inputPopulations_tabMigrationEquation.png

#. **sélection de la saison** :
   Liste déroulante contenant l'ensemble des saisons actuellement enregistrée
   pour l'espèce dans la pêcherie. Il faut en sélectionner une pour définir une
   migration-émigration ou immigration.
#. pour utiliser le mode équation, il faut cocher ici.
#. **sélection équation de migration** :
   Liste déroulante contenant l'ensemble des équations de migration connues
   dans la pêcherie.
#. **éditeur équation de migration** :
   Pour éditer l'équation de migration sans passer par l'éditeur d'équations 
   externe.
#. **sélection équation de émigration** :
   Liste déroulante contenant l'ensemble des équations d'émigration connues
   dans la pêcherie.
#. **éditeur équation de émigration** :
   Pour éditer l'équation d'émigration sans passer par l'éditeur d'équations 
   externe.
#. **sélection équation de immigration** :
   Liste déroulante contenant l'ensemble des équations d'immigration connues 
   dans la pêcherie.
#. **éditeur équation de immigration** :
   Pour éditer l'équation d'immigration sans passer par l'éditeur d'équations
   externe.
#. **save as model** pour équation de migration
   pour sauver une équation de type migration dans les scripts d'IsisFish (formules/migration TODO)
   Si la fonction créée peut a priori être réutilisée pour d'autres population
   il peut être utile de la sauver comme modèle en utilisant ce bouton.
#. **open editor** pour équation de migration :
   pour ouvrir l'éditeur d'équation de migration (voir figure inputEquation_XX_migration).
#. **save as model** pour équation d'émigration :
   pour sauver une équation de type émigration dans les scripts d'IsisFish (formules/emigration TODO)
   Si la fonction créée peut a priori être réutilisée pour d'autres population
   il peut être utile de la sauver comme modèle en utilisant ce bouton.
#. **open editor** pour équation d'émigration :
   pour ouvrir l'éditeur d'équation d'émigration (voir figure inputEquation_XX_emigration).
#. **save as model** pour équation de immigration :
   pour sauver une équation de type immigration dans les scripts d'IsisFish (formules/immigration TODO)
   Si la fonction créée peut a priori être réutilisée pour d'autres population
   il peut être utile de la sauver comme modèle en utilisant ce bouton.
#. **open editor** pour équation de immigration :
   pour ouvrir l'éditeur d'équation de migration (voir figure inputEquation_XX_immigration).
#. **sauver**
   sauver les modifications sur les équations.
#. **annuler** :
   pour annuler les modifications effectuées et revenir à la dernière version enregistrée..


7. **saisie des engins**
========================

La saisie d'un engin se compose de deux onglets:

#. **caractéristiques** : pour renseigner les caratéristiques générales de l'engin.
#. **sélectivité** : pour renseigner les caractéristisques de sélectivité de
  l'engin (assocation equation de sélectivite - pouplation).

**caractéristiques d'un engin**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: images/input_21_inputEngins.png

#. **nouveau engin** :
   On commence toujours par créer l'engin, avant de renseigner ces caractéristiques.
#. **nom de l'engin** :
   le nom de l'engin (il faut utiliser de préférence un nom standardisé comme
   pour tous les objets de la pêcherie).
#. **unité d'effort** :
   pour renseigner l'unité dans laquelle est mesurée l'effort.
#. **facteur de standardisation** :
   facteur permettant de standardiser l'effort de pêche entre les différents
   engins (réel positif).
#. **paramètre technique** : TODO ?
#. **type de valeur** :
   pour définir le type des valeurs de l'intervalle renseigné ci-dessous.
#. **intervalle de valeurs** :
   Valeurs possibles du paramètres est l'intervalle des valeurs pouvant être
   prises par le paramètre technique. Cela peut se présenter sous la forme
   d'une liste de valeurs (pour les paramètres discrets) ou d'un intervalle
   (pour les paramètres continus).
   Utiliser un tiret pour saisir un intervalle de valeurs, et un point virgule
   pour une liste.
   Ex:
   80-100
   ou 10;30;50
   ou petit;moyen;grand
#. **commentaires** :
   Pour permettre de saisir des commentaires propres à cet engin. Ils seront
   sauvegarder en base lors de la sauvegarde de l'engin.
#. **sauver** :
  sauver les modifications apportés sur l'engin.
#. **annuler** :
  annuler les modifications effectuées depuis la dernière suavegarde.
#. **supprimer** :
  pour supprimer un engin de la pêcherie. Attention cette opération est
  irréversible, une fois supprimé, l'engin n'est pas récupérable.
#. **engins dans la navigation** :
  positionnement de l'engin dans les objets de la pêcherie.
#. **continuer** vers les métiers  :
  Une fois le paramétrage de l'engin terminé, continuer vers la prochaine étape
  de la configuration de la pêcherie : les métiers.

**sélectivité**
~~~~~~~~~~~~~~~

L'équation de sélectivité définie pour chaque espèce pouvant être capturée
par l'engin.

La sélectivité doit être saisie pour chaque population susceptible d'être
capturée par l'engin.

.. image:: images/input_22_inputEnginsSelectivity.png

#. **sélection de la population** :
   Dans cette liste déroulante, sont représentées les populations connues de la
   pêcherie.
#. **sélection de l'équation de sélectivité** :
   Dans cette liste déroulante, on retrouve l'ensemble des équation de
   sélectivités connues dans la pêcherie. Quand vous sauvegarderez l'engin,
   l'équation utilisée sera ajoutée ici (s'il s'agit d'une nouvelle équation).
   Si l'équation de sélectivité à appliquer à la population a déjà été saisie
   pour une autre population, la sélectionner parmi les équation existantes.
#. **éditeur de l'équation de sélectivité** :
   pour éditer dans la fenêtre de configuration de l'engin l'équation de
   sélectivité sans ouvrir l'éditeur d'équation.
#. **sauver comme modèle** :
   permet de sauvegarder une équation comme modèle, qui peut être ensuite
   réutilisée ailleurs.
#. **ouvrir dans l'éditeur d'équation de sélectivité** :
   pour éditer dans l'éditeur d'équation l'équation de sélectivité.(TODO figure)
#. **ajouter** :
   Permet d'ajouter une asosciation équation de sélectivité - population.
#. **liste ajoutés** :
  On retrouve ici l'ensemble des associations équations de sélectivité - pouplation
  ajoutés dans la pêcherie pour cet engin.
#. **supprimer** :
  Pour supprimer une asosciation équation de sélectivité- population
  précédemment ajouté à l'engin.
#. **sauver** :
  sauver les modifications.
#. **annuler** :
  annuler les modifications effectuées.
#. **engins dans la navigation** :
  position de l'engin dans l'arborescence de naivgation des objets de la pêcherie.
#. **continuer** vers les métiers
   Une fois le paramétrage de l'engin terminé, continuer vers la prochaine étape
   de la configuration de la pêcherie : les métiers.

8. **saisie des métiers**
=========================

Trois différentes feuilles de saisie sont disponibles afin de décrire les métiers:
#. **caractéristiques** : où le métier est décrit.
#. **Saison-Zones** : où les saisons où sont spécifiées les saisons et zones
   correspondant à la description du métier.
#. **Espèces capturables** : où sont décrites les espèces capturées.

**caractéristiques du métier**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: images/input_23_inputMetiers.png

#. **nouveau** :
   On commence toujours par ajouter un métier avant de renseigner ces caractéristiques.
   Une fois ajouté, le métier apparaît dans la zone de navigation des objets de
   la pêcherie.
#. **nom du métier** :
   Le nom donné pour ce métier (comme pour tous les objets, il faut que ce soit
   un nom normé, puisque sauvé en base).
#. **sélection engin** :
   Dans cette liste déroulante, on retrouve l'ensebmel des engins connus de la
   pêcherie. Il faut en sélectionner un car un métier est lié à un engin.
#. **valeurs** :
   La valeur du paramètre contrôlable est la valeur du paramètre technique
   utilisée par le métier. Cette valeur doit être sélectionnée parmi les valeurs
   possibles du paramètre.
#. **commentaires** :
   Des informations complémentaires peuvent être ajoutées grâce cette zone.
   Chaque commentaire est lié à un métier, il est ainsi possible d'ajouter un
   commentaire pour chaque métier préalablement défini.
#. **sauver** :
  Pour sauvegarder ce métier en base.
#. **annuler ** :
  Pour annuler toutes les modifications et revenir à la dernière sauvegarde.
#. **supprimer** :
  Permet de supprimer un métrier de la pêcherie. Attention cette opération est
  irréversible, une fois le métier supprimé, il ne sera plus récupérable.
#. **métier** dans la zone de navigation :
  Positionnement du métier dans la navigation des objets de la pêcherie.
#. **continuer** vers les types de trajets :
  Une fois terminée la configuration des métiers, on passe à la prochaine étape
  de la saisie, à savoir configuration des types de trajets (ou marées).


**Saison - zones**
~~~~~~~~~~~~~~~~~~

.. image:: images/input_24_inputMetiersSeasonZone.png

#. **nouveau** :
  Toujours commencer par ajouter un nouvelle association saison-zone. Une fois
  celle-ci ajoutée, une nouvelle entrée est ajouté dans la liste de sélection
  des saisons.
#. **sélection saison** :
  Dans cette liste déroulante sont représentées les associations saisons-zones
  ajoutées pour ce métier.
#. **définition saison** :
  Définir un ensemble de mois durant lesquels le métier est pratiqué en utilisant
  le curseur et les cellules jaunes/bleue (jaune: le mois est inclus dans la
  saison, bleu: il ne l'est pas).
#. **zones sélectionnées** :
  Dans cette liste, on retrouve l'ensemble des zones de la pêcherie.
  Sélectionner les Zones dans lesquelles le métier est pratiqué pendant la
  saison définie parmi les zones prédéfinies.  
#. **commentaires** :
   Des informations complémentaires peuvent être ajoutées grâce à cette zone.
   Chaque commentaire est lié à une saison, il est ainsi possible d'ajouter un
   commentaire pour chaque saison préalablement définie.
#. **sauver** :
  Pour sauvegarder en base, les modifications apportées à l'écran.
#. **annuler ** :
  Pour annuler les modifications depuis la dernière sauvegarde.
#. **supprimer** :
  Permet de supprimer une association Saison-Zone pour le métier en cours de modification.
  Attention, cette opération est irréversible, une fois supprimée, l'association
  n'est plus récupérable.
#. **métier** dans la zone de navigation :
  Positionnement du métier dans la navigation des objets de la pêcherie.
#. **continuer** vers les types de trajets :
  Une fois terminée la configuration des métiers, on passe à la prochaine étape
  de la saisie, à savoir configuration des types de trajets (ou marées).

**espèces capturables**
~~~~~~~~~~~~~~~~~~~~~~~

Le **Facteur de ciblage** quantifie la manière avec laquelle un métier cible
la population. Cela comprend la puissance de pêche, le calibrage des engins,
etc. (voir les différents articles sur ISIS-Fish).
Pour une population donnée, il peut être vu comme le ratio des captures entre
deux métiers pêchant au même endroit, au même moment avec le même engin.

.. image:: images/input_25_inputMetierCapturability.png                

#. **sélection saison** :
   Dans cette liste déroulante appraît l'ensemble des saisons définies dans
   l'onglet précédent. Il faut sélectionner une.
#. **sélection espèce** :
   Dans cette liste déroulante, on retrouve l'ensemble des espèces de pouplation
   connues parmi le spopulations du cas d'étude..
#. **sélection équation Facteur de ciblage**
   Dans cette liste déroulante, on retrouve l'ensemble des facteurs cibles.
   Il faut en sélectionner une.
#. **éditeur équation Facteur **
   Pour éditer une équation de **facteur cible** sans utiliser l'éditeur externe
   d'équation.
#. **sauver comme modèle **
   Permet de sauvegarder l'équation saisie dans la zone précédente en tant que
   modèle et la rendre ains réutilisable.
#. **ouvrir dans l'éditeur **
   Permet d'ouvrir l'éditeur externe d'équation de facteur cible.
#. **espèce principale pour le métier **
   Permet de spécifier si l'espèce actuellement sélectionnée est une capture
   principale pour ce métier.
#. **ajouter** :
  Une fois sélectionnée la saison, l'espèce et le facteur cible, on appuie sur
  ce bouton afin d'ajouter l'association. Elle sera alors ajoutée dans la table
  juste en dessous.
#. **espèces ajoutées** :
  On retrouve ici l'ensemble des associations ajoutées pour ce métier.
  Tip : en cliquant sur la cellule facteur cible d'une des entrées l'éditeur
  d'équations externe s'ouvre avec l'équation correspondante.
#. **sauver** :
  Pour sauvegarder en base, les modifications apportés à l'écran.
#. **annuler ** :
  Pour annuler les modifications depuis la dernière sauvegarde.
#. **supprimer** :
  Permet de supprimer une association Saison-Zone pour le métier en cours.
  Attention, cette opération est irréversible, une fois supprimée, l'association
  n'est plus récupérable.
#. **continuer** vers les types de trajets :
   Une fois terminée la configuration des métiers, on passe à la prochaine étape
   de la saisie, à savoir configuration des types de trajets (ou marées).

Remarque : Il n'y a pas de test sur le facteur de ciblage, il est possible
d'ajouter plusieurs facteurs de ciblage pour le même métier et la même espèce
pour la même période sans générer d'erreur.


9. **saisie des marées**
========================

.. image:: images/input_26_inputTripType.png

#. **nouveau** type de trajet :
   On commence tjours par ajouter un nouveau type de trajet avant d'ne modifier
   ces caractéristiques. Une fois le type de trajet crée, il apparaît dans la
   zone de navigation des objets de la pêcherie avec un nom initialement générique.
#. **nom** du type de trajet :
   Pour modifier le nom d'un type de trajet
#. **durée** du trajet :
   Pour indiquer le nombre de jours en mer par marée.
#. **temp minimal entre deux voyages** :
  Pour indiquer la durée minimale entre deux voyages pour ce type de trajet.
#. **commentaires** :
   Des informations complémentaires peuvent être ajoutées grâce à cette zone.
   Chaque commentaire est lié à un type de marée, il est ainsi possible d'ajouter
   un commentaire pour chaque type de marée préalablement défini.
#. **sauver** :
   Pour sauvegarder en base les caractéristiques de ce type de trajet.
#. **annuler** :
   Permet d'annuler les modfications depuis la dernière sauvegarde du type de
   trajet.
#. **supprimer** :
   Pour supprimer ce type de trajet de la pêcherie. Attention cette opération
   est irréversible, une fois un type de trajet supprimé, il ne sera plus
   récupérable.
#. **continuer** vers les types de navires
   Une fois terminées les modifications sur les types de trajets de la pêcherie,
    on passe à l'étape suivante de la configuration : les types de navires.

10. **saisie des types de navires**
===================================

.. image:: images/input_27_inputVesselTypes.png

#. ** nom du type de navire** :
   Pour indiquer le nom du type de navire. Ce nom doit être normalisé comme le
   nom de tous les objets de la pêcherie.
#. **longueur** :
   Longueur moyenne (en m) d'un bateau appartenant à ce type de bateau.
#. **vitese** :
   Permet de définir la vitesse moyenne de ce type de navire (en km.h-1).
#. **duréee maximale du trajet** :
   Pour définir la durée maximale du trajet en jours.
#. **intervalle d'activité**:
   Pour définir le temps d'activité d'un bateau TODO Revoir
#. **taille minimale d'équipage** :
   Pour définir la taille minimale de l'équipage d'un navire de ce type.
#. **cout d'un trajet en fuel** :
   Permet de définir le coût en fuel d'un trajet pour un navire de ce type.
#. **type de trajet** :
   Dans cette liste apparaissent tous les type de trajets connus dans la pêcherie.
   Un type de trajet doit être sélectionné.
#. **commentaires** :
   Des informations complémentaires peuvent être ajoutées grâce à cette zone.
   Chaque commentaire est lié à un type de bateau, il est ainsi possible d'ajouter
   un commentaire pour chaque type de bateau préalablement défini.
#. **sauver** :
   Pour sauver le type de navire.
#. **nouveau** :
   Permet de construire un nouveau type de navire. C'est l'action à réaliser
   en premier avant toute modification des caractéristiques de ce type de navire.
   Une fois crée, le type de navire apparaît dans la zone de navigation des
   objets de la pêcherie.
#. **annuler** :
   Pour annuler les modifications effectuées et revenir à la dernière version
   sauvegardée.
#. **supprimer** :
   Pour supprimer le type de navire de la base. Attention, cette opération est
   irreversible et une fois supprimé, le type de navire n'est plus récupérable.
#. **continuer** vers les ensembles de navires :
   Une fois terminées les modifications sur les types de navires de la pêcherie,
   on passe à l'étape suivante de la configuration : les flotilles.

11. **saisie des flotilles**
============================

Deux onglets permettent de décrire les flottilles:
#. **Caractéristiques** qui permet de décrire les caractéristiques de la flottile
#. **DescriptonEffortParMétier** qui permet de détailler les différents métiers 
   possibles et les coûts associés.

**caractéristiques de la flotille**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: images/input_28_inputVessels.png

#. **nouveau** :
   On commence toujours par créer une flotille avant d'en modifier les
   caractéristiques. Une fois crée, la flotille apparaît dans la zone de
   nagivation des objets de la pêcherie avec un nom générique.
#. **nom du navire** :
   Pour modifier le nom de la flotille.
#. **port d'attache** :
   Dans cette liste déroulante apparaissent les différents ports connus dans la
   pêcherie. Il faut en sélectionner un port d'attache par flotille.
#. **type de navire** :
   Dans cette liste déroulante appraissent les différents type de niavres
   connus dans la pêcherie. Il faut en sélectionner pour définir le type de
   navires dont est composée le flotille.
#. **nombre de navires** :
   Pour indiquer le nombre de navire dont est composé la flotille.
#. **coût fixe** :
   Pour renseigner les **coûts fixes** imputés à chaque propriétaire d'un
   bateau de la flottille en euros par an.
#. **commentaires** :
   Des informations complémentaires peuvent être ajoutées grâce à cette zone.
   Chaque commentaire est lié à une flottille, il est ainsi possible d'ajouter
   un commentaire pour chaque flottille préalablement définie.
#. **sauver** :
   Pour sauvgarder les modifications effectuées sur la flotille.
#. **annuler** :
   Permet d'annuler les modifications effectuées depuis la dernière sauvegarde.
#. **supprimer** :
   Pour supprimer la flotille de la pêcherie. Attention cette opération est
   irreversible, une fois supprimée la flotille ne sera pus récupérable.
#. **flotilles** dans la navigation :
   Position de la flotille dans la zone de navigation des objet de la pêcherie.
#. **continuer** vers les stratégies :
   Une fois terminées les modifications sur les flotilles de la pêcherie, on
   passe à la dernière étape de la configuration : les stratégies.

**descriptions des efforts**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: images/input_29_inputVesselsEffortDescription.png

#. **sélection du métier** :
   Dans cette liste apparaissent la liste des métiers connues dans le pêcherie.
   Il faut en sélectionner en pour ajouter un type d'effort sur ce métier.
#. **ajouter** :
   On ajoute les différents métiers susceptibles d'être opérés lors d'une marée
   en sélecionnant le métier dans la liste des différents métier.
#. **opération de pêche ** :
   Pour définir le nombre d'opération de pêche par jour pour ce métier et
   cette flotille.
#. **durée de la pêche ** :
   Pour définir la durée d'une opération de pêche en heure pour ce métier et
   cette flotille.
#. **nombre d'engins par opération ** :
   Pour indiquer le nombre d'engins à utiliser par opération de pêche pour ce
   métier et cette flotille.
#. **taille de l'équipage ** :
   Pour indiquer la taille de l'équipage d'une pêche pour ce métier et cette flotille.
#. **coût unitaire de la pêche ** :
   Pour indiquer le coût unitaire de la pêche, à savoir le carburant, l'huile
   et glace pour un bateau de cette flotille pratiquant ce métier.
#. **salaire d'équipage fixe ** :
   Pour indiquer la part fixe du salaire de l'équipage de cette flotille
   pratiquant ce métier (en euros par mois).
#. **coût de l'alimentation pour l'équipage ** :
   Pour indiquer le coût de l'alimentation pour l'équipage pour un bateau de
   cette flotille pratiquant ce métier.
#. **taux de partage de l'équipage ** :
   Pour indiquer le Part variable du salaire de l'équipage pour un bateau de
   cette flotille pratiquant ce métier.
#. **coût de maintenance de l'engin** :
   Pour renseigner le coût de réparation et de maintenance des engins de pêche
   par jour pour un bateau de cette flotille pratiquant ce métier (en euros
   par jour).
#. **coût de revient à terre** :
   Pour renseigner le coût de débarquement (taux) lié au métier et au port
   de la stratégie.
#. **autres coûts ** :
   Pour indiquer les autres coûts d'exploitation de cette flotille pratiquant
   ce métier (en euros par heure).
#. **sauver** :
   Pour sauvegarder les modifications effectuées sur l'engin.
#. **annuler** :
   Pour annuler les modifications effectuées depuis la dernière sauvegarde.
#. **supprimer** :
   Pour supprimer un métier de la pêcherie. Attention cette opération est
   irréversible, une fois supprimé, le métier n'est plus récupérable.
#. **continuer** vers les stratégies :
   Une fois terminées les modifications sur les flotilles de la pêcherie,
   on passe à la dernière étape de la configuration : les stratégies.

Remarque : Tous les paramètres économiques n'ont pas besoin d'être renseignés
afin de faire tourner une simulation, il est possible de faire tourner un
simulation (avec uniquement la partie biologique du modèle), en laissant les
coûts à zéro (valeurs par défaut).

12. **saisie des stratégies**
=============================

Deux onglets permettent de décrire les stratégies:
#. **cararctéristiques** de la stratégie
#. **description des informations mensuelles**

**caractéristiques de la stratégie**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: images/input_30_inputStrategies.png

#. **nouveau** :
   On commence toujours par crée une stratégie avant d'en modifier les
   caractéristiques. Une fois créée, la stratégie apparaît dans la zone de
   navigation des objets de la pêcherie avec un nom générique.
#. **nom de la stratégie** :
   Pour indiquer le nom de la stratégie, comme les autres noms d'objets dans
   la pêcherie, il est préférable que le nom soit normalisé.
#. **sélection de la flotille** :
   Dans cette liste déroulate, on affiche l'ensemble des flotilles connues pour
   la pêcherie. Il faut en sélectionner une pour l'associer à la stratégie
   courante.
#. **proportion des ensembles de navires** :
   Pour renseigner la proportion du nombre de bateaux de la flotille qui suivent
   cette stratégie.
#. **commentaires** :
   Des informations complémentaires peuvent être ajoutées grâce à cette zone.
   Chaque commentaire est lié à une stratégie, il est ainsi possible d'ajouter
   un commentaire pour chaque stratégie préalablement définie.

#. **sauver** :
   Pour sauvegarder les modifications effectuées sur la stratégie.
#. **annuler** :
   Pour annuler les modifications effectuées depuis la dernière sauvegarde.
#. **supprimer** :
   Pour supprimer une stratégie de la pêcherie. Attention cette opération est
   irréversible, une fois supprimée, la stratégie n'est plus récupérable.
#. **stratégies** dans la navigation :
   Position de la stratégie dans la zone de navigation des objets de la pêcherie.


**descriptions des informations mensuelles**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: images/input_31_inputStrategiesMonthInfo.png

Cet écran permet de décrire par mois la stratégie. On retrouve douze zones pour
configurer chaque mois. On décrit ici la configuration d'un mois.

#. **sélection du type de trajet** :
   Dans cette liste déroulante sont affichés les types de trajet connus dans la
   pêcherie. Il faut en sélectionner un afin de pouvoir configurer pour
   le métier associé à l'engin pour ce mois. Une fois sélectionné, on voit
   s'afficher juste en dessous le nombre de trajets possibles calculé pour ce mois.
#. **nombre de jours d'inactivités** :
   Une fois un type de trajet sélectionné, on peut renseigner ici le nombre
   minimum de jours d'inactivité,  correspondant au nombre minimal de jours
   d'inactivité dans le mois pour un bateau pratiquant cette stratégie le mois
   donné.
#. **proportion** :
   Pour renseigner la proportion du temps passé par les bateaux de la stratégie
   pour un métier et un mois doné. (réel entre 0 et 1). 
#. **sauver** :
   Pour sauvegarder les modifications effectués sur cet écran.
#. **annuler** :
   Pour annuler les modifications effectués sur cet écran.

**Les équations renceontrées dans les pêcheries**
================================================

On donne ici les figures des différents éditeurs d'équations rencontrés dans
la saisie des population d'un pêcherie.

**équation de croissance**
~~~~~~~~~~~~~~~~~~~~~~~~~~
.. image:: images/inputEquation_00_Growth.png

**équation de croissance inverse**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. image:: images/inputEquation_01_GrowthReverse.png

**équation de taux de mortalité naturelle**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. image:: images/inputEquation_02_NaturalDeathRate.png

**équation de poids principal**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. image:: images/inputEquation_03_MeanWeight.png

**équation de prix principal**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. image:: images/inputEquation_04_Price.png

**équation de migration**
~~~~~~~~~~~~~~~~~~~~~~~~~
.. image:: images/inputEquation_05_Migration.png

**équation d'émigration**
~~~~~~~~~~~~~~~~~~~~~~~~~
.. image:: images/inputEquation_06_Emigration.png

**équation d'immigration**
~~~~~~~~~~~~~~~~~~~~~~~~~~
.. image:: images/inputEquation_07_Immigration.png

**équation de reproduction**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. image:: images/inputEquation_08_GearSelectivity.png
