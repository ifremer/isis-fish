.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
=======================
L'interface utilisateur
=======================

Fenêtre principale
==================

L'interface utilisateur est constituée d'une première fenêtre qui
permet d'ouvrir d'autre fenêtre suivant l'utilisation que l'on
souhaite faire du simulateur.

.. image:: images/mainWindow.png

Cette fenêtre contient quatre icônes qui servent à ouvrir les fenêtres:

- de lancement de simulation
- de rendu des résultats
- de saisie de région
- de saisie de script, règles de gestion, export, plan d'analyse, ...

Ces fenêtres sont aussi accessible depuis le menu *fenêtre*.

Le menu fichier
---------------

Le premier sous menu *synchronisation serveur* permet de synchroniser
les fichiers de scripts avec le serveur.

Le deuxième sous menu *quitter* permet de fermer ISIS-Fish

Le menu fenêtre
---------------

Les quatres premiers sous menus sont l'équivalent des quatres icônes.

Le sous menu *liste des simulations* permet d'afficher une fenêtre
récapitulant toutes les simulations en cours avec leur états.

Le dernier menu *configuration* permet de modifier la configuration
d'ISIS-Fish. Lorsqu'on utilise cette fenêtre pour modifier la
configuration pour certain option il faut relancer ISIS-Fish pour les
nouvelles valeurs soient prises en compte, comme par exemple la
langue.

La barre de statut
------------------

La barre de statut indique à droite la mémoire utilisé puis la mémoire
actuellement aloué pour ISIS-Fish.

La zone centrale permet à ISIS-Fish de d'afficher des messages et la
partie gauche indique l'avancement de tâche en train de s'exécuter.

Lancement de simulation
=======================

La fenêtre de lancement de simulation permet de paramètrer les
simulations et de les exécuter.

.. image:: images/simulationWindow.png

Cette fenêtre est consituée de plusieurs onglets qui permettent de modifier:

- **Paramètres** de base de la simulation: identifiant, description, région,
  stratégies, populations, règles, nombre d'années
- **Script de présimulation**, pour l'activer il faut coché la
  case *utiliser un script de pré-simulation*. Le script de pré-simulation est du BSH
  exécuter avant toutes les simulations et permet de modifier la base de données.
- **Analyse de sensibilité**, pour l'activer il faut coché la case
  *Utiliser le plan d'analyse*.
- **Export des résultats** qui permet de sélectionner les scripts d'export à utiliser
  et le répertoire dans lequel on souhaite exporter.
- **Choix des résultats** qui permet de sélectionner les résultats à conserver
  pour une visualisation ultérieure via l'interface de rendu des résultats
- **Paramètres avancés** qui permet de changer le simulateur à utiliser, activer ou
  désactiver le cache, activer ou désactiver les statistiques, ajouter des
  paramètres libres récupérable dans les règles de gestion, le simulateur, ...

Si une rêgle de gestion ou un export a besoin d'un résultat qui n'est
pas sélectionné alors ce résultat est automatiquement ajouté a la
liste des résultats souhaités.

Lors du lancement de la simulation, automatiquement la date courante
sera ajouté au nom de la simulation.

Pour plus d'information sur le lancement d'une simulation vous pouvez
vous reporter à la documentation du simulation v2 chapitre 6.

rendu des résultats
===================

La fenêtre de rendu des résultats permet de visualiser les résultats
en graph, en carte, ou en nombre. Elle permet aussi de supprimer
d'ancienne simulation.

.. image:: images/resultWindow.png

Pour ouvrir une ancienne simulation, sélectionnez la simulation
souhaitée dans la combobox et cliquez sur *ouvrir une nouvelle fenêtre*.

.. image:: images/resultWindowGraph.png

.. image:: images/resultWindowMap.png

Saisie de région
================

La fenêtre saisie de régions permet de visualisées et modifiées les
régions.

vous pouvez sélectionner dans la combobox votre région pour la faire
apparaître.

.. image:: images/inputWindow.png

vous pouvez sélectionner dans la combobox votre région pour la faire
apparaître.

.. image:: images/inputWindowRegion.png

Pour plus d'information sur les différents objets et champs
disponibles vous pouvez vous reporter à la documentation du simulation
v2 chapitres 4 et 5 (http://www.ifremer.fr/isis-fish/databases/manuelFrancais1104.zip)

Saisie de script
================

La fenêtre de script permet de modifier tous les scripts:

- scripts
- simulateurs
- exports
- règles
- plans d'analyses
- modèle d'équations

.. image:: images/scriptWindow.png

Tous ces scripts sont des fichiers textes sur le système de
fichier. Il est donc possible d'utiliser n'importe quel éditeur pour
les modifiers. Voir
http://isis-fish.labs.libre-entreprise.org/v3/user/ExternalScriptEditor.html
pour plus d'explication.
