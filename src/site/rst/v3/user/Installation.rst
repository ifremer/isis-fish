.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
Installion et lancementd'ISIS-Fish
==================================

Matériel requis
---------------

Afin de pouvoir faire tourner ISIS-Fish sur votre machine, il est
nécessaire d'avoir au moins la vesion 1.6 de Java d'installée. Il est
possible de connaître la version actuellement installée sur votre
ordinateur en tapant dans une commande DOS : «java -version». Si vous
aboutissez à un message d'erreur, cela signifie que Java n'est pas
installé. Si la version installée est antérieur à la version 1.6 ou
que Java n'est pas installé, vous devez télécharger la dernière
version de Java SE Development Kit (JDK). Ceci peut se faire
directement à cette adresse : http://java.sun.com/javase/downloads/index.jsp

Comment se procurer ISIS-Fish
-----------------------------

Il est possible de télécharger ISIS-Fish à cette adresse :
http://isis-fish.labs.libre-entreprise.org/download/version3. les
fichiers contenant ISIS-Fish et tout ce qui est nécessaire à son bon
fonctionnement sont nommés : «isis-fish-x.y.z.zip» où x.y.z
correspond au numéro de la version.

Installation
------------

Une fois le zip téléchargé, il faut le dézipper dans le répertoire de
votre choix.

Sous linux vous pouvez taper::

  unzip isis-fish-x.y.z.zip

Un nouveau répertoire isis-fish-x.y.z apparaît

Fichier de lancement par défaut .bat et .sh
-------------------------------------------

Par défaut isis-fish peut allouer au maximum 512Mo de Ram pour son
utilisation. Si votre région contient de très nombreux métiers,
stratégies et populations vous pouvez devoir augmenter cette valeur
pour cela éditez le fichier *go.bat* ou *go.sh* suivant votre
plateforme (windows, Unix/Linux).

Par défaut les fichiers de lancement redirigent les sorties vers un
fichier error.txt. Si jamais un problème se produit durant
l'utilisation Isis-fish ce fichier peut vous permettre de comprendre
plus simplement le problème.

Lancer ISIS-Fish sous windows
-----------------------------

Double-cliquer sur le fichier .bat créé. Une fenêtre DOS va alors
s'ouvrir. Puis l'interface suivante dans le coin en haut à gauche de
votre écran.

.. image:: ./images/mainWindow.png

Si l'interface n'apparaît pas, cela signifie qu'une erreur s'est
produite lors du lancement. Le message d'erreur est situé dans le
fichier error.txt, mais il peut dans certains cas être difficilement
compréhensible. Si vous ne pouvez comprendre ce message, envoyé le sur
la liste utilisateur (voir chapitre 18 : Comment souscrire à la liste
utilisateur).

Lancer ISIS-Fish sous Unix/Linux
--------------------------------

Placez-vous dans le répertoire contenant le fichier go.sh et exécutez
le avec::

  ./go.sh

Option de la ligne de commande
------------------------------

Il est possible de passer des paramètres à la ligne de commande par exemple::

  ./go.sh --help

l'option --help permet d'afficher la liste des options disponibles

Voici les autres options.

:paramètre: --help
:description: affichage de l'aide
:syntaxe: --help | -h

:paramètre: --listRegion
:description: affiche la liste des régions disponibles
:syntaxe: --listRegion

:paramètre: --importRegion
:description: importe une région dans la base local, il est possible
  d'importer des régions depuis ISIS-Fish version 3 ou version 2. Suivant
  l'extension du fichier ISIS-Fish fera la bonne opération.
:syntaxe: --importRegion <v3region.zip|v2region.xml|v2region.xml.gz>

:paramètre: --importAndRenameRegion
:description: importe une région dans la base local et modifie son nom
  durant l'import.
:syntaxe: --importAndRenameRegion <v3region.zip> <'new name'>

:paramètre: --exportRegion
:description: exporte une région existante dans la base local sous la forme
  d'un fichier zip importable ultérieurement.
:syntaxe: --exportRegion <'region name'> <'fichier.zip'>

:paramètre: --listSimulation
:description: affiche la liste des simulations disponibles
:syntaxe: --listSimulation

:paramètre: --importSimulation
:description: importe une simulation dans la base local
:syntaxe: --importSimulation <v3simulation.zip>

:paramètre: --exportSimulation
:description: exporte une simulation existante dans la base local sous la
  forme d'un fichier zip importable ultérieurement.
:syntaxe: --exportSimulation <'simulation name'> <'fichier.zip'>

:paramètre: --update
:description: force la mise à jour des scripts comme lors du premier
  lancement d'ISIS-Fish
:syntaxe: --update <true|false>

:paramètre: --ui
:description: affiche ou non les interfaces utilisateurs
:syntaxe: --ui <true|false>

:paramètre: --create-ssh-key
:description: permet de créer une paire de clé ssh. Ces clés sont utilisées
  pour pouvoir modifier les scripts pour les personnes ayant un login sur
  le CVS hébergeant les sources des scripts. (Voir le chapitre CVS)
:syntaxe: --create-ssh-key

:paramètre: --ssh-key-file
:description: permet d'indiqué à ISIS-Fish d'utiliser une paire de clés ssh
  existantes pour l'accès au CVS. (Voir le chapitre CVS)
:syntaxe: --ssh-key-file <'/chemin/vers/la/cle/privée'>

:paramètre: --config
:description: permet de modifier des valeurs de configuration.
  Cela revient au même que de modifier le fichier $HOME/.isis-config-3, ou
  utiliser l'interface depuis le menu *fenêtre->configuration*.
:syntaxe: --config <nom> <valeur>

Liste des options disponibles:

:option: compileDirectory
:description: répertoire utilisé pour la compilation des scripts
:type: chemin vers un répertoire
:exemple: /tmp/isis-build

:option: language
:description: La langue à utiliser
:type: ISO Language Code http://www.loc.gov/standards/iso639-2/englangn.html
:exemple: fr

:option: country
:description: La variate pays pour la langue
:type: ISO Country Code http://www.iso.ch/iso/en/prods-services/iso3166ma/02iso-3166-code-lists/list-en1.html
:exemple: FR

:option: database
:description: répertoire utilisé pour stocker les informations
:type: chemin vers un répertoire
:exemple: /home/poussin/isis-database-3

:option: defaultSimulator
:description: nom du fichier à utiliser comme simulateur
:type: nom d'un fichier .java contenu dans le répertoire $database/simulators
:exemple: DefaultSimulator.java

:option: javadocUrl
:description: URL pointant vers la javadoc du simulateur
:type: URL
:exemple: http://isis-fish.labs.libre-entreprise.org/apidocs/

:option: defaultExportDirectory
:description: Répertoire ou les exports de simulation sont placé par défaut
:type: chemin vers un répertoire
:exemple: /home/poussin/isis-export

:option: defaultExportNames
:description: la liste des noms des exports par défaut à utiliser
:type: liste séparé par des virgules
:exemple: Abundances.java,Biomasses.java

:option: defaultResultNames
:description: la liste des résultats que l'on souhaite conserver durant la
  simulation pour les visualiser ultérieurement via l'interface de rendu
  des résultats.
:type: liste séparé par des virgules
:exemple: matrixDiscardsWeightPerStrMet,matrixPrice

:option: defaultMap
:description: la carte à utiliser par défaut pour nouvelle région
:type: le chemin d'un fichier de carte .shp sans l'extension
:exemple: maps/vmap_area_thin

:option: defaultTagValue
:description: la liste des tags values à utiliser par défaut pour les simulations
:type: liste séparé par des virgules de couple "nom":"valeur"
:exemple: "ecoResult":"true","maVal":"truc"

:option: cvsHostName
:description: le nom du serveur contenant l'arbre CVS des données
:type: le nom d'un serveur ou une ip
:exemple: labs.libre-entreprise.org

:option: cvsRepository
:description: le répertoire racine du CVS sur le serveur
:type: chemin vers un répertoire
:exemple: /cvsroot/isis-fish

:option: cvsDataBase
:description: le nom du module CVS contenant les données sur le serveur
:type: String
:exemple: isis-fish-data

:option: cvsHost
:description: fichier contenant les cles des serveurs CVS
:type: chemin d'un fichier xml
:exemple: /home/poussin/.isis-ssh-host.xml

:option: cvsUserName
:description: le nom de l'utilisateur pouvant accéder au CVS
:type: login
:exemple: anonymous
:exemple: bpoussin

:option: cvsSsh2Connexion
:description: indique si l'on utilise ou non un accès sécurisé au CVS.
  Cela n'est pas utile pour le compte anonymous et nécessite une clé SSH
:type: boolean
:exemple: true

:option: cvsKeyFile
:description: le fichier contenant la clé privée SSH
:type: chemin vers un fichier
:exemple: /home/poussin/.ssh/id_dsa

:option: smtpServer
:description: le nom du serveur SMTP à utiliser pour envoyer des mails.
:type: nom d'un serveur ou ip
:exemple: smtp.codelutin.com

:option: localSimulator
:description: valeur par défaut utilisée pour savoir si les simulations que l'on
  exécute doivent être faites localement ou sur un serveur de simulation
:type: boolean
:exemple: true

:option: simulatorServer
:description: URL permettant de contacter le serveur de simulations
:type: URL
:exemple: http://simulateur.ifremer.fr:9090

:option: simulatorServerLogin
:description: le login à utiliser pour le serveur de simulations
:type: login
:exemple: bpoussin

:option: simulatorServerPassword
:description: le mot de passe à utiliser pour le serveur de simulations
:type: mot de passe
:exemple: guest

Premier lancement et fichier de configuration
---------------------------------------------

Lors du premier lancement ISIS-Fish vous posera un certain nombre de
question.

Si vous souhaitez les modifier ultérieurement il faut éditer le
fichier $HOME/.isis-config-3.

$HOME est une variable d'environnement sous Unix/Linux qui pointe vers
le répertoire utilisateur de la personne logguée. Sous Windows
l'utilisateur à aussi un espace de travail personnel, mais son
emplacement varie suivant la version de Windows utilisé.

Données et scripts initiaux
---------------------------

Lors du premier lancement il est conseillé d'être connecté à Internet,
car ISIS-Fish récupère une région de démonstration et les scripts de
simulation, export, ...

Si vous ne pouvez pas être connecté, depuis la version 3.0.16 il vous
est possible de récupérer à l'adresse:
http://isis-fish.labs.libre-entreprise.org/download/version3 le fichier se
nommant isis-database.x.y.z.zip où x.y.z correspond au numéro de la version.

Décompressez ce fichier et placé le répertoire obtenu dans votre répertoire
$HOME. Tous les scripts normalement récupéré par Isis-fish se trouvent dans
ce répertoire. La région de démonstration n'est pas valide et vous ne pouvez
pas l'utiliser.

Mettre à jour les scripts
-------------------------

Les scripts de simulation sont modifiés indépendament de la version
d'ISIS-Fish. Il est donc possible de mettre à jour les scripts sans
modifier sa version d'ISIS-Fish. Dans certain cas, les nouveaux
scripts ne fonctionneront qu'avec une nouvelle version d'ISIS-Fish,
par exemple en cas de modification de la structure des régions (ajout
de champs à un objet).

Pour mettre à jour les scripts, il faut dans la fenêtre principale
aller dans le menu **fichier->Synchronisation serveur**.

Vous devez voir apparaitre une fenêtre vous invitant à sélectionner
les éléments à mettre à jour ou les nouveaux éléments à récupérer.

.. image:: images/synchroWindow.png

Dans la zone texte du bas vous pouvez voir les changements évectués
sur le fichier sélectionné.
