.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
==============================
Connexion à **isis-fish-data**
==============================

Cette documentation présente le nouveau module **isis-fish-data** et comment
configurer **isis-fish** pour s'y connecter.

**Isis-fish-data**
------------------

A partir de la version **3.1.0**, les données d'Isis-fish **Isis-fish-data**
forme un module indépendant, hébergé sur le labs
(https://labs.libre-entreprise.org/projects/isis-fish-data).

De plus le protocole de versionning utilisé est désormais **SVN** (aka Subservion)
remplaçant **CVS** utilisé auparavant.

Accès en lecture sur Isis-fish-data
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Comme auparavant il est toujours possible de se connecter au serveur de données
de manière anonyme et en lecture seule.

Accès en écriture sur Isis-fish-data
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A partir de la version 3.1.0, Isis-Fish permet les accès en écriture sur le
serveur de donnés hébergeant isis-fish-data.

Le protocole **ssh2** est utilisé pour communiquer de manière sûre avec le serveur
distant.

Pour pouvoir configurer votre accès au serveur de données, vous devez au
préalable avoir un compte sur le **Labs** (http://...), et posséder une clef ssh
enregistrée sur le serveur du **Labs**.

Pour obtenir un compte sur le labs, voir TODO.

Isis-Fish vous assiste dans la génération et/ou le dépot de clef publique sur le
serveur du labs.

A noter que le dépot de votre clef publique ssh sur le labs n'est pas instantanné,
cette tâche est effectuée une fois par heure sur le serveur, vous ne pourrez pas
utiliser votre accès en écriture durant ce temps. 

La première utilisation d'Isis-Fish v 3.1.0
-------------------------------------------
Cette section décrit comment configurer votre accès au serveur de données en
décrivant l'écran que vous rencontrerz lors de votre premier lancement d'Isis-Fish.


Sur cet écran, on peut aussi générer une clef ssh et de la déposer sur le labs.
 
Dans cette interface de configuration, le bouton de validation n'est accessible
que lorsque vous avez saisi toutes les informations requises.

Un champ dont le libelé est en rouge n'est pas valide. 

A noter que si la configuration n'est pas valide, IsisFish ne pourra pas démarrer.

configurer un accès en **lecture seule**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: images/isisFishData_00_firstUsageReadOnly.png

#. **droit d'accès au serveur** :
   pour configurer en lecture seule, sélectionner l'option **lecture seule**.
#. **nom** de l'utilisateur :
   renseigner le nom de l'utilisateur dans IsisFish (servira notamment pour
   l'envoi de courriel).
#. **prénom** de l'utilisateur :
   renseigner le prénom de l'utilisateur dans IsisFish (servira notamment pour
   l'envoi de courriel).
#. **courriel** de l'utilisateur :
   renseigner le courriel de l'utilisateur dans IsisFish (utilisé notamment pour
   envoyer les simulations par courriel).
#. **appliquer** :
   pour accepter les modifications effectuées. A noter que le bouton est grisé
   tant que la configuration n'est pas valide.
#. **annuler** :
   pour annuler les modifications effectuées.

configurer un accès en **lecture-écriture**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: images/isisFishData_01_firstUsageReadWrite.png

#. **droit d'accès au serveur** :
   pour configurer en lecture-écriture, sélectionner l'option **lecture-écriture**.
   Une fois cette option choisie, tous les champs en dessous sont accessibles.
#. **nom** de l'utilisateur :
   renseigner le nom de l'utilisateur dans IsisFish (servira notamment pour
   l'envoi de courriel).
#. **prénom** de l'utilisateur :
   renseigner le prénom de l'utilisateur dans IsisFish (servira notamment pour
   l'envoi de courriel).
#. **courriel** de l'utilisateur :
   renseigner le courriel de l'utilisateur dans IsisFish (utilisé notamment pour
   envoyer les simulations par courriel).
#. **nom utilisateur** sur le serveur :
   c'est le login de l'utilisateur sur le serveur de données (login de connexion
   du labs).
#. **pas de passphrase** :
   cochez ici pour indiquer que votre clef privée ssh ne comporte pas de
   passphrase. Attention si votre clef privée ssh contient une passphrase et que
   vous cochez cette case, IsisFish ne sera pas capable de démarrer en mode
   lecture-écriture.
   Si vous générez la clef ssh par Isisfish, cette case à cochez sera
   automatiquement renseigné au retour de l'écran de génération de clef (si
   tout c'est bien déroulé). 
#. **changer la clef ssh** :
   pour changer l'emplacement de votre clef privée ssh dans la configuration
   d'IsisFish. A noter, que si vous changer votre clef ici, vous devez aussi
   renseigner la case à cochez **nopassphrase** (TODO faire ca automatiquement : tester si pass=empty)
#. **générer** : pour générer une clef ssh.
   Ce controle n'est pas accessible tant que vous n'avez pas saisi de login
   utilisateur.
   Si vous avez déjà renseigné un chemin de clef privé, il sera utilisé et
   écrasera votre ancienne clef (après confirmation de votre part), sinon on
   utilisera par défaut le fichier suivant **~/.isis-ssh-key** pour la clef privée.
   La clef publique sera **~/.isis-ssh-key.pub**.
   Un nouvel écran apparait pour vous permettre de renseigner la passphrase que
   vous voulez associer à votre clef privée ssh (voir section suivante) et
   lancer la génération de la clef privé et publique.
#. **appliquer** :
   pour accepter les modifications effectuées. A noter que le bouton est grisé
   tant que la configuration n'est pas valide.
#. **annuler** :
   pour annuler les modifications effectuées.  A noter que si la configuration
   n'était pas valide auparavant (cas par exemple d'une première utilisation),
   IsisFish ne démarrera pas.
#. **documentation en ligne** :
   ce lien pointe vers la page de documentation pour enregistrer votre clef
   publique ssh sur le serveur du labs.
#. **accès au labs ** : ce lien pointe vers votre page du labs pour enregistrer
   votre clef publique ssh.
   A noter qu'un message d'avertissement vous est addressé si vous n'avez pas
   choisi le mode lecture-écriture, ou si vous n'avez pas encore défini (ou
   générer) votre clef ssh. Voir section **Dépot de clef publique ssh sur le labs**

Génération de clef ssh
~~~~~~~~~~~~~~~~~~~~~~
.. image:: images/isisFishData_02_generateKey.png

#. chemin de la **clef privée** de l'utilisateur :
   apparait ici la localisation de votre clef privée à générer.
#. chemin de la **clef publique** de l'utilisateur :
   apparait ici la localisation de votre clef publique à générer. (Son nom est
   celui de la clef privée suffixé par .pub).
#. **passe phrase**:
   Vous pouvez protéger votre clef privée par une passe phrase. Cette donnée
   sensible ne sera jamais sauvegardée sur le système et il vous faudra la saisir
   à chaque lancement d'Isis-fish (par interface graphique si vous être en mode
   graphique) ou dans la console en mode 'script' (TODO étudier une option?).
#. **confirmer la passe phrase** :
   Si vous utilisez une passe phrase, confirmez la ici, quand les deux phrases
   seront identiques, les libellés ne seront plus rouges.
#. **appliquer** :
   Pour lancer la génération de la clef. Le controle n'est accessible que lorsque
   les deux passe phrase correspondent.
   Si l'étape se déroule avec succès il vous sera demandé si vous voulez
   déposer la clef publique sur le labs.
   Si vous répondez par l'affirmative, reportez vous à la section suivante.   
#. **annuler** :
   Pour annuler la génération de la clef, rien ne sera écrit sur le disque.

Dépot de clef publique ssh sur le labs
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

   Que ce soit en cliquant sur le lien pour accéder au labs ou en acceptant de
   déposer la clef suite à la génération d'une nouvelle clef, la démarche pour
   enregistrer votre clef est la même.

   Tout d'abord le contenu de votre clef publique est inséré dans le presse-papier,
   puis un navigateur s'ouvrira sur votre page de login sur le labs.

.. image:: images/add_ssh_key_00.png

   Une fois authentifié, vous serez automatiquement redirigé sur votre page de
   dépot de clef ssh.

.. image:: images/add_ssh_key_01.png

   Il vous suffit alors de positionner le curseur sur le dernier caractère de la
   zone d'édition (1) (si vous aviez déjà des clefs d'enregistrées) et de coller le
   contenu du presse-papier (Ctrl-v), puis valider sur le bouton **Mettre à jour** (2).

   Vous clef sera active dans les deux heures.

Validation de la configuration
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A la sortie de l'écran de configuration, si votre configuration n'est pas valide,
un message vous l'indiquera et vous proposera de retourner modifier votre configuration.
 
Tant que votre accès en écriture n'est pas actif, vous serez automatiquement
basculé en accès anonyme. TODO Voir comment ne pas écraser la configuration...
