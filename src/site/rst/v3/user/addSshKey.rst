.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
===============================
Ajouter la clef ssh sur le labs
===============================

Ce tutorial explique la démarche à suivre afin de pouvoir publier des documents
à partir de l'application IsisFish vers le serveurs de données (vcs).

Le serveur vcs du labs utilise le protocole ssh pour la communication du vcs.

Pour vous permettre d'avoir des droits d'écriture, il faut que vous enregistriez
votre clef ssh publique sur ce serveur.

Cela se fait en 2 étapes.

1. Création de la clef ssh
--------------------------

IsisFish permet en le lançant en ligne de commande de vous générer la paire de
clefs ssh public-privé.

Pour ce faire, lancer la commande <pre>isisfish --create-ssh-key</pre> ou <pre>isisfish -c</pre>. TODO Revoir ...

Cette action va générer pour vous la paire de clef public-privé. Il vous sera
demandé pendant cette opération des informations pour la constitution des clefs.

Aprés l'opération, les clefs ont été générées dans un répertoire que vous
pouvez modifier dans la configuration d'IsisFish. (voir configuration Isis).

2. Enregistrement de la clef publique sur le labs
-------------------------------------------------

Une fois votre clef public-privée crées, il faut transmettre au serveur du labs
votre clef publique (elle a été générée à l'étape précédente).

Récupérer le contenu du fichier id_dsa.pub (qui contient votre clef publique)

Le chemin exact de ce fichier est sauvegardé par l'application IsisFish et vous
pouvez le récupérer en accedant au paneau de configuration d'IsisFish et en
recherchant la valeur de la clef **vcs.ssh2.keyFile**.


Accès à la page de modification de clef publique
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: images/add_ssh_key_00.png


Rendez-vous à l'adresse suivante : https://labs.libre-entreprise.org/account/editsshkeys.php
pour accéder à votre page de modification de clef publique sur le labs.

Dans un premier temps, vous serez redirigé sur la page d'authentification

Entrez vos identifiants et cliquez sur **Connexion avec SSL** pour vous authentifier.

Une fois identifié, vous êtes redirigé sur la page de modification de vos clefs publiques.

Ajouter la clef publique
~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: images/add_ssh_key_01.png

Sur la page d'édition des clefs publiques, rajouter le contenu de votre clef
publique précedemment récupéré du fichier adéquate dans la zone d'édition (1).

Appuyez ensuite sur **Mettre à jour** (2) pour enregistrer votre nouvelle clef publique.

Attention, les sauts de lignes ne sont pas autorisés et cela peut empécher le système de reconnaitre votre clef publique.
Malheureusement dans l'interface on ne distingue pas les espaces des sauts
de lignes.

Vérification de la prise en compte du changement
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: images/add_ssh_key_02.png

Une fois que vous avez appuyé sur **metre à jour**, vous être redirigé sur votre
page de gestion de compte.

Tout en bas, é gauche du libellé **Clés publiques SSH autorisées** se trouve un
nombre (1) qui reflète le nombre clef publiques ssh que le systéme a reconnu.

Si ce nombre n'est pas exactement le nombre de clefs que vous avez enregistrés,
cela signifie qu'un problème est survenu, il faut alors renouveller l'opération
en cliquant sur **Editer les clés** (2) pour retourner sur la page de modification
de vos clefs publiques.

