.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
Introduction
============

Objectifs
---------

La plupart des pêcheries sont des systèmes complexes en raison de la
diversité des ressources exploitées (multispécifique) et des activités
de pêche (multiflottille). Ces pêcheries sont dites mixtes ou
composites. La complexité des pêcheries provient aussi des
hétérogénéités spatiales et saisonnières des distributions des
ressources et de la pêche. Dans les pêcheries mixtes, les ressources
sont exploitées soit simultanément, soit de manière séquentielle par
des unités de pêche (navire et équipage) pratiquant différentes
activités de pêche. Il est ainsi difficile d'évaluer la dynamique des
ressources et de l'exploitation, et la mortalité par pêche qui en
résulte pour les populations exploitées. La diversité des activités de
pêche et des captures associées est due à la multiplicité des lieux de
pêche, des espèces ciblées et des engins utilisés, mais aussi à
d'autres facteurs comme le comportement individuel des pêcheurs et les
conditions économiques et environnementales.

Par ailleurs, les pêcheurs connaissent les distributions
spatio-temporelles des ressources, et ils allouent leur effort de
pêche en conséquence. Aux grandes échelles, les variations de ces
distributions spatiales sont principalement dues aux mouvements et
migrations ontogéniques des populations, occasionnant des
concentrations de certains stades démographiques dans certaines zones
à certaines périodes de l'année, en relation avec certains processus
du cycle de vie (reproduction, alimentation').

Dans ce contexte, il est essentiel de prendre en compte l'allocation
spatiale et saisonnière de l'effort de pêche entre les lieux de pêche
pour évaluer la dynamique de la pêcherie. Dans les pêcheries mixtes,
ces aspects sont d'autant plus importants que les pêcheurs peuvent non
seulement changer de lieu de pêche, mais aussi d'engin et
d'espèce-cible. Des modèles spatialement explicites sont nécessaires
pour comprendre la dynamique de ces pêcheries.

ISIS-Fish est un modèle de simulation spatial et saisonnier qui décrit
la dynamique des ressources, de l'exploitation et de la gestion. Il a
été développé pour explorer les conséquences d'un ensemble de mesures
de gestion sur la dynamique des pêcheries. Il permet de comparer les
impacts respectifs de mesures de gestion conventionnelles comme les
Totaux Autorisés de Capture (TAC), des contrôles sur l'effort de
pêche, des mesures techniques sur les engins et des mesures comme des
Aires Marines Protégées (AMP) au sens large, i.e. des mesures de
gestion spatialisées.

ISIS-Fish est aussi générique que possible afin d'être applicable à
différents types de pêcherie. Le logiciel comprend une base de données
qui permet d'intégrer la connaissance sur la pêcherie tout en étant
facilement modifiable. Cette connaissance comprend les paramètres
décrivant chaque population et chaque activité de pêche.

ISIS-Fish permet une grande flexibilité pour plusieurs hypothèses du
modèle, notamment les relations entre stock de géniteurs et
reproduction, les relations de sélectivité, etc. ; et ce afin de
garantir une utilisation pour un grand nombre de pêcheries démersales
et benthiques. Les mesures de gestion et la réponse des pêcheurs à ces
mesures et aux conditions économiques et environnementales peuvent
être codées interactivement grâce à un langage de script.

Des applications d'ISIS-Fish à plusieurs pêcheries européennes sont
en cours de développement, dans le cadre de projets de recherche
nationaux et européens.

Description du modèle
---------------------

ISIS-Fish est destiné à évaluer l'efficacité de mesures de gestion
saisonnières et spatialisées sur des pêcheries mixtes. Cela passe par
le contrôle de certaines variables d'exploitation, notamment captures
et effort de pêche.

Le simulateur repose sur trois sous-modèles: un modèle de la dynamique
des populations, un modèle de l'activité de pêche et un modèle de
gestion. Chaque sous-modèle comporte une dimension spatiale et une
dimension saisonnière

.. image:: images/modelesFr.jpg

La région de la pêcherie se définit par ses contours et est découpée
en mailles. La résolution spatiale du maillage est choisie en fonction
de la dynamique à décrire et des données disponibles. Des zones (c'est
à dire des ensembles de mailles contiguës) sont ensuite définies
indépendamment pour chaque population, chaque activité de pêche et
chaque mesure de gestion.

Le modèle a un pas de temps mensuel. Des saisons (c'est à dire une
suite de mois) sont également définies indépendamment pour chaque
population, chaque activité de pêche et chaque mesure de gestion.  Au
sein de chaque zone et pour chaque saison, l'effort de pêche d'une
activité particulière, ou l'abondance d'une population, sont supposées
homogènes et uniformément distribuées.

