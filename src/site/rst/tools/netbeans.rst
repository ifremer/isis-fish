.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
Comment débugger avec NetBeans ?
================================

Ce tutoriel explique comment installer, configurer et utiliser une plateforme de
développement java utilisant le JDK (Kit de développement Java), Maven 2
(Logiciel de gestion et d'automatisation pour les projets Java) et NetBeans
(Environnement de développement intégré).

Installation du JDK
-------------------

Le JDK est l'environnement dans lequel le code Java est compilé afin que la JVM
(machine virtuelle de Java) puisse l'interpréter.

Pour acquérir la dernière version du JDK il faut se rendre sur la page de
téléchargement de Sun et télécharger la version propre à son système
d'exploitation (Plateform) : http://java.sun.com/javase/downloads/index.jsp

Pour Windows
~~~~~~~~~~~~

Il faut exécuter le fichier d'installation et l'installer dans le répertoire
désiré.

Pour Linux
~~~~~~~~~~

Il faut l'exécuter et l'installer dans le répertoire désiré::

  sh jdk-6<version>-linux-i586.bin


Reste plus qu'à renseigner les variables d'environnements. Pour que cela soit
valable à chaque démarrage, il faut éditer le fichier ~/.bashrc et ajouter à la
fin les lignes suivantes::

  JAVA_HOME=chemin_ou_se_trouve_jdk
  export JAVA_HOME
  PATH=$PATH:$JAVA_HOME/bin
  export PATH

Installation de Maven
---------------------

Maven est utilisé dans de nombreux projet Java pour gérer et automatiser les
dépendances.

Pour acquérir la dernière version de Maven il faut se rendre sur la page de
téléchargement d'Apache : http://maven.apache.org/download.html

Pour Windows
~~~~~~~~~~~~

Préférez la version zipée et dézipez le vers le répertoire désiré.

Il faut ensuite renseigner les variables d'environnements : Cliquez sur
Démarrer > Panneau de configuration > Système > Parametres avancés > Variables
d'environnements et renseignez la variable PATH avec
chemin_ou_se_trouve_maven\bin et créez la variable M2_HOME et renseignez avec la
valeur : le chemin_ou_se_trouve_maven (pas le chemin du bin).

Pour Linux
~~~~~~~~~~

Préférez la version tar.gz et décompressez le avec la commande::

  tar-zxvf apache-maven-<version>-bin.tar.gz

Déplacez le répertoire vers le dossier désiré::

  mv apache-maven chemin_ou_se_trouve_maven

Reste là aussi à renseigner les variables d'environnements en éditant le fichier
~/.bashrc et rajoutez à la fin les lignes suivantes::

  M2_HOME=chemin_ou_se_trouve_maven
  export M2_HOME
  M2=$M2_HOME/bin
  export M2
  PATH=$PATH:$M2
  export PATH

Installation de NetBeans
------------------------

Téléchargez la dernière version de NetBeans Web & Java EE correspondante à votre
Système d'exploitation (Plateform) sur http://www.netbeans.org.

Pour Windows
~~~~~~~~~~~~

Exécutez le .exe téléchargé et installez le dans le répertoire désiré.

Pour Linux
~~~~~~~~~~

Il faut l'exécuter et l'installer dans le répertoire désiré::

  sh netbeans-<version>-javaee-linux.sh

Configuration de NetBeans
-------------------------

Une fois NetBeans lancé, on va le configurer pour qu'il utilise le maven
installé. Pour cela, allez dans le menu Tools > Options, dans l'onglet
"Miscellaneous" cliquez sur l'onglet Maven. Renseignez le répertoire où Maven
est installé (External Maven Home) et cochez l'option "Always use external Maven
for building projects", confirmez.

Importation d'un projet depuis un dépôt Subversion (SVN)
--------------------------------------------------------

Cliquez sur le menu Versionning > Subversion > Checkout, renseignez le
Repository URL, par exemple, pour isis-fish en anonyme il faudra renseigner::

  svn://labs.libre-entreprise.org/svnroot/isis-fish-data

Choisissez la version désirée et spécifiez où le projet va être installé.

Débuggage du projet
-------------------

En règle général, le débuggage du projet est intéressant qu'à partir d'un
certain moment dans l'exécution du projet. Pour sélectionner le moment à partir
duquel on veut que le projet s'arrête pour nous laisser le temps d'observer les
comportements et l'état du programme, il faut se rendre dans les sources à
l'endroit approprié et double cliquer dans la marge de l'éditeur. La ligne
devrait devenir rouge et un carré rouge devrait apparaitre. Cela signifie que le
programme, lors de son exécution en mode debug va s'arrêter à ce point d'arrêt.
On peut en ajouter autant que souhaité. Il suffit de cliquer sur l'icone "Debug
Main Project" (ctrl F5) pour lancer l'exécution du programme en mode debug.
