.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
==============================================================
Utilisation d'un outil externe d'édition des scripts (Eclipse)
==============================================================

Documentation non à jour (préférez l'utilisation de [[v3/usermanual/netbeanseditor|Netbeans]])
==============================================================================================

Ce document montre comment éditer les scripts (Java) avec un editeur
externe. Dans ce document nous nous baserons sur l'éditeur Eclipse mais tout
autre éditeur fonctionne.


Pré-requis
==========

Il faut avoir les programmes suivant installés et fonctionnels:

- Une connection Internet durant l'utilisation de maven
- Eclipse http://www.eclipse.org
- Maven http://maven.apache.org


Création d'un projet Eclipse
============================

Grâce à Maven nous allons pouvoir créer les fichiers nécessaires à un
projet Eclipse.

Lancer la commande::

  mvn scm:checkout -DconnectionUrl=scm:cvs:pserver:anonymous:@cvs.labs.libre-entreprise.org:/cvsroot/isis-fish:isis-fish-data

Vous avez alors un répertoire target/checkout dans lequel vous trouvez
les fichiers:

- pom.xml
- profiles.xml

vous les déplacez dans votre répertoire de data (sans doute
$HOME/isis-database-3) et vous pouvez effacer le répertoire target et
ce qu'il contient.

Dans votre répertoire de data (sans doute $HOME/isis-database-3) lancer la
commande::

  mvn eclipse:eclipse

puis forcé la récupération des librairies avec::

  mvn compile

Import du projet dans Eclipse
=============================

- Allez dans le menu File->Import...
- Sélectionnez General/Existing Projects into Workspace
- Sélectionnez le répertoire de data
- Cliquez sur Finish

Il faut ensuite créer une variable M2_REPO qui pointe sur
$HOME/.m2/repository. Pour cela suivre le chemin suivant::

  Project -> properties -> Java build path -> Libraries -> Add Variable

Lancement d'isis
================

Il faut maintenant lancer isis avec l'option debug en premier argument::

  # sous Unix
  ./go.sh debug
  # sous Windows
  go.bat debug


Debuggage dans Eclipse
======================

- Allez dans le menu Run->Debug...
- Cliquez avec le bouton droit sur "Remote Java Application"
- Cliquez sur New

Dans l'onglet Connect:

- Donnez un nom pour le projet: isis-fish
- Modifier le port pour le mettre à 38000

Dans l'onglet Source

- Ajouter le nouveau projet

Maintenant cliqué sur Debug en bas de la fenêtre, Eclipse doit se connecter
à Isis. Il vous faut maintenant poser des points d'arret dans votre code
pour qu'Eclipse arrete l'execution et que vous puissiez faire du pas a pas à
partir de ces lignes.
