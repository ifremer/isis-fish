.. -
.. * #%L
.. * IsisFish
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 1999 - 2010 Ifremer, Code Lutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -
Autres utilitaires
==================

* `Eclipse <http://www.eclipse.org>`_ (licence cpl) logiciel libre pour editer
  les fichiers et plus particulièrement programmer en java
  + `Utilisation-Installation de Eclipse <tools/eclipse.html>`_
* `Notepad++ <http://notepad-plus.sourceforge.net/fr/site.htm>`_ (licence gpl)
  logiciel libre pour editer les fichiers et plus particulièrement programmer en
  java
* `Netbeans <http://www.netbeans.org/>`_ (licence gpl) logiciel libre pour
  éditer les fichiers et plus particulièrement programmer en java

  + `Utilisation-Installation de Netbeans <tools/netbeans.html`_
  
