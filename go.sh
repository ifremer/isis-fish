#!/bin/sh

# Java executable
JAVA=java

TEMP=`getopt -o dnt:: --long debug,nosuspend,cheked,trace:: \
     -n 'go.sh' -- "$@"`

if [ $? != 0 ] ; then
  echo "bag argument"
  exit 1
fi

eval set -- "$TEMP"

while true ; do
    case "$1" in
                  -d|--debug)
        echo "Debug mode"
        DEBUG="-server -Xdebug -Xnoagent -Djava.compiler=NONE -Xrunjdwp:transport=dt_socket,address=38000,server=y"
        shift ;;
                  -n|--nosuspend)
        echo "Debug no suspend mode"
        DEBUG="$DEBUG,suspend=n"
        shift ;;
                  -c|--checked)
        echo "Checked mode"
        CHECK="-server -Djava.compiler=NONE ,address=38000,server=y"
        shift ;;
                  -t|--trace)
        echo "Trace mode"

            case "$2" in
                    "")
                TRACE_FILE="-Daspectwerkz.definition.file=$rep/src/main/resources/trace-aop.xml" # -Daspectwerkz.transform.verbose=true" # -Daspectwerkz.transform.details=true"
                ;;
                    *)
                TRACE_FILE="-Daspectwerkz.definition.file=$2" # -Daspectwerkz.transform.verbose=true" # -Daspectwerkz.transform.details=true"
            esac

        shift 2 ;;
                  --)
                  shift ;
                  break ;;
                  *)
                  echo "Bad argument $1!";
                  exit 1 ;;
    esac
done

dir=$(dirname $0)
cd $dir

VER=$(xmlstarlet sel -N "p=http://maven.apache.org/POM/4.0.0" -t -v "/p:project/p:version" pom.xml)
RELEASE=isis-fish-$VER

MX=512M

rep=`dirname $0`
rep=`cd $rep && pwd`

cd $rep

if [ ! -f $rep/target/$RELEASE.jar ]; then
  mvn -o compile jar:jar
fi

if [ ! -d $rep/target/lib ]; then
  mvn -o dependency:copy-dependencies
fi

aspectwerkz=""
CL=$rep/target/classes #:$rep/target/$RELEASE.jar
for f in $rep/target/lib/*.jar; do
  CL=$CL:$f
  if [ -n "$(echo $f | grep aspectwerkz-jdk5)" ]; then
    aspectwerkz="-javaagent:$f"
  fi
done

LOG="-Dlog4j.configuration=file:$rep/src/main/resources/log4j.properties" # -Dlog4j.debug"
echo $JAVA -Xmx$MX -Xms$MX -classpath $CL $NOaspectwerkz $TRACE_FILE $LOG $JVM_OPT $DEBUG $CHECK fr.ifremer.isisfish.IsisFish "$@"
$JAVA -Xmx$MX -Xms$MX -classpath $CL $NOaspectwerkz $TRACE_FILE $LOG $JVM_OPT $DEBUG $CHECK fr.ifremer.isisfish.IsisFish "$@"
#$JAVA -Xmx$MX -Xms$MX -classpath $CL $NOaspectwerkz $TRACE_FILE $LOG $JVM_OPT $DEBUG fr.ifremer.isisfish.ui.update.Counter "$@"
