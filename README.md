IsisFish
========

Simulateur de pêcherie complexe.

Compilation et autres
=====================
mvn clean   # nettoie le repertoire (target/*)
mvn compile # compile isis
mvn package # genere le fichier .jar et copie les dependances dans target
mvn assembly:single -DperformRelease=true # genere le zip de release: .jar + lib

Si on ne veut pas passer les tests lors de la compilation, il faut utiliser
l'option -Dmaven.test.skip=true
